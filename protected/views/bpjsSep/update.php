
<div class="page-header">
	<h1>Data BpjsSep</h1>
</div>
<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>

<div class="row-fluid">
	<div id="content" class="span12">
		<div class="row-fluid">
			<div class="navbar">
				<div class="navbar-inner">
					  <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'BpjsSep','url'=>Yii::app()->createUrl('BpjsSep/index')),
                            array('name' => 'Update')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); 
                        ?>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="block">
				<div class="navbar navbar-inner block-header">
					<div class="muted pull-left"><img src="/images/icon.png"/> Update BpjsSep</div>
				</div>
				<div class="block-content collapse in">
					<div class="span12">
						
<?php $this->renderPartial('_form', array('model'=>$model)); ?>					</div>
				</div>
			</div>
		</div>
	</div>
</div>

