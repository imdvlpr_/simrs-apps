<?php
/* @var $this BpjsSepController */
/* @var $model BpjsSep */

$this->breadcrumbs=array(
	'Bpjs Seps'=>array('index'),
	$model->NoSEP,
);

$this->menu=array(
	array('label'=>'List BpjsSep', 'url'=>array('index')),
	array('label'=>'Create BpjsSep', 'url'=>array('create')),
	array('label'=>'Update BpjsSep', 'url'=>array('update', 'id'=>$model->NoSEP)),
	array('label'=>'Delete BpjsSep', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->NoSEP),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BpjsSep', 'url'=>array('admin')),
);
?>

<h1>View BpjsSep #<?php echo $model->NoSEP; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'NoSEP',
		'JENIS_RAWAT',
		'KELAS_RAWAT',
		'NO_RM',
		'NO_RUJUKAN',
		'ASAL_RUJUKAN',
		'TGL_RUJUKAN',
		'TGL_SEP',
		'DIAG_AWAL',
		'POLI_TUJUAN',
		'CATATAN',
		'LAKA_LANTAS',
		'CREATED',
		'lokasi_laka',
	),
)); ?>
