<?php
/* @var $this BpjsSepController */
/* @var $model BpjsSep */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpjs-sep-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'NoSEP'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'NoSEP',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'NoSEP'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'JENIS_RAWAT'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'JENIS_RAWAT'); ?>
		<?php echo $form->error($model,'JENIS_RAWAT'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'KELAS_RAWAT'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'KELAS_RAWAT'); ?>
		<?php echo $form->error($model,'KELAS_RAWAT'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'NO_RM'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'NO_RM'); ?>
		<?php echo $form->error($model,'NO_RM'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'NO_RUJUKAN'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'NO_RUJUKAN'); ?>
		<?php echo $form->error($model,'NO_RUJUKAN'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'ASAL_RUJUKAN'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'ASAL_RUJUKAN'); ?>
		<?php echo $form->error($model,'ASAL_RUJUKAN'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'TGL_RUJUKAN'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'TGL_RUJUKAN'); ?>
		<?php echo $form->error($model,'TGL_RUJUKAN'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'TGL_SEP'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'TGL_SEP'); ?>
		<?php echo $form->error($model,'TGL_SEP'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'DIAG_AWAL'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'DIAG_AWAL',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DIAG_AWAL'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'POLI_TUJUAN'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'POLI_TUJUAN',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'POLI_TUJUAN'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'CATATAN'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'CATATAN',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'CATATAN'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'LAKA_LANTAS'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'LAKA_LANTAS'); ?>
		<?php echo $form->error($model,'LAKA_LANTAS'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'CREATED'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'CREATED'); ?>
		<?php echo $form->error($model,'CREATED'); ?>
		</div>
	</div>

	<div class="form-action">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
