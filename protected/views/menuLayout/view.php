<?php
/* @var $this MenuLayoutController */
/* @var $model MenuLayout */

$this->breadcrumbs=array(
	array('name'=>'Menu Layout','url'=>array('admin')),
	array('name'=>'Menu Layout'),
);

$this->menu=array(
	array('label'=>'List MenuLayout', 'url'=>array('index')),
	array('label'=>'Create MenuLayout', 'url'=>array('create')),
	array('label'=>'Update MenuLayout', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MenuLayout', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MenuLayout', 'url'=>array('admin')),
);
?>

<h1>View MenuLayout #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'link',
		'parent',
		'level',
		'urutan',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
