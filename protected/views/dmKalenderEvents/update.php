<?php
/* @var $this DmKalenderEventsController */
/* @var $model DmKalenderEvents */

$this->breadcrumbs=array(
	'Dm Kalender Events'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DmKalenderEvents', 'url'=>array('index')),
	array('label'=>'Create DmKalenderEvents', 'url'=>array('create')),
	array('label'=>'View DmKalenderEvents', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DmKalenderEvents', 'url'=>array('admin')),
);
?>

<h1>Update DmKalenderEvents <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>