<?php
/* @var $this DmKalenderEventsController */
/* @var $model DmKalenderEvents */

$this->breadcrumbs=array(
	'Dm Kalender Events'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DmKalenderEvents', 'url'=>array('index')),
	array('label'=>'Manage DmKalenderEvents', 'url'=>array('admin')),
);
?>

<h1>Create DmKalenderEvents</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>