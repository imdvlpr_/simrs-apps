<?php
/* @var $this DmKalenderEventsController */
/* @var $model DmKalenderEvents */

$this->breadcrumbs=array(
	'Dm Kalender Events'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DmKalenderEvents', 'url'=>array('index')),
	array('label'=>'Create DmKalenderEvents', 'url'=>array('create')),
	array('label'=>'Update DmKalenderEvents', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmKalenderEvents', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmKalenderEvents', 'url'=>array('admin')),
);
?>

<h1>View DmKalenderEvents #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama_event',
	),
)); ?>
