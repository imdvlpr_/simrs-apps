<?php
/* @var $this DmKalenderEventsController */
/* @var $data DmKalenderEvents */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_event')); ?>:</b>
	<?php echo CHtml::encode($data->nama_event); ?>
	<br />


</div>