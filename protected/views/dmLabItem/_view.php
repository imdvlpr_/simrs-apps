<?php
/* @var $this DmLabItemController */
/* @var $data DmLabItem */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kategori_id')); ?>:</b>
	<?php echo CHtml::encode($data->kategori_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelas_id')); ?>:</b>
	<?php echo CHtml::encode($data->kelas_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bhp')); ?>:</b>
	<?php echo CHtml::encode($data->bhp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jrs')); ?>:</b>
	<?php echo CHtml::encode($data->jrs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('japel')); ?>:</b>
	<?php echo CHtml::encode($data->japel); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_removed')); ?>:</b>
	<?php echo CHtml::encode($data->is_removed); ?>
	<br />

	*/ ?>

</div>