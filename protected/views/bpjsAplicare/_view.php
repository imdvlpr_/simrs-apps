<?php
/* @var $this BpjsAplicareController */
/* @var $data BpjsAplicare */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('koderuang')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->koderuang), array('view', 'id'=>$data->koderuang)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namaruang')); ?>:</b>
	<?php echo CHtml::encode($data->namaruang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodekelas')); ?>:</b>
	<?php echo CHtml::encode($data->kodekelas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kapasitas')); ?>:</b>
	<?php echo CHtml::encode($data->kapasitas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tersedia')); ?>:</b>
	<?php echo CHtml::encode($data->tersedia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tersediapria')); ?>:</b>
	<?php echo CHtml::encode($data->tersediapria); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tersediawanita')); ?>:</b>
	<?php echo CHtml::encode($data->tersediawanita); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tersediapriawanita')); ?>:</b>
	<?php echo CHtml::encode($data->tersediapriawanita); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>