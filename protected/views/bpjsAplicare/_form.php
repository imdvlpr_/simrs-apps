<?php
/* @var $this BpjsAplicareController */
/* @var $model BpjsAplicare */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpjs-aplicare-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'koderuang'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'koderuang',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'koderuang'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'namaruang'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'namaruang',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'namaruang'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'kodekelas'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'kodekelas',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'kodekelas'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'kapasitas'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'kapasitas'); ?>
		<?php echo $form->error($model,'kapasitas'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'tersedia'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'tersedia'); ?>
		<?php echo $form->error($model,'tersedia'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'tersediapria'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'tersediapria'); ?>
		<?php echo $form->error($model,'tersediapria'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'tersediawanita'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'tersediawanita'); ?>
		<?php echo $form->error($model,'tersediawanita'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'tersediapriawanita'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'tersediapriawanita'); ?>
		<?php echo $form->error($model,'tersediapriawanita'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'created'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
		</div>
	</div>

	<div class="form-action">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
