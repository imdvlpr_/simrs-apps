<?php
/* @var $this BpjsAplicareController */
/* @var $model BpjsAplicare */

$this->breadcrumbs=array(
	'Bpjs Aplicares'=>array('index'),
	$model->koderuang,
);

$this->menu=array(
	array('label'=>'List BpjsAplicare', 'url'=>array('index')),
	array('label'=>'Create BpjsAplicare', 'url'=>array('create')),
	array('label'=>'Update BpjsAplicare', 'url'=>array('update', 'id'=>$model->koderuang)),
	array('label'=>'Delete BpjsAplicare', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->koderuang),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BpjsAplicare', 'url'=>array('admin')),
);
?>

<h1>View BpjsAplicare #<?php echo $model->koderuang; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'koderuang',
		'namaruang',
		'kodekelas',
		'kapasitas',
		'tersedia',
		'tersediapria',
		'tersediawanita',
		'tersediapriawanita',
		'created',
	),
)); ?>
