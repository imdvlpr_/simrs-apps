<?php
/* @var $this TrRawatJalanTindakanController */
/* @var $model TrRawatJalanTindakan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_trx'); ?>
		<?php echo $form->textField($model,'kode_trx',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tr_rawat_jalan_id'); ?>
		<?php echo $form->textField($model,'tr_rawat_jalan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dm_tindakan_id'); ?>
		<?php echo $form->textField($model,'dm_tindakan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dokter_id'); ?>
		<?php echo $form->textField($model,'dokter_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nilai'); ?>
		<?php echo $form->textField($model,'nilai'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status_bayar'); ?>
		<?php echo $form->textField($model,'status_bayar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_at'); ?>
		<?php echo $form->textField($model,'updated_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->