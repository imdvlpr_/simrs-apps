<?php
/* @var $this TrRawatJalanTindakanController */
/* @var $data TrRawatJalanTindakan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_trx')); ?>:</b>
	<?php echo CHtml::encode($data->kode_trx); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tr_rawat_jalan_id')); ?>:</b>
	<?php echo CHtml::encode($data->tr_rawat_jalan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dm_tindakan_id')); ?>:</b>
	<?php echo CHtml::encode($data->dm_tindakan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dokter_id')); ?>:</b>
	<?php echo CHtml::encode($data->dokter_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilai')); ?>:</b>
	<?php echo CHtml::encode($data->nilai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_bayar')); ?>:</b>
	<?php echo CHtml::encode($data->status_bayar); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	*/ ?>

</div>