<?php
/* @var $this TrRawatJalanTindakanController */
/* @var $model TrRawatJalanTindakan */

$this->breadcrumbs=array(
	array('name'=>'Tr Rawat Jalan Tindakan','url'=>array('admin')),
	array('name'=>'Tr Rawat Jalan Tindakan'),
);

$this->menu=array(
	array('label'=>'List TrRawatJalanTindakan', 'url'=>array('index')),
	array('label'=>'Create TrRawatJalanTindakan', 'url'=>array('create')),
	array('label'=>'Update TrRawatJalanTindakan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrRawatJalanTindakan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrRawatJalanTindakan', 'url'=>array('admin')),
);
?>

<h1>View TrRawatJalanTindakan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kode_trx',
		'tr_rawat_jalan_id',
		'dm_tindakan_id',
		'dokter_id',
		'nilai',
		'status_bayar',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
