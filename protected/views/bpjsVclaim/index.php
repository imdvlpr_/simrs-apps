<?php
/* @var $this BpjsVclaimController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bpjs Vclaims',
);

$this->menu=array(
	array('label'=>'Create BpjsVclaim', 'url'=>array('create')),
	array('label'=>'Manage BpjsVclaim', 'url'=>array('admin')),
);
?>

<h1>Bpjs Vclaims</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
