<?php
/* @var $this BpjsVclaimController */
/* @var $model BpjsVclaim */

?>

<h1>Manage Bpjs Vclaim</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#bpjs-vclaim-grid').yiiGridView.update('bpjs-vclaim-grid', {
			    url:'?r=<?php echo Yii::app()->createUrl("BpjsVclaim/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span3" id="sidebar">


        </div> -->

        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">

                <div class="navbar">
                    <div class="navbar-inner">
                       <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'BpjsVclaim','url'=>Yii::app()->createUrl('BpjsVclaim/index')),
                            array('name' => 'List')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); 
                        ?>
                    </div>
                </div>
            </div>



            <div class="row-fluid">
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>


                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('BpjsVclaim/create'),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('BpjsVclaim[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size','size'=>1)); 
                            ?> 
                             <?php        echo CHtml::textField('BpjsVclaim[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    </div>
                    <div class="block-content collapse in">

<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'bpjs-vclaim-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		'id',
		'noSep',
		'tglMasuk',
		'tglKeluar',
		'jaminan',
		'poli',
		/*
		'ruangRawat',
		'kelasRawat',
		'spesialistik',
		'caraKeluar',
		'kondisiPulang',
		'tindakLanjut',
		'kodePPK',
		'tglKontrol',
		'kode_poli',
		'DPJP',
		'user',
		'kode_lpk',
		'created',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn',
									'previousPageCssClass'=>'btn',
									'nextPageCssClass'=>'btn',
									'lastPageCssClass'=>'btn',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn',
									'selectedPageCssClass'=>'btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'btn-group paging_full_numbers'
)); ?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>

</div>
