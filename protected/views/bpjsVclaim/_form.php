<?php
/* @var $this BpjsVclaimController */
/* @var $model BpjsVclaim */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpjs-vclaim-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'noSep'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'noSep',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'noSep'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'tglMasuk'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'tglMasuk'); ?>
		<?php echo $form->error($model,'tglMasuk'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'tglKeluar'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'tglKeluar'); ?>
		<?php echo $form->error($model,'tglKeluar'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'jaminan'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'jaminan'); ?>
		<?php echo $form->error($model,'jaminan'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'poli'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'poli',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'poli'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'ruangRawat'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'ruangRawat',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ruangRawat'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'kelasRawat'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'kelasRawat',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'kelasRawat'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'spesialistik'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'spesialistik',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'spesialistik'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'caraKeluar'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'caraKeluar',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'caraKeluar'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'kondisiPulang'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'kondisiPulang',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'kondisiPulang'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'tindakLanjut'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'tindakLanjut',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tindakLanjut'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'kodePPK'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'kodePPK',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kodePPK'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'tglKontrol'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'tglKontrol'); ?>
		<?php echo $form->error($model,'tglKontrol'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'kode_poli'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'kode_poli',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kode_poli'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'DPJP'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'DPJP',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'DPJP'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'user'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'user',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'user'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'kode_lpk'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'kode_lpk',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kode_lpk'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'created'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
		</div>
	</div>

	<div class="form-action">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
