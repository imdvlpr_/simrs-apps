<?php
/* @var $this BpjsVclaimController */
/* @var $data BpjsVclaim */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('noSep')); ?>:</b>
	<?php echo CHtml::encode($data->noSep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglMasuk')); ?>:</b>
	<?php echo CHtml::encode($data->tglMasuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglKeluar')); ?>:</b>
	<?php echo CHtml::encode($data->tglKeluar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jaminan')); ?>:</b>
	<?php echo CHtml::encode($data->jaminan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('poli')); ?>:</b>
	<?php echo CHtml::encode($data->poli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruangRawat')); ?>:</b>
	<?php echo CHtml::encode($data->ruangRawat); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kelasRawat')); ?>:</b>
	<?php echo CHtml::encode($data->kelasRawat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spesialistik')); ?>:</b>
	<?php echo CHtml::encode($data->spesialistik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('caraKeluar')); ?>:</b>
	<?php echo CHtml::encode($data->caraKeluar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kondisiPulang')); ?>:</b>
	<?php echo CHtml::encode($data->kondisiPulang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tindakLanjut')); ?>:</b>
	<?php echo CHtml::encode($data->tindakLanjut); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodePPK')); ?>:</b>
	<?php echo CHtml::encode($data->kodePPK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglKontrol')); ?>:</b>
	<?php echo CHtml::encode($data->tglKontrol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_poli')); ?>:</b>
	<?php echo CHtml::encode($data->kode_poli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DPJP')); ?>:</b>
	<?php echo CHtml::encode($data->DPJP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user')); ?>:</b>
	<?php echo CHtml::encode($data->user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_lpk')); ?>:</b>
	<?php echo CHtml::encode($data->kode_lpk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>