<?php
/* @var $this DmKamarController */
/* @var $model DmKamar */

$this->breadcrumbs=array(
	array('name'=>'Dm Kamar','url'=>array('admin')),
	array('name'=>'Dm Kamar'),
);

$this->menu=array(
	array('label'=>'List DmKamar', 'url'=>array('index')),
	array('label'=>'Create DmKamar', 'url'=>array('create')),
	array('label'=>'Update DmKamar', 'url'=>array('update', 'id'=>$model->id_kamar)),
	array('label'=>'Delete DmKamar', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_kamar),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmKamar', 'url'=>array('admin')),
);
?>

<h1>View DmKamar #<?php echo $model->id_kamar; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_kamar',
		'kelas_id',
		'nama_kamar',
		'tingkat_kamar',
		'biaya_kamar',
		'biaya_askep',
		'biaya_asnut',
		'jumlah_kasur',
		'terpakai',
		'created',
		'user_kamar',
		'is_hapus',
		'kamar_master_id',
	),
)); ?>
	</div>
</div>
