<?php
/* @var $this DmKamarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Dm Kamars'),
);

$this->menu=array(
	array('label'=>'Create DmKamar', 'url'=>array('create')),
	array('label'=>'Manage DmKamar', 'url'=>array('admin')),
);
?>

<h1>Dm Kamars</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
