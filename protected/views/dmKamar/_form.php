<?php
/* @var $this DmKamarController */
/* @var $model DmKamar */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dm-kamar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_id', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kelas_id'); ?>
		<?php echo $form->error($model,'kelas_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_kamar', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_kamar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama_kamar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tingkat_kamar', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tingkat_kamar'); ?>
		<?php echo $form->error($model,'tingkat_kamar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_kamar', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_kamar'); ?>
		<?php echo $form->error($model,'biaya_kamar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_askep', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_askep'); ?>
		<?php echo $form->error($model,'biaya_askep'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_asnut', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_asnut'); ?>
		<?php echo $form->error($model,'biaya_asnut'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jumlah_kasur', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jumlah_kasur'); ?>
		<?php echo $form->error($model,'jumlah_kasur'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'terpakai', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'terpakai'); ?>
		<?php echo $form->error($model,'terpakai'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'created', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'user_kamar', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'user_kamar',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'user_kamar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'is_hapus', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'is_hapus'); ?>
		<?php echo $form->error($model,'is_hapus'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kamar_master_id', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kamar_master_id'); ?>
		<?php echo $form->error($model,'kamar_master_id'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
