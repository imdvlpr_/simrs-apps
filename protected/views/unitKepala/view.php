<?php
/* @var $this UnitKepalaController */
/* @var $model UnitKepala */

$this->breadcrumbs=array(
	array('name'=>'Unit Kepala','url'=>array('admin')),
	array('name'=>'Unit Kepala'),
);

$this->menu=array(
	array('label'=>'List UnitKepala', 'url'=>array('index')),
	array('label'=>'Create UnitKepala', 'url'=>array('create')),
	array('label'=>'Update UnitKepala', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UnitKepala', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UnitKepala', 'url'=>array('admin')),
);
?>

<h1>View UnitKepala #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'unit_id',
		'pegawai_id',
		'signature',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
