<?php
/* @var $this BPendaftaranTindakanCodingController */
/* @var $model BPendaftaranTindakanCoding */

$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Tindakan Coding','url'=>array('admin')),
	array('name'=>'Bpendaftaran Tindakan Coding'),
);

$this->menu=array(
	array('label'=>'List BPendaftaranTindakanCoding', 'url'=>array('index')),
	array('label'=>'Create BPendaftaranTindakanCoding', 'url'=>array('create')),
	array('label'=>'Update BPendaftaranTindakanCoding', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BPendaftaranTindakanCoding', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BPendaftaranTindakanCoding', 'url'=>array('admin')),
);
?>

<h1>View BPendaftaranTindakanCoding #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'reg_id',
		'kode_dtd',
		'deskripsi',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
