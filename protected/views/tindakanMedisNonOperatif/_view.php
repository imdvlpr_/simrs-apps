<?php
/* @var $this TindakanMedisNonOperatifController */
/* @var $data TindakanMedisNonOperatif */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tindakan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tindakan), array('view', 'id'=>$data->id_tindakan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->kode_tindakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_tindakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelas_id')); ?>:</b>
	<?php echo CHtml::encode($data->kelas_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jrs')); ?>:</b>
	<?php echo CHtml::encode($data->jrs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('drOP')); ?>:</b>
	<?php echo CHtml::encode($data->drOP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('drANAS')); ?>:</b>
	<?php echo CHtml::encode($data->drANAS); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('perawatOK')); ?>:</b>
	<?php echo CHtml::encode($data->perawatOK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perawatANAS')); ?>:</b>
	<?php echo CHtml::encode($data->perawatANAS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tarip')); ?>:</b>
	<?php echo CHtml::encode($data->tarip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>