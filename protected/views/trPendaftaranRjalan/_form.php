<?php
/* @var $this TrPendaftaranRjalanController */
/* @var $model TrPendaftaranRjalan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tr-pendaftaran-rjalan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'NoDaftar'); ?>
		<?php echo $form->textField($model,'NoDaftar',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'NoDaftar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NoMedrec'); ?>
		<?php echo $form->textField($model,'NoMedrec'); ?>
		<?php echo $form->error($model,'NoMedrec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'GolPasien'); ?>
		<?php echo $form->textField($model,'GolPasien'); ?>
		<?php echo $form->error($model,'GolPasien'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KodePoli'); ?>
		<?php echo $form->textField($model,'KodePoli'); ?>
		<?php echo $form->error($model,'KodePoli'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KodeSubPlg'); ?>
		<?php echo $form->textField($model,'KodeSubPlg'); ?>
		<?php echo $form->error($model,'KodeSubPlg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KetPlg'); ?>
		<?php echo $form->textField($model,'KetPlg',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'KetPlg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UrutanPoli'); ?>
		<?php echo $form->textField($model,'UrutanPoli'); ?>
		<?php echo $form->error($model,'UrutanPoli'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KodeTdkLanjut'); ?>
		<?php echo $form->textField($model,'KodeTdkLanjut'); ?>
		<?php echo $form->error($model,'KodeTdkLanjut'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CaraMasuk'); ?>
		<?php echo $form->textField($model,'CaraMasuk',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'CaraMasuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KetCaraMasuk'); ?>
		<?php echo $form->textField($model,'KetCaraMasuk',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'KetCaraMasuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'StatusKunj'); ?>
		<?php echo $form->textField($model,'StatusKunj'); ?>
		<?php echo $form->error($model,'StatusKunj'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KetTdkL1'); ?>
		<?php echo $form->textField($model,'KetTdkL1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KetTdkL2'); ?>
		<?php echo $form->textField($model,'KetTdkL2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KetTdkL3'); ?>
		<?php echo $form->textField($model,'KetTdkL3',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KetTdkL4'); ?>
		<?php echo $form->textField($model,'KetTdkL4',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL4'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KetTdkL5'); ?>
		<?php echo $form->textField($model,'KetTdkL5',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL5'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NoAntriPoli'); ?>
		<?php echo $form->textField($model,'NoAntriPoli'); ?>
		<?php echo $form->error($model,'NoAntriPoli'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PostMRS'); ?>
		<?php echo $form->textField($model,'PostMRS',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'PostMRS'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'WAKTU_DAFTAR'); ?>
		<?php echo $form->textField($model,'WAKTU_DAFTAR'); ?>
		<?php echo $form->error($model,'WAKTU_DAFTAR'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->