<?php
/* @var $this TrPendaftaranRjalanController */
/* @var $model TrPendaftaranRjalan */

$this->breadcrumbs=array(
	'Tr Pendaftaran Rjalans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TrPendaftaranRjalan', 'url'=>array('index')),
	array('label'=>'Manage TrPendaftaranRjalan', 'url'=>array('admin')),
);
?>

<h1>Create TrPendaftaranRjalan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>