<?php
/* @var $this TrPendaftaranRjalanController */
/* @var $model TrPendaftaranRjalan */

$this->breadcrumbs=array(
	'Tr Pendaftaran Rjalans'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'List TrPendaftaranRjalan', 'url'=>array('index')),
	array('label'=>'Create TrPendaftaranRjalan', 'url'=>array('create')),
	array('label'=>'Update TrPendaftaranRjalan', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Delete TrPendaftaranRjalan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrPendaftaranRjalan', 'url'=>array('admin')),
);
?>

<h1>View TrPendaftaranRjalan #<?php echo $model->Id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'NoDaftar',
		'NoMedrec',
		'GolPasien',
		'KodePoli',
		'KodeSubPlg',
		'KetPlg',
		'UrutanPoli',
		'KodeTdkLanjut',
		'CaraMasuk',
		'KetCaraMasuk',
		'StatusKunj',
		'KetTdkL1',
		'KetTdkL2',
		'KetTdkL3',
		'KetTdkL4',
		'KetTdkL5',
		'NoAntriPoli',
		'PostMRS',
		'WAKTU_DAFTAR',
		'created',
	),
)); ?>
