<?php
/* @var $this TrPendaftaranRjalanController */
/* @var $model TrPendaftaranRjalan */

$this->breadcrumbs=array(
	'Tr Pendaftaran Rjalans'=>array('index'),
	$model->Id=>array('view','id'=>$model->Id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TrPendaftaranRjalan', 'url'=>array('index')),
	array('label'=>'Create TrPendaftaranRjalan', 'url'=>array('create')),
	array('label'=>'View TrPendaftaranRjalan', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Manage TrPendaftaranRjalan', 'url'=>array('admin')),
);
?>

<h1>Update TrPendaftaranRjalan <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>