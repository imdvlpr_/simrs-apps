<?php
/* @var $this TrPendaftaranRjalanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tr Pendaftaran Rjalans',
);

$this->menu=array(
	array('label'=>'Create TrPendaftaranRjalan', 'url'=>array('create')),
	array('label'=>'Manage TrPendaftaranRjalan', 'url'=>array('admin')),
);
?>

<h1>Tr Pendaftaran Rjalans</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
