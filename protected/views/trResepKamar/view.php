<?php
/* @var $this TrResepKamarController */
/* @var $model TrResepKamar */

$this->breadcrumbs=array(
	array('name'=>'Tr Resep Kamar','url'=>array('admin')),
	array('name'=>'Tr Resep Kamar'),
);

$this->menu=array(
	array('label'=>'List TrResepKamar', 'url'=>array('index')),
	array('label'=>'Create TrResepKamar', 'url'=>array('create')),
	array('label'=>'Update TrResepKamar', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrResepKamar', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrResepKamar', 'url'=>array('admin')),
);
?>

<h1>View TrResepKamar #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'no_resep',
		'dokter_id',
		'kode_daftar',
		'created',
		'petugas_id',
		'tgl_resep',
		'kamar_id',
		'pasien_id',
	),
)); ?>
	</div>
</div>
