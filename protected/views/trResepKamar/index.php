 <?php
$this->breadcrumbs=array(
	array('name'=>'Tr Resep Kamar','url'=>array('index')),
	array('name'=>'Manage'),
);

?>
<h1>Manage Tr Resep Kamar</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#tr-resep-kamar-grid').yiiGridView.update('tr-resep-kamar-grid', {
			    url:'<?php echo Yii::app()->createUrl("TrResepKamar/index"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                        <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('TrResepKamar/create'),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('TrResepKamar[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('TrResepKamar[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'tr-resep-kamar-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		'id',
		'no_resep',
		'dokter_id',
		'kode_daftar',
		'created',
		'petugas_id',
		/*
		'tgl_resep',
		'kamar_id',
		'pasien_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

