<?php
/* @var $this TrResepKamarController */
/* @var $data TrResepKamar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_resep')); ?>:</b>
	<?php echo CHtml::encode($data->no_resep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dokter_id')); ?>:</b>
	<?php echo CHtml::encode($data->dokter_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_daftar')); ?>:</b>
	<?php echo CHtml::encode($data->kode_daftar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('petugas_id')); ?>:</b>
	<?php echo CHtml::encode($data->petugas_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_resep')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_resep); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kamar_id')); ?>:</b>
	<?php echo CHtml::encode($data->kamar_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pasien_id')); ?>:</b>
	<?php echo CHtml::encode($data->pasien_id); ?>
	<br />

	*/ ?>

</div>