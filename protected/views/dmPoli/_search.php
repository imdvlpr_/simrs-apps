<?php
/* @var $this DmPoliController */
/* @var $model DmPoli */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_poli'); ?>
		<?php echo $form->textField($model,'id_poli'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_poli'); ?>
		<?php echo $form->textField($model,'kode_poli',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_poli'); ?>
		<?php echo $form->textField($model,'nama_poli',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->