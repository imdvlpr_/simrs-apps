<?php
/* @var $this DmPoliController */
/* @var $model DmPoli */

$this->breadcrumbs=array(
	'Dm Polis'=>array('index'),
	$model->id_poli,
);

$this->menu=array(
	array('label'=>'List DmPoli', 'url'=>array('index')),
	array('label'=>'Create DmPoli', 'url'=>array('create')),
	array('label'=>'Update DmPoli', 'url'=>array('update', 'id'=>$model->id_poli)),
	array('label'=>'Delete DmPoli', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_poli),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmPoli', 'url'=>array('admin')),
);
?>

<h1>View DmPoli #<?php echo $model->id_poli; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_poli',
		'kode_poli',
		'nama_poli',
		'created',
	),
)); ?>
