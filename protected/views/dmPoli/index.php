<?php
/* @var $this DmPoliController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dm Polis',
);

$this->menu=array(
	array('label'=>'Create DmPoli', 'url'=>array('create')),
	array('label'=>'Manage DmPoli', 'url'=>array('admin')),
);
?>

<h1>Dm Polis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
