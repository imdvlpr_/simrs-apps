<?php
/* @var $this DmPoliController */
/* @var $model DmPoli */

$this->breadcrumbs=array(
	'Dm Polis'=>array('index'),
	$model->id_poli=>array('view','id'=>$model->id_poli),
	'Update',
);

$this->menu=array(
	array('label'=>'List DmPoli', 'url'=>array('index')),
	array('label'=>'Create DmPoli', 'url'=>array('create')),
	array('label'=>'View DmPoli', 'url'=>array('view', 'id'=>$model->id_poli)),
	array('label'=>'Manage DmPoli', 'url'=>array('admin')),
);
?>

<h1>Update DmPoli <?php echo $model->id_poli; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>