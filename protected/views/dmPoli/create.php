<?php
/* @var $this DmPoliController */
/* @var $model DmPoli */

$this->breadcrumbs=array(
	'Dm Polis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DmPoli', 'url'=>array('index')),
	array('label'=>'Manage DmPoli', 'url'=>array('admin')),
);
?>

<h1>Create DmPoli</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>