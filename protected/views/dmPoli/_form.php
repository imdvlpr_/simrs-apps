<?php
/* @var $this DmPoliController */
/* @var $model DmPoli */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dm-poli-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'kode_poli'); ?>
		<?php echo $form->textField($model,'kode_poli',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'kode_poli'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_poli'); ?>
		<?php echo $form->textField($model,'nama_poli',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama_poli'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->