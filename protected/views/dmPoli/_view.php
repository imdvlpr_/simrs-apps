<?php
/* @var $this DmPoliController */
/* @var $data DmPoli */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_poli')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_poli), array('view', 'id'=>$data->id_poli)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_poli')); ?>:</b>
	<?php echo CHtml::encode($data->kode_poli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_poli')); ?>:</b>
	<?php echo CHtml::encode($data->nama_poli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />


</div>