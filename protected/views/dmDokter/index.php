<?php
/* @var $this DmDokterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Dm Dokters'),
);

$this->menu=array(
	array('label'=>'Create DmDokter', 'url'=>array('create')),
	array('label'=>'Manage DmDokter', 'url'=>array('admin')),
);
?>

<h1>Dm Dokters</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
