<?php
/* @var $this DmDokterController */
/* @var $model DmDokter */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dm-dokter-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_dokter', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_dokter',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama_dokter'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenis_dokter', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jenis_dokter',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'jenis_dokter'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'prosentasi_jasa', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'prosentasi_jasa'); ?>
		<?php echo $form->error($model,'prosentasi_jasa'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'alamat_praktik', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'alamat_praktik',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'alamat_praktik'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'telp', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'telp',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'telp'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'created', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_panggilan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_panggilan',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nama_panggilan'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
