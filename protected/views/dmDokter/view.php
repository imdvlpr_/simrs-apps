<?php
/* @var $this DmDokterController */
/* @var $model DmDokter */

$this->breadcrumbs=array(
	array('name'=>'Dm Dokter','url'=>array('admin')),
	array('name'=>'Dm Dokter'),
);

$this->menu=array(
	array('label'=>'List DmDokter', 'url'=>array('index')),
	array('label'=>'Create DmDokter', 'url'=>array('create')),
	array('label'=>'Update DmDokter', 'url'=>array('update', 'id'=>$model->id_dokter)),
	array('label'=>'Delete DmDokter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_dokter),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmDokter', 'url'=>array('admin')),
);
?>

<h1>View DmDokter #<?php echo $model->id_dokter; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_dokter',
		'nama_dokter',
		'jenis_dokter',
		'prosentasi_jasa',
		'alamat_praktik',
		'telp',
		'created',
		'nama_panggilan',
	),
)); ?>
	</div>
</div>
