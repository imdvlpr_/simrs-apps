<?php
/* @var $this GolPasienController */
/* @var $model GolPasien */

$this->breadcrumbs=array(
	array('name'=>'Gol Pasien','url'=>array('admin')),
	array('name'=>'Gol Pasien'),
);

$this->menu=array(
	array('label'=>'List GolPasien', 'url'=>array('index')),
	array('label'=>'Create GolPasien', 'url'=>array('create')),
	array('label'=>'Update GolPasien', 'url'=>array('update', 'id'=>$model->KodeGol)),
	array('label'=>'Delete GolPasien', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->KodeGol),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GolPasien', 'url'=>array('admin')),
);
?>

<h1>View GolPasien #<?php echo $model->KodeGol; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'KodeGol',
		'NamaGol',
		'a_kpid',
		'Inisial',
		'KodeKlsHak',
		'JenisKlsHak',
		'KodeAturan',
		'NoAwal',
		'IsPBI',
		'MblAmbGratis',
		'MblJnhGratis',
		'KDJNSKPST',
		'KDJNSPESERTA',
		'IsKaryawan',
	),
)); ?>
	</div>
</div>
