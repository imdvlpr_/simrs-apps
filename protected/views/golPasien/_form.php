<?php
/* @var $this GolPasienController */
/* @var $model GolPasien */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gol-pasien-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'NamaGol', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'NamaGol',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'NamaGol'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'a_kpid', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'a_kpid'); ?>
		<?php echo $form->error($model,'a_kpid'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Inisial', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Inisial',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'Inisial'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeKlsHak', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeKlsHak'); ?>
		<?php echo $form->error($model,'KodeKlsHak'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'JenisKlsHak', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'JenisKlsHak'); ?>
		<?php echo $form->error($model,'JenisKlsHak'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeAturan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeAturan'); ?>
		<?php echo $form->error($model,'KodeAturan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'NoAwal', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'NoAwal',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'NoAwal'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'IsPBI', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'IsPBI'); ?>
		<?php echo $form->error($model,'IsPBI'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'MblAmbGratis', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'MblAmbGratis'); ?>
		<?php echo $form->error($model,'MblAmbGratis'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'MblJnhGratis', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'MblJnhGratis'); ?>
		<?php echo $form->error($model,'MblJnhGratis'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KDJNSKPST', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KDJNSKPST'); ?>
		<?php echo $form->error($model,'KDJNSKPST'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KDJNSPESERTA', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KDJNSPESERTA'); ?>
		<?php echo $form->error($model,'KDJNSPESERTA'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'IsKaryawan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'IsKaryawan'); ?>
		<?php echo $form->error($model,'IsKaryawan'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
