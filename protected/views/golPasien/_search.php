<?php
/* @var $this GolPasienController */
/* @var $model GolPasien */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'KodeGol'); ?>
		<?php echo $form->textField($model,'KodeGol'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NamaGol'); ?>
		<?php echo $form->textField($model,'NamaGol',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'a_kpid'); ?>
		<?php echo $form->textField($model,'a_kpid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Inisial'); ?>
		<?php echo $form->textField($model,'Inisial',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeKlsHak'); ?>
		<?php echo $form->textField($model,'KodeKlsHak'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'JenisKlsHak'); ?>
		<?php echo $form->textField($model,'JenisKlsHak'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeAturan'); ?>
		<?php echo $form->textField($model,'KodeAturan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NoAwal'); ?>
		<?php echo $form->textField($model,'NoAwal',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IsPBI'); ?>
		<?php echo $form->textField($model,'IsPBI'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MblAmbGratis'); ?>
		<?php echo $form->textField($model,'MblAmbGratis'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MblJnhGratis'); ?>
		<?php echo $form->textField($model,'MblJnhGratis'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KDJNSKPST'); ?>
		<?php echo $form->textField($model,'KDJNSKPST'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KDJNSPESERTA'); ?>
		<?php echo $form->textField($model,'KDJNSPESERTA'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IsKaryawan'); ?>
		<?php echo $form->textField($model,'IsKaryawan'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->