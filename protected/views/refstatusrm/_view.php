<?php
/* @var $this RefstatusrmController */
/* @var $data Refstatusrm */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeStatusRM')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->KodeStatusRM), array('view', 'id'=>$data->KodeStatusRM)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NamaStatusRM')); ?>:</b>
	<?php echo CHtml::encode($data->NamaStatusRM); ?>
	<br />


</div>