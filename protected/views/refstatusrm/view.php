<?php
/* @var $this RefstatusrmController */
/* @var $model Refstatusrm */

$this->breadcrumbs=array(
	array('name'=>'Refstatusrm','url'=>array('admin')),
	array('name'=>'Refstatusrm'),
);

$this->menu=array(
	array('label'=>'List Refstatusrm', 'url'=>array('index')),
	array('label'=>'Create Refstatusrm', 'url'=>array('create')),
	array('label'=>'Update Refstatusrm', 'url'=>array('update', 'id'=>$model->KodeStatusRM)),
	array('label'=>'Delete Refstatusrm', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->KodeStatusRM),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Refstatusrm', 'url'=>array('admin')),
);
?>

<h1>View Refstatusrm #<?php echo $model->KodeStatusRM; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'KodeStatusRM',
		'NamaStatusRM',
	),
)); ?>
	</div>
</div>
