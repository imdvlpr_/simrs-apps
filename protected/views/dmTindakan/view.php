<?php
/* @var $this DmTindakanController */
/* @var $model DmTindakan */

$this->breadcrumbs=array(
	array('name'=>'Dm Tindakan','url'=>array('admin')),
	array('name'=>'Dm Tindakan'),
);

$this->menu=array(
	array('label'=>'List DmTindakan', 'url'=>array('index')),
	array('label'=>'Create DmTindakan', 'url'=>array('create')),
	array('label'=>'Update DmTindakan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmTindakan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmTindakan', 'url'=>array('admin')),
);
?>

<h1>View DmTindakan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kode',
		'nama',
		'biaya',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
