<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	array('name'=>'Pasien','link'=>Yii::app()->createUrl('pasien')),
	array('name'=>'Daftar'),
);

$this->title = 'Daftar Pasien';
$this->icon_title = '';
$this->description = '';
?>

<style>

#size{
width:8%;
}

</style>

<?php 
Yii::app()->clientScript->registerScript('cancel', "
    $('#search').change(function(){
		
        $('table#pasien-grid').yiiGridView.update('pasien-grid', {
            url:'?filter='+$('#search').val()+'&size='+$('#size').val()   });
			
     });

	   $('#size').change(function(){
		
        $('table#pasien-grid').yiiGridView.update('pasien-grid', {
            url:'?filter='+$('#search').val()+'&size='+$('#size').val()    });
		
     });	   
	
", CClientScript::POS_READY);

?>
<div class="datatable-wrapper form-inline" role="grid">
<div class="row-fluid">
<div class="widget-header no-border-bottom">
	<?php echo CHtml::dropDownList('User[PAGESIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,20=>20,30=>30,),array('id'=>'size','size'=>1)); ?>
	<label>data per halaman</label>
	<ul class="btn-toolbar pull-right" style="padding-right:10px">
           <li class="divider-vertical"></li>
			<li>
				<label>
				<?php
				echo CHtml::textField('Pasien[SEARCH]','',array('placeholder'=>'Cari','id'=>'search')); 
				?>
				</label>
			</li>
	</ul>
</div>
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'pasien-grid',
	'dataProvider'=>$model->search(),

	// 'filter'=>$model,
	'columns'=>array(
		'NoMedrec',
		'NAMA',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
			'buttons' => 
					array(
						'delete' => array(
							'url'=>'Yii::app()->createUrl("user/hapus/",array("id"=>$data->NoMedrec))',      
					        'click' => "function( e ){
					            e.preventDefault();
					            $( '#update-dialog' ).children( ':eq(0)' ).empty(); // Stop auto POST
					            updateDialog( $( this ).attr( 'href' ) );
					            $( '#update-dialog' )
					              .dialog( { title: 'Delete confirmation' } )
					              .dialog( 'open' ); }",
					       ),
								
						
					),
			'afterDelete'=>'function(link,success,data){ if(success) alert(data); }',
		),
	),
	'htmlOptions'=>array(
		'class'=>'table'
	),
	'pager'=>array(
                        'class'=>'SimplePager',                  
                        'header'=>'',                      
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',        
                        'lastPageLabel'=>'Terakhir',  
						'firstPageCssClass'=>'btn',
                        'previousPageCssClass'=>'btn',
                        'nextPageCssClass'=>'btn',        
                        'lastPageCssClass'=>'btn',
						'hiddenPageCssClass'=>'disabled',
						'internalPageCssClass'=>'btn',
						'selectedPageCssClass'=>'btn-sky',
						'maxButtonCount'=>5,
                ),
	'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
	'summaryCssClass'=>'table-message-info',
	'filterCssClass'=>'filter',
	'summaryText'=>'menampilkan {start} - {end} dari {count} data',
	'template'=>'{items}{summary}{pager}',
	'emptyText'=>'Data tidak ditemukan',
	'pagerCssClass'=>'btn-group paging_full_numbers',
)); ?>

</div>
</div>