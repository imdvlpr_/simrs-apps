<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
	'Forms',
);
?>

<!-- <div class="page-header">
	<h1>Forms</h1>
</div> -->
<style>
</style>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="row-fluid">
  <div class="span6">

<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>"Data Kepesertaan",
	));


			echo(CHtml::label('Nama KPST', 'name'));
			echo(CHtml::textField('name','',array('class'=>'input-xlarge')));

			echo(CHtml::label('No KPST', 'name'));
			echo(CHtml::textField('name','',array('class'=>'input-medium')));

			echo(CHtml::label('No SEP', 'name'));
			echo(CHtml::textField('name','',array('class'=>'input-medium')));

			echo(CHtml::label('Jenis Pasien', 'name'));
			echo(CHtml::textField('name','',array('class'=>'input-xlarge')));
	 $this->endWidget();


 ?>

 <?php
$this->beginWidget('zii.widgets.CPortlet', array('title'=>"Data Pasien",));
 	echo $form->labelEx($model,'NoMedrec');
	echo $form->textField($model,'NoMedrec',array('class'=>'input-large'));
	echo $form->error($model,'NoMedrec');

 	echo $form->labelEx($model,'NAMA');
	echo $form->textField($model,'NAMA',array('class'=>'input-large'));
	echo $form->error($model,'NAMA');

	echo $form->labelEx($model,'JENSKEL');
	echo $form->textField($model,'JENSKEL',array('class'=>'input-large'));
	echo $form->error($model,'JENSKEL');

	echo $form->labelEx($model,'TGLLAHIR');
	echo $form->textField($model,'TGLLAHIR',array('class'=>'input-large'));
	echo $form->error($model,'TGLLAHIR');

	echo '<BR>Alamat:<br>';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->labelEx($model,'ALAMAT_JALAN');
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->textField($model,'ALAMAT_JALAN',array('class'=>'input-large'));
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->error($model,'ALAMAT_JALAN');

	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->labelEx($model,'Desa');
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->textField($model,'Desa',array('class'=>'input-large'));
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->error($model,'Desa');

	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->labelEx($model,'Kecamatan');
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->textField($model,'Kecamatan',array('class'=>'input-large'));
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->error($model,'Kecamatan');

	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->labelEx($model,'Kota');
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->textField($model,'Kota',array('class'=>'input-large'));
	echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$form->error($model,'Kota');
	 $this->endWidget();
?>


    </div>
    <div class="span6">
    <?php
	$this->beginWidget('zii.widgets.CPortlet', array('title'=>"Data Detil Pasien",));

 	echo $form->labelEx($model,'TMPLAHIR');
	echo $form->textField($model,'TMPLAHIR',array('class'=>'input-large'));
	echo $form->error($model,'TMPLAHIR');

	echo $form->labelEx($model,'BeratLahir');
	echo $form->textField($model,'BeratLahir',array('class'=>'input-large'));
	echo $form->error($model,'BeratLahir');

	echo $form->labelEx($model,'GOLDARAH');
	echo $form->textField($model,'GOLDARAH',array('class'=>'input-large'));
	echo $form->error($model,'GOLDARAH');

	echo $form->labelEx($model,'AGAMA');
	echo $form->textField($model,'AGAMA',array('class'=>'input-large'));
	echo $form->error($model,'AGAMA');

	echo $form->labelEx($model,'STATUSPERKAWINAN');
	echo $form->textField($model,'STATUSPERKAWINAN',array('class'=>'input-large'));
	echo $form->error($model,'STATUSPERKAWINAN');

	echo $form->labelEx($model,'PEKERJAAN');
	echo $form->textField($model,'PEKERJAAN',array('class'=>'input-large'));
	echo $form->error($model,'PEKERJAAN');

	echo $form->labelEx($model,'JENISIDENTITAS');
	echo $form->textField($model,'JENISIDENTITAS',array('class'=>'input-large'));
	echo $form->error($model,'JENISIDENTITAS');


	echo $form->labelEx($model,'NOIDENTITAS');
	echo $form->textField($model,'NOIDENTITAS',array('class'=>'input-large'));
	echo $form->error($model,'NOIDENTITAS');


	echo $form->labelEx($model,'TELP');
	echo $form->textField($model,'TELP',array('class'=>'input-large'));
	echo $form->error($model,'TELP');


	echo $form->labelEx($model,'NamaOrtu');
	echo $form->textField($model,'NamaOrtu',array('class'=>'input-large'));
	echo $form->error($model,'NamaOrtu');

	echo $form->labelEx($model,'NamaSuamiIstri');
	echo $form->textField($model,'NamaSuamiIstri',array('class'=>'input-large'));
	echo $form->error($model,'NamaSuamiIstri');

	 $this->endWidget();


?>
    </div>

</div>


<?php $this->endWidget(); ?>
