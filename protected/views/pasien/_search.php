<?php
/* @var $this PasienController */
/* @var $model Pasien */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'NoMedrec'); ?>
		<?php echo $form->textField($model,'NoMedrec'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NAMA'); ?>
		<?php echo $form->textField($model,'NAMA',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ALAMAT'); ?>
		<?php echo $form->textField($model,'ALAMAT',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeKec'); ?>
		<?php echo $form->textField($model,'KodeKec'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TMPLAHIR'); ?>
		<?php echo $form->textField($model,'TMPLAHIR',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TGLLAHIR'); ?>
		<?php echo $form->textField($model,'TGLLAHIR'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PEKERJAAN'); ?>
		<?php echo $form->textField($model,'PEKERJAAN',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AGAMA'); ?>
		<?php echo $form->textField($model,'AGAMA',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'JENSKEL'); ?>
		<?php echo $form->textField($model,'JENSKEL',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'GOLDARAH'); ?>
		<?php echo $form->textField($model,'GOLDARAH',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TELP'); ?>
		<?php echo $form->textField($model,'TELP',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'JENISIDENTITAS'); ?>
		<?php echo $form->textField($model,'JENISIDENTITAS',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NOIDENTITAS'); ?>
		<?php echo $form->textField($model,'NOIDENTITAS',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'STATUSPERKAWINAN'); ?>
		<?php echo $form->textField($model,'STATUSPERKAWINAN',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BeratLahir'); ?>
		<?php echo $form->textField($model,'BeratLahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Desa'); ?>
		<?php echo $form->textField($model,'Desa',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeGol'); ?>
		<?php echo $form->textField($model,'KodeGol'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TglInput'); ?>
		<?php echo $form->textField($model,'TglInput'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'JamInput'); ?>
		<?php echo $form->textField($model,'JamInput'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AlmIp'); ?>
		<?php echo $form->textField($model,'AlmIp',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NoMedrecLama'); ?>
		<?php echo $form->textField($model,'NoMedrecLama'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NoKpst'); ?>
		<?php echo $form->textField($model,'NoKpst',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodePisa'); ?>
		<?php echo $form->textField($model,'KodePisa'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KdPPK'); ?>
		<?php echo $form->textField($model,'KdPPK',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NamaOrtu'); ?>
		<?php echo $form->textField($model,'NamaOrtu',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NamaSuamiIstri'); ?>
		<?php echo $form->textField($model,'NamaSuamiIstri',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->