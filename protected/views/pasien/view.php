<?php
/* @var $this PasienController */
/* @var $model Pasien */

$this->breadcrumbs=array(
	'Pasiens'=>array('index'),
	$model->NoMedrec,
);

$this->menu=array(
	array('label'=>'List Pasien', 'url'=>array('index')),
	array('label'=>'Create Pasien', 'url'=>array('create')),
	array('label'=>'Update Pasien', 'url'=>array('update', 'id'=>$model->NoMedrec)),
	array('label'=>'Delete Pasien', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->NoMedrec),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pasien', 'url'=>array('admin')),
);

?>

<h1>View Pasien #<?php echo $model->NoMedrec; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'NoMedrec',
		'NAMA',
		'ALAMAT',
		'KodeKec',
		'TMPLAHIR',
		'TGLLAHIR',
		'PEKERJAAN',
		'AGAMA',
		'JENSKEL',
		'GOLDARAH',
		'TELP',
		'JENISIDENTITAS',
		'NOIDENTITAS',
		'STATUSPERKAWINAN',
		'BeratLahir',
		'Desa',
		'KodeGol',
		'TglInput',
		'JamInput',
		'AlmIp',
		'NoMedrecLama',
		'NoKpst',
		'KodePisa',
		'KdPPK',
		'NamaOrtu',
		'NamaSuamiIstri',
	),
)); ?>
