<?php
/* @var $this TrObatRacikanController */
/* @var $model TrObatRacikan */

$this->breadcrumbs=array(
	array('name'=>'Tr Obat Racikan','url'=>array('admin')),
	array('name'=>'Tr Obat Racikan'),
);

$this->menu=array(
	array('label'=>'List TrObatRacikan', 'url'=>array('index')),
	array('label'=>'Create TrObatRacikan', 'url'=>array('create')),
	array('label'=>'Update TrObatRacikan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrObatRacikan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrObatRacikan', 'url'=>array('admin')),
);
?>

<h1>View TrObatRacikan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kode',
		'nama',
	),
)); ?>
	</div>
</div>
