<?php
/* @var $this NotifController */
/* @var $model Notif */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'keterangan'); ?>
		<?php echo $form->textField($model,'keterangan',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit_from_id'); ?>
		<?php echo $form->textField($model,'unit_from_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit_to_id'); ?>
		<?php echo $form->textField($model,'unit_to_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_read_from'); ?>
		<?php echo $form->textField($model,'is_read_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_read_to'); ?>
		<?php echo $form->textField($model,'is_read_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_hapus'); ?>
		<?php echo $form->textField($model,'is_hapus'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_id'); ?>
		<?php echo $form->textField($model,'item_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_at'); ?>
		<?php echo $form->textField($model,'updated_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->