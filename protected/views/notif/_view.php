<?php
/* @var $this NotifController */
/* @var $data Notif */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit_from_id')); ?>:</b>
	<?php echo CHtml::encode($data->unit_from_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit_to_id')); ?>:</b>
	<?php echo CHtml::encode($data->unit_to_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_read_from')); ?>:</b>
	<?php echo CHtml::encode($data->is_read_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_read_to')); ?>:</b>
	<?php echo CHtml::encode($data->is_read_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_hapus')); ?>:</b>
	<?php echo CHtml::encode($data->is_hapus); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('item_id')); ?>:</b>
	<?php echo CHtml::encode($data->item_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	*/ ?>

</div>