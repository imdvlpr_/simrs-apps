<?php
/* @var $this NotifController */
/* @var $model Notif */

$this->breadcrumbs=array(
	array('name'=>'Notif','url'=>array('admin')),
	array('name'=>'Notif'),
);

$this->menu=array(
	array('label'=>'List Notif', 'url'=>array('index')),
	array('label'=>'Create Notif', 'url'=>array('create')),
	array('label'=>'Update Notif', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Notif', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Notif', 'url'=>array('admin')),
);
?>

<h1>View Notif #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'keterangan',
		'unit_from_id',
		'unit_to_id',
		'is_read_from',
		'is_read_to',
		'is_hapus',
		'item_id',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
