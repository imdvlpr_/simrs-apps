<?php
/* @var $this TdOkBiayaController */
/* @var $model TdOkBiaya */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'td-ok-biaya-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'biaya_bhn'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'biaya_bhn'); ?>
		<?php echo $form->error($model,'biaya_bhn'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'td_register_ok_id'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'td_register_ok_id'); ?>
		<?php echo $form->error($model,'td_register_ok_id'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'biaya_dr_op'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'biaya_dr_op'); ?>
		<?php echo $form->error($model,'biaya_dr_op'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'biaya_dr_anas'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'biaya_dr_anas'); ?>
		<?php echo $form->error($model,'biaya_dr_anas'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'biaya_pwrt_ok'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'biaya_pwrt_ok'); ?>
		<?php echo $form->error($model,'biaya_pwrt_ok'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'biaya_prwt_anas'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'biaya_prwt_anas'); ?>
		<?php echo $form->error($model,'biaya_prwt_anas'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'created'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'td_ok_tindakan_id'); ?>
		<div class="controls">
		<?php echo $form->textField($model,'td_ok_tindakan_id'); ?>
		<?php echo $form->error($model,'td_ok_tindakan_id'); ?>
		</div>
	</div>

	<div class="form-action">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
