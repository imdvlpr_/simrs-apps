
<div class="page-header">
	<h1>Data TdOkBiaya</h1>
</div>
<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>

<div class="row-fluid">
	<div id="content" class="span12">
		<div class="row-fluid">
			<div class="navbar">
				<div class="navbar-inner">
					  <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'TdOkBiaya','url'=>Yii::app()->createUrl('TdOkBiaya/index')),
                            array('name' => 'Create')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); 
                        ?>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="block">
				<div class="navbar navbar-inner block-header">
					<div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form TdOkBiaya</div>
				</div>
				<div class="block-content collapse in">
					<div class="span12">
						
<?php $this->renderPartial('_form', array('model'=>$model)); ?>					</div>
				</div>
			</div>
		</div>
	</div>
</div>

