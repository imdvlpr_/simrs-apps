<?php
/* @var $this TdOkBiayaController */
/* @var $data TdOkBiaya */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_bhn')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_bhn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('td_register_ok_id')); ?>:</b>
	<?php echo CHtml::encode($data->td_register_ok_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_dr_op')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_dr_op); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_dr_anas')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_dr_anas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_pwrt_ok')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_pwrt_ok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_prwt_anas')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_prwt_anas); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('td_ok_tindakan_id')); ?>:</b>
	<?php echo CHtml::encode($data->td_ok_tindakan_id); ?>
	<br />

	*/ ?>

</div>