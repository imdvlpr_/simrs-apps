<?php
/* @var $this SettingController */
/* @var $data Setting */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('option_kode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->option_kode), array('view', 'id'=>$data->option_kode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('option_nama')); ?>:</b>
	<?php echo CHtml::encode($data->option_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('option_value')); ?>:</b>
	<?php echo CHtml::encode($data->option_value); ?>
	<br />


</div>