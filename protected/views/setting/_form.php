<?php
/* @var $this SettingController */
/* @var $model Setting */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'setting-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'option_kode', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'1')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'option_kode',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'option_kode'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'option_nama', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'option_nama',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'option_nama'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'option_value', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'3')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'option_value'); ?>
		<?php echo $form->error($model,'option_value'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
