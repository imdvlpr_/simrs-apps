<?php
/* @var $this JenisVisiteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Jenis Visites'),
);

$this->menu=array(
	array('label'=>'Create JenisVisite', 'url'=>array('create')),
	array('label'=>'Manage JenisVisite', 'url'=>array('admin')),
);
?>

<h1>Jenis Visites</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
