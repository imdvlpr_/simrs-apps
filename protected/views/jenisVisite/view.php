<?php
/* @var $this JenisVisiteController */
/* @var $model JenisVisite */

$this->breadcrumbs=array(
	array('name'=>'Jenis Visite','url'=>array('admin')),
	array('name'=>'Jenis Visite'),
);

$this->menu=array(
	array('label'=>'List JenisVisite', 'url'=>array('index')),
	array('label'=>'Create JenisVisite', 'url'=>array('create')),
	array('label'=>'Update JenisVisite', 'url'=>array('update', 'id'=>$model->id_jenis_visite)),
	array('label'=>'Delete JenisVisite', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_jenis_visite),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage JenisVisite', 'url'=>array('admin')),
);
?>

<h1>View JenisVisite #<?php echo $model->id_jenis_visite; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_jenis_visite',
		'nama_visite',
		'kode_visite',
		'created',
		'kelas_tingkat',
		'biaya',
	),
)); ?>
	</div>
</div>
