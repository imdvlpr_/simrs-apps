<?php
/* @var $this JenisVisiteController */
/* @var $data JenisVisite */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jenis_visite')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_jenis_visite), array('view', 'id'=>$data->id_jenis_visite)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_visite')); ?>:</b>
	<?php echo CHtml::encode($data->nama_visite); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_visite')); ?>:</b>
	<?php echo CHtml::encode($data->kode_visite); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelas_tingkat')); ?>:</b>
	<?php echo CHtml::encode($data->kelas_tingkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya')); ?>:</b>
	<?php echo CHtml::encode($data->biaya); ?>
	<br />


</div>