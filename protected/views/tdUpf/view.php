<?php
/* @var $this TdUpfController */
/* @var $model TdUpf */

$this->breadcrumbs=array(
	array('name'=>'Td Upf','url'=>array('admin')),
	array('name'=>'Td Upf'),
);

$this->menu=array(
	array('label'=>'List TdUpf', 'url'=>array('index')),
	array('label'=>'Create TdUpf', 'url'=>array('create')),
	array('label'=>'Update TdUpf', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TdUpf', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TdUpf', 'url'=>array('admin')),
);
?>

<h1>View TdUpf #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'is_removed',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
