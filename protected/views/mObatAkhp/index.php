<?php
/* @var $this MObatAkhpController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Mobat Akhps'),
);

$this->menu=array(
	array('label'=>'Create MObatAkhp', 'url'=>array('create')),
	array('label'=>'Manage MObatAkhp', 'url'=>array('admin')),
);
?>

<h1>Mobat Akhps</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
