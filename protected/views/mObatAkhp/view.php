<?php
/* @var $this MObatAkhpController */
/* @var $model MObatAkhp */

$this->breadcrumbs=array(
	array('name'=>'Mobat Akhp','url'=>array('admin')),
	array('name'=>'Mobat Akhp'),
);

$this->menu=array(
	array('label'=>'List MObatAkhp', 'url'=>array('index')),
	array('label'=>'Create MObatAkhp', 'url'=>array('create')),
	array('label'=>'Update MObatAkhp', 'url'=>array('update', 'id'=>$model->kd_barang)),
	array('label'=>'Delete MObatAkhp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kd_barang),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MObatAkhp', 'url'=>array('admin')),
);
?>

<h1>View MObatAkhp #<?php echo $model->kd_barang; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'kd_t',
		'kd_barang',
		'kd_js',
		'nama_barang',
		'nama_generik',
		'kekuatan',
		'satuan_kekuatan',
		'satuan',
		'jns_sediaan',
		'b_i_r',
		'gen_non',
		'nar_p_non',
		'oakrl',
		'kronis',
		'stok',
		'stok_min',
		'hb',
		'hna',
		'diskon',
		'hj',
	),
)); ?>
	</div>
</div>
