<?php
/* @var $this MObatAkhpController */
/* @var $model MObatAkhp */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'kd_t'); ?>
		<?php echo $form->textField($model,'kd_t',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kd_barang'); ?>
		<?php echo $form->textField($model,'kd_barang',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kd_js'); ?>
		<?php echo $form->textField($model,'kd_js',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_barang'); ?>
		<?php echo $form->textField($model,'nama_barang',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_generik'); ?>
		<?php echo $form->textField($model,'nama_generik',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kekuatan'); ?>
		<?php echo $form->textField($model,'kekuatan',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'satuan_kekuatan'); ?>
		<?php echo $form->textField($model,'satuan_kekuatan',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'satuan'); ?>
		<?php echo $form->textField($model,'satuan',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jns_sediaan'); ?>
		<?php echo $form->textField($model,'jns_sediaan',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'b_i_r'); ?>
		<?php echo $form->textField($model,'b_i_r',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gen_non'); ?>
		<?php echo $form->textField($model,'gen_non',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nar_p_non'); ?>
		<?php echo $form->textField($model,'nar_p_non',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'oakrl'); ?>
		<?php echo $form->textField($model,'oakrl',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kronis'); ?>
		<?php echo $form->textField($model,'kronis',array('size'=>60,'maxlength'=>75)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stok'); ?>
		<?php echo $form->textField($model,'stok'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stok_min'); ?>
		<?php echo $form->textField($model,'stok_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hb'); ?>
		<?php echo $form->textField($model,'hb'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hna'); ?>
		<?php echo $form->textField($model,'hna'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'diskon'); ?>
		<?php echo $form->textField($model,'diskon'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hj'); ?>
		<?php echo $form->textField($model,'hj'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->