<?php
/* @var $this UnitTindakanController */
/* @var $model UnitTindakan */

$this->breadcrumbs=array(
	array('name'=>'Unit Tindakan','url'=>array('admin')),
	array('name'=>'Unit Tindakan'),
);

$this->menu=array(
	array('label'=>'List UnitTindakan', 'url'=>array('index')),
	array('label'=>'Create UnitTindakan', 'url'=>array('create')),
	array('label'=>'Update UnitTindakan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UnitTindakan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UnitTindakan', 'url'=>array('admin')),
);
?>

<h1>View UnitTindakan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'unit_id',
		'dm_tindakan_id',
		'nilai',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
