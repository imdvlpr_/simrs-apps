<?php
/* @var $this DmLabGrupController */
/* @var $model DmLabGrup */

$this->breadcrumbs=array(
	array('name'=>'Dm Lab Grup','url'=>array('admin')),
	array('name'=>'Dm Lab Grup'),
);

$this->menu=array(
	array('label'=>'List DmLabGrup', 'url'=>array('index')),
	array('label'=>'Create DmLabGrup', 'url'=>array('create')),
	array('label'=>'Update DmLabGrup', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmLabGrup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmLabGrup', 'url'=>array('admin')),
);
?>

<h1>View DmLabGrup #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kode',
		'nama',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
