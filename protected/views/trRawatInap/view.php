<?php
/* @var $this TrRawatInapController */
/* @var $model TrRawatInap */

$this->breadcrumbs=array(
	'Tr Rawat Inaps'=>array('index'),
	$model->id_rawat_inap,
);

$this->menu=array(
	array('label'=>'List TrRawatInap', 'url'=>array('index')),
	array('label'=>'Create TrRawatInap', 'url'=>array('create')),
	array('label'=>'Update TrRawatInap', 'url'=>array('update', 'id'=>$model->id_rawat_inap)),
	array('label'=>'Delete TrRawatInap', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_rawat_inap),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrRawatInap', 'url'=>array('admin')),
);
?>

<h1>View TrRawatInap #<?php echo $model->id_rawat_inap; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_rawat_inap',
		'tanggal_masuk',
		'jam_masuk',
		'tanggal_keluar',
		'jam_keluar',
		'datetime_masuk',
		'datetime_keluar',
		'created',
		'pasien_id',
		'ri_item_id',
	),
)); ?>
