<?php
/* @var $this TrRawatInapController */
/* @var $model TrRawatInap */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tr-rawat-inap-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggal_masuk'); ?>
		<?php echo $form->textField($model,'tanggal_masuk'); ?>
		<?php echo $form->error($model,'tanggal_masuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jam_masuk'); ?>
		<?php echo $form->textField($model,'jam_masuk'); ?>
		<?php echo $form->error($model,'jam_masuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggal_keluar'); ?>
		<?php echo $form->textField($model,'tanggal_keluar'); ?>
		<?php echo $form->error($model,'tanggal_keluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jam_keluar'); ?>
		<?php echo $form->textField($model,'jam_keluar'); ?>
		<?php echo $form->error($model,'jam_keluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datetime_masuk'); ?>
		<?php echo $form->textField($model,'datetime_masuk'); ?>
		<?php echo $form->error($model,'datetime_masuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datetime_keluar'); ?>
		<?php echo $form->textField($model,'datetime_keluar'); ?>
		<?php echo $form->error($model,'datetime_keluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pasien_id'); ?>
		<?php echo $form->textField($model,'pasien_id'); ?>
		<?php echo $form->error($model,'pasien_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ri_item_id'); ?>
		<?php echo $form->textField($model,'ri_item_id'); ?>
		<?php echo $form->error($model,'ri_item_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->