<?php
/* @var $this TrRawatInapController */
/* @var $model TrRawatInap */

$this->breadcrumbs=array(
	'Tr Rawat Inaps'=>array('index'),
	$model->id_rawat_inap=>array('view','id'=>$model->id_rawat_inap),
	'Update',
);

$this->menu=array(
	array('label'=>'List TrRawatInap', 'url'=>array('index')),
	array('label'=>'Create TrRawatInap', 'url'=>array('create')),
	array('label'=>'View TrRawatInap', 'url'=>array('view', 'id'=>$model->id_rawat_inap)),
	array('label'=>'Manage TrRawatInap', 'url'=>array('admin')),
);
?>

<h1>Update TrRawatInap <?php echo $model->id_rawat_inap; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>