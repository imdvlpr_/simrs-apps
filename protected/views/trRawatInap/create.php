<?php
/* @var $this TrRawatInapController */
/* @var $model TrRawatInap */

$this->breadcrumbs=array(
	'Tr Rawat Inaps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TrRawatInap', 'url'=>array('index')),
	array('label'=>'Manage TrRawatInap', 'url'=>array('admin')),
);
?>

<h1>Create TrRawatInap</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>