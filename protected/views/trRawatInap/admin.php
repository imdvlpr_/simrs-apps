<?php
/* @var $this TrRawatInapController */
/* @var $model TrRawatInap */

$this->breadcrumbs=array(
	'Tr Rawat Inaps'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TrRawatInap', 'url'=>array('index')),
	array('label'=>'Create TrRawatInap', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tr-rawat-inap-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tr Rawat Inaps</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tr-rawat-inap-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_rawat_inap',
		'tanggal_masuk',
		'jam_masuk',
		'tanggal_keluar',
		'jam_keluar',
		'datetime_masuk',
		/*
		'datetime_keluar',
		'created',
		'pasien_id',
		'ri_item_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
