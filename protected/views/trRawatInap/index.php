<?php
/* @var $this TrRawatInapController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tr Rawat Inaps',
);

$this->menu=array(
	array('label'=>'Create TrRawatInap', 'url'=>array('create')),
	array('label'=>'Manage TrRawatInap', 'url'=>array('admin')),
);
?>

<h1>Tr Rawat Inaps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
