<?php
/* @var $this BPendaftaranRjalanTindakanController */
/* @var $model BPendaftaranRjalanTindakan */

$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Rjalan Tindakan','url'=>array('admin')),
	array('name'=>'Bpendaftaran Rjalan Tindakan'),
);

$this->menu=array(
	array('label'=>'List BPendaftaranRjalanTindakan', 'url'=>array('index')),
	array('label'=>'Create BPendaftaranRjalanTindakan', 'url'=>array('create')),
	array('label'=>'Update BPendaftaranRjalanTindakan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BPendaftaranRjalanTindakan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BPendaftaranRjalanTindakan', 'url'=>array('admin')),
);
?>

<h1>View BPendaftaranRjalanTindakan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'dm_poli_tindakan_id',
		'b_pendaftaran_rjalan_id',
		'tanggal',
		'biaya',
		'keterangan',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
