<?php
/* @var $this BPendaftaranController */
/* @var $model BPendaftaran */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpendaftaran-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'NoMedrec', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'NoMedrec'); ?>
		<?php echo $form->error($model,'NoMedrec'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodePtgs', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'3')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodePtgs'); ?>
		<?php echo $form->error($model,'KodePtgs'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'TGLDAFTAR', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'TGLDAFTAR'); ?>
		<?php echo $form->error($model,'TGLDAFTAR'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'JamDaftar', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'JamDaftar'); ?>
		<?php echo $form->error($model,'JamDaftar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'umurthn', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'6')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'umurthn'); ?>
		<?php echo $form->error($model,'umurthn'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'umurbln', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'7')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'umurbln'); ?>
		<?php echo $form->error($model,'umurbln'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'UmurHr', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'8')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'UmurHr'); ?>
		<?php echo $form->error($model,'UmurHr'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeJnsUsia', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'9')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeJnsUsia'); ?>
		<?php echo $form->error($model,'KodeJnsUsia'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeMasuk', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'10')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeMasuk'); ?>
		<?php echo $form->error($model,'KodeMasuk'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KetMasuk', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'11')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KetMasuk',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'KetMasuk'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'JnsRawat', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'12')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'JnsRawat'); ?>
		<?php echo $form->error($model,'JnsRawat'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'DUtama', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'13')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'DUtama',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DUtama'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'D1', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'14')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'D1',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'D1'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'D2', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'15')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'D2',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'D2'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'D3', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'16')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'D3',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'D3'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'D4', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'17')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'D4',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'D4'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'D5', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'18')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'D5',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'D5'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'P1', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'19')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'P1',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'P1'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'P2', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'20')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'P2',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'P2'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'P3', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'21')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'P3',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'P3'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'P4', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'22')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'P4',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'P4'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'P5', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'23')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'P5',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'P5'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'PxBaruLama', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'24')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'PxBaruLama',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'PxBaruLama'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'IdAntri', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'25')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'IdAntri'); ?>
		<?php echo $form->error($model,'IdAntri'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KunjunganKe', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'26')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KunjunganKe'); ?>
		<?php echo $form->error($model,'KunjunganKe'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KeluarRS', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'27')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KeluarRS',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'KeluarRS'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'TglKRS', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'28')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'TglKRS'); ?>
		<?php echo $form->error($model,'TglKRS'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'JamKRS', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'29')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'JamKRS'); ?>
		<?php echo $form->error($model,'JamKRS'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'LamaRawat', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'30')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'LamaRawat'); ?>
		<?php echo $form->error($model,'LamaRawat'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeUnitDaftar', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'31')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeUnitDaftar'); ?>
		<?php echo $form->error($model,'KodeUnitDaftar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KunjTerakhirRJ', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'32')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KunjTerakhirRJ'); ?>
		<?php echo $form->error($model,'KunjTerakhirRJ'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeGol', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'33')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeGol'); ?>
		<?php echo $form->error($model,'KodeGol'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeStatusRM', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'34')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeStatusRM'); ?>
		<?php echo $form->error($model,'KodeStatusRM'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'IsRGT', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'35')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'IsRGT'); ?>
		<?php echo $form->error($model,'IsRGT'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'NoSEP', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'36')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'NoSEP',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'NoSEP'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeSubPulang', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'37')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeSubPulang'); ?>
		<?php echo $form->error($model,'KodeSubPulang'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'DPJP', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'38')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'DPJP',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'DPJP'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
