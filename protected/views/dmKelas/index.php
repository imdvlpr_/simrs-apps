<?php
/* @var $this DmKelasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Dm Kelases'),
);

$this->menu=array(
	array('label'=>'Create DmKelas', 'url'=>array('create')),
	array('label'=>'Manage DmKelas', 'url'=>array('admin')),
);
?>

<h1>Dm Kelases</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
