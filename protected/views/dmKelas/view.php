<?php
/* @var $this DmKelasController */
/* @var $model DmKelas */

$this->breadcrumbs=array(
	array('name'=>'Dm Kelas','url'=>array('index')),
	array('name'=>'Dm Kelas'),
);

$this->menu=array(
	array('label'=>'List DmKelas', 'url'=>array('index')),
	array('label'=>'Create DmKelas', 'url'=>array('create')),
	array('label'=>'Update DmKelas', 'url'=>array('update', 'id'=>$model->id_kelas)),
	array('label'=>'Delete DmKelas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_kelas),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmKelas', 'url'=>array('admin')),
);
?>

<h1>View DmKelas #<?php echo $model->id_kelas; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_kelas',
		'kode_kelas',
		'nama_kelas',
		'kode_kelas_bpjs',
	),
)); ?>
	</div>
</div>
