<?php
/* @var $this TindakanMedisOperatifController */
/* @var $model TindakanMedisOperatif */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_tindakan'); ?>
		<?php echo $form->textField($model,'id_tindakan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_tindakan'); ?>
		<?php echo $form->textField($model,'nama_tindakan',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_tindakan'); ?>
		<?php echo $form->textField($model,'kode_tindakan',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kelas_id'); ?>
		<?php echo $form->textField($model,'kelas_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jrs'); ?>
		<?php echo $form->textField($model,'jrs'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'drOP'); ?>
		<?php echo $form->textField($model,'drOP'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'drANAS'); ?>
		<?php echo $form->textField($model,'drANAS'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'perawatOK'); ?>
		<?php echo $form->textField($model,'perawatOK'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'perawatANAS'); ?>
		<?php echo $form->textField($model,'perawatANAS'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total'); ?>
		<?php echo $form->textField($model,'total'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tarip'); ?>
		<?php echo $form->textField($model,'tarip'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->