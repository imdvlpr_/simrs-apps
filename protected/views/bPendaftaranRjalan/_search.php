<?php
/* @var $this BPendaftaranRjalanController */
/* @var $model BPendaftaranRjalan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NoDaftar'); ?>
		<?php echo $form->textField($model,'NoDaftar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodePoli'); ?>
		<?php echo $form->textField($model,'KodePoli'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeSubPlg'); ?>
		<?php echo $form->textField($model,'KodeSubPlg'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KetPlg'); ?>
		<?php echo $form->textField($model,'KetPlg',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UrutanPoli'); ?>
		<?php echo $form->textField($model,'UrutanPoli'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeTdkLanjut'); ?>
		<?php echo $form->textField($model,'KodeTdkLanjut'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'StatusKunj'); ?>
		<?php echo $form->textField($model,'StatusKunj'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KetTdkL1'); ?>
		<?php echo $form->textField($model,'KetTdkL1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KetTdkL2'); ?>
		<?php echo $form->textField($model,'KetTdkL2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KetTdkL3'); ?>
		<?php echo $form->textField($model,'KetTdkL3',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KetTdkL4'); ?>
		<?php echo $form->textField($model,'KetTdkL4',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KetTdkL5'); ?>
		<?php echo $form->textField($model,'KetTdkL5',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NoAntriPoli'); ?>
		<?php echo $form->textField($model,'NoAntriPoli'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PostMRS'); ?>
		<?php echo $form->textField($model,'PostMRS',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->