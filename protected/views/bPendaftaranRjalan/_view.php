<?php
/* @var $this BPendaftaranRjalanController */
/* @var $data BPendaftaranRjalan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoDaftar')); ?>:</b>
	<?php echo CHtml::encode($data->NoDaftar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodePoli')); ?>:</b>
	<?php echo CHtml::encode($data->KodePoli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeSubPlg')); ?>:</b>
	<?php echo CHtml::encode($data->KodeSubPlg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KetPlg')); ?>:</b>
	<?php echo CHtml::encode($data->KetPlg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UrutanPoli')); ?>:</b>
	<?php echo CHtml::encode($data->UrutanPoli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeTdkLanjut')); ?>:</b>
	<?php echo CHtml::encode($data->KodeTdkLanjut); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('StatusKunj')); ?>:</b>
	<?php echo CHtml::encode($data->StatusKunj); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KetTdkL1')); ?>:</b>
	<?php echo CHtml::encode($data->KetTdkL1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KetTdkL2')); ?>:</b>
	<?php echo CHtml::encode($data->KetTdkL2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KetTdkL3')); ?>:</b>
	<?php echo CHtml::encode($data->KetTdkL3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KetTdkL4')); ?>:</b>
	<?php echo CHtml::encode($data->KetTdkL4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KetTdkL5')); ?>:</b>
	<?php echo CHtml::encode($data->KetTdkL5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoAntriPoli')); ?>:</b>
	<?php echo CHtml::encode($data->NoAntriPoli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PostMRS')); ?>:</b>
	<?php echo CHtml::encode($data->PostMRS); ?>
	<br />

	*/ ?>

</div>