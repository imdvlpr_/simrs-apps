<?php
/* @var $this DmLabKategoriController */
/* @var $model DmLabKategori */

$this->breadcrumbs=array(
	array('name'=>'Dm Lab Kategori','url'=>array('admin')),
	array('name'=>'Dm Lab Kategori'),
);

$this->menu=array(
	array('label'=>'List DmLabKategori', 'url'=>array('index')),
	array('label'=>'Create DmLabKategori', 'url'=>array('create')),
	array('label'=>'Update DmLabKategori', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmLabKategori', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmLabKategori', 'url'=>array('admin')),
);
?>

<h1>View DmLabKategori #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
