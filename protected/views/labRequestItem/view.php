<?php
/* @var $this LabRequestItemController */
/* @var $model LabRequestItem */

$this->breadcrumbs=array(
	array('name'=>'Lab Request Item','url'=>array('admin')),
	array('name'=>'Lab Request Item'),
);

$this->menu=array(
	array('label'=>'List LabRequestItem', 'url'=>array('index')),
	array('label'=>'Create LabRequestItem', 'url'=>array('create')),
	array('label'=>'Update LabRequestItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LabRequestItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LabRequestItem', 'url'=>array('admin')),
);
?>

<h1>View LabRequestItem #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'request_id',
		'item_id',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
