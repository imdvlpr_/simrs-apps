<?php
/* @var $this UnitController */
/* @var $model Unit */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'KodeUnit'); ?>
		<?php echo $form->textField($model,'KodeUnit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NamaUnit'); ?>
		<?php echo $form->textField($model,'NamaUnit',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit_tipe'); ?>
		<?php echo $form->textField($model,'unit_tipe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->