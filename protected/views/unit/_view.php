<?php
/* @var $this UnitController */
/* @var $data Unit */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeUnit')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->KodeUnit), array('view', 'id'=>$data->KodeUnit)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NamaUnit')); ?>:</b>
	<?php echo CHtml::encode($data->NamaUnit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit_tipe')); ?>:</b>
	<?php echo CHtml::encode($data->unit_tipe); ?>
	<br />


</div>