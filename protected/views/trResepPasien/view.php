<?php
/* @var $this TrResepPasienController */
/* @var $model TrResepPasien */

$this->breadcrumbs=array(
	array('name'=>'Tr Resep Pasien','url'=>array('admin')),
	array('name'=>'Tr Resep Pasien'),
);

$this->menu=array(
	array('label'=>'List TrResepPasien', 'url'=>array('index')),
	array('label'=>'Create TrResepPasien', 'url'=>array('create')),
	array('label'=>'Update TrResepPasien', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrResepPasien', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrResepPasien', 'url'=>array('admin')),
);
?>

<h1>View TrResepPasien #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nodaftar_id',
		'pasien_id',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
