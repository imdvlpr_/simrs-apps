<?php
/* @var $this MenuLayoutRbacController */
/* @var $model MenuLayoutRbac */

$this->breadcrumbs=array(
	array('name'=>'Menu Layout Rbac','url'=>array('admin')),
	array('name'=>'Menu Layout Rbac'),
);

$this->menu=array(
	array('label'=>'List MenuLayoutRbac', 'url'=>array('index')),
	array('label'=>'Create MenuLayoutRbac', 'url'=>array('create')),
	array('label'=>'Update MenuLayoutRbac', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MenuLayoutRbac', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MenuLayoutRbac', 'url'=>array('admin')),
);
?>

<h1>View MenuLayoutRbac #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'role_id',
		'menu_id',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
