<?php
/* @var $this DmOkGolOperasiController */
/* @var $model DmOkGolOperasi */

$this->breadcrumbs=array(
	array('name'=>'Dm Ok Gol Operasi','url'=>array('admin')),
	array('name'=>'Dm Ok Gol Operasi'),
);

$this->menu=array(
	array('label'=>'List DmOkGolOperasi', 'url'=>array('index')),
	array('label'=>'Create DmOkGolOperasi', 'url'=>array('create')),
	array('label'=>'Update DmOkGolOperasi', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmOkGolOperasi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmOkGolOperasi', 'url'=>array('admin')),
);
?>

<h1>View DmOkGolOperasi #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
	),
)); ?>
	</div>
</div>
