<?php
/* @var $this TindakanMedisLainController */
/* @var $data TindakanMedisLain */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tindakan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tindakan), array('view', 'id'=>$data->id_tindakan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_tindakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelas_id')); ?>:</b>
	<?php echo CHtml::encode($data->kelas_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bhp')); ?>:</b>
	<?php echo CHtml::encode($data->bhp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jrs')); ?>:</b>
	<?php echo CHtml::encode($data->jrs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('japel')); ?>:</b>
	<?php echo CHtml::encode($data->japel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tarip')); ?>:</b>
	<?php echo CHtml::encode($data->tarip); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('param')); ?>:</b>
	<?php echo CHtml::encode($data->param); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('catatan')); ?>:</b>
	<?php echo CHtml::encode($data->catatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>