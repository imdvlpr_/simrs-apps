<?php
/* @var $this TindakanMedisLainController */
/* @var $model TindakanMedisLain */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_tindakan'); ?>
		<?php echo $form->textField($model,'id_tindakan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_tindakan'); ?>
		<?php echo $form->textField($model,'nama_tindakan',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kelas_id'); ?>
		<?php echo $form->textField($model,'kelas_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bhp'); ?>
		<?php echo $form->textField($model,'bhp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jrs'); ?>
		<?php echo $form->textField($model,'jrs'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'japel'); ?>
		<?php echo $form->textField($model,'japel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tarip'); ?>
		<?php echo $form->textField($model,'tarip'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'param'); ?>
		<?php echo $form->textField($model,'param',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'catatan'); ?>
		<?php echo $form->textField($model,'catatan',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->