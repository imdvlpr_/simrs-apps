<?php
/* @var $this PegawaiController */
/* @var $model Pegawai */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pegawai-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'KDPEGAWAI'); ?>
		<?php echo $form->textField($model,'KDPEGAWAI',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'KDPEGAWAI'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NAMA'); ?>
		<?php echo $form->textField($model,'NAMA',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'NAMA'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ALAMAT'); ?>
		<?php echo $form->textField($model,'ALAMAT',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ALAMAT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KOTA'); ?>
		<?php echo $form->textField($model,'KOTA',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'KOTA'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'KODEPOS'); ?>
		<?php echo $form->textField($model,'KODEPOS',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'KODEPOS'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TELEPON'); ?>
		<?php echo $form->textField($model,'TELEPON',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'TELEPON'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PONSEL'); ?>
		<?php echo $form->textField($model,'PONSEL',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'PONSEL'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'EMAIL'); ?>
		<?php echo $form->textField($model,'EMAIL',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'EMAIL'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'STATUS'); ?>
		<?php echo $form->textField($model,'STATUS',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'STATUS'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'USERLOG'); ?>
		<?php echo $form->textField($model,'USERLOG',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'USERLOG'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PASSWD'); ?>
		<?php echo $form->passwordField($model,'PASSWD',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'PASSWD'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->