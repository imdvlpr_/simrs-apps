<?php
/* @var $this PegawaiController */
/* @var $data Pegawai */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('KDPEGAWAI')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->KDPEGAWAI), array('view', 'id'=>$data->KDPEGAWAI)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NAMA')); ?>:</b>
	<?php echo CHtml::encode($data->NAMA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ALAMAT')); ?>:</b>
	<?php echo CHtml::encode($data->ALAMAT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KOTA')); ?>:</b>
	<?php echo CHtml::encode($data->KOTA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KODEPOS')); ?>:</b>
	<?php echo CHtml::encode($data->KODEPOS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TELEPON')); ?>:</b>
	<?php echo CHtml::encode($data->TELEPON); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PONSEL')); ?>:</b>
	<?php echo CHtml::encode($data->PONSEL); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('EMAIL')); ?>:</b>
	<?php echo CHtml::encode($data->EMAIL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('STATUS')); ?>:</b>
	<?php echo CHtml::encode($data->STATUS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('USERLOG')); ?>:</b>
	<?php echo CHtml::encode($data->USERLOG); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PASSWD')); ?>:</b>
	<?php echo CHtml::encode($data->PASSWD); ?>
	<br />

	*/ ?>

</div>