<?php
/* @var $this DmKalenderController */
/* @var $model DmKalender */

$this->breadcrumbs=array(
	'Dm Kalenders'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DmKalender', 'url'=>array('index')),
	array('label'=>'Create DmKalender', 'url'=>array('create')),
	array('label'=>'View DmKalender', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DmKalender', 'url'=>array('admin')),
);
?>

<h1>Update DmKalender <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>