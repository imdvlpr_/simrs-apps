<?php
/* @var $this DmKalenderController */
/* @var $model DmKalender */

$this->breadcrumbs=array(
	'Dm Kalenders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DmKalender', 'url'=>array('index')),
	array('label'=>'Manage DmKalender', 'url'=>array('admin')),
);
?>

<h1>Create DmKalender</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>