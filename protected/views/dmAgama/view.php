<?php
/* @var $this DmAgamaController */
/* @var $model DmAgama */

$this->breadcrumbs=array(
	array('name'=>'Dm Agama','url'=>array('index')),
	array('name'=>'Dm Agama'),
);

$this->menu=array(
	array('label'=>'List DmAgama', 'url'=>array('index')),
	array('label'=>'Create DmAgama', 'url'=>array('create')),
	array('label'=>'Update DmAgama', 'url'=>array('update', 'id'=>$model->id_agama)),
	array('label'=>'Delete DmAgama', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_agama),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmAgama', 'url'=>array('admin')),
);
?>

<h1>View DmAgama #<?php echo $model->id_agama; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_agama',
		'nama_agama',
	),
)); ?>
	</div>
</div>
