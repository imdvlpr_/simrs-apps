 <?php
$this->breadcrumbs=array(
	array('name'=>'Tr Rawat Jalan Rujukan','url'=>array('admin')),
	array('name'=>'Update'),
);

?>



<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>
<div class="row">
	<div class="col-xs-12">
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>
