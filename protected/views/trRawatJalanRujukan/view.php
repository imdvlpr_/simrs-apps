<?php
/* @var $this TrRawatJalanRujukanController */
/* @var $model TrRawatJalanRujukan */

$this->breadcrumbs=array(
	array('name'=>'Tr Rawat Jalan Rujukan','url'=>array('admin')),
	array('name'=>'Tr Rawat Jalan Rujukan'),
);

$this->menu=array(
	array('label'=>'List TrRawatJalanRujukan', 'url'=>array('index')),
	array('label'=>'Create TrRawatJalanRujukan', 'url'=>array('create')),
	array('label'=>'Update TrRawatJalanRujukan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrRawatJalanRujukan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrRawatJalanRujukan', 'url'=>array('admin')),
);
?>

<h1>View TrRawatJalanRujukan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tr_rawat_jalan_id',
		'unit_id',
		'tanggal_rujukan',
		'is_approved',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
