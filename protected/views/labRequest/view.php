<?php
/* @var $this LabRequestController */
/* @var $model LabRequest */

$this->breadcrumbs=array(
	array('name'=>'Lab Request','url'=>array('admin')),
	array('name'=>'Lab Request'),
);

$this->menu=array(
	array('label'=>'List LabRequest', 'url'=>array('index')),
	array('label'=>'Create LabRequest', 'url'=>array('create')),
	array('label'=>'Update LabRequest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LabRequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LabRequest', 'url'=>array('admin')),
);
?>

<h1>View LabRequest #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'pasien_id',
		'unit_id',
		'reg_id',
		'tanggal',
		'status_pelayanan',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
