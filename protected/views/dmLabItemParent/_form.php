<?php
/* @var $this DmLabItemParentController */
/* @var $model DmLabItemParent */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dm-lab-item-parent-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'parent_id', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'parent_id'); ?>
		<?php echo $form->error($model,'parent_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'item_id', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'3')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'item_id'); ?>
		<?php echo $form->error($model,'item_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'batas_bawah', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'batas_bawah'); ?>
		<?php echo $form->error($model,'batas_bawah'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'batas_bawah_kritis', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'batas_bawah_kritis'); ?>
		<?php echo $form->error($model,'batas_bawah_kritis'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'batas_atas', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'6')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'batas_atas'); ?>
		<?php echo $form->error($model,'batas_atas'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'batas_atas_kritis', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'7')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'batas_atas_kritis'); ?>
		<?php echo $form->error($model,'batas_atas_kritis'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'batas_bawah_p', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'8')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'batas_bawah_p'); ?>
		<?php echo $form->error($model,'batas_bawah_p'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'batas_bawah_kritis_p', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'9')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'batas_bawah_kritis_p'); ?>
		<?php echo $form->error($model,'batas_bawah_kritis_p'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'batas_atas_p', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'10')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'batas_atas_p'); ?>
		<?php echo $form->error($model,'batas_atas_p'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'batas_atas_kritis_p', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'11')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'batas_atas_kritis_p'); ?>
		<?php echo $form->error($model,'batas_atas_kritis_p'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'metode', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'12')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'metode',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'metode'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'satuan', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'13')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'satuan',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'satuan'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
