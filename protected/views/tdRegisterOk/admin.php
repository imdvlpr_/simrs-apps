<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */

$this->breadcrumbs=array(
	'Td Register Oks'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TdRegisterOk', 'url'=>array('index')),
	array('label'=>'Create TdRegisterOk', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#td-register-ok-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Td Register Oks</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'td-register-ok-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_ok',
		'no_urut',
		'no_rm',
		'tgl_operasi',
		'jam_ok',
		'diagnosa',
		/*
		'tindakan',
		'jenis_operasi',
		'jenis_anastesi',
		'dr_operator',
		'dr_anastesi',
		'asisten_dokter1',
		'asisten_dokter2',
		'cito_elektif',
		'spesimen',
		'id_rawat_inap',
		'created',
		'status_ok',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
