<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */

$this->breadcrumbs=array(
	'Td Register Oks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TdRegisterOk', 'url'=>array('index')),
	array('label'=>'Manage TdRegisterOk', 'url'=>array('admin')),
);
?>

<h1>Create TdRegisterOk</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>