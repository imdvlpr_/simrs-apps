<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */

$this->breadcrumbs=array(
	'Td Register Oks'=>array('index'),
	$model->id_ok,
);

$this->menu=array(
	array('label'=>'List TdRegisterOk', 'url'=>array('index')),
	array('label'=>'Create TdRegisterOk', 'url'=>array('create')),
	array('label'=>'Update TdRegisterOk', 'url'=>array('update', 'id'=>$model->id_ok)),
	array('label'=>'Delete TdRegisterOk', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_ok),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TdRegisterOk', 'url'=>array('admin')),
);
?>

<h1>View TdRegisterOk #<?php echo $model->id_ok; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_ok',
		'no_urut',
		'no_rm',
		'tgl_operasi',
		'jam_ok',
		'diagnosa',
		'tindakan',
		'jenis_operasi',
		'jenis_anastesi',
		'dr_operator',
		'dr_anastesi',
		'asisten_dokter1',
		'asisten_dokter2',
		'cito_elektif',
		'spesimen',
		'id_rawat_inap',
		'created',
		'status_ok',
	),
)); ?>
