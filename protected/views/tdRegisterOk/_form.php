<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'td-register-ok-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'no_urut'); ?>
		<?php echo $form->textField($model,'no_urut'); ?>
		<?php echo $form->error($model,'no_urut'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_rm'); ?>
		<?php echo $form->textField($model,'no_rm'); ?>
		<?php echo $form->error($model,'no_rm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_operasi'); ?>
		<?php echo $form->textField($model,'tgl_operasi'); ?>
		<?php echo $form->error($model,'tgl_operasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jam_ok'); ?>
		<?php echo $form->textField($model,'jam_ok'); ?>
		<?php echo $form->error($model,'jam_ok'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'diagnosa'); ?>
		<?php echo $form->textField($model,'diagnosa',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'diagnosa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tindakan'); ?>
		<?php echo $form->textField($model,'tindakan',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'tindakan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_operasi'); ?>
		<?php echo $form->textField($model,'jenis_operasi',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'jenis_operasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_anastesi'); ?>
		<?php echo $form->textField($model,'jenis_anastesi',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'jenis_anastesi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dr_operator'); ?>
		<?php echo $form->textField($model,'dr_operator'); ?>
		<?php echo $form->error($model,'dr_operator'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dr_anastesi'); ?>
		<?php echo $form->textField($model,'dr_anastesi'); ?>
		<?php echo $form->error($model,'dr_anastesi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'asisten_dokter1'); ?>
		<?php echo $form->textField($model,'asisten_dokter1',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'asisten_dokter1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'asisten_dokter2'); ?>
		<?php echo $form->textField($model,'asisten_dokter2',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'asisten_dokter2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cito_elektif'); ?>
		<?php echo $form->textField($model,'cito_elektif',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'cito_elektif'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'spesimen'); ?>
		<?php echo $form->textField($model,'spesimen',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'spesimen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_rawat_inap'); ?>
		<?php echo $form->textField($model,'id_rawat_inap'); ?>
		<?php echo $form->error($model,'id_rawat_inap'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status_ok'); ?>
		<?php echo $form->textField($model,'status_ok'); ?>
		<?php echo $form->error($model,'status_ok'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->