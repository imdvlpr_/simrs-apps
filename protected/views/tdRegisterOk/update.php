<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */

$this->breadcrumbs=array(
	'Td Register Oks'=>array('index'),
	$model->id_ok=>array('view','id'=>$model->id_ok),
	'Update',
);

$this->menu=array(
	array('label'=>'List TdRegisterOk', 'url'=>array('index')),
	array('label'=>'Create TdRegisterOk', 'url'=>array('create')),
	array('label'=>'View TdRegisterOk', 'url'=>array('view', 'id'=>$model->id_ok)),
	array('label'=>'Manage TdRegisterOk', 'url'=>array('admin')),
);
?>

<h1>Update TdRegisterOk <?php echo $model->id_ok; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>