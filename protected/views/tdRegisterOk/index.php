<?php
/* @var $this TdRegisterOkController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Td Register Oks',
);

$this->menu=array(
	array('label'=>'Create TdRegisterOk', 'url'=>array('create')),
	array('label'=>'Manage TdRegisterOk', 'url'=>array('admin')),
);
?>

<h1>Td Register Oks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
