<?php
/* @var $this TrackingRmController */
/* @var $model TrackingRm */

$this->breadcrumbs=array(
	array('name'=>'Tracking Rm','url'=>array('admin')),
	array('name'=>'Tracking Rm'),
);

$this->menu=array(
	array('label'=>'List TrackingRm', 'url'=>array('index')),
	array('label'=>'Create TrackingRm', 'url'=>array('create')),
	array('label'=>'Update TrackingRm', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrackingRm', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrackingRm', 'url'=>array('admin')),
);
?>

<h1>View TrackingRm #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'b_pendaftaran_id',
		'status_rm_id',
		'unit_penerima',
		'keterangan',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
