<?php
/* @var $this TindakanMedisPenunjangController */
/* @var $model TindakanMedisPenunjang */

$this->breadcrumbs=array(
	array('name'=>'Tindakan Medis Penunjang','url'=>array('admin')),
	array('name'=>'Tindakan Medis Penunjang'),
);

$this->menu=array(
	array('label'=>'List TindakanMedisPenunjang', 'url'=>array('index')),
	array('label'=>'Create TindakanMedisPenunjang', 'url'=>array('create')),
	array('label'=>'Update TindakanMedisPenunjang', 'url'=>array('update', 'id'=>$model->id_tindakan_penunjang)),
	array('label'=>'Delete TindakanMedisPenunjang', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tindakan_penunjang),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TindakanMedisPenunjang', 'url'=>array('admin')),
);
?>

<h1>View TindakanMedisPenunjang #<?php echo $model->id_tindakan_penunjang; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tindakan_penunjang',
		'nama_tindakan',
		'kelas_id',
		'bhp',
		'jrs',
		'japel',
		'tarip',
		'param',
		'catatan',
		'created',
		'jenis_penunjang',
	),
)); ?>
	</div>
</div>
