<?php
/* @var $this TdPasienMasukController */
/* @var $model TdPasienMasuk */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Kode_Detail'); ?>
		<?php echo $form->textField($model,'Kode_Detail',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Kode_TMPM'); ?>
		<?php echo $form->textField($model,'Kode_TMPM',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'No_RM'); ?>
		<?php echo $form->textField($model,'No_RM',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Nama_pasien'); ?>
		<?php echo $form->textField($model,'Nama_pasien',array('size'=>60,'maxlength'=>220)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Alamat'); ?>
		<?php echo $form->textField($model,'Alamat',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Ruang'); ?>
		<?php echo $form->textField($model,'Ruang',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bed'); ?>
		<?php echo $form->textField($model,'bed',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Dokter'); ?>
		<?php echo $form->textField($model,'Dokter',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Kelas'); ?>
		<?php echo $form->textField($model,'Kelas',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UPF'); ?>
		<?php echo $form->textField($model,'UPF',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Jenis_Pasien'); ?>
		<?php echo $form->textField($model,'Jenis_Pasien',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Tgl_MRS'); ?>
		<?php echo $form->textField($model,'Tgl_MRS'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'waktu_daftar'); ?>
		<?php echo $form->textField($model,'waktu_daftar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Tgl_KRS'); ?>
		<?php echo $form->textField($model,'Tgl_KRS'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Status_Pasien'); ?>
		<?php echo $form->textField($model,'Status_Pasien',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Ket_Pulang'); ?>
		<?php echo $form->textField($model,'Ket_Pulang',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Ket_Masuk'); ?>
		<?php echo $form->textField($model,'Ket_Masuk',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Jenis_kelamin'); ?>
		<?php echo $form->textField($model,'Jenis_kelamin',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Lama_Dirawat'); ?>
		<?php echo $form->textField($model,'Lama_Dirawat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Hari_Perawatan'); ?>
		<?php echo $form->textField($model,'Hari_Perawatan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Tgl_Setor'); ?>
		<?php echo $form->textField($model,'Tgl_Setor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Status_Berkas'); ?>
		<?php echo $form->textField($model,'Status_Berkas',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'umur'); ?>
		<?php echo $form->textField($model,'umur',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rjri'); ?>
		<?php echo $form->textField($model,'rjri',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_kunj_trakhir'); ?>
		<?php echo $form->textField($model,'tgl_kunj_trakhir'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'post_mrs'); ?>
		<?php echo $form->textField($model,'post_mrs',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status_bpjs'); ?>
		<?php echo $form->textField($model,'status_bpjs',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_pindah'); ?>
		<?php echo $form->textField($model,'tgl_pindah'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kelas_kemkes'); ?>
		<?php echo $form->textField($model,'kelas_kemkes',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kelas_bpjs'); ?>
		<?php echo $form->textField($model,'kelas_bpjs',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ruang_kemkes'); ?>
		<?php echo $form->textField($model,'ruang_kemkes',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'antrian'); ?>
		<?php echo $form->textField($model,'antrian',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pindahan_titipan'); ?>
		<?php echo $form->textField($model,'pindahan_titipan',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kelas_pasien'); ?>
		<?php echo $form->textField($model,'kelas_pasien',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jenis_klaim'); ?>
		<?php echo $form->textField($model,'jenis_klaim',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'diagnosa'); ?>
		<?php echo $form->textField($model,'diagnosa',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->