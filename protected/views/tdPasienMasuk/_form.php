<?php
/* @var $this TdPasienMasukController */
/* @var $model TdPasienMasuk */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'td-pasien-masuk-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Kode_Detail', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'1')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Kode_Detail',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Kode_Detail'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Kode_TMPM', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Kode_TMPM',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Kode_TMPM'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'No_RM', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'3')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'No_RM',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'No_RM'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Nama_pasien', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Nama_pasien',array('size'=>60,'maxlength'=>220)); ?>
		<?php echo $form->error($model,'Nama_pasien'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Alamat', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Alamat',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'Alamat'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Ruang', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'6')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Ruang',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Ruang'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bed', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'7')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'bed',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'bed'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Dokter', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'8')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Dokter',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'Dokter'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Kelas', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'9')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Kelas',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Kelas'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'UPF', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'10')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'UPF',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'UPF'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Jenis_Pasien', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'11')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Jenis_Pasien',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Jenis_Pasien'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Tgl_MRS', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'12')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Tgl_MRS'); ?>
		<?php echo $form->error($model,'Tgl_MRS'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'waktu_daftar', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'13')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'waktu_daftar'); ?>
		<?php echo $form->error($model,'waktu_daftar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Tgl_KRS', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'14')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Tgl_KRS'); ?>
		<?php echo $form->error($model,'Tgl_KRS'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Status_Pasien', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'15')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Status_Pasien',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'Status_Pasien'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Ket_Pulang', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'16')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Ket_Pulang',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Ket_Pulang'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Ket_Masuk', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'17')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Ket_Masuk',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Ket_Masuk'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Jenis_kelamin', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'18')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Jenis_kelamin',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Jenis_kelamin'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Lama_Dirawat', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'19')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Lama_Dirawat'); ?>
		<?php echo $form->error($model,'Lama_Dirawat'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Hari_Perawatan', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'20')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Hari_Perawatan'); ?>
		<?php echo $form->error($model,'Hari_Perawatan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Tgl_Setor', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'21')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Tgl_Setor'); ?>
		<?php echo $form->error($model,'Tgl_Setor'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Status_Berkas', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'22')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'Status_Berkas',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'Status_Berkas'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'umur', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'23')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'umur',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'umur'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'rjri', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'24')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'rjri',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'rjri'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tgl_kunj_trakhir', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'25')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tgl_kunj_trakhir'); ?>
		<?php echo $form->error($model,'tgl_kunj_trakhir'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'post_mrs', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'26')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'post_mrs',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'post_mrs'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status_bpjs', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'27')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'status_bpjs',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'status_bpjs'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tgl_pindah', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'28')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tgl_pindah'); ?>
		<?php echo $form->error($model,'tgl_pindah'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_kemkes', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'29')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kelas_kemkes',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'kelas_kemkes'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_bpjs', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'30')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kelas_bpjs',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'kelas_bpjs'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'ruang_kemkes', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'31')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'ruang_kemkes',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ruang_kemkes'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'antrian', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'32')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'antrian',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'antrian'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pindahan_titipan', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'33')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'pindahan_titipan',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'pindahan_titipan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_pasien', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'34')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kelas_pasien',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'kelas_pasien'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenis_klaim', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'35')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jenis_klaim',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'jenis_klaim'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'diagnosa', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'36')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'diagnosa',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'diagnosa'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
