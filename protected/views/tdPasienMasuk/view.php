<?php
/* @var $this TdPasienMasukController */
/* @var $model TdPasienMasuk */

$this->breadcrumbs=array(
	array('name'=>'Td Pasien Masuk','url'=>array('admin')),
	array('name'=>'Td Pasien Masuk'),
);

$this->menu=array(
	array('label'=>'List TdPasienMasuk', 'url'=>array('index')),
	array('label'=>'Create TdPasienMasuk', 'url'=>array('create')),
	array('label'=>'Update TdPasienMasuk', 'url'=>array('update', 'id'=>$model->Kode_Detail)),
	array('label'=>'Delete TdPasienMasuk', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Kode_Detail),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TdPasienMasuk', 'url'=>array('admin')),
);
?>

<h1>View TdPasienMasuk #<?php echo $model->Kode_Detail; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Kode_Detail',
		'Kode_TMPM',
		'No_RM',
		'Nama_pasien',
		'Alamat',
		'Ruang',
		'bed',
		'Dokter',
		'Kelas',
		'UPF',
		'Jenis_Pasien',
		'Tgl_MRS',
		'waktu_daftar',
		'Tgl_KRS',
		'Status_Pasien',
		'Ket_Pulang',
		'Ket_Masuk',
		'Jenis_kelamin',
		'Lama_Dirawat',
		'Hari_Perawatan',
		'Tgl_Setor',
		'Status_Berkas',
		'umur',
		'rjri',
		'tgl_kunj_trakhir',
		'post_mrs',
		'status_bpjs',
		'tgl_pindah',
		'kelas_kemkes',
		'kelas_bpjs',
		'ruang_kemkes',
		'antrian',
		'pindahan_titipan',
		'kelas_pasien',
		'jenis_klaim',
		'diagnosa',
	),
)); ?>
	</div>
</div>
