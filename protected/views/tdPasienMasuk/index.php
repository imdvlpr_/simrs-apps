 <?php
$this->breadcrumbs=array(
	array('name'=>'Td Pasien Masuk','url'=>array('index')),
	array('name'=>'Manage'),
);

?>
<h1>Manage Td Pasien Masuk</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#td-pasien-masuk-grid').yiiGridView.update('td-pasien-masuk-grid', {
			    url:'<?php echo Yii::app()->createUrl("TdPasienMasuk/index"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                        <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('TdPasienMasuk/create'),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('TdPasienMasuk[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('TdPasienMasuk[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'td-pasien-masuk-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		'Kode_Detail',
		'Kode_TMPM',
		'No_RM',
		'Nama_pasien',
		'Alamat',
		'Ruang',
		/*
		'bed',
		'Dokter',
		'Kelas',
		'UPF',
		'Jenis_Pasien',
		'Tgl_MRS',
		'waktu_daftar',
		'Tgl_KRS',
		'Status_Pasien',
		'Ket_Pulang',
		'Ket_Masuk',
		'Jenis_kelamin',
		'Lama_Dirawat',
		'Hari_Perawatan',
		'Tgl_Setor',
		'Status_Berkas',
		'umur',
		'rjri',
		'tgl_kunj_trakhir',
		'post_mrs',
		'status_bpjs',
		'tgl_pindah',
		'kelas_kemkes',
		'kelas_bpjs',
		'ruang_kemkes',
		'antrian',
		'pindahan_titipan',
		'kelas_pasien',
		'jenis_klaim',
		'diagnosa',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

