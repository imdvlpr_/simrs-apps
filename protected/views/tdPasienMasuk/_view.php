<?php
/* @var $this TdPasienMasukController */
/* @var $data TdPasienMasuk */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Kode_Detail')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Kode_Detail), array('view', 'id'=>$data->Kode_Detail)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Kode_TMPM')); ?>:</b>
	<?php echo CHtml::encode($data->Kode_TMPM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('No_RM')); ?>:</b>
	<?php echo CHtml::encode($data->No_RM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nama_pasien')); ?>:</b>
	<?php echo CHtml::encode($data->Nama_pasien); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Alamat')); ?>:</b>
	<?php echo CHtml::encode($data->Alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ruang')); ?>:</b>
	<?php echo CHtml::encode($data->Ruang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bed')); ?>:</b>
	<?php echo CHtml::encode($data->bed); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Dokter')); ?>:</b>
	<?php echo CHtml::encode($data->Dokter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Kelas')); ?>:</b>
	<?php echo CHtml::encode($data->Kelas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UPF')); ?>:</b>
	<?php echo CHtml::encode($data->UPF); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Jenis_Pasien')); ?>:</b>
	<?php echo CHtml::encode($data->Jenis_Pasien); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Tgl_MRS')); ?>:</b>
	<?php echo CHtml::encode($data->Tgl_MRS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu_daftar')); ?>:</b>
	<?php echo CHtml::encode($data->waktu_daftar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Tgl_KRS')); ?>:</b>
	<?php echo CHtml::encode($data->Tgl_KRS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status_Pasien')); ?>:</b>
	<?php echo CHtml::encode($data->Status_Pasien); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ket_Pulang')); ?>:</b>
	<?php echo CHtml::encode($data->Ket_Pulang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ket_Masuk')); ?>:</b>
	<?php echo CHtml::encode($data->Ket_Masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Jenis_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->Jenis_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Lama_Dirawat')); ?>:</b>
	<?php echo CHtml::encode($data->Lama_Dirawat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Hari_Perawatan')); ?>:</b>
	<?php echo CHtml::encode($data->Hari_Perawatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Tgl_Setor')); ?>:</b>
	<?php echo CHtml::encode($data->Tgl_Setor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status_Berkas')); ?>:</b>
	<?php echo CHtml::encode($data->Status_Berkas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('umur')); ?>:</b>
	<?php echo CHtml::encode($data->umur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rjri')); ?>:</b>
	<?php echo CHtml::encode($data->rjri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_kunj_trakhir')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_kunj_trakhir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_mrs')); ?>:</b>
	<?php echo CHtml::encode($data->post_mrs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_bpjs')); ?>:</b>
	<?php echo CHtml::encode($data->status_bpjs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_pindah')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_pindah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelas_kemkes')); ?>:</b>
	<?php echo CHtml::encode($data->kelas_kemkes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelas_bpjs')); ?>:</b>
	<?php echo CHtml::encode($data->kelas_bpjs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruang_kemkes')); ?>:</b>
	<?php echo CHtml::encode($data->ruang_kemkes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('antrian')); ?>:</b>
	<?php echo CHtml::encode($data->antrian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pindahan_titipan')); ?>:</b>
	<?php echo CHtml::encode($data->pindahan_titipan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelas_pasien')); ?>:</b>
	<?php echo CHtml::encode($data->kelas_pasien); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_klaim')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_klaim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('diagnosa')); ?>:</b>
	<?php echo CHtml::encode($data->diagnosa); ?>
	<br />

	*/ ?>

</div>