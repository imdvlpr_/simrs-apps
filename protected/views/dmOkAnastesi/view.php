<?php
/* @var $this DmOkAnastesiController */
/* @var $model DmOkAnastesi */

$this->breadcrumbs=array(
	array('name'=>'Dm Ok Anastesi','url'=>array('admin')),
	array('name'=>'Dm Ok Anastesi'),
);

$this->menu=array(
	array('label'=>'List DmOkAnastesi', 'url'=>array('index')),
	array('label'=>'Create DmOkAnastesi', 'url'=>array('create')),
	array('label'=>'Update DmOkAnastesi', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmOkAnastesi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmOkAnastesi', 'url'=>array('admin')),
);
?>

<h1>View DmOkAnastesi #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'kode',
	),
)); ?>
	</div>
</div>
