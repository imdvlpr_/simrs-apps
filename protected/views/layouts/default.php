<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<style>
		hr {
		    display: block;
		    height: 7px;
		    border: 0;
		    border-top: 1px solid #ccc;
		   
		}

		div#header-text{
			padding-top:20px;
			padding-bottom:20px;
			background-color:rgb(85,142,213);
		}
	</style>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<h1>
			<div id="header-text" align="center"><font color="white">Selamat datang di program Sistem Informasi Manajemen RSUD Kab Kediri</font></div>
	</h1>
		<div id="logo">

			<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/header.png', Yii::app()->name);?>
	</div><!-- header -->

	<hr/>
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<?php echo CHtml::link('Beranda',Yii::app()->createUrl('site/index'));?>
		<br>
		Copyright &copy; 2015 - <?php echo date('Y'); ?> by EurekaTour<br/>
		All Rights Reserved.<br/>
		
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
