<?php
/* @var $this BPendaftaranDiagnosisController */
/* @var $model BPendaftaranDiagnosis */

$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Diagnosis','url'=>array('admin')),
	array('name'=>'Bpendaftaran Diagnosis'),
);

$this->menu=array(
	array('label'=>'List BPendaftaranDiagnosis', 'url'=>array('index')),
	array('label'=>'Create BPendaftaranDiagnosis', 'url'=>array('create')),
	array('label'=>'Update BPendaftaranDiagnosis', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BPendaftaranDiagnosis', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BPendaftaranDiagnosis', 'url'=>array('admin')),
);
?>

<h1>View BPendaftaranDiagnosis #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'reg_id',
		'diagnosis',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
