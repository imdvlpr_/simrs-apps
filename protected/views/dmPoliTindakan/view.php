<?php
/* @var $this DmPoliTindakanController */
/* @var $model DmPoliTindakan */

$this->breadcrumbs=array(
	array('name'=>'Dm Poli Tindakan','url'=>array('admin')),
	array('name'=>'Dm Poli Tindakan'),
);

$this->menu=array(
	array('label'=>'List DmPoliTindakan', 'url'=>array('index')),
	array('label'=>'Create DmPoliTindakan', 'url'=>array('create')),
	array('label'=>'Update DmPoliTindakan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmPoliTindakan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmPoliTindakan', 'url'=>array('admin')),
);
?>

<h1>View DmPoliTindakan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'unit_id',
		'dm_tindakan_id',
		'biaya',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
