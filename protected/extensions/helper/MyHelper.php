<?php 

class MyHelper extends CApplicationComponent
{

	function dmYtoYmd($tgl){
		$date = str_replace('/', '-', $tgl);
	    return date('Y-m-d',strtotime($date));
	}

	function YmdtodmY($tgl){
		return date('Y-m-d',strtotime($tgl));
	}

	function syncBankRM(){
		$sql = 'CALL proc_sync_bankrm_dipakai';
		Yii::app()->db->createCommand($sql)->execute();
	}

	function hitungDurasiMenit($date1, $date2){
		$date1 = new DateTime($date1);
		$date2 = new DateTime($date2);
		$interval = $date1->diff($date2);

		$elapsed = '';
		if($interval->d > 0)
			$elapsed = $interval->d * $interval->h * 60 + $interval->i;			
		else if($interval->h > 0)
			$elapsed = $interval->h * 60 + $interval->i;
		else
			$elapsed = $interval->i;
			
		return $elapsed;
	}

	function hitungDurasi($date1, $date2){
		$date1 = new DateTime($date1);
		$date2 = new DateTime($date2);
		$interval = $date1->diff($date2);

		$elapsed = '';
		if($interval->d > 0)
			$elapsed = $interval->format('%a hari %h jam %i menit %s detik');
		else if($interval->h > 0)
			$elapsed = $interval->format('%h jam %i menit %s detik');
		else
			$elapsed = $interval->format('%i menit %s detik');
		

		return $elapsed;
	}

	function hitungUmur($tgl_lahir)
	{
		$tanggal = new DateTime($tgl_lahir);

// tanggal hari ini
		$today = new DateTime('today');

		// tahun
		$y = $today->diff($tanggal)->y;

		// bulan
		$m = $today->diff($tanggal)->m;

		// hari
		$d = $today->diff($tanggal)->d;
		return $y . " tahun ";
	}

	function hitungUmurFull($tgl_lahir)
	{
		$tanggal = new DateTime($tgl_lahir);

// tanggal hari ini
		$today = new DateTime('today');

		// tahun
		$y = $today->diff($tanggal)->y;

		// bulan
		$m = $today->diff($tanggal)->m;

		// hari
		$d = $today->diff($tanggal)->d;
		return ['y'=>$y,'m'=>$m,'d'=>$d];
	}

	function contains($haystack, $needle)
	{
		return strpos($haystack, $needle) !== false;
	}

	function getKunjunganTerakhir($rm)
	{
		// $model = Kunjungan::model()->findAllByAttributes(array(
		// 	'NO_RM'=>$rm),array(
		// 	'order' => 'NO_KUNJUNGAN DESC',
		// 	'limit' => 2
		// ));

		$k_last = Yii::app()->db->createCommand()
		    ->select('TGLDAFTAR')
		    ->from('b_pendaftaran t')
		    ->order('NODAFTAR DESC')
		    ->where('NoMedrec=:p1',array(':p1'=>$rm))
		    ->limit(2)
		    ->queryAll();

		$data = array();

		$model = (object)$k_last;

		foreach($model as $m){
			$data[] = $m;
		}

		$hasil = null;

		$size = count($data);
		switch($size){
			case 1 :
				$hasil = $data[0];
				break;
			case 2 :
				$hasil = $data[1];
				break;
		}

		return (object)$hasil;
	}

	function terbilang($bilangan) {

	  $angka = array('0','0','0','0','0','0','0','0','0','0',
	                 '0','0','0','0','0','0');
	  $kata = array('','satu','dua','tiga','empat','lima',
	                'enam','tujuh','delapan','sembilan');
	  $tingkat = array('','ribu','juta','milyar','triliun');

	  $panjang_bilangan = strlen($bilangan);

	  /* pengujian panjang bilangan */
	  if ($panjang_bilangan > 15) {
	    $kalimat = "Diluar Batas";
	    return $kalimat;
	  }

	  /* mengambil angka-angka yang ada dalam bilangan,
	     dimasukkan ke dalam array */
	  for ($i = 1; $i <= $panjang_bilangan; $i++) {
	    $angka[$i] = substr($bilangan,-($i),1);
	  }

	  $i = 1;
	  $j = 0;
	  $kalimat = "";


	  /* mulai proses iterasi terhadap array angka */
	  while ($i <= $panjang_bilangan) {

	    $subkalimat = "";
	    $kata1 = "";
	    $kata2 = "";
	    $kata3 = "";

	    /* untuk ratusan */
	    if ($angka[$i+2] != "0") {
	      if ($angka[$i+2] == "1") {
	        $kata1 = "seratus";
	      } else {
	        $kata1 = $kata[$angka[$i+2]] . " ratus";
	      }
	    }

	    /* untuk puluhan atau belasan */
	    if ($angka[$i+1] != "0") {
	      if ($angka[$i+1] == "1") {
	        if ($angka[$i] == "0") {
	          $kata2 = "sepuluh";
	        } elseif ($angka[$i] == "1") {
	          $kata2 = "sebelas";
	        } else {
	          $kata2 = $kata[$angka[$i]] . " belas";
	        }
	      } else {
	        $kata2 = $kata[$angka[$i+1]] . " puluh";
	      }
	    }

	    /* untuk satuan */
	    if ($angka[$i] != "0") {
	      if ($angka[$i+1] != "1") {
	        $kata3 = $kata[$angka[$i]];
	      }
	    }

	    /* pengujian angka apakah tidak nol semua,
	       lalu ditambahkan tingkat */
	    if (($angka[$i] != "0") OR ($angka[$i+1] != "0") OR
	        ($angka[$i+2] != "0")) {
	      $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
	    }

	    /* gabungkan variabe sub kalimat (untuk satu blok 3 angka)
	       ke variabel kalimat */
	    $kalimat = $subkalimat . $kalimat;
	    $i = $i + 3;
	    $j = $j + 1;

	  }

	  /* mengganti satu ribu jadi seribu jika diperlukan */
	  if (($angka[5] == "0") AND ($angka[6] == "0")) {
	    $kalimat = str_replace("satu ribu","seribu",$kalimat);
	  }

	  return trim($kalimat);

	} 

	function convertIDDate($date)
	{
		$date = str_replace('-', '/', $date);
				

		return date('d-m-Y', strtotime($date));
	}

	function formatBulat($val,$comma = 0)
	{
		return round($val);
	}

	function formatRupiah($val,$comma = 0, $xls='non_xls')
	{
		if($xls == 'xls')
			return round($val);
		else
			return number_format($val,$comma,',','.');
	}

	function convertSQLDate($date)
	{
		$date = str_replace('/', '-', $date);
				

		return date('Y-m-d', strtotime($date));
	}

	function getSelisihHari($old, $new)
	{
		$date1 = new DateTime($old);
		$date2 = new DateTime($new);
		$interval = $date1->diff($date2);
		return $interval->d ; 

		// shows the total amount of days (not divided into years, months and days like above)
		//echo "difference " . $interval->days . " days ";
	}

	function getSelisihHariInap($old, $new)
	{
		$date1 = strtotime($old);
		$date2 = strtotime($new);
		$interval = $date2 - $date1;
		return round($interval / (60 * 60 * 24)) + 1; 

		// shows the total amount of days (not divided into years, months and days like above)
		//echo "difference " . $interval->days . " days ";
	}

	function appendZeros($str, $charlength=6)
	{

		return str_pad($str, $charlength, '0', STR_PAD_LEFT);
	}
	
	function getSelisihTanggal($old, $new)
	{
		$date1 = new DateTime($old);
		$date2 = new DateTime($new);
		$interval = $date1->diff($date2);
		return $interval->y. " tahun " . $interval->m." bulan ".$interval->d." hari"; 

		// shows the total amount of days (not divided into years, months and days like above)
		//echo "difference " . $interval->days . " days ";
	}

	function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	
	function generateUniqueCode()
	{
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$string = '';
		 for ($i = 0; $i < 10; $i++) {
			  $string .= $characters[rand(0, strlen($characters) - 1)];
		 }
		
		return strtoupper($string);
	}

	function generateNoDaftar()
	{
		$characters = '0123456789';
		$string = '';
		 for ($i = 0; $i < 6; $i++) {
			  $string .= $characters[rand(0, strlen($characters) - 1)];
		 }
		
		return strtoupper($string);
	}

	function generateNoPegawai()
	{
		$characters = '0123456789';
		$string = '';
		 for ($i = 0; $i < 6; $i++) {
			  $string .= $characters[rand(0, strlen($characters) - 1)];
		 }
		
		return 'PEG'.$this->appendZeros(strtoupper($string),17);
	}

	function getKodeRawat()
	{
		$sql = 'SELECT kode_rawat FROM tr_rawat_inap ORDER BY kode_rawat DESC LIMIT 1';
		$medrec = Yii::app()->db->createCommand($sql)->queryAll();

		$hasil = 0;

		if(!empty($medrec))
			$hasil = $medrec[0]['kode_rawat'];

		return $hasil+1;

	}
	
	function getMinimumUnusedID()
	{
		$setting = Setting::model()->findByPk('norm');

		$sql = 'SELECT banknorm  FROM a_pasien_bankrm WHERE dipakai = 0 AND banknorm > '.$setting->option_value;
		$medrec = Yii::app()->db->createCommand($sql)->queryRow();

		$hasil = $medrec['banknorm'];

	// 	$sql = 'SELECT MIN(NoMedrec + 1) as nextID FROM a_pasien t
 // WHERE NOT EXISTS (SELECT NoMedrec FROM a_pasien WHERE NoMedrec = t.NoMedrec + 1)';
	// 	$medrec = Yii::app()->db->createCommand($sql)->queryAll();


	// 	$hasil = 1;

	// 	if (count($medrec[0]['nextID']) > 0)
	// 		$hasil = $medrec[0]['nextID'];


		
		return $this->appendZeros($hasil);

	}
}

?>