<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebUser
 *
 * @author andy
 */
class WebUser extends CWebUser {
    const R_SA = '1';
    const R_OP_DAFTAR = '2';
    const R_OP_RM = '3';
    const R_OP_KASIR = '4';
    const R_OP_KAMAR = '5';
    const R_OP_KEUANGAN = '6';
    const R_OP_FARMASI = '7';
    const R_OP_RADIOLOGI = '8';
    const R_OP_OPERASI = '9';
    const R_OP_IGD = '10';
    const R_KA_KASIR = '11';
    const R_OP_POLI = '12';
    const R_DIREKTUR = '13';
    const R_OP_LAB = '14';
    const R_OP_RR = '15';
    
    /**
     * this method is used for checking user right access
     * @param <string> $operation
     * @param <array> $params
     * @return <boolean>
     */
    public function checkAccess($operation, $params=array(), $allowCaching = true) {
        /*if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }*/
        $role = $this->getState("level");
		if(is_array($operation)){
			return in_array($role,$operation);
		}
		else{
			return ($operation === $role);
		}
    }
}
?>
