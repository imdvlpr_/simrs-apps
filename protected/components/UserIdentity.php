<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

	const ERROR_USER_INACTIVE = 3;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = User::model()->findByPk($this->username);

		if(is_null($user)){
			$this->errorCode=self::ERROR_USERNAME_INVALID;

		}
		else if($user->PASSWORD!==$this->password)
		{
			
			$this->errorCode=self::ERROR_PASSWORD_INVALID;

		}
		else if($user->STATUS != 1){

			$this->errorCode=self::ERROR_USER_INACTIVE;
			
		}
		else{

			$this->errorCode=self::ERROR_NONE;
			$this->setState('isLogin',true);
			$this->setState('username', $user->USERNAME);
			$this->setState('level', $user->LEVEL);
			$this->setState('pegawai_id',$user->PEGAWAI_ID);
			$this->setState('nama',$user->pEGAWAI->NAMA);
			$this->setState('userSessionTimeout', time() + Yii::app()->params['sessionTimeoutSeconds']);
			$this->setState('unit','');
			$this->setState('unitNama','');
			$this->setState('euid',$user->euid);
			
			if($user->LEVEL == WebUser::R_OP_POLI || $user->LEVEL == WebUser::R_OP_RM || $user->LEVEL == WebUser::R_OP_LAB)
			{

				$unit = UnitUser::model()->findByAttributes(['user_id'=>$user->USERNAME]);
				
				if(!empty($unit)){
					$this->setState('unit',$unit->unit_id);
					$this->setState('unitNama',$unit->unit->NamaUnit);
				}
			}

			else if($user->LEVEL == WebUser::R_OP_KAMAR)
			{
				$unit = DmKamar::model()->findByAttributes(['user_kamar'=>$user->USERNAME]);
				$this->setState('kamar_master_id',$unit->kamar_master_id);
				
				if(!empty($unit)){
					$this->setState('unit',$unit->kamarMaster->unit_id);
					$this->setState('unitNama',$unit->kamarMaster->nama_kamar);
				}
			}
		}


		return !$this->errorCode;
	}
}