<?php
class MyActiveRecord extends CActiveRecord
{

	private static $dbErp = null;
	public $log_key;


	public function afterSave(){
		
		return parent::afterSave();
	}
	
	public function afterDelete(){
		
		return parent::afterDelete();
	}



    protected static function getErpDbConnection()
    {
        if (self::$dbErp !== null)
            return self::$dbErp;
        else
        {
            self::$dbErp = Yii::app()->dbErp;
            if (self::$dbErp instanceof CDbConnection)
            {
                self::$dbErp->setActive(true);
                return self::$dbErp;
            }
            else
                throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
        }
    }
}
?>