<?php
class BreadCrumb extends CWidget {
 
    public $links = array();
    public $delimiter = ' ';
 
    public function run() {
        $this->render('breadCrumb');
    }
 
}
?>