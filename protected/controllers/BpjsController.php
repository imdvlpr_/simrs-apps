<?php

class BpjsController extends Controller
{

	


	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('baru','lama','sep','updateSep','sepPopup','printSep'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionPrintSep($id)
	{
		$model = BpjsSep::model()->findByPk($id);
		$bpjsPasien = BpjsPasien::model()->findByAttributes(array('PASIEN_ID'=>$model->NO_RM));
		$kelasoptions = array(
        		'1' => 'Kelas I',
        		'2' => 'Kelas II',
        		'3' => 'Kelas III'
        	);

		$jenisRawat = '';
		switch ($model->JENIS_RAWAT) {
			case 0:
				$jenisRawat = 'IGD';
				break;
			case 1:
				$jenisRawat = 'Rawat Inap';
				break;
			case 2:
				$jenisRawat = 'Rawat Jalan';
			
		}

		$params = array(
			'NoSEP' 		=> $id,
			'JenisRawat' 	=> $jenisRawat,
			'KelasRawat' 	=> $kelasoptions[$model->KELAS_RAWAT],
			'NoRM' 			=> $model->NO_RM,
			'NoRujukan' 	=> $model->NO_RUJUKAN,
			'AsalRujukan' 	=> $model->nmProvider,
			'TglRujukan' 	=> $model->TGL_RUJUKAN,
			'TglSep' 		=> $model->TGL_SEP,
			'DiagAwal' 		=> $model->namaDiagnosa,
			'PoliTujuan' 	=> $model->nmPoli,
			'Catatan' 		=> $model->CATATAN,
			'NamaPeserta' 	=> $bpjsPasien->NAMAPESERTA,
			'TglLahir' 		=> $bpjsPasien->TGLLAHIR,
			'JenisKelamin'	=> $bpjsPasien->KELAMIN,
			'Kepesertaan'	=> $bpjsPasien->NAMA_PISAT,
			'NoKartu'		=> $bpjsPasien->NoKartu,
			'COB'			=> '',
		);

		$this->CetakSep($params);
	}

	public function actionSepPopup($kartu, $jr=0, $idPasien=0)
	{
		$this->title = 'Pembuatan SEP | '.Yii::app()->name;

		$listPisat = array(
				1 => 'Peserta',
				2 => 'Suami',
				3 => 'Istri',
				4 => 'Anak',
				5 => 'Tambahan'
			);


		$bpjsSep = new BpjsSep;
		$bpjsSep->JENIS_RAWAT = $jr;
		$model = Pasien::model()->findByPk($idPasien);
		$bpjsPasien = BpjsPasien::model()->findByAttributes(array('PASIEN_ID'=>$idPasien));

		

		// $bpjsPasien = new BpjsPasien;
		$bpjsData = null;
		$riwayatTerakhir = null;
		
		
		if(isset($_POST['BpjsSep']))
		{
			$bpjsSep->attributes = $_POST['BpjsSep'];

			$attr = array(
				'TGLLAHIR' => $bpjsPasien->TGLLAHIR,
				'NAMA' => $bpjsPasien->NAMAPESERTA,
				'JENSKEL' => $bpjsPasien->KELAMIN
			);

			$p = Pasien::model()->findByAttributes($attr);

			$transaction=Yii::app()->db->beginTransaction();
			try
			{
				if(empty($p))
				{

					$model->NoMedrec = Yii::app()->helper->getMinimumUnusedID();

					$model->JENSKEL = $bpjsPasien->KELAMIN;
					$model->KodeKec = 99999;
					$model->ALAMAT = '-';
					$model->Desa = '-';
					$model->NAMA = $bpjsPasien->NAMAPESERTA;
					$model->TGLLAHIR = $bpjsPasien->TGLLAHIR;

					if(!empty($bpjsPasien->NIK))
						$model->NOIDENTITAS = $bpjsPasien->NIK;
					else
						$model->NOIDENTITAS = Yii::app()->helper->generateUniqueCode();

					// print_r($model);exit;
					if($model->validate())
					{

						$model->save();	
					}

					else
					{
						 $errors = $model->getErrors();
					      var_dump($errors); //or print_r($errors)
					      exit;
						// throw new Exception();
						
					}
					
				}


				else{

					$model = $p;
				}


				

				$transaction->commit();

			}

			catch(Exception $e){
				
				$transaction->rollback();
			}

			

			$jenisRawat = '';
			switch ($bpjsSep->JENIS_RAWAT) {
				case 0:
					$jenisRawat = 'IGD';
					break;
				case 1:
					$jenisRawat = 'Rawat Inap';
					break;
				case 2:
					$jenisRawat = 'Rawat Jalan';
				
			}


			$kelasoptions = array(
        		'1' => 'Kelas I',
        		'2' => 'Kelas II',
        		'3' => 'Kelas III'
        	);

			$request = array(
				'noKartu' 		=> $bpjsPasien->NoKartu,
				'tglSep' 		=> $bpjsSep->TGL_SEP,
				'tglRujukan'	=> $bpjsSep->TGL_RUJUKAN,
				'noRujukan'		=> $bpjsSep->NO_RUJUKAN,
				'ppkRujukan'	=> $bpjsSep->kdProvider,
				'ppkPelayanan'	=> Yii::app()->params->kodeppk,
				'jnsPelayanan'	=> $bpjsSep->JENIS_RAWAT,
				'catatan'		=> $bpjsSep->CATATAN,
				'diagAwal'		=> $bpjsSep->DIAG_AWAL,
				'poliTujuan'	=> $bpjsSep->POLI_TUJUAN,
				'klsRawat'		=> $bpjsSep->KELAS_RAWAT,
				'lakaLantas'	=> $bpjsSep->LAKA_LANTAS,
				'lokasiLaka'    => $bpjsSep->lokasi_laka,
				'user'			=> Yii::app()->user->getState('username'),
				'noMr'			=> $bpjsPasien->PASIEN_ID

			);



			// print_r($request);exit;
				
			$result = Yii::app()->rest->getNoSEP($request);


			if($result->metadata->code == '200')
			{
				$bpjsSep->NoSEP = $result->response;
				$bpjsSep->NO_RM = $request['noMr'];
				if($bpjsSep->validate())
				{	
					// print_r($bpjsSep->attributes);exit;
					$bpjsSep->save();
					$pisat = array("None","Peserta","Suami","Istri","Anak","Tambahan");

					//print_r($bpjsSep->aSALRUJUKAN);exit;
					$params = array(
						'NoSEP' 		=> $result->response,
						'JenisRawat' 	=> $jenisRawat,
						'KelasRawat' 	=> $kelasoptions[$bpjsSep->KELAS_RAWAT],
						'NoRM' 			=> $request['noMr'],
						'NoRujukan' 	=> $bpjsSep->NO_RUJUKAN,
						'AsalRujukan' 	=> $bpjsSep->nmProvider,
						'TglRujukan' 	=> $bpjsSep->TGL_RUJUKAN,
						'TglSep' 		=> $bpjsSep->TGL_SEP,
						'DiagAwal' 		=> $bpjsSep->namaDiagnosa,
						'PoliTujuan' 	=> $bpjsSep->nmPoli,
						'Catatan' 		=> $bpjsSep->CATATAN,
						'NamaPeserta' 	=> $bpjsPasien->NAMAPESERTA,
						'TglLahir' 		=> $bpjsPasien->TGLLAHIR,
						'JenisKelamin'	=> $bpjsPasien->KELAMIN,
						'Kepesertaan'	=> $bpjsPasien->NAMA_PISAT,
						'NoKartu'		=> $bpjsPasien->NoKartu,
						'COB'			=> '',
					);
					$this->CetakSep($params);
				}

				
			}
			else
			{

				$bpjsSep->addError('RESPONSE_ERROR',$result->metadata->code.' '.$result->metadata->message);
					
			}
			
			

		}

		date_default_timezone_set('Asia/Jakarta');
		$bpjsSep->TGL_SEP = date('Y-m-d H:i:s');

		$this->render('_sep',array(
			'model'=>$model,
			'bpjsPasien' => $bpjsPasien,
			'bpjsData' => $bpjsData,
			'bpjsSep' => $bpjsSep,
			'riwayatTerakhir' => $riwayatTerakhir,
			'jenisRawat' => $jr
		));
	}

	public function actionSep($idPasien = 0,$jr = 0)
	{
		$this->title = 'Pembuatan SEP | '.Yii::app()->name;

		$listPisat = array(
				1 => 'Peserta',
				2 => 'Suami',
				3 => 'Istri',
				4 => 'Anak',
				5 => 'Tambahan'
			);


		$bpjsSep = new BpjsSep;
		$bpjsSep->JENIS_RAWAT = $jr;
		if(!empty($idPasien)){
			$model = Pasien::model()->findByPk($idPasien);
			$bpjsPasien = BpjsPasien::model()->findByAttributes(array('PASIEN_ID'=>$idPasien));

		}
		else{
			$model= new Pasien;
			$bpjsPasien = new BpjsPasien;

		}

		// $bpjsPasien = new BpjsPasien;
		$bpjsData = null;
		$riwayatTerakhir = null;
		
		
		if(isset($_POST['BpjsSep']))
		{
			$bpjsSep->attributes = $_POST['BpjsSep'];

			$attr = array(
				'TGLLAHIR' => $_POST['BpjsPasien']['TGLLAHIR'],
				'NAMA' => $_POST['BpjsPasien']['NAMAPESERTA'],
				'JENSKEL' => $_POST['BpjsPasien']['KELAMIN']
			);

			$p = Pasien::model()->findByAttributes($attr);

			$transaction=Yii::app()->db->beginTransaction();
			try
			{
				if(empty($p))
				{

					$model->NoMedrec = Yii::app()->helper->getMinimumUnusedID();

					$model->JENSKEL = $_POST['BpjsPasien']['KELAMIN'];
					$model->KodeKec = 99999;
					$model->ALAMAT = '-';
					$model->Desa = '-';
					$model->NAMA = $_POST['BpjsPasien']['NAMAPESERTA'];
					$model->TGLLAHIR = $_POST['BpjsPasien']['TGLLAHIR'];
// 					print_r($_POST['BpjsPasien']);
// exit;

					if(!empty($_POST['BpjsPasien']['NIK']))
						$model->NOIDENTITAS = $_POST['BpjsPasien']['NIK'];
					else
						$model->NOIDENTITAS = Yii::app()->helper->generateUniqueCode();

					// print_r($model);exit;
					if($model->validate())
					{

						$model->save();	
					}

					else
					{
						 $errors = $model->getErrors();
					      var_dump($errors); //or print_r($errors)
					      exit;
						// throw new Exception();
						
					}
					
				}


				else{

					$model = $p;
				}


				$bpjsPasien->attributes = $_POST['BpjsPasien'];
				$bpjsPasien->PASIEN_ID = $model->NoMedrec;

				if($bpjsPasien->validate()){
					$bpjsPasien->save();
				}
				else{
					throw new Exception();
					
				}

				$transaction->commit();

				//$this->redirect(array('TrRawatInap/input','id'=>$id));
			}

			catch(Exception $e){
				
				$transaction->rollback();
			}

			

			$jenisRawat = '';
			switch ($bpjsSep->JENIS_RAWAT) {
				case 0:
					$jenisRawat = 'IGD';
					break;
				case 1:
					$jenisRawat = 'Rawat Inap';
					break;
				case 2:
					$jenisRawat = 'Rawat Jalan';
				
			}


			$kelasoptions = array(
        		'1' => 'Kelas I',
        		'2' => 'Kelas II',
        		'3' => 'Kelas III'
        	);

			$request = array(
				'noKartu' 		=> $_POST['BpjsPasien']['NoKartu'],
				'tglSep' 		=> $bpjsSep->TGL_SEP,
				'tglRujukan'	=> $bpjsSep->TGL_RUJUKAN,
				'noRujukan'		=> $bpjsSep->NO_RUJUKAN,
				'ppkRujukan'	=> $_POST['ASAL_RUJUKAN'],
				'ppkPelayanan'	=> Yii::app()->params->kodeppk,
				'jnsPelayanan'	=> $bpjsSep->JENIS_RAWAT,
				'catatan'		=> $bpjsSep->CATATAN,
				'diagAwal'		=> $bpjsSep->DIAG_AWAL,
				'poliTujuan'	=> $bpjsSep->POLI_TUJUAN,
				'klsRawat'		=> $bpjsSep->KELAS_RAWAT,
				'lakaLantas'	=> $bpjsSep->LAKA_LANTAS,
				'lokasiLaka'    => $bpjsSep->lokasi_laka,
				'user'			=> Yii::app()->user->getState('username'),
				'noMr'			=> $bpjsPasien->PASIEN_ID

			);



			// print_r($request);exit;
				
			$result = Yii::app()->rest->getNoSEP($request);


			if($result->metadata->code == '200')
			{
				$bpjsSep->NoSEP = $result->response;
				$bpjsSep->NO_RM = $request['noMr'];
				if($bpjsSep->validate())
				{	
					$bpjsSep->save();
					$pisat = array("None","Peserta","Suami","Istri","Anak","Tambahan");

					//print_r($bpjsSep->aSALRUJUKAN);exit;
					$params = array(
						'NoSEP' 		=> $result->response,
						'JenisRawat' 	=> $jenisRawat,
						'KelasRawat' 	=> $kelasoptions[$bpjsSep->KELAS_RAWAT],
						'NoRM' 			=> $request['noMr'],
						'NoRujukan' 	=> $bpjsSep->NO_RUJUKAN,
						'AsalRujukan' 	=> $bpjsSep->aSALRUJUKAN->NAMA_PPK,
						'TglRujukan' 	=> $bpjsSep->TGL_RUJUKAN,
						'TglSep' 		=> $bpjsSep->TGL_SEP,
						'DiagAwal' 		=> $bpjsSep->dIAGAWAL->DESKRIPSI,
						'PoliTujuan' 	=> $bpjsSep->pOLITUJUAN->NAMA_POLI,
						'Catatan' 		=> $bpjsSep->CATATAN,
						'NamaPeserta' 	=> $bpjsPasien->NAMAPESERTA,
						'TglLahir' 		=> $bpjsPasien->TGLLAHIR,
						'JenisKelamin'	=> $bpjsPasien->KELAMIN,
						'Kepesertaan'	=> $_POST['PISAT_NAMA'],
						'NoKartu'		=> $bpjsPasien->NoKartu,
						'COB'			=> '',
					);
					$this->CetakSep($params);
				}

				
			}
			else
			{

				$bpjsSep->addError('RESPONSE_ERROR',$result->metadata->code.' '.$result->metadata->message);
					
			}
			
			

		}

		date_default_timezone_set('Asia/Jakarta');
		$bpjsSep->TGL_SEP = date('Y-m-d H:i:s');

		$this->render('sep',array(
			'model'=>$model,
			'bpjsPasien' => $bpjsPasien,
			'bpjsData' => $bpjsData,
			'bpjsSep' => $bpjsSep,
			'riwayatTerakhir' => $riwayatTerakhir,
			'jenisRawat' => $jr
		));
	}

	private function CetakSep($params = array())
	{

		if(empty($params)) exit;


		try
		{
			ob_start();
			$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'P', 'mm', 'A4', true, 'UTF-8');

			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$page_format = array(
			    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 74.25),
			

			);

			$pdf->AddPage('L', $page_format, false, false);
			$pdf->SetAutoPageBreak(TRUE, 0);

			$left = 5;
			$ti = 4.5;
			$right = 130;

			

			// LOGO
			$pdf->Image(Yii::app()->baseUrl.'/images/bpjs-logo.jpg', $right+25, 4, 46, 7.5, 'JPG');

			$pdf->Image(Yii::app()->baseUrl.'/images/logo-rsud.jpg', $left, 3, 10, 10, 'JPG');

			// TITLE
			$pdf->SetFont('helvetica', '', 12);
			$pdf->Text($left + 11,3 ,'SURAT ELIGIBILITAS PESERTA');
			$pdf->Text($left + 11,8 ,'RSUD KAB KEDIRI');


			$top = 10;

			$pdf->SetFont('helvetica', '', 9);
			$top = $top + $ti;
			$pdf->Text($left,$top ,'No. SEP');
			$pdf->Text($left + 30,$top,':');

			$pdf->SetFont('helvetica', 'B', 11);
			$pdf->Text($left + 33,$top - 0.5,$params['NoSEP']);
			
			$pdf->SetFont('helvetica', '', 9);
			$top = $top + $ti;
			$pdf->Text($left,$top ,'Tgl. SEP');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['TglSep']);
			
			$top = $top + $ti;
			$pdf->Text($left,$top ,'No. Kartu');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['NoKartu']);
			$pdf->Text($left + 60,$top,'No.MR : '.$params['NoRM']);


			$pdf->Text($right,$top ,'Peserta');
			$pdf->Text($right + 30,$top,':');
			$pdf->Text($right + 33,$top,$params['Kepesertaan']);
			
			$top = $top + $ti;
			$pdf->Text($left,$top ,'Nama Peserta');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['NamaPeserta']);
			
			$top = $top + $ti;
			$pdf->Text($left,$top ,'Tgl. Lahir');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['TglLahir']);
			
			$pdf->Text($right,$top ,'COB');
			$pdf->Text($right + 30,$top,':');
			$pdf->Text($right + 33,$top,$params['COB']);

			$top = $top + $ti;
			$pdf->Text($left,$top ,'Jns. Kelamin');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['JenisKelamin']);

			$pdf->Text($right,$top ,'Jns. Rawat');
			$pdf->Text($right + 30,$top,':');
			$pdf->Text($right + 33,$top,$params['JenisRawat']);

			
			$top = $top + $ti;
			$pdf->Text($left,$top ,'Poli. Tujuan');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['PoliTujuan']);

			$pdf->Text($right,$top ,'Kls. Rawat');
			$pdf->Text($right + 30,$top,':');
			$pdf->Text($right + 33,$top,$params['KelasRawat']);

			
			$top = $top + $ti;
			$pdf->Text($left,$top ,'Asal Faskes Tk. I');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['AsalRujukan']);
			
			$top = $top + $ti;
			$pdf->Text($left,$top ,'Diagnosa Awal');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['DiagAwal']);

			$pdf->Text($right,$top ,'Pasien /');
			$pdf->Text($right + 40,$top -1 ,'Petugas');
			$pdf->Text($right,$top+15 ,'___________');

			$pdf->Text($right,$top + 3.5,'Keluarga Pasien');
			$pdf->Text($right + 40,$top +2.5 ,'BPJS Kesehatan');
			$pdf->Text($right+ 40,$top+15 ,'___________');


			$top = $top + $ti;
			$pdf->Text($left,$top ,'Catatan');
			$pdf->Text($left + 30,$top,':');
			$pdf->Text($left + 33,$top,$params['Catatan']);
			
			$pdf->SetFont('helvetica', 'I', 8);
			$top = $top + $ti;
			$pdf->Text($left,$top ,'*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan');
			
			$top = $top + 3;
			$pdf->Text($left,$top ,'*SEP bukan sebagai bukti penjaminan peserta');
			
			$top = $top + $ti;
			$pdf->Text($left,$top ,'Catatan ke 1 - '.date('Y-m-d H:i:s A'));
			


			$pdf->Output("SEP.pdf", "I");

		}
		catch(HTML2PDF_exception $e) {
		    echo $e;
		    exit;
		}
		
	}

	public function actionLama($id)
	{


		if(empty($id)) 
		{
			echo 'Invalid params : ID';
			exit;
		}

		$id = ltrim($id,'0');

		$listPisat = array(
				1 => 'Peserta',
				2 => 'Suami',
				3 => 'Istri',
				4 => 'Anak',
				5 => 'Tambahan'
			);


		$bpjsSep = new BpjsSep;

		$model= Pasien::model()->findByPk($id);
		$bpjsPasien = BpjsPasien::model()->findByAttributes(array('PASIEN_ID'=> $id));
		
		if(empty($bpjsPasien))
		{	
			$bpjsPasien = new BpjsPasien;
			$bpjsData = null;
			$riwayatTerakhir = null;
		}

		else
		{

			$nokartu = $bpjsPasien->NoKartu;
			$jenis = 'kartu';
			$riwayatTerakhir = Yii::app()->rest->getRiwayatTerakhir($nokartu);

			if(!empty($riwayatTerakhir))
			{
				if($riwayatTerakhir->metadata->code != '200') 
				{

					echo $riwayatTerakhir->metadata->code.' : '.$riwayatTerakhir->metadata->message;
					exit;
				}
			}


			$param = array(
				'jenis' => $jenis,
				'nomor' => $nokartu, 
			);
			$data = Yii::app()->rest->getPesertaByNomor($param);
			
			$peserta = $data->response->peserta;


			
			$bpjsPasien->NAMAPESERTA = $peserta->nama;
			$bpjsPasien->NIK = $peserta->nik;
			$bpjsPasien->PISAT = $listPisat[$peserta->pisa];
			$bpjsPasien->PPKTK1 = $peserta->provUmum->kdProvider.' - '.$peserta->provUmum->nmProvider;
			$bpjsPasien->PESERTA = $peserta->jenisPeserta->kdJenisPeserta;
			$bpjsPasien->NAMA_KEPESERTAAN = $peserta->jenisPeserta->nmJenisPeserta;
			
			$bpjsPasien->KELASRAWAT = $peserta->kelasTanggungan->nmKelas;
			$bpjsPasien->TGLCETAKKARTU = $peserta->tglCetakKartu;
			$bpjsPasien->TGLLAHIR = $peserta->tglLahir;
			$bpjsPasien->TAT = $peserta->tglTAT;
			$bpjsPasien->TMT = $peserta->tglTMT;
			


			$bpjsData = array(
				'kelas' => array(
					'id' => $peserta->kelasTanggungan->kdKelas,
					'value' => $peserta->kelasTanggungan->nmKelas
				),
			);
		}



		if(isset($_POST['BpjsSep']))
		{
			$bpjsSep->attributes = $_POST['BpjsSep'];
			$jenisRawat = '';
			switch ($bpjsSep->JENIS_RAWAT) {
				case 0:
					$jenisRawat = 'IGD';
					break;
				case 1:
					$jenisRawat = 'Rawat Inap';
					break;
				case 2:
					$jenisRawat = 'Rawat Jalan';
				
			}


			$kelasoptions = array(
        		'1' => 'Kelas I',
        		'2' => 'Kelas II',
        		'3' => 'Kelas III'
        	);
			$request = array(
				'noKartu' 		=> $bpjsPasien->NoKartu,
				'tglSep' 		=> $bpjsSep->TGL_SEP,
				'tglRujukan'	=> $bpjsSep->TGL_RUJUKAN,
				'noRujukan'		=> $bpjsSep->NO_RUJUKAN,
				'ppkRujukan'	=> $bpjsSep->ASAL_RUJUKAN,
				'ppkPelayanan'	=> Yii::app()->params->kodeppk,
				'jnsPelayanan'	=> $bpjsSep->JENIS_RAWAT,
				'catatan'		=> $bpjsSep->CATATAN,
				'diagAwal'		=> $bpjsSep->DIAG_AWAL,
				'poliTujuan'	=> $bpjsSep->POLI_TUJUAN,
				'klsRawat'		=> $bpjsSep->KELAS_RAWAT,
				'lakaLantas'	=> $bpjsSep->LAKA_LANTAS,
				'lokasiLaka'    => $bpjsSep->lokasi_laka,
				'user'			=> Yii::app()->user->getState('username'),
				'noMr'			=> $id

			);

				
			$result = Yii::app()->rest->getNoSEP($request);


			if($result->metadata->code == '200')
			{


				//print_r($bpjsSep->aSALRUJUKAN);exit;
				$params = array(
					'NoSEP' 		=> $result->response,
					'JenisRawat' 	=> $jenisRawat,
					'KelasRawat' 	=> $kelasoptions[$bpjsSep->KELAS_RAWAT],
					'NoRM' 			=> $id,
					'NoRujukan' 	=> $bpjsSep->NO_RUJUKAN,
					'AsalRujukan' 	=> 'RSUD KAB KEDIRI',
					'TglRujukan' 	=> $bpjsSep->TGL_RUJUKAN,
					'TglSep' 		=> $bpjsSep->TGL_SEP,
					'DiagAwal' 		=> $bpjsSep->dIAGAWAL->DESKRIPSI,
					'PoliTujuan' 	=> $bpjsSep->pOLITUJUAN->NAMA_POLI,
					'Catatan' 		=> $bpjsSep->CATATAN,
					'NamaPeserta' 	=> $bpjsPasien->NAMAPESERTA,
					'TglLahir' 		=> $bpjsPasien->TGLLAHIR,
					'JenisKelamin'	=> $bpjsPasien->KELAMIN,
					'Kepesertaan'	=> $peserta->jenisPeserta->nmJenisPeserta,
					'NoKartu'		=> $bpjsPasien->NoKartu,
					'COB'			=> '',
				);
				$this->CetakSep($params);

			}
			else
			{

				$bpjsSep->addError('RESPONSE_ERROR',$result->metadata->code.' '.$result->metadata->message);
					
			}
			
			

		}

		date_default_timezone_set('Asia/Jakarta');
		$bpjsSep->TGL_SEP = date('Y-m-d H:i:s');

		$this->render('lama',array(
			'model'=>$model,
			'bpjsPasien' => $bpjsPasien,
			'bpjsData' => $bpjsData,
			'bpjsSep' => $bpjsSep,
			'riwayatTerakhir' => $riwayatTerakhir
		));
	}


	public function actionBaru($id)
	{

		if(empty($id)) 
		{
			echo 'Invalid params : ID';
			exit;
		}

		$id = ltrim($id,'0');

		$listPisat = array(
				1 => 'Peserta',
				2 => 'Suami',
				3 => 'Istri',
				4 => 'Anak',
				5 => 'Tambahan'
			);


		$bpjsSep = new BpjsSep;

		$model= Pasien::model()->findByPk($id);
		$bpjsPasien = BpjsPasien::model()->findByAttributes(array('PASIEN_ID'=> $id));



		
		$nokartu = $bpjsPasien->NoKartu;
		$jenis = 'kartu';

		

		// $riwayatTerakhir = Yii::app()->rest->getRiwayatTerakhir($nokartu);
		
		// if($riwayatTerakhir->metadata->code != '200') 
		// {

		// 	echo $riwayatTerakhir->metadata->code.' : '.$riwayatTerakhir->metadata->message;
		// 	exit;
		// }

		$param = array(
			'jenis' => $jenis,
			'nomor' => $nokartu, 
		);
		$data = Yii::app()->rest->getPesertaByNomor($param);
		
		$peserta = $data->response->peserta;
		
		$bpjsPasien->NAMAPESERTA = $peserta->nama;
		$bpjsPasien->NIK = $peserta->nik;
		$bpjsPasien->PISAT = $listPisat[$peserta->pisa];
		$bpjsPasien->PPKTK1 = $peserta->provUmum->kdProvider.' - '.$peserta->provUmum->nmProvider;
		$bpjsPasien->PESERTA = $peserta->jenisPeserta->kdJenisPeserta;
		$bpjsPasien->NAMA_KEPESERTAAN = $peserta->jenisPeserta->nmJenisPeserta;
		
		$bpjsPasien->KELASRAWAT = $peserta->kelasTanggungan->nmKelas;
		$bpjsPasien->TGLCETAKKARTU = $peserta->tglCetakKartu;
		$bpjsPasien->TGLLAHIR = $peserta->tglLahir;
		$bpjsPasien->TAT = $peserta->tglTAT;
		$bpjsPasien->TMT = $peserta->tglTMT;
		
		$bpjsData = array(
			'kelas' => array(
				'id' => $peserta->kelasTanggungan->kdKelas,
				'value' => $peserta->kelasTanggungan->nmKelas
			),
		);


		if(isset($_POST['BpjsSep']))
		{
			$bpjsSep->attributes = $_POST['BpjsSep'];
			$jenisRawat = '';
			switch ($bpjsSep->JENIS_RAWAT) {
				case 0:
					$jenisRawat = 'IGD';
					break;
				case 1:
					$jenisRawat = 'Rawat Inap';
					break;
				case 2:
					$jenisRawat = 'Rawat Jalan';
				
			}


			$kelasoptions = array(
        		'1' => 'Kelas I',
        		'2' => 'Kelas II',
        		'3' => 'Kelas III'
        	);

			$request = array(
				'noKartu' 		=> $bpjsPasien->NoKartu,
				'tglSep' 		=> $bpjsSep->TGL_SEP,
				'tglRujukan'	=> $bpjsSep->TGL_RUJUKAN,
				'noRujukan'		=> $bpjsSep->NO_RUJUKAN,
				'ppkRujukan'	=> $bpjsSep->ASAL_RUJUKAN,
				'ppkPelayanan'	=> Yii::app()->params->kodeppk,
				'jnsPelayanan'	=> $bpjsSep->JENIS_RAWAT,
				'catatan'		=> $bpjsSep->CATATAN,
				'diagAwal'		=> $bpjsSep->DIAG_AWAL,
				'poliTujuan'	=> $bpjsSep->POLI_TUJUAN,
				'klsRawat'		=> $bpjsSep->KELAS_RAWAT,
				'lakaLantas'	=> $bpjsSep->LAKA_LANTAS,
				'lokasiLaka'    => 'testing',
				'user'			=> Yii::app()->user->getState('username'),
				'noMr'			=> $id

			);

			if($bpjsSep->validate())
			{
				$result = Yii::app()->rest->getNoSEP($request);

				if($result->metadata->code == '200')
				{
					$params = array(
						'NoSEP' 		=> $result->response,
						'JenisRawat' 	=> $jenisRawat,
						'KelasRawat' 	=> $kelasoptions[$bpjsSep->KELAS_RAWAT],
						'NoRM' 			=> $id,
						'NoRujukan' 	=> $bpjsSep->NO_RUJUKAN,
						'AsalRujukan' 	=> $bpjsSep->aSALRUJUKAN->NAMA_PPK,
						'TglRujukan' 	=> $bpjsSep->TGL_RUJUKAN,
						'TglSep' 		=> $bpjsSep->TGL_SEP,
						'DiagAwal' 		=> $bpjsSep->dIAGAWAL->DESKRIPSI,
						'PoliTujuan' 	=> $bpjsSep->pOLITUJUAN->NAMA_POLI,
						'Catatan' 		=> $bpjsSep->CATATAN,
						'NamaPeserta' 	=> $bpjsPasien->NAMAPESERTA,
						'TglLahir' 		=> $bpjsPasien->TGLLAHIR,
						'JenisKelamin'	=> $bpjsPasien->KELAMIN,
						'Kepesertaan'	=> $peserta->jenisPeserta->nmJenisPeserta,
						'NoKartu'		=> $bpjsPasien->NoKartu,
						'COB'			=> '',
					);

					$this->CetakSep($params);

				}
				else
				{

					$bpjsSep->addError('RESPONSE_ERROR',$result->metadata->code.' '.$result->metadata->message);
						
				}
			}
			

		}

		$bpjsSep->TGL_SEP = date('Y-m-d H:i:s');

		$this->render('baru',array(
			'model'=>$model,
			'bpjsPasien' => $bpjsPasien,
			'bpjsData' => $bpjsData,
			'bpjsSep' => $bpjsSep,
			'riwayatTerakhir' => $riwayatTerakhir
		));
		
	}

	
}