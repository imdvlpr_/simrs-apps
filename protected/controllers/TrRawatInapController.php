<?php

class TrRawatInapController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','input','tindakan','pindah','keluarIrd','laporan','cetakKwitansi',
					'dataPasien','pelMedik','medikTOP','medikTNOP','penunjang','alkes','lainnya','total','kwitansi','pulang','datarawat','formBpjs','obatPasien','inputObat','ajaxObatPasien','ajaxObatPasienList','deletePasienObat','cetakRincianIRD','admin','visiteKosong','openAccess','lockAccess','inputDiagnosis'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionInputDiagnosis($id)
	{
		if($id != 0)
		{
			$model=BPendaftaran::model()->findByPk($id);
			
			if(isset($_POST['BPendaftaran']))
			{
				
				$model->attributes=$_POST['BPendaftaran'];
				$transaction=Yii::app()->db->beginTransaction();
				$errors = "";
				try
				{	

					if(isset($_POST['diagnosis']))
					{

						BPendaftaranDiagnosis::model()->deleteAllByAttributes(['reg_id'=>$model->NODAFTAR,'tipe'=>1]);
						foreach($_POST['diagnosis'] as $data)
						{
							if(empty($data)) continue;
							$tmp = new BPendaftaranDiagnosis;
							$tmp->reg_id = $model->NODAFTAR;
							$tmp->tipe = 1;
							$tmp->keterangan = $data;
							if(!$tmp->save())
							{
								foreach($tmp->getErrors() as $attribute){
									foreach($attribute as $error){
										$errors .= $error.' ';
									}
								}
								throw new Exception();
							}
						}
					}

					if(isset($_POST['tindakan']))
					{
						BPendaftaranDiagnosis::model()->deleteAllByAttributes(['reg_id'=>$model->NODAFTAR,'tipe'=>2]);
						foreach($_POST['tindakan'] as $data)
						{
							if(empty($data)) continue;
							$tmp = new BPendaftaranDiagnosis;
							$tmp->reg_id = $model->NODAFTAR;
							$tmp->tipe = 2;
							$tmp->keterangan = $data;
							if(!$tmp->save())
							{
								foreach($tmp->getErrors() as $attribute){
									foreach($attribute as $error){
										$errors .= $error.' ';
									}
								}
								throw new Exception();
							}
						}	
					}

					if($model->save()){

						$transaction->commit();
						Yii::app()->user->setFlash('success', "Data telah tersimpan.");
						$this->redirect(['inputDiagnosis','id'=>$id]);
					}

					else
					{
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						throw new Exception("Error Processing Request", 1);
					}
				}

				catch(Exception $e){
					
					$transaction->rollback();
					Yii::app()->user->setFlash('danger', $errors.$e->getMessage());

					$this->redirect(['inputDiagnosis','id'=>$id]);
				}
			}

			$this->render('inputDiagnosis',array(
				'model'=>$model,
			));
		}

	}

	public function actionLockAccess($id)
	{
		if(!empty($id))
		{
			if( Yii::app()->request->isAjaxRequest )
			{
				$model = TrRawatInap::model()->findByPk($id);

			    // Stop jQuery from re-initialization
			    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
			 
			    if( isset( $_POST['action'] ) && $_POST['action'] == 'confirmApprove' )
			    {
				
			    	$model->is_locked = 1;
					$model->update(array('is_locked'));
					
					echo CJSON::encode( array(
							'status' => 'success',
							'content' => 'Sukses',
					   ));
			    		
			       exit;	
			    	
			       
			    }
			    else if( isset( $_POST['action'] ) )
			    {
			       echo CJSON::encode( array(
			         'status' => 'canceled',
			         'content' => 'Batal Tutup Akses',
			       ));
			      exit;
			    }
			    else
			    {

			      echo CJSON::encode( array(
			        'status' => 'failure',
			        'content' => $this->renderPartial( '_lockAccess', array(
			        	'model' => $model
			           ), true, true ),
			      ));
			      exit;
			    }
			}
			else
			{
			    if( isset( $_POST['confirmApprove'] ) )
			    {
			      // $model->delete();
			       $this->redirect( array( 'admin' ) );
			    }
			    else if( isset( $_POST['denyDelete'] ) )
			       $this->redirect( array( 'admin' ) );
			    else
			       $this->render( '_lockAccess',array(
			       		
			       	));
			}
		}
	}

	public function actionOpenAccess($id)
	{
		if(!empty($id))
		{
			if( Yii::app()->request->isAjaxRequest )
			{
				$model = TrRawatInap::model()->findByPk($id);

			    // Stop jQuery from re-initialization
			    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
			 
			    if( isset( $_POST['action'] ) && $_POST['action'] == 'confirmApprove' )
			    {
				
			    	$model->is_locked = 0;
					$model->update(array('is_locked'));
					
					echo CJSON::encode( array(
							'status' => 'success',
							'content' => 'Sukses',
					   ));
			    		
			       exit;	
			    	
			       
			    }
			    else if( isset( $_POST['action'] ) )
			    {
			       echo CJSON::encode( array(
			         'status' => 'canceled',
			         'content' => 'Batal Akses Rawat Inap',
			       ));
			      exit;
			    }
			    else
			    {

			      echo CJSON::encode( array(
			        'status' => 'failure',
			        'content' => $this->renderPartial( '_openAccess', array(
			        	'model' => $model
			           ), true, true ),
			      ));
			      exit;
			    }
			}
			else
			{
			    if( isset( $_POST['confirmApprove'] ) )
			    {
			      // $model->delete();
			       $this->redirect( array( 'admin' ) );
			    }
			    else if( isset( $_POST['denyDelete'] ) )
			       $this->redirect( array( 'admin' ) );
			    else
			       $this->render('_openAccess',array(
			       		
			       	));
			}
		}
	}

	public function actionVisiteKosong()
	{
		$data = Yii::app()->db->createCommand()
	    ->select('*')
	    ->from('v_visite_all')
	    ->queryAll();

	    $dataVisite = JenisVisite::model()->findAllByAttributes(array('nama_visite'=>'Visite Dokter Ahli'));


	    $listVisite = array();
	    foreach($dataVisite as $dv)
		{

			$listVisite[] = $dv->id_jenis_visite;
		}



		$result = array();
		$transaction=Yii::app()->db->beginTransaction();
		try
		{


			$i=0;
			foreach($data as $d)
			{
				$i++;
				$d = (object)$d;
				// $visiteDokter = TrRawatInapVisiteDokter::model()->findAllByAttributes(array('id_rawat_inap'=>$d->id_rawat_inap));

				// $listDokterIrna = array();
				// foreach($visiteDokter as $vd)
				// {
				// 	if(!empty($vd->id_dokter_irna))
				// 		$listDokterIrna[] = $vd->id_dokter_irna;
				// }

				if(empty($d->lm)) continue;

				$isnotfound = TrRawatInap::model()->cekDokterVisite($d->id_rawat_inap,$d->id_dokter);
				if($isnotfound)
				{
					echo $d->id_rawat_inap.' new dokter visite<br>';
					$rvd = new TrRawatInapVisiteDokter;
					$rvd->id_rawat_inap = $d->id_rawat_inap;
					$rvd->id_jenis_visite = $d->id_visite;
					$rvd->id_dokter = 1;
					$rvd->id_dokter_irna = $d->id_dokter;
					$rvd->jumlah_visite = $d->lm;
					$rvd->jumlah_visite_ird = 0;
					$rvd->biaya_visite = 0;
					$rvd->biaya_visite_irna = $d->biaya;

					if($rvd->validate())
					{
						$rvd->save();
					}

					else
					{
						
						print_r($rvd->getErrors());exit;
						
					}	
				}

				else 
				{
					if(TrRawatInap::model()->cekVisiteAhli($d->id_rawat_inap,$d->id_dokter))
					{
						echo $d->id_rawat_inap.' new jenis visite<br>';
						$rvd = new TrRawatInapVisiteDokter;
						$rvd->id_rawat_inap = $d->id_rawat_inap;
						$rvd->id_jenis_visite = $d->id_visite;
						$rvd->id_dokter = 1;
						$rvd->id_dokter_irna = $d->id_dokter;
						$rvd->jumlah_visite = $d->lm;
						$rvd->jumlah_visite_ird = 0;
						$rvd->biaya_visite = 0;
						$rvd->biaya_visite_irna = $d->biaya;

						if($rvd->validate())
						{
							$rvd->save();
						}

						else
						{
							
							print_r($rvd->getErrors());exit;
							
						}	
					}
				}
				
				// print_r($listVisite);exit;
	    // 		if(!empty($visiteDokter))
	    // 		{

	    // 			$cek = false;
	    // 			$idcek=0;
	    // 			foreach($visiteDokter as $vd)
					// {

					// 	$idcek++;

					// 	if(!in_array($d->id_dokter, $listDokterIrna))
					// 	{
					// 		$cek = true;
					// 		echo $d->id_rawat_inap.' '.$d->NAMA.' '.$vd->id_ri_visite.'<br>';
					// 		break;
					// 	}

					// 	else if(!in_array($vd->id_jenis_visite,$listVisite) && in_array($vd->id_dokter, $listDokterIrna))
					// 	{
					// 		$cek = true;
					// 		echo $idcek.'<br>';
					// 		print_r($vd);
					// 		exit;
					// 		echo 'No Dokter: '.$d->id_rawat_inap.' '.$d->NAMA.' '.$vd->id_ri_visite.'<br>';
					// 		break;
					// 	}	
					// 	else
					// 	{
					// 		break;
					// 	}


					// }

					// if($cek)
					// {	
					// 	$rvd = new TrRawatInapVisiteDokter;
					// 	$rvd->id_rawat_inap = $d->id_rawat_inap;
					// 	$rvd->id_jenis_visite = $d->id_visite;
					// 	$rvd->id_dokter = 1;
					// 	$rvd->id_dokter_irna = $d->id_dokter;
					// 	$rvd->jumlah_visite = $d->lm;
					// 	$rvd->jumlah_visite_ird = 0;
					// 	$rvd->biaya_visite = 0;
					// 	$rvd->biaya_visite_irna = $d->biaya;

					// 	if($rvd->validate())
					// 	{
					// 		$rvd->save();
					// 	}

					// 	else
					// 	{
							
					// 		print_r($rvd->getErrors());exit;
							
					// 	}	
					// }
	    // 		}

	    // 		else
	    // 		{
	    // 			echo 'NV:'.$d->id_rawat_inap.' '.$d->NAMA.'<br>';
	    // 			$rvd = new TrRawatInapVisiteDokter;
					// $rvd->id_rawat_inap = $d->id_rawat_inap;
					// $rvd->id_jenis_visite = $d->id_visite;
					// $rvd->id_dokter = 1;
					// $rvd->id_dokter_irna = $d->id_dokter;
					// $rvd->jumlah_visite = $d->lm;
					// $rvd->jumlah_visite_ird = 0;
					// $rvd->biaya_visite = 0;
					// $rvd->biaya_visite_irna = $d->biaya;

					// if($rvd->validate())
					// {
					// 	$rvd->save();
					// }

					// else
					// {
						
					// 	print_r($rvd->getErrors());exit;
						
					// }	
	    // 		}

	    		

				// if(empty($d->id_visite) || empty($d->lm)) continue;

				// $rvd = new TrRawatInapVisiteDokter;
				// $rvd->id_rawat_inap = $d->id_rawat_inap;
				// $rvd->id_jenis_visite = $d->id_visite;
				// $rvd->id_dokter = 1;
				// $rvd->id_dokter_irna = $d->id_dokter;
				// $rvd->jumlah_visite = $d->lm;
				// $rvd->jumlah_visite_ird = 0;
				// $rvd->biaya_visite = 0;
				// $rvd->biaya_visite_irna = $d->biaya;

				// if($rvd->validate())
				// {
				// 	$rvd->save();
				// }

				// else
				// {
					
				// 	print_r($rvd->getErrors());exit;
				// 	// throw new Exception();
				// 	exit;
				// }	
				
			}
			// Data Visite
					
			$transaction->commit();


		}

		catch(Exception $e){
			// print_r($e);
			// Yii::app()->user->setFlash('error', print_r($e));
			$transaction->rollback();
		}
		

		exit;
	}

	public function actionCetakRincianIRD($id)
	{
		

		$rawatInap = TrRawatInap::model()->findByPk($id);

		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;


		if(!empty($rawatInap))
		{


			$html = $this->renderPartial('cetakRincianIRD',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			),true,true);

			try
			{
				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
	                        'P', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				
				$pdf->AddPage();
				$pdf->SetAutoPageBreak(TRUE, 0);
				$pdf->SetFont('helvetica', '', 9);
				$pdf->writeHTML($html, true, false, true, false, '');
				//$pdf->WriteHTMLCell(0,0,0,0 ,$html);
				$pdf->Output('kwitansi_rawat_inap.pdf', "I");
				Yii::app()->end();
				ob_end_clean();
			}

			catch(HTML2PDF_exception $e) 
			{
			    echo $e;
			    exit;
			}
		}
	
	}



	public function actionDeletePasienObat($id)
	{
		$model = TrRawatInapAlkesObat::model()->findByPk($id);
		$id_ri = $model->id_rawat_inap;
		$model->delete(); // tidak aktif

		$rawatInap = TrRawatInap::model()->findByPk($id_ri);
		$rawatInap->updateHargaObatPasien($id_ri);
		

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionAjaxObatPasienList()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$id = $_POST['rid'];
			$params = array(
				'id_rawat_inap' => $id,
				'kode_alkes' => 'OBAT'
			);
			$rits = TrRawatInapAlkesObat::model()->findAllByAttributes($params);

			$hasil = array();

			foreach($rits as $r)
			{
				$hasil[] = array(
					'id' => $r->id_m_obat_akhp,
					'nama' => $r->keterangan,
					'harga' => $r->nilai
				);
			}

			echo CJSON::encode($hasil);
		}
	}

	public function actionAjaxObatPasien()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			$errors = '';
			$transaction=Yii::app()->db->beginTransaction();
			try
			{

				$id = $_POST['rid'];
				$dataObat = $_POST['dataObat'];
				
				// $params = array(
				// 	'id_rawat_inap' => $id,
				// 	// 'id_m_obat_akhp' => $dataObat['id'],
				// 	'kode_alkes' => 'OBAT'
				// );

				// $rit = TrRawatInapAlkesObat::model()->findByAttributes($params);
				$rawatInap = TrRawatInap::model()->findByPk($id);

				// if(empty($rit))
				$rit = new TrRawatInapAlkesObat;

				$rit->id_rawat_inap = $id;
				$rit->kode_alkes = 'OBAT';
				$rit->keterangan = $dataObat['nama'];
				$rit->nilai = $dataObat['harga']; 
				$rit->nilai = str_replace(".", "", $rit->nilai);
				$rit->id_m_obat_akhp = $dataObat['id'];
				$rit->id_dokter = $dataObat['dokter'];
				$rit->tanggal_input = Yii::app()->helper->convertSQLDate($dataObat['tanggal']);
				$rit->jumlah = $dataObat['jumlah'];
				$rit->satuan = $dataObat['satuan'];
				$rit->save();

				if($rit->validate())
				{
					$rit->save();
					
					$total = 0;
					foreach($rawatInap->searchAlkesObat($id) as $obat)
					{
						$total += $obat->nilai;
					}


					$obatAlkes = ObatAlkes::model()->findByAttributes(array('kode_alkes'=>'OBAT'));
					$rit = TrRawatInapAlkes::model()->findByAttributes(array('id_rawat_inap'=>$id));
					if(empty($rit))
						$rit = new TrRawatInapAlkes;

					$rit->id_rawat_inap = $id;
					$rit->id_alkes = $obatAlkes->id_obat_alkes;
					$rit->biaya_irna = $total;
					$rit->biaya_ird = 0;
					$rit->jumlah_tindakan = 1;
					$rit->save();

					$result = array(
						'shortmsg' => 'success',
						'message' => 'Data telah disimpan',
					);
				}

				else
				{
					
					foreach($rit->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}
			

					$rawatInap->addError('ERROR_LAIN',$errors);
					throw new Exception();


				}

				
			
				$transaction->commit();

				
				
				echo CJSON::encode($result);
			}

			catch(Exception $e){
				
				$transaction->rollback();

				$result = array(
					'shortmsg' => 'danger',
					'message' => $errors,
				);

				echo CJSON::encode($result);
			}	
		}
		
	}

	public function actionInputObat($id)
	{
		$this->title = 'Input Obat | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);
		

		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		$alkesObat = new TrRawatInapAlkesObat;

		if(!empty($rawatInap))
		{

			$params = array(
				'id_rawat_inap' => $id,
				'kode_alkes' => 'OBAT'
			);
			$rits = TrRawatInapAlkesObat::model()->findAllByAttributes($params);
			$this->render('inputObat',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian,
				'rits' => $rits,
				'alkesObat' =>$alkesObat
			));
		
		}
	}

	public function actionObatPasien()
	{
		$this->title = 'Farmasi | Rawat Inap | '.Yii::app()->name;


		$model=new TrRawatInap('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['jenis_pasien']))
			$model->TIPE_PASIEN=$_GET['jenis_pasien'];

		if(isset($_GET['TrRawatInap']))
			$model->attributes=$_GET['TrRawatInap'];

		$this->render('obatPasien',array(
			'model'=>$model,
		));
		// $this->render('obatPasien');
		
		
	}

	public function actionFormBpjs($id)
	{
		$this->title = 'Form BPJS | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);

		$this->countTotal($rawatInap);
		
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{

			$html = $this->renderPartial('_formBpjs',array(
					'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			),true,true);


			try
			{
				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
	                        'L', 'mm', 'A5', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				
				$pdf->AddPage();
				$pdf->SetAutoPageBreak(TRUE, 0);
				$pdf->SetFont('helvetica', '', 9);
				$pdf->writeHTML($html, true, false, true, false, '');
				//$pdf->WriteHTMLCell(0,0,0,0 ,$html);
				$pdf->Output('form_bpjs_lampiran.pdf', "I");
				Yii::app()->end();
				ob_end_clean();
			}

			catch(HTML2PDF_exception $e) 
			{
			    echo $e;
			    exit;
			}

			$this->render('formBpjs',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}

	private function countTotal($rawatInap)
	{
		
		if(empty($rawatInap)) exit;

		$total_ird = 0;
		$total_irna = 0;

		$selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
           $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }

        else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }

        $model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian)){
			$rawatRincian = new TrRawatInapRincian;
			$rawatRincian->id_rawat_inap = $rawatInap->id_rawat_inap;
			$rawatRincian->dokter_ird = 1;
			$rawatRincian->save(true,array('id_rawat_inap','dokter_ird'));
		}

		$pasien = $rawatInap->pASIEN;
		
		$total_ird = $total_ird + $rawatRincian->obs_ird;
	 	$isneonatus = $rawatInap->kamar->nama_kamar == 'NEONATUS';

      	if($isneonatus)
      	{


        	$biaya_neonatus = 0;

	        $attr = array(
	            'tr_ri_id' => $rawatInap->id_rawat_inap
	        );
	        $model_kamar = TrRawatInapKamarHistory::model()->findAllByAttributes($attr);

	        foreach($model_kamar as $item)
	        {

	          	$biaya_neonatus = $biaya_neonatus + $item->nilai;
	        }
        

        	$total_irna = $total_irna + $biaya_neonatus;
      	}

      	else
      	{
        	$total_irna = $total_irna + $rawatInap->kamar->biaya_kamar * $selisih_hari;   
      	}
     
     	// $total_irna = $total_irna + $rawatRincian->askep_kamar;

     	$total_irna += $rawatInap->kamar->biaya_askep * $selisih_hari;

		if(!empty($rawatRincian))
			$total_irna = $total_irna + $rawatRincian->asuhan_nutrisi;


		if(!empty($rawatRincian))
			$total_ird = $total_ird + $rawatRincian->biaya_pengawasan;

		if(empty($rawatRincian->askep_kamar))
			$rawatRincian->askep_kamar = $rawatInap->kamar->biaya_askep; 

		if(empty($rawatRincian->asuhan_nutrisi))
      		$rawatRincian->asuhan_nutrisi = $rawatInap->kamar->biaya_asnut; 
      
		$urutan = 1;
		if(!empty($rawatInap->tRIVisiteDokters)){

			foreach($rawatInap->tRIVisiteDokters as $visite)
			{

				if(!empty($visite))
					$total_ird = $total_ird + $visite->biaya_visite;// * $visite->jumlah_visite_ird;

				if(!empty($visite))
					$total_irna = $total_irna + $visite->biaya_visite_irna;// * $visite->jumlah_visite;

			
			}
		}
		 
		$listTop = TindakanMedisOperatif::model()->findAll();
		$i = 0;


		foreach($listTop as $top){
			$attr = array(
			'id_rawat_inap' => $rawatInap->id_rawat_inap,
			'id_top' => $top->id_tindakan
			);
			$itemTop = TrRawatInapTop::model()->findByAttributes($attr);

			$jumlah_tindakan = 0;
			$biaya_irna = 0;

			$valueJm = '';
			$valueAnas = '';
			$biaya_irna_top_total = 0;
			if(!empty($itemTop)){
				$jumlah_tindakan = $itemTop->jumlah_tindakan;
				$biaya_irna = $itemTop->biaya_irna;

				$biaya_irna_top_total = $biaya_irna_top_total + $biaya_irna;// * $jumlah_tindakan;

			}

			$total_irna = $total_irna + $biaya_irna_top_total;


			if($top->kode_tindakan == 'JMO')
			{
				$total_top_jm = 0;
				foreach($rawatInap->trRawatInapTopJm as $item)
				{

					$total_top_jm = $total_top_jm + $item->nilai;
				}
			}

			else if($top->kode_tindakan == "JMA")
			{
				$total_top_ja = 0;
				foreach($rawatInap->trRawatInapTopJa as $item)
				{

					$total_top_ja = $total_top_ja + $item->nilai;
				}

			}

			$i++;
		}

		$listTnop = TindakanMedisNonOperatif::model()->findAll();
		$i = 0;

		foreach($listTnop as $tnop)
		{
			$attr = array(
				'id_rawat_inap' => $rawatInap->id_rawat_inap,
				'id_tnop' => $tnop->id_tindakan
			);
			$itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);


			$jumlah_tindakan = 1;
			$biaya_irna = 0;
			$biaya_ird = 0;
			$biaya_irna_tnop_total = 0;

			if(!empty($itemTnop)){
				$jumlah_tindakan = $itemTnop->jumlah_tindakan;
				$biaya_irna = $itemTnop->biaya_irna;
				$biaya_ird = $itemTnop->biaya_ird;

				$biaya_irna_tnop_total = $biaya_irna_tnop_total + $biaya_irna;// * $jumlah_tindakan;
			}

			$total_ird = $total_ird + $biaya_ird;
			$total_irna = $total_irna + $biaya_irna_tnop_total;
			$jml_tnop_irna = 0 ;
			if($tnop->kode_tindakan == 'jrm')
			{ 
				$t = 0;
				foreach($rawatInap->trRawatInapTnopJrm as $item)
				{
					$t = $t + $item->nilai;
				}

				$jml_tnop_irna = $t;
			}

			else if($tnop->kode_tindakan == 'jm')
			{ 
				$t = 0;
				foreach($rawatInap->trRawatInapTnopJm as $item)
				{
					$t = $t + $item->nilai;
				}

			$jml_tnop_irna = $t;
			}

			else if($tnop->kode_tindakan == 'japel')
			{ 
				$t = 0;
				foreach($rawatInap->trRawatInapTnopJapel as $item)
				{
					$t = $t + $item->nilai;
				}

				$jml_tnop_irna = $t;
			}

			$i++;
		}

		$urutan = 1;
		$biaya_supp_irna = 0;
		foreach($rawatInap->trRawatInapPenunjangs as $supp)
		{

			$total_ird = $total_ird + $supp->biaya_ird;
			// $total_irna = $total_irna + $supp->biaya_irna;
			$biaya_supp_irna = $biaya_supp_irna + $supp->biaya_irna;// * $supp->jumlah_tindakan; 

		}

		$total_irna = $total_irna + $biaya_supp_irna;

		$listObat = ObatAlkes::model()->findAll();
		$i = 0;
		foreach($listObat as $obat){
			$attr = array(
				'id_rawat_inap' => $rawatInap->id_rawat_inap,
				'id_alkes' => $obat->id_obat_alkes
			);
			$itemObat = TrRawatInapAlkes::model()->findByAttributes($attr);

			$jumlah_tindakan = 1;
			$biaya_irna = $obat->tarip;
			$biaya_ird = 0;

			$biaya_obat_ird_total = 0;
			$biaya_obat_irna_total = 0;
			if(!empty($itemObat)){
				$jumlah_tindakan = $itemObat->jumlah_tindakan;
				$biaya_irna = $itemObat->biaya_irna;
				$biaya_ird = $itemObat->biaya_ird;
				$biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird;
				$biaya_obat_irna_total = $biaya_obat_irna_total + $biaya_irna ;//* $jumlah_tindakan;
			}

			$total_ird = $total_ird + $biaya_obat_ird_total;
			$total_irna = $total_irna + $biaya_obat_irna_total;


			$i++;
		}

		 $listLain = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>$rawatInap->kamar->kelas_id));
		$i = 0;
		foreach($listLain as $lain){
			$attr = array(
				'id_rawat_inap' => $rawatInap->id_rawat_inap,
				'id_lain' => $lain->id_tindakan
			);

			$itemLain = TrRawatInapLain::model()->findByAttributes($attr);
			$jumlah_tindakan = 1;
			$biaya_irna = $lain->tarip;
			$biaya_ird = 0;

			$biaya_irna_lain_total = 0;
			if(!empty($itemLain)){
				$jumlah_tindakan = $itemLain->jumlah_tindakan;
				$biaya_irna = $itemLain->biaya_irna;
				$biaya_ird = $itemLain->biaya_ird;
				$biaya_irna_lain_total = $biaya_irna_lain_total + $itemLain->biaya_irna;// * $jumlah_tindakan;
			}

			$total_ird = $total_ird + $biaya_ird;
			$total_irna = $total_irna + $biaya_irna_lain_total;
			$i++;
		}

		$subtotal = $total_ird + $total_irna;

		$nilai_total = 0;

		

		if(strpos($rawatInap->jenisPasien->NamaGol, 'BPJS') !== false)
		{
			
			$nama_kelas = $rawatInap->kamar->kelas->nama_kelas;
			// Khusus VIP
			if($nama_kelas == 'VIP' && $rawatInap->biaya_paket_1 != 0)
			{

				$selisih_maks = 0;
				// nilai real VIP > 175 % dari tarif INA CBG kelas 1
				if(($subtotal / $rawatInap->biaya_paket_1) > 1.75)
				{

				$selisih_maks = 0.75 * $rawatInap->biaya_paket_1;

				}

			// nilai real VIP < 175 % dari tarif INA CBG kelas 1 dan real > paket 1
				else if(($subtotal / $rawatInap->biaya_paket_1) <= 1.75 && $subtotal >= $rawatInap->biaya_paket_1)
				{
				$selisih_maks = $subtotal - $rawatInap->biaya_paket_1;

				}

				$selisih_inacbg = 0;
				if($rawatInap->biaya_paket_3 != 0)
				{
				$selisih_inacbg = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_3;     
				}

				else if($rawatInap->biaya_paket_2 != 0)
				{
					$selisih_inacbg = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_2;
				}



				$tambahan_pasien = $selisih_maks + $selisih_inacbg;
				$pendapatan_rs = $rawatInap->biaya_paket_1 + $tambahan_pasien;

				$nilai_total = $tambahan_pasien;
			}

			// NON VIP
			else
			{
				if($rawatInap->biaya_paket_1 != 0 && $rawatInap->biaya_paket_3 != 0)
				{
				$selisih_paket = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_3; 
				} 

				else if($rawatInap->biaya_paket_2 != 0 && $rawatInap->biaya_paket_3 != 0)
				{
				$selisih_paket = $rawatInap->biaya_paket_2 - $rawatInap->biaya_paket_3;
				}

				else if($rawatInap->biaya_paket_1 != 0 && $rawatInap->biaya_paket_2 != 0)
				{
				$selisih_paket = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_2;
				}

				else
				{
					$selisih_paket = $subtotal;
				}

				// $nilai_total = $subtotal - $selisih_paket;
					$nilai_total = $selisih_paket;
			}
		}

		else
		{
			$nilai_total = $subtotal;
		}

		$rawatRincian->subtotal = $subtotal;
		$rawatRincian->dibayar = $nilai_total;
		$rawatInap->biaya_dibayar = $nilai_total;
		
		$rawatRincian->save(false,array('subtotal','dibayar'));

		$rawatInap->biaya_total_kamar = $total_irna;
		$rawatInap->biaya_total_ird = $total_ird;
		$rawatInap->save(false,array('biaya_dibayar','biaya_total_ird','biaya_total_kamar'));
	}

	public function actionPulang($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrRawatInap']))
		{
			
		//	$model->attributes=$_POST['TrRawatInap'];
			

			if($this->pasienKeluar($model, $_POST))
			{	
				
				Yii::app()->user->setFlash('success', "Data tersimpan. Pasien telah dinyatakan PULANG.");
				$this->redirect(array('index'));
			
			}

			
		}

		$this->render('pulang',array(
			'model'=>$model,
		));
	}

	public function actionKwitansi($id)
	{
		$rawatInaps = TrRawatInap::model()->findAllByAttributes(array('kode_rawat'=>$id));
		

		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;
		
		if(!empty($rawatInaps))
		{

			foreach($rawatInaps as $ri)
			{
				if($ri->biaya_total_kamar == 0)
				{
					$this->countTotal($ri);
				}
			}
			// $this->render('kwitansi',array(
			// 	'rawatInaps' => $rawatInaps,
			// 	// 'pasien' => $pasien,
			// 	'listDokter' => $listDokter,
			// 	'jenisVisite' => $jenisVisite,
			// ));
			$html = $this->renderPartial('kwitansi',array(
				'rawatInaps' => $rawatInaps,
				// 'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
			),true,true);


			try
			{
				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
	                        'L', 'mm', 'A5', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				
				$pdf->AddPage();
				$pdf->SetAutoPageBreak(TRUE, 0);
				$pdf->SetFont('helvetica', '', 9);
				$pdf->writeHTML($html, true, false, true, false, '');
				//$pdf->WriteHTMLCell(0,0,0,0 ,$html);
				$pdf->Output('kwitansi_rawat_inap.pdf', "I");
				Yii::app()->end();
				ob_end_clean();
			}

			catch(HTML2PDF_exception $e) 
			{
			    echo $e;
			    exit;
			}
		}
	}

	public function actionTotal($id)
	{
		$this->title = 'Tindakan Rawat Inap | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		$this->countTotal($rawatInap);

		if(empty($rawatRincian)){
			$rawatRincian = new TrRawatInapRincian;
			$rawatRincian->id_rawat_inap = $id;
		}

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{

			if(isset($_POST['TrRawatInap']))
			{

				if(!empty($_POST['btn-keluar']))
					$this->pasienKeluar($rawatInap, $_POST);	
				
				$rawatInap->biaya_paket_1 = $_POST['TrRawatInap']['biaya_paket_1'];
				$rawatInap->biaya_paket_2 = $_POST['TrRawatInap']['biaya_paket_2'];
				$rawatInap->biaya_paket_3 = $_POST['TrRawatInap']['biaya_paket_3'];
				$rawatRincian->subtotal = $_POST['subtotal'];
				$rawatRincian->dibayar = $_POST['dibayar'];
				$rawatRincian->id_rawat_inap = $id;
				
				$rawatRincian->save(true,array('subtotal','dibayar','id_rawat_inap'));

				$rawatInap->biaya_total_ird = $_POST['TrRawatInap']['biaya_total_ird'];
				$rawatInap->biaya_total_kamar = $_POST['TrRawatInap']['biaya_total_kamar'];
				$rawatInap->biaya_dibayar = $_POST['TrRawatInap']['biaya_dibayar'];
				$rawatInap->biaya_paket_1 = str_replace(".", "", $rawatInap->biaya_paket_1);
				$rawatInap->biaya_paket_2 = str_replace(".", "", $rawatInap->biaya_paket_2);
				$rawatInap->biaya_paket_3 = str_replace(".", "", $rawatInap->biaya_paket_3);
				Yii::app()->user->setFlash('success', "Data telah tersimpan");
				$rawatInap->save(true,array('biaya_paket_1','biaya_paket_2','biaya_paket_3','biaya_total_kamar','biaya_total_ird','biaya_dibayar'));
			}


			$this->render('total',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}

	public function actionLainnya($id)
	{
		$this->title = 'Tindakan Rawat Inap | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);
		
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{

			// Data Lain-lain
			if(isset($_POST['id_tindakan_lain']))
			{

				$transaction=Yii::app()->db->beginTransaction();
				try
				{
					TrRawatInapLain::model()->deleteAllByAttributes(array('id_rawat_inap'=>$id));
				
					$i = 0;
					foreach($_POST['id_tindakan_lain'] as $item)
					{

						

						$rit = new TrRawatInapLain;
						$rit->id_rawat_inap = $id;
						$rit->id_lain = $_POST['id_tindakan_lain'][$i];
						$rit->biaya_irna = $_POST['biaya_irna_lain'][$i];
						$rit->biaya_ird = $_POST['biaya_ird_lain'][$i];
						$rit->jumlah_tindakan = $_POST['jumlah_rm'][$i];

						$rit->biaya_irna = str_replace(".", "", $rit->biaya_irna);
						$rit->biaya_ird = str_replace(".", "", $rit->biaya_ird);

						if($rit->validate())
						{

							$rit->save();
						}
						else
						{

							$errors = '';
							foreach($rit->getErrors() as $attribute){
								foreach($attribute as $error){
									$errors .= $error.' ';
								}
							}
					

							$rawatInap->addError('ERROR_LAIN',$errors);
							throw new Exception();
						}
					


						$i++;
					}
					Yii::app()->user->setFlash('success', "Data telah tersimpan");
					$transaction->commit();

					$this->redirect(array('trRawatInap/lainnya','id'=>$id));
				}

				catch(Exception $e){
					// Yii::app()->user->setFlash('error', print_r($e));
					$transaction->rollback();
				}	
			}


			$this->render('lainnya',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}


	public function actionAlkes($id)
	{
		$this->title = 'Tindakan Rawat Inap | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);
		

		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{

			// Data Obat dan Alkes
			if(isset($_POST['id_tindakan_obat']))
			{

				$transaction=Yii::app()->db->beginTransaction();
				try
				{
					TrRawatInapAlkes::model()->deleteAllByAttributes(array('id_rawat_inap'=>$id));
				
					$i = 0;
					foreach($_POST['id_tindakan_obat'] as $item)
					{



						$rit = new TrRawatInapAlkes;
						$rit->id_rawat_inap = $id;
						$rit->id_alkes = $_POST['id_tindakan_obat'][$i];
						$rit->biaya_irna = $_POST['biaya_irna_alkes'][$i];
						$rit->biaya_ird = $_POST['biaya_ird_alkes'][$i];
						$rit->jumlah_tindakan = 1;

						// 
						$rit->biaya_irna = str_replace(".", "", $rit->biaya_irna);
						$rit->biaya_ird = str_replace(".", "", $rit->biaya_ird);


						if($rit->validate())
						{

							$rit->save();
						}
						else
						{

							$errors = '';
							foreach($rit->getErrors() as $attribute){
								foreach($attribute as $error){
									$errors .= $error.' ';
								}
							}
					

							$rawatInap->addError('ERROR_ALKES',$errors);
							throw new Exception();
						}
					


						$i++;
					}	
					// exit;
					Yii::app()->user->setFlash('success', "Data telah tersimpan");
					$transaction->commit();

					$this->redirect(array('trRawatInap/alkes','id'=>$id));
				}

				catch(Exception $e){
					// Yii::app()->user->setFlash('error', print_r($e));
					$transaction->rollback();
				}	
			}

			$this->render('alkes',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}


	public function actionPenunjang($id)
	{
		$this->title = 'Tindakan Rawat Inap | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);
		
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$listSupp = TindakanMedisPenunjang::model()->findAll();
		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{

			// Data Layanan PEnunjang
			if(!empty($_POST))
			{

				$transaction=Yii::app()->db->beginTransaction();
				try
				{
					TrRawatInapPenunjang::model()->deleteAllByAttributes(array('id_rawat_inap'=>$id));
			
					$i = 0;
					$idxPj = 0;
					foreach($listSupp as $item)
					{
						
						$b_ird = $_POST['biaya_ird_supp'][$i];
						$b_irna = $_POST['biaya_irna_supp'][$i];


						$rit = new TrRawatInapPenunjang;
						if($item->nama_tindakan == 'USG'){
							$rit->dokter_id = !empty($_POST['dokter_pj_tindakan'][0]) ? $_POST['dokter_pj_tindakan'][0] : 1;
						}

						else if($item->nama_tindakan == 'EKG'){
							$rit->dokter_id = !empty($_POST['dokter_pj_tindakan'][1]) ? $_POST['dokter_pj_tindakan'][1] : 1;
						}

						else if($item->nama_tindakan == 'PA'){
							$rit->dokter_id = !empty($_POST['dokter_pj_tindakan'][2]) ? $_POST['dokter_pj_tindakan'][2] : 1;
						}
						
						// print_r($_POST['dokter_pj_tindakan']);

						
						$rit->id_rawat_inap = $id;
						$rit->id_penunjang = $item->id_tindakan_penunjang;
						$rit->biaya_irna = $b_irna;
						$rit->biaya_ird = $b_ird;
						$rit->jumlah_tindakan = 1;//$_POST['jumlah_tindakan_supp'][$i];

						$rit->biaya_ird = str_replace(".", "", $rit->biaya_ird);
						$rit->biaya_irna = str_replace(".", "", $rit->biaya_irna);
						

						if($rit->validate())
						{


							$rit->save();
						}
						else
						{

							$errors = '';
							foreach($rit->getErrors() as $attribute){
								foreach($attribute as $error){
									$errors .= $error.' ';
								}
							}
					

							$rawatInap->addError('ERROR_SUPP',$errors);

							throw new Exception();
						}

						$i++;
					}
					Yii::app()->user->setFlash('success', "Data telah tersimpan");
					$transaction->commit();

					$this->redirect(array('trRawatInap/penunjang','id'=>$id));
				}

				catch(Exception $e){
					// Yii::app()->user->setFlash('error', print_r($e));
					$transaction->rollback();
				}	
			}

			$this->render('penunjang',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}

	public function actionMedikTNOP($id)
	{
		$this->title = 'Tindakan Rawat Inap | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);

		$this->countTotal($rawatInap);
		
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{

			if(isset($_POST['id_tindakan_tnop']))
			{
				$transaction=Yii::app()->db->beginTransaction();
				try
				{
					// Data Tindakan Non Operatif
					TrRawatInapTnop::model()->deleteAllByAttributes(array('id_rawat_inap'=>$id));
					
					$i = 0;
					foreach($_POST['id_tindakan_tnop'] as $item)
					{

						$rit = new TrRawatInapTnop;
						$rit->id_rawat_inap = $id;
						$rit->id_tnop = $_POST['id_tindakan_tnop'][$i];
						$rit->biaya_irna = $_POST['biaya_irna_tnop'][$i];
						$rit->biaya_ird = $_POST['biaya_ird_tnop'][$i];
						$rit->jumlah_tindakan = 1;

						$rit->biaya_ird = str_replace(".", "", $rit->biaya_ird);
						$rit->biaya_irna = str_replace(".", "", $rit->biaya_irna);


						if($rit->validate())
						{

							$rit->save();
						}
						else
						{

							$errors = '';
							foreach($rit->getErrors() as $attribute){
								foreach($attribute as $error){
									$errors .= $error.' ';
								}
							}
					

							$rawatInap->addError('ERROR_TNOP',$errors);
							throw new Exception();
						}
					
						


						$i++;
					}	
						

					
					Yii::app()->user->setFlash('success', "Data telah tersimpan");
					$transaction->commit();

					$this->redirect(array('trRawatInap/medikTNOP','id'=>$id));
				}

				catch(Exception $e){
					// Yii::app()->user->setFlash('error', print_r($e));
					$transaction->rollback();
				}
			}

			$this->render('medikTNOP',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}

	public function actionMedikTOP($id)
	{
		$this->title = 'Tindakan Rawat Inap | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);

		$this->countTotal($rawatInap);
		
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{
			if(isset($_POST['id_tindakan_top']))
			{
				$transaction=Yii::app()->db->beginTransaction();
				try
				{
					// Data Tindakan Operatif
					TrRawatInapTop::model()->deleteAllByAttributes(array('id_rawat_inap'=>$id));
				
					$i = 0;
					foreach($_POST['id_tindakan_top'] as $item)
					{

					

						$rit = new TrRawatInapTop;
						$rit->id_rawat_inap = $id;

						// if(!empty($_POST['id_dokter_jm'][$i]))
						// 	$rit->id_dokter_jm = $_POST['id_dokter_jm'][$i];
						
						// if(!empty($_POST['id_dokter_anas'][$i]))
						// 	$rit->id_dokter_anas = $_POST['id_dokter_anas'][$i];

						$rit->id_top = $_POST['id_tindakan_top'][$i];
						$rit->biaya_irna = $_POST['biaya_irna_top'][$i];
						$rit->biaya_ird = 0;
						$rit->jumlah_tindakan =1;			

						$rit->biaya_irna = str_replace(".", "", $rit->biaya_irna);

						if($rit->validate())
						{
							$rit->save();
						}
						else
						{

							$errors = '';
							foreach($rit->getErrors() as $attribute){
								foreach($attribute as $error){
									$errors .= $error.' ';
								}
							}
					

							$rawatInap->addError('ERROR_TOP',$errors);
							throw new Exception();
						}



						$i++;
					}	

					
					Yii::app()->user->setFlash('success', "Data telah tersimpan");
					$transaction->commit();

					$this->redirect(array('trRawatInap/medikTOP','id'=>$id));
				}

				catch(Exception $e){
					// Yii::app()->user->setFlash('error', print_r($e));
					$transaction->rollback();
				}
			}

			$this->render('medikTOP',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}

	public function actionPelMedik($id)
	{
		$this->title = 'Tindakan Rawat Inap | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);

		$this->countTotal($rawatInap);
		
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{

			if(isset($_POST['TrRawatInapRincian']))
			{

				if(isset($_POST['TrRawatInap'])){
					$rawatInap->is_tarif_kamar_penuh = $_POST['TrRawatInap']['is_tarif_kamar_penuh'];
					$rawatInap->save(false, ['is_tarif_kamar_penuh']);
				}
			
				$rawatRincian->id_rawat_inap = $id;
				$rawatRincian->attributes = $_POST['TrRawatInapRincian'];
				$rawatRincian->askep_kamar = $_POST['TrRawatInapRincian']['askep_kamar'];
				$rawatRincian->asuhan_nutrisi = $_POST['TrRawatInapRincian']['asuhan_nutrisi'];
				
				$rawatRincian->jumlah_askep = 1;//$_POST['TrRawatInapRincian']['jumlah_askep'];
				$rawatRincian->jumlah_asnut = $_POST['TrRawatInapRincian']['jumlah_asnut'];
			
				$rawatRincian->obs_ird = str_replace(".", "", $rawatRincian->obs_ird);
				$rawatRincian->asuhan_nutrisi = str_replace(".", "", $rawatRincian->asuhan_nutrisi);
				$rawatRincian->askep_kamar = str_replace(".", "", $rawatRincian->askep_kamar);
				$rawatRincian->biaya_pengawasan = str_replace(".", "", $rawatRincian->biaya_pengawasan);

				$isneonatus = $rawatInap->kamar->nama_kamar == 'NEONATUS';
   
			    if($isneonatus)
			    {
			    	$rawatInap->biaya_kamar = str_replace(".", "", $_POST['biaya_per_malam']);
			    	$rawatInap->save(false,array('biaya_kamar'));
			    }

				if($rawatRincian->validate())
				{

					$transaction=Yii::app()->db->beginTransaction();
					try
					{
						// Data Visite
						TrRawatInapVisiteDokter::model()->deleteAllByAttributes(array('id_rawat_inap'=>$id));
						if(isset($_POST['id_jenis_visite']))
						{
							$i = 0;

							foreach($_POST['id_jenis_visite'] as $item)
							{

								if(!empty($item))
								{
									$rvd = new TrRawatInapVisiteDokter;
									$rvd->id_rawat_inap = $id;
									$rvd->id_jenis_visite = $_POST['id_jenis_visite'][$i];
									$rvd->id_dokter = $_POST['id_dokter_visite_ird'][$i];
									$rvd->id_dokter_irna = $_POST['id_dokter_visite_irna'][$i];
									$rvd->jumlah_visite = $_POST['jumlah_visite'][$i];
									$rvd->jumlah_visite_ird = $_POST['jumlah_visite_ird'][$i];
									$rvd->biaya_visite = $_POST['biaya_visite_ird'][$i];
									$rvd->biaya_visite_irna = $_POST['biaya_visite_irna'][$i];

									$rvd->biaya_visite = str_replace(".", "", $rvd->biaya_visite);
									$rvd->biaya_visite_irna = str_replace(".", "", $rvd->biaya_visite_irna);

									if($rvd->validate())
									{
										$rvd->save();
									}

									else
									{
										$errors = '';
										foreach($rvd->getErrors() as $attribute){
											foreach($attribute as $error){
												$errors .= $error.' ';
											}
										}
								
										$rawatInap->addError('ERROR_VISITE',$errors);
										throw new Exception();
									}
								}
								$i++;
							}	
						}

						$rawatRincian->save();
						Yii::app()->user->setFlash('success', "Data telah tersimpan");
						$transaction->commit();

						$this->redirect(array('TrRawatInap/pelMedik','id'=>$id));
					}

					catch(Exception $e){
						// Yii::app()->user->setFlash('error', print_r($e));
						$transaction->rollback();
					}
				}
			}

			$rawatRincian->obs_ird = Yii::app()->helper->formatRupiah($rawatRincian->obs_ird);
			$rawatRincian->asuhan_nutrisi = Yii::app()->helper->formatRupiah($rawatRincian->asuhan_nutrisi);
			$rawatRincian->askep_kamar = Yii::app()->helper->formatRupiah($rawatRincian->askep_kamar);
			$rawatRincian->biaya_pengawasan = Yii::app()->helper->formatRupiah($rawatRincian->biaya_pengawasan);

			$this->render('pelMedik',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}

	private function pasienKeluar($model, $param)
	{

		$is_saved = false;
		$model->datetime_pulang = $param['TrRawatInap']['datetime_keluar'];



		if(!empty($model->datetime_pulang))
		{
			
			$datetime_pulang = explode(' ',$model->datetime_pulang);
			$tgl_pulang = $datetime_pulang[0];
			$jam_pulang = $datetime_pulang[1];
			$model->tanggal_pulang = $tgl_pulang;
			$model->jam_pulang = $jam_pulang;

			$model->tanggal_keluar = $tgl_pulang;
			$model->jam_keluar = $jam_pulang;
			$model->datetime_keluar = $tgl_pulang.' '.$jam_pulang;
		}
		
		// status inap = 2 -> pulang
		$model->status_inap = 2;

		

		$is_saved = true;
		$model->save(true,array('tanggal_pulang','status_inap','jam_pulang','datetime_pulang','tanggal_keluar','jam_keluar','datetime_keluar'));
			
		

		return $is_saved;
	}


	public function actionDataPasien($id)
	{
		$this->title = 'Tindakan Rawat Inap | '.Yii::app()->name;
		$rawatInap = TrRawatInap::model()->findByPk($id);

		$this->countTotal($rawatInap);
		
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatInap))
		{


			if(isset($_POST['TrRawatInap']))
			{

				if(!empty($_POST['btn-keluar']))
				{

					$this->pasienKeluar($rawatInap, $_POST);
					
				}


				$id_dokter = $_POST['TrRawatInap']['dokter_id'];
				$rawatInap->dokter_id = $id_dokter;
				$rawatInap->save(true,array('dokter_id'));

				$attr = array(
					'kode_visite' => 'VISITE',
					'kelas_tingkat' => $rawatInap->kamar->kelas_id
				);
				$jenisVisite = JenisVisite::model()->findByAttributes($attr);

				// print_r($jenisVisite);exit;
				if(!empty($jenisVisite))
				{

					$attr = array(
						'id_jenis_visite' => $jenisVisite->id_jenis_visite,
						'id_dokter_irna' => $id_dokter,
						'id_rawat_inap' => $rawatInap->id_rawat_inap
					);
					

					$rvd = TrRawatInapVisiteDokter::model()->findByAttributes($attr);

					// 
					if(empty($rvd)){

						$rvd = new TrRawatInapVisiteDokter;
					}

					$rvd->id_rawat_inap = $id;
					$rvd->id_jenis_visite = $jenisVisite->id_jenis_visite;
					$rvd->id_dokter_irna = $id_dokter;
					$rvd->jumlah_visite = 1;
					$rvd->jumlah_visite_ird = 1;
					$rvd->biaya_visite = 0;
					$rvd->biaya_visite_irna = $jenisVisite->biaya;

					$rvd->save();
				}

				Yii::app()->user->setFlash('success', "Data telah tersimpan");
		
				$this->redirect(array('TrRawatInap/dataPasien','id'=>$id));
			
			}

			$this->render('dataPasien',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		
		}
	}

	public function actionCetakKwitansi($id)
	{
		

		$rawatInap = TrRawatInap::model()->findByPk($id);

		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;


		if(!empty($rawatInap))
		{


			$html = $this->renderPartial('cetakKwitansi',array(
				'model'=>$model,
				'rawatInap' => $rawatInap,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			),true,true);

			try
			{
				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
	                        'P', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				
				$pdf->AddPage();
				$pdf->SetAutoPageBreak(TRUE, 0);
				$pdf->SetFont('helvetica', '', 9);
				$pdf->writeHTML($html, true, false, true, false, '');
				//$pdf->WriteHTMLCell(0,0,0,0 ,$html);
				$pdf->Output('kwitansi_rawat_inap.pdf', "I");
				Yii::app()->end();
				ob_end_clean();
			}

			catch(HTML2PDF_exception $e) 
			{
			    echo $e;
			    exit;
			}
		}
	
	}


	public function actionLaporan()
	{

		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}

		

		$this->render('dokter',array(
			'model'=>$model,
		));
	}

	public function actionKeluarIrd($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrRawatInap']))
		{
		//	$model->attributes=$_POST['TrRawatInap'];
			$model->datetime_keluar_ird = $_POST['TrRawatInap']['datetime_keluar_ird'];
			if(!empty($model->datetime_keluar_ird))
			{
				$datetime_keluar_ird = explode(' ',$model->datetime_keluar_ird);
				$tgl_keluar = $datetime_keluar_ird[0];
				$jam_keluar = $datetime_keluar_ird[1];
				$model->tanggal_keluar_ird = $tgl_keluar;
				$model->jam_keluar_ird = $jam_keluar;
			}
			


			if($model->validate())
			{	
				$model->save(true,array('tanggal_keluar_ird','jam_keluar_ird','datetime_keluar_ird'));
				Yii::app()->user->setFlash('success', "Data tersimpan. Pasien telah dinyatakan keluar IRD");
				$this->redirect(array('index'));
			
			}

			
		}

		$this->render('keluarIrd',array(
			'model'=>$model,
		));
	}

	public function actionPindah($id,$kode)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrRawatInap']))
		{
		//	$model->attributes=$_POST['TrRawatInap'];
			$model->datetime_keluar = $_POST['TrRawatInap']['datetime_keluar'];

			$kamar_id = $_POST['TrRawatInap']['kamar_id'];

			if($kamar_id == $model->kamar_id)
			{
				Yii::app()->user->setFlash('error', "Silakan Pilih Kamar yang berbeda dengan sebelumnya");
				$this->redirect(array('trRawatInap/pindah','id'=>$id,'kode'=>$kode));
			}

			if(!empty($model->datetime_keluar))
			{
				$datetime_keluar = explode(' ',$model->datetime_keluar);
				$tgl_keluar = $datetime_keluar[0];
				$jam_keluar = $datetime_keluar[1];
				$model->tanggal_keluar = $tgl_keluar;
				$model->jam_keluar = $jam_keluar;
			
	
				$modelNew = new TrRawatInap;
				
				$dtm = explode(' ',$_POST['TrRawatInap']['datetime_keluar']);
				$tgm = $dtm[0];
				$jmm = $dtm[1];
	
				
				$var = $tgm;
				$date = str_replace('/', '-', $var);
				$modelNew->datetime_masuk = $tgm.' '.$jmm;
				
				if(!empty($modelNew->datetime_masuk))
				{
					$datetime_masuk = explode(' ',$modelNew->datetime_masuk);
					$tgl_masuk = $datetime_masuk[0];
					$jam_masuk = $datetime_masuk[1];

					
					$modelNew->tanggal_masuk = $tgl_masuk;

					$modelNew->jam_masuk = $jam_masuk;
					//$modelNew->datetime_masuk = $model->tanggal_masuk.' '.$jam_masuk;
				}
				
				$model->status_inap = 0;

				if($model->validate())
				{	
					$modelNew->kamar_id = $_POST['TrRawatInap']['kamar_id'];
					$model->next_kamar = $modelNew->kamar_id;
					$attr = array(
						'status_inap','tanggal_keluar','jam_keluar','datetime_keluar','next_kamar'
					);
					$model->save(true,$attr);

					$modelNew->pasien_id = $_POST['TrRawatInap']['pasien_id'];
					
					$modelNew->jenis_pasien = $_POST['TrRawatInap']['jenis_pasien'];
					$modelNew->kode_rawat = $kode;
					$modelNew->next_kamar = 0;
					$modelNew->prev_kamar = $model->kamar_id;
					$modelNew->is_naik_kelas = $_POST['TrRawatInap']['is_naik_kelas'];

					if($modelNew->validate())
						$modelNew->save();
					else{
						// print_r($modelNew->getErrors());exit;
						Yii::app()->user->setFlash('error', var_dump($modelNew->getErrors()));
						$this->redirect(array('trRawatInap/pindah','id'=>$id,'kode'=>$kode));
					}

					


					Yii::app()->user->setFlash('success', "Data tersimpan. Pasien telah dinyatakan keluar dari kamar.");
					$this->redirect(array('index'));
				
				}
			}

			else if(empty($_POST['TrRawatInap']['datetime_keluar']))
			{
				Yii::app()->user->setFlash('error', "Tanggal Pindah harus diisi");
				$this->redirect(array('trRawatInap/pindah','id'=>$id,'kode'=>$kode));
			}

			
		}

		$this->render('pindah',array(
			'model'=>$model,
		));
	}

	public function actionTindakan($id)
	{

		$rawatInap = TrRawatInap::model()->findByPk($id);
		$model = new TrRawatInapLayanan;
		$rawatRincian = $rawatInap->trRawatInapRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatInapRincian;

		$pasien = $rawatInap->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter',function($dokter){
			$gelar_depan = $dokter->gelar_depan;
			$gelar_blkg = $dokter->gelar_belakang;
			$nama_tengah = $dokter->nama_tengah;
			$nama_blkg = $dokter->nama_belakang;

			$full = $gelar_depan.'. '.$dokter->nama_depan;
				
			if(!empty($nama_belakang))
				$full .= ' '.$nama_belakang;

			if(!empty($gelar_blkg))
				$full .= ', '.$gelar_blkg;

			return CHtml::encode($full);
		});

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;
		
		$this->render('tindakan',array(
			'model'=>$model,
			'rawatInap' => $rawatInap,
			'pasien' => $pasien,
			'listDokter' => $listDokter,
			'jenisVisite' => $jenisVisite,
			'rawatRincian' => $rawatRincian
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($rm = 0, $jp = 0)
	{
		$model=new TrRawatInap;

		if(!empty($rm))
			$model->pasien_id = $rm;

		if(!empty($jp))
			$model->jenis_pasien = $jp;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrRawatInap']))
		{
			$model->attributes=$_POST['TrRawatInap'];


			if(!empty($model->datetime_masuk))
			{


				$datetime_masuk = explode(' ',$model->datetime_masuk);
				$tgl_masuk = $datetime_masuk[0];
				$jam_masuk = $datetime_masuk[1];

				$var = $tgl_masuk;
				$date = str_replace('/', '-', $var);
				


				$model->tanggal_masuk = date('Y-m-d', strtotime($date));

				$model->jam_masuk = $jam_masuk;
				$model->datetime_masuk = $model->tanggal_masuk.' '.$jam_masuk;


				// ########################### IRD
				if(!empty($_POST['TrRawatInap']['datetime_masuk_ird']))
				{

					$model->jenis_ird = $_POST['TrRawatInap']['jenis_ird'];
					
					$model->datetime_masuk_ird = $_POST['TrRawatInap']['datetime_masuk_ird'];
					$datetime_masuk_ird = explode(' ',$model->datetime_masuk_ird);
					
					$tgl_masuk_ird = $datetime_masuk_ird[0];
					$jam_masuk_ird = $datetime_masuk_ird[1];

					$var_ird = $tgl_masuk_ird;
					$date_ird = str_replace('/', '-', $var_ird);
					

					$model->tanggal_masuk_ird = date('Y-m-d', strtotime($date_ird));

					$model->jam_masuk_ird = $jam_masuk_ird;
					$model->datetime_masuk_ird = $model->tanggal_masuk_ird.' '.$jam_masuk_ird;
				}
				

			}

			$transaction=Yii::app()->db->beginTransaction();
			try{

				$tdPasienMasuk = TdPasienMasuk::model()->findByAttributes([
					'Tgl_MRS' => $model->tanggal_masuk,
					'No_RM' => $model->pasien_id
				]);

				if(empty($tdPasienMasuk))
				{
					$tdPasienMasuk = TdPasienMasuk::model()->findByAttributes([
						'tgl_pindah' => $model->tanggal_masuk,
						'No_RM' => $model->pasien_id
					]);
				}

				if(empty($tdPasienMasuk)){
					$tdPasienMasuk = TdPasienMasuk::model()->findByAttributes([
						
						'No_RM' => $model->pasien_id
					],['order'=>'tgl_pindah DESC']);	
				}



				
				if(!empty($tdPasienMasuk)){
					$tdPasienMasuk->No_RM = $model->pasien_id;

					$tdPasienMasuk->save(false, ['No_RM']);
					$data = Yii::app()->db->createCommand()
				    ->select('NODAFTAR, NoMedrec, TGLDAFTAR, JamDaftar')
				    ->from('b_pendaftaran b')
				    
				    ->where('b.NoMedrec='.$model->pasien_id)
				    ->limit(1)
				    ->order(['b.NODAFTAR DESC'])
				    ->queryRow();
					
					$data = (object)$data;
					$result = [];

					if(!empty($data->NoMedrec)){
						
						$model->kode_rawat = $data->NODAFTAR;
					} 

				}


				if($model->validate())
				{	
					
					$model->save();


					// $obat = new TrRawatInapAlkesObat;
					// $obat->id_rawat_inap= $model->id_rawat_inap;
					// $obat->keterangan = 'Gelang';
					// $obat->nilai = 1800;
					// $obat->kode_alkes = 'BHP';
					// $obat->tanggal_input = date('Y-m-d');
					// $obat->id_dokter = 1;
					// if(!$obat->save()){
					// 	print_r($obat->getErrors());
					// 	exit;
					// }

					$dataOk = TdRegisterOk::model()->findByAttributes([
						'kode_daftar'=>$model->kode_rawat,
						'no_rm' => $model->pasien_id
					]);

					if(!empty($dataOk))
					{
						// $ja = new TrRawatInapTopJa;
						// $ja->id_rawat_inap= $model->id_rawat_inap;
						// $ja->id_dokter = $dataOk->dr_anastesi;
						// $ja->nilai = 0;
						// if(!$ja->save()){
						// 	print_r($ja->getErrors());
						// 	exit;
						// }		
						$item = $dataOk->tdOkBiayas;
						// foreach($dataOk->tdOkBiayas as $item)
						// {
						
						$jm = new TrRawatInapTopJm;
						$jm->nilai = $item->biaya_dr_op;
						$jm->id_rawat_inap= $model->id_rawat_inap;
						$jm->id_dokter = $item->dr_operator;
						$jm->save();

						if(!$jm->save()){
							print_r($jm->getErrors());
							exit;
						}

						
						$ja = new TrRawatInapTopJa;

						$ja->nilai = $item->biaya_dr_anas;
						$ja->id_rawat_inap= $model->id_rawat_inap;
						$ja->id_dokter = $dataOk->dr_anastesi;
						
						$ja->save();

						if(!$ja->save()){
							print_r($ja->getErrors());
							exit;
						}

						


						$listTindakan = array(
							'BHP'=>$item->biaya_bhn,
							'JRS'=>$item->biaya_jrs,
							'JPO'=>$item->biaya_prwt_ok,
							'JPA'=>$item->biaya_prwt_anas,
							'JMA'=> $item->biaya_dr_anas,
							'JMO'=> $item->biaya_dr_op
						);

						foreach($listTindakan as $q => $v)
						{
							$jsPrwtOk = TindakanMedisOperatif::model()->findByAttributes(array('kode_tindakan'=>$q));

							$rit = new TrRawatInapTop;
							$rit->id_rawat_inap = $model->id_rawat_inap;
							$rit->id_top = $jsPrwtOk->id_tindakan;
							$rit->biaya_irna = $v;
							$rit->biaya_ird = 0;
							$rit->jumlah_tindakan =1;			
							if(!$rit->save()){
								print_r($rit->getErrors());
								exit;
							}

							
						}

						// }
					}

					$dataResep = TrResepPasien::model()->findByAttributes([
						'nodaftar_id'=>$model->kode_rawat,
						'pasien_id' => $model->pasien_id
					]);

					if(!empty($dataResep)){
						foreach($dataResep->trResepKamars as $items):
							foreach($items->trResepKamarItems as $item)
							{
								$obat = MObatAkhp::model()->findByPk($item->obat_id);
							
								$m = new TrRawatInapAlkesObat;
								$m->id_rawat_inap= $model->id_rawat_inap;
								$m->keterangan = $obat->nama_barang;
								$m->nilai = str_replace(".", "", $item->harga);
								$m->kode_alkes = 'OBAT';
								$m->id_dokter = $items->dokter_id;
								$m->tanggal_input = date('Y-m-d');
								$m->resep_non_racik_id = $item->id;
								
								if(!$m->save()){
									print_r($m->getErrors());
									exit;
								}

								
							
								TrRawatInapAlkes::model()->updateTotalObat($model->id_rawat_inap);
							}

							foreach($items->trResepKamarRacikans as $item)
							{
								$obat = TrObatRacikan::model()->findByPk($item->tr_obat_racikan_id);
							
								$m = new TrRawatInapAlkesObat;
								$m->id_rawat_inap= $model->id_rawat_inap;
								$m->keterangan = $obat->nama;
								$m->nilai = str_replace(".", "", $item->harga);
								$m->kode_alkes = 'OBAT';
								$m->id_dokter = $items->dokter_id;
								$m->tanggal_input = date('Y-m-d');
								$m->resep_racik_id = $item->id;
								if(!$m->save()){
									print_r($m->getErrors());
									exit;
								}

								
								
								TrRawatInapAlkes::model()->updateTotalObat($model->id_rawat_inap);
							}
						endforeach;
					}

					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data tersimpan");
					$this->redirect(array('trRawatInap/dataPasien','id'=>$model->id_rawat_inap));
				
				}
			}

			catch(Exception $e)
			{
				// print_r($e);exit;
				$transaction->rollback();
			}

	
			
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrRawatInap']))
		{
			$model->attributes=$_POST['TrRawatInap'];
			
			// $model->tanggal_keluar = date('Y-m-d', strtotime(str_replace('/', '-', $model->tanggal_keluar)));

			if(!empty($model->datetime_masuk))
			{


				$datetime_masuk = explode(' ',$model->datetime_masuk);
				$tgl_masuk = $datetime_masuk[0];
				$jam_masuk = $datetime_masuk[1];

				$var = $tgl_masuk;
				$date = str_replace('/', '-', $var);

				$model->tanggal_masuk = date('Y-m-d', strtotime($date));

				$model->jam_masuk = $jam_masuk;
				$model->datetime_masuk = $model->tanggal_masuk.' '.$jam_masuk;

				
				// ########################### IRD
				if(!empty($_POST['TrRawatInap']['datetime_masuk_ird']))
				{


					$model->datetime_masuk_ird = $_POST['TrRawatInap']['datetime_masuk_ird'];
					$datetime_masuk_ird = explode(' ',$model->datetime_masuk_ird);

					$model->jenis_ird = $_POST['TrRawatInap']['jenis_ird'];
					
					$tgl_masuk_ird = $datetime_masuk_ird[0];
					$jam_masuk_ird = $datetime_masuk_ird[1];

					$var_ird = $tgl_masuk_ird;
					$date_ird = str_replace('/', '-', $var_ird);
					

					$model->tanggal_masuk_ird = date('Y-m-d', strtotime($date_ird));

					$model->jam_masuk_ird = $jam_masuk_ird;
					$model->datetime_masuk_ird = $model->tanggal_masuk_ird.' '.$jam_masuk_ird;
				}
				

			}

			if($model->validate())
			{	

				$tdPasienMasuk = TdPasienMasuk::model()->findByAttributes([
					'Tgl_MRS' => $model->tanggal_masuk,
					'No_RM' => $model->pasien_id
				]);

				if(empty($tdPasienMasuk))
				{
					$tdPasienMasuk = TdPasienMasuk::model()->findByAttributes([
						'tgl_pindah' => $model->tanggal_masuk,
						'No_RM' => $model->pasien_id
					]);
				}

				if(empty($tdPasienMasuk)){
					$tdPasienMasuk = TdPasienMasuk::model()->findByAttributes([
						
						'No_RM' => $model->pasien_id
					],['order'=>'tgl_pindah DESC']);	
				}

				// print_r($model->pasien_id);exit;
				if(!empty($tdPasienMasuk)){
					$tdPasienMasuk->No_RM = $model->pasien_id;
					$tdPasienMasuk->save(false, ['No_RM']);
					$data = Yii::app()->db->createCommand()
				    ->select('NODAFTAR, NoMedrec, TGLDAFTAR, JamDaftar')
				    ->from('b_pendaftaran b')
				    
				    ->where('b.NoMedrec='.$model->pasien_id)
				    ->limit(1)
				    ->order(['b.NODAFTAR DESC'])
				    ->queryRow();
					
					$data = (object)$data;
					$result = [];
					if(!empty($data->NoMedrec)){

						// $tgl_masuk = $data->TGLDAFTAR;
						// $jam_masuk = $data->JamDaftar;

						// $model->tanggal_masuk = $tgl_masuk;
						// $model->jam_masuk = $jam_masuk;
						// $model->datetime_masuk = $model->tanggal_masuk.' '.$jam_masuk;
						$model->kode_rawat = $data->NODAFTAR;
					} 
				}

				$model->save();

				// print_r($model->tanggal_masuk);exit;
				Yii::app()->user->setFlash('success', "Data tersimpan");
				$this->redirect(array('index'));
			
			}

			else{
				print_r($model->getErrors());exit;
			}
			
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		$model->status_pasien = 0; // tidak aktif
		$model->save(false,array('status_pasien'));

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

		$this->actionAdmin();
		// if(isset($_POST['id_rawat_inap']))
		// {
		// 	$this->redirect(array('dataPasien','id'=>$_POST['id_rawat_inap']));
		// }

		// $this->render('index');
	}

	public function actionDatarawat()
	{

		$this->title = 'Rawat Inap | '.Yii::app()->name;

		$model=new TrRawatInap('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		

		if(isset($_GET['TrRawatInap']))
			$model->attributes=$_GET['TrRawatInap'];

		$this->render('datarawat',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

		$this->title = 'Rawat Inap | '.Yii::app()->name;

		$model=new TrRawatInap('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['jenis_pasien']))
			$model->TIPE_PASIEN=$_GET['jenis_pasien'];

		if(isset($_GET['kamar_id']))
			$model->kamar_id=$_GET['kamar_id'];

		if(isset($_GET['search_by']))
			$model->SEARCH_BY=$_GET['search_by'];

		if(isset($_GET['TrRawatInap']))
			$model->attributes=$_GET['TrRawatInap'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TrRawatInap the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TrRawatInap::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TrRawatInap $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tr-rawat-inap-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
