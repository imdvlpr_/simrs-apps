<?php

class BpjsSepController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

		$hasil = Yii::app()->rest->getDetailSep($id);
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'hasil' => $hasil
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BpjsSep;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BpjsSep']))
		{
			$model->attributes=$_POST['BpjsSep'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->NoSEP));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->title = 'Update Data SEP | '.Yii::app()->name;
		$model=$this->loadModel($id);

		$listPisat = array(
				1 => 'Peserta',
				2 => 'Suami',
				3 => 'Istri',
				4 => 'Anak',
				5 => 'Tambahan'
			);


		$bpjsSep = $model;

		$pasien = Pasien::model()->findByPk($bpjsSep->NO_RM);
		$bpjsPasien = BpjsPasien::model()->findByAttributes(array('PASIEN_ID'=>$bpjsSep->NO_RM));


		// $bpjsPasien = new BpjsPasien;
		$bpjsData = null;
		$riwayatTerakhir = null;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BpjsSep']))
		{
			$bpjsSep->attributes = $_POST['BpjsSep'];

			
			
			$jenisRawat = '';
			switch ($bpjsSep->JENIS_RAWAT) {
				case 0:
					$jenisRawat = 'IGD';
					break;
				case 1:
					$jenisRawat = 'Rawat Inap';
					break;
				case 2:
					$jenisRawat = 'Rawat Jalan';
				
			}


			$kelasoptions = array(
        		'1' => 'Kelas I',
        		'2' => 'Kelas II',
        		'3' => 'Kelas III'
        	);

			$request = array(
				'noSep' 		=> $bpjsSep->NoSEP,
				'noKartu' 		=> $_POST['BpjsPasien']['NoKartu'],
				'tglSep' 		=> $bpjsSep->TGL_SEP,
				'tglRujukan'	=> $bpjsSep->TGL_RUJUKAN,
				'noRujukan'		=> $bpjsSep->NO_RUJUKAN,
				'ppkRujukan'	=> $_POST['ASAL_RUJUKAN'],
				'ppkPelayanan'	=> Yii::app()->params->kodeppk,
				'jnsPelayanan'	=> $bpjsSep->JENIS_RAWAT,
				'catatan'		=> $bpjsSep->CATATAN,
				'diagAwal'		=> $bpjsSep->DIAG_AWAL,
				'poliTujuan'	=> $bpjsSep->POLI_TUJUAN,
				'klsRawat'		=> $bpjsSep->KELAS_RAWAT,
				'lakaLantas'	=> $bpjsSep->LAKA_LANTAS,
				'lokasiLaka'    => $bpjsSep->lokasi_laka,
				'user'			=> Yii::app()->user->getState('username'),
				'noMr'			=> $bpjsPasien->PASIEN_ID

			);



			// print_r($request);exit;
				
			$result = Yii::app()->rest->updateSEP($request);


			if($result->metadata->code == '200')
			{
				
				$bpjsSep->NO_RM = $request['noMr'];
				if($bpjsSep->validate())
				{	
					$bpjsSep->save();

					Yii::app()->user->setFlash('success', "Data tersimpan..");
					$this->redirect(array('update','id'=>$id));
				}

				
			}
			else
			{

				$bpjsSep->addError('RESPONSE_ERROR',$result->metadata->code.' '.$result->metadata->message);
					
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'pasien' => $pasien,
			'bpjsPasien' => $bpjsPasien,
			'bpjsData' => $bpjsData,
			'bpjsSep' => $bpjsSep,
			'riwayatTerakhir' => $riwayatTerakhir,
			'jenisRawat' => $model->JENIS_RAWAT
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		Yii::app()->rest->hapusSEP($id);
				
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('BpjsSep');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BpjsSep('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['BpjsSep']))
			$model->attributes=$_GET['BpjsSep'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BpjsSep the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BpjsSep::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BpjsSep $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bpjs-sep-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
