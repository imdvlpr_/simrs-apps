<?php

class TrPendaftaranRjalanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','input','laporan','cetakKwitansi','cetakTreser','lama','updateRJ','bpjs','bpjsUpdate','bpjsLama','cariBpjsLama','cariPasien'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionBpjsLama($jenis, $no_rm)
	{

		$model=new TrPendaftaranRjalan;
		$bpjsPasien = BpjsPasien::model()->findByAttributes(array('PASIEN_ID'=>$no_rm));

		$pasien = Pasien::model()->findByPk($no_rm);
		
		$model->NoMedrec = $pasien->NoMedrec;

		$model->NoDaftar = Yii::app()->helper->generateNoDaftar();
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$golpasien = GolPasien::model()->findByPk($jenis);
		$model->GolPasien = !empty($golpasien) ? $golpasien->KodeGol : '';
		if(isset($_POST['TrPendaftaranRjalan']))
		{
			$transaction=Yii::app()->db->beginTransaction();
			try
			{


				$model->attributes=$_POST['TrPendaftaranRjalan'];
				$pasien->attributes=$_POST['Pasien'];
				$bpjsPasien->attributes=$_POST['BpjsPasien'];
				$bpjsPasien->PASIEN_ID = $pasien->NoMedrec;
				$bpjsPasien->NAMA_KELASRAWAT = $_POST['KELASRAWAT_NAMA'];
				
				$k_last = Yii::app()->db->createCommand()
			    ->select('MAX(NO_KUNJUNGAN) as no_kunjungan')
			    ->from('kunjungan t')
			    ->queryRow();

			    $k_last = (object)$k_last;


				$time = strtotime($model->WAKTU_DAFTAR);
				$tgl_daftar = date("Y-m-d", $time);


				$tgllahir = $pasien->TGLLAHIR;

				$k_pasien_sebelum = Yii::app()->helper->getKunjunganTerakhir($model->NoMedrec);
				$sql = "insert into kunjungan (NO_KUNJUNGAN, no_daftar,NO_RM, no_rm_lama, nama, tgl_lahir, ALAMAT, GOL_PASIEN, TGL_DAFTAR, TGL_KRS, T1, T2, T3, tgl_kunj_sebelumnya, BARU_LAMA, status_timur, status_barat, post_mrs) values (:p1, :p2, :p3, :p4, :p5, :p6, :p7, :p8, :p9, :p10, :p11, :p12, :p13, :p14, :p15, :p16, :p17, :p18)";

				
				$parameters = array(
					':p1'=>$k_last->no_kunjungan+1,
					':p2'=>$model->NoDaftar,
					':p3'=>$model->NoMedrec,
					':p4'=>$model->NoMedrec,
					':p5'=>$pasien->NAMA,
					':p6'=>$tgllahir,
					':p7'=>$pasien->ALAMAT,
					':p8'=>$golpasien->NamaGol,
					':p9'=>$tgl_daftar,
					':p10'=>$tgl_daftar,
					':p11'=>'-',
					':p12'=>'-',
					':p13'=>'-',
					':p14'=>!empty($k_pasien_sebelum) && !empty($k_pasien_sebelum->TGL_DAFTAR) ? $k_pasien_sebelum->TGL_DAFTAR : $tgl_daftar,
					':p15'=>!empty($k_pasien_sebelum) ? 'LAMA' : 'BARU',
					':p16'=>1,
					':p17'=>1,
					':p18'=>$model->PostMRS

				);
				Yii::app()->db->createCommand($sql)->execute($parameters);				

				if($model->save()){
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data tersimpan");
					$this->redirect(array('bpjs/sepPopup','kartu'=>$bpjsPasien->NoKartu,'jr'=>2,'idPasien'=>$pasien->NoMedrec));
				}

			}
			catch(Exception $e){
				throw new Exception($e);
			    $transaction->rollback();
		    }	
		}

		$model->WAKTU_DAFTAR = date('Y-m-d H:i:s');

		$this->render('bpjsLama',array(
			'bpjsPasien'=>$bpjsPasien,
			'model'=>$model,
			'pasien' => $pasien,
			'golpasien' => $golpasien,
		));
	}

	public function actionCariBpjsLama()
	{

		if(!empty($_POST['cari_no_rm']))
		{


			$no_rm = $_POST['cari_no_rm'];
			$this->redirect(array('bpjsLama','jenis'=>111,'no_rm'=>$no_rm));
		}

		else
		{
			$this->render('index');
		}
	}

	public function actionBpjsUpdate($jenis, $id, $norm)
	{
		$model=TrPendaftaranRjalan::model()->findByPk($id);
		$bpjsPasien = BpjsPasien::model()->findByAttributes(array('PASIEN_ID'=>$norm));


		$pasien = Pasien::model()->findByPk($norm);
		// print_r($id);exit;
		$model->NoDaftar = Yii::app()->helper->generateNoDaftar();
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$golpasien = GolPasien::model()->findByPk($jenis);
		$model->GolPasien = !empty($golpasien) ? $golpasien->KodeGol : '';
		if(isset($_POST['TrPendaftaranRjalan']))
		{
			$transaction=Yii::app()->db->beginTransaction();
			try
			{


				$model->attributes=$_POST['TrPendaftaranRjalan'];
				$pasien->attributes=$_POST['Pasien'];
				$bpjsPasien->attributes=$_POST['BpjsPasien'];
				$bpjsPasien->PASIEN_ID = $pasien->NoMedrec;
					

				if($pasien->save() && $model->save() && $bpjsPasien->save()){
					$transaction->commit();
					$this->redirect(array('index'));
				}

			}
			catch(Exception $e){
				throw new Exception($e);
			    $transaction->rollback();
		    }	
		}

		$model->WAKTU_DAFTAR = date('Y-m-d H:i:s');

		$this->render('bpjsUpdate',array(
			'bpjsPasien'=>$bpjsPasien,
			'model'=>$model,
			'pasien' => $pasien,
			'golpasien' => $golpasien,
		));
	}

	public function actionBpjs($jenis)
	{
		$model=new TrPendaftaranRjalan;
		$bpjsPasien = new BpjsPasien;


		$pasien = new Pasien;
		$pasien->NoMedrec = Yii::app()->helper->getMinimumUnusedID();
		$model->NoMedrec = $pasien->NoMedrec;

		$model->NoDaftar = Yii::app()->helper->generateNoDaftar();
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$golpasien = GolPasien::model()->findByPk($jenis);
		$model->GolPasien = !empty($golpasien) ? $golpasien->KodeGol : '';
		if(isset($_POST['TrPendaftaranRjalan']))
		{
			$transaction=Yii::app()->db->beginTransaction();
			try
			{


				$model->attributes=$_POST['TrPendaftaranRjalan'];
				$pasien->attributes=$_POST['Pasien'];
				$bpjsPasien->attributes=$_POST['BpjsPasien'];
				$bpjsPasien->PASIEN_ID = $pasien->NoMedrec;
				$bpjsPasien->NAMA_KELASRAWAT = $_POST['KELASRAWAT_NAMA'];
				
				$k_last = Yii::app()->db->createCommand()
			    ->select('MAX(NO_KUNJUNGAN) as no_kunjungan')
			    ->from('kunjungan t')
			    ->queryRow();

			    $k_last = (object)$k_last;


				$time = strtotime($model->WAKTU_DAFTAR);
				$tgl_daftar = date("Y-m-d", $time);


				$tgllahir = $pasien->TGLLAHIR;

				$k_pasien_sebelum = Yii::app()->helper->getKunjunganTerakhir($model->NoMedrec);
				$sql = "insert into kunjungan (NO_KUNJUNGAN, no_daftar,NO_RM, no_rm_lama, nama, tgl_lahir, ALAMAT, GOL_PASIEN, TGL_DAFTAR, TGL_KRS, T1, T2, T3, tgl_kunj_sebelumnya, BARU_LAMA, status_timur, status_barat, post_mrs) values (:p1, :p2, :p3, :p4, :p5, :p6, :p7, :p8, :p9, :p10, :p11, :p12, :p13, :p14, :p15, :p16, :p17, :p18)";

				
				$parameters = array(
					':p1'=>$k_last->no_kunjungan+1,
					':p2'=>$model->NoDaftar,
					':p3'=>$model->NoMedrec,
					':p4'=>$model->NoMedrec,
					':p5'=>$pasien->NAMA,
					':p6'=>$tgllahir,
					':p7'=>$pasien->ALAMAT,
					':p8'=>$golpasien->NamaGol,
					':p9'=>$tgl_daftar,
					':p10'=>$tgl_daftar,
					':p11'=>'-',
					':p12'=>'-',
					':p13'=>'-',
					':p14'=>!empty($k_pasien_sebelum) && !empty($k_pasien_sebelum->TGL_DAFTAR) ? $k_pasien_sebelum->TGL_DAFTAR : $tgl_daftar,
					':p15'=>!empty($k_pasien_sebelum) ? 'LAMA' : 'BARU',
					':p16'=>1,
					':p17'=>1,
					':p18'=>$model->PostMRS

				);
				Yii::app()->db->createCommand($sql)->execute($parameters);				

				if($pasien->save() && $model->save() && $bpjsPasien->save()){
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data tersimpan");
					$this->redirect(array('bpjs/sepPopup','kartu'=>$bpjsPasien->NoKartu,'jr'=>2,'idPasien'=>$pasien->NoMedrec));
				}

			}
			catch(Exception $e){
				throw new Exception($e);
			    $transaction->rollback();
		    }	
		}

		$model->WAKTU_DAFTAR = date('Y-m-d H:i:s');

		$this->render('bpjsCreate',array(
			'bpjsPasien'=>$bpjsPasien,
			'model'=>$model,
			'pasien' => $pasien,
			'golpasien' => $golpasien,
		));
	}

	public function actionUpdateRJ($id)
	{
		$model=TrPendaftaranRjalan::model()->findByPk($id);

		$pasien = $model->pASIEN;
		$date = DateTime::createFromFormat('Y-m-d', $pasien->TGLLAHIR);
		$pasien->TGLLAHIR = $date->format('d/m/Y');
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$golpasien = $model->gOLPASIEN;
		$model->GolPasien = !empty($golpasien) ? $golpasien->KodeGol : '';
		if(isset($_POST['TrPendaftaranRjalan']))
		{
			$transaction=Yii::app()->db->beginTransaction();
			try
			{
				$model->attributes=$_POST['TrPendaftaranRjalan'];
				$pasien->attributes=$_POST['Pasien'];
				$model->poli2_id = $_POST['TrPendaftaranRjalan']['poli2_id'];
				$model->poli3_id = $_POST['TrPendaftaranRjalan']['poli3_id'];

				$date = DateTime::createFromFormat('d/m/Y', $pasien->TGLLAHIR);
				$tgllahir = $date->format('Y-m-d');

				$pasien->TGLLAHIR = $tgllahir;

				$k_pasien_sebelum = Yii::app()->helper->getKunjunganTerakhir($model->NoMedrec);
				$sql = "update kunjungan SET GOL_PASIEN = :p1 WHERE no_daftar = :p2";
				$parameters = array(
					':p1'=>$golpasien->NamaGol,
					':p2'=>$model->NoDaftar,
				);
				Yii::app()->db->createCommand($sql)->execute($parameters);				

				if($pasien->save() && $model->save()){
					$transaction->commit();
					$this->redirect(array('index'));
				}

			}
			catch(Exception $e){
				throw new Exception($e);
			    $transaction->rollback();
		    }	
		}

		
		$this->render('updateRJ',array(
			'model'=>$model,
			'pasien' => $pasien,
			'golpasien' => $golpasien,
		));
	}

	public function actionCariPasien($jenis=''){
		if(!empty($_GET['cari_no_rm'])){
			$pasien = Pasien::model()->findByPk($_GET['cari_no_rm']);
			if(!empty($pasien)){
				$this->redirect(['TrPendaftaranRjalan/lama','jenis'=>$jenis,'norm'=>$_GET['cari_no_rm']]);
			}
			else{
				Yii::app()->user->setFlash('danger', "Data Pasien Tidak Ditemukan");
				$this->redirect(['TrPendaftaranRjalan/cariPasien','jenis'=>$jenis]);
			}
		}
		$golpasien = GolPasien::model()->findByPk($jenis);
		$this->render('cari_pasien',array(
			// 'model'=>$model,
			// 'pasien' => $pasien,
			'golpasien' => $golpasien,
				// 'rjalan' =>$rjalan,
				// 'alamat' => $alamat
		));
	}

	

	public function actionLaporan()
	{

		$model=new TrPendaftaranRjalan;
		$ird = new TrRawatInap;

		if(!empty($_POST['TrPendaftaranRjalan']['TANGGAL_AWAL']) && !empty($_POST['TrPendaftaranRjalan']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrPendaftaranRjalan']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrPendaftaranRjalan']['TANGGAL_AKHIR']);

			$ird->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrPendaftaranRjalan']['TANGGAL_AWAL']);
			$ird->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrPendaftaranRjalan']['TANGGAL_AKHIR']);
		}



		$this->render('laporan',array(
			'model'=>$model,
			'ird' => $ird
		));
	}

	public function actionCetakKwitansi($id)
	{
		$rawatJalan= TrPendaftaranRjalan::model()->findByPk($id);
		
		$rawatRincian = $rawatJalan->trRawatJalanRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatJalanRincian;

		$pasien = $rawatJalan->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatJalan))
		{

			$html = $this->renderPartial('cetakKwitansi',array(
				'rawatJalan' => $rawatJalan,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			),true,true);

			try
			{

				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
	                        'P', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				
				$pdf->AddPage();
				$pdf->SetAutoPageBreak(TRUE, 0);
				$pdf->SetFont('helvetica', '', 8);
				$pdf->writeHTML($html, true, false, true, false, '');
				//$pdf->WriteHTMLCell(0,0,0,0 ,$html);
				$pdf->Output('kwitansi_rawat_jalan.pdf', "I");
				Yii::app()->end();

				ob_end_clean();
			}

			catch(HTML2PDF_exception $e) 
			{
			    echo $e;
			    exit;
			}
		}
	}

	public function actionInput($id)
	{
		$rawatJalan= TrPendaftaranRjalan::model()->findByPk($id);
		
		$rawatRincian = $rawatJalan->trRawatJalanRincians;

		if(empty($rawatRincian))
			$rawatRincian = new TrRawatJalanRincian;

		$pasien = $rawatJalan->pASIEN;
		$listDokter = CHtml::listData(DmDokter::model()->findAll(),'id_dokter','FULLNAME');

		$listVisite = CHtml::listData(JenisVisite::model()->findAll(),'id_jenis_visite','nama_jenis_visite');
		$jenisVisite = $listVisite;

		if(!empty($rawatJalan))
		{

			if(isset($_POST['TrRawatJalanRincian']))
			{
				$rawatJalan->dokter_id = $_POST['TrPendaftaranRjalan']['dokter_id'];
				$rawatJalan->biaya_paket_1 = $_POST['TrPendaftaranRjalan']['biaya_paket_1'];
				$rawatJalan->biaya_paket_2 = $_POST['TrPendaftaranRjalan']['biaya_paket_2'];
				$rawatJalan->save(true,array('dokter_id','biaya_paket_1','biaya_paket_2'));
				$rawatRincian->id_rawat_jalan = $id;
				$rawatRincian->attributes = $_POST['TrRawatJalanRincian'];


				if($rawatRincian->validate())
				{

					$transaction=Yii::app()->db->beginTransaction();
					try{

						// Data Visite
						TrRawatJalanVisiteDokter::model()->deleteAllByAttributes(array('id_rawat_jalan'=>$id));
						if(isset($_POST['id_jenis_visite']))
						{
							$i = 0;

							foreach($_POST['id_jenis_visite'] as $item)
							{

								if(!empty($item))
								{
									$rvd = new TrRawatJalanVisiteDokter;
									$rvd->id_rawat_jalan = $id;
									$rvd->id_jenis_visite = $_POST['id_jenis_visite'][$i];
									$rvd->id_dokter = $_POST['id_dokter_visite_ird'][$i];

									$rvd->jumlah_visite_ird = $_POST['jumlah_visite_ird'][$i];
									$rvd->biaya_visite = $_POST['biaya_visite_ird'][$i];

									if($rvd->validate())
									{
										$rvd->save();
									}

									else
									{
										$errors = '';
										foreach($rvd->getErrors() as $attribute){
											foreach($attribute as $error){
												$errors .= $error.' ';
											}
										}
								
										$rawatJalan->addError('ERROR_VISITE',$errors);
										throw new Exception();
									}
								}
								$i++;
							}	

						}

						

						// Data Tindakan Non Operatif
						TrRawatJalanTnop::model()->deleteAllByAttributes(array('id_rawat_jalan'=>$id));
						if(isset($_POST['id_tindakan_tnop']))
						{
							$i = 0;
							foreach($_POST['id_tindakan_tnop'] as $item)
							{

								$rit = new TrRawatJalanTnop;
								$rit->id_rawat_jalan = $id;
								$rit->id_tnop = $_POST['id_tindakan_tnop'][$i];

								$rit->biaya_ird = $_POST['biaya_ird_tnop'][$i];
								$rit->jumlah_tindakan = $_POST['jumlah_tindakan_tnop'][$i];


								if($rit->validate())
								{

									$rit->save();
								}
								else
								{

									$errors = '';
									foreach($rit->getErrors() as $attribute){
										foreach($attribute as $error){
											$errors .= $error.' ';
										}
									}
							

									$rawatJalan->addError('ERROR_TNOP',$errors);
									throw new Exception();
								}
							
								


								$i++;
							}	
						}

						// Data Layanan PEnunjang
						TrRawatJalanPenunjang::model()->deleteAllByAttributes(array('id_rawat_jalan'=>$id));
						if(isset($_POST['id_jenis_supp']))
						{
							$i = 0;
							foreach($_POST['id_jenis_supp'] as $item)
							{

								if(!empty($item)){
									$rit = new TrRawatJalanPenunjang;
									$rit->id_rawat_jalan = $id;
									$rit->id_penunjang = $_POST['id_jenis_supp'][$i];

									$rit->biaya_ird = $_POST['biaya_ird_supp'][$i];
									$rit->jumlah_tindakan = $_POST['jumlah_tindakan_supp'][$i];


									if($rit->validate())
									{

										$rit->save();
									}
									else
									{

										$errors = '';
										foreach($rit->getErrors() as $attribute){
											foreach($attribute as $error){
												$errors .= $error.' ';
											}
										}
								

										$rawatJalan->addError('ERROR_SUPP',$errors);
										throw new Exception();
									}
								}

								$i++;
							}	
						}

						// Data Obat dan Alkes
						TrRawatJalanAlkes::model()->deleteAllByAttributes(array('id_rawat_jalan'=>$id));
						if(isset($_POST['id_tindakan_obat']))
						{
							$i = 0;
							foreach($_POST['id_tindakan_obat'] as $item)
							{


								$rit = new TrRawatJalanAlkes;
								$rit->id_rawat_jalan = $id;
								$rit->id_alkes = $_POST['id_tindakan_obat'][$i];
								$rit->biaya_ird = $_POST['biaya_ird_alkes'][$i];
								$rit->jumlah_tindakan = $_POST['jumlah_obat'][$i];


								if($rit->validate())
								{

									$rit->save();
								}
								else
								{

									$errors = '';
									foreach($rit->getErrors() as $attribute){
										foreach($attribute as $error){
											$errors .= $error.' ';
										}
									}
							

									$rawatJalan->addError('ERROR_ALKES',$errors);
									throw new Exception();
								}
							


								$i++;
							}	
						}

						// Data Lain-lain
						TrRawatJalanLain::model()->deleteAllByAttributes(array('id_rawat_jalan'=>$id));
						if(isset($_POST['id_tindakan_lain']))
						{
							$i = 0;
							foreach($_POST['id_tindakan_lain'] as $item)
							{

								

								$rit = new TrRawatJalanLain;
								$rit->id_rawat_jalan = $id;
								$rit->id_lain = $_POST['id_tindakan_lain'][$i];
								$rit->biaya_ird = $_POST['biaya_ird_lain'][$i];
								$rit->jumlah_tindakan = $_POST['jumlah_rm'][$i];


								if($rit->validate())
								{

									$rit->save();
								}
								else
								{

									$errors = '';
									foreach($rit->getErrors() as $attribute){
										foreach($attribute as $error){
											$errors .= $error.' ';
										}
									}
							

									$rawatJalan->addError('ERROR_LAIN',$errors);
									throw new Exception();
								}
							


								$i++;
							}	
						}

						$rawatRincian->save();
						Yii::app()->user->setFlash('success', "Data telah tersimpan");
						$transaction->commit();

						$this->redirect(array('TrPendaftaranRjalan/input','id'=>$id));
					}

					catch(Exception $e){
						Yii::app()->user->setFlash('error', print_r($e->errorInfo));
						$transaction->rollback();
					}
				}


			}

			$this->render('input',array(
				'rawatJalan' => $rawatJalan,
				'pasien' => $pasien,
				'listDokter' => $listDokter,
				'jenisVisite' => $jenisVisite,
				'rawatRincian' => $rawatRincian
			));
		}
	}
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	private function savePasien($model, $pasien, $rjalan){
		$transaction=Yii::app()->db->beginTransaction();
		try
		{
			$model->attributes=$_POST['BPendaftaran'];
			$model->KodePtgs = 1;
			$model->TGLDAFTAR = date("Y-m-d", strtotime($model->TGLDAFTAR));
			$pasien->attributes=$_POST['Pasien'];
			$pasien->AGAMA = $_POST['Pasien']['AGAMA'];
			$pasien->STATUSPERKAWINAN = $_POST['Pasien']['STATUSPERKAWINAN'];
			// $pasien->AGAMA = $_POST['Pasien']['AGAMA'];
			// print_r($_POST['Pasien']);exit;
			$pasien->TGLLAHIR = date("Y-m-d", strtotime($pasien->TGLLAHIR));
			$rjalan->attributes=$_POST['BPendaftaranRjalan'];
			
			if($model->isNewRecord)
				$model->KunjunganKe = $model->countPasien()+1;
			
			$pasien->JamInput = date('Y-m-d H:i:s');

			$pasien->ALAMAT = $pasien->jalan.' '.$pasien->rt.'/'.$pasien->rw.' '.(!empty($pasien->Desa) ? $pasien->Desa : '');


			if($pasien->validate() && $model->validate() && $rjalan->validate())
			{
				
				Yii::app()->helper->syncBankRM();
				
				$pasien->save();
		
				$model->save();
				$rjalan->NoDaftar = $model->NODAFTAR;
				
				
				$rjalan->save();
				$transaction->commit();
				$this->redirect(array('bPendaftaranRjalan/index'));
					
				
					
				
			}
			else{
				// print_r($model->getErrors());
				// print_r($rjalan->getErrors());
				// print_r($alamat->getErrors());
				// print_r($pasien->getErrors());
				// exit;
			}
			

		}
		catch(Exception $e){

			throw new Exception($e);

		    $transaction->rollback();
	    }	
	}

	public function actionLama($jenis='',$norm)
	{


		// $model->NoDaftar = Yii::app()->helper->generateNoDaftar();
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$golpasien = GolPasien::model()->findByPk($jenis);

		$no_rm = ltrim($norm, '0');

		$pasien = Pasien::model()->findByPk($no_rm);

		$model=new BPendaftaran;
		$model->KodeGol = $jenis;
		$rjalan = new BPendaftaranRjalan;
		
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['BPendaftaran']))
		{
			
			$model->NoMedrec = $no_rm;
			$model->KodePtgs = 1;
			$model->TGLDAFTAR = date("Y-m-d", strtotime($model->TGLDAFTAR));
			$usia = Yii::app()->helper->hitungUmurFull($pasien->TGLLAHIR);
			$model->umurthn = $usia['y'];
			$model->umurbln = $usia['m'];
			$model->UmurHr = $usia['d'];
			$this->savePasien($model, $pasien, $rjalan);

				

		}

		$model->TGLDAFTAR = date('Y-m-d');
		$model->JamDaftar = date('H:i:s');

		$this->render('lama',array(
			'model'=>$model,
			'pasien' => $pasien,
			'golpasien' => $golpasien,
			'rjalan' =>$rjalan,
		));
	}
	

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($jenis='')
	{
		$model=new BPendaftaran;
		$rjalan = new BPendaftaranRjalan;

		$pasien = new Pasien;
		
		$pasien->NoMedrec = Yii::app()->helper->getMinimumUnusedID();
		$model->NoMedrec = $pasien->NoMedrec;
		

		// $model->NoDaftar = Yii::app()->helper->generateNoDaftar();
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$golpasien = GolPasien::model()->findByPk($jenis);
		// $model->GolPasien = !empty($golpasien) ? $golpasien->KodeGol : '';
		if(isset($_POST['BPendaftaran']))
		{
			$usia = Yii::app()->helper->hitungUmurFull($pasien->TGLLAHIR);
			$model->umurthn = $usia['y'];
			$model->umurbln = $usia['m'];
			$model->UmurHr = $usia['d'];
			$this->savePasien($model, $pasien, $rjalan);
			
		}

		$model->TGLDAFTAR = date('Y-m-d');
		$model->JamDaftar = date('H:i:s');


		$this->render('create',array(
			'model'=>$model,
			'pasien' => $pasien,
			'golpasien' => $golpasien,
			'rjalan' =>$rjalan,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{


		$model=BPendaftaran::model()->findByPk($id);
		$rjalan = BPendaftaranRjalan::model()->findByAttributes(['NoDaftar'=>$id]);;

		$pasien = Pasien::model()->findByPk($model->NoMedrec);
		
		$model->NoMedrec = $pasien->NoMedrec;
		
		// $model->NoDaftar = Yii::app()->helper->generateNoDaftar();
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$golpasien = GolPasien::model()->findByPk($model->KodeGol);
		// $model->GolPasien = !empty($golpasien) ? $golpasien->KodeGol : '';
		if(isset($_POST['BPendaftaran']))
		{
			$this->savePasien($model, $pasien, $rjalan);
			
		}

		$model->TGLDAFTAR = date('Y-m-d');
		$model->JamDaftar = date('H:i:s');

		$this->render('update',array(
			'model'=>$model,
			'pasien' => $pasien,
			'golpasien' => $golpasien,
			'rjalan' =>$rjalan,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	public function actionCetakTreser()
	{
		$param = (object)$_POST['param'];
		$kunjungan = Kunjungan::model()->findByAttributes(array('no_daftar'=>$param->id));



		if(!empty($kunjungan))
		{
			$transaction=Yii::app()->db->beginTransaction();
			try
			{
				$printer = $param->printer;
				

				switch ($printer) {
					case 0:
						$kunjungan->status_timur = '1';

						$kunjungan->save(false,array('status_timur'));
						
						break;
					case 1 :
						$kunjungan->status_barat = '1';
						$kunjungan->save(false,array('status_barat'));
						break;
					default:
						# code...
						break;
				}

				$transaction->commit();
				$msg = array(
					'code' => 200,
					'message' => 'Success'
				);

				echo json_encode($msg);
			}
			catch(Exception $e){
				print_r($e);
				// $msg = array(
				// 	'code' => 1001,
				// 	'message' => 'Password Salah'
				// );

				// echo json_encode($msg);				
			    $transaction->rollback();
		    }		

			
		}

		else
		{
			$msg = array(
				'code' => 1004,
				'message' => 'Tidak Ada Pasien Dengan Kode Daftar Ini'
			);

			echo json_encode($msg);
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

		$this->title = 'Rawat Jalan | '.Yii::app()->name;
		$model=new TrPendaftaranRjalan('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['poli']))
			$model->POLI=$_GET['poli'];

		if(isset($_GET['TrPendaftaranRjalan']))
			$model->attributes=$_GET['TrPendaftaranRjalan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TrPendaftaranRjalan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TrPendaftaranRjalan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TrPendaftaranRjalan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tr-pendaftaran-rjalan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
