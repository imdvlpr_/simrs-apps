<?php

class TrRawatJalanTindakanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','admin','printBukti','printPengantar'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionPrintBukti($id)
	{
		$model = $this->loadModel($id);
		try
		{
			$pasien = $model->trRawatJalan->noMedrec;
			$reg = $model->trRawatJalan->reg;
			$regJalan = $model->trRawatJalan->regJalan;
			$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
	                        'P', 'mm', 'A4', true, 'UTF-8');

			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
			$page_format = array(
			    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 95, 'ury' => 215),
			);
			$pdf->SetMargins(3, 2, 3, false);
			$pdf->AddPage('L', $page_format);
			
			$this->layout = '';
			ob_start();
			
			echo $this->renderPartial('_printBukti',[
				'model'=>$model,
				'pasien' => $pasien,
				'reg' => $reg,
				'regJalan' => $regJalan
			]);

			$data = ob_get_clean();
			ob_start();
			$pdf->writeHTML($data);

			$pdf->Output("pengantar_".$model->kode_trx.".pdf", "I");

		}
		catch(HTML2PDF_exception $e) {
		    echo $e;
		    exit;
		}
	}

	public function actionPrintPengantar($id)
	{
		$model = $this->loadModel($id);
		try
		{
			$pasien = $model->trRawatJalan->noMedrec;
			$reg = $model->trRawatJalan->reg;
			$regJalan = $model->trRawatJalan->regJalan;
			$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
	                        'P', 'mm', 'A4', true, 'UTF-8');

			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
			$page_format = array(
			    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 75, 'ury' => 130),
			);
			$pdf->SetMargins(3, 2, 3, false);
			$pdf->AddPage('P', $page_format);
			
			$this->layout = '';
			ob_start();
			
			echo $this->renderPartial('_printPengantar',[
				'model'=>$model,
				'pasien' => $pasien,
				'reg' => $reg,
				'regJalan' => $regJalan
			]);

			$data = ob_get_clean();
			ob_start();
			$pdf->writeHTML($data);

			$pdf->Output("pengantar_".$model->kode_trx.".pdf", "I");

		}
		catch(HTML2PDF_exception $e) {
		    echo $e;
		    exit;
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new TrRawatJalanTindakan;

		$model->tr_rawat_jalan_id = $id;

		$unitUser = !empty(Yii::app()->user->getState('unit')) ? Yii::app()->user->getState('unit') : 0;

		$listTindakan = UnitTindakan::model()->findAllByAttributes(['unit_id'=>$unitUser]);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$criteria = new CDbCriteria;
		$criteria->join = "JOIN a_unit_dokter ut ON ut.dokter_id = t.id_dokter";
		$criteria->condition = "ut.unit_id = ".$unitUser;
		$listDokter = CHtml::listData(DmDokter::model()->findAll($criteria),'id_dokter','FULLNAME');

		if(isset($_POST['TrRawatJalanTindakan']))
		{
			$model->kode_trx = 'PL'.Yii::app()->helper->appendZeros($unitUser.date('Ymdhis'),10);
			$model->attributes=$_POST['TrRawatJalanTindakan'];
			
			if($model->save())
			{
				$params = [
					'nama' => $model->trRawatJalan->reg->noMedrec->NAMA,
					'status_bayar' => 0,
                    'kode_trx' => $model->kode_trx,
                    'trx_date' => date('Ymdhis'),
                    'jenis_tagihan' => 'TINDAKAN',
                    'person_in_charge' => 'Perawat Poli '.$model->trRawatJalan->unit->NamaUnit,
                    'custid' => $model->trRawatJalan->reg->NoMedrec,
                    'issued_by' => $model->trRawatJalan->unit->NamaUnit,
                    'keterangan' => 'Tagihan Poli : '.$model->trRawatJalan->unit->NamaUnit,
                    'nilai' => $model->nilai,
                    'jenis_customer' => $model->trRawatJalan->reg->kodeGol->NamaGol
                ];
               	Yii::app()->rest->insertTagihan($params);
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(['index','id'=>$id]);
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'listTindakan' =>$listTindakan,
			'listDokter' => $listDokter
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$unitUser = !empty(Yii::app()->user->getState('unit')) ? Yii::app()->user->getState('unit') : 0;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$listTindakan = CHtml::listData(UnitTindakan::model()->findAllByAttributes(['unit_id'=>$unitUser]),'id',function($model){
			return $model->dmTindakan->kode.' - '.$model->dmTindakan->nama;
		});
		$criteria = new CDbCriteria;
		$criteria->join = "JOIN a_unit_dokter ut ON ut.dokter_id = t.id_dokter";
		$criteria->condition = "ut.unit_id = ".$unitUser;
		$listDokter = CHtml::listData(DmDokter::model()->findAll($criteria),'id_dokter','FULLNAME');
		if(isset($_POST['TrRawatJalanTindakan']))
		{
			$model->attributes=$_POST['TrRawatJalanTindakan'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(['index','id'=>$id]);
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'listTindakan' =>$listTindakan,
			'listDokter'=>$listDokter
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
		$model=new TrRawatJalanTindakan('search');
		$model->unsetAttributes();  // clear any default values

		$model->tr_rawat_jalan_id = $id;

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['TrRawatJalanTindakan']))
			$model->attributes=$_GET['TrRawatJalanTindakan'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TrRawatJalanTindakan('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['TrRawatJalanTindakan']))
			$model->attributes=$_GET['TrRawatJalanTindakan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TrRawatJalanTindakan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TrRawatJalanTindakan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TrRawatJalanTindakan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tr-rawat-jalan-tindakan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
