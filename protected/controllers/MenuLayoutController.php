<?php

class MenuLayoutController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			['allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			],
			['allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','nestable','ajaxBulkSave'),
				'users'=>array('@'),
			],
			['allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			],
			['deny',  // deny all users
				'users'=>array('*'),
			],
		);
	}

	public function actionAjaxBulkSave()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$postData = $_POST['item'];

			// print_r($postData);exit;
			foreach($postData as $q1 => $m1)
			{
				$m = MenuLayout::model()->findByPk($m1['id']);
				$m->urutan = ($q1+1);
				$m->save();

				if(!empty($m1['children']))
				{
					foreach($m1['children'] as $q2 => $m2)
					{
						$m = MenuLayout::model()->findByPk($m2['id']);
						$m->urutan = ($q2+1);				
						$m->save();	

						if(!empty($m2['children']))
						{
							foreach($m2['children'] as $q3 => $m3)
							{
								$m = MenuLayout::model()->findByPk($m3['id']);
								$m->urutan = ($q3+1);					
								$m->save();

								if(!empty($m3['children']))
								{
									foreach($m3['children'] as $q4 => $m4)
									{
										$m = MenuLayout::model()->findByPk($m4['id']);
										$m->urutan = ($q4+1);					
										$m->save();
									}
								}
							}
						}
					}
				}
			}			

			$result = [
				'code' => 200,
				'message' => 'Menu Updated'
			];
			echo json_encode($result);
		}
	}

	public function actionNestable()
	{
		$listParent = MenuLayout::model()->findAllByAttributes(['level'=>1],['order'=>'urutan']);
		$this->render('nestable',[
			'models'=>$listParent,
		]);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',[
			'model'=>$this->loadModel($id),
		]);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MenuLayout;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$listParent = MenuLayout::model()->findAll();
		if(isset($_POST['MenuLayout']))
		{
			$parent = MenuLayout::model()->findByPk($_POST['MenuLayout']['parent']);

			$model->attributes=$_POST['MenuLayout'];
			$model->level = !empty($parent) ? $parent->level+1 : 1;
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(['index']);
			}
		}

		$this->render('create',[
			'model'=>$model,
			'listParent' => $listParent
		]);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$listParent = MenuLayout::model()->findAll();

		if(isset($_POST['MenuLayout']))
		{
			$model->attributes=$_POST['MenuLayout'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(['index']);
			}
		}

		$this->render('update',[
			'model'=>$model,
			'listParent' => $listParent
		]);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new MenuLayout('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['MenuLayout']))
			$model->attributes=$_GET['MenuLayout'];

		$this->render('index',[
			'model'=>$model,
		]);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MenuLayout('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['MenuLayout']))
			$model->attributes=$_GET['MenuLayout'];

		$this->render('admin',[
			'model'=>$model,
		]);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MenuLayout the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MenuLayout::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MenuLayout $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='menu-layout-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
