<?php

class TdOkBiayaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','export','exportXls','ajaxGetRekap'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}



	public function actionAjaxGetRekap()
	{
		$sd = Yii::app()->helper->convertSQLDate($_POST['sd']);
		$ed = Yii::app()->helper->convertSQLDate($_POST['ed']);
		$url = "/ok/rekap/bulanan";
		$params = [
			'sd' => $sd,'ed'=>$ed
		];


		$list = Yii::app()->rest->getDataApi($url,$params);

		$results = [];
		
		$results['items'] = $list->values;

		$sum_khusus = 0; $sum_besar= 0; $sum_sedang= 0; $sum_kecil= 0; $sum_cito= 0; $sum_elektif= 0; $sum_vip= 0; $sum_kelas1= 0; $sum_kelas2= 0; $sum_kelas3= 0; $sum_la= 0; $sum_sab= 0; $sum_ga= 0; $sum_epi= 0; $sum_per= 0; $sum_bpjs= 0; $sum_umum= 0;
		foreach($list->values as $q => $v)
		{
			$sum_khusus += $v->khusus;
			$sum_besar += $v->besar;
			$sum_sedang += $v->sedang;
			$sum_kecil += $v->kecil;
			$sum_cito += $v->cito;
			$sum_elektif += $v->elektif;
			$sum_vip += $v->vip;
			$sum_kelas1 += $v->kelas1;
			$sum_kelas2 += $v->kelas2;
			$sum_kelas3 += $v->kelas3;
			$sum_la += $v->la;
			$sum_sab += $v->sab;
			$sum_ga += $v->ga;
			$sum_epi += $v->epi;
			$sum_per += $v->per;
			$sum_bpjs += $v->bpjs;
			$sum_umum += $v->umum;
		}

 		$results['total'] = [
 			'khusus' => $sum_khusus,
 			'besar' => $sum_besar,
 			'sedang' => $sum_sedang,
 			'kecil' => $sum_kecil,
 			'cito' => $sum_cito,
 			'elektif' => $sum_elektif,
 			'vip' => $sum_vip,
 			'kelas1' => $sum_kelas1,
 			'kelas2' => $sum_kelas2,
 			'kelas3' => $sum_kelas3,
 			'la' => $sum_la,
 			'sab' => $sum_sab,
 			'ga' => $sum_ga,
 			'epi' => $sum_epi,
 			'per' => $sum_per,
 			'bpjs' => $sum_bpjs,
 			'umum' => $sum_umum
 		];

		echo json_encode($results);
	}

	public function actionExportXls($sd, $ed, $upf, $dokter)
	{
		$model = new TdOkBiaya;
		$cdb = new CDbCriteria;
        $cdb->join = 'JOIN td_ok_biaya b ON t.id_dokter = b.dr_operator';
        $listDokter = DmDokter::model()->findAll($cdb);

		$cdb = new CDbCriteria;
		$cdb->join = 'JOIN td_register_ok o ON t.id = o.upf';

		$listUpf = TdUpf::model()->findAll($cdb);
		$models = [];
		$model->TANGGAL_AWAL = $sd ?: date('01/m/Y');
		$model->TANGGAL_AKHIR = $ed ?: date('d/m/Y');
		$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($sd);
		$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($ed);
		
		$model->UPF = $upf ?: '';
		$model->DOKTER = $dokter ?: '';

		$this->layout = '';
		$models = $model->searchLaporan();
		$this->renderPartial('_export',array(
			'models' => $models,
			'model' => $model
		));
		exit;
			
		
	}
	
	public function actionExport()
	{
		$model = new TdOkBiaya;
		$cdb = new CDbCriteria;
        $cdb->join = 'JOIN td_ok_biaya b ON t.id_dokter = b.dr_operator';
        $listDokter = DmDokter::model()->findAll($cdb);

		$cdb = new CDbCriteria;
		$cdb->join = 'JOIN td_register_ok o ON t.id = o.upf';

		$listUpf = TdUpf::model()->findAll($cdb);
		$model->TANGGAL_AWAL = date('01/m/Y');
		$model->TANGGAL_AKHIR = date('d/m/Y');
		$models = [];
		if(!empty($_POST['TdOkBiaya']['TANGGAL_AWAL']) && !empty($_POST['TdOkBiaya']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TdOkBiaya']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TdOkBiaya']['TANGGAL_AKHIR']);
			
			$model->UPF = !empty($_POST['TdOkBiaya']['UPF']) ? $_POST['TdOkBiaya']['UPF'] : '';
			$model->DOKTER = !empty($_POST['TdOkBiaya']['dr_operator']) ? $_POST['TdOkBiaya']['dr_operator'] : '';

			// $this->layout = '';
			$models = $model->searchLaporan();
			// $this->renderPartial('_export',array(
			// 	'models' => $models,
			// 	'model' => $model
			// ));
			// exit;
			
		}

		$this->render('export',array(
			'model' => $model,
			'listDokter' => $listDokter,
			'listUpf' => $listUpf,
			'models' => $models,
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id_ok)
	{
		$model=new TdOkBiaya;

		$ok = TdRegisterOk::model()->findByPk($id_ok);
		$model->td_register_ok_id = $id_ok;


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TdOkBiaya']))
		{
			$model->attributes=$_POST['TdOkBiaya'];
			$transaction=Yii::app()->db->beginTransaction();
			try{

				$model->biaya_dr_op = str_replace(".", "", $model->biaya_dr_op);
				$model->biaya_dr_anas = str_replace(".", "", $model->biaya_dr_anas);
				$model->biaya_prwt_ok = str_replace(".", "", $model->biaya_prwt_ok);
				$model->biaya_prwt_anas = str_replace(".", "", $model->biaya_prwt_anas);
				$model->biaya_jrs = str_replace(".", "", $model->biaya_jrs);
				$model->biaya_bhn = str_replace(".", "", $model->biaya_bhn);

				if($model->save())
				{
					$rawatInap = TrRawatInap::model()->findByAttributes([
						'kode_rawat'=>$ok->kode_daftar,
						'pasien_id' => $ok->no_rm
					]);
					if(!empty($rawatInap))
					{

						$tdRegisterOks = TdRegisterOk::model()->findAllByAttributes([
							'kode_daftar' => $ok->kode_daftar
						]);

						// TrRawatInapTopJm::model()->deleteAllByAttributes(['id_rawat_inap' => $rawatInap->id_rawat_inap]);

						foreach($tdRegisterOks as $item)
						{
							$biaya = $item->tdOkBiayas;
							
							$jm = TrRawatInapTopJm::model()->findByAttributes([
								'id_rawat_inap' => $rawatInap->id_rawat_inap,
								'id_dokter' => $biaya->dr_operator
							]);

							if(empty($jm))
								$jm = new TrRawatInapTopJm;
							
							$jm->ok_id = $item->id_ok;
							$jm->nilai = $biaya->biaya_dr_op;
							$jm->id_rawat_inap= $rawatInap->id_rawat_inap;
							$jm->id_dokter = $biaya->dr_operator;
							$jm->save();
							


						}
							// TrRawatInapTopJm::model()->deleteAllByAttributes(['id_rawat_inap' => $rawatInap->id_rawat_inap]);
						$ja = TrRawatInapTopJa::model()->findByAttributes(array('id_rawat_inap'=>$rawatInap->id_rawat_inap));
						
						if(empty($ja))
							$ja = new TrRawatInapTopJa;

						$ja->nilai = $model->biaya_dr_anas;
						$ja->id_rawat_inap= $rawatInap->id_rawat_inap;
						$ja->id_dokter = $ok->dr_anastesi;
						
						$ja->save();


						$listTindakan = array(
							'JRS'=>$model->biaya_jrs,
							'BHP' => $model->biaya_bhn,
							'JPO'=>$model->biaya_prwt_ok,
							'JPA'=>$model->biaya_prwt_anas,
							'JMA'=> $model->biaya_dr_anas,
							'JMO'=> $model->biaya_dr_op
						);

						foreach($listTindakan as $q => $v)
						{
							$jsPrwtOk = TindakanMedisOperatif::model()->findByAttributes(array('kode_tindakan'=>$q));

							$rit = new TrRawatInapTop;
							$rit->id_rawat_inap = $rawatInap->id_rawat_inap;
							$rit->id_top = $jsPrwtOk->id_tindakan;
							$rit->biaya_irna = $v;
							$rit->biaya_ird = 0;
							$rit->jumlah_tindakan =1;			
							$rit->save();

							TrRawatInapTopTindakan::model()->deleteAllByAttributes([
								'id_rawat_inap' => $rawatInap->id_rawat_inap,
								'id_tindakan' => $jsPrwtOk->id_tindakan,
							]);
							$topTindakan = new TrRawatInapTopTindakan;
							$topTindakan->id_rawat_inap= $rawatInap->id_rawat_inap;
							$topTindakan->keterangan = $q;
							$topTindakan->nilai = $v;
							$topTindakan->id_tindakan = $jsPrwtOk->id_tindakan;
							$topTindakan->save();
						}
						// }
					}
					
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data telah tersimpan.");
					$this->redirect(array('tdRegisterOk/view','id'=>$model->td_register_ok_id));
				}

				
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
		}

		else
		{
			$model->biaya_dr_op = $ok->tindakan0->biaya_drop;
			$model->biaya_dr_anas = $ok->tindakan0->biaya_dran;
			$model->biaya_prwt_ok = $ok->tindakan0->biaya_prwt_op;
			$model->biaya_prwt_anas = $ok->tindakan0->biaya_prwt_an;
			$model->biaya_bhn = 0;
			$model->biaya_jrs = $ok->tindakan0->biaya_jrs;
			$model->biaya_rr_monitor = $ok->tindakan0->dmKelas->biaya_rr_monitor;
			$model->biaya_rr_askep = $ok->tindakan0->dmKelas->biaya_rr_askep;
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TdOkBiaya']))
		{
			$model->attributes=$_POST['TdOkBiaya'];
			$transaction=Yii::app()->db->beginTransaction();
			try{

				$model->biaya_dr_op = str_replace(".", "", $model->biaya_dr_op);
				$model->biaya_dr_anas = str_replace(".", "", $model->biaya_dr_anas);
				$model->biaya_prwt_ok = str_replace(".", "", $model->biaya_prwt_ok);
				$model->biaya_prwt_anas = str_replace(".", "", $model->biaya_prwt_anas);
				$model->biaya_bhn = str_replace(".", "", $model->biaya_bhn);
				$model->biaya_jrs = str_replace(".", "", $model->biaya_jrs);
				
				if($model->save())
				{
					$ok = $model->tdRegisterOk;

					$rawatInap = TrRawatInap::model()->findByAttributes(['kode_rawat'=>$ok->kode_daftar]);
					if(!empty($rawatInap))
					{

						$tdRegisterOks = TdRegisterOk::model()->findAllByAttributes([
							'kode_daftar' => $ok->kode_daftar
						]);

						// TrRawatInapTopJm::model()->deleteAllByAttributes(['id_rawat_inap' => $rawatInap->id_rawat_inap]);

						foreach($tdRegisterOks as $item)
						{
							$biaya = $item->tdOkBiayas;
							
							$jm = TrRawatInapTopJm::model()->findByAttributes([
								'id_rawat_inap' => $rawatInap->id_rawat_inap,
								'id_dokter' => $biaya->dr_operator
							]);

							if(empty($jm))
								$jm = new TrRawatInapTopJm;
							
							$jm->ok_id = $item->id_ok;
							$jm->nilai = $biaya->biaya_dr_op;
							$jm->id_rawat_inap= $rawatInap->id_rawat_inap;
							$jm->id_dokter = $biaya->dr_operator;
							$jm->save();
							


						}
						
						// $jm = TrRawatInapTopJm::model()->findByAttributes(['id_rawat_inap'=>$rawatInap->id_rawat_inap]);;
						// $jm->id_rawat_inap= $rawatInap->id_rawat_inap;
						// $jm->id_dokter = $model->dr_operator;
						// $jm->nilai = $model->biaya_dr_op;;
						// $jm->save();

						$ja = TrRawatInapTopJa::model()->findByAttributes(['id_rawat_inap'=>$rawatInap->id_rawat_inap]);;
						$ja->id_rawat_inap= $rawatInap->id_rawat_inap;
						$ja->id_dokter = $ok->dr_anastesi;
						$ja->nilai = $model->biaya_dr_anas;
						$ja->save();

						$listTindakan = array(
							'BHP' => $model->biaya_bhn,
							'JRS'=>$model->biaya_jrs,
							'JPO'=>$model->biaya_prwt_ok,
							'JPA'=>$model->biaya_prwt_anas,
							'JMA'=> $model->biaya_dr_anas,
							'JMO'=> $model->biaya_dr_op
						);

						foreach($listTindakan as $q => $v)
						{
							$jsPrwtOk = TindakanMedisOperatif::model()->findByAttributes(array('kode_tindakan'=>$q));

							$rit = TrRawatInapTop::model()->findByAttributes(array(
								'id_rawat_inap' => $rawatInap->id_rawat_inap,
								'id_top' => $jsPrwtOk->id_tindakan
							));
							$rit->biaya_irna = $v;
							$rit->save(false, array('biaya_irna'));

							TrRawatInapTopTindakan::model()->deleteAllByAttributes([
								'id_rawat_inap' => $rawatInap->id_rawat_inap,
								'id_tindakan' => $jsPrwtOk->id_tindakan,
							]);
							$topTindakan = new TrRawatInapTopTindakan;
							$topTindakan->id_rawat_inap= $rawatInap->id_rawat_inap;
							$topTindakan->keterangan = $q;
							$topTindakan->nilai = $v;
							$topTindakan->id_tindakan = $jsPrwtOk->id_tindakan;

							$topTindakan->save();
						}

					}
					
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data telah tersimpan.");
					$this->redirect(array('tdRegisterOk/view','id'=>$ok->id_ok));
				}
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TdOkBiaya');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TdOkBiaya('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['UPF']))
			$model->UPF=$_GET['UPF'];

		if(isset($_GET['DOKTER']))
			$model->DOKTER=$_GET['DOKTER'];		

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['TdOkBiaya']))
			$model->attributes=$_GET['TdOkBiaya'];

	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TdOkBiaya the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TdOkBiaya::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TdOkBiaya $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='td-ok-biaya-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
