<?php

class AjaxRequestController extends Controller
{

	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getAllCalendarEvents','getBatasSEP'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('getKec','getKota','getKecamatan','getPasienByRM','getDiagAwal','getPoli','getPasienLikeParam','getTindakanAll','getDokter','getJenisVisiteDokter','inputOk',
					'getTOP','getTNOP','getLayananPenunjang','getObat','getLainlain',
					'inputTopJM','inputTopJA','inputTnopJRM','inputTnopJM','inputTnopJapel',
					'inputTnopIrdJRM','inputTnopIrdJM','inputTnopIrdJapel','inputAlkesObat','inputAlkesObatIrd','inputTOP','inputBiayaKamar','getPasienInMaster','getPasienRawatInap','getObatLikeParam','getPasienInRawatInap','getPasienBpjs','getKamarByKelas','getLastRegisteredPx','getLastRegisteredPxRJ','getPasienPendaftaran','getRacikanLikeParam','getUnit','getICD','getProcedure','getRekapKunjungan','getRekapKunjunganRawatInap','getLaba','getKunjunganGolongan','countKunjunganGolongan','countKunjunganGolonganLastfive','getUsiaSexGolTanggal','getRekapPoliSexUsia','getPenjualan','getPembelian','getPerbandingan','getTopTenPenyakit','getPendapatanOk','getOkCitoElektif','getRekapAnastesi','getRekapOperator','getRekapOperasiLastfive','getRincianDiagnosa','getRincianTindakan','getPegawai','getItemTindakanPoli'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionGetItemTindakanPoli()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			$dataItem = $_POST['dataItem'];
			$id = $dataItem['poli_tindakan_id'];
			$hasil = UnitTindakan::model()->findByPk($id);
			$res = [
				'jrs' => $hasil->jrs,
				'jaspel' => $hasil->jaspel,
				'akhp' => $hasil->akhp,
				'nilai' => $hasil->nilai,
				'id'=>$hasil->id
			];

			$result = [
				'shortmsg' => 'success',
				'result' => $res,
				// 'message' => $e->getMessage(),
			];

			echo CJSON::encode($result);
			
		}
	}

	public function actionGetRincianTindakan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$noreg = $_POST['reg'];
			$criteria = new CDbCriteria;
			$criteria->condition = "NoDaftar = ".$noreg;
			$criteria->order = "NoDaftar DESC";
			$rincian = BPendaftaranRjalan::model()->findAll($criteria);

			$results = [];
			foreach($rincian as $r)
			{
				foreach($r->noDaftar->listProsedur as $v)
				{
					$results[] = [
						'kd' => $v->kode_proc,
						// 'shtnm' => $v->kodeProc->short_description,
						'nm' => $v->kodeProc->description
					];
				}
			}

			echo json_encode($results);
		}
	}

	public function actionGetRincianDiagnosa()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$noreg = $_POST['reg'];
			$criteria = new CDbCriteria;
			$criteria->condition = "NoDaftar = ".$noreg;
			$criteria->order = "NoDaftar DESC";
			$rincian = BPendaftaranRjalan::model()->findAll($criteria);

			$results = [];
			foreach($rincian as $r)
			{
				foreach($r->noDaftar->listIcd10 as $v)
				{
					$results[] = [
						'kd' => $v->kode_icd,
						'nm' => $v->kodeIcd->deskripsi
					];
				}
			}

			echo json_encode($results);
		}
	}

	public function actionGetRekapOperasiLastfive()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$results = [];
			
			$listTahun = [];
			for($i = date('Y') - 4 ;$i<=date('Y');$i++)
				$listTahun[$i] = $i;

			$results['listTahun'] = $listTahun;
			$url = "/ok/count/tahunan";
			foreach($listTahun as $tahun)
			{	
				
				$sd = $tahun.'-01-01';
				$ed = $tahun.'-12-31';

				$params = [
					'sd' => $sd,
					'ed' => $ed
				];
				
				$result = Yii::app()->rest->getDataApi($url,$params);
				$results['items'][] = $result->values;
			}

			$results['tahun'] = $listTahun;

			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetRekapOperator()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');

			$sd = $tahun.'-01-01';
			$ed = $tahun.'-12-31';

			$params = [
				'sd' => $sd,
				'ed' => $ed
			];
			$url = "/ok/rekap/operator";
			$results = Yii::app()->rest->getDataApi($url,$params);

			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	
	}

	public function actionGetRekapAnastesi()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');

			$sd = $tahun.'-01-01';
			$ed = $tahun.'-12-31';

			$params = [
				'sd' => $sd,
				'ed' => $ed
			];
			$url = "/ok/upf/anastesi";
			$results = Yii::app()->rest->getDataApi($url,$params);

			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	
	}

	public function actionGetOkCitoElektif()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');

			$sd = $tahun.'-01-01';
			$ed = $tahun.'-12-31';

			$params = [
				'sd' => $sd,
				'ed' => $ed
			];
			$url = "/ok/upf/citoelektif";
			$results = Yii::app()->rest->getDataApi($url,$params);

			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	
	}

	public function actionGetPendapatanOk()
	{
	
		if(Yii::app()->request->isAjaxRequest)
		{
			$url = '/ok/jasa/rs';
			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			$results = [];
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'sd' => $sd,
					'ed' => $ed
				];

				
				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['now'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }

		    $tahun = $tahun - 1;
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'sd' => $sd,
					'ed' => $ed
				];

				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['then'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }


			
			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	
	}

	public function actionGetTopTenPenyakit()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			
			$url = '/p/penyakit/top';
			$params = ['tahun'=>$tahun];
			$result = Yii::app()->rest->getDataApi($url, $params);
			$results = !empty($result->values) ? $result->values : [];
			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetPerbandingan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			$results = [];
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'startdate' => $sd,
					'enddate' => $ed
				];

				$url = '/integra/pembelian';
				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['buy'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }

		    // $tahun = $tahun - 1;
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'startdate' => $sd,
					'enddate' => $ed
				];

				$url = '/integra/penjualan';
				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['sales'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }


			
			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetPembelian()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			$results = [];
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'startdate' => $sd,
					'enddate' => $ed
				];

				$url = '/integra/pembelian';
				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['now'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }

		    $tahun = $tahun - 1;
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'startdate' => $sd,
					'enddate' => $ed
				];

				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['then'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }


			
			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetPenjualan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			$results = [];
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'startdate' => $sd,
					'enddate' => $ed
				];

				$url = '/integra/penjualan';
				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['now'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }

		    $tahun = $tahun - 1;
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'startdate' => $sd,
					'enddate' => $ed
				];

				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['then'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }


			
			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetRekapPoliSexUsia()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			// $cache=Yii::app()->cache;
			// $cache->flushAll();
			$bulan = !empty($_POST['bulan']) ? $_POST['bulan'] : date('m');
			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			// $gol = $_POST['kode'];
			$sd = $tahun.'-'.$bulan.'-01';
			$ed = date('Y-m-t',strtotime($sd));

			$params = [
				'tahun' => $tahun,
				'bulan' => $bulan,
				'tipe' => 2
			];

			$url = "/p/unit/list";

			$listUnit = Yii::app()->rest->getDataApi($url,$params);
			$results = [];
			$listOfUnits = [];
			// if(empty($cache['listUnit']))
			// {
			// 	$cache->set('listUnit',$listUnit->values,5);
				$listOfUnits = $listUnit->values;
			// }

			// else{
			// 	$listOfUnits = $cache['listUnit'];
			// }

			
			foreach($listOfUnits as $gol)
			{	
				

				$url = '/p/gol/sexusia';

				$params = [
					'sd' => $sd,
					'ed' => $ed,
					'tipe' => 'poli',
					'key' => $gol->KodeUnit
				];
				// $value = $cache[$gol->KodeUnit];

				// if(empty($value))
				// {
				$tmp = Yii::app()->rest->getDataApi($url,$params);
				$results['listUnit'][] = [
					'unit' => $gol->NamaUnit,
					'items' => $tmp->values
				];

					// $cache->set($gol->KodeUnit,[
					// 	'unit' => $gol->NamaUnit,
					// 	'items' => $tmp->values
					// ],5);

						
				// }

				// else
				// {
				// 	$results['listUnit'][] = $cache[$gol->KodeUnit];
					
				// }
				
			}
			echo CJSON::encode($results);
		}
	}

	public function actionGetUsiaSexGolTanggal()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$results = [];
			$url = '/p/gol/sexusia';

			$bulan = !empty($_POST['bulan']) ? date('m',strtotime($_POST['bulan'])) : date('m');
			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			$gol = $_POST['kode'];
			$sd = $tahun.'-'.$bulan.'-01';
			$ed = date('Y-m-t',strtotime($sd));

			$params = [
				'sd' => $sd,
				'ed' => $ed,
				'tipe' => 'gol',
				'key' => $gol
			];

			$results = Yii::app()->rest->getDataApi($url,$params);
			// print_r($result->values);exit;
			// $list[$gol->KodeGol] = $result->values;

			$listMale = [];
			$listFemale = [];
			$listKategori = [];

			foreach($results->values as $item)
			{
				if($item->jk == 'L')
				{
					$listMale[] = $item->count;
					$listKategori[] = $item->range;
				}

				else{
					$listFemale[] = $item->count;
				}
			}	

			$results = [
				'males' => $listMale,
				'females' => $listFemale,
				'kategori' => $listKategori
			];		


			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionCountKunjunganGolonganLastfive()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$url = "/p/golongan/list/five";
			$listGol = Yii::app()->rest->getDataApi($url,[]);
			$results = [];
			
			$listTahun = [];
			for($i = date('Y') - 4 ;$i<=date('Y');$i++)
				$listTahun[$i] = $i;

			$results['listTahun'] = $listTahun;
			$results['listGol'] = $listGol;
			$url = '/kunjungan/golongan/count/lastfive';
			foreach($listGol->values as $gol)
			{	
				
				$params = [
					'kode' => $gol->KodeGol
				];
				
				$result = Yii::app()->rest->getDataApi($url,$params);
				// print_r($result->values);exit;
				// $list[$gol->KodeGol] = $result->values;
				$results['rows'][$gol->KodeGol] = $result->values;
			}


			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionCountKunjunganGolongan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			$kode = !empty($_POST['kode']) ? $_POST['kode'] : '';
			$sd = date('Y-m-d',strtotime($tahun.'-01-01'));
			$ed = date('Y-m-d',strtotime($tahun.'-12-31'));
			$params = [
				'tahun' => $tahun,
				'kode' => $kode
			];
			$url = "/p/golongan/list";
			$listGol = Yii::app()->rest->getDataApi($url,$params);
			$results = [];
			$countGol = [];
			
			$results['listGol'] = $listGol;
			for ($m=1; $m<=12; $m++) 
			{
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				$results['listBulan'][] = $label;

			}

			$url = "/kunjungan/golongan/count";
			
			if(!empty($kode))
			{
				$params = [
					'kode' => $kode,
					'tahun' => $tahun
				];
				
				$result = Yii::app()->rest->getDataApi($url,$params);
				// print_r($result->values);exit;
				// $list[$gol->KodeGol] = $result->values;

				$results['rows'][$kode] = $result->values;

			}

			else
			{
				foreach($listGol->values as $gol)
				{	
					

					$params = [
						'kode' => $gol->KodeGol,
						'tahun' => $tahun
					];
					


					$result = Yii::app()->rest->getDataApi($url,$params);
					// print_r($result->values);exit;
					// $list[$gol->KodeGol] = $result->values;
					$results['rows'][$gol->KodeGol] = $result->values;
				}

			}
			
			// print_r($results);exit;

			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetKunjunganGolongan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$bulan = !empty($_POST['bulan']) ? $_POST['bulan'] : date('m');
			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');

			$sd = $tahun.'-'.$bulan.'-01';
			$ed = date('Y-m-t',strtotime($sd));

			$params = [
				'startdate' => $sd,
				'enddate' => $ed
			];
			$url = "/poli/kunjungan/golongan";
			$results = Yii::app()->rest->getDataApi($url,$params);

			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetLaba()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');
			$results = [];
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'startdate' => $sd,
					'enddate' => $ed
				];

				$url = '/integra/laba';
				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['now'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }

		    $tahun = $tahun - 1;
			for ($m=1; $m<=12; $m++) {
				$month = date('m', mktime(0,0,0,$m, 1, $tahun));
				$label = date('F', mktime(0,0,0,$m, 1, $tahun));
				
				$sd = date('Y-m-d',strtotime($tahun.'-'.$m.'-01'));
				$ed = date('Y-m-t',strtotime($sd));

				$params = [
					'startdate' => $sd,
					'enddate' => $ed
				];

				$result = Yii::app()->rest->getDataApi($url, $params);

				$total = $result->values;

				$results['then'][] = [
					'bulan' => $label,
					'jumlah' => round($total,2)
				];
		    }


			
			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}


	public function actionGetRekapKunjunganRawatInap()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$bulan = !empty($_POST['bulan']) ? $_POST['bulan'] : date('m');
			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');

			$sd = $tahun.'-'.$bulan.'-01';
			$ed = date('Y-m-t',strtotime($sd));

			$params = [
				'startdate' => $sd,
				'enddate' => $ed
			];

			$url = "/kamar/rekap/kunjungan";
			$results = Yii::app()->rest->getDataApi($url,$params);

			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetRekapKunjungan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$bulan = !empty($_POST['bulan']) ? $_POST['bulan'] : date('m');
			$tahun = !empty($_POST['tahun']) ? $_POST['tahun'] : date('Y');

			$sd = $tahun.'-'.$bulan.'-01';
			$ed = date('Y-m-t',strtotime($sd));

			$params = [
				'startdate' => $sd,
				'enddate' => $ed
			];
			$url = "/poli/rekap/kunjungan";
			$results = Yii::app()->rest->getDataApi($url,$params);

			// header("Content-type: text/json");
			echo CJSON::encode($results);
		}
	}

	public function actionGetProcedure()
	{
		$result = [];
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];
			$model = ProcedureIcd::model()->searchProc($q);

			
			foreach($model as $m)
			{
				$result[] = array(
					'id' => $m->code,
					'value' => $m->code.' - '.$m->description,

				);
			}
		}
		echo CJSON::encode($result);
	}

	public function actionGetICD()
	{
		$result = [];
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];
			$model = Icd::model()->searchDiagAwal($q);

			
			foreach($model as $m)
			{
				$result[] = array(
					'id' => $m->kode,
					'value' => $m->kode.' - '.$m->deskripsi,

				);
			}
		}
		echo CJSON::encode($result);
	}

	public function actionGetUnit()
	{

		$q = $_GET['term'];
		
		$criteria=new CDbCriteria;

		$criteria->addSearchCondition('NamaUnit',$q,true,'OR');
		$criteria->addCondition("unit_tipe=2");
		$criteria->order = 'NamaUnit ASC';
		$criteria->limit = 20;
		

		$data = Unit::model()->findAll($criteria);
		
		// print_r($data);exit;

		$result = array();
		foreach($data as $d)
		{
			$value = strtoupper($d->KodeUnit.' - '.$d->NamaUnit);
			$result[] = array(
				'id' => $d->KodeUnit,
				'value'=>$value,		
			);
		}

		echo CJSON::encode($result);
		
	}

	public function actionGetLastRegisteredPxRJ()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$model = new BPendaftaranRjalan;
			$result=[];
			$dataProvider = $model->searchLimit(10);
			foreach($dataProvider->getData() as $px)
			{
				$full = Yii::app()->helper->appendZeros($px->noDaftar->NoMedrec,6).' / '.$px->noDaftar->noMedrec->NAMA.' ke Unit '.$px->unit->NamaUnit ;
					
				$result[] = array(
					'id' => $px->NoDaftar,
					'nama' => strtoupper($px->noDaftar->noMedrec->NAMA),
					'alamat' => $px->noDaftar->noMedrec->ALAMAT,
					'rm' => $px->noDaftar->noMedrec,
					'ruang' => $px->unit->NamaUnit,
					'tanggal' => $px->noDaftar->TGLDAFTAR.' '.$px->noDaftar->JamDaftar,
					'jk'=>trim($px->noDaftar->noMedrec->JENSKEL),
					'msg' => $full
				);
			}

			echo json_encode($result);
		}
	}

	public function actionGetLastRegisteredPx()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$model = new TdPasienMasuk;
			$result=[];
			foreach($model->searchLimit(10) as $px)
			{
				$full = Yii::app()->helper->appendZeros($px->No_RM,6).' / '.$px->Nama_pasien.' pasien Ruang '.$px->Ruang.' Kelas '.$px->Kelas  ;
					
				$result[] = array(
					'id' => $px->Kode_Detail,
					'nama' => strtoupper($px->Nama_pasien),
					'alamat' => $px->Alamat,
					'rm' => $px->No_RM,
					'ruang' => $px->Ruang,
					'tanggal' => $px->Tgl_MRS,
					'dokter' => $px->Dokter,
					'jk'=>trim($px->Jenis_kelamin),
					'msg' => $full
				);
			}

			echo json_encode($result);
		}
	}

	public function actionInputOk()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			
			$id_register_ok = $_POST['idok'];
			$status_ok = $_POST['sok'];

			$transaction=Yii::app()->db->beginTransaction();
			try{
				$btn = '';
				$btnTxt = '';
				switch ($status_ok) {
					case 1:
						$btn = 'danger';
						$btnTxt = 'Belum Operasi';
						break;
					case 2:
						$btn = 'warning';
						$btnTxt = 'Sedang Operasi';
						break;
					case 3:
						$btn = 'success';
						$btnTxt = 'Sudah Operasi';
						break;
				}
				$hasil = array(
					'code' => 'success',
					'msg' => 'Data telah disimpan',
					'btn' => $btn,
					'btnTxt' => $btnTxt
				);
				$model = TdRegisterOk::model()->findByPk($id_register_ok);
				$model->status_ok = $status_ok;
				$model->save(false,array('status_ok'));
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$hasil = array(
					'code' => 'error',
					'msg' => 'Terjadi kesalahan'
				);
				$transaction->rollback();
			}

		}
	}

	public function actionGetKamarByKelas()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
				
			$cid = Yii::app()->request->getPost('q'); 

			$dm_kelas = DmKelas::model()->findByAttributes(array('kode_kelas_bpjs'=>$cid));

			$list = array();
			if(!empty($dm_kelas))
			{

				$kamars= DmKamar::model()->findAll(
		                array(
		               'condition'=>'kelas_id=:cid', 
		               'params'=>array(':cid'=>$dm_kelas->id_kelas)));
	            $list = CHtml::listData($kamars, 'id_kamar', 'nama_kamar');    
	        }
	        echo json_encode($list);
		}
	}

	public function actionGetRacikanLikeParam()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = TrObatRacikan::model()->searchRacikanBy($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $obat)
				{
					$full = $obat->nama;
						
					$result[] = array(
						'id' => $obat->id,
						'value' => strtoupper($full),
						'hj' => $obat->sumObatItem,
						'kekuatan' => $obat->kekuatan


					);
				}

			echo CJSON::encode($result);
		}
		
	}

	public function actionGetObatLikeParam()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$params = [
				'euid' => Yii::app()->user->getState('euid'),
				'key' => $q
			];

			$result = ErpSalesMasterBarang::model()->searchObatBy($q);
			// print_r($result);exit;
			// $result = Yii::app()->rest->api_getObatLike($params);
			$response = [];

			if(!empty($result)){
				
				foreach($result as $obat)
				{

					$full = $obat->nama_barang;
						
					$response[] = array(
						'id' => $obat->kode_barang,
						'value' => strtoupper($full),
						'hj' => $obat->harga_jual,
						'hb' => $obat->harga_beli,
						'satuan'=>$obat->id_satuan,
						// 'kekuatan' => $obat->kekuatan,
						// 'satuan_kekuatan' =>$obat->satuan_kekuatan,
						// 'stok' => $obat->stok,

					);
					// $response[] = array(
					// 	'id' => $obat->kd_barang,
					// 	'value' => strtoupper($full),
					// 	'hj' => $obat->hj,
					// 	'hb' => $obat->hb,
					// 	'satuan'=>$obat->satuan,
					// 	'kekuatan' => $obat->kekuatan,
					// 	'satuan_kekuatan' =>$obat->satuan_kekuatan,
					// 	'stok' => $obat->stok,

					// );
				}

				echo CJSON::encode($response);
			}

				
			else{

				$data[] = [
					'id' => 0,
					'nama' => 'Data Empty',
					'jenis' => 'Data Empty'
				];
				echo CJSON::encode($data);
			}
			// $model = MObatAkhp::model()->searchObatBy($q);
			

			// $result = array();
			
			// if(!empty($model))
			// 	foreach($model as $obat)
			// 	{
			// 		$full = $obat->nama_barang;
						
			// 		$result[] = array(
			// 			'id' => $obat->kd_barang,
			// 			'value' => strtoupper($full),
			// 			'hj' => $obat->hj,
			// 			'hb' => $obat->hb,
			// 			'satuan'=>$obat->satuan,
			// 			'kekuatan' => $obat->kekuatan,
			// 			'satuan_kekuatan' =>$obat->satuan_kekuatan,
			// 			'stok' => $obat->stok,

			// 		);
			// 	}

			// echo CJSON::encode($result);
		}
		
	}

	public function actionGetPasienRawatInap()
	{

		$q = $_GET['term'];

		$criteria=new CDbCriteria;


		$criteria->addSearchCondition('pASIEN.NoMedrec',$q,true,'OR');
		$criteria->addSearchCondition('pASIEN.NAMA',$q,true,'OR');
		$criteria->addCondition("status_pasien=1");
		$criteria->order = 'datetime_masuk DESC';
		$criteria->with = array('pASIEN');
		$criteria->together = true;
		$criteria->limit = 20;
		

		$data = TrRawatInap::model()->findAll($criteria);
		
		// print_r($data);exit;

		$result = array();
		foreach($data as $d)
		{
			$value = strtoupper($d->pASIEN->NAMA.' - '.$d->pASIEN->NoMedrec);
			$result[] = array(
				'id' => $d->pASIEN->NoMedrec,
				'value'=>$value,		
			);
		}

		echo CJSON::encode($result);
		
	}

	public function actionInputBiayaKamar()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'tr_ri_id' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapKamarHistory::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapKamarHistory;
					$model->tr_ri_id= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data telah disimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTOP()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				'id_tindakan' => $obj[0]->id_tindakan,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTopTindakan::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTopTindakan;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					$model->id_tindakan = $ob->id_tindakan;
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data telah disimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputAlkesObatIrd()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				'kode_alkes' => $obj[0]->kode_alkes,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapAlkesObatIrd::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapAlkesObatIrd;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					$model->kode_alkes = $ob->kode_alkes;
					
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data telah disimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputAlkesObat()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				'kode_alkes' => $obj[0]->kode_alkes,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapAlkesObat::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapAlkesObat;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					$model->kode_alkes = $ob->kode_alkes;
					$model->tanggal_input = date('Y-m-d');
					$model->id_dokter = $ob->id_dokter;
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data telah disimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'danger',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTnopIrdJapel()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTnopIrdJapel::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTnopIrdJapel;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data terhitung. Klik Simpan agar data tersimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTnopIrdJM()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTnopIrdJm::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTnopIrdJm;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data terhitung. Klik Simpan agar data tersimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTnopIrdJRM()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTnopIrdJrm::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTnopIrdJrm;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data terhitung. Klik Simpan agar data tersimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTnopJapel()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTnopJapel::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTnopJapel;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data terhitung. Klik Simpan agar data tersimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTnopJM()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTnopJm::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTnopJm;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data terhitung. Klik Simpan agar data tersimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTnopJRM()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTnopJrm::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTnopJrm;
					$model->id_rawat_inap= $ob->ri;
					$model->keterangan = $ob->keterangan;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data terhitung. Klik Simpan agar data tersimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTopJA()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTopJa::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTopJa;
					$model->id_rawat_inap= $ob->ri;
					$model->id_dokter = $ob->dokter;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data telah disimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}

	public function actionInputTopJM()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$json = file_get_contents('php://input'); 
			$obj = json_decode($json);

			$transaction=Yii::app()->db->beginTransaction();
			$attr = array(
				'id_rawat_inap' => $obj[0]->ri,
				// 'nilai' => $ob->nilai
			);

			try{
				TrRawatInapTopJm::model()->deleteAllByAttributes($attr);
		
				$hasil = array();
				$line = 1;
				foreach($obj as $ob)
				{
					$model = new TrRawatInapTopJm;
					$model->id_rawat_inap= $ob->ri;
					$model->id_dokter = $ob->dokter;
					$model->nilai = str_replace(".", "", $ob->biaya);
					
					if($model->validate()){
						$model->save();
						$hasil = array(
							'code' => 'success',
							'msg' => 'Data telah disimpan'
						);


					}
					else{

						$errors = 'Baris ke : '.$line.'<br>';
						
						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						$hasil = array(
							'code' => 'error',
							'msg' => $errors
						);

						$line++;
						// throw new Exception();
						break;
						
						 // echo ;
					}

					
				}
				$transaction->commit();
				echo json_encode($hasil);
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
			
		}
	}



	public function actionGetLainlain()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = TindakanMedisLain::model()->searchByNama($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $m)
				{

					
					$result[] = array(
						'id' => $m->id_tindakan,
						'value' => $m->nama_tindakan,

					);
				}

			echo CJSON::encode($result);
		}
	}

	public function actionGetObat()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = ObatAlkes::model()->searchByNama($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $m)
				{

					
					$result[] = array(
						'id' => $m->id_obat_alkes,
						'value' => $m->nama_obat_alkes,

					);
				}

			echo CJSON::encode($result);
		}
	}

	public function actionGetLayananPenunjang()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = TindakanMedisPenunjang::model()->searchByNama($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $m)
				{

					
					$result[] = array(
						'id' => $m->id_tindakan_penunjang,
						'value' => $m->nama_tindakan,

					);
				}

			echo CJSON::encode($result);
		}
	}


	public function actionGetTNOP()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = TindakanMedisNonOperatif::model()->searchByNama($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $m)
				{

					
					$result[] = array(
						'id' => $m->id_tindakan,
						'value' => $m->nama_tindakan,

					);
				}

			echo CJSON::encode($result);
		}
	}

	public function actionGetTOP()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = TindakanMedisOperatif::model()->searchByNama($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $m)
				{

					
					$result[] = array(
						'id' => $m->id_tindakan,
						'value' => $m->nama_tindakan,

					);
				}

			echo CJSON::encode($result);
		}
	}

	public function actionGetJenisVisiteDokter()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];
			$t = $_GET['tkt'];

			$model = JenisVisite::model()->searchByNama($q,$t);
			

			$result = array();
			
			if(!empty($model))
			{
				foreach($model as $m)
				{

					
					$result[] = array(
						'id' => $m->id_jenis_visite,
						'value' => $m->nama_visite,
						'kode' => $m->kode_visite,
						'biaya' => Yii::app()->helper->formatRupiah($m->biaya)

					);
				}
			}
			echo CJSON::encode($result);
		}
	}

	public function actionGetPegawai()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = Pegawai::model()->searchByNama($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $p)
				{
					$full = $p->NAMA;
						
					$result[] = array(
						'id' => $p->KDPEGAWAI,
						'value' => $full,

					);
				}

			echo CJSON::encode($result);
		}
	}

	public function actionGetDokter()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = DmDokter::model()->searchByNama($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $dokter)
				{
					$full = $dokter->nama_dokter;
						
					$result[] = array(
						'id' => $dokter->id_dokter,
						'value' => $full,
						'jenis' => strtoupper($dokter->jenis_dokter),

					);
				}

			echo CJSON::encode($result);
		}
	}


	public function actionGetTindakanAll()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];
			$j = $_GET['jenis'];

			switch($j)
			{
				case 0 :  // Kamar
					$criteria=new CDbCriteria;
					$criteria->addSearchCondition('nama_item',$q,true,'OR');

					$criteria->limit = 20;

					$model = TarifKamar::model()->findAll($criteria);

					$result = array();
					foreach($model as $m)
					{

						$result[] = array(
							'id' => $m->id_tarif_kamar,
							'value' => $m->nama_item,

						);
					}

					break;
				case 1 :  // Medis
					$criteria=new CDbCriteria;
					$criteria->addSearchCondition('nama_tindakan',$q,true,'OR');

					$criteria->limit = 20;

					$model = TindakanMedisAll::model()->findAll($criteria);

					$result = array();
					foreach($model as $m)
					{

						$result[] = array(
							'id' => $m->id_tindakan,
							'value' => $m->nama_tindakan.' | '.$m->kelas->nama_kelas,

						);
					}

					break;

				case 2 : // Darurat

					$criteria=new CDbCriteria;
					$criteria->addSearchCondition('nama_tindakan',$q,true,'OR');

					$criteria->limit = 20;

					$model = TindakanRawatDarurat::model()->findAll($criteria);

					$result = array();
					foreach($model as $m)
					{

						$result[] = array(
							'id' => $m->id_tindakan,
							'value' => $m->nama_tindakan,

						);
					}


					break;
			}

			

			echo CJSON::encode($result);
		}

	}

	public function actionGetPoli()
	{

		$q = $_GET['term'];
		// $model = Yii::app()->rest->getPoli($q);


  //    	$result = array();
		// foreach($model->response->list as $m)
		// {

		// 	$result[] = array(
		// 		'id' => $m->kdPoli,
		// 		'value' => $m->nmPoli,

		// 	);
		// }

		$model = BpjsPoli::model()->searchPoli($q);

		$result = array();
		foreach($model as $m)
		{

			$result[] = array(
				'id' => $m->KODE_POLI,
				'value' => $m->NAMA_POLI,

			);
		}

		echo CJSON::encode($result);
	}

	public function actionGetDiagAwal()
	{
		$q = $_GET['term'];
		$model = Yii::app()->rest->getDiagnosa($q);


     	$result = array();
		foreach($model->response->list as $m)
		{

			$result[] = array(
				'id' => $m->kodeDiagnosa,
				'value' => $m->namaDiagnosa,

			);
		}
		

		// $model = BpjsDiagnosis::model()->searchDiagAwal($q);

		// $result = array();
		// foreach($model as $m)
		// {

		// 	$result[] = array(
		// 		'id' => $m->KODE_DIAG,
		// 		'value' => $m->DESKRIPSI,

		// 	);
		// }

		echo CJSON::encode($result);
	}

	

	public function actionGetBatasSEP()
	{

		$tanggal = $_POST['tgl'];
		
		$result = TrPendaftaranRjalan::model()->cekBatasSEPFromGoogle($tanggal);
		echo CJSON::encode($result);
	}

	public function actionGetAllCalendarEvents()
	{

		if(isset($_POST['type']))
		{

			$type = $_POST['type'];

			if($type== 'fetch')
			{
				$criteria=new CDbCriteria;
				$criteria->with = array('eVENT');
				$criteria->together = true;

				$model = DmKalender::model()->findAll($criteria);

				$result = array();
				foreach($model as $m)
				{

					$result[] = array(
						'id' => $m->id,
						'title' => $m->eVENT->nama_event,
						'start' => $m->startdate,
						'end' => $m->enddate,
						'allDay' => true
					);
				}

				echo CJSON::encode($result);
			}

			else if($type == 'delete')
			{
				$id = $_POST['id'];
				$model = DmKalender::model()->findByPk($id);
				if(!empty($model))
				{
					$model->delete();
					echo CJSON::encode(array('status'=>'success','eventid'=>$id));
				}
			}

			else if($type=='new')
			{
				$event_id = $_POST['id'];
				$ket = $_POST['ket'];
				$model = new DmKalender();
				$model->event_id = $event_id;
				$model->title = $ket;
				$model->startdate = $_POST['startdate'];
				$model->enddate = $model->startdate;
				if($model->save())
					echo CJSON::encode(array('status'=>'success','eventid'=>$event_id));
				else
					echo CJSON::encode(array('status'=>'error','eventid'=>$event_id));

			}
		}
	}

	public function actionGetPasienByRM()
	{
		if(isset($_POST['norm']))
		{
			$norm = $_POST['norm'];

			$pasien = Pasien::model()->findByPk($norm);

			$data = array();
			if(!empty($pasien))
			{
				$data['pasien'] = $pasien;

				$data['kota'] = array();
				$data['kec'] = array();
				if(!empty($pasien->kEC)){
					$data['kec'] = array(
						'id' => $pasien->kEC->KodeKec,
						'nama' => $pasien->kEC->NamaKec
					);
				}

				if(!empty($pasien->kOTA)){
					$data['kota'] = array(
						'id' => $pasien->kOTA->KodeKabKota,
						'nama' => $pasien->kOTA->NamaKabKota
					);
				}
					
			}
			
			echo CJSON::encode($data);
		}


	}

	public function actionGetKec()
	{
		if(!empty($_POST['kid']))
			$kota_id = $_POST['kid'];

		if(!empty($_POST['kota_id']))	
			$kota_id = $_POST['kota_id'];
		
	    $data=Refkec::model()->findAll('KodeKabKota=:pid', 
			          array(':pid'=>$kota_id));
	    
	    $data=CHtml::listData($data,'KodeKec','NamaKec');
	    foreach($data as $value=>$name)
	    {
	        echo CHtml::tag('option',
					   array('value'=>$value),CHtml::encode($name),true);
	    }
	}

	public function actionGetKota()
	{
		if(!empty($_POST['pid']))
			$propinsi_id = $_POST['pid'];

		if(!empty($_POST['propinsi_id']))	
			$propinsi_id = $_POST['propinsi_id'];
	    
	    $data=Refkabkota::model()->findAll('KodeProv=:pid', 
			          array(':pid'=>$propinsi_id));
	    
	    $data=CHtml::listData($data,'KodeKabKota','NamaKabKota');
	    foreach($data as $value=>$name)
	    {
	        echo CHtml::tag('option',
					   array('value'=>$value),CHtml::encode(strtoupper($name)),true);
	    }
	}

	public function actionGetKecamatan()
	{

		$q = $_GET['term'];

		$criteria=new CDbCriteria;
		$criteria->addSearchCondition('NamaKec',$q,true);
		$criteria->limit = 20;
		

		$data = Refkec::model()->findAll($criteria);
		
		$result = array();
		foreach($data as $d)
		{
			$value = strtoupper($d->NamaKec);
			$id_kota = '';
			$nama_kota_prov = '';
			if(!empty($d->kOTA))
			{
				$value = strtoupper($d->NamaKec.' - '.$d->kOTA->NamaKabKota);

				if(!empty($d->kOTA->pROV))
				{
					$id_kota = $d->kOTA->KodeKabKota;
					$nama_kota_prov = $d->kOTA->NamaKabKota.' - '.$d->kOTA->pROV->NamaProv;
				}
			}



			$result[] = array(
					'id' => $d->KodeKec,
					'value'=>$value,
					'id_kota' => $id_kota,
					'nama_kota_prov' => $nama_kota_prov,
				);
		}

		echo CJSON::encode($result);
		
	}

	public function actionGetPasienBpjs()
	{

		$q = $_GET['term'];
		$data = Yii::app()->db->createCommand()
	    ->select('p.NAMA, PASIEN_ID')
	    ->from('bpjs_pasien t')
	    ->join('a_pasien p', 't.PASIEN_ID=p.NoMedrec')
	    ->where(array('like','p.NAMA','%'.$q.'%'))
	    ->orWhere(array('like','t.PASIEN_ID','%'.$q.'%'))
	    ->group('PASIEN_ID')
	    ->limit(20)
	    ->queryAll();

		$result = array();
		foreach($data as $d)
		{
			$d = (object)$d;
			$value = strtoupper($d->NAMA.' - '.$d->PASIEN_ID);
			$result[] = array(
				'id' => $d->PASIEN_ID,
				'value'=>$value,

			);
		}

		echo CJSON::encode($result);
		
	}

	public function actionGetPasienInRawatInap()
	{

		$q = $_GET['term'];
		$data = Yii::app()->db->createCommand()
	    ->select('id_rawat_inap, p.NAMA, pasien_id')
	    ->from('tr_rawat_inap t')
	    ->join('a_pasien p', 't.pasien_id=p.NoMedrec')
	    ->where(array('like','p.NAMA','%'.$q.'%'))
	    ->orWhere(array('like','t.pasien_id','%'.$q.'%'))
	    // ->group('pasien_id')
	    ->limit(20)
	    ->queryAll();
		// $criteria=new CDbCriteria;
		// $criteria->addSearchCondition('pasien_id',$q,true,'OR');
		// // $criteria->addSearchCondition('pASIEN.NAMA',$q,true,'OR');
		// // $criteria->with = array('pASIEN');
		// // $criteria->together = true;
		// // $criteria->addCondition('t.Status_Pasien="1(PasienMasuk)"');
		// // $criteria->join = 'JOIN td_pasien_masuk p ON t.NoMedrec = p.No_RM';
		// // $criteria->condition = 'p.Status_Pasien = "1(PasienMasuk)" AND (t.NAMA LIKE "%'.$q.'%" OR t.NoMedrec LIKE "%'.$q.'%")';
		
		// $criteria->limit = 20;
		

		// $data = TrRawatInap::model()->findAll($criteria);
		
		// print_r($data);exit;

		$result = array();
		foreach($data as $d)
		{
			$d = (object)$d;
			$value = strtoupper($d->NAMA.' - '.$d->pasien_id);
			$result[] = array(
				'id' => $d->pasien_id,
				'value'=>$value,
				'id_rawat_inap' => $d->id_rawat_inap		
			);
		}

		echo CJSON::encode($result);
		
	}

	public function actionGetPasienInMaster()
	{

		$q = $_GET['term'];

		$criteria=new CDbCriteria;
		$criteria->addSearchCondition('t.NoMedrec',$q,true,'OR');
		$criteria->addSearchCondition('t.NAMA',$q,true,'OR');
		// $criteria->addCondition('t.Status_Pasien="1(PasienMasuk)"');
		// $criteria->join = 'JOIN td_pasien_masuk p ON t.NoMedrec = p.No_RM';
		// $criteria->condition = 'p.Status_Pasien = "1(PasienMasuk)" AND (t.NAMA LIKE "%'.$q.'%" OR t.NoMedrec LIKE "%'.$q.'%")';
		
		$criteria->limit = 20;
		

		$data = Pasien::model()->findAll($criteria);
		
		// print_r($data);exit;

		$result = array();
		foreach($data as $d)
		{
			$value = strtoupper($d->NAMA.' - '.$d->NoMedrec);
			$result[] = array(
				'id' => $d->NoMedrec,
				'value'=>$value,		
			);
		}

		echo CJSON::encode($result);
		
	}

	public function actionGetPasienLikeParam()
	{

		$q = $_GET['term'];

		$data = Yii::app()->db->createCommand()
	    ->select('NODAFTAR, NoMedrec, TGLDAFTAR, JamDaftar, g.KodeGol, g.NamaGol')
	    ->from('b_pendaftaran b')
	    ->join('a_golpasien g','g.KodeGol = b.KodeGol')
	    ->where(array('like','NoMedrec','%'.$q.'%'))
	    ->limit(20)
	    ->order(['NODAFTAR DESC'])
	    ->queryAll();
		
		$result = array();
		foreach($data as $d)
		{
			$d = (object)$d;
			$value = strtoupper('No.RM/No.Daftar: '.Yii::app()->helper->appendZeros($d->NoMedrec,6).'/'.$d->NODAFTAR);
			$result[] = array(
				'id' => $d->NoMedrec,
				'value'=>$value,
				'kode_daftar' => $d->NODAFTAR,
				'kode_gol' => $d->KodeGol,		
				'nama_gol' => $d->NamaGol
			);
		}

		// $result = array();
		// foreach($data as $d)
		// {
		// 	$value = strtoupper($d->NAMA.' - '.$d->NoMedrec);
		// 	$result[] = array(
		// 		'id' => $d->NoMedrec,
		// 		'value'=>$value,		
		// 	);
		// }

		echo CJSON::encode($result);
		
	}


	public function actionGetPasienPendaftaran()
	{
		if(isset($_POST['norm']))
		{
			$data = Yii::app()->db->createCommand()
		    ->select('NODAFTAR, NoMedrec, TGLDAFTAR, JamDaftar, g.KodeGol, g.NamaGol')
		    ->from('b_pendaftaran b')
		    ->join('a_golpasien g','g.KodeGol = b.KodeGol')
		    ->where('b.NoMedrec='.$_POST['norm'])
		    ->limit(10)
		    ->order(['b.NODAFTAR DESC'])
		    ->queryAll();
			
			$result = [];
			if(!empty($data)){
				foreach($data as $item)
				{
					$item = (object)$item;
			

					$value = strtoupper('No.RM/No.Daftar: '.Yii::app()->helper->appendZeros($item->NoMedrec,6).'/'.$item->NODAFTAR);
					$result[] = [
						'id' => $item->NoMedrec,
						'value'=>$value,
						'kode_daftar' => $item->NODAFTAR,
						'kode_gol' => $item->KodeGol,		
						'nama_gol' => $item->NamaGol,
						'tgl_daftar' => date('d/m/Y',strtotime($item->TGLDAFTAR))
					];
				}
			}


			echo CJSON::encode($result);
		}

	}

	public function actionIndex()
	{
		$this->render('index');
	}


}