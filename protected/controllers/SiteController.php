<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$state = Yii::app()->user->getState('isLogin');

		if(!$state)
			$this->actionLogin();
		else{
			if(Yii::app()->user->checkAccess(array(WebUser::R_DIREKTUR)))
			{
				$this->actionDashboard();
			}

			else
				$this->render('index');
		}
	}

	public function actionDashboard()
	{
		$this->render('dashboard');
	}

	public function actionDashboardIncome(){
		$this->render('dashboard_income');
	}

	public function actionDashboardRm(){
		$this->render('dashboard_rm');
	}

	public function actionDashboardOk(){
		$this->render('dashboard_ok');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$error = Yii::app()->errorHandler->error;

        if($error)
        {
            $this->render('error', array('error' => $error));
        }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
	
		$model=new User;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['User']))
		{
		
		
			$model->attributes=$_POST['User'];


			// validate user input and redirect to the previous page if valid
			switch($model->login())
			{
				case UserIdentity::ERROR_NONE:
					$this->redirect(array('site/dashboard'));
					break;
				case UserIdentity::ERROR_USERNAME_INVALID:
				case UserIdentity::ERROR_PASSWORD_INVALID:
					$model->addError('USERNAME','Incorrect username or password.');
					
					break;
				case UserIdentity::ERROR_USER_INACTIVE:
					$model->addError('USERNAME','Akun Anda belum aktif. Silakan menghubungi Administrator.');
					
					break;
			
			}
			
		}
		
		$this->layout = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.column3';

		
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	
	public function actionForget()
	{
	
		$model=new User;
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->validate(array('USERNAME')))
			{
				
				
				
			}
		}
		
		$this->render('forget',array('model'=>$model));
	}
}