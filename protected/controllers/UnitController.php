<?php

class UnitController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','ajaxAddDokter','ajaxAddBiaya','all'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionAll()
	{
		$model=new Unit;
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['Unit']))
			$model->attributes=$_GET['Unit'];

		$this->render('unit_all',array(
			'model'=>$model,
		));
	}	

	public function actionAjaxAddBiaya()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$errors = '';
			$transaction=Yii::app()->db->beginTransaction();
			try
			{

				$dataItem = $_POST['dataItem'];
								

				if(!$dataItem['is_old']){
					$model = new UnitBiaya;
					
				}
				else{
					$model = UnitBiaya::model()->findByPk($dataItem['unit_biaya_id']);
				}

				$model->attributes = $dataItem;
				

				if($model->validate())
				{
					
					$model->save();
					$result = array(
						'shortmsg' => 'success',
						'message' => 'Data telah disimpan',
					);
				}

				else
				{
					
					foreach($model->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}
			
					throw new Exception();

				}
			
				$transaction->commit();

				
				echo CJSON::encode($result);
			}

			catch(Exception $e){
				
				$transaction->rollback();

				$result = array(
					'shortmsg' => 'danger',
					'message' => $e->getMessage(),
				);

				echo CJSON::encode($result);
			}	
		}
	}

	public function actionAjaxAddDokter()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$errors = '';
			$transaction=Yii::app()->db->beginTransaction();
			try
			{

				$dataItem = $_POST['dataItem'];
								

				if(!$dataItem['is_old']){
					$model = new UnitDokter;
					
				}
				else{
					$model = UnitDokter::model()->findByPk($dataItem['unit_dokter_id']);
				}

				$model->attributes = $dataItem;
				

				if($model->validate())
				{
					
					$model->save();
					$result = array(
						'shortmsg' => 'success',
						'message' => 'Data telah disimpan',
					);
				}

				else
				{
					
					foreach($model->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}
			
					throw new Exception();

				}
			
				$transaction->commit();

				
				echo CJSON::encode($result);
			}

			catch(Exception $e){
				
				$transaction->rollback();

				$result = array(
					'shortmsg' => 'danger',
					'message' => $e->getMessage(),
				);

				echo CJSON::encode($result);
			}	
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{


		$dokter=new UnitDokter('search');
		$dokter->unit_id = $id;

		$biaya=new UnitBiaya('search');
		$biaya->unit_id = $id;

		$kepala = new UnitKepala;
		$kepala->unit_id = $id;
		
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'dokter' => $dokter,
			'biaya' => $biaya,
			'kepala' => $kepala
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Unit;

		$unitUser = new UnitUser;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Unit']))
		{
			$model->attributes=$_POST['Unit'];
			$unitUser->attributes=$_POST['UnitUser'];

			if($model->save()){
				$unitUser->unit_id = $model->KodeUnit;
				$unitUser->save();
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'unitUser' => $unitUser
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$unitUser = UnitUser::model()->findByAttributes(['unit_id'=>$id]);

		if(empty($unitUser))
			$unitUser = new UnitUser;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Unit']))
		{
			$model->attributes=$_POST['Unit'];
			$unitUser->attributes=$_POST['UnitUser'];
			$unitUser->unit_id = $model->KodeUnit;

			if($model->save() && $unitUser->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'unitUser' => $unitUser
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Unit('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['Unit']))
			$model->attributes=$_GET['Unit'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Unit('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['Unit']))
			$model->attributes=$_GET['Unit'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Unit the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Unit::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Unit $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='unit-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
