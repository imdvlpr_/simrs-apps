<?php

class BPendaftaranRjalanTindakanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','admin','cetakKwitansi','cetakPengantar'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCetakPengantar($id)
	{
		$model = $this->loadModel($id);
		if(!empty($model))
		{
			try
			{

				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                            'P', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$page_format = array(
				    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 130, 'ury' => 75),
				);

				$pdf->AddPage('P', $page_format, false, false);
				$pdf->SetAutoPageBreak(TRUE, 0);

				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('_pengantar',array(
					'model'=>$model,
				));

				$data = ob_get_clean();
				ob_start();
				$style = array(
				    'position' => '',
				    'align' => 'C',
				    'stretch' => false,
				    'fitwidth' => true,
				    'cellfitalign' => '',
				    'border' => true,
				    'hpadding' => 'auto',
				    'vpadding' => '1',
				    'fgcolor' => array(0,0,0),
				    'bgcolor' => false, //array(255,255,255),
				    'text' => false,
				    'font' => 'helvetica',
				    'fontsize' => 8,
				    'stretchtext' => 5
				);


				$pdf->writeHTML($data);
				$pdf->write1DBarcode($model->bPendaftaranRjalan->noDaftar->noMedrec->NoMedrec, 'C39', '', '', '', 10, 0.4, $style, 'N');
				

				$pdf->Output();

			}
			catch(HTML2PDF_exception $e) {
			    echo $e;
			    exit;
			}

		}
	}


	public function actionCetakKwitansi($id)
	{
		$model = $this->loadModel($id);
		if(!empty($model))
		{
			try
			{

				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                            'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$page_format = array(
				    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 95, 'ury' => 215),
				);

				$pdf->AddPage('L', $page_format, false, false);
				$pdf->SetAutoPageBreak(TRUE, 0);

				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('_kwitansi',array(
					'model'=>$model,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();

			}
			catch(HTML2PDF_exception $e) {
			    echo $e;
			    exit;
			}

		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new BPendaftaranRjalanTindakan;

		$model->b_pendaftaran_rjalan_id = $id;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$unit_id = Yii::app()->user->getState('unit') ?: 0;
		$listTindakan = UnitTindakan::searchTindakanUnit($unit_id);
		if(isset($_POST['BPendaftaranRjalanTindakan']))
		{
			$model->attributes=$_POST['BPendaftaranRjalanTindakan'];
			
			$parent = BPendaftaranRjalan::model()->findByPk($id);

			$pasien = $parent->noDaftar->noMedrec;
			$date = str_replace('/', '-', $parent->noDaftar->TGLDAFTAR);
	    	$jam_registrasi = date('Y-m-d',strtotime($date)).' '.$parent->noDaftar->JamDaftar;
	    	$jam_pelayanan = date('Y-m-d H:i:s');
	    	$elapsed = Yii::app()->helper->hitungDurasiMenit($jam_registrasi,$jam_pelayanan);
			$model->durasi = $elapsed;

			$elapsed_label = Yii::app()->helper->hitungDurasi($jam_registrasi,$jam_pelayanan);
			$model->durasi_label = $elapsed_label;


			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin','id'=>$model->b_pendaftaran_rjalan_id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'listTindakan' => $listTindakan
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BPendaftaranRjalanTindakan']))
		{
			$model->attributes=$_POST['BPendaftaranRjalanTindakan'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin','id'=>$model->b_pendaftaran_rjalan_id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{

		$this->actionAdmin($id);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{ 
		$model=new BPendaftaranRjalanTindakan('search');
		$model->unsetAttributes();  // clear any default values
		
		$model->b_pendaftaran_rjalan_id = $id;
		$parent = BPendaftaranRjalan::model()->findByPk($id);

		$pasien = $parent->noDaftar->noMedrec;

		$firstPelayanan = BPendaftaranRjalanTindakan::model()->findByAttributes(['b_pendaftaran_rjalan_id'=>$id],['order'=>'id ASC']);

		$dmTindakan = DmTindakan::model()->findByAttributes(['kode'=>'K001']);

		$dokterUnit = UnitDokter::model()->findByAttributes(['unit_id'=>$parent->KodePoli]);
		
		$tmp = BPendaftaranRjalanTindakan::model()->findByAttributes([
			'b_pendaftaran_rjalan_id'=> $id,
			'dm_poli_tindakan_id'=> $dmTindakan->id
		]);

		
		if(empty($tmp) && !empty($dokterUnit)){
			$tmp = new BPendaftaranRjalanTindakan;
			$tmp->dm_poli_tindakan_id = $dmTindakan->id;
			$tmp->b_pendaftaran_rjalan_id = $id;
			$tmp->dokter_id = $dokterUnit->dokter_id;
			$tmp->tanggal = date('Y-m-d');
			$tmp->keterangan = 'Konsul Poli';
		
			$tmp->durasi = 0;
			$tmp->durasi_label = '';
			$tmp->biaya = $dmTindakan->biaya;
			
			$tmp->save();
		}

		else if(!empty($tmp)){
			$tmp->biaya = $dmTindakan->biaya;
			
			$tmp->save();
		}

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['BPendaftaranRjalanTindakan']))
			$model->attributes=$_GET['BPendaftaranRjalanTindakan'];

		// print_r($id);exit;
		$this->render('index',array(
			'model'=>$model,
			'pasien' => $pasien,
			'parent'=>$parent,
			'firstPelayanan' => $firstPelayanan
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BPendaftaranRjalanTindakan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BPendaftaranRjalanTindakan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BPendaftaranRjalanTindakan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bpendaftaran-rjalan-tindakan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
