<?php

class DmLabItemController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			],
			['allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','ajaxGetItem','delete'),
				'users'=>array('@'),
			],
			['allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			],
			['deny',  // deny all users
				'users'=>array('*'),
			],
		];
	}

	public function actionAjaxGetItem()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$q = $_GET['term'];

			$model = DmLabItem::model()->searchByNama($q);
			

			$result = array();
			
			if(!empty($model))
				foreach($model as $m)
				{
					$full = $m->nama;
						
					$result[] = array(
						'id' => $m->id,
						'value' => $full,
					);
				}

			echo CJSON::encode($result);
		}
	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',[
			'model'=>$this->loadModel($id),
		]);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DmLabItem;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$listKategori = DmLabKategori::model()->findAll();
		$listGrup = DmLabGrup::model()->findAll(['order'=>'nama ASC']);
		$listKelas = DmKelas::model()->findAll();
		
		if(isset($_POST['DmLabItem']))
		{
			$model->attributes=$_POST['DmLabItem'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(['index']);
			}
		}

		$this->render('create',[
			'model'=>$model,
			'listKategori' => $listKategori,
			'listKelas' => $listKelas,
			'listGrup' => $listGrup,
		]);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$listKategori = DmLabKategori::model()->findAll();
		$listGrup = DmLabGrup::model()->findAll(['order'=>'nama ASC']);
		$listKelas = DmKelas::model()->findAll();
		
		if(isset($_POST['DmLabItem']))
		{
			$model->attributes=$_POST['DmLabItem'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(['index']);
			}
		}

		$this->render('update',[
			'model'=>$model,
			'listKategori' => $listKategori,
			'listKelas' => $listKelas,
			'listGrup' => $listGrup,
		]);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->is_removed = 1;
		$model->save();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new DmLabItem('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['DmLabItem']))
			$model->attributes=$_GET['DmLabItem'];

		$this->render('index',[
			'model'=>$model,
		]);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DmLabItem('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['DmLabItem']))
			$model->attributes=$_GET['DmLabItem'];

		$this->render('admin',[
			'model'=>$model,
		]);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DmLabItem the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=DmLabItem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param DmLabItem $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='dm-lab-item-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
