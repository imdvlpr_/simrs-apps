<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','approveUser','disapproveUser'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionDisapproveUser($id)
	{
		if(!empty($id))
		{
			if( Yii::app()->request->isAjaxRequest )
			{
				$model = User::model()->findByPk($id);

			    // Stop jQuery from re-initialization
			    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
			 
			    if( isset( $_POST['action'] ) && $_POST['action'] == 'confirmApprove' )
			    {
				
			    	$model->STATUS = 0;
					$model->update(array('STATUS'));
					
					echo CJSON::encode( array(
							'status' => 'success',
							'content' => 'Sukses',
					   ));
			    		
			       exit;	
			    	
			       
			    }
			    else if( isset( $_POST['action'] ) )
			    {
			       echo CJSON::encode( array(
			         'status' => 'canceled',
			         'content' => 'Batal Deaktivasi Pengguna',
			       ));
			      exit;
			    }
			    else
			    {

			      echo CJSON::encode( array(
			        'status' => 'failure',
			        'content' => $this->renderPartial( 'disapprove_user', array(
			        	'model' => $model
			           ), true, true ),
			      ));
			      exit;
			    }
			}
			else
			{
			    if( isset( $_POST['confirmApprove'] ) )
			    {
			      // $model->delete();
			       $this->redirect( array( 'admin' ) );
			    }
			    else if( isset( $_POST['denyDelete'] ) )
			       $this->redirect( array( 'admin' ) );
			    else
			       $this->render( 'disapprove_user',array(
			       		
			       	));
			}
		}
	}

	public function actionApproveUser($id)
	{
		if(!empty($id))
		{
			if( Yii::app()->request->isAjaxRequest )
			{
				$model = User::model()->findByPk($id);

			    // Stop jQuery from re-initialization
			    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
			 
			    if( isset( $_POST['action'] ) && $_POST['action'] == 'confirmApprove' )
			    {
				
			    	$model->STATUS = 1;
					$model->update(array('STATUS'));
					
					echo CJSON::encode( array(
							'status' => 'success',
							'content' => 'Sukses',
					   ));
			    		
			       exit;	
			    	
			       
			    }
			    else if( isset( $_POST['action'] ) )
			    {
			       echo CJSON::encode( array(
			         'status' => 'canceled',
			         'content' => 'Batal Aktivasi Pengguna',
			       ));
			      exit;
			    }
			    else
			    {

			      echo CJSON::encode( array(
			        'status' => 'failure',
			        'content' => $this->renderPartial( 'approve_user', array(
			        	'model' => $model
			           ), true, true ),
			      ));
			      exit;
			    }
			}
			else
			{
			    if( isset( $_POST['confirmApprove'] ) )
			    {
			      // $model->delete();
			       $this->redirect( array( 'admin' ) );
			    }
			    else if( isset( $_POST['denyDelete'] ) )
			       $this->redirect( array( 'admin' ) );
			    else
			       $this->render( 'approve_user',array(
			       		
			       	));
			}
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;
		$model->setScenario('repeatPwd');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->STATUS = 1;
			$model->euid = $_POST['User']['euid'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan");
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->setScenario('repeatPwd');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->euid = $_POST['User']['euid'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan");
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values


		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['level']))
			$model->LEVEL=$_GET['level'];		

		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
