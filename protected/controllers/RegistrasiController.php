<?php

class RegistrasiController extends Controller
{

	


	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('cekPasien','pasien','rawatJalan','bpjs','umum','kunjungan'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionKunjungan()
	{

		$this->title = 'Kunjungan Hari Ini | '.Yii::app()->name;
		$model=new TrPendaftaranRjalan;
		$model->unsetAttributes();  // clear any default values

		$poli = DmPoli::model()->findAll();
		
		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['poli1']))
			$model->poli1_id=$_GET['poli1'];
		


		$this->render('kunjungan',array(
			'model'=>$model,
			'poli' => $poli,
		));
	}

	public function actionUmum($id)
	{

		

		$model = new TrPendaftaranRjalan;
		$nomedrec = '';
		if(!empty($id))
			$nomedrec = $id;

	
		$view = '_form_rawat_jalan_umum';
				
		if(!empty($nomedrec))
		{
			$pasien=Pasien::model()->findByPk($nomedrec);
			$bpjsPasien = new BpjsPasien;
		

			$selisih_umur = 0;
			if(!empty($pasien))
			{
			
				$model->NoMedrec = $pasien->NoMedrec;
				if(isset($_POST['TrPendaftaranRjalan']))
				{
					$model->attributes = $_POST['TrPendaftaranRjalan'];
					if(!empty($_POST['TrPendaftaranRjalan']['BATAS_CETAK_SEP']))
						$model->BATAS_CETAK_SEP = $_POST['TrPendaftaranRjalan']['BATAS_CETAK_SEP'];
					
					if($model->validate())
					{
						$model->save();
						$this->redirect(Yii::app()->createUrl('TrPendaftaranRjalan/index'));
					}
				}

				else
				{

					$model->NoDaftar = Yii::app()->helper->generateNoDaftar();
					$model->WAKTU_DAFTAR = date('Y-m-d H:i:s');

				}

				$selisih_umur = Yii::app()->helper->getSelisihTanggal($pasien->TGLLAHIR, date('Y-m-d'));
				$kunjunganKe = TrPendaftaranRjalan::model()->countKunjungan($model->NoMedrec);

				$this->render('form_rawat_jalan',array(
					'model'=>$model,
					'pasien' => $pasien,
					'umur' => $selisih_umur,
					'kunjunganKe' => $kunjunganKe + 1,
					'bpjsPasien' => $bpjsPasien,
					'view' => $view
				));
			}
			else
			{
				$pasien = new Pasien;
				$this->render('form_rawat_jalan',array(
					'model'=>$model,
					'pasien' => $pasien,
					'umur' => -1,
					'kunjunganKe' => -1,
					'bpjsPasien' => $bpjsPasien,
					'view' => $view
				));
			}
		}
		
	}

	public function actionBpjs($id)
	{
		$model = new TrPendaftaranRjalan;
		$nomedrec = '';
		if(!empty($id))
			$nomedrec = $id;

	
		if(!empty($nomedrec))
		{
			$pasien=Pasien::model()->findByPk($nomedrec);
			$bpjsPasien = new BpjsPasien;
		

			$selisih_umur = 0;
			if(!empty($pasien))
			{
			
				$model->NoMedrec = $pasien->NoMedrec;
				if(isset($_POST['TrPendaftaranRjalan']))
				{
					$model->attributes = $_POST['TrPendaftaranRjalan'];
					if(!empty($_POST['TrPendaftaranRjalan']['BATAS_CETAK_SEP']))
						$model->BATAS_CETAK_SEP = $_POST['TrPendaftaranRjalan']['BATAS_CETAK_SEP'];
					
					if($model->validate())
					{
						$model->save();
						$this->redirect(Yii::app()->createUrl('TrPendaftaranRjalan/index'));
					}
				}

				else
				{

					$model->NoDaftar = Yii::app()->helper->generateNoDaftar();
					$model->WAKTU_DAFTAR = date('Y-m-d H:i:s');
					$batasSEP = $model->cekBatasSEPFromGoogle($model->WAKTU_DAFTAR);
					$model->BATAS_CETAK_SEP = $batasSEP['tanggal'];
				}

				$selisih_umur = Yii::app()->helper->getSelisihTanggal($pasien->TGLLAHIR, date('Y-m-d'));
				$kunjunganKe = TrPendaftaranRjalan::model()->countKunjungan($model->NoMedrec);


				$this->render('bpjs',array(
					'model'=>$model,
					'pasien' => $pasien,
					'umur' => $selisih_umur,
					'kunjunganKe' => $kunjunganKe + 1,
					'bpjsPasien' => $bpjsPasien
				));
			}
			else
			{
				$pasien = new Pasien;
				$this->render('bpjs',array(
					'model'=>$model,
					'pasien' => $pasien,
					'umur' => -1,
					'kunjunganKe' => -1,
					'bpjsPasien' => $bpjsPasien
				));
			}
		}
		
	}

	public function actionRawatJalan($id)
	{

		

		$model = new TrPendaftaranRjalan;
		$nomedrec = '';
		if(!empty($id))
			$nomedrec = $id;

		$view = '_form_rawat_jalan';
	
		if(!empty($nomedrec))
		{

			$poli = Poli::model()->findAll();

			$pasien=Pasien::model()->findByPk($nomedrec);
			$bpjsPasien = new BpjsPasien;
		

			$selisih_umur = 0;
			if(!empty($pasien))
			{
			
				$model->NoMedrec = $pasien->NoMedrec;
				if(isset($_POST['TrPendaftaranRjalan']))
				{
					$model->attributes = $_POST['TrPendaftaranRjalan'];
					$model->poli1_id = $_POST['TrPendaftaranRjalan']['poli1_id'];
					$model->poli2_id = $_POST['TrPendaftaranRjalan']['poli2_id'];
					$model->poli3_id = $_POST['TrPendaftaranRjalan']['poli3_id'];

					if(!empty($_POST['TrPendaftaranRjalan']['BATAS_CETAK_SEP']))
						$model->BATAS_CETAK_SEP = $_POST['TrPendaftaranRjalan']['BATAS_CETAK_SEP'];
					
					if($model->validate())
					{
						$model->save();
						$this->redirect(Yii::app()->createUrl('TrPendaftaranRjalan/index'));
					}
				}

				else
				{

					$model->NoDaftar = Yii::app()->helper->generateNoDaftar();
					$model->WAKTU_DAFTAR = date('Y-m-d H:i:s');
					// $batasSEP = $model->cekBatasSEPFromGoogle($model->WAKTU_DAFTAR);
					// $model->BATAS_CETAK_SEP = $batasSEP['tanggal'];
				}

				$selisih_umur = Yii::app()->helper->getSelisihTanggal($pasien->TGLLAHIR, date('Y-m-d'));
				$kunjunganKe = TrPendaftaranRjalan::model()->countKunjungan($model->NoMedrec);


				$this->render('form_rawat_jalan',array(
					'model'=>$model,
					'pasien' => $pasien,
					'umur' => $selisih_umur,
					'kunjunganKe' => $kunjunganKe + 1,
					'bpjsPasien' => $bpjsPasien,
					'view' => $view,
					'poli' => $poli
				));
			}
			else
			{
				$pasien = new Pasien;
				$this->render('form_rawat_jalan',array(
					'model'=>$model,
					'pasien' => $pasien,
					'umur' => -1,
					'kunjunganKe' => -1,
					'bpjsPasien' => $bpjsPasien,
					'view' => $view,
					'poli' => $poli
				));
			}
		}
		
	}

	public function actionPasien($jenis,$nomor = '')
	{
		$this->title = 'Pendaftaran Pasien | '.Yii::app()->name;
		$lama = 'baru';

		if(!empty($nomor))
		{
			$model = Pasien::model()->findByPk($nomor);
			if(!empty($model))
				$lama = 'lama';
				
			
		}
		else
		{
			$model=new Pasien;
			$model->NoMedrec = Yii::app()->helper->getMinimumUnusedID();

		}

		$bpjsPasien = new BpjsPasien;

		if(isset($_POST['BpjsPasien']) && isset($_POST['Pasien']))
		{

			$m = BpjsPasien::model()->findByPk($_POST['BpjsPasien']['NoKartu']);

			if(!empty($m))
				$bpjsPasien = $m;

			if(!$bpjsPasien->isNewRecord)
			{
				$model = Pasien::model()->findByPk($bpjsPasien->PASIEN_ID);
			}
			
			$bpjsPasien->attributes = $_POST['BpjsPasien'];
			$model->attributes = $_POST['Pasien'];
			$bpjsPasien->PASIEN_ID = $model->NoMedrec;
			$bpjsPasien->NAMA_KEPESERTAAN = $_POST['PESERTA_NAMA'];
			$bpjsPasien->NAMA_PISAT = $_POST['pisat_nama'];
			$bpjsPasien->NAMA_KELASRAWAT = $_POST['KELASRAWAT_NAMA'];

			$transaction=Yii::app()->db->beginTransaction();
			try
			{
				if($bpjsPasien->validate() && $model->validate()){
					$model->save();

					$bpjsPasien->save();
					


					$transaction->commit();	
					

					$this->redirect(Yii::app()->createUrl('bpjs/sep',array('idPasien'=>$model->NoMedrec,'jr' => $_POST['jenis_rawat'])));
					
				}
				else{
					throw new Exception("Error Processing Request", 1);
					
				}

			}
		
			catch(Exception $e){
				$transaction->rollback();
			}
			
		}

		else if(isset($_POST['Pasien']))
		{
			$model->attributes = $_POST['Pasien'];

			$var = $_POST['Pasien']['TGLLAHIR'];
			$date = str_replace('/', '-', $var);
			$model->TGLLAHIR = date('Y-m-d', strtotime($date));
	
			if($model->validate())
			{

				$model->save();
				

				switch($_POST['jenis_rawat'])
				{
					case 1: // Rawat Inap 
						$kepesertaan = $_POST['Kepesertaan'];
						$this->redirect(Yii::app()->createUrl('trRawatInap/create',array('rm'=>$model->NoMedrec,'jp'=>$kepesertaan)));
						break;

					case 2: // Rawat Jalan
						$this->redirect(Yii::app()->createUrl('registrasi/rawatJalan',array('id'=>$model->NoMedrec)));
						break;
				}
				
			}

		
			
		}



		if($jenis == 'bpjs')
		{


			$this->render('_form_pasien_bpjs',array(
				'model'=>$model,
				'bpjsPasien' => $bpjsPasien,
				'jenis' => $jenis,
				'lama' => $lama
			));
		}
		else{

			$this->render('_form_pasien_umum',array(
				'model'=>$model,
				
				'lama' => $lama,
				'jenis' => $jenis
			));
		}
	}

	public function actionCekPasien()
	{
		$this->title = 'Pendaftaran Pasien Lama | '.Yii::app()->name;
		$model=new Pasien('search');
		$model->unsetAttributes();  // clear any default values
		$model->PAGE_SIZE = 20;

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['cekby']))
			$model->CEKBY=$_GET['cekby'];

		if(isset($_GET['Pasien']))
			$model->attributes=$_GET['Pasien'];

	
		$this->render('cekPasien',array(
			'model'=>$model,
		));

		
	}
}