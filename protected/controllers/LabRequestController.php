<?php

class LabRequestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			],
			['allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','ajaxSimpan','list','printPengantar','printHasil','edit','ajaxSimpanHasil','printKimiaKlinik','printRincian','delete'),
				'users'=>array('@'),
			],
			['allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			],
			['deny',  // deny all users
				'users'=>array('*'),
			],
		];
	}

	public function actionPrintRincian($id)
	{
		$model=$this->loadModel($id);

		$rawatInap = TrRawatInap::model()->findByPk($model->rawat_inap_id);
		$rawatRincian = $rawatInap->trRawatInapRincians;
		
		$pasien = $model->pasien;
		$html = $this->renderPartial('_printRincian',array(
			'model'=>$model,
			'pasien' => $pasien,
			'rawatRincian' => $rawatRincian,
			'rawatInap' => $rawatInap
		),true,true);

		try
		{
			ob_start();
			$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'P', 'mm', 'Legal', true, 'UTF-8');

			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			
			$pdf->AddPage();
			$pdf->SetAutoPageBreak(TRUE, 0);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->writeHTML($html, true, false, true, false, '');
			//$pdf->WriteHTMLCell(0,0,0,0 ,$html);
			$pdf->Output('hasil_lab.pdf', "I");
			Yii::app()->end();
			ob_end_clean();
		}

		catch(HTML2PDF_exception $e) 
		{
		    echo $e;
		    exit;
		}
	}

	public function actionPrintKimiaKlinik($id)
	{
		$model=$this->loadModel($id);

		$rawatInap = TrRawatInap::model()->findByPk($model->rawat_inap_id);
		$rawatRincian = $rawatInap->trRawatInapRincians;
		
		$pasien = $model->pasien;
		
		$html = $this->renderPartial('_printKimiaKlinik',array(
			'model'=>$model,
			'pasien' => $pasien,
			'rawatRincian' => $rawatRincian,
			'rawatInap' => $rawatInap
		),true,true);



		try
		{
			ob_start();
			$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'P', 'mm', 'Legal', true, 'UTF-8');

			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			
			$pdf->AddPage();
			$pdf->SetAutoPageBreak(TRUE, 0);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->writeHTML($html, true, false, true, false, '');
			//$pdf->WriteHTMLCell(0,0,0,0 ,$html);
			$pdf->Output('hasil_lab.pdf', "I");
			Yii::app()->end();
			ob_end_clean();
		}

		catch(HTML2PDF_exception $e) 
		{
		    echo $e;
		    exit;
		}
	}

	public function actionList($pid='')
	{
		$model=new LabRequest('search');
		$model->unsetAttributes();  // clear any default values
		$model->pasien_id = $pid;

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['LabRequest']))
			$model->attributes=$_GET['LabRequest'];

		$this->render('list',[
			'model'=>$model,
		]);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$items = new LabRequestItem('search');
		$items->request_id = $id;
		$items->pasien_id = $model->pasien_id;
		$rawatInap = TrRawatInap::model()->findByAttributes(['id_rawat_inap'=>$model->rawat_inap_id]);
		$rawatRincian = $rawatInap->trRawatInapRincians;
		
		$pasien = $model->pasien;
		$this->layout = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.lite';
		$this->render('view',[
			'model'=>$model,
			'items' => $items,
			'pasien' => $pasien,
			'rawatInap' => $rawatInap
		]);
	}

	public function actionAjaxSimpanHasil()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$errors = '';
			$result = [];
			$transaction=Yii::app()->db->beginTransaction();
			try
			{
				parse_str($_POST['dataPost'],$dataPost);
				// print_r($dataPost);exit;
				
				$total_japel = 0;
				$total_bhp = 0;
				$total_biaya = 0;

				$index = 0;
				$labRequestId =$dataPost['LabRequest']['id'];
				$main_labRequestItems = LabRequestItem::model()->findAllByAttributes(['request_id'=>$labRequestId]);
				foreach($main_labRequestItems as $item)
				{					

					$items = $item->labRequestItemKomponens;
					foreach($items as $q => $child)
					{
						$ch = $child;
						$hasil = $dataPost['hasil_'.$ch->id];
						
						$tmp = LabRequestItemKomponen::model()->findByPk($ch->id);
						$tmp->hasil = $hasil;		
						$tmp->save();
					}

					$tarif = DmLabItemTarif::model()->findByAttributes([
						'item_id' => $item->parent->parent_id,
						'kelas_id' => $item->request->kelas_id
					]);

					$item->bhp = !empty($tarif) ? $tarif->bhp : 0;
					$item->japel = !empty($tarif) ? $tarif->japel : 0;
					$item->biaya = !empty($tarif) ? $tarif->jumlah : 0;
					$item->save();

				}
				
				$model = $this->loadModel($dataPost['LabRequest']['id']);
				$model->attributes = $dataPost['LabRequest'];
				if(empty($dataPost['LabRequest']['tanggal_selesai_uji']))
					$model->tanggal_selesai_uji = null;

				if(empty($dataPost['LabRequest']['tanggal_terima_sampel']))
					$model->tanggal_terima_sampel = null;

				$model->status_pelayanan = 1;

				$model->save();
				
				$rawatInap = TrRawatInap::model()->findByPk($model->rawat_inap_id);
				if(!empty($rawatInap))
				{	
					$penunjang = TindakanMedisPenunjang::model()->findByAttributes(['nama_tindakan'=>'Laboratorium']);
					
					if(!empty($penunjang))
					{
							

						$list_cek_lab = LabRequest::model()->findAllByAttributes([
							'rawat_inap_id' => $model->rawat_inap_id,
						]);

						$total_cek_lab = 0;
						foreach($list_cek_lab as $p)
						{
							foreach($p->labRequestItems as $sb)
							{
								$total_cek_lab += $sb->biaya;	
							}
						}



						$tr_penunjang = TrRawatInapPenunjang::model()->findByAttributes([
							'id_rawat_inap' => $model->rawat_inap_id,
							'id_penunjang' => $penunjang->id_tindakan_penunjang
						]);

						if(empty($tr_penunjang))
						{
							$tr_penunjang = new TrRawatInapPenunjang;
							$tr_penunjang->id_rawat_inap = $model->rawat_inap_id;
							$tr_penunjang->biaya_ird = 0;
							$tr_penunjang->jumlah_tindakan = 1;
							$tr_penunjang->id_penunjang = $penunjang->id_tindakan_penunjang;
						}

						$tr_penunjang->biaya_irna = $total_cek_lab;
						$tr_penunjang->save();

					}
				}
				// $tmp->item_id = $_POST['']

				$result = array(
					'code' => 200,
					'shortmsg' => 'success',
					'message' => 'Data telah disimpan',
				);
				
				

				$transaction->commit();

				echo CJSON::encode($result);
			}

			catch(Exception $e){
				$errors = $errors.$e->getMessage();
				$transaction->rollback();

				$result = array(
					'code' => 500,
					'shortmsg' => 'danger',
					'message' => $errors,
				);

				echo CJSON::encode($result);
			}	
		}
	}

	public function actionAjaxSimpan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$errors = '';
			$result = [];
			$transaction=Yii::app()->db->beginTransaction();
			try
			{

				parse_str($_POST['dataPost'],$dataPost);
				$model = new LabRequest;
				$model->rawat_inap_id = $dataPost['LabRequest']['rawat_inap_id'];
				$model->attributes=$dataPost['LabRequest'];
				if($model->reg_id == 0)
					$model->reg_id = null;

				if($model->save())
				{

					foreach($dataPost['item_id'] as $item)
					{
						if(empty($item)) continue;
						$dmItem = DmLabItem::model()->findByPk($item);
						$parent = DmLabItemParent::model()->findByAttributes(['parent_id'=>$item]);
						$tmp = new LabRequestItem;
						
						if(empty($parent)){
							$errors .= 'Data item '.$dmItem->nama.' belum dipetakan di Item Parent';
							throw new Exception;
						}

						$tmp->request_id = $model->id;	
						$tmp->parent_id = $parent->id;



						$tmp->save();
						$labItem = $dmItem->dmLabItemParents;

						// print_r($labItem);exit;
						foreach($labItem as $it)
						{
							$subitem = $it->item;

							$sub = $subitem->dmLabItemParents;
							
							if(count($sub) > 0)
							{
								foreach($sub as $subit)
								{
									// echo $subit->parent_id.'<br>';
									$labit = new LabRequestItemKomponen;
									$labit->lab_request_item_id = $tmp->id;	
									$labit->item_id = $subit->id;
									if(!$labit->save())
									{
										foreach($labit->getErrors() as $attribute){
											foreach($attribute as $error){
												$errors .= $error.' ';
											}
										}

										throw new Exception;
									}
								}
							}

							else
							{
								$labit = new LabRequestItemKomponen;
								$labit->lab_request_item_id = $tmp->id;	
								$labit->item_id = $it->id;
								if(!$labit->save())
								{
									foreach($labit->getErrors() as $attribute){
										foreach($attribute as $error){
											$errors .= $error.' ';
										}
									}

									throw new Exception;
								}
							}
								
						}
						
					}
					// exit;
					
					// $tmp->item_id = $_POST['']

					$result = array(
						'code' => 200,
						'shortmsg' => 'success',
						'message' => 'Data telah disimpan',
					);
				}

				else
				{
					foreach($model->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}

					throw new Exception;
					
				}

				$unit = UnitUser::model()->findByAttributes(['user_id'=>'laboran']);
				$notif = new Notif;
				$notif->unit_from_id = Yii::app()->user->level;
				$notif->unit_to_id = $unit->unit_id;
				$notif->keterangan = 'New Lab Check Request from '.$model->rawatInap->kamar->nama_kamar;
				$notif->item_id = $model->id;
				if(!$notif->save())
				{
					foreach($notif->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}

					throw new Exception("Error Processing Request", 1);
				}

				$transaction->commit();

				echo CJSON::encode($result);
			}

			catch(Exception $e){
				$errors = $errors.$e->getMessage();
				$transaction->rollback();

				$result = array(
					'code' => 500,
					'shortmsg' => 'danger',
					'message' => $errors,
				);

				echo CJSON::encode($result);
			}	
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new LabRequest;
		$rawatInap = TrRawatInap::model()->findByPk($id);
		
		$rawatRincian = $rawatInap->trRawatInapRincians;

		$pasien = $rawatInap->pASIEN;
		$model->pasien_id = $pasien->NoMedrec;
		$model->rawat_inap_id = !empty($rawatInap) ? $rawatInap->id_rawat_inap : null;
		$model->unit_id = !empty($rawatInap) ? $rawatInap->kamar->kamarMaster->unit_id : '';
		$model->reg_id = !empty($rawatInap) ? $rawatInap->kode_rawat : '';
		$model->kelas_id = !empty($rawatInap) ? $rawatInap->kamar->kelas_id : '';

		$listGrup = DmLabGrup::model()->findAll(['order'=>'nama ASC']);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		// if(isset($_POST['LabRequest']))
		// {
		// 	$model->attributes=$_POST['LabRequest'];
		// 	if($model->save()){

		// 		$tmp = new LabRequestItem;
		// 		$tmp->request_id = $model->id;
		// 		$tmp->item_id = $_POST['']

		// 		Yii::app()->user->setFlash('success', "Data telah tersimpan.");
		// 		$this->redirect(['index']);
		// 	}
		// }

		$this->render('create',[
			'model'=>$model,
			'pasien' => $pasien,
			'rawatRincian' => $rawatRincian,
			'rawatInap' => $rawatInap,
			'listGrup' => $listGrup
		]);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// $criteria = new CDbCriteria;
		// $criteria->condition = 'labRequestItem.request_id = '.$id;
		// $criteria->with = ['labRequestItem','item','item.item','item.item.grup'];
		// $criteria->together = true;
		// $criteria->order = 'g.nama ASC';

		$results = [];//LabRequestItemKomponen::model()->findAll($criteria);
		// exit;
// 		SELECT k.id, g.nama, i.nama, k.hasil  FROM lab_request_item_komponen k 
// JOIN lab_request_item ri ON ri.id = k.lab_request_item_id
// JOIN dm_lab_item_parent p ON p.id = k.item_id
// JOIN dm_lab_item i ON i.id = p.item_id
// JOIN dm_lab_grup g ON g.id = i.grup_id
// WHERE ri.request_id = 39

		// $dmLabGrups = DmLabGrup::model()->findAll();

		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		// $main_labRequestItems = LabRequest::model()->findByPk($id);

		// foreach($main_labRequestItems as $m)
		// {
		// 	$crit = new CDbCriteria;
		// 	$crit->condition = 'level = 2 OR level = 3 AND request_id = '.$id;
		// 	$list = LabRequestItem::model()->findAll($crit);
		// 	$tm = [];
		// 	foreach($list as $l)
		// 	{
		// 		$tm[] = [
		// 			'item' => $l,
		// 		];
		// 	}

		// 	$childs_labRequestItems[$m->id] = [
		// 		'items' => $tm,
		// 	];
		// }


		// // print_r($childs_labRequestItems);exit;
		if(isset($_POST['LabRequest']))
		{
			$model->attributes=$_POST['LabRequest'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(['index']);
			}
		}

		
		$rawatInap = TrRawatInap::model()->findByAttributes(['id_rawat_inap'=>$model->rawat_inap_id]);
		$rawatRincian = $rawatInap->trRawatInapRincians;
		
		$pasien = $model->pasien;

		$this->render('update',[
			'model'=>$model,
			'pasien' => $pasien,
			'rawatRincian' => $rawatRincian,
			'rawatInap' => $rawatInap,
			'results' => $results
			// 'main_labRequestItems' => $main_labRequestItems,
			// 'childs_labRequestItems' => $childs_labRequestItems
		]);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		$rawatInap = TrRawatInap::model()->findByPk($model->rawat_inap_id);
		if(!empty($rawatInap))
		{	
			$penunjang = TindakanMedisPenunjang::model()->findByAttributes(['nama_tindakan'=>'Laboratorium']);
			
			if(!empty($penunjang))
			{
				
				$tr_penunjang = TrRawatInapPenunjang::model()->findByAttributes([
					'id_rawat_inap' => $model->rawat_inap_id,
					'id_penunjang' => $penunjang->id_tindakan_penunjang
				]);

				if(empty($tr_penunjang))
				{
					$tr_penunjang = new TrRawatInapPenunjang;
					$tr_penunjang->id_rawat_inap = $model->rawat_inap_id;
					$tr_penunjang->biaya_ird = 0;
					$tr_penunjang->jumlah_tindakan = 1;
					$tr_penunjang->id_penunjang = $penunjang->id_tindakan_penunjang;
				}

				$tr_penunjang->biaya_irna = 0;
				$tr_penunjang->save();

			}
		}



		foreach($model->labRequestItems as $item)
		{
			foreach($item->labRequestItemKomponens as $k)
				$k->delete();

			$item->delete();
		}

		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($pid='')
	{
		$model=new LabRequest('search');
		$model->unsetAttributes();  // clear any default values
		$model->pasien_id = $pid;

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['LabRequest']))
			$model->attributes=$_GET['LabRequest'];

		$this->render('index',[
			'model'=>$model,
		]);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new LabRequest('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['LabRequest']))
			$model->attributes=$_GET['LabRequest'];

		$this->render('admin',[
			'model'=>$model,
		]);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LabRequest the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=LabRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LabRequest $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='lab-request-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
