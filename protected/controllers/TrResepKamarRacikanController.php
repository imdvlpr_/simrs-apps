<?php

class TrResepKamarRacikanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionPrint($id)
	{
		$model = $this->loadModel($id);
		if(!empty($model))
		{
			try
			{
				 ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                            'L', 'mm', 'Legal', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$page_format = array(
				    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 68, 'ury' => 43),
				    'PZ' => 1,
				);
				$pdf->SetFont('helvetica', '', 8);


				$pdf->AddPage('L', $page_format, false, false);
				$pdf->SetAutoPageBreak(TRUE, 0);
				// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				// $pdf->writeHTML($html, true, false, false, false, '');
				$textWidth = $pdf->GetStringWidth('INSTALASI FARMASI');
				$pdf->Text(68/2 - $textWidth/2,1, 'INSTALASI FARMASI');

				$textWidth = $pdf->GetStringWidth('RSUD KABUPATEN KEDIRI');
				$pdf->Text(68/2 - $textWidth/2,5, 'RSUD KABUPATEN KEDIRI');

				$topPos = 10;
				$topPosIncr = 4;
				$leftPos = 20; 
				$leftStartPos = 3;
				$pdf->Text($leftStartPos,$topPos, 'No Resep');
				$pdf->Text($leftPos,$topPos, ':');
				$pdf->Text($leftPos+2,$topPos, strtoupper($model->trResepKamar->no_resep));

				$topPos += $topPosIncr;
				$pdf->Text($leftStartPos,$topPos, 'Tanggal');
				$pdf->Text($leftPos,$topPos, ':');
				$pdf->Text($leftPos+2,$topPos, $model->trResepKamar->tgl_resep);
				$topPos += $topPosIncr;
				$pdf->Text($leftStartPos,$topPos, 'Nama Px');
				$pdf->Text($leftPos,$topPos, ':');
				$pdf->Text($leftPos+2,$topPos, $model->trResepKamar->trResepPasien->pasien->NAMA);
				$topPos += $topPosIncr;
				$pdf->Text($leftStartPos,$topPos, 'No RM');
				$pdf->Text($leftPos,$topPos, ':');
				$pdf->Text($leftPos+2,$topPos, $model->trResepKamar->trResepPasien->pasien->NoMedrec);
				
				$topPos += $topPosIncr;
				$pdf->Text($leftStartPos,$topPos, 'Tgl Lahir');
				$pdf->Text($leftPos,$topPos, ':');
				$pdf->Text($leftPos+2,$topPos, date('d/m/Y',strtotime($model->trResepKamar->trResepPasien->pasien->TGLLAHIR)));
				$topPos += $topPosIncr;
				$pdf->Text($leftStartPos,$topPos, 'Nama Obat');
				$pdf->Text($leftPos,$topPos, ':');
				$pdf->Text($leftPos+2,$topPos, $model->trObatRacikan->nama);
				$topPos += $topPosIncr;
				$pdf->Text($leftStartPos,$topPos, 'Aturan');
				$pdf->Text($leftPos,$topPos, ':');
				$pdf->Text($leftPos+2,$topPos, $model->signa1.' X '.$model->signa2.' '.$model->aturan);

				$pdf->Output("aturan_pakai.pdf", "I");
			}
			catch(HTML2PDF_exception $e) {
			    echo $e;
			    exit;
			}
		}
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TrResepKamarRacikan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrResepKamarRacikan']))
		{
			$model->attributes=$_POST['TrResepKamarRacikan'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrResepKamarRacikan']))
		{
			$model->attributes=$_POST['TrResepKamarRacikan'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$transaction=Yii::app()->db->beginTransaction();
		try
		{
			$rawatInap = TrRawatInap::model()->findByAttributes(['kode_rawat'=>$model->trResepKamar->trResepPasien->nodaftar_id]);
			if(!empty($rawatInap))
			{
				$alkesObat = TrRawatInapAlkesObat::model()->findByAttributes([
					'resep_racik_id' => $id
				]);
				
				if(!empty($alkesObat))
					$alkesObat->delete();

				TrRawatInapAlkes::model()->updateTotalObat($rawatInap->id_rawat_inap);
			}
			$model->delete();
			$transaction->commit();
		}

		catch(Exception $e){
			
			$transaction->rollback();

			$result = array(
				'shortmsg' => 'danger',
				'message' => $errors,
			);

			echo CJSON::encode($result);
		}	

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new TrResepKamarRacikan('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['TrResepKamarRacikan']))
			$model->attributes=$_GET['TrResepKamarRacikan'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TrResepKamarRacikan('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['TrResepKamarRacikan']))
			$model->attributes=$_GET['TrResepKamarRacikan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TrResepKamarRacikan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TrResepKamarRacikan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TrResepKamarRacikan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tr-resep-kamar-racikan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
