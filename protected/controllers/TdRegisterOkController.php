<?php

class TdRegisterOkController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','tindakan','admin','delete','listPasien','print','infografis'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionInfografis()
	{
		$upf = TdUpf::model()->findAll(['order'=>'nama ASC']);
		$listGol = ['KHUSUS'=>'KHUSUS','BESAR'=>'BESAR','SEDANG'=>'SEDANG','KECIL'=>'KECIL'];

		$dataTable = [];
		$total_v['KHUSUS']['ELEKTIF'] = [];
		$total_v['SEDANG']['ELEKTIF'] = [];
		$total_v['BESAR']['ELEKTIF'] = [];
		$total_v['KECIL']['ELEKTIF'] = [];
		$total_v['KHUSUS']['CITO'] = [];
		$total_v['SEDANG']['CITO'] = [];
		$total_v['BESAR']['CITO'] = [];
		$total_v['KECIL']['CITO'] = [];
		foreach($upf as $item)
		{
			$subtotal_e = 0;
			$subtotal_c = 0;
			foreach($listGol as $q => $v)
			{
				$cdb = new CDbCriteria;
				$cdb->compare('jenisOperasi.grup',$v);
				$cdb->compare('upf',$item->id);
				$cdb->compare('cito_elektif',1);
				$cdb->addBetweenCondition('tgl_operasi',date('Y-01-01'),date('Y-12-31'));
				$cdb->with = ['jenisOperasi'];
				$cdb->together = true;
				$c = count(TdRegisterOk::model()->findAll($cdb));

				$cdb = new CDbCriteria;
				$cdb->compare('jenisOperasi.grup',$v);
				$cdb->compare('upf',$item->id);
				$cdb->compare('cito_elektif',2);
				$cdb->addBetweenCondition('tgl_operasi',date('Y-01-01'),date('Y-12-31'));
				$cdb->with = ['jenisOperasi'];
				$cdb->together = true;
				$e = count(TdRegisterOk::model()->findAll($cdb));
				$dataTable[$item->id][$v] = [
					 'CITO' => $c,
					 'ELEKTIF' => $e
				];

				$subtotal_c += $c;
				$subtotal_e += $e;
				$total_v[$v]['ELEKTIF'][] = $e;
				$total_v[$v]['CITO'][] = $c;
			}

			$subtotal = $subtotal_c + $subtotal_e;
			
		}

		$this->render('infografis',[
			'listUpf' => $upf,
			'listGol' => $listGol,
			'dataTable' => $dataTable
		]);
	}

	public function actionPrint($id)
	{
		$model = TdRegisterOk::model()->findByPk($id);
		$modelBiaya=$model->tdOkBiayas;
		
		$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A5', true, 'UTF-8');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
		$pdf->AddPage();
		
		$this->layout = '';
		ob_start();
		
		echo $this->renderPartial('print',array(
			'model'=>$model,
			'modelBiaya' => $modelBiaya
		));

		$data = ob_get_clean();
		ob_start();
		$pdf->writeHTML($data);

		$pdf->Output();
	}

	public function actionListPasien()
	{
		$this->title = 'OK | Rawat Inap | '.Yii::app()->name;


		$model=new BPendaftaran('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['jenis_pasien']))
			$model->KodeGol=$_GET['jenis_pasien'];

		if(isset($_GET['BPendaftaran']))
			$model->attributes=$_GET['BPendaftaran'];

		$this->render('listPasien',array(
			'model'=>$model,
		));
		// $this->render('obatPasien');
		
		
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$modelTop=new TdOkBiaya('search');
		$modelTop->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$modelTop->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$modelTop->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['TdOkBiaya']))
			$modelTop->attributes=$_GET['TdOkBiaya'];

		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'modelTop' => $modelTop
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($kodeDaftar)
	{
		$reg = BPendaftaran::model()->findByPk($kodeDaftar);
		$model=new TdRegisterOk;

		$criteria = new CDbCriteria;
        $criteria->condition = 'unit_tipe = 1 OR unit_tipe = 2';
        $criteria->order = 'NamaUnit ASC';
		$listUnit = Unit::model()->findAll($criteria);

		$temp = TdRegisterOk::model()->findByAttributes(
			['kode_daftar'=>$kodeDaftar,'no_rm'=>$reg->NoMedrec],
			[
				'condition'=>'status_ok <> :p1',
				'params' => [':p1'=>3]
			]
		);
		if(!empty($temp)){
			Yii::app()->user->setFlash('danger', "Pasien No RM: ".$reg->NoMedrec." sedang dioperasi.");
			$this->redirect(array('listPasien'));
		}


		$tindakan = new DmOkTindakan;

		$pasien = $reg->noMedrec;
		
		$model->no_rm = $reg->NoMedrec;
		$rawatInap = TrRawatInap::model()->findByAttributes(['kode_rawat'=>$reg->NODAFTAR]);
		// if(!empty($rawatInap)){
		// 	$model->unit = $rawatInap->kamar_id;
		// }
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$model->kode_daftar = $reg->NODAFTAR;

		if(isset($_POST['TdRegisterOk']))
		{
			$model->attributes=$_POST['TdRegisterOk'];
			$transaction=Yii::app()->db->beginTransaction();
			try{
				if($model->save())
				{
					

					if(!empty($rawatInap))
					{
						TrRawatInapTopJa::model()->deleteAllByAttributes(['id_rawat_inap'=>$rawatInap->id_rawat_inap]);

						$ja = new TrRawatInapTopJa;
						$ja->id_rawat_inap= $rawatInap->id_rawat_inap;
						$ja->id_dokter = $model->dr_anastesi;
						$ja->nilai = 0;
						$ja->save();
					}
					// $attr = array(
					// 	'id_rawat_inap' => $idri,
					// );
					
					// TrRawatInapTopJa::model()->deleteAllByAttributes($attr);

					// $ja = new TrRawatInapTopJa;
					// $ja->id_rawat_inap= $idri;
					// $ja->id_dokter = $model->dr_anastesi;
					// $ja->nilai = 0;
					// $ja->save();
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data telah tersimpan.");
					$this->redirect(array('view','id'=>$model->id_ok));
				}
			}

			catch(Exception $e)
			{
				print_r($e);exit;
				$transaction->rollback();
			}
			
		}

		$this->render('create',array(
			'model'=>$model,
			'pasien'=>$pasien,
			'listUnit' => $listUnit,
			'tindakan' => $tindakan->searchTindakan()
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$tindakan = new DmOkTindakan;

		$pasien = $model->pASIEN;
		$reg = $model->kodeDaftar;

		$criteria = new CDbCriteria;
        $criteria->condition = 'unit_tipe = 1 OR unit_tipe = 2';
        $criteria->order = 'NamaUnit ASC';
		$listUnit = Unit::model()->findAll($criteria);
		// $model->ruang = $reg->kamar_id;
		// $model->kelas = $reg->kamar->kelas_id;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		
		if(isset($_POST['TdRegisterOk']))
		{
			$model->attributes=$_POST['TdRegisterOk'];
			$transaction=Yii::app()->db->beginTransaction();
			try{
				if($model->save()){
					// $attr = array(
					// 	'id_rawat_inap' => $model->id_rawat_inap,

					// );

					// $ja = TrRawatInapTopJa::model()->findByAttributes($attr);
					// $ja->id_rawat_inap= $model->id_rawat_inap;
					// $ja->id_dokter = $model->dr_anastesi;
					// $ja->nilai = 0;
					// $ja->save();
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data telah tersimpan.");
					$this->redirect(array('view','id'=>$model->id_ok));
				}
			}

			catch(Exception $e)
			{
				$transaction->rollback();
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'pasien'=>$pasien,
			'tindakan' => $tindakan->searchTindakan(),
			'listUnit' => $listUnit
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['cari_no_rm']))
		{
		
		
		
			$this->redirect(array('create','idri'=>$_POST['cari_no_rm']));
		}

		$this->render('index',array(
			
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TdRegisterOk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['filter']))
            $model->SEARCH=$_GET['filter'];

        if(isset($_GET['size']))
            $model->PAGE_SIZE=$_GET['size'];

        if(isset($_GET['status_ok']))
            $model->status_ok=$_GET['status_ok'];
        
        if(isset($_GET['TdRegisterOk']))
            $model->attributes=$_GET['TdRegisterOk'];


		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TdRegisterOk the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TdRegisterOk::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TdRegisterOk $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='td-register-ok-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
