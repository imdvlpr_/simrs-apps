<?php

class BPendaftaranController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','terima'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	private function handleCekCheckout($no_rm)
	{
		$model = BPendaftaran::model()->findAllByAttributes(['NoMedrec'=>$no_rm,'TGLDAFTAR'=>date('Y-m-d')],['order'=>'TGLDAFTAR DESC']);
		$unitUser = !empty(Yii::app()->user->getState('unit')) ? Yii::app()->user->getState('unit') : 0;

		$count = 0;
		foreach($model as $m)
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 'b_pendaftaran_id = "'.$m->NODAFTAR.'" AND (status_rm_id = 3 OR status_rm_id = 4)' ;
			$bj = TrackingRm::model()->findAll($criteria);

			if(!empty($bj))
				$count++;
		}

		return $count > 0;

	    // if($count > 0){
	    // 	$this->addError($attribute, 'Dokumen Pasien ini sudah masuk di UNIT ini hari ini.');
	    // }
	}

	private function handleCekKunjungan($no_rm)
	{
		$model = BPendaftaran::model()->findAllByAttributes(['NoMedrec'=>$no_rm,'TGLDAFTAR'=>date('Y-m-d')],['order'=>'TGLDAFTAR DESC']);
		$unitUser = !empty(Yii::app()->user->getState('unit')) ? Yii::app()->user->getState('unit') : 0;

		$count = 0;
		foreach($model as $m)
		{
			$bj = BPendaftaranRjalan::model()->findAllByAttributes(['KodePoli'=>$unitUser,'NoDaftar'=>$m->NODAFTAR]);

			if(!empty($bj))
				$count++;
		}

		return $count == 0;

	    // if($count == 0){
	    // 	$this->addError($attribute, 'Data Pasien ini TIDAK terdaftar di UNIT ini hari ini');
	    // }
	}

	private function handleCekPoli($no_rm)
	{
		$model = BPendaftaran::model()->findAllByAttributes(['NoMedrec'=>$no_rm,'TGLDAFTAR'=>date('Y-m-d')],['order'=>'TGLDAFTAR DESC']);
		$unitUser = !empty(Yii::app()->user->getState('unit')) ? Yii::app()->user->getState('unit') : 0;

		$count = 0;
		foreach($model as $m)
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 'b_pendaftaran_id = "'.$m->NODAFTAR.'" AND unit_id = "'.$unitUser.'";' ;
			$bj = TrackingRm::model()->findAll($criteria);

			if(!empty($bj))
				$count++;
		}

		return $count > 0;

	    // if($count > 0){
	    // 	$this->addError($attribute, 'Dokumen Pasien ini sudah masuk di UNIT ini hari ini.');
	    // }
	}

	public function actionTerima()
	{
		$model = new TrackingRm;
		$errors = '';
		if(isset($_POST['no_rm']))
		{
			$transaction=Yii::app()->db->beginTransaction();
			try
			{

				if(Yii::app()->user->checkAccess([WebUser::R_OP_RM,WebUser::R_OP_DAFTAR]))
				{

					$is_passed = $this->handleCekCheckout($_POST['no_rm']);
					if(!$is_passed){
						$errors .= 'Dokumen Pasien ini masih belum dikeluarkan dari unit';	
						throw new Exception;
					}

					$is_passed = $this->handleCekPoli($_POST['no_rm']);

					if($is_passed){
						$errors .= 'Dokumen Pasien ini sudah masuk di UNIT ini hari ini.';	
						throw new Exception;
					}
				}

				else
				{
					$is_passed = $this->handleCekPoli($_POST['no_rm']);

					if($is_passed){
						$errors .= 'Dokumen Pasien ini sudah masuk di UNIT ini hari ini.';	
						throw new Exception;
					}

					$is_passed = $this->handleCekKunjungan($_POST['no_rm']);

					if($is_passed){
						$errors .= 'Data Pasien ini TIDAK terdaftar di UNIT ini hari ini';	
						throw new Exception;
					}
				}

				$unit_id = !empty(Yii::app()->user->getState('unit')) ? Yii::app()->user->getState('unit') : 0;
				$reg = BPendaftaran::model()->findByAttributes(['NoMedrec'=>$_POST['no_rm'],'KeluarRS'=>'N'],['order'=>'NODAFTAR DESC']);
				$prev = TrackingRm::model()->findByAttributes(['b_pendaftaran_id'=>$reg->NODAFTAR]);

				$regJalan = BPendaftaranRjalan::model()->findByAttributes([
					'NoDaftar' => $reg->NODAFTAR,
					'KodePoli' => $unit_id
				]);


				$model->prev_id = !empty($prev) ? $prev->id : null;
				
				$model->b_pendaftaran_id = $reg->NODAFTAR;
				$model->status_rm_id = 1;
				$model->no_medrec = $reg->NoMedrec;
				$model->unit_id = $unit_id;
				$start_date = Yii::app()->helper->dmYtoYmd($reg->TGLDAFTAR).' '.$reg->JamDaftar;
				
				$elapsed = Yii::app()->helper->hitungDurasiMenit($start_date,date('Y-m-d h:i:s'));

				$model->durasi = $elapsed;

				$criteria = new CDbCriteria;
				$criteria->join = "JOIN dm_tindakan dt ON dt.id = t.dm_tindakan_id";
				$criteria->condition = "t.unit_id = ".$unit_id." AND dt.kode = 'K001'";
				$dmTindakan = UnitTindakan::model()->find($criteria);

				$criteria = new CDbCriteria;
				$criteria->join = "JOIN a_unit_dokter ut ON ut.dokter_id = t.id_dokter";
				$criteria->condition = "ut.unit_id = ".$unit_id;
				$dmDokter = DmDokter::model()->find($criteria);
			
				if($model->save())
				{
					if(!Yii::app()->user->checkAccess([WebUser::R_OP_RM,WebUser::R_OP_DAFTAR]))
					{	
						$m = new TrRawatJalan;
						$m->reg_id = $reg->NODAFTAR;
						$m->reg_jalan_id = !empty($regJalan) ? $regJalan->Id : null;
						$m->no_medrec = $_POST['no_rm'];
						$m->unit_id = $unit_id;
						$m->tanggal = date('Y-m-d');
			
						if($m->save())
						{

							$params = [
								'nama' => $reg->noMedrec->NAMA,
								'status_bayar' => 0,
			                    'kode_trx' => 'PL'.$unit_id.date('Ymd').Yii::app()->helper->appendZeros($m->id,4),
			                    'trx_date' => date('Ymdhis'),
			                    'jenis_tagihan' => 'KONSULPOLI',
			                    'person_in_charge' => 'Perawat Poli '.$regJalan->unit->NamaUnit,
			                    'custid' => $_POST['no_rm'],
			                    'issued_by' => $regJalan->unit->NamaUnit,
			                    'keterangan' => 'Tagihan Konsul Poli : '.$regJalan->unit->NamaUnit,
			                    'nilai' => !empty($dmTindakan) ? $dmTindakan->nilai : 0,
			                    'jenis_customer' => $reg->kodeGol->NamaGol
			                ];


			                $tindakan = new TrRawatJalanTindakan;
			                $tindakan->kode_trx = $params['kode_trx'];
			                $tindakan->tr_rawat_jalan_id = $m->id;
			                $tindakan->dm_tindakan_id = $dmTindakan->id;
			                $tindakan->dokter_id = $dmDokter->id_dokter;
			                $tindakan->nilai = $dmTindakan->nilai;
			                $tindakan->status_bayar = 0;
		                	


			                if($tindakan->save())
			                {	

			                	Yii::app()->rest->insertTagihan($params);
			                	$transaction->commit();
								Yii::app()->user->setFlash('success', "Data diterima.");
								$this->redirect(['terima']);	
			                }

			                else{

			                	foreach($tindakan->getErrors() as $attribute){
									foreach($attribute as $error){
										$errors .= $error.' ';
									}
								}

								throw new Exception("Error Processing Request", 1);
			                }

							
						}

						else{

							foreach($m->getErrors() as $attribute){
								foreach($attribute as $error){
									$errors .= $error.' ';
								}
							}

							throw new Exception("Error Processing Request", 1);
						}	
					}
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data telah tersimpan.");
					$this->redirect(['terima']);
				}

				else
				{

                	foreach($model->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}

					throw new Exception("Error Processing Request", 1);
                }
			}

			catch(Exception $e){
				$transaction->rollback();
				$model->addError('no_rm',$errors);
				Yii::app()->user->setFlash('danger', $errors.' '.$e->getMessage());
				$this->redirect(['BPendaftaran/terima']);
			}

		}

		$this->render('terima',[
			'model'=>$model,
		]);	
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BPendaftaran;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BPendaftaran']))
		{
			$model->attributes=$_POST['BPendaftaran'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{

		if($id != 0)
		{
			$model=$this->loadModel($id);

			if(isset($_POST['BPendaftaran']))
			{

				$transaction=Yii::app()->db->beginTransaction();
				$errors = '';
				try
				{
					if(isset($_POST['diagnosis']))
					{

						BPendaftaranDiagnosis::model()->deleteAllByAttributes(['reg_id'=>$id,'tipe'=>1]);
						foreach($_POST['diagnosis'] as $data)
						{
							if(empty($data)) continue;
							$tmp = new BPendaftaranDiagnosis;
							$tmp->reg_id = $id;
							$tmp->tipe = 1;
							$tmp->keterangan = $data;
							if(!$tmp->save())
							{
								foreach($tmp->getErrors() as $attribute){
									foreach($attribute as $error){
										$errors .= $error.' ';
									}
								}
								throw new Exception();
							}
						}
					}

					if(isset($_POST['tindakan']))
					{
						BPendaftaranDiagnosis::model()->deleteAllByAttributes(['reg_id'=>$id,'tipe'=>2]);
						foreach($_POST['tindakan'] as $data)
						{
							if(empty($data)) continue;
							$tmp = new BPendaftaranDiagnosis;
							$tmp->reg_id = $id;
							$tmp->tipe = 2;
							$tmp->keterangan = $data;
							if(!$tmp->save())
							{
								foreach($tmp->getErrors() as $attribute){
									foreach($attribute as $error){
										$errors .= $error.' ';
									}
								}
								throw new Exception();
							}
						}	
					}

					if(isset($_POST['kode_icd10']))
					{
						BPendaftaranCoding::model()->deleteAllByAttributes(['reg_id'=>$id]);
						foreach($_POST['kode_icd10'] as $data)
						{
							if(empty($data)) continue;

							$tmp = new BPendaftaranCoding;
							$tmp->reg_id = $id;
							$tmp->kode_icd = $data;
							if(!$tmp->save())
							{
								foreach($tmp->getErrors() as $attribute){
									foreach($attribute as $error){
										$errors .= $error.' ';
									}
								}
								throw new Exception();
							}
						}	
					}


					if(isset($_POST['kode_prosedur']))
					{
						BPendaftaranTindakanCoding::model()->deleteAllByAttributes(['reg_id'=>$id]);
						foreach($_POST['kode_prosedur'] as $data)
						{
							if(empty($data)) continue;

							$tmp = new BPendaftaranTindakanCoding;
							$tmp->reg_id = $id;
							$tmp->kode_proc = $data;
							if(!$tmp->save())
							{
								foreach($tmp->getErrors() as $attribute){
									foreach($attribute as $error){
										$errors .= $error.' ';
									}
								}
								throw new Exception();
							}
						}	
					}

					$model->attributes=$_POST['BPendaftaran'];
					if($model->save()){
						$transaction->commit();
						Yii::app()->user->setFlash('success', "Data telah tersimpan.");
						$this->redirect(['update','id'=>$id]);
					}

					else
					{

						foreach($model->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}
						throw new Exception();
						
					}

				}

				catch(Exception $e){
					Yii::app()->user->setFlash('success', $errors.$e->getMessage());
					$this->redirect(['update','id'=>$id]);
				}	
			}

			$this->render('update',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new BPendaftaran('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['BPendaftaran']))
			$model->attributes=$_GET['BPendaftaran'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BPendaftaran('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['BPendaftaran']))
			$model->attributes=$_GET['BPendaftaran'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BPendaftaran the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BPendaftaran::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BPendaftaran $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bpendaftaran-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
