<?php

class WSBpjsController extends Controller
{

	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','test'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('getPesertaByNomor','getRujukanPPK','getRiwayatSEP','hapusSEP','getDataKunjungan','getDataRujukan','getRefVclaim','getRefVclaimList','getRefRuang','getRefDiagnosa','getRefProsedur'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionGetRefProsedur()
	{
		$q = $_GET['term'];
		$model = Yii::app()->rest->getRefProsedur($q);


     	$result = array();

     	if($model->metaData->code == 200)
     	{
			foreach($model->response->procedure as $m)
			{

				$result[] = array(
					'id' => $m->kode,
					'value' => $m->nama,

				);
			}
		}

		else
		{
			$result[] = array(
				'id' => $model->metaData->code,
				'value' => $model->metaData->message,

			);
		}

		echo CJSON::encode($result);
	}

	public function actionGetRefDiagnosa()
	{
		$q = $_GET['term'];
		$model = Yii::app()->rest->getRefDiagnosa($q);


     	$result = array();
     	
     	if($model->metaData->code == 200)
     	{
			foreach($model->response->diagnosa as $m)
			{

				$result[] = array(
					'id' => $m->kode,
					'value' => $m->nama,

				);
			}
		}

		else
		{
			$result[] = array(
				'id' => $model->metaData->code,
				'value' => $model->metaData->message,

			);
		}

		echo CJSON::encode($result);
	}

	public function actionGetRefRuang()
	{
		$data = Yii::app()->rest->getRefRuang();
		if($data->metadata->code == 1)
		{
			foreach($data->response->list as $d)
			{
				echo $d->kodekelas.' '.$d->namakelas."<br>";
			}
		}		
	}

	public function actionGetRefVclaimList()
	{

		$refs = array(
			'kelasrawat','spesialistik','caraKeluar','pascapulang','ruangrawat'
		);

		foreach($refs as $q => $ref)
		{
			$data = Yii::app()->rest->getRefVclaimList($ref);


			foreach($data->response->list as $m)
			{
				$model = BpjsMasterRef::model()->findByAttributes(array('jenis'=>$ref,'kode'=>$m->kode));
				if(empty($model))
				{	
					echo $m->kode.' - '.$m->nama.' inserted<br>';
					$model = new BpjsMasterRef;
					$model->kode = $m->kode;
					$model->nama = $m->nama;
					$model->jenis = $ref;
					$model->save();
				}
				
			}
		}
	}

	public function actionGetRefVclaim()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			if(!empty($_GET['param']))
			{

				$param = $_GET['param'];
				$ref = $_GET['ref'];				
				$data = Yii::app()->rest->getRefVclaim($ref,$param);
				$result = array();

				

				if($data->metaData->code == '200')
				{


					if($ref == 'poli')
						$list = $data->response->poli;
					else
						$list = $data->response->list;



					foreach($list as $m)
					{

						$result[] = array(
							'id' => $m->kode,
							'value' => $m->nama,

						);
					}

				}

				
				
				echo CJSON::encode($result);
			}

		}
	}

	public function actionGetDataRujukan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			
			if(!empty($_POST['param']))
			{
				
				$param = $_POST['param'];
				$jns = $_POST['jns'];
				$origin = $_POST['origin'];
				
				$data = Yii::app()->rest->getRujukanByOrigin($origin,$jns, $param);
				

				echo CJSON::encode($data);
			}

		}
	}


	public function actionGetDataKunjungan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			if(!empty($_POST['noSep']))
			{
				
				$noSep = $_POST['noSep'];
				
				$data = Yii::app()->rest->getDataKunjungan($noSep);
				

			
				echo CJSON::encode($data);
			}

		}
	}


	public function actionHapusSEP()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			if(!empty($_POST['nokartu']))
			{
				$noSep = $_POST['nokartu'];
				
				$data = Yii::app()->rest->hapusSEP($noSep);
				

			
				echo $data;
			}

		}
	}


	public function actionGetRiwayatSEP()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			if(!empty($_POST['nokartu']))
			{
				$nokartu = $_POST['nokartu'];
				
				$data = Yii::app()->rest->getRiwayatTerakhir($nokartu);
				

			
				echo CJSON::encode($data);
			}

		}
	}



	public function actionGetRujukanPPK()
	{

		$q = $_GET['term'];

		// $model = BpjsPpk::model()->searchPpkJatim($q);

		$model = Yii::app()->rest->getFaskes($q,0,20);

     	$result = array();
		foreach($model->response->list as $m)
		{

			$result[] = array(
				'key' => $m->kdProvider,
				'id' => $m->kdProvider,
				'value' => $m->nmProvider,

			);
		}

		// $result = array();
		// foreach($model as $m)
		// {

		// 	$result[] = array(
		// 		'key' => $m->ID_PPK,
		// 		'id' => $m->KODE_PPK,
		// 		'value' => $m->NAMA_PPK,

		// 	);
		// }

		echo CJSON::encode($result);
	}

	public function actionGetBatasSEP()
	{

		$tanggal = $_POST['tgl'];
		
		$result = TrPendaftaranRjalan::model()->cekBatasSEPFromGoogle($tanggal);
		echo CJSON::encode($result);
	}

	public function actionTest($kartu = '0001261832477'){
		header('Content-Type: application/json');
		$param = array(
			'jenis' => 'kartu',
			'nomor' => $kartu, 
		);

		$data = Yii::app()->rest->getPesertaByNomor($param);
		

	
		echo CJSON::encode($data);
	}


	public function actionGetPesertaByNomor()
	{
		if(!empty($_POST['nokartu']))
		{
			$nokartu = $_POST['nokartu'];
			$jenis = $_POST['jenis'];

			$param = array(
				'jenis' => $jenis,
				'nomor' => $nokartu, 
			);

			$data = Yii::app()->rest->getPesertaByNomor($param);
			

		
			echo CJSON::encode($data);
		}

		exit;
	}


}