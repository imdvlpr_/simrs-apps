<?php

class TrResepKamarController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','ajaxSimpanResepItem','ajaxSimpanResepRacikan','input','viewXls','print','resepBaru','getKodeRacikan'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionResepBaru($trid='')
	{
		if(empty($trid)) return;

		$model=new TrResepKamar;
		$daftar = BPendaftaran::model()->findByPk($kode_daftar);
		$model->kode_daftar = $kode_daftar;
		$model->pasien_id = $daftar->NoMedrec;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrResepKamar']))
		{
			$model->attributes=$_POST['TrResepKamar'];
			$model->petugas_id = Yii::app()->user->pegawai_id;
			$model->no_resep = 'RI'.date('YmdHis').$model->pasien_id;
			if($model->save()){

				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('resepBaru',array(
			'model'=>$model,
		));
	}

	public function actionPrint($id)
	{
		$model = $this->loadModel($id);
		// $resepItem = new TrResepKamarItem;
		// $resepRacikan = new TrResepKamarRacikan;
		
		try
		{

			ob_start();
			$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                        'P', 'mm', 'A4', true, 'UTF-8');

			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$page_format = array(
			    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 160.0, 'ury' => 76.0),


			);

			$pdf->AddPage('P', $page_format, false, false);
			$pdf->SetAutoPageBreak(TRUE, 0);

			
			$top = 5;
			$left = 10;
			$pdf->SetFont('helveticaB', '', 8);
			$pdf->Text($left,$top,'No.Resep');
			$pdf->Text($left+16,$top,':');
			$pdf->Text($left+18.5,$top,strtoupper($model->no_resep));
			$pdf->SetFont('helvetica', '', 8);
			$top += 5;
			$pdf->Text($left,$top,'Tgl Resep');
			$pdf->Text($left+16,$top,':');
			$pdf->Text($left+18.5,$top,($model->tgl_resep));

			$top += 5;
			$pdf->Text($left,$top,'Tgl Cetak');
			$pdf->Text($left+16,$top,':');
			$pdf->Text($left+18.5,$top,date('d/m/Y'));
			
			$top += 5;
			$pdf->Text($left,$top,'No.RM');
			$pdf->Text($left+16,$top,':');
			$pdf->Text($left+18.5,$top,($model->pasien->NoMedrec));

			$top += 5;
			$pdf->Text($left,$top,'Nama');
			$pdf->Text($left+16,$top,':');
			$pdf->Text($left+18.5,$top,($model->pasien->NAMA));

			$subtotal = $model->sumObatItem;
			$subtotal_racikan = $model->sumObatRacikan;
			$total = $subtotal + $subtotal_racikan;
			
			$top += 5;
			$pdf->SetFont('helveticaB', '', 8);
			$pdf->Text($left,$top,'Total');
			$pdf->Text($left+16,$top,':');
			$pdf->SetFont('helvetica', '', 9);
			
			$pdf->Text($left+18.5,$top,Yii::app()->helper->formatRupiah($total));
			$pdf->SetFont('helveticaB', '', 8);
			$top += 5;
			$pdf->Text(25,$top,'Obat Non-racikan');
			
			$top += 5;
			$posQty = 43;
			$posHarga = 50;
			$pdf->Text($left,$top,'Nama Obat');
			$pdf->Text($left+$posQty,$top,'Qty');
			$pdf->Text($left+$posHarga,$top,'Harga');
			$i=0;
			foreach($model->trResepKamarItems as $item)
			{

				$i++;
				$top += 5;
				$pdf->SetFont('helvetica', '', 8);
			
				$pdf->Text($left,$top,$item->obat->nama_barang);
				
				$pdf->SetFont('helvetica', '', 9);
				$pdf->Text($left+$posQty,$top,$item->jumlah);

				$pdf->Text($left+$posHarga,$top,Yii::app()->helper->formatRupiah($item->obat->hj * $item->jumlah));
			}

			if(!empty($model->trResepKamarRacikans))
			{
				$top += 5;
				$pdf->Text(25,$top,'Obat racikan');
				
				$top += 5;
				$pdf->Text($left,$top,'Nama Obat');
				$pdf->Text($left+$posQty,$top,'Qty');
				$pdf->Text($left+$posHarga,$top,'Harga');
				$i=0;
				foreach($model->trResepKamarRacikans as $item)
				{
					$i++;
					$top += 5;
					$pdf->SetFont('helvetica', '', 8);
			
					$pdf->Text($left,$top,$item->trObatRacikan->nama);
					$pdf->SetFont('helvetica', '', 9);
			
					$pdf->Text($left+$posQty,$top,$item->jumlah);
					$pdf->Text($left+$posHarga,$top,Yii::app()->helper->formatRupiah($item->harga));
				}
			}

			$top += 5;

			$pdf->SetFont('helveticaB', '', 8);
			$pdf->Text($left+20,$top+25,'Petugas Apotik');
			$pdf->Text($left+20,$top+35,$model->petugas->NAMA);
			
			$pdf->SetLineStyle( array( 'width' => 0.2, 'color' => array(0,0,0)));

			$lineMargin = 10;
			$rmargin = 5;
			$pdf->Line($lineMargin,$rmargin,$pdf->getPageWidth()-$rmargin,$rmargin); 
			$pdf->Line($pdf->getPageWidth()-$rmargin,$rmargin,$pdf->getPageWidth()-$rmargin,$pdf->getPageHeight()-$lineMargin);
			$pdf->Line($lineMargin,$pdf->getPageHeight()-$lineMargin,$pdf->getPageWidth()-$rmargin,$pdf->getPageHeight()-$lineMargin);
			$pdf->Line($lineMargin,$rmargin,$lineMargin,$pdf->getPageHeight()-$lineMargin);

			$pdf->Output("kartu_".$model->no_resep.".pdf", "I");

		}
		catch(HTML2PDF_exception $e) {
		    echo $e;
		    exit;
		}
	}

	public function actionViewXls($kode_daftar)
	{

		$resep = new TrResepKamar;
		$dataDaftar = BPendaftaran::model()->findByPk($kode_daftar);
		$listResep = $resep->searchRincian($kode_daftar);
		$this->renderPartial('view_xls',array(
			// 'model'=>$this->loadModel($id),
			'resep' => $resep,
			'dataDaftar' => $dataDaftar,
			'listResep' => $listResep
		));
	}

	public function actionGetKodeRacikan(){
		echo json_encode(Yii::app()->helper->generateUniqueCode());
	}


	public function actionAjaxSimpanResepRacikan()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$errors = '';
			$transaction=Yii::app()->db->beginTransaction();
			try
			{
				$result = [];
				$id = $_POST['rid'];
				$dataObat = $_POST['dataObat'];

				$rit = new TrResepKamarRacikan;

				if($dataObat['komposisi'] == 2) // racikan non-paket
				{

					$obat = MObatAkhp::model()->findByPk($dataObat['obat_id']);
					$tor = TrObatRacikan::model()->findByAttributes([
						'kode'=>$obat->kd_barang,
						'obat_id' => $dataObat['obat_id']
					]);

					if(empty($tor))
						$tor = new TrObatRacikan;

					
					$tor->kode = $obat->kd_barang;
					$tor->nama = $obat->nama_barang;
					$tor->kekuatan = $obat->kekuatan;
					$tor->obat_id = $dataObat['obat_id'];
					$tor->save();

					$tori = TrObatRacikanItem::model()->findByAttributes([
						'tr_obat_racikan_id' => $tor->id
					]);

					if(empty($tori))
						$tori = new TrObatRacikanItem;


					$tori->tr_obat_racikan_id = $tor->id;
					$tori->obat_id = $dataObat['obat_id'];
					$tori->qty = $dataObat['jml_stok'];
					$tori->harga = $obat->hj * $dataObat['jml_stok']; 
					
					$tori->save();
					
					$rit->kode_racikan = $dataObat['kode_racikan'];
					$rit->tr_obat_racikan_id = $tor->id;
				}

				else
				{
					$rit->tr_obat_racikan_id = $dataObat['obat_id'];
				}

				$rit->tr_resep_kamar_id = $id;
				
				$rit->jumlah = $dataObat['jml_stok'];
				$rit->harga = str_replace(".", "", $dataObat['harga']) * $dataObat['jml_stok']; 
				$rit->signa1 = $dataObat['signa1'];
				$rit->signa2 = $dataObat['signa2'];
				$rit->hari = $dataObat['hari'];
				$rit->jml_ke_apotik = $dataObat['jml_ke_apotik'];
				$rit->jml_ke_bpjs = $dataObat['jml_ke_bpjs'];
				$rit->dosis_permintaan = $dataObat['dosis_permintaan'];
				$rit->aturan = $dataObat['aturan'];

				

				if($rit->validate())
				{
					$rit->save();

					$resep = TrResepKamar::model()->findByPk($id);
					$rawatInap = TrRawatInap::model()->findByAttributes(['kode_rawat'=>$resep->trResepPasien->nodaftar_id]);

					if(!empty($rawatInap)){
					
						$obat = TrObatRacikan::model()->findByPk($rit->tr_obat_racikan_id);
						
						$model = new TrRawatInapAlkesObat;
						$model->id_rawat_inap= $rawatInap->id_rawat_inap;
						$model->keterangan = $obat->nama;
						$model->nilai = str_replace(".", "", $rit->harga);
						$model->kode_alkes = 'OBAT';
						$model->id_dokter = $resep->dokter_id;
						$model->tanggal_input = date('Y-m-d');
						$model->resep_racik_id = $rit->id;
						$model->save();
						
						TrRawatInapAlkes::model()->updateTotalObat($rawatInap->id_rawat_inap);
					}

					$result = array(
						'shortmsg' => 'success',
						'message' => 'Data telah disimpan',
					);
				}

				else{
					foreach($rit->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}
			
				}		

			
				$transaction->commit();

				echo CJSON::encode($result);
			}

			catch(Exception $e){
				
				$transaction->rollback();

				$result = array(
					'shortmsg' => 'danger',
					'message' => $errors,
				);

				echo CJSON::encode($result);
			}	
		}
	}

	public function actionAjaxSimpanResepItem()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$errors = '';
			$transaction=Yii::app()->db->beginTransaction();
			try
			{
				$result = [];
				$id = $_POST['rid'];
				$dataObat = $_POST['dataObat'];
				

				$rit = new TrResepKamarItem;

				$rit->tr_resep_kamar_id = $id;
				$rit->obat_id = $dataObat['obat_id'];
				$rit->jumlah = $dataObat['jumlah'];
				$rit->harga = str_replace(".", "", $dataObat['harga']) * $dataObat['jumlah']; 
				$rit->jml_stok = $dataObat['jumlah'];
				$rit->signa1 = $dataObat['signa1'];
				$rit->signa2 = $dataObat['signa2'];
				$rit->hari = $dataObat['hari'];
				$rit->jml_ke_apotik = $dataObat['jml_ke_apotik'];
				$rit->jml_ke_bpjs = $dataObat['jml_ke_bpjs'];
				$rit->dosis_permintaan = $dataObat['dosis_permintaan'];
				$rit->aturan = $dataObat['aturan'];

				$resep = TrResepKamar::model()->findByPk($id);
				$rawatInap = TrRawatInap::model()->findByAttributes(['kode_rawat'=>$resep->trResepPasien->nodaftar_id]);
				

				if($rit->validate())
				{
					$rit->save();
					
					if(!empty($rawatInap)){
					
						$obat = MObatAkhp::model()->findByPk($rit->obat_id);
						
						$model = new TrRawatInapAlkesObat;
						$model->id_rawat_inap= $rawatInap->id_rawat_inap;
						$model->keterangan = $rit->obat->nama_barang;
						$model->nilai = str_replace(".", "", $rit->harga);
						$model->kode_alkes = 'OBAT';
						$model->id_dokter = $resep->dokter_id;
						$model->tanggal_input = date('Y-m-d');
						$model->resep_non_racik_id = $rit->id;
						$model->save();
					
						TrRawatInapAlkes::model()->updateTotalObat($rawatInap->id_rawat_inap);
					}

					$result = array(
						'shortmsg' => 'success',
						'message' => 'Data telah disimpan',
					);
				}
				else{
					foreach($rit->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}
			
				}		
			

				$transaction->commit();

				echo CJSON::encode($result);
			}

			catch(Exception $e){
				
				$transaction->rollback();

				$result = array(
					'shortmsg' => 'danger',
					'message' => $errors.$e->getMessage(),
				);

				echo CJSON::encode($result);
			}	
		}
	}

	public function actionInput($id)
	{

		$resepItem = new TrResepKamarItem;
		$resepRacikan = new TrResepKamarRacikan;
		$this->render('input',array(
			'model'=>$this->loadModel($id),
			'resepItem' => $resepItem,
			'resepRacikan' => $resepRacikan
		));
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($kode_daftar)
	{

		$resep = new TrResepKamar;
		$dataDaftar = BPendaftaran::model()->findByPk($kode_daftar);
		$listResep = $resep->searchRincian($kode_daftar);
		$this->render('view',array(
			// 'model'=>$this->loadModel($id),
			'resep' => $resep,
			'dataDaftar' => $dataDaftar,
			'listResep' => $listResep
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($trid)
	{
		$model=new TrResepKamar;
		$model->tr_resep_pasien_id = $trid;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrResepKamar']))
		{
			$model->attributes=$_POST['TrResepKamar'];
			$model->petugas_id = Yii::app()->user->pegawai_id;
			$model->no_resep = 'RI'.date('YmdHis').$trid;
			if($model->save()){

				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('trResepPasien/view','id'=>$trid));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TrResepKamar']))
		{
			$model->attributes=$_POST['TrResepKamar'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new TrResepKamar('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['TrResepKamar']))
			$model->attributes=$_GET['TrResepKamar'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($trid)
	{
		$model=new TrResepKamar('search');
		$model->tr_resep_pasien_id = $trid;
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['TrResepKamar']))
			$model->attributes=$_GET['TrResepKamar'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TrResepKamar the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TrResepKamar::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TrResepKamar $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tr-resep-kamar-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
