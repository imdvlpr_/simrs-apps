<?php

class BPendaftaranRjalanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','terima','kunjungan'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionKunjungan()
	{
		$model=new BPendaftaranRjalan('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['search_by']))
			$model->SEARCH_BY=$_GET['search_by'];

		if(isset($_GET['kode_poli']))
			$model->KodePoli=$_GET['kode_poli'];
		
		if(isset($_GET['BPendaftaranRjalan']))
			$model->attributes=$_GET['BPendaftaranRjalan'];

		$this->render('kunjungan',array(
			'model'=>$model,
		));
	}

	public function actionTerima($id){
		$model=new TrackingRm;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$breg = $this->loadModel($id);
		$bp = $breg->noDaftar;
		// if(isset($_POST['cari_no_rm']))
		// {
		$model->no_medrec = $bp->NoMedrec;
		
		$errors = "";
		$model->b_pendaftaran_id = $bp->NODAFTAR;
		$model->status_rm_id = 1;
		$model->unit_penerima = !empty(Yii::app()->user->getState('unit')) ? Yii::app()->user->getState('unit') : 0;

		$criteria = new CDbCriteria;

		$criteria->join = "JOIN dm_tindakan dt ON dt.id = t.dm_tindakan_id";
		$criteria->condition = "t.unit_id = ".$model->unit_penerima." AND dt.kode = 'K001'";
		$dmTindakan = UnitTindakan::model()->find($criteria);

		if(empty($dmTindakan)){
			// $model->addError('KodePoli',$errors);
			Yii::app()->user->setFlash('danger', 'Unit ini belum setting Konsul Poli');
			$this->redirect(['BPendaftaranRjalan/kunjungan']);
		}

		$criteria = new CDbCriteria;
		$criteria->join = "JOIN a_unit_dokter ut ON ut.dokter_id = t.id_dokter";
		$criteria->condition = "ut.unit_id = ".$model->unit_penerima;
		$dmDokter = DmDokter::model()->find($criteria);

		if(empty($dmDokter)){
			// $model->addError('KodePoli',$errors);
			Yii::app()->user->setFlash('danger', 'Unit ini belum setting Dokter');
			$this->redirect(['BPendaftaranRjalan/kunjungan']);
		}



		// $model->attributes=$_POST['TrackingRm'];
		$transaction=Yii::app()->db->beginTransaction();
		try
		{
			if($model->save())
			{

				$m = new TrRawatJalan;
				$m->reg_id = $model->b_pendaftaran_id;
				$m->reg_jalan_id = $breg->Id;
				$m->no_medrec = $model->no_medrec;
				$m->unit_id = $model->unit_penerima;
				$m->tanggal = date('Y-m-d');
	
				if($m->save())
				{
					$params = [
						'nama' => $bp->noMedrec->NAMA,
						'status_bayar' => 0,
	                    'kode_trx' => 'PL'.$breg->KodePoli.date('Ymd').Yii::app()->helper->appendZeros($m->id,4),
	                    'trx_date' => date('Ymdhis'),
	                    'jenis_tagihan' => 'KONSULPOLI',
	                    'person_in_charge' => 'Perawat Poli '.$breg->unit->NamaUnit,
	                    'custid' => $bp->NoMedrec,
	                    'issued_by' => $breg->unit->NamaUnit,
	                    'keterangan' => 'Tagihan Konsul Poli : '.$breg->unit->NamaUnit,
	                    'nilai' => $dmTindakan->nilai,
	                    'jenis_customer' => $bp->kodeGol->NamaGol
	                ];

	                $tindakan = new TrRawatJalanTindakan;
	                $tindakan->kode_trx = $params['kode_trx'];
	                $tindakan->tr_rawat_jalan_id = $m->id;
	                $tindakan->dm_tindakan_id = $dmTindakan->id;
	                $tindakan->dokter_id = $dmDokter->id_dokter;
	                $tindakan->nilai = $dmTindakan->nilai;
	                $tindakan->status_bayar = 0;
                	

	                if($tindakan->save())
	                {	

	                	Yii::app()->rest->insertTagihan($params);
	                	$transaction->commit();
						Yii::app()->user->setFlash('success', "Data diterima.");
						$this->redirect(['trRawatJalan/index']);	
	                }

	                else{

	                	foreach($tindakan->getErrors() as $attribute){
							foreach($attribute as $error){
								$errors .= $error.' ';
							}
						}

						throw new Exception("Error Processing Request", 1);
	                }

					
				}

				else{

					foreach($m->getErrors() as $attribute){
						foreach($attribute as $error){
							$errors .= $error.' ';
						}
					}

					throw new Exception("Error Processing Request", 1);
				}	
				
			}

			else{
				foreach($model->getErrors() as $attribute){
					foreach($attribute as $error){
						$errors .= $error.' ';
					}
				}

				throw new Exception("Error Processing Request", 1);
			}
		}

		catch(Exception $e){
			$transaction->rollback();
			$model->addError('KodePoli',$errors);
			Yii::app()->user->setFlash('danger', $errors.' '.$e->getMessage());
			$this->redirect(['BPendaftaranRjalan/kunjungan']);
		}
			

		// }

		// $this->render('terima_dokumen',array(
		// 	'model'=>$model,
		// ));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BPendaftaranRjalan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BPendaftaranRjalan']))
		{
			$model->attributes=$_POST['BPendaftaranRjalan'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BPendaftaranRjalan']))
		{
			$model->attributes=$_POST['BPendaftaranRjalan'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data telah tersimpan.");
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$parent = $model->noDaftar;
		$model->delete();
		$parent->delete();


		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new BPendaftaranRjalan('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['search_by']))
			$model->SEARCH_BY=$_GET['search_by'];

		if(isset($_GET['kode_poli']))
			$model->KodePoli=$_GET['kode_poli'];
		
		if(isset($_GET['BPendaftaranRjalan']))
			$model->attributes=$_GET['BPendaftaranRjalan'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BPendaftaranRjalan('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];
		
		if(isset($_GET['BPendaftaranRjalan']))
			$model->attributes=$_GET['BPendaftaranRjalan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BPendaftaranRjalan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BPendaftaranRjalan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BPendaftaranRjalan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bpendaftaran-rjalan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
