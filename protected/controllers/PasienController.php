<?php

class PasienController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	public $layout='//layouts/column2';




	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(
					'create','update','admin','cetakKartu','kartu',
					'approveKartu','batalKartu', 'cetakETiket', 'cetakGelang'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionKartu()
	{
		$model=new Pasien('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['Pasien']))
			$model->attributes=$_GET['Pasien'];


		$this->render('kartu',array(
			'model'=>$model,
		));
	}

	public function actionBatalKartu($id)
	{
		if(!empty($id))
		{
			if( Yii::app()->request->isAjaxRequest )
			{
				$model = Pasien::model()->findByPk($id);

			    // Stop jQuery from re-initialization
			    Yii::app()->clientScript->scriptMap['jquery.js'] = false;

			    if( isset( $_POST['action'] ) && $_POST['action'] == 'confirmApprove' )
			    {

			    	$model->CETAK_KARTU = 0;
					$model->update(array('CETAK_KARTU'));

					echo CJSON::encode( array(
							'status' => 'success',
							'content' => 'Sukses',
					   ));

			       exit;


			    }
			    else if( isset( $_POST['action'] ) )
			    {
			       echo CJSON::encode( array(
			         'status' => 'canceled',
			         'content' => 'Batal Pembayaran Kartu',
			       ));
			      exit;
			    }
			    else
			    {

			      echo CJSON::encode( array(
			        'status' => 'failure',
			        'content' => $this->renderPartial( 'batal_kartu', array(
			        	'model' => $model
			           ), true, true ),
			      ));
			      exit;
			    }
			}
			else
			{
			    if( isset( $_POST['confirmApprove'] ) )
			    {
			      // $model->delete();
			       $this->redirect( array( 'kartu' ) );
			    }
			    else if( isset( $_POST['denyDelete'] ) )
			       $this->redirect( array( 'kartu' ) );
			    else
			       $this->render( 'batal_kartu',array(

			       	));
			}
		}
	}

	public function actionApproveKartu($id)
	{
		if(!empty($id))
		{
			if( Yii::app()->request->isAjaxRequest )
			{
				$model = Pasien::model()->findByPk($id);

			    // Stop jQuery from re-initialization
			    Yii::app()->clientScript->scriptMap['jquery.js'] = false;

			    if( isset( $_POST['action'] ) && $_POST['action'] == 'confirmApprove' )
			    {

			    	$model->CETAK_KARTU = 1;
					$model->update(array('CETAK_KARTU'));

					echo CJSON::encode( array(
							'status' => 'success',
							'content' => 'Sukses',
					   ));

			       exit;


			    }
			    else if( isset( $_POST['action'] ) )
			    {
			       echo CJSON::encode( array(
			         'status' => 'canceled',
			         'content' => 'Batal Pembayaran Kartu',
			       ));
			      exit;
			    }
			    else
			    {

			      echo CJSON::encode( array(
			        'status' => 'failure',
			        'content' => $this->renderPartial( 'approve_kartu', array(
			        	'model' => $model
			           ), true, true ),
			      ));
			      exit;
			    }
			}
			else
			{
			    if( isset( $_POST['confirmApprove'] ) )
			    {
			      // $model->delete();
			       $this->redirect( array( 'kartu' ) );
			    }
			    else if( isset( $_POST['denyDelete'] ) )
			       $this->redirect( array( 'kartu' ) );
			    else
			       $this->render( 'approve_kartu',array(

			       	));
			}
		}
	}

	public function actionCetakKartu($id)
	{
		$model = $this->loadModel($id);
		if(!empty($model))
		{
			try
			{

				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                            'L', 'mm', 'A7', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$page_format = array(
				    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 53.98, 'ury' => 85.60),


				);

				$pdf->AddPage('L', $page_format, false, false);
				$pdf->SetAutoPageBreak(TRUE, 0);

				$pdf->write1DBarcode($model->NoMedrec,'C128',52.5,43.5,100,7);

				$pdf->SetFont('helvetica', '', 8.5);
				$pdf->Text(5,26.5,'No.RM');
				$pdf->Text(16,26.5,':');
				$pdf->Text(5,32.3,'Nama');
				$pdf->Text(16,32.3,':');
				$pdf->Text(18.5,32.3,strtoupper($model->NAMA));
				$pdf->Text(5,38.3,'Alamat');
				$pdf->Text(16,38.3,':');
				$pdf->Text(18.5,38.3,$model->FULLADDRESS);

				$pdf->SetFont('helvetica', 'B', 14);
				$pdf->Text(18.5,24.7,$model->NoMedrec);


				$pdf->Output("kartu_".$model->NoMedrec.".pdf", "I");

			}
			catch(HTML2PDF_exception $e) {
			    echo $e;
			    exit;
			}

		}
	}

	public function actionCetakETiket($id)
	{
		$model = $this->loadModel($id);
		if(!empty($model))
		{
			try
			{
				 ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                            'L', 'mm', 'Legal', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				// $page_format = array(
				//     'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 65.00, 'ury' => 40.00),
				//     'CropBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 65.00, 'ury' => 40.00),
				// );

				$page_format = array(
				    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 68, 'ury' => 43),
				    // 'CropBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 65, 'ury' => 40),
				    // 'BleedBox' => array ('llx' => 5, 'lly' => 5, 'urx' => 65, 'ury' => 40),
				    // 'TrimBox' => array ('llx' => 10, 'lly' => 10, 'urx' => 65, 'ury' => 40),
				    // 'ArtBox' => array ('llx' => 15, 'lly' => 15, 'urx' => 65, 'ury' => 40),
				    // 'Dur' => 3,
				    // 'trans' => array(
				    //     'D' => 1.5,
				    //     'S' => 'Split',
				    //     'Dm' => 'V',
				    //     'M' => 'O'
				    // ),
				    // 'Rotate' =>270,
				    'PZ' => 1,
				);

				$pdf->AddPage('L', $page_format, false, false);
				$pdf->SetAutoPageBreak(TRUE, 0);

				// format date to dd/mm/yyyy
				$date = date_create($model->TGLLAHIR);
				$date = date_format($date, 'd-m-Y');

				$pdf->write1DBarcode($model->NoMedrec,'C128',35,18,100,7);
				$pdf->SetFont('helvetica', 'B', 13);
				$pdf->Text(6,5, strtoupper($model->NAMA));
				$pdf->SetFont('helvetica', '', 10);
				$pdf->Text(6,18, $date);
				$pdf->Text(6,24,$model->NoMedrec.' / '.$model->JENSKEL);
				$pdf->Text(6,30, $model->FULLADDRESS);


				$pdf->Output("etiket_".$model->NoMedrec.".pdf", "I");
			}
			catch(HTML2PDF_exception $e) {
			    echo $e;
			    exit;
			}
		}
	}

	public function actionCetakGelang($id)
	{
		// print_r(Yii::app()->getBasePath().'/../');exit;
		$model = $this->loadModel($id);
		if(!empty($model))
		{
			try
			{
				ob_start();
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                            'L', 'mm', 'A7', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				// $page_format = array(
				//     'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 280.00, 'ury' => 25.00),
				//     'Rotate' => '180'
				// );
				$page_format = array(
				    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 30, 'ury' => 280),
				    // 'CropBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 30, 'ury' => 280),
				    // 'BleedBox' => array ('llx' => 5, 'lly' => 5, 'urx' => 30, 'ury' => 280),
				    // 'TrimBox' => array ('llx' => 10, 'lly' => 10, 'urx' => 30, 'ury' => 280),
				    // 'ArtBox' => array ('llx' => 15, 'lly' => 15, 'urx' => 30, 'ury' => 280),
				    // 'Dur' => 3,
				    // 'trans' => array(
				    //     'D' => 1.5,
				    //     'S' => 'Split',
				    //     'Dm' => 'V',
				    //     'M' => 'O'
				    // ),
				    'Rotate' =>-90,
				    // 'PZ' => 1,
				);

				$pdf->AddPage('L', $page_format, false, false);
				$pdf->SetAutoPageBreak(TRUE, 0);

				// format date to dd/mm/yyyy
				$date = date_create($model->TGLLAHIR);
				$date = date_format($date, 'd-m-Y');
				$pdf->StartTransform();
				
				// $pdf->Image(Yii::app()->getBasePath().'/../images/logo-rsud.jpg', 10, 5, 50, 50, 'JPG');
				$pdf->SetFont('helvetica', 'B', 10);
				$pdf->Text(65,7,'RSUD KABUPATEN KEDIRI');
				$pdf->SetFont('helvetica', 'B', 7);
				$pdf->Text(65,12, strtoupper($model->NAMA));
				$pdf->Text(65,16, $model->NoMedrec.' , '.$date);
				$pdf->Text(65,20, $model->FULLADDRESS);
				$pdf->StopTransform();
				$pdf->Output("gelang_".$model->NoMedrec.".pdf", "I");
			}
			catch(HTML2PDF_exception $e) {
			    echo $e;
			    exit;
			}
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pasien;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pasien']))
		{
			$model->attributes=$_POST['Pasien'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->NoMedrec));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		$pasien = Pasien::model()->findByPk($id);

	
		
		$alamat = PasienAlamat::model()->findByAttributes(['pasien_id'=>$id]);
		if(empty($alamat))
			$alamat = new PasienAlamat;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pasien']))
		{
			$pasien->attributes=$_POST['Pasien'];
			$pasien->AGAMA = $_POST['Pasien']['AGAMA'];
			
			$pasien->TGLLAHIR = date("Y-m-d", strtotime($pasien->TGLLAHIR));
			$alamat->attributes=$_POST['PasienAlamat'];
			$pasien->Desa = $alamat->desa;
			$alamat->pasien_id = $pasien->NoMedrec;
			
			if($alamat->validate() && $pasien->validate())
			{
				$pasien->ALAMAT = $alamat->jalan.' '.$alamat->rt.'/'.$alamat->rw.' '.$alamat->kecamatan->NamaKec;
					
				$pasien->attributes=$_POST['Pasien'];
				$pasien->AGAMA = $_POST['Pasien']['AGAMA'];
				$pasien->STATUSPERKAWINAN = $_POST['Pasien']['STATUSPERKAWINAN'];
				
				$pasien->save();
				$alamat->save();
				Yii::app()->user->setFlash('success', "Data Tersimpan");

				$this->redirect(array('update','id'=>$pasien->NoMedrec));
			}
		}

		$this->render('update',array(
			'pasien'=>$pasien,
			'alamat' => $alamat
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pasien('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['filter']))
			$model->SEARCH=$_GET['filter'];

		if(isset($_GET['size']))
			$model->PAGE_SIZE=$_GET['size'];

		if(isset($_GET['Pasien']))
			$model->attributes=$_GET['Pasien'];


		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pasien the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pasien::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pasien $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pasien-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
