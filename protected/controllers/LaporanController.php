<?php
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class LaporanController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('dokter','ajaxDokter','dokterPasien','kamar','kamarPasien','radiologi','radiologiPasien','patologiKlinik','patologiKlinikPasien','patologiAnatomi','patologiAnatomiPasien','gizi','hd','hdPasien','operasi','anestesi','prwtOp','prwtAn','rr','jasaDokter','obat','alkes','rekapDokter',
					'ird','irdPasien','rawatInap','rawatInapPasien','dokterPerKamar','resep','cssd','kunjungan','download','okRekap','labTindakan','labTindakanExport'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionLabTindakanExport($sd, $ed)
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->setActiveSheetIndex(0);
		
		$parents = new DmLabItemParent;
		$model = new LabRequestItem;
		$model->TANGGAL_AWAL = $sd;
		$model->TANGGAL_AKHIR = $ed;
		
		

		$total = 0; 
		$sheet->setCellValue('A1', 'Laporan Rekap Tindakan Lab Tanggal : '.$sd. ' hingga '.$ed);
		$sheet->mergeCells('A1:D1');
		$sheet->setCellValue('A2','No');
		$sheet->getColumnDimension('A')->setWidth(6);
		$sheet->setCellValue('B2','Kategori');
		$sheet->setCellValue('C2','Item Tindakan');
		$sheet->setCellValue('D2','Jumlah');
		$sheet->getColumnDimension('B')->setWidth(20);
		$sheet->getColumnDimension('C')->setWidth(40);
		$sheet->getColumnDimension('D')->setWidth(10);	
	

		$rowstart = 3;
		$index = 1;
		$total = 0;
		foreach($parents->searchAll() as $data)
		{
			$model->item_id = $data->item_id;
			$count = $model->countTindakan();		
			$total += $count;
			$sheet->setCellValue('A'.$rowstart,$index);
			$sheet->setCellValue('B'.$rowstart,$data->parent->nama);
			$sheet->setCellValue('C'.$rowstart,$data->item->nama);
			$sheet->setCellValue('D'.$rowstart,$count);
			$rowstart++;
			$index ++;
		}

		$sheet->setCellValue('A'.$rowstart,'TOTAL');
		$sheet->mergeCells('A'.$rowstart.':C'.$rowstart);
		$sheet->setCellValue('D'.$rowstart,$total);
	
		$sheet->getStyle('A')->getAlignment()->setHorizontal('center');
		$sheet->getStyle('B:C')->getAlignment()->setHorizontal('left');
		$sheet->getStyle('D')->getAlignment()->setHorizontal('center');


 		header('Content-Type: application/vnd.ms-excel;charset=utf-8');
		header('Content-Disposition: attachment;filename="laporan_jumlah_tindakan_lab.xlsx"');
		header('Cache-Control: max-age=0');
		
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		ob_end_clean();
		ob_start();
		$writer->save('php://output');
		die();
	}

	public function actionLabTindakan()
	{
		$parents = new DmLabItemParent;
		$model = new LabRequestItem;
		$model->TANGGAL_AWAL = date('01/m/Y');
		$model->TANGGAL_AKHIR = date('d/m/Y');
		$results = [];
		foreach($parents->searchAll() as $data)
		{
			$results[$data->id] = 0;
		}

		if(!empty($_POST['LabRequestItem']['TANGGAL_AWAL']) && !empty($_POST['LabRequestItem']['TANGGAL_AKHIR']))
		{

			$sd = Yii::app()->helper->convertSQLDate($_POST['LabRequestItem']['TANGGAL_AWAL']);
			$ed = Yii::app()->helper->convertSQLDate($_POST['LabRequestItem']['TANGGAL_AKHIR']);
			$model->TANGGAL_AWAL = $sd;;
			$model->TANGGAL_AKHIR = $ed;
			foreach($parents->searchAll() as $data)
			{

				$model->item_id = $data->item_id;
				$results[$data->id] = $model->countTindakan();		
			}
		}

		$this->render('lab_tindakan',[
			'model' => $model,
			'parents' => $parents,
			'results' => $results
		]);
	}

	public function actionOkRekap()
	{
		$model = new TdOkBiaya;
		$model->TANGGAL_AWAL = date('01/m/Y');
		$model->TANGGAL_AKHIR = date('d/m/Y');

		if(!empty($_POST['TdOkBiaya']['TANGGAL_AWAL']) && !empty($_POST['TdOkBiaya']['TANGGAL_AKHIR']))
		{
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->setActiveSheetIndex(0);
			$sd = Yii::app()->helper->convertSQLDate($_POST['TdOkBiaya']['TANGGAL_AWAL']);
			$ed = Yii::app()->helper->convertSQLDate($_POST['TdOkBiaya']['TANGGAL_AKHIR']);

			$url = "/ok/rekap/bulanan";
			$params = [
				'sd' => $sd,'ed'=>$ed
			];
			$list = Yii::app()->rest->getDataApi($url,$params);

			$results = [];
			
			$results['items'] = $list->values;

			$sum_khusus = 0; $sum_besar= 0; $sum_sedang= 0; $sum_kecil= 0; $sum_cito= 0; $sum_elektif= 0; $sum_vip= 0; $sum_kelas1= 0; $sum_kelas2= 0; $sum_kelas3= 0; $sum_la= 0; $sum_sab= 0; $sum_ga= 0; $sum_epi= 0; $sum_per= 0; $sum_bpjs= 0; $sum_umum= 0;
			
			$sheet->setCellValue('A1', 'Laporan Rekap Tindakan OK');
			$sheet->mergeCells('A1:S1');
			$alpha = ['C','D','E','F','G','H','I','J','K','L','M','N','S','T'];
			$sheet->setCellValue('A2','No');
			$sheet->getColumnDimension('A')->setWidth(6);
			$sheet->mergeCells('A2:A3');
			$sheet->setCellValue('B2','UPF/Dokter');
			$sheet->getColumnDimension('B')->setWidth(40);
			foreach($alpha as $a)
				$sheet->getColumnDimension($a)->setWidth(10);	
			$sheet->mergeCells('B2:B3');
			$sheet->setCellValue('C2','Jenis Operasi');
			$sheet->mergeCells('C2:G2');
			$sheet->setCellValue('C3','KHUSUS');
			$sheet->setCellValue('D3','BESAR');
			$sheet->setCellValue('E3','SEDANG');
			$sheet->setCellValue('F3','KECIL');
			$sheet->setCellValue('G3','TOTAL');
			$sheet->setCellValue('H2','CITO');
			$sheet->mergeCells('H2:H3');
			$sheet->setCellValue('I2','ELEKTIF');
			$sheet->mergeCells('I2:I3');
			$sheet->setCellValue('J2','KELAS');
			$sheet->setCellValue('J3','VIP');
			$sheet->setCellValue('K3','I');
			$sheet->setCellValue('L3','II');
			$sheet->setCellValue('M3','III');
			$sheet->mergeCells('J2:M2');
			$sheet->setCellValue('N2','ANASTESI');
			$sheet->setCellValue('N3','LA');
			$sheet->setCellValue('O3','SAB');
			$sheet->setCellValue('P3','GA');
			$sheet->setCellValue('Q3','EPI');
			$sheet->setCellValue('R3','PER');
			$sheet->mergeCells('N2:R2');
			$sheet->setCellValue('S2','BPJS');
			$sheet->mergeCells('S2:S3');
			$sheet->setCellValue('T2','UMUM');
			$sheet->mergeCells('T2:T3');
			$listUpf = [];

			$rowstart = 4;
			$index = 0;
			$sum_tindakan = 0;
			foreach($list->values as $q => $v)
			{
				$total_tindakan = $v->khusus + $v->besar + $v->sedang + $v->kecil;
				if(!in_array($v->nama,$listUpf))
				{
					$listUpf[] = $v->nama;
					$sheet->setCellValue('A'.$rowstart,' ');
					$sheet->setCellValue('B'.$rowstart,$v->nama);
					$sheet->mergeCells('B'.$rowstart.':T'.$rowstart);
					$index = 0;
					$rowstart++;
				}
				$index++;
				$sheet->setCellValue('A'.$rowstart,$index);
				$sheet->setCellValue('B'.$rowstart,$v->nama_dokter);
				$sheet->setCellValue('C'.$rowstart,$v->khusus);
				$sheet->setCellValue('D'.$rowstart,$v->besar);
				$sheet->setCellValue('E'.$rowstart,$v->sedang);
				$sheet->setCellValue('F'.$rowstart,$v->kecil);
				$sheet->setCellValue('G'.$rowstart,$total_tindakan);
				$sheet->setCellValue('H'.$rowstart,$v->cito);
				$sheet->setCellValue('I'.$rowstart,$v->elektif);
				$sheet->setCellValue('J'.$rowstart,$v->vip);
				$sheet->setCellValue('K'.$rowstart,$v->kelas1);
				$sheet->setCellValue('L'.$rowstart,$v->kelas2);
				$sheet->setCellValue('M'.$rowstart,$v->kelas3);
				$sheet->setCellValue('N'.$rowstart,$v->la);
				$sheet->setCellValue('O'.$rowstart,$v->sab);
				$sheet->setCellValue('P'.$rowstart,$v->ga);
				$sheet->setCellValue('Q'.$rowstart,$v->epi);
				$sheet->setCellValue('R'.$rowstart,$v->per);
				$sheet->setCellValue('S'.$rowstart,$v->bpjs);
				$sheet->setCellValue('T'.$rowstart,$v->umum);

				$sum_khusus += $v->khusus;
				$sum_besar += $v->besar;
				$sum_sedang += $v->sedang;
				$sum_kecil += $v->kecil;
				$sum_tindakan += $total_tindakan;
				$sum_cito += $v->cito;
				$sum_elektif += $v->elektif;
				$sum_vip += $v->vip;
				$sum_kelas1 += $v->kelas1;
				$sum_kelas2 += $v->kelas2;
				$sum_kelas3 += $v->kelas3;
				$sum_la += $v->la;
				$sum_sab += $v->sab;
				$sum_ga += $v->ga;
				$sum_epi += $v->epi;
				$sum_per += $v->per;
				$sum_bpjs += $v->bpjs;
				$sum_umum += $v->umum;

				$rowstart++;
			}
			$sheet->setCellValue('A'.$rowstart,'TOTAL');
			$sheet->mergeCells('A'.$rowstart.':B'.$rowstart);
			$sheet->setCellValue('C'.$rowstart,$sum_khusus);
			$sheet->setCellValue('D'.$rowstart,$sum_besar);
			$sheet->setCellValue('E'.$rowstart,$sum_sedang);
			$sheet->setCellValue('F'.$rowstart,$sum_kecil);
			$sheet->setCellValue('G'.$rowstart,$sum_tindakan);
			$sheet->setCellValue('H'.$rowstart,$sum_cito);
			$sheet->setCellValue('I'.$rowstart,$sum_elektif);
			$sheet->setCellValue('J'.$rowstart,$sum_vip);
			$sheet->setCellValue('K'.$rowstart,$sum_kelas1);
			$sheet->setCellValue('L'.$rowstart,$sum_kelas2);
			$sheet->setCellValue('M'.$rowstart,$sum_kelas3);
			$sheet->setCellValue('N'.$rowstart,$sum_la);
			$sheet->setCellValue('O'.$rowstart,$sum_sab);
			$sheet->setCellValue('P'.$rowstart,$sum_ga);
			$sheet->setCellValue('Q'.$rowstart,$sum_epi);
			$sheet->setCellValue('R'.$rowstart,$sum_per);
			$sheet->setCellValue('S'.$rowstart,$sum_bpjs);
			$sheet->setCellValue('T'.$rowstart,$sum_umum);
			$sheet->getStyle('B')->getAlignment()->setHorizontal('left');
			$sheet->getStyle('A')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('C:T')->getAlignment()->setHorizontal('center');


	 		header('Content-Type: application/vnd.ms-excel;charset=utf-8');
			header('Content-Disposition: attachment;filename="laporan_jumlah_tindakan_ok.xlsx"');
			header('Cache-Control: max-age=0');
			
			$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
			ob_end_clean();
			ob_start();
			$writer->save('php://output');
			die();
		}

		$this->render('operasi_rekap',[
			'model' => $model
		]);
	}

	public function actionDownload($bulan, $tahun){
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->setActiveSheetIndex(0);
		

		$bulan = !empty($bulan) ? $bulan : date('m');
		$tahun = !empty($tahun) ? $tahun : date('Y');
			
		$sd = $tahun.'-'.$bulan.'-01';
		$ed = date('Y-m-t',strtotime($sd));

		$params = [
			'tahun' => $tahun,
			'bulan' => $bulan,
			'tipe' => 2
		];

		$url = "/p/unit/list";
		$listUnit = Yii::app()->rest->getDataApi($url,$params);
		$results = [];
		$rowstart = 2;
		$sheet->setCellValue('A1', 'Laporan Kunjungan Poli Bulan '.$bulan.' Tahun '.$tahun);
		$sheet->mergeCells('A1:M1');
		$alpha = ['C','D','E','F','G','H','I','J','K','L','M'];
		$sheet->mergeCells('A'.$rowstart.':A'.($rowstart+1));
		$sheet->setCellValue('A'.$rowstart, 'No');
		$sheet->getColumnDimension('A')->setWidth(5);
		$sheet->mergeCells('B'.$rowstart.':B'.($rowstart+1));
		$sheet->setCellValue('B'.$rowstart, 'Unit');
		$sheet->getColumnDimension('B')->setWidth(30);
		$sheet->mergeCells('C'.$rowstart.':D'.$rowstart);
		$sheet->setCellValue('C'.$rowstart, 'JK');
		$sheet->mergeCells('E'.$rowstart.':M'.$rowstart);
		$sheet->setCellValue('E'.$rowstart, 'Usia');
		$sheet->setCellValue('C'.($rowstart+1), 'L');
		$sheet->getColumnDimension('C')->setWidth(10);
		$sheet->getColumnDimension('D')->setWidth(10);
		$sheet->setCellValue('D'.($rowstart+1), 'P');
		$sheet->setCellValue('E'.($rowstart+1), '0-5');
		$sheet->getColumnDimension('E')->setWidth(10);
		$sheet->setCellValue('F'.($rowstart+1), '6-11');
		$sheet->getColumnDimension('F')->setWidth(10);
		$sheet->setCellValue('G'.($rowstart+1), '12-16');
		$sheet->getColumnDimension('G')->setWidth(10);
		$sheet->setCellValue('H'.($rowstart+1), '17-25');
		$sheet->getColumnDimension('H')->setWidth(10);
		$sheet->setCellValue('I'.($rowstart+1), '26-35');
		$sheet->getColumnDimension('I')->setWidth(10);
		$sheet->setCellValue('J'.($rowstart+1), '36-45');
		$sheet->getColumnDimension('J')->setWidth(10);
		$sheet->setCellValue('K'.($rowstart+1), '46-55');
		$sheet->getColumnDimension('K')->setWidth(10);
		$sheet->setCellValue('L'.($rowstart+1), '56-65');
		$sheet->getColumnDimension('L')->setWidth(10);
		$sheet->setCellValue('M'.($rowstart+1), '> 65');
		$sheet->getColumnDimension('M')->setWidth(10);

		$i = $rowstart+2;
		foreach($listUnit->values as $q => $gol)
		{	

			$url = '/p/gol/sexusia';

			$params = [
				'sd' => $sd,
				'ed' => $ed,
				'tipe' => 'poli',
				'key' => $gol->KodeUnit
			];
			$sheet->setCellValue('A'.$i, $q+1);
			$sheet->setCellValue('B'.$i, $gol->NamaUnit);
			$tmp = Yii::app()->rest->getDataApi($url,$params);
			// $results['listUnit'][] = [
			// 	'unit' => $gol->NamaUnit,
			// 	'items' => $tmp->values
			// ];
			// print_r($tmp->values[10]);exit;
			for($j=0;$j<11;$j++)
			{

				$obj = !empty($tmp->values[$j]) ? $tmp->values[$j] : NULL;
				if(!empty($obj) && $obj->count != NULL)
				{
					$sheet->setCellValue($alpha[$j].$i, $obj->count);
				}

				else
				{
					$sheet->setCellValue($alpha[$j].$i, 0);	
				}
				
			}

			$i++;
		}
		$sheet->getStyle('A:M')->getAlignment()->setHorizontal('center');
		
		header('Content-Type: application/vnd.ms-excel;charset=utf-8');
		header('Content-Disposition: attachment;filename="laporan_kunjungan.xlsx"');
		header('Cache-Control: max-age=0');
		
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		ob_end_clean();
		ob_start();
		$writer->save('php://output');
	}

	public function actionKunjungan()
	{

		if(!empty($_POST['bulan']))
		{
			
			$this->redirect(['download','bulan'=>$_POST['bulan'],'tahun'=>$_POST['tahun']]);
			

		}
		$this->render('kunjungan');
	}

	public function actionCssd($tgl_awal='', $tgl_akhir='', $print=0)
	{

		// print_r($_GET);exit;
		$this->title = 'Laporan CSSD | '.Yii::app()->name;
		$model=new TdOkBiaya;

		if(!empty($_GET['TdOkBiaya']['TANGGAL_AWAL']) && !empty($_GET['TdOkBiaya']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_GET['TdOkBiaya']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_GET['TdOkBiaya']['TANGGAL_AKHIR']);
		}

		if(!empty($_GET['TANGGAL_AWAL']) && !empty($_GET['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = ($_GET['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = ($_GET['TANGGAL_AKHIR']);
			$_POST['print'] = 0;
		}


		$print = !empty($_GET['print']) ? $_GET['print'] : 0;
		
		switch ($print) {
			case 0:
			
				$this->render('cssd',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
					'print' => $print
				));
				break;
			
			
			case 2:
				$model->TANGGAL_AWAL = $tgl_awal;
				$model->TANGGAL_AKHIR = $tgl_akhir;
				
				echo $this->renderPartial('cssd_table',array(
					'model'=>$model,
					'tanggal_awal' => $tgl_awal,
					'tanggal_akhir' => $tgl_akhir,
					'print' => $print
				));
			break;
		}

		
	
	}

	public function actionAjaxDokter(){
		$params = [
			'startdate' => $_POST['sd'],
			'enddate' => $_POST['ed'],
		];

		$result = Yii::app()->rest->getListRekapDokterBulanan($params);
		if(!empty($result->values))
			echo CJSON::encode($result->values);
		else{
			$data[] = [
				'id' => 0,
				'nama' => 'Data Empty',
				'jenis' => 'Data Empty'
			];
			echo CJSON::encode($data);
		}
	}

	public function actionResep($tgl_awal='', $tgl_akhir='', $print=0)
	{

		// print_r($_GET);exit;
		$this->title = 'Laporan Resep Kamar | '.Yii::app()->name;
		$model=new TrResepKamar;

		if(!empty($_GET['TrResepKamar']['TANGGAL_AWAL']) && !empty($_GET['TrResepKamar']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_GET['TrResepKamar']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_GET['TrResepKamar']['TANGGAL_AKHIR']);
		}

		$id_dokter = 0;
		if(!empty($_GET['TANGGAL_AWAL']) && !empty($_GET['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = ($_GET['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = ($_GET['TANGGAL_AKHIR']);
			$_POST['print'] = 0;

		}


		$print = !empty($_GET['print']) ? $_GET['print'] : 0;
		
		switch ($print) {
			case 0:
				$params = 'non_xls';
				$this->render('resep',array(
					'model'=>$model,
					'params' => $params,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			
			case 2:
				$model->TANGGAL_AWAL = $tgl_awal;
				$model->TANGGAL_AKHIR = $tgl_akhir;
				$params = 'xls';
				echo $this->renderPartial('resep_table',array(
					'model'=>$model,
					'params' => $params,
					'tanggal_awal' => $tgl_awal,
					'tanggal_akhir' => $tgl_akhir,
				));
			break;
		}

		
	
	}


	public function actionRawatInap()
	{
		$this->title = 'Laporan Kamar | '.Yii::app()->name;
		$model=new TrRawatInap;
		$rawatInaps = [];
		if(!empty($_GET['TrRawatInap']['TANGGAL_AWAL']) && !empty($_GET['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_GET['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_GET['TrRawatInap']['TANGGAL_AKHIR']);	
			// $model->TIPE_PASIEN = $_GET['TrRawatInap']['TIPE_PASIEN'];
			$rawatInaps = $model->searchLaporanAll();

			$this->renderPartial('_rawatInapPasien',array(
				'model'=>$model,
				'rawatInaps' => $rawatInaps
			));

			exit;
		}

		else
		{
			$this->render('rawatInap',array(
				'model'=>$model,
				'rawatInaps' => $rawatInaps
			));
		}
	}

	
	public function actionIrd()
	{
		$this->title = 'Laporan Kamar | '.Yii::app()->name;
		$model=new TrRawatInap;
		$rawatInaps = [];
		if(!empty($_GET['TrRawatInap']['TANGGAL_AWAL']) && !empty($_GET['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_GET['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_GET['TrRawatInap']['TANGGAL_AKHIR']);	
			// $model->TIPE_PASIEN = $_GET['TrRawatInap']['TIPE_PASIEN'];
			$rawatInaps = $model->searchLaporanAll();
			
			$this->renderPartial('_irdPasien',array(
				'model'=>$model,
				'rawatInaps' => $rawatInaps
			));

			exit();
		}

		else
		{
			$this->render('ird',array(
				'model'=>$model,
				'rawatInaps' => $rawatInaps
			));
		}
	}

	public function actionRekapDokter()
	{
		$this->title = 'Laporan Rekapitulasi Jasa Dokter | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}

		$print = !empty($_POST['print']) ? $_POST['print'] : 0;
		$jenis = !empty($_POST['jenisPasien']) ? $_POST['jenisPasien'] : '-';

		switch ($print) {
			case 0:
				$this->render('rekap_dokter',array(

					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_rekap_dokter',array(
					'jenis' => $jenis,
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2:
				echo $this->renderPartial('xls_rekap_dokter',array(
					'jenis' => $jenis,
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
			break;
		}

		
	
	}

	public function actionPrwtAn()
	{
		$this->title = 'Laporan Perawat Anestesi | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}

		$print = !empty($_POST['print'])  ? $_POST['print'] : 0;

		switch ($print) {
			case 0:
				$this->render('prwt_an',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_prwt_an',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2:
				echo $this->renderPartial('xls_prwt_an',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
		}
	}

	public function actionPrwtOp()
	{
		$this->title = 'Laporan Perawat Operasi | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}

		$print = !empty($_POST['print'])  ? $_POST['print'] : 0;

		switch ($print) {
			case 0:
				$this->render('prwt_op',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();

				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_prwt_op',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2:
				echo $this->renderPartial('xls_prwt_op',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
		}
	}

	public function actionAnestesi()
	{
		$this->title = 'Laporan Tindakan Anestesi | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}

		$print = !empty($_POST['print']) ? $_POST['print'] : 0;

		switch ($print) {
			case 0:
				$this->render('anestesi',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_anestesi',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2:

				echo $this->renderPartial('xls_anestesi',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
			break;
		}
	}

	public function actionAlkes()
	{
		$this->title = 'Laporan Alkes | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}

		$print = !empty($_POST['print']) ? $_POST['print'] : 0;

		switch ($print) {
			case 0:
				$this->render('alkes',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_alkes',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2 :
			echo $this->renderPartial('xls_alkes',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
		}
	}

	public function actionObat()
	{
		$this->title = 'Laporan Obat | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}


		$print = !empty($_POST['print']) ? $_POST['print'] : 0;

		switch ($print) {
			case 0:


				$this->render('obat',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
					'kamar_pasien' => $model->KAMAR_PASIEN,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_obat',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
					'kamar_pasien' => $model->KAMAR_PASIEN,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2:
				echo $this->renderPartial('xls_obat',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
					'kamar_pasien' => $model->KAMAR_PASIEN,
				));

				break;
		}
	}

	public function actionJasaDokter()
	{
		$this->title = 'Laporan Jasa Dokter | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}

		$print = !empty($_POST['print'])? $_POST['print'] : 0;

		switch ($print) {
			case 0:
				$this->render('jasaDokter',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_jasaDokter',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2 :
				echo $this->renderPartial('xls_jasaDokter',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
			break;
		}	
	}

	public function actionRr()
	{
		$this->title = 'Laporan RR | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}

		$print = !empty($_POST['print'])  ? $_POST['print'] : 0;

		switch ($print) {
			case 0:
				$this->render('rr',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_rr',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2:
				echo $this->renderPartial('xls_rr',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				break;
		}
	
	}

	public function actionOperasi($tgl_awal='', $tgl_akhir='', $print=0)
	{

		// print_r($_GET);exit;
		$this->title = 'Laporan Tindakan Operasi | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_GET['TrRawatInap']['TANGGAL_AWAL']) && !empty($_GET['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_GET['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_GET['TrRawatInap']['TANGGAL_AKHIR']);
		}

		$id_dokter = 0;
		if(!empty($_GET['TANGGAL_AWAL']) && !empty($_GET['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = ($_GET['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = ($_GET['TANGGAL_AKHIR']);
			$_POST['print'] = 0;
			$id_dokter = $_POST['id_dokter'];
		}


		$print = !empty($_GET['print']) ? $_GET['print'] : 0;
		
		switch ($print) {
			case 0:
				$this->render('operasi',array(
					'id_dokter' => $id_dokter,
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			
			case 2:
				$model->TANGGAL_AWAL = $tgl_awal;
				$model->TANGGAL_AKHIR = $tgl_akhir;
				echo $this->renderPartial('xls_operasi',array(
					'model'=>$model,
					'tanggal_awal' => $tgl_awal,
					'tanggal_akhir' => $tgl_akhir,
				));
			break;
		}

		
	
	}

	public function actionHdPasien($oid,$t1,$t2,$print=0)
	{
		$this->title = 'Laporan Hemodialisis | '.Yii::app()->name;
		$model=new TrRawatInap;

		$model->TANGGAL_AWAL = $t1;
		$model->TANGGAL_AKHIR = $t2;
		$penunjang = TindakanMedisPenunjang::model()->findByPk($oid);
		$listRI = array();
		
		foreach($model->searchLaporan() as $m)
		{
			foreach($m->trRawatInapPenunjangs as $vd)
			{
				if(($vd->penunjang->jenis_penunjang == 4) && ($vd->id_penunjang == $oid))
					$listRI[] = $vd->id_rawat_inap;
			}
			
		}


		$listRI = array_filter(array_unique($listRI));
		
		$rawatInaps = array();
		foreach($listRI as $item)
		{
			$ri = TrRawatInap::model()->findByPk($item);
			$rawatInaps[] = $ri;
		}

		switch ($print) {
			case 0:
				$this->render('hdPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,

					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_hdPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,

					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2:
				echo $this->renderPartial('xls_hdPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,

					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
		}

		
	}

	public function actionHd()
	{
		$this->title = 'Laporan Hemodialisis | '.Yii::app()->name;
		$model=new TrRawatInap;

		
		$listRI = array();

		$listAnatomi = array();
		$penunjangs = array();
		


		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);

			foreach($model->searchLaporan() as $m)
			{

				foreach($m->trRawatInapPenunjangs as $vd)
				{

					// 2 = pato klinik
					if($vd->penunjang->jenis_penunjang == 4)
						$listAnatomi[] = $vd->id_penunjang;
				}
			}

			$listAnatomi = array_filter(array_unique($listAnatomi));
			
			foreach($listAnatomi as $item)
			{
				$dr = TindakanMedisPenunjang::model()->findByPk($item);
				$penunjangs[] = $dr;
			}
		}


		$this->render('hd',array(
			'model'=>$model,

			'penunjangs' => $penunjangs,
			'tanggal_awal' => $model->TANGGAL_AWAL,
			'tanggal_akhir' => $model->TANGGAL_AKHIR,
		));
	
	}

	public function actionGizi()
	{
		$this->title = 'Laporan Gizi | '.Yii::app()->name;
		$model=new TrRawatInap;

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
		}


		$print = !empty($_POST['print']) ? $_POST['print'] : 0;

		switch ($print) {
			case 0:
				$this->render('gizi',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_gizi',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;

			case 2: // export to xls
				// ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				$this->layout = '';
				 $this->renderPartial('xls_gizi',array(
					'model'=>$model,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				// $data = ob_get_clean();
				// ob_start();
				break;
		}

		
	
	}

	public function actionPatologiAnatomiPasien($oid,$t1,$t2, $print=0)
	{
		$this->title = 'Laporan Patologi Anatomi | '.Yii::app()->name;
		$model=new TrRawatInap;

		$model->TANGGAL_AWAL = $t1;
		$model->TANGGAL_AKHIR = $t2;
		$penunjang = TindakanMedisPenunjang::model()->findByPk($oid);
		$listRI = array();
		
		foreach($model->searchLaporan() as $m)
		{
			foreach($m->trRawatInapPenunjangs as $vd)
			{
				if(($vd->penunjang->jenis_penunjang == 3) && ($vd->id_penunjang == $oid))
					$listRI[] = $vd->id_rawat_inap;
			}
			
		}


		$listRI = array_filter(array_unique($listRI));
		
		$rawatInaps = array();
		foreach($listRI as $item)
		{
			$ri = TrRawatInap::model()->findByPk($item);
			$rawatInaps[] = $ri;
		}

		switch ($print) {
			case 0:
				$this->render('patologiAnatomiPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,

					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_patologiAnatomiPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,

					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2 :
				echo $this->renderPartial('xls_patologiAnatomiPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,

					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

			break;
		}

		
	}

	public function actionPatologiAnatomi()
	{
		$this->title = 'Laporan Patologi Anatomi | '.Yii::app()->name;
		$model=new TrRawatInap;

		

		
		$listRI = array();
		$penunjangs = array();
		$listAnatomi = array();
		

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);

			foreach($model->searchLaporan() as $m)
			{

				foreach($m->trRawatInapPenunjangs as $vd)
				{

					// 2 = pato klinik
					if($vd->penunjang->jenis_penunjang == 3)
						$listAnatomi[] = $vd->id_penunjang;
				}
			}

			$listAnatomi = array_filter(array_unique($listAnatomi));
			
			foreach($listAnatomi as $item)
			{
				$dr = TindakanMedisPenunjang::model()->findByPk($item);
				$penunjangs[] = $dr;
			}
		}

		$this->render('patologiAnatomi',array(
			'model'=>$model,

			'penunjangs' => $penunjangs,
			'tanggal_awal' => $model->TANGGAL_AWAL,
			'tanggal_akhir' => $model->TANGGAL_AKHIR,
		));
	
	}

	public function actionPatologiKlinikPasien($oid,$jns,$t1,$t2, $print=0)
	{

		$this->title = 'Laporan Patologi Klinik | '.Yii::app()->name;
		$model=new TrRawatInap;

		$model->TANGGAL_AWAL = $t1;
		$model->TANGGAL_AKHIR = $t2;

		$obat = new ObatAlkes;
		$penunjang = new TindakanMedisPenunjang;

		switch($jns)
		{
			case 1 : // obat

				$obat = ObatAlkes::model()->findByPk($oid);

			break;

			case 2 : // penunjang

				$penunjang = TindakanMedisPenunjang::model()->findByPk($oid);


			break;
		}

		

		$listRI = array();
		
		foreach($model->searchLaporan() as $m)
		{
			foreach($m->trRawatInapAlkes as $vd)
			{
				if(($vd->alkes->param == 'patoklinik') && ($vd->id_alkes == $oid))
					$listRI[] = $vd->id_rawat_inap;
			}

			foreach($m->trRawatInapPenunjangs as $vd)
			{
				if(($vd->penunjang->jenis_penunjang == 2) && ($vd->id_penunjang == $oid))
					$listRI[] = $vd->id_rawat_inap;
			}
			
		}


		$listRI = array_filter(array_unique($listRI));
		
		$rawatInaps = array();
		foreach($listRI as $item)
		{
			$ri = TrRawatInap::model()->findByPk($item);
			$rawatInaps[] = $ri;
		}

		switch ($print) {
			case 0:
				$this->render('patologiKlinikPasien',array(
					'model'=>$model,
					'oid' => $oid,
					'jns' => $jns,
					'rawatInaps' => $rawatInaps,
					'obat' => $obat,
					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));	
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_patologiKlinikPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'obat' => $obat,
					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));	

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2 :

				echo $this->renderPartial('xls_patologiKlinikPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'obat' => $obat,
					'penunjang' => $penunjang,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));	

				break;
		}

		
	}

	public function actionPatologiKlinik()
	{
		$this->title = 'Laporan Patologi Klinik | '.Yii::app()->name;
		
		$model=new TrRawatInap;

		

		$listObat = array();
		$listRI = array();
		$obats = array();
		$listLab = array();
		$penunjangs = array();
		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
			foreach($model->searchLaporan() as $m)
			{

				foreach($m->trRawatInapAlkes as $vd)
				{

					if($vd->alkes->param == 'patoklinik')
						$listObat[] = $vd->id_alkes;
				}
			}

			$listObat = array_filter(array_unique($listObat));
			
			// print_r($listDokter);exit;
			
			foreach($listObat as $item)
			{
				$dr = ObatAlkes::model()->findByPk($item);
				$obats[] = $dr;
			}

			
			foreach($model->searchLaporan() as $m)
			{

				foreach($m->trRawatInapPenunjangs as $vd)
				{

					// 2 = pato klinik
					if($vd->penunjang->jenis_penunjang == 2)
						$listLab[] = $vd->id_penunjang;
				}
			}

			$listLab = array_filter(array_unique($listLab));
			
			foreach($listLab as $item)
			{
				$dr = TindakanMedisPenunjang::model()->findByPk($item);
				$penunjangs[] = $dr;
			}
		}
		


		$this->render('patologiKlinik',array(
			'model'=>$model,
			'obats' => $obats,
			'penunjangs' => $penunjangs,
			'tanggal_awal' => $model->TANGGAL_AWAL,
			'tanggal_akhir' => $model->TANGGAL_AKHIR,
		));
	
	}

	public function actionRadiologi()
	{
		$this->title = 'Laporan Radiologi | '.Yii::app()->name;
		
		$model=new TrRawatInap;	

		
		$listRadio = array();
		$listRI = array();
		

		
		
		// print_r($listDokter);exit;
		$penunjangs = array();
		

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
			foreach($model->searchLaporan() as $m)
			{

				foreach($m->trRawatInapPenunjangs as $vd)
				{

					if($vd->penunjang->jenis_penunjang == 0)
						$listRadio[] = $vd->id_penunjang;
				}
			}

			$listRadio = array_filter(array_unique($listRadio));

			foreach($listRadio as $item)
			{
				$dr = TindakanMedisPenunjang::model()->findByPk($item);
				$penunjangs[] = $dr;
			}
		}


		$this->render('radiologi',array(
			'model'=>$model,
			'penunjangs' => $penunjangs,
			'tanggal_awal' => $model->TANGGAL_AWAL,
			'tanggal_akhir' => $model->TANGGAL_AKHIR,
		));
	
	}

	public function actionRadiologiPasien($rid,$t1,$t2, $print = 0)
	{
		$this->title = 'Laporan Radiologi Pasien | '.Yii::app()->name;
		
		$model=new TrRawatInap;

		$model->TANGGAL_AWAL = $t1;
		$model->TANGGAL_AKHIR = $t2;

		$radio = TindakanMedisPenunjang::model()->findByPk($rid);

		$listRI = array();
		
		foreach($model->searchLaporan() as $m)
		{
			foreach($m->trRawatInapPenunjangs as $vd)
			{
				if(($vd->penunjang->jenis_penunjang == 0) && ($vd->id_penunjang == $rid))
					$listRI[] = $vd->id_rawat_inap;
			}
			
		}


		$listRI = array_filter(array_unique($listRI));
		
		$rawatInaps = array();
		foreach($listRI as $item)
		{
			$ri = TrRawatInap::model()->findByPk($item);
			$rawatInaps[] = $ri;
		}

		switch ($print) {
			case 0:
				$this->render('radiologiPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'radio' => $radio,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_radiologiPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'radio' => $radio,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2: // export to xls
				// ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				$this->layout = '';
				echo $this->renderPartial('xls_radiologiPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'radio' => $radio,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				// $data = ob_get_clean();
				// ob_start();
				break;
		}

		
	}

	public function actionKamarPasien($kid,$t1,$t2,$tipe, $print=0)
	{
		$this->title = 'Laporan Kamar | '.Yii::app()->name;

		$kamar = DmKamar::model()->findByPk($kid);

		$listRI = array();
		$listTindakan = array();

		$model=new TrRawatInap;

		$model->TANGGAL_AWAL = $t1;
		$model->TANGGAL_AKHIR = $t2;
		$model->kamar_id = $kid;
		$model->TIPE_PASIEN = $tipe;

		foreach($model->searchLaporan() as $m)
		{

			if($m->kamar_id == $kid)
				$listRI[] = $m->id_rawat_inap;

			foreach($m->trRawatInapTnops as $vd)
			{
				
				$listRI[] = $vd->id_rawat_inap;
				
			}

		}



		$listRI = array_filter(array_unique($listRI));
		

		$rawatInaps = array();
		foreach($listRI as $item)
		{
			$ri = TrRawatInap::model()->findByPk($item);
			$rawatInaps[] = $ri;
		}

		// print_r($kamar);exit;
		
		switch ($print) {
			case 0:
				$this->render('kamarPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'kamar' => $kamar,
					'tipe' => $tipe,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));
				break;
			
			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_kamarPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'kamar' => $kamar,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				# code...
				break;
			case 2: // export to xls
				// ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				$this->layout = '';
				 $this->renderPartial('xls_kamarPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'kamar' => $kamar,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				// $data = ob_get_clean();
				// ob_start();
				break;
		}

		


	}

	public function actionKamar()
	{
		$this->title = 'Laporan Kamar | '.Yii::app()->name;
		$model=new TrRawatInap;
		$listKamar = array();
		$listRI = array();
		$kamars = array();
		// 

		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{


			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);
			
			if(!empty($_POST['TIPE_PASIEN']))
			$model->TIPE_PASIEN = $_POST['TIPE_PASIEN'];

			
			foreach($model->searchLaporan() as $m)
			{
				$listKamar[] = $m->kamar_id;
			}

			$listKamar = array_filter(array_unique($listKamar));
			
			// print_r($listDokter);exit;
			
			foreach($listKamar as $item)
			{
				$dr = DmKamar::model()->findByPk($item);
				$kamars[] = $dr;
			}
			
		}



		



		$this->render('kamar',array(
			'model'=>$model,
			'kamars' => $kamars,
			'tanggal_awal' => $model->TANGGAL_AWAL,
			'tanggal_akhir' => $model->TANGGAL_AKHIR,
		));
	
	}

	public function actionDokterPasien($drid,$t1,$t2,$print=0)
	{

		$this->title = 'Laporan Dokter | '.Yii::app()->name;
		
		$model=new TrRawatInap;

		$model->TANGGAL_AWAL = $t1;
		$model->TANGGAL_AKHIR = $t2;

		$dokter = DmDokter::model()->findByPk($drid);

		$listRI = array();
		
		foreach($model->searchDokter() as $m)
		{

			if(!empty($m->dokter_id))
				if($m->dokter_id == $drid)
					$listRI[] = $m->id_rawat_inap;
				

			foreach($m->tRIVisiteDokters as $vd)
			{
				if(!empty($vd->id_dokter_irna))
					if($vd->id_dokter_irna == $drid)
						$listRI[] = $vd->id_rawat_inap;
				
				if(!empty($vd->id_dokter))
					if($vd->id_dokter == $drid)
						$listRI[] = $vd->id_rawat_inap;
			}

			$rRincian = $m->trRawatInapRincians;
			if(!empty($rRincian))
				if($rRincian->dokter_ird == $drid)
					$listRI[] = $rRincian->id_rawat_inap;

			foreach($m->trRawatInapTops as $vd)
			{
				if(!empty($vd->id_dokter_jm))
					if($vd->id_dokter_jm == $drid)
						$listRI[] = $vd->id_rawat_inap;
				
				if(!empty($vd->id_dokter_anas))
					if($vd->id_dokter_anas == $drid)
						$listRI[] = $vd->id_rawat_inap;
			}

			foreach($m->trRawatInapTopJm as $vd)
			{
				if(!empty($vd->id_dokter))
					if($vd->id_dokter == $drid)
						$listRI[] = $vd->id_rawat_inap;
			}

			foreach($m->trRawatInapTopJa as $vd)
			{
				if(!empty($vd->id_dokter))
					if($vd->id_dokter == $drid)
						$listRI[] = $vd->id_rawat_inap;
			}

			foreach($m->trRawatInapPenunjangs as $vd)
			{
				if(!empty($vd->dokter_id))
					if($vd->dokter_id == $drid){
						$listRI[] = $vd->id_rawat_inap;
						// print_r($listRI);exit;
					}


			}
		}



		$listRI = array_filter(array_unique($listRI));
		

		$rawatInaps = array();
		foreach($listRI as $item)
		{
			$ri = TrRawatInap::model()->findByPk($item);
			$rawatInaps[] = $ri;
		}
		
		$params = 'non_xls';

		switch($print)
		{
			case 0:
				$this->render('dokterPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'dokter' => $dokter,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
					'params' => $params
				));
			break;

			case 1:
				$pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 
                        'L', 'mm', 'A4', true, 'UTF-8');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM-2);
				$pdf->AddPage();
				
				$this->layout = '';
				ob_start();
				//echo $this->renderPartial(“createnewpdf“,array(‘content’=>$content));
				
				echo $this->renderPartial('print_dokterPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'dokter' => $dokter,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
				));

				$data = ob_get_clean();
				ob_start();
				$pdf->writeHTML($data);

				$pdf->Output();
				break;
			case 2 :
				$params = 'xls';
				echo $this->renderPartial('_tbl_dokterPasien',array(
					'model'=>$model,
					'rawatInaps' => $rawatInaps,
					'dokter' => $dokter,
					'tanggal_awal' => $model->TANGGAL_AWAL,
					'tanggal_akhir' => $model->TANGGAL_AKHIR,
					'params' => $params
				));
				break;
		}

		

	}

	public function actionDokter()
	{
		$this->title = 'Laporan Dokter | '.Yii::app()->name;
		$model=new TrRawatInap;
		$dokters = array();
		// $time_start = microtime(true); 
		if(!empty($_POST['TrRawatInap']['TANGGAL_AWAL']) && !empty($_POST['TrRawatInap']['TANGGAL_AKHIR']))
		{
			$model->TANGGAL_AWAL = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AWAL']);
			$model->TANGGAL_AKHIR = Yii::app()->helper->convertSQLDate($_POST['TrRawatInap']['TANGGAL_AKHIR']);

			// $listDokter = array();
			// $listRI = array();

			// foreach($model->searchDokter() as $m)
			// {

			// 	$listDokter[] = $m->dokter_id;

			// 	foreach($m->tRIVisiteDokters as $vd)
			// 	{
			// 		if(!empty($vd->id_dokter_irna)&& $vd->biaya_visite_irna != 0)
			// 			$listDokter[] = $vd->id_dokter_irna;
					
			// 		if(!empty($vd->id_dokter)&& $vd->biaya_visite != 0)
			// 			$listDokter[] = $vd->id_dokter;
			// 	}

			// 	$rRincian = $m->trRawatInapRincians;

			// 	if(!empty($rRincian->dokter_ird))
			// 		$listDokter[] = $rRincian->dokter_ird;
					

			// 	foreach($m->trRawatInapTops as $vd)
			// 	{
			// 		if(!empty($vd->id_dokter_jm) && $vd->biaya_irna != 0 )
			// 			$listDokter[] = $vd->id_dokter_jm;
					
			// 		if(!empty($vd->id_dokter_anas))
			// 			$listDokter[] = $vd->id_dokter_anas;
			// 	}

			// 	foreach($m->trRawatInapPenunjangs as $vd)
			// 	{
			// 		if(!empty($vd->dokter_id) && $vd->biaya_irna != 0)
			// 			$listDokter[] = $vd->dokter_id;
					
			// 	}

			// }

			// $listDokter = array_filter(array_unique($listDokter));
			
			// // echo count($listDokter);			
			// foreach($listDokter as $item)
			// {
			// 	$dr = DmDokter::model()->findByPk($item);
			// 	$dokters[] = $dr;
			// }
		}


		// $time_end = microtime(true);

		$this->render('dokter',array(
			'model'=>$model,
			'dokters' => $dokters,
			'tanggal_awal' => $model->TANGGAL_AWAL,
			'tanggal_akhir' => $model->TANGGAL_AKHIR,
		));
	

		//dividing with 60 will give the execution time in minutes otherwise seconds
		// $execution_time = ($time_end - $time_start);

		// //execution time of the script
		// echo '<b>Total Execution Time:</b> '.$execution_time.' Secs';

		exit;
	}


	public function actionDokterPerKamar()
	{
		$this->title = 'Laporan Kamar | '.Yii::app()->name;
		$model=new TrRawatInap;
		$rawatInaps = [];
		if(Yii::app()->user->checkAccess([WebUser::R_OP_KAMAR]))
		{
			$km_id = Yii::app()->user->getState('kamar_master_id');
			$list_kamar = CHtml::listData(DmKamarMaster::model()->findAllByAttributes(['id'=>$km_id]),'id','nama_kamar');
		}
		else
			$list_kamar = CHtml::listData(DmKamarMaster::model()->findAll(['order'=>'nama_kamar ASC']),'id','nama_kamar');
		
		if(!empty($_GET['TrRawatInap']['TANGGAL_AWAL']) && !empty($_GET['TrRawatInap']['TANGGAL_AKHIR']))
		{
			
			
			$tgl_awal = Yii::app()->helper->convertSQLDate($_GET['TrRawatInap']['TANGGAL_AWAL']);
			$tgl_akhir = Yii::app()->helper->convertSQLDate($_GET['TrRawatInap']['TANGGAL_AKHIR']);
			$modelKamarMaster = DmKamarMaster::model()->findByPk($_GET['kamar']);
			$rawatInaps = $model->searchLaporanAllPerKamar($tgl_awal, $tgl_akhir, $modelKamarMaster->id);
			
			$this->renderPartial('dokterPerKamarPasien',array(
				'modelKamarMaster'=>$modelKamarMaster,
				'tgl_awal' => $tgl_awal,
				'tgl_akhir' => $tgl_akhir,
				'rawatInaps' => $rawatInaps
			));

			exit;
		}

		else
		{
			$this->render('dokterPerKamar',array(
				'model'=>$model,
				'rawatInaps' => $rawatInaps,
				'list_kamar' => $list_kamar
			));
		}
	}
}