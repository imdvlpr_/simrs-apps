<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'SIM RSUD Kabupaten Kediri',

	// Theme
	'theme' => 'ace',

	'timeZone' => 'Asia/Jakarta',
	
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'12345',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'class' => 'WebUser',
			'allowAutoLogin'=>true,
		),
		'cache'=>array(
            'class'=>'ext.redis.CRedisCache',
            //if you dont set up the servers options it will use the default one 
            //"host=>'127.0.0.1',port=>6379"
            'servers'=>array(
                array(
                        'host'=>'localhost',
                        'port'=>6379,
                ),
                //if you use 2 servers
                // array(
                //         'host'=>'server2',
                //         'port'=>6379,
                // ),
            ),
        ),
		'GoogleApis' => array(
			'class' => 'ext.GoogleApis.GoogleApis',
			'clientId' => '560325443158-gs308td6jhi6alkdc80pagtcpvvfr1q2.apps.googleusercontent.com',
            'clientSecret' => 'wAmp9d1R4vlUTsNgZoGqBKY_',
            'redirectUri' => 'http:/localhost/simrs/index.php',
            // // This is the API key for 'Simple API Access'
            'developerKey' => 'AIzaSyBlTXHMNYPx7mgryryPBIaFYUulb5rxM3o',
		),
/*
		'ePdf' => array(
			'class' => 'ext.yii-pdf.EYiiPdf',
			'params' => array(
				'HTML2PDF' => array(
					'librarySourcePath' => 'application.vendor.html2pdf.*',
					'classFile' 		=> 'html2pdf.class.php'
				),
			),
		),
*/
		'rest' => array(
			'class' => 'application.extensions.rest.MyRest',
			'id' => '22821',
			'secretkey' => '8uS2861A18',
			// 'id' => '25946',
			// 'secretkey' => '0kX4792E53',
			'baseurl_apigateway' => 'http://localhost:1988',
			'baseurl' => 'https://dvlp.bpjs-kesehatan.go.id/vclaim-rest',
			'baseurlVClaim' => 'http://dvlp.bpjs-kesehatan.go.id:8081/vclaim-rest',
			'baseurlAplicare' => 'http://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws',
		),

		'helper' => array(
			'class' => 'application.extensions.helper.MyHelper'
		),

		'printer' => array(
			'class' => 'application.extensions.printer.MyPrinter'
		),

		'outcli' => array(
			'class' => 'application.extensions.printer.MyOutputCLI'
		),

		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		'db'=>require(dirname(__FILE__) . '/db.php'),
		'dbErp'=>require(dirname(__FILE__) . '/dbErp.php'),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'kodeppk' => '1318R001',
		'sessionTimeoutSeconds'=>36000

	),
);