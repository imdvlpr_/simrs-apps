<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			// uncomment the following to provide test database connection
			'db'=>array(
				'connectionString' => 'mysql:host=192.168.52.1;dbname=simrs_test',
				'emulatePrepare' => true,
				'username' => 'sirs',
				'password' => 'sirs',
				'charset' => 'utf8',
			),
			
		),
	)
);
