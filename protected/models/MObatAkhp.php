<?php

/**
 * This is the model class for table "m_obat_akhp".
 *
 * The followings are the available columns in table 'm_obat_akhp':
 * @property string $kd_t
 * @property string $kd_barang
 * @property string $kd_js
 * @property string $nama_barang
 * @property string $nama_generik
 * @property string $kekuatan
 * @property string $satuan_kekuatan
 * @property string $satuan
 * @property string $jns_sediaan
 * @property string $b_i_r
 * @property string $gen_non
 * @property string $nar_p_non
 * @property string $oakrl
 * @property string $kronis
 * @property integer $stok
 * @property integer $stok_min
 * @property double $hb
 * @property double $hna
 * @property integer $diskon
 * @property double $hj
 */
class MObatAkhp extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'm_obat_akhp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kd_t, kd_barang', 'required'),
			array('stok, stok_min, diskon', 'numerical', 'integerOnly'=>true),
			array('hb, hna, hj', 'numerical'),
			array('kd_t, kd_barang, kd_js', 'length', 'max'=>20),
			array('nama_barang, nama_generik', 'length', 'max'=>200),
			array('kekuatan, satuan_kekuatan, satuan, jns_sediaan, b_i_r, gen_non, nar_p_non, oakrl, kronis', 'length', 'max'=>75),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kd_t, kd_barang, kd_js, nama_barang, nama_generik, kekuatan, satuan_kekuatan, satuan, jns_sediaan, b_i_r, gen_non, nar_p_non, oakrl, kronis, stok, stok_min, hb, hna, diskon, hj', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kd_t' => 'Kd T',
			'kd_barang' => 'Kd Barang',
			'kd_js' => 'Kd Js',
			'nama_barang' => 'Nama Barang',
			'nama_generik' => 'Nama Generik',
			'kekuatan' => 'Kekuatan',
			'satuan_kekuatan' => 'Satuan Kekuatan',
			'satuan' => 'Satuan',
			'jns_sediaan' => 'Jns Sediaan',
			'b_i_r' => 'B I R',
			'gen_non' => 'Gen Non',
			'nar_p_non' => 'Nar P Non',
			'oakrl' => 'Oakrl',
			'kronis' => 'Kronis',
			'stok' => 'Stok',
			'stok_min' => 'Stok Min',
			'hb' => 'Harga Beli',
			'hna' => 'Hna',
			'diskon' => 'Diskon',
			'hj' => 'Harga Jual',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$sort = new CSort();
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('kd_barang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kd_t',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama_barang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama_generik',$this->SEARCH,true,'OR');
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
	
	}

	public function searchObatBy($q)
	{
		$criteria=new CDbCriteria;
		// $criteria->addSearchCondition('kd_barang',$q,true,'OR');
		$criteria->addSearchCondition('nama_barang',$q,true,'OR');
		// $criteria->addSearchCondition('nama_generik',$q,true,'OR');
		$criteria->order = 'nama_barang';
		
		$criteria->limit = 20;
		

		$data = MObatAkhp::model()->findAll($criteria);
		
		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MObatAkhp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
