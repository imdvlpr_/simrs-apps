<?php

/**
 * This is the model class for table "td_ok_biaya".
 *
 * The followings are the available columns in table 'td_ok_biaya':
 * @property integer $id
 * @property double $biaya_bhn
 * @property integer $td_register_ok_id
 * @property double $biaya_dr_op
 * @property double $biaya_dr_anas
 * @property double $biaya_pwrt_ok
 * @property double $biaya_prwt_anas
 * @property string $created
 * @property integer $td_ok_tindakan_id
 *
 * The followings are the available model relations:
 * @property TdOkTindakan $tdOkTindakan
 * @property TdRegisterOk $tdRegisterOk
 */
class TdOkBiaya extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $TANGGAL_AWAL = '';
	public $TANGGAL_AKHIR = '';
	public $UPF = '';
	public $DOKTER = '';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'td_ok_biaya';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('td_register_ok_id, dr_operator, biaya_bhn, biaya_jrs, biaya_dr_op, biaya_dr_anas, biaya_prwt_ok, biaya_prwt_anas,biaya_rr_monitor, biaya_rr_askep', 'required'),
            array('td_register_ok_id, dr_operator', 'numerical', 'integerOnly'=>true),
            array('biaya_bhn, biaya_dr_op, biaya_dr_anas, biaya_prwt_ok, biaya_prwt_anas', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, biaya_bhn, td_register_ok_id, biaya_dr_op, biaya_dr_anas, biaya_prwt_ok, biaya_prwt_anas, created, dr_operator', 'safe', 'on'=>'search'),
        ); 
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

            'drOperator' => array(self::BELONGS_TO, 'DmDokter', 'dr_operator'),
            'tdRegisterOk' => array(self::BELONGS_TO, 'TdRegisterOk', 'td_register_ok_id'),
        ); 
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'biaya_bhn' => 'Biaya BHN',
			'biaya_jrs' => 'Biaya JRS',
			'td_register_ok_id' => 'Td Register Ok',
			'biaya_dr_op' => 'Biaya Dr OP',
			'biaya_dr_anas' => 'Biaya Dr AN',
			'biaya_prwt_ok' => 'Biaya Pwrt OP',
			'biaya_prwt_anas' => 'Biaya Prwt AN',
			'biaya_rr_monitor' => 'Biaya RR Monitor',
			'biaya_rr_askep' => 'Biaya RR Askep',
			'created' => 'Created',
			'dr_operator' => 'Dr Operator',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id_ok='')
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('biaya_bhn',$this->SEARCH,true,'OR');
		
		$criteria->addSearchCondition('biaya_dr_op',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('biaya_dr_anas',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('biaya_prwt_ok',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('biaya_prwt_anas',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('created',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('td_ok_tindakan_id',$this->SEARCH,true,'OR');
		if(!empty($id_ok))
			$criteria->compare('td_register_ok_id',$id_ok);

		if(!empty($this->UPF))
			$criteria->compare('tdRegisterOk.upf',$this->UPF);

		if(!empty($this->DOKTER))
			$criteria->compare('dr_operator',$this->DOKTER);
		
		$criteria->with = array('tdRegisterOk');
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function searchLaporan()
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tdRegisterOk.tgl_operasi', $this->TANGGAL_AWAL.' 00:00:00', $this->TANGGAL_AKHIR.' 23:59:59', 'AND');

		if(!empty($this->UPF))
		{
			$criteria->compare('tdRegisterOk.upf',$this->UPF);
		}

		if(!empty($this->DOKTER))
		{
			$criteria->compare('dr_operator',$this->DOKTER);
		}

		$criteria->with = array('tdRegisterOk');
		$criteria->order = 'tdRegisterOk.tgl_operasi ASC';
		$criteria->together = true;
		return TdOkBiaya::model()->findAll($criteria);
	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TdOkBiaya the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
