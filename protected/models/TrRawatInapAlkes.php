<?php

/**
 * This is the model class for table "tr_rawat_inap_alkes".
 *
 * The followings are the available columns in table 'tr_rawat_inap_alkes':
 * @property integer $id_ri_alkes
 * @property integer $id_alkes
 * @property double $biaya_ird
 * @property double $biaya_irna
 * @property integer $jumlah_tindakan
 * @property string $created
 * @property integer $id_rawat_inap
 *
 * The followings are the available model relations:
 * @property TrRawatInap $idRawatInap
 * @property ObatAlkes $idAlkes
 */
class TrRawatInapAlkes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_alkes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_alkes, biaya_ird, biaya_irna, jumlah_tindakan', 'required','message'=>'{attribute} harus diisi'),
			array('id_alkes, jumlah_tindakan, id_rawat_inap', 'numerical', 'integerOnly'=>true),
			array('biaya_ird, biaya_irna', 'numerical'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ri_alkes, id_alkes, biaya_ird, biaya_irna, jumlah_tindakan, created, id_rawat_inap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'id_rawat_inap'),
			'alkes' => array(self::BELONGS_TO, 'ObatAlkes', 'id_alkes'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ri_alkes' => 'Id Ri Alkes',
			'id_alkes' => 'Obat & Alkes',
			'biaya_ird' => 'Biaya Ird',
			'biaya_irna' => 'Biaya Irna',
			'jumlah_tindakan' => 'Jumlah Tindakan',
			'created' => 'Created',
			'id_rawat_inap' => 'Id Rawat Inap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ri_alkes',$this->id_ri_alkes);
		$criteria->compare('id_alkes',$this->id_alkes);
		$criteria->compare('biaya_ird',$this->biaya_ird);
		$criteria->compare('biaya_irna',$this->biaya_irna);
		$criteria->compare('jumlah_tindakan',$this->jumlah_tindakan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('id_rawat_inap',$this->id_rawat_inap);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function updateTotalObat($id_ri){
		$rawatInap = TrRawatInap::model()->findByPk($id_ri);
		$total = 0;
		foreach($rawatInap->searchAlkesObat($id_ri) as $obat)
		{
			$total += $obat->nilai;
		}

		$attr = [
			'id_rawat_inap' => $id_ri,
			'id_alkes' => 1
		];

		$rialkes = TrRawatInapAlkes::model()->findByAttributes($attr);
		if(empty($rialkes))
			$rialkes = new TrRawatInapAlkes;


		$rialkes->id_rawat_inap = $id_ri;
		$rialkes->id_alkes = 1;
		$rialkes->biaya_irna = $total;
		$rialkes->biaya_ird = 0;
		$rialkes->jumlah_tindakan = 1;
		$rialkes->save();
	} 

	public function getBiayaAlkes($id, $param)
	{
		$rawatInap = TrRawatInap::model()->findByPk($id);
		$listObat = ObatAlkes::model()->findAll();
		$total_ird = 0;
		$total_irna = 0;
		foreach($listObat as $obat){
			$attr = array(
			'id_rawat_inap' => $rawatInap->id_rawat_inap,
			'id_alkes' => $obat->id_obat_alkes
			);

			if($obat->kode_alkes != $param)
				continue;


			$itemObat = TrRawatInapAlkes::model()->findByAttributes($attr);

			
			$jumlah_tindakan = 1;
			$biaya_irna = $obat->tarip;
			$biaya_ird = 0;

			$biaya_obat_ird_total = 0;
			$biaya_obat_irna_total = 0;
			if(!empty($itemObat)){
				$jumlah_tindakan = $itemObat->jumlah_tindakan;
				$biaya_irna = $itemObat->biaya_irna;
				$biaya_ird = $itemObat->biaya_ird;
				$biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird;
				$biaya_obat_irna_total = $biaya_obat_irna_total + $biaya_irna * $jumlah_tindakan;
			}

			$total_ird = $total_ird + $biaya_obat_ird_total;
			$total_irna = $total_irna + $biaya_obat_irna_total;
		}

		return $total_ird + $total_irna;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapAlkes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
