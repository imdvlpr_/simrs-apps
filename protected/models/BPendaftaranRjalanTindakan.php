<?php

/**
 * This is the model class for table "b_pendaftaran_rjalan_tindakan".
 *
 * The followings are the available columns in table 'b_pendaftaran_rjalan_tindakan':
 * @property integer $id
 * @property integer $dm_poli_tindakan_id
 * @property double $b_pendaftaran_rjalan_id
 * @property string $tanggal
 * @property double $biaya
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property DmPoliTindakan $dmPoliTindakan
 * @property BPendaftaranRjalan $bPendaftaranRjalan
 */
class BPendaftaranRjalanTindakan extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $nama_dokter = '';


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b_pendaftaran_rjalan_tindakan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dm_poli_tindakan_id, b_pendaftaran_rjalan_id, tanggal, dokter_id', 'required'),
			array('dm_poli_tindakan_id', 'numerical', 'integerOnly'=>true),
			array('b_pendaftaran_rjalan_id, biaya', 'numerical'),
			array('keterangan', 'length', 'max'=>255),
			array('tanggal', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dm_poli_tindakan_id,durasi_label, b_pendaftaran_rjalan_id, tanggal, biaya, keterangan, created_at, updated_at, durasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'unitTindakan' => array(self::BELONGS_TO, 'UnitTindakan', 'dm_poli_tindakan_id'),
			'dokter' => array(self::BELONGS_TO, 'DmDokter', 'dokter_id'),
			'bPendaftaranRjalan' => array(self::BELONGS_TO, 'BPendaftaranRjalan', 'b_pendaftaran_rjalan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dm_poli_tindakan_id' => 'Tindakan',
			'b_pendaftaran_rjalan_id' => 'B Pendaftaran Rjalan',
			'tanggal' => 'Tanggal',
			'biaya' => 'Biaya',
			'keterangan' => 'Keterangan',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'dokter_id' => 'Dokter',
			'namaDokter' => 'Dokter',
			'durasi' => 'Durasi',
			'durasi_label' => 'Durasi'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;
		$criteria->compare('b_pendaftaran_rjalan_id',$this->b_pendaftaran_rjalan_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function getNamaTindakan(){
		return $this->dmTindakan->nama;
	}

	public function getNamaDokter(){
		return $this->dokter->FULLNAME;
	}

	protected function afterFind(){
		$this->nama_dokter = $this->getNamaDokter();
		$this->tanggal = date('d/m/Y', strtotime($this->tanggal));
		return parent::beforeSave();
	}

	protected function beforeSave(){
		$date = str_replace('/', '-', $this->tanggal);
		$this->tanggal = date('Y-m-d', strtotime($date));
		
		return parent::beforeSave();
	}

	public function getTotals($records){
		$total = 0;
		foreach($records as $item){
			$total += $item->biaya;
		}


		return $total;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BPendaftaranRjalanTindakan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
