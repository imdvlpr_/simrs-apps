<?php

/**
 * This is the model class for table "dm_kamar".
 *
 * The followings are the available columns in table 'dm_kamar':
 * @property integer $id_kamar
 * @property integer $kelas_id
 * @property string $nama_kamar
 * @property string $created
 *
 * The followings are the available model relations:
 * @property DmKelas $kelas
 * @property TrRawatInapKamar[] $trRawatInapKamars
 */
class DmKamar extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $biaya_total_kamar = 0;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_kamar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kelas_id, nama_kamar,biaya_kamar,biaya_makan,biaya_askep,biaya_asnut,tingkat_kamar,user_kamar,kamar_master_id', 'required'),
			array('kelas_id, tingkat_kamar, jumlah_kasur, terpakai, is_hapus', 'numerical', 'integerOnly'=>true),
			array('nama_kamar', 'length', 'max'=>255),
	
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_kamar, kelas_id, nama_kamar, tingkat_kamar,biaya_kamar,biaya_askep,biaya_asnut, jumlah_kasur, terpakai, created, user_kamar, kamar_master_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kelas' => array(self::BELONGS_TO, 'DmKelas', 'kelas_id'),
			'kamarMaster' => array(self::BELONGS_TO, 'DmKamarMaster', 'kamar_master_id'),
			'trRawatInapKamars' => array(self::HAS_MANY, 'TrRawatInapKamarHistory', 'kamar_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_kamar'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kamar' => 'Id Kamar',
			'kelas_id' => 'Kelas',
			'nama_kamar' => 'Nama Kamar',
			'tingkat_kamar' => 'Tingkat Kamar',
			'biaya_kamar' => 'Biaya Kamar',
			'biaya_makan' => 'Biaya Makan',
			'biaya_askep' => 'Biaya Askep',
			'biaya_asnut' => 'Asuhan Nutrisi',
			'jumlah_kasur' => 'Tempat Tidur',
			'terpakai' => 'Terpakai',
			'created' => 'Created',
			'user_kamar' => 'User Kamar',
			'is_hapus' => 'Hapus',
			'kamar_master_id' => 'Nama Kamar Induk'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$sort = new CSort();
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('nama_kamar',$this->SEARCH,true,'OR');
		$criteria->addCondition('is_hapus=0');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));


		// $criteria=new CDbCriteria;

		// $criteria->compare('id_kamar',$this->id_kamar);
		// $criteria->compare('kelas_id',$this->kelas_id);
		// $criteria->compare('nama_kamar',$this->nama_kamar,true);
		// $criteria->compare('created',$this->created,true);

		// return new CActiveDataProvider($this, array(
		// 	'criteria'=>$criteria,
		// ));
	}

	protected function afterFind(){
		$this->biaya_total_kamar = $this->biaya_kamar + $this->biaya_makan; 
		return parent::afterFind();
	}

	public function getFullName()
    {
    	return $this->kelas->nama_kelas. " - " . $this->nama_kamar;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmKamar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
