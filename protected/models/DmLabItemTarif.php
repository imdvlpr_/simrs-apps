<?php

/**
 * This is the model class for table "dm_lab_item_tarif".
 *
 * The followings are the available columns in table 'dm_lab_item_tarif':
 * @property integer $id
 * @property integer $item_id
 * @property integer $kelas_id
 * @property double $bhp
 * @property double $jrs
 * @property double $japel
 * @property double $jumlah
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_removed
 *
 * The followings are the available model relations:
 * @property DmLabItem $item
 * @property DmKelas $kelas
 */
class DmLabItemTarif extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_lab_item_tarif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, kelas_id', 'required'),
			array('item_id, kelas_id, is_removed', 'numerical', 'integerOnly'=>true),
			array('bhp, jrs, japel, jumlah', 'numerical'),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item_id, kelas_id, bhp, jrs, japel, jumlah, created_at, updated_at, is_removed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'item' => array(self::BELONGS_TO, 'DmLabItem', 'item_id'),
			'kelas' => array(self::BELONGS_TO, 'DmKelas', 'kelas_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_id' => 'Item',
			'kelas_id' => 'Kelas',
			'bhp' => 'Bhp',
			'jrs' => 'Jrs',
			'japel' => 'Japel',
			'jumlah' => 'Jumlah',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'is_removed' => 'Is Removed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('item.nama',$this->SEARCH,true,'OR');
		$criteria->addCondition('item.is_removed = 0');
		$criteria->with = ['item'];
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmLabItemTarif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
