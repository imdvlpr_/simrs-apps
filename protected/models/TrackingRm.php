<?php

/**
 * This is the model class for table "tr_tracking_rm".
 *
 * The followings are the available columns in table 'tr_tracking_rm':
 * @property integer $id
 * @property integer $prev_id
 * @property double $b_pendaftaran_id
 * @property integer $status_rm_id
 * @property integer $unit_id
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property ARefstatusrm $statusRm
 * @property BPendaftaran $bPendaftaran
 */
class TrackingRm extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $no_medrec;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_tracking_rm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('b_pendaftaran_id, status_rm_id, unit_id', 'required'),
			array('prev_id, status_rm_id, unit_id', 'numerical', 'integerOnly'=>true),
			array('b_pendaftaran_id', 'numerical'),
			array('keterangan', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// array('no_medrec','cekKunjunganPoli','on'=>'insert'),
			// array('no_medrec','cekSudahMasuk','on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, prev_id, b_pendaftaran_id, status_rm_id, unit_id, keterangan, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prev' => array(self::BELONGS_TO, 'TrackingRm', 'prev_id'),
			'unit' => array(self::BELONGS_TO, 'Unit', 'unit_id'),
			'statusRm' => array(self::BELONGS_TO, 'Refstatusrm', 'status_rm_id'),
			'bPendaftaran' => array(self::BELONGS_TO, 'BPendaftaran', 'b_pendaftaran_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'prev_id' => 'Prev',
			'b_pendaftaran_id' => 'B Pendaftaran',
			'status_rm_id' => 'Status Rm',
			'unit_id' => 'Unit',
			'keterangan' => 'Keterangan',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('prev_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('b_pendaftaran_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NAMA',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('unit.NamaUnit',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('keterangan',$this->SEARCH,true,'OR');
		$criteria->with = ['unit','bPendaftaran.noMedrec'];
		$criteria->together = true;


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}


	public function getNamaStatusRM(){
		return $this->statusRm->NamaStatusRM;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrackingRm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
