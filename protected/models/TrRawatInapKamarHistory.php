<?php

/**
 * This is the model class for table "tr_rawat_inap_kamar".
 *
 * The followings are the available columns in table 'tr_rawat_inap_kamar':
 * @property integer $id_ri_kamar
 * @property integer $kamar_id
 * @property integer $tr_ri_id
 * @property string $created
 *
 * The followings are the available model relations:
 * @property TrRawatInap $trRi
 * @property DmKamar $kamar
 */
class TrRawatInapKamarHistory  extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_kamar_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('tr_ri_id', 'numerical', 'integerOnly'=>true),
			array('nilai', 'numerical'),
			array('keterangan', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ri_kamar, nilai, keterangan, tr_ri_id, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trRi' => array(self::BELONGS_TO, 'TrRawatInap', 'tr_ri_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ri_kamar' => 'Id Ri Kamar',
			'tr_ri_id' => 'Tr Ri',
			'nilai' => 'Nilai',
			'keterangan' => 'Keterangan',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ri_kamar',$this->id_ri_kamar);
		$criteria->compare('tr_ri_id',$this->tr_ri_id);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getBiayaKamar($id)
	{

		$rawatInap = TrRawatInap::model()->findByPk($id);
		$rawatRincian = $rawatInap->trRawatInapRincians;
		
		 $selisih_hari = 1;

		if(!empty($rawatInap->tanggal_keluar))
		{
		  $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
		}
		else
		{
		  $dnow = date('Y-m-d');
		    $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
		}
		$total_ird = 0;
        $total_irna = 0;

        // $total_ird = $rawatRincian->obs_ird;
         $isneonatus = $rawatInap->kamar->nama_kamar == 'NEONATUS';
        if($isneonatus)
        {


        $biaya_neonatus = 0;

          $attr = array(
              'tr_ri_id' => $rawatInap->id_rawat_inap,

            );
          $model_kamar = TrRawatInapKamarHistory::model()->findAllByAttributes($attr);


         foreach($model_kamar as $item)
         {

            $biaya_neonatus = $biaya_neonatus + $item->nilai;
          }
          
          $total_irna = $total_irna + $biaya_neonatus;
        }

        else
        {

          if($rawatInap->kamar->kelas->kode_kelas != 'IRD')
          {

          
            $total_irna = $total_irna + $rawatInap->kamar->biaya_kamar * $selisih_hari;   
          }

        }

        return $total_ird + $total_irna;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapKamar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
