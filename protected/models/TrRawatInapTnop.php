<?php

/**
 * This is the model class for table "tr_rawat_inap_tnop".
 *
 * The followings are the available columns in table 'tr_rawat_inap_tnop':
 * @property integer $id_ri_tnop
 * @property integer $id_tnop
 * @property double $biaya
 * @property string $created
 * @property integer $id_rawat_inap
 *
 * The followings are the available model relations:
 * @property TindakanMedisNonOperatif $idTnop
 * @property TrRawatInap $idRawatInap
 */
class TrRawatInapTnop extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_tnop';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tnop, biaya_irna,biaya_ird,jumlah_tindakan',  'required','message'=>'{attribute} harus diisi'),
			array('id_tnop, id_rawat_inap', 'numerical', 'integerOnly'=>true),
			array('biaya_ird, biaya_irna', 'numerical'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ri_tnop, id_tnop, biaya_irna, biaya_ird, jumlah_tindakan, created, id_rawat_inap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tnop' => array(self::BELONGS_TO, 'TindakanMedisNonOperatif', 'id_tnop'),
			'rawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'id_rawat_inap'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ri_tnop' => 'Id Ri Tnop',
			'id_tnop' => 'Jenis Tindakan Non-Operatif',
			'biaya_irna' => 'Biaya Tindakan',
			'biaya_ird' => 'Biaya IRD',
			'jumlah_tindakan' => 'Jumlah Tindakan',
			'created' => 'Created',
			'id_rawat_inap' => 'Id Rawat Inap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ri_tnop',$this->id_ri_tnop);
		$criteria->compare('id_tnop',$this->id_tnop);
		$criteria->compare('biaya',$this->biaya);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('id_rawat_inap',$this->id_rawat_inap);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapTnop the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
