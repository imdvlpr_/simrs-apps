<?php

/**
 * This is the model class for table "dm_kelompok_khusus".
 *
 * The followings are the available columns in table 'dm_kelompok_khusus':
 * @property integer $id_tindakan_khusus
 * @property string $kode_kelompok
 * @property string $nama_kelompok
 *
 * The followings are the available model relations:
 * @property TindakanMedisOperatifKhusus[] $tindakanMedisOperatifKhususes
 */
class DmKelompokKhusus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_kelompok_khusus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_kelompok, nama_kelompok', 'required'),
			array('kode_kelompok', 'length', 'max'=>5),
			array('nama_kelompok', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tindakan_khusus, kode_kelompok, nama_kelompok', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tindakanMedisOperatifKhususes' => array(self::HAS_MANY, 'TindakanMedisOperatifKhusus', 'jenis_tindakan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tindakan_khusus' => 'Id Tindakan Khusus',
			'kode_kelompok' => 'Kode Kelompok',
			'nama_kelompok' => 'Nama Kelompok',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tindakan_khusus',$this->id_tindakan_khusus);
		$criteria->compare('kode_kelompok',$this->kode_kelompok,true);
		$criteria->compare('nama_kelompok',$this->nama_kelompok,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmKelompokKhusus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
