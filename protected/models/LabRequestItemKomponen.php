<?php

/**
 * This is the model class for table "lab_request_item_komponen".
 *
 * The followings are the available columns in table 'lab_request_item_komponen':
 * @property integer $id
 * @property integer $lab_request_item_id
 * @property integer $item_id
 * @property string $hasil
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property LabRequestItem $labRequestItem
 * @property DmLabItemParent $item
 */
class LabRequestItemKomponen extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lab_request_item_komponen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lab_request_item_id, item_id', 'required'),
			array('lab_request_item_id, item_id', 'numerical', 'integerOnly'=>true),
			array('hasil', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lab_request_item_id, item_id, hasil, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'labRequestItem' => array(self::BELONGS_TO, 'LabRequestItem', 'lab_request_item_id'),
			'item' => array(self::BELONGS_TO, 'DmLabItemParent', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lab_request_item_id' => 'Lab Request Item',
			'item_id' => 'Item',
			'hasil' => 'Hasil',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('lab_request_item_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('item_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('hasil',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LabRequestItemKomponen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
