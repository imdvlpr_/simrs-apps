<?php

/**
 * This is the model class for table "erp_obat_detil".
 *
 * The followings are the available columns in table 'erp_obat_detil':
 * @property integer $id
 * @property integer $barang_id
 * @property string $nama_generik
 * @property string $kekuatan
 * @property string $satuan_kekuatan
 * @property string $jns_sediaan
 * @property string $b_i_r
 * @property string $gen_non
 * @property string $nar_p_non
 * @property string $oakrl
 * @property string $kronis
 *
 * The followings are the available model relations:
 * @property ErpSalesMasterBarang $barang
 */
class ErpObatDetil extends MyActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public function getDbConnection()
    {
        return self::getErpDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'erp_obat_detil';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barang_id', 'required'),
			array('barang_id', 'numerical', 'integerOnly'=>true),
			array('nama_generik', 'length', 'max'=>255),
			array('kekuatan, satuan_kekuatan, jns_sediaan, b_i_r, gen_non', 'length', 'max'=>75),
			array('nar_p_non, oakrl', 'length', 'max'=>50),
			array('kronis', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, barang_id, nama_generik, kekuatan, satuan_kekuatan, jns_sediaan, b_i_r, gen_non, nar_p_non, oakrl, kronis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'barang' => array(self::BELONGS_TO, 'ErpSalesMasterBarang', 'barang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'barang_id' => 'Barang',
			'nama_generik' => 'Nama Generik',
			'kekuatan' => 'Kekuatan',
			'satuan_kekuatan' => 'Satuan Kekuatan',
			'jns_sediaan' => 'Jns Sediaan',
			'b_i_r' => 'B I R',
			'gen_non' => 'Gen Non',
			'nar_p_non' => 'Nar P Non',
			'oakrl' => 'Oakrl',
			'kronis' => 'Kronis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('barang_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama_generik',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kekuatan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('satuan_kekuatan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jns_sediaan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('b_i_r',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('gen_non',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nar_p_non',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('oakrl',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kronis',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ErpObatDetil the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
