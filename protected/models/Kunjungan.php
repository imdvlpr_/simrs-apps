<?php

/**
 * This is the model class for table "kunjungan".
 *
 * The followings are the available columns in table 'kunjungan':
 * @property integer $NO_KUNJUNGAN
 * @property string $no_daftar
 * @property string $NO_RM
 * @property string $no_rm_lama
 * @property string $nama
 * @property string $tgl_lahir
 * @property string $ALAMAT
 * @property string $GOL_PASIEN
 * @property string $TGL_DAFTAR
 * @property string $TGL_KRS
 * @property string $T1
 * @property string $T2
 * @property string $T3
 * @property string $tgl_kunj_sebelumnya
 * @property string $post_mrs
 * @property string $BARU_LAMA
 * @property string $status_timur
 * @property string $status_barat
 * @property integer $id
 */
class Kunjungan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kunjungan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_daftar, no_rm_lama, nama, tgl_lahir, ALAMAT, GOL_PASIEN, tgl_kunj_sebelumnya, post_mrs, status_timur, status_barat', 'required'),
			array('NO_KUNJUNGAN', 'numerical', 'integerOnly'=>true),
			array('no_daftar, GOL_PASIEN', 'length', 'max'=>100),
			array('NO_RM, BARU_LAMA, status_timur, status_barat', 'length', 'max'=>20),
			array('no_rm_lama', 'length', 'max'=>50),
			array('nama, post_mrs', 'length', 'max'=>150),
			array('ALAMAT, T1, T2, T3', 'length', 'max'=>200),
			array('TGL_DAFTAR, TGL_KRS', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NO_KUNJUNGAN, no_daftar, NO_RM, no_rm_lama, nama, tgl_lahir, ALAMAT, GOL_PASIEN, TGL_DAFTAR, TGL_KRS, T1, T2, T3, tgl_kunj_sebelumnya, post_mrs, BARU_LAMA, status_timur, status_barat, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NO_KUNJUNGAN' => 'No Kunjungan',
			'no_daftar' => 'No Daftar',
			'NO_RM' => 'No Rm',
			'no_rm_lama' => 'No Rm Lama',
			'nama' => 'Nama',
			'tgl_lahir' => 'Tgl Lahir',
			'ALAMAT' => 'Alamat',
			'GOL_PASIEN' => 'Gol Pasien',
			'TGL_DAFTAR' => 'Tgl Daftar',
			'TGL_KRS' => 'Tgl Krs',
			'T1' => 'T1',
			'T2' => 'T2',
			'T3' => 'T3',
			'tgl_kunj_sebelumnya' => 'Tgl Kunj Sebelumnya',
			'post_mrs' => 'Post Mrs',
			'BARU_LAMA' => 'Baru Lama',
			'status_timur' => 'Status Timur',
			'status_barat' => 'Status Barat',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('NO_KUNJUNGAN',$this->NO_KUNJUNGAN);
		$criteria->compare('no_daftar',$this->no_daftar,true);
		$criteria->compare('NO_RM',$this->NO_RM,true);
		$criteria->compare('no_rm_lama',$this->no_rm_lama,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);
		$criteria->compare('ALAMAT',$this->ALAMAT,true);
		$criteria->compare('GOL_PASIEN',$this->GOL_PASIEN,true);
		$criteria->compare('TGL_DAFTAR',$this->TGL_DAFTAR,true);
		$criteria->compare('TGL_KRS',$this->TGL_KRS,true);
		$criteria->compare('T1',$this->T1,true);
		$criteria->compare('T2',$this->T2,true);
		$criteria->compare('T3',$this->T3,true);
		$criteria->compare('tgl_kunj_sebelumnya',$this->tgl_kunj_sebelumnya,true);
		$criteria->compare('post_mrs',$this->post_mrs,true);
		$criteria->compare('BARU_LAMA',$this->BARU_LAMA,true);
		$criteria->compare('status_timur',$this->status_timur,true);
		$criteria->compare('status_barat',$this->status_barat,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getKunjunganSebelumnya($rm)
	{
		$model = Kunjungan::model()->findAllByAttributes(array(
			'NO_RM'=>$rm),array(
			'order' => 'NO_KUNJUNGAN DESC',
			'limit' => 2
		));

		$data = array();

		foreach($model as $m){
			$data[] = $m;
		}

		$hasil = null;

		$size = count($data);
		switch($size){
			case 1 :
				$hasil = $data[0];
				break;
			case 2 :
				$hasil = $data[1];
				break;
		}

		return $hasil;

	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kunjungan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
