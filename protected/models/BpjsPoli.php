<?php

/**
 * This is the model class for table "bpjs_poli".
 *
 * The followings are the available columns in table 'bpjs_poli':
 * @property string $KODE_POLI
 * @property string $NAMA_POLI
 * @property string $CREATED
 * @property integer $AKTIF
 *
 * The followings are the available model relations:
 * @property BpjsSep[] $bpjsSeps
 */
class BpjsPoli extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs_poli';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODE_POLI, NAMA_POLI, CREATED', 'required'),
			array('AKTIF', 'numerical', 'integerOnly'=>true),
			array('KODE_POLI', 'length', 'max'=>3),
			array('NAMA_POLI', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KODE_POLI, NAMA_POLI, CREATED, AKTIF', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bpjsSeps' => array(self::HAS_MANY, 'BpjsSep', 'POLI_TUJUAN'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KODE_POLI' => 'Kode Poli',
			'NAMA_POLI' => 'Nama Poli',
			'CREATED' => 'Created',
			'AKTIF' => 'Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KODE_POLI',$this->KODE_POLI,true);
		$criteria->compare('NAMA_POLI',$this->NAMA_POLI,true);
		$criteria->compare('CREATED',$this->CREATED,true);
		$criteria->compare('AKTIF',$this->AKTIF);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchPoli($q)
	{
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('NAMA_POLI',$q,true,'OR');
		$criteria->addSearchCondition('KODE_POLI',$q,true,'OR');
		$criteria->limit = 20;
		
		return BpjsPoli::model()->findAll($criteria);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BpjsPoli the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
