<?php

/**
 * This is the model class for table "tr_rawat_inap_visite_dokter".
 *
 * The followings are the available columns in table 'tr_rawat_inap_visite_dokter':
 * @property integer $id_ri_visite
 * @property integer $id_rawat_inap
 * @property integer $id_dokter
 * @property integer $jumlah_visite
 * @property integer $id_jenis_visite
 * @property string $created
 *
 * The followings are the available model relations:
 * @property DmDokter $idDokter
 * @property JenisVisite $idJenisVisite
 */
class TrRawatInapVisiteDokter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_visite_dokter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jumlah_visite, id_jenis_visite', 'required','message'=>'{attribute} harus diisi'),
			array('id_rawat_inap, id_dokter, jumlah_visite, jumlah_visite_ird, id_jenis_visite', 'numerical', 'integerOnly'=>true),
			
			array('biaya_visite, biaya_visite_irna', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ri_visite, id_rawat_inap, id_dokter, jumlah_visite_ird, jumlah_visite, biaya_visite, biaya_visite_irna, id_jenis_visite, id_dokter_irna, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dokterIrd' => array(self::BELONGS_TO, 'DmDokter', 'id_dokter'),
			'dokterIrna' => array(self::BELONGS_TO, 'DmDokter', 'id_dokter_irna'),
			'jenisVisite' => array(self::BELONGS_TO, 'JenisVisite', 'id_jenis_visite'),
			'rawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'id_rawat_inap'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ri_visite' => 'Id Ri Visite',
			'id_rawat_inap' => 'Id Rawat Inap',
			'id_dokter' => 'Dokter Visite',
			'jumlah_visite' => 'Jumlah Visite',
			'id_jenis_visite' => 'Jenis Visite',
			'biaya_visite' => 'Biaya Visite',
			'jumlah_visite_ird' => 'Jumlah Visite IRD',
			'id_dokter_irna' => 'Dokter Rawat Inap',
			'biaya_visite_irna' => 'Biaya Dokter Rawat Inap',
			'created' => 'Created',
		);
	}

	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ri_visite',$this->id_ri_visite);
		$criteria->compare('id_rawat_inap',$this->id_rawat_inap);
		$criteria->compare('id_dokter',$this->id_dokter);
		$criteria->compare('jumlah_visite',$this->jumlah_visite);
		$criteria->compare('id_jenis_visite',$this->id_jenis_visite);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapVisiteDokter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
