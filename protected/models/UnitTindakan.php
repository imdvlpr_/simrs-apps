<?php

/**
 * This is the model class for table "a_unit_tindakan".
 *
 * The followings are the available columns in table 'a_unit_tindakan':
 * @property integer $id
 * @property integer $unit_id
 * @property integer $dm_tindakan_id
 * @property double $akhp
 * @property double $jrs
 * @property double $jaspel
 * @property double $nilai
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property AUnit $unit
 * @property DmTindakan $dmTindakan
 * @property TrRawatJalanTindakan[] $trRawatJalanTindakans
 */
class UnitTindakan extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'a_unit_tindakan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unit_id, dm_tindakan_id', 'required'),
			array('unit_id, dm_tindakan_id', 'numerical', 'integerOnly'=>true),
			array('akhp, jrs, jaspel, nilai', 'numerical'),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, unit_id, dm_tindakan_id, akhp, jrs, jaspel, nilai, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'unit' => array(self::BELONGS_TO, 'Unit', 'unit_id'),
			'dmTindakan' => array(self::BELONGS_TO, 'DmTindakan', 'dm_tindakan_id'),
			'trRawatJalanTindakans' => array(self::HAS_MANY, 'TrRawatJalanTindakan', 'dm_tindakan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'unit_id' => 'Unit',
			'dm_tindakan_id' => 'Tindakan',
			'akhp' => 'BHP',
			'jrs' => 'JRS',
			'jaspel' => 'Jaspel',
			'nilai' => 'Tarif',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('unit_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dm_tindakan_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('akhp',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jrs',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jaspel',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nilai',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public static function searchTindakanUnit($unit_id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition = 'unit_id = '.$unit_id;
		$criteria->with = ['dmTindakan'];
		$criteria->together = true;
		return UnitTindakan::model()->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UnitTindakan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
