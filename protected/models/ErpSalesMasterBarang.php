<?php

/**
 * This is the model class for table "erp_sales_master_barang".
 *
 * The followings are the available columns in table 'erp_sales_master_barang':
 * @property integer $id_barang
 * @property string $kode_barang
 * @property string $nama_barang
 * @property double $harga_beli
 * @property double $harga_jual
 * @property string $id_satuan
 * @property string $created
 * @property integer $id_perusahaan
 * @property integer $is_hapus
 * @property integer $perkiraan_id
 *
 * The followings are the available model relations:
 * @property ErpBarangDatang[] $erpBarangDatangs
 * @property ErpBarangHarga[] $erpBarangHargas
 * @property ErpBarangLoss[] $erpBarangLosses
 * @property ErpBarangRekap[] $erpBarangRekaps
 * @property ErpBarangStok[] $erpBarangStoks
 * @property ErpBarangStokOpname[] $erpBarangStokOpnames
 * @property ErpBbmDispenser[] $erpBbmDispensers
 * @property ErpBbmDispenserLog[] $erpBbmDispenserLogs
 * @property ErpBbmFakturItem[] $erpBbmFakturItems
 * @property ErpBbmJual[] $erpBbmJuals
 * @property ErpDepartemenStok[] $erpDepartemenStoks
 * @property ErpKartuStok[] $erpKartuStoks
 * @property ErpObatDetil[] $erpObatDetils
 * @property ErpPenjualan[] $erpPenjualans
 * @property ErpPiutang[] $erpPiutangs
 * @property ErpRequestOrderItem[] $erpRequestOrderItems
 * @property ErpReturItem[] $erpReturItems
 * @property ErpSalesFakturBarang[] $erpSalesFakturBarangs
 * @property ErpPerkiraan $perkiraan
 * @property ErpPerusahaan $idPerusahaan
 * @property ErpSalesStokGudang[] $erpSalesStokGudangs
 * @property ErpStokAwal[] $erpStokAwals
 */
class ErpSalesMasterBarang extends MyActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public function getDbConnection()
    {
        return self::getErpDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'erp_sales_master_barang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_barang, created, id_perusahaan', 'required'),
			array('id_perusahaan, is_hapus, perkiraan_id', 'numerical', 'integerOnly'=>true),
			array('harga_beli, harga_jual', 'numerical'),
			array('kode_barang', 'length', 'max'=>20),
			array('nama_barang', 'length', 'max'=>255),
			array('id_satuan', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_barang, kode_barang, nama_barang, harga_beli, harga_jual, id_satuan, created, id_perusahaan, is_hapus, perkiraan_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'erpBarangDatangs' => array(self::HAS_MANY, 'ErpBarangDatang', 'barang_id'),
			'erpBarangHargas' => array(self::HAS_MANY, 'ErpBarangHarga', 'barang_id'),
			'erpBarangLosses' => array(self::HAS_MANY, 'ErpBarangLoss', 'barang_id'),
			'erpBarangRekaps' => array(self::HAS_MANY, 'ErpBarangRekap', 'barang_id'),
			'erpBarangStoks' => array(self::HAS_MANY, 'ErpBarangStok', 'barang_id'),
			'erpBarangStokOpnames' => array(self::HAS_MANY, 'ErpBarangStokOpname', 'barang_id'),
			'erpBbmDispensers' => array(self::HAS_MANY, 'ErpBbmDispenser', 'barang_id'),
			'erpBbmDispenserLogs' => array(self::HAS_MANY, 'ErpBbmDispenserLog', 'barang_id'),
			'erpBbmFakturItems' => array(self::HAS_MANY, 'ErpBbmFakturItem', 'barang_id'),
			'erpBbmJuals' => array(self::HAS_MANY, 'ErpBbmJual', 'barang_id'),
			'erpDepartemenStoks' => array(self::HAS_MANY, 'ErpDepartemenStok', 'barang_id'),
			'erpKartuStoks' => array(self::HAS_MANY, 'ErpKartuStok', 'barang_id'),
			'erpObatDetils' => array(self::HAS_MANY, 'ErpObatDetil', 'barang_id'),
			'erpPenjualans' => array(self::HAS_MANY, 'ErpPenjualan', 'barang_id'),
			'erpPiutangs' => array(self::HAS_MANY, 'ErpPiutang', 'barang_id'),
			'erpRequestOrderItems' => array(self::HAS_MANY, 'ErpRequestOrderItem', 'item_id'),
			'erpReturItems' => array(self::HAS_MANY, 'ErpReturItem', 'barang_id'),
			'erpSalesFakturBarangs' => array(self::HAS_MANY, 'ErpSalesFakturBarang', 'id_barang'),
			'perkiraan' => array(self::BELONGS_TO, 'ErpPerkiraan', 'perkiraan_id'),
			'idPerusahaan' => array(self::BELONGS_TO, 'ErpPerusahaan', 'id_perusahaan'),
			'erpSalesStokGudangs' => array(self::HAS_MANY, 'ErpSalesStokGudang', 'id_barang'),
			'erpStokAwals' => array(self::HAS_MANY, 'ErpStokAwal', 'barang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_barang' => 'Id Barang',
			'kode_barang' => 'Kode Barang',
			'nama_barang' => 'Nama Barang',
			'harga_beli' => 'Harga Beli',
			'harga_jual' => 'Harga Jual',
			'id_satuan' => 'Id Satuan',
			'created' => 'Created',
			'id_perusahaan' => 'Id Perusahaan',
			'is_hapus' => 'Is Hapus',
			'perkiraan_id' => 'Perkiraan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id_barang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kode_barang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama_barang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('harga_beli',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('harga_jual',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('id_satuan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('id_perusahaan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_hapus',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('perkiraan_id',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function searchObatBy($q)
	{
		$criteria=new CDbCriteria;
		// $criteria->addSearchCondition('kd_barang',$q,true,'OR');
		$criteria->addSearchCondition('nama_barang',$q,true,'OR');
		// $criteria->addSearchCondition('nama_generik',$q,true,'OR');
		$criteria->compare('id_perusahaan',5);
		$criteria->order = 'nama_barang';
		// print_r(expression)
		
		$criteria->limit = 20;
		

		$data = ErpSalesMasterBarang::model()->findAll($criteria);
		
		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ErpSalesMasterBarang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
