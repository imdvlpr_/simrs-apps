<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $USERNAME
 * @property string $PASSWORD
 * @property integer $LEVEL
 * @property integer $STATUS
 * @property string $PEGAWAI_ID
 *
 * The followings are the available model relations:
 * @property Pegawai $pEGAWAI
 */
class User extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	private $_identity;
	public $rememberMe;
	
	public $old_password;
    public $new_password;
    public $repeat_password;

    public $STATUS_LABEL = '';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('USERNAME, PASSWORD, LEVEL, PEGAWAI_ID', 'required'),
			array('LEVEL, STATUS', 'numerical', 'integerOnly'=>true),
			array('USERNAME, PASSWORD', 'length', 'max'=>100),
			array('PEGAWAI_ID', 'length', 'max'=>20),
			array('repeat_password', 'compare', 'compareAttribute'=>'PASSWORD', 'on'=>'repeatPwd','message' => Yii::t('yii', '{attribute} tidak sama dengan PASSWORD')),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('USERNAME, PASSWORD, LEVEL, PEGAWAI_ID', 'safe', 'on'=>'search'),
		);
	}

	//matching the old password with your existing password.
    public function findPasswords($attribute, $params)
    {
		$username = Yii::app()->user->getState('username');
        $user = User::model()->findByPk($username);
        if ($user->PASSWORD != md5($this->old_password))
            $this->addError($attribute, 'Old password is incorrect.');
    }


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lEVEL' => array(self::BELONGS_TO, 'UserLevel', 'LEVEL'),
			'pEGAWAI' => array(self::BELONGS_TO, 'Pegawai', 'PEGAWAI_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
			'USERNAME' => 'Username',
			'PASSWORD' => 'Password',
			'LEVEL' => 'Level',
			'STATUS' => 'Status',
			'PEGAWAI_ID' => 'Pegawai',
			'repeat_password' => 'Ulangi Password'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$sort = new CSort();
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('USERNAME',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('pEGAWAI.NAMA',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('pEGAWAI.ALAMAT',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('pEGAWAI.EMAIL',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('lEVEL.NAMA_LEVEL',$this->SEARCH,true,'OR');

		if(!empty($this->LEVEL))
			$criteria->compare('LEVEL',$this->LEVEL);
	

		$criteria->with = array('pEGAWAI','lEVEL');
		
		
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->USERNAME,md5($this->PASSWORD));
			if(!$this->_identity->authenticate())
				$this->addError('PASSWORD','Username atau password salah');
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->USERNAME,md5($this->PASSWORD));
			$this->_identity->authenticate();
		}



		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			
			return UserIdentity::ERROR_NONE;
		}
		else
			return $this->_identity->errorCode;
	}

	protected function beforeSave()
	{
		$this->PASSWORD = md5($this->PASSWORD);

		return parent::beforeSave();
	}

	protected function afterFind()
	{
		$this->STATUS_LABEL = ($this->STATUS == 1) ? '<span class="label label-success">AKTIF</span>' : '<span class="label label-important">TIDAK AKTIF</span>';
		return parent::afterFind();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
