<?php

/**
 * This is the model class for table "bpjs_pasien".
 *
 * The followings are the available columns in table 'bpjs_pasien':
 * @property string $NoKartu
 * @property string $NIK
 * @property string $NAMAPESERTA
 * @property string $ALAMAT
 * @property string $KELASRAWAT
 * @property string $TMPLAHIR
 * @property string $TGLLAHIR
 * @property string $PPKTK1
 * @property string $PESERTA
 * @property string $KELAMIN
 * @property string $PISAT
 * @property string $TGLCETAKKARTU
 * @property string $TMT
 * @property string $TAT
 * @property integer $PASIEN_ID
 * @property string $created
 */
class BpjsPasien extends CActiveRecord
{



	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NoKartu, NAMAPESERTA,TGLLAHIR', 'required'),
			array('PASIEN_ID', 'numerical', 'integerOnly'=>true),
			array('NoKartu, KELASRAWAT, PESERTA', 'length', 'max'=>50),
			array('NIK, NAMAPESERTA, PPKTK1', 'length', 'max'=>100),
			array('ALAMAT', 'length', 'max'=>80),
			array('TMPLAHIR', 'length', 'max'=>15),
			array('KELAMIN', 'length', 'max'=>1),
			array('PISAT', 'length', 'max'=>10),
			array('TGLLAHIR, TGLCETAKKARTU, TMT, TAT', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NoKartu, NIK, NAMAPESERTA, ALAMAT, KELASRAWAT, TMPLAHIR, TGLLAHIR, PPKTK1, PESERTA, KELAMIN, PISAT, TGLCETAKKARTU, TMT, TAT, PASIEN_ID,NAMA_KEPESERTAAN, NAMA_KELASRAWAT,NAMA_PISAT, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pASIEN' => array(self::BELONGS_TO, 'Pasien', 'PASIEN_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NoKartu' => 'No Kartu',
			'NIK' => 'NIK',
			'NAMAPESERTA' => 'Nama Peserta',
			'ALAMAT' => 'Alamat',
			'KELASRAWAT' => 'Kelas Rawat',
			'NAMA_KELASRAWAT' => 'Kelas Rawat',
			'TMPLAHIR' => 'Tempat Lahir',
			'TGLLAHIR' => 'Tgl Lahir',
			'PPKTK1' => 'PPK TK I',
			'PESERTA' => 'Peserta',
			'KELAMIN' => 'Jenis Kelamin',
			'PISAT' => 'PISAT',
			'TGLCETAKKARTU' => 'Tgl Cetak Kartu',
			'TMT' => 'TMT',
			'TAT' => 'TAT',
			'PASIEN_ID' => 'Pasien',
			'NAMA_KEPESERTAAN' => 'NAMA KEPESERTAAN',
			'NAMA_PISAT' => 'PISAT',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('NoKartu',$this->NoKartu,true);
		$criteria->compare('NIK',$this->NIK,true);
		$criteria->compare('NAMAPESERTA',$this->NAMAPESERTA,true);
		$criteria->compare('ALAMAT',$this->ALAMAT,true);
		$criteria->compare('KELASRAWAT',$this->KELASRAWAT,true);
		$criteria->compare('TMPLAHIR',$this->TMPLAHIR,true);
		$criteria->compare('TGLLAHIR',$this->TGLLAHIR,true);
		$criteria->compare('PPKTK1',$this->PPKTK1,true);
		$criteria->compare('PESERTA',$this->PESERTA,true);
		$criteria->compare('KELAMIN',$this->KELAMIN,true);
		$criteria->compare('PISAT',$this->PISAT,true);
		$criteria->compare('TGLCETAKKARTU',$this->TGLCETAKKARTU,true);
		$criteria->compare('TMT',$this->TMT,true);
		$criteria->compare('TAT',$this->TAT,true);
		$criteria->compare('PASIEN_ID',$this->PASIEN_ID);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BpjsPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
