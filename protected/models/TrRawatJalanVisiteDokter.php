<?php

/**
 * This is the model class for table "tr_rawat_jalan_visite_dokter".
 *
 * The followings are the available columns in table 'tr_rawat_jalan_visite_dokter':
 * @property integer $id_rj_visite
 * @property integer $id_rawat_jalan
 * @property integer $id_dokter
 * @property double $biaya_visite
 * @property integer $jumlah_visite
 * @property integer $jumlah_visite_ird
 * @property integer $id_jenis_visite
 * @property string $created
 *
 * The followings are the available model relations:
 * @property JenisVisite $idJenisVisite
 * @property TrPendaftaranRjalan $idRawatJalan
 * @property DmDokter $idDokter
 */
class TrRawatJalanVisiteDokter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_jalan_visite_dokter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_rawat_jalan, biaya_visite, id_jenis_visite', 'required'),
			array('id_rawat_jalan, id_dokter, jumlah_visite_ird, id_jenis_visite', 'numerical', 'integerOnly'=>true),
			array('biaya_visite', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_rj_visite, id_rawat_jalan, id_dokter, biaya_visite, jumlah_visite_ird, id_jenis_visite, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisVisite' => array(self::BELONGS_TO, 'JenisVisite', 'id_jenis_visite'),
			'rawatJalan' => array(self::BELONGS_TO, 'TrPendaftaranRjalan', 'id_rawat_jalan'),
			'dokter' => array(self::BELONGS_TO, 'DmDokter', 'id_dokter'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_rj_visite' => 'Id Rj Visite',
			'id_rawat_jalan' => 'Id Rawat Jalan',
			'id_dokter' => 'Id Dokter',
			'biaya_visite' => 'Biaya Visite',
			'jumlah_visite_ird' => 'Jumlah Visite Ird',
			'id_jenis_visite' => 'Id Jenis Visite',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_rj_visite',$this->id_rj_visite);
		$criteria->compare('id_rawat_jalan',$this->id_rawat_jalan);
		$criteria->compare('id_dokter',$this->id_dokter);
		$criteria->compare('biaya_visite',$this->biaya_visite);
		$criteria->compare('jumlah_visite',$this->jumlah_visite);
		$criteria->compare('jumlah_visite_ird',$this->jumlah_visite_ird);
		$criteria->compare('id_jenis_visite',$this->id_jenis_visite);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatJalanVisiteDokter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
