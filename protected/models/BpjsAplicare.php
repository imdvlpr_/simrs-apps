<?php

/**
 * This is the model class for table "bpjs_aplicare".
 *
 * The followings are the available columns in table 'bpjs_aplicare':
 * @property string $koderuang
 * @property string $namaruang
 * @property string $kodekelas
 * @property integer $kapasitas
 * @property integer $tersedia
 * @property integer $tersediapria
 * @property integer $tersediawanita
 * @property integer $tersediapriawanita
 * @property string $created
 * @property integer $kode_kamar
 */
class BpjsAplicare extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs_aplicare';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('koderuang, namaruang, kodekelas, kode_kamar', 'required'),
			array('kapasitas, tersedia, tersediapria, tersediawanita, tersediapriawanita, kode_kamar', 'numerical', 'integerOnly'=>true),
			array('koderuang, kodekelas', 'length', 'max'=>10),
			array('namaruang', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('koderuang, namaruang, kodekelas, kapasitas, tersedia, tersediapria, tersediawanita, tersediapriawanita, created, kode_kamar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'koderuang' => 'Koderuang',
			'namaruang' => 'Namaruang',
			'kodekelas' => 'Kodekelas',
			'kapasitas' => 'Kapasitas',
			'tersedia' => 'Tersedia',
			'tersediapria' => 'Tersediapria',
			'tersediawanita' => 'Tersediawanita',
			'tersediapriawanita' => 'Tersediapriawanita',
			'created' => 'Created',
			'kode_kamar' => 'Kode Kamar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('koderuang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('namaruang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kodekelas',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kapasitas',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tersedia',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tersediapria',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tersediawanita',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tersediapriawanita',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kode_kamar',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BpjsAplicare the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
