<?php

/**
 * This is the model class for table "tr_rawat_jalan_rincian".
 *
 * The followings are the available columns in table 'tr_rawat_jalan_rincian':
 * @property integer $id_rincian
 * @property integer $id_rawat_jalan
 * @property double $obs_ird
 * @property integer $dokter_ird
 * @property double $biaya_pengawasan
 * @property integer $jml_dokter_ird
 * @property double $jasa_perawat
 * @property double $askep_kamar
 * @property double $asuhan_nutrisi
 * @property integer $jumlah_asuhan
 * @property string $created
 *
 * The followings are the available model relations:
 * @property DmDokter $dokterIrd
 * @property TrPendaftaranRjalan $idRawatJalan
 */
class TrRawatJalanRincian extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_jalan_rincian';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_rawat_jalan, dokter_ird, biaya_pengawasan', 'required'),
			array('id_rawat_jalan, dokter_ird, jml_dokter_ird', 'numerical', 'integerOnly'=>true),
			array('obs_ird, biaya_pengawasan, jasa_perawat', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_rincian, id_rawat_jalan, obs_ird, dokter_ird, biaya_pengawasan, jml_dokter_ird, jasa_perawat, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dokterIrd' => array(self::BELONGS_TO, 'DmDokter', 'dokter_ird'),
			'rawatJalan' => array(self::BELONGS_TO, 'TrPendaftaranRjalan', 'id_rawat_jalan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_rincian' => 'Id Rincian',
			'id_rawat_jalan' => 'Id Rawat Jalan',
			'obs_ird' => 'Obs Ird',
			'dokter_ird' => 'Dokter Ird',
			'biaya_pengawasan' => 'Biaya Pengawasan',
			'jml_dokter_ird' => 'Jml Dokter Ird',
			'jasa_perawat' => 'Jasa Perawat',
			
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_rincian',$this->id_rincian);
		$criteria->compare('id_rawat_jalan',$this->id_rawat_jalan);
		$criteria->compare('obs_ird',$this->obs_ird);
		$criteria->compare('dokter_ird',$this->dokter_ird);
		$criteria->compare('biaya_pengawasan',$this->biaya_pengawasan);
		$criteria->compare('jml_dokter_ird',$this->jml_dokter_ird);
		$criteria->compare('jasa_perawat',$this->jasa_perawat);
		
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatJalanRincian the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
