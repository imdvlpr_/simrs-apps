<?php

/**
 * This is the model class for table "tr_pendaftaran_rjalan".
 *
 * The followings are the available columns in table 'tr_pendaftaran_rjalan':
 * @property integer $Id
 * @property string $NoDaftar
 * @property integer $NoMedrec
 * @property integer $GolPasien
 * @property integer $KodePoli
 * @property integer $KodeSubPlg
 * @property string $KetPlg
 * @property integer $UrutanPoli
 * @property integer $KodeTdkLanjut
 * @property string $CaraMasuk
 * @property string $KetCaraMasuk
 * @property integer $StatusKunj
 * @property string $KetTdkL1
 * @property string $KetTdkL2
 * @property string $KetTdkL3
 * @property string $KetTdkL4
 * @property string $KetTdkL5
 * @property integer $NoAntriPoli
 * @property string $PostMRS
 * @property string $WAKTU_DAFTAR
 * @property string $created
 */
class TrPendaftaranRjalan extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $IS_BPJS = false;

	public $PASIEN_NAMA='';
	public $tanggal_masuk = '';
	public $tanggal_pulang = '';

	public $TANGGAL_AWAL = '';
	public $TANGGAL_AKHIR = '';
	public $POLI = '';



	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_pendaftaran_rjalan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NoMedrec,GolPasien, WAKTU_DAFTAR,CaraMasuk,poli1_id', 'required','message'=> '{attribute} harus diisi'),
			array('NoMedrec, GolPasien, KodePoli, KodeSubPlg, UrutanPoli, KodeTdkLanjut, StatusKunj, NoAntriPoli', 'numerical', 'integerOnly'=>true),
			array('NoDaftar', 'length', 'max'=>100),
			array('KetPlg, PostMRS', 'length', 'max'=>30),
			array('CaraMasuk, KetCaraMasuk', 'length', 'max'=>255),
			array('KetTdkL1, KetTdkL2, KetTdkL3, KetTdkL4, KetTdkL5', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, NoDaftar, NoMedrec, GolPasien, KodePoli, KodeSubPlg, KetPlg, UrutanPoli, KodeTdkLanjut, CaraMasuk, KetCaraMasuk, StatusKunj, KetTdkL1, KetTdkL2, KetTdkL3, KetTdkL4, KetTdkL5, NoAntriPoli, PostMRS, WAKTU_DAFTAR,BATAS_CETAK_SEP,waktu_pulang,biaya_paket_1,biaya_paket_2,poli1_id,poli2_id,poli3_id, created,no_sep', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			'pASIEN' => array(self::BELONGS_TO, 'Pasien', 'NoMedrec'),
			'gOLPASIEN' => array(self::BELONGS_TO, 'GolPasien', 'GolPasien'),
			'dokter' => array(self::BELONGS_TO, 'DmDokter', 'dokter_id'),
			'trRawatJalanRincians' => array(self::HAS_ONE, 'TrRawatJalanRincian', 'id_rawat_jalan'),
			'trRawatJalanAlkes' => array(self::HAS_MANY, 'TrRawatJalanAlkes', 'id_rawat_jalan'),
			'trRawatJalanLains' => array(self::HAS_MANY, 'TrRawatJalanLain', 'id_rawat_jalan'),
			'trRawatJalanPenunjangs' => array(self::HAS_MANY, 'TrRawatJalanPenunjang', 'id_rawat_jalan'),
			'trRawatJalanTnops' => array(self::HAS_MANY, 'TrRawatJalanTnop', 'id_rawat_jalan'),
			'trRawatJalanVisiteDokters' => array(self::HAS_MANY, 'TrRawatJalanVisiteDokter', 'id_rawat_jalan'),
			'poli1' => array(self::BELONGS_TO, 'Poli', 'poli1_id'),
			'poli2' => array(self::BELONGS_TO, 'Poli', 'poli2_id'),
			'poli3' => array(self::BELONGS_TO, 'Poli', 'poli3_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'NoDaftar' => 'No Daftar',
			'NoMedrec' => 'No RM Pasien',
			'GolPasien' => 'Gol Pasien',
			'KodePoli' => 'Kode Poli',
			'KodeSubPlg' => 'Kode Sub Plg',
			'KetPlg' => 'Ket Plg',
			'UrutanPoli' => 'Urutan Poli',
			'KodeTdkLanjut' => 'Kode Tdk Lanjut',
			'CaraMasuk' => 'Cara Masuk',
			'KetCaraMasuk' => 'Ket Cara Masuk',
			'StatusKunj' => 'Status Kunj',
			'KetTdkL1' => 'Tindakan Poli 1',
			'KetTdkL2' => 'Tindakan Poli 2',
			'KetTdkL3' => 'Tindakan Poli 3',
			'KetTdkL4' => 'Ket Tdk L4',
			'KetTdkL5' => 'Ket Tdk L5',
			'biaya_paket_1' => 'Paket 1',
			'biaya_paket_2' => 'Paket 2',
			'NoAntriPoli' => 'No Antri Poli',
			'PostMRS' => 'Post MRS dari Ruang',
			'WAKTU_DAFTAR' => 'Waktu Daftar',
			'waktu_pulang' => 'Waktu Pulang',
			'BATAS_CETAK_SEP' => 'Batas Cetak SEP',
			'poli1_id' => 'Poli 1',
			'poli2_id' => 'Poli 2',
			'poli3_id' => 'Poli 3',
			'created' => 'Kedatangan',
			'no_sep' => 'No SEP'
		);
	}

	public function searchLaporan()
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('WAKTU_DAFTAR', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR, 'AND');

		return TrPendaftaranRjalan::model()->findAll($criteria);
	}


	public function countKunjungan($norm)
	{
		$model = TrPendaftaranRjalan::model()->findAllByAttributes(array('NoMedrec'=>$norm));

		return count($model);
	}

	public function searchKunjungan()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$sort = new CSort();
		$criteria=new CDbCriteria;

		
		$criteria->addSearchCondition('NoDaftar',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('t.NoMedrec',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('gOLPASIEN.GolPasien',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('KodePoli',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('KodeSubPlg',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('PostMRS',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('WAKTU_DAFTAR',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('pASIEN.NAMA',$this->SEARCH,true,'OR');
		$criteria->compare('poli1_id',$this->poli1_id);
		$criteria->with = array('pASIEN','gOLPASIEN');
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$sort = new CSort();
		$criteria=new CDbCriteria;

		
		$criteria->addSearchCondition('NoDaftar',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('t.NoMedrec',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('gOLPASIEN.GolPasien',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('KodePoli',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('KodeSubPlg',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('PostMRS',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('WAKTU_DAFTAR',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('pASIEN.NAMA',$this->SEARCH,true,'OR');

		if(!empty($this->POLI))
			$criteria->compare("poli1_id",$this->POLI);

		$criteria->with = array('pASIEN','gOLPASIEN');
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrPendaftaranRjalan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function cekBatasSEPFromGoogle($tgldaftar)
	{
		$tanggal = date('Y-m-d', strtotime($tgldaftar));
		$time = date('H:i:s', strtotime($tgldaftar));
	
		$hari = array();
		
		$calendar = Yii::app()->GoogleApis->serviceFactory('Calendar');
		
		$client = Yii::app()->GoogleApis->client;
		 
		$tgl = date('Y-01-01').'T00:00:00+00:00';
		$calendarId = 'en.indonesian#holiday@group.v.calendar.google.com';
		$optParams = array(
		  //'maxResults' => 10,
			'orderBy' => 'startTime',
			'singleEvents' => TRUE,
			'timeMin' => $tgl
		);
		
		$results = $calendar->events->listEvents($calendarId,$optParams);
	
		if (count($results['items']) == 0) {
		  	print "No upcoming events found.\n";
		} else {
			
			$counter = 1;
			$is_finish = false;
			$normal_counter = 0;
			
			
			while(!$is_finish)
			{
				$tanggal = date('Y-m-d', strtotime($tgldaftar . ' +'.$counter.' day'));
			
				$found_date = '';
				foreach ($results['items'] as $event) {

					$gcal = $event['start']['date'];

					if($tanggal == $gcal)
					{
						$found_date = $gcal;
					}

				}



				if(!empty($found_date))
				{

					$counter++;
					continue;
				}

				else
				{
					$weekDay = date('w', strtotime($tanggal));
					
					if($weekDay != 0 && $weekDay != 6)
					{
						$counter++;
						$normal_counter++;
						
						$hari[] = array(
							'tanggal' => $tanggal.' '.$time
						);
						
						if($normal_counter >= 2)
						{
							$is_finish = true;
						}

						continue;
					}
					else
					{
						$counter++;
						continue;
					}
				}
				
			}


			
		}

		return $hari[count($hari)-1];
	}

	public function cekBatasSEP($tgldaftar)
	{
		$counter = 1;
		$is_finish = false;
		$normal_counter = 0;
		$hari = array();
		
		while(!$is_finish)
		{
			$tanggal = date('Y-m-d', strtotime($tgldaftar . ' +'.$counter.' day'));
			
			$criteria = new CDbCriteria;
			$criteria->addSearchCondition('startdate',$tanggal,true,'OR');
			$cal = DmKalender::model()->find($criteria);
			if(!empty($cal))
			{	
				$counter++;
				continue;
			}
			else
			{

				$weekDay = date('w', strtotime($tanggal));
				
				if($weekDay != 0 && $weekDay != 6)
				{
					$counter++;
					$normal_counter++;
					
					$hari[] = array(
						'normal' => $tanggal
					);
					
					if($normal_counter >= 2)
					{
						$is_finish = true;
					}

					continue;
				}
				else
				{
					$counter++;
					continue;
				}
			}
		}

		return $hari;
		
	}

	private function contains($needle, $haystack)
	{
	    return strpos($haystack, $needle) !== false;
	}

	protected function afterFind()
	{

		$this->IS_BPJS = $this->contains("BPJS",$this->gOLPASIEN->NamaGol);
		
		$this->NoMedrec = Yii::app()->helper->appendZeros($this->NoMedrec, 6);

		$datetime_masuk = explode(' ',$this->WAKTU_DAFTAR);
		$tgl_masuk = $datetime_masuk[0];
		$jam_masuk = $datetime_masuk[1];

		$var = $tgl_masuk;
		$date = str_replace('/', '-', $var);
		

		$this->tanggal_masuk = date('Y-m-d', strtotime($date));

		$this->tanggal_masuk = date('d/m/Y', strtotime($this->tanggal_masuk));
		
		parent::afterFind();
	}
}
