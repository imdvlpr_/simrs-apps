<?php

/**
 * This is the model class for table "tr_rawat_inap_lain".
 *
 * The followings are the available columns in table 'tr_rawat_inap_lain':
 * @property integer $id_ri_lain
 * @property integer $id_lain
 * @property double $biaya_ird
 * @property double $biaya_irna
 * @property integer $jumlah_tindakan
 * @property string $created
 * @property integer $id_rawat_inap
 *
 * The followings are the available model relations:
 * @property TrRawatInap $idRawatInap
 * @property TindakanMedisLain $idLain
 */
class TrRawatInapLain extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_lain';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_lain, biaya_ird, biaya_irna, jumlah_tindakan, id_rawat_inap', 'required'),
			array('id_lain, jumlah_tindakan, id_rawat_inap', 'numerical', 'integerOnly'=>true),
			array('biaya_ird, biaya_irna', 'numerical'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ri_lain, id_lain, biaya_ird, biaya_irna, jumlah_tindakan, created, id_rawat_inap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'id_rawat_inap'),
			'lain' => array(self::BELONGS_TO, 'TindakanMedisLain', 'id_lain'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ri_lain' => 'Id Ri Lain',
			'id_lain' => 'Item Lain-lain',
			'biaya_ird' => 'Biaya Ird',
			'biaya_irna' => 'Biaya Irna',
			'jumlah_tindakan' => 'Jumlah Tindakan',
			'created' => 'Created',
			'id_rawat_inap' => 'Id Rawat Inap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ri_lain',$this->id_ri_lain);
		$criteria->compare('id_lain',$this->id_lain);
		$criteria->compare('biaya_ird',$this->biaya_ird);
		$criteria->compare('biaya_irna',$this->biaya_irna);
		$criteria->compare('jumlah_tindakan',$this->jumlah_tindakan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('id_rawat_inap',$this->id_rawat_inap);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getBiayaLainBy($id, $param)
	{

		$rawatInap = TrRawatInap::model()->findByPk($id);
		$listLain = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>$rawatInap->kamar->kelas_id));
	   	$total_ird = 0;
	   	$total_irna = 0;
	   	foreach($listLain as $lain)
	   	{
	   		
	   	   if($lain->nama_tindakan != $param)
	   	   	  continue;

	       $attr = array(
	        'id_rawat_inap' => $rawatInap->id_rawat_inap,
	        'id_lain' => $lain->id_tindakan
	      );

	      $itemLain = TrRawatInapLain::model()->findByAttributes($attr);
	      $jumlah_tindakan = 1;
	      $biaya_irna = $lain->tarip;
	      $biaya_ird = 0;

	      $biaya_irna_lain_total = 0;
	      if(!empty($itemLain)){
	        $jumlah_tindakan = $itemLain->jumlah_tindakan;
	        $biaya_irna = $itemLain->biaya_irna;
	        $biaya_ird = $itemLain->biaya_ird;
	        $biaya_irna_lain_total = $biaya_irna_lain_total + $itemLain->biaya_irna;// * $jumlah_tindakan;
	      }

	      $total_ird = $total_ird + $biaya_ird;
	      $total_irna = $total_irna + $biaya_irna_lain_total;
	  }

	  return $total_irna + $total_ird;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapLain the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
