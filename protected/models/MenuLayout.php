<?php

/**
 * This is the model class for table "menu_layout".
 *
 * The followings are the available columns in table 'menu_layout':
 * @property integer $id
 * @property string $nama
 * @property string $link
 * @property integer $parent
 * @property integer $level
 * @property integer $urutan
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property MenuLayout $parent0
 * @property MenuLayout[] $menuLayouts
 * @property MenuLayoutRbac[] $menuLayoutRbacs
 */
class MenuLayout extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menu_layout';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, link, icon', 'required'),
			array('parent, level, urutan', 'numerical', 'integerOnly'=>true),
			array('nama, link', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, link, parent, level, urutan, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent0' => array(self::BELONGS_TO, 'MenuLayout', 'parent'),
			'menuLayouts' => array(self::HAS_MANY, 'MenuLayout', 'parent'),
			'menuLayoutRbacs' => array(self::HAS_MANY, 'MenuLayoutRbac', 'menu_id'),
		);
	}

	public function getSubmenus()
	{
		$menu = MenuLayout::model()->findAllByAttributes(['parent'=>$this->id],['order'=>'urutan ASC']);
		return $menu;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'link' => 'Link',
			'icon' => 'Icon',
			'parent' => 'Parent',
			'level' => 'Level',
			'urutan' => 'Urutan',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('link',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('parent',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('level',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('urutan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public static function getMenu()
	{
		$results = MenuLayout::model()->findAllByAttributes(['level'=>1],['order'=>'urutan']);
		$menus = [];
		foreach($results as $m)
		{

			$privileges = [];

			foreach($m->menuLayoutRbacs as $role)
			{
				$privileges[] = $role->role_id;
			}

		    $tmp = [
		        'label' => '<i class="menu-icon '.$m->icon.'"></i><span class="menu-text"> '.$m->nama.' </span>',
		        'url' => [$m->link],
		        'visible' => Yii::app()->user->checkAccess($privileges)
		    ];

		    if(count($m->menuLayouts) > 0)
		    {
		        $tmp['label'] = $tmp['label'].'<i class="caret"></i>';
		        $tmp['itemOptions'] = ['class'=>'dropdown-toggle','tabindex'=>"-1"];
		        $tmp['url'] = '#';
		        $tmp['linkOptions'] = ['class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'];
		        
		    }

		    foreach($m->getSubmenus() as $submenu)
		    {

		    	$privileges = [];
		       	foreach($submenu->menuLayoutRbacs as $role)
				{
					$privileges[] = $role->role_id;
				}

		        if($submenu->nama == '-')
		        {
		            $subtmp = [
		                'label' => '',
		                'itemOptions' => ['class'=>'divider'],
		                'visible' => Yii::app()->user->checkAccess($privileges)
		            ];                    
		            $tmp['items'][] = $subtmp;
		            continue;
		        }

		        $subtmp = [
		            'label' => '<i class="menu-icon '.$submenu->icon.'"></i><span class="menu-text"> '.$submenu->nama.' </span>',
		            'url' => [$submenu->link],
		            'visible' => Yii::app()->user->checkAccess($privileges)
		        ];

		        if(count($submenu->menuLayouts) > 0)
		        {
		            $subtmp['label'] = $subtmp['label'].' <b class="arrow fa fa-angle-down"></b>';
		            $subtmp['itemOptions'] = ['class'=>'dropdown-toggle','tabindex'=>"-1"];
		            $subtmp['url'] = '#';
		            $subtmp['linkOptions'] = ['class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'];
		        }

		        foreach($submenu->getSubmenus() as $ssm)
		        {
		        	$privileges = [];
			       	foreach($ssm->menuLayoutRbacs as $role)
					{
						$privileges[] = $role->role_id;
					}
		            if($ssm->nama == '-')
		            {
		                $stmp = [
		                    'label' => '',
		                    'itemOptions' => ['class'=>'divider']
		                ];                    
		                $subtmp['items'][] = $stmp;
		                continue;
		            }
		            
		            $stmp = [
		                'label' => '<i class="menu-icon '.$ssm->icon.'"></i><span class="menu-text"> '.$ssm->nama.' </span>',
		                'url' => [$ssm->link],
		                'visible' => Yii::app()->user->checkAccess($privileges)
		            ];

		            if(count($ssm->menuLayouts) > 0)
		            {
		                $stmp['label'] = $stmp['label'].' <b class="arrow fa fa-angle-down"></b>';
		                $stmp['itemOptions'] = ['class'=>'dropdown-toggle','tabindex'=>"-1"];
		                $stmp['url'] = '#';
		                $stmp['linkOptions'] = ['class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'];
		            }

		            foreach($ssm->getSubmenus() as $sssm)
		            {
		            	$privileges = [];
				       	foreach($sssm->menuLayoutRbacs as $role)
						{
							$privileges[] = $role->role_id;
						}

		                $sstmp = [
		                    'label' => '<i class="menu-icon '.$sssm->icon.'"></i><span class="menu-text"> '.$sssm->nama.' </span>',
		                    'url' => [$sssm->link],
		                    'visible' => Yii::app()->user->checkAccess($privileges)
		                ];

		                $stmp['items'][] = $sstmp;
		            }

		            $subtmp['items'][] = $stmp;
		        }

		        $tmp['items'][] = $subtmp;
		        
		    }

		    $menus[] = $tmp;
		}
		return $menus;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MenuLayout the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
