<?php

/**
 * This is the model class for table "a_pasien_alamat".
 *
 * The followings are the available columns in table 'a_pasien_alamat':
 * @property integer $id
 * @property integer $pasien_id
 * @property integer $propinsi_id
 * @property integer $kota_id
 * @property integer $kecamatan_id
 * @property string $desa
 * @property string $rt
 * @property string $rw
 * @property string $jalan
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Pasien $pasien
 * @property Refprov $propinsi
 * @property Refkabkota $kota
 * @property Refkec $kecamatan
 */
class PasienAlamat extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'a_pasien_alamat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pasien_id, propinsi_id, kota_id, kecamatan_id, desa, rt, rw, jalan, salutation', 'required','message'=>'{attribute} harus diisi.'),
			array('pasien_id, propinsi_id, kota_id, kecamatan_id, rt, rw', 'numerical', 'integerOnly'=>true),
			array('desa, jalan', 'length', 'max'=>255),
			// array('rt, rw', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pasien_id, propinsi_id, kota_id, kecamatan_id, desa, rt, rw, jalan, created,salutation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pasien' => array(self::BELONGS_TO, 'Pasien', 'pasien_id'),
			'propinsi' => array(self::BELONGS_TO, 'Refprov', 'propinsi_id'),
			'kota' => array(self::BELONGS_TO, 'Refkabkota', 'kota_id'),
			'kecamatan' => array(self::BELONGS_TO, 'Refkec', 'kecamatan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pasien_id' => 'Pasien',
			'salutation' =>'Panggilan',
			'propinsi_id' => 'Propinsi',
			'kota_id' => 'Kota',
			'kecamatan_id' => 'Kecamatan',
			'desa' => 'Desa',
			'rt' => 'RT',
			'rw' => 'RW',
			'jalan' => 'Jalan/Dusun',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('pasien_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('propinsi_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kota_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kecamatan_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('desa',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('rt',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('rw',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jalan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PasienAlamat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
