<?php

/**
 * This is the model class for table "erp_user".
 *
 * The followings are the available columns in table 'erp_user':
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property integer $status
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $account_activation_token
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $perusahaan_id
 * @property string $access_role
 *
 * The followings are the available model relations:
 * @property ErpDepartemenUser[] $erpDepartemenUsers
 * @property ErpPerusahaan $perusahaan
 */
class ErpUser extends MyActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public function getDbConnection()
    {
        return self::getErpDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'erp_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, email, password_hash, status, auth_key, created_at, updated_at', 'required'),
			array('status, created_at, updated_at, perusahaan_id', 'numerical', 'integerOnly'=>true),
			array('username, email, password_hash, password_reset_token, account_activation_token', 'length', 'max'=>255),
			array('auth_key', 'length', 'max'=>32),
			array('access_role', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, email, password_hash, status, auth_key, password_reset_token, account_activation_token, created_at, updated_at, perusahaan_id, access_role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'erpDepartemenUsers' => array(self::HAS_MANY, 'ErpDepartemenUser', 'user_id'),
			'perusahaan' => array(self::BELONGS_TO, 'ErpPerusahaan', 'perusahaan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'email' => 'Email',
			'password_hash' => 'Password Hash',
			'status' => 'Status',
			'auth_key' => 'Auth Key',
			'password_reset_token' => 'Password Reset Token',
			'account_activation_token' => 'Account Activation Token',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'perusahaan_id' => 'Perusahaan',
			'access_role' => 'Access Role',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('username',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('email',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('password_hash',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('status',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('auth_key',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('password_reset_token',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('account_activation_token',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('perusahaan_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('access_role',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ErpUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
