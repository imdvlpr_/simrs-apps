<?php

/**
 * This is the model class for table "tr_rawat_inap_asnut".
 *
 * The followings are the available columns in table 'tr_rawat_inap_asnut':
 * @property integer $id_ri_asnut
 * @property integer $id_rawat_inap
 * @property double $nilai_asnut
 * @property string $created
 *
 * The followings are the available model relations:
 * @property TrRawatInap $idRawatInap
 */
class TrRawatInapAsnut extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_asnut';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_rawat_inap, nilai_asnut, created', 'required'),
			array('id_rawat_inap', 'numerical', 'integerOnly'=>true),
			array('nilai_asnut', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ri_asnut, id_rawat_inap, nilai_asnut, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'id_rawat_inap'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ri_asnut' => 'Id Ri Asnut',
			'id_rawat_inap' => 'Id Rawat Inap',
			'nilai_asnut' => 'Nilai Asnut',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ri_asnut',$this->id_ri_asnut);
		$criteria->compare('id_rawat_inap',$this->id_rawat_inap);
		$criteria->compare('nilai_asnut',$this->nilai_asnut);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapAsnut the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
