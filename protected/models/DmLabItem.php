<?php

/**
 * This is the model class for table "dm_lab_item".
 *
 * The followings are the available columns in table 'dm_lab_item':
 * @property integer $id
 * @property integer $kategori_id
 * @property integer $grup_id
 * @property string $nama
 * @property string $metode
 * @property string $satuan
 * @property integer $is_removed
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property DmLabKategori $kategori
 * @property DmLabGrup $grup
 * @property DmLabItemParent[] $dmLabItemParents
 * @property DmLabItemParent[] $dmLabItemParents1
 * @property DmLabItemTarif[] $dmLabItemTarifs
 * @property LabRequestItem[] $labRequestItems
 * @property LabRequestItem[] $labRequestItems1
 */
class DmLabItem extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_lab_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('kategori_id, grup_id, is_removed', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, kategori_id, grup_id, nama, is_removed, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kategori' => array(self::BELONGS_TO, 'DmLabKategori', 'kategori_id','alias'=>'k'),
			'grup' => array(self::BELONGS_TO, 'DmLabGrup', 'grup_id','alias' => 'g'),
			'dmLabItemParents' => array(self::HAS_MANY, 'DmLabItemParent', 'parent_id'),
			'dmLabItemOptions' => array(self::HAS_MANY, 'DmLabItemOption', 'item_id'),
			'dmLabItemParents1' => array(self::HAS_MANY, 'DmLabItemParent', 'item_id'),
			'dmLabItemTarifs' => array(self::HAS_MANY, 'DmLabItemTarif', 'item_id'),
			'labRequestItems' => array(self::HAS_MANY, 'LabRequestItem', 'item_id'),
			'countTindakan'	=> array(self::STAT, 'BPendaftaranRjalanTindakan', 'Id', 'select'=>'SUM(biaya)'),
			'labRequestItems1' => array(self::HAS_MANY, 'LabRequestItem', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'kategori_id' => 'Kategori',
			'grup_id' => 'Grup',
			'nama' => 'Nama',
			'is_removed' => 'Is Removed',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function getParentName()
	{
		return !empty($this->parent) ? $this->parent->nama : '';
	}

	public function getKategoriName()
	{
		return !empty($this->kategori) ? $this->kategori->nama : '';
	}

	public function getGrupName()
	{
		return !empty($this->grup) ? $this->grup->nama : '';
	}

	public function searchByNama($q)
	{
	
		$criteria=new CDbCriteria;
	
		$criteria->addSearchCondition('t.nama',$q,true,'OR');
		$criteria->addCondition('is_removed = 0');
		$criteria->limit = 10;
		$criteria->order = 't.nama ASC';
	
		return DmLabItem::model()->findAll($criteria);
	
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;
		$sort->attributes = [
			
			'kategoriName'=>[
				'asc'=>'k.nama',
				'desc'=>'k.nama desc',
			],
			'grupName'=>[
				'asc'=>'g.nama',
				'desc'=>'g.nama desc',
			],
			'nama'=>[
				'asc'=>'t.nama',
				'desc'=>'t.nama desc',
			],
		];


		$criteria->addSearchCondition('k.nama',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('g.nama',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('t.nama',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('t.nilai_normal',$this->SEARCH,true,'OR');
		$criteria->with = ['kategori','grup'];

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmLabItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
