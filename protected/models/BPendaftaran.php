<?php

/**
 * This is the model class for table "b_pendaftaran".
 *
 * The followings are the available columns in table 'b_pendaftaran':
 * @property double $NODAFTAR
 * @property integer $NoMedrec
 * @property integer $KodePtgs
 * @property string $TGLDAFTAR
 * @property string $JamDaftar
 * @property integer $umurthn
 * @property integer $umurbln
 * @property integer $UmurHr
 * @property integer $KodeJnsUsia
 * @property integer $KodeMasuk
 * @property string $KetMasuk
 * @property integer $JnsRawat
 * @property string $DUtama
 * @property string $D1
 * @property string $D2
 * @property string $D3
 * @property string $D4
 * @property string $D5
 * @property string $P1
 * @property string $P2
 * @property string $P3
 * @property string $P4
 * @property string $P5
 * @property string $PxBaruLama
 * @property double $IdAntri
 * @property integer $KunjunganKe
 * @property string $KeluarRS
 * @property string $TglKRS
 * @property string $JamKRS
 * @property integer $LamaRawat
 * @property integer $KodeUnitDaftar
 * @property integer $KunjTerakhirRJ
 * @property integer $KodeGol
 * @property integer $KodeStatusRM
 * @property integer $IsRGT
 * @property string $NoSEP
 * @property integer $KodeSubPulang
 * @property string $DPJP
 *
 * The followings are the available model relations:
 * @property APasien $noMedrec
 * @property AGolpasien $kodeGol
 * @property ARefcarapulangdtl $kodeSubPulang
 * @property ARefcaramasuk $kodeMasuk
 * @property BPendaftaranRinap[] $bPendaftaranRinaps
 * @property BPendaftaranRjalan[] $bPendaftaranRjalans
 * @property TdRegisterOk[] $tdRegisterOks
 * @property TrRawatJalan[] $trRawatJalans
 * @property TrResepPasien[] $trResepPasiens
 * @property TrTrackingRm[] $trTrackingRms
 */
class BPendaftaran extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public $namaICD;
	public $namaP1, $namaP2, $namaP3, $namaP4, $namaP5;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b_pendaftaran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KodePtgs', 'required'),
			array('NoMedrec, KodePtgs, umurthn, umurbln, UmurHr, KodeJnsUsia, KodeMasuk, JnsRawat, KunjunganKe, LamaRawat, KodeUnitDaftar, KunjTerakhirRJ, KodeGol, KodeStatusRM, IsRGT, KodeSubPulang', 'numerical', 'integerOnly'=>true),
			array('IdAntri', 'numerical'),
			array('KetMasuk', 'length', 'max'=>60),
			array('DUtama, D1, D2, D3, D4, D5, P1, P2, P3, P4, P5', 'length', 'max'=>10),
			array('PxBaruLama', 'length', 'max'=>4),
			array('KeluarRS', 'length', 'max'=>1),
			array('NoSEP', 'length', 'max'=>20),
			array('DPJP', 'length', 'max'=>30),
			array('TGLDAFTAR, JamDaftar, TglKRS, JamKRS', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NODAFTAR, NoMedrec, KodePtgs, TGLDAFTAR, JamDaftar, umurthn, umurbln, UmurHr, KodeJnsUsia, KodeMasuk, KetMasuk, JnsRawat, DUtama, D1, D2, D3, D4, D5, P1, P2, P3, P4, P5, PxBaruLama, IdAntri, KunjunganKe, KeluarRS, TglKRS, JamKRS, LamaRawat, KodeUnitDaftar, KunjTerakhirRJ, KodeGol, KodeStatusRM, IsRGT, NoSEP, KodeSubPulang, DPJP', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'noMedrec' => array(self::BELONGS_TO, 'Pasien', 'NoMedrec'),
			'kodeGol' => array(self::BELONGS_TO, 'GolPasien', 'KodeGol'),
			'kodeSubPulang' => array(self::BELONGS_TO, 'Refcarapulangdtl', 'KodeSubPulang'),
			'kodeMasuk' => array(self::BELONGS_TO, 'Refcaramasuk', 'KodeMasuk'),
			'bPendaftaranRinaps' => array(self::HAS_MANY, 'BPendaftaranRinap', 'NoDaftar'),
			'bPendaftaranRjalans' => array(self::HAS_MANY, 'BPendaftaranRjalan', 'NoDaftar'),
			'tdRegisterOks' => array(self::HAS_MANY, 'TdRegisterOk', 'kode_daftar'),
			'trRawatJalans' => array(self::HAS_MANY, 'TrRawatJalan', 'reg_id'),
			'trResepPasiens' => array(self::HAS_MANY, 'TrResepPasien', 'nodaftar_id'),
			'trTrackingRms' => array(self::HAS_MANY, 'TrTrackingRm', 'b_pendaftaran_id'),
			'listDiagnosis' => array(self::HAS_MANY, 'BPendaftaranDiagnosis', 'reg_id','condition'=>'tipe=1'),
			'listTindakan' => array(self::HAS_MANY, 'BPendaftaranDiagnosis', 'reg_id','condition'=>'tipe=2'),
			'listIcd10' => array(self::HAS_MANY, 'BPendaftaranCoding', 'reg_id'),
			'listProsedur' => array(self::HAS_MANY, 'BPendaftaranTindakanCoding', 'reg_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NODAFTAR' => 'Nodaftar',
			'NoMedrec' => 'No Medrec',
			'KodePtgs' => 'Kode Ptgs',
			'TGLDAFTAR' => 'Tgldaftar',
			'JamDaftar' => 'Jam Daftar',
			'umurthn' => 'Umurthn',
			'umurbln' => 'Umurbln',
			'UmurHr' => 'Umur Hr',
			'KodeJnsUsia' => 'Kode Jns Usia',
			'KodeMasuk' => 'Kode Masuk',
			'KetMasuk' => 'Ket Masuk',
			'JnsRawat' => 'Jns Rawat',
			'DUtama' => 'Diag utama',
			'D1' => 'D1',
			'D2' => 'D2',
			'D3' => 'D3',
			'D4' => 'D4',
			'D5' => 'D5',
			'P1' => 'P1',
			'P2' => 'P2',
			'P3' => 'P3',
			'P4' => 'P4',
			'P5' => 'P5',
			'PxBaruLama' => 'Px Baru Lama',
			'IdAntri' => 'Id Antri',
			'KunjunganKe' => 'Kunjungan Ke',
			'KeluarRS' => 'Keluar Rs',
			'TglKRS' => 'Tgl Krs',
			'JamKRS' => 'Jam Krs',
			'LamaRawat' => 'Lama Rawat',
			'KodeUnitDaftar' => 'Kode Unit Daftar',
			'KunjTerakhirRJ' => 'Kunj Terakhir Rj',
			'KodeGol' => 'Kode Gol',
			'KodeStatusRM' => 'Kode Status Rm',
			'IsRGT' => 'Is Rgt',
			'NoSEP' => 'No Sep',
			'KodeSubPulang' => 'Kode Sub Pulang',
			'DPJP' => 'Dpjp',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		// $criteria->addSearchCondition('NODAFTAR',$this->SEARCH,true,'OR');
		$criteria->compare('NoMedrec',$this->SEARCH);
		// $criteria->addSearchCondition('TGLDAFTAR',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('JamDaftar',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('umurthn',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('umurbln',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('UmurHr',$this->SEARCH,true,'OR');
		
		if(!empty($this->KodeGol))
			$criteria->compare('KodeGol',$this->KodeGol);

		$criteria->order = 'NODAFTAR DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function countPasien(){
		$total = BPendaftaran::model()->findAllByAttributes(['NoMedrec'=>$this->NoMedrec]);
		return count($total);
	}

	protected function beforeSave()
	{
		$this->TGLDAFTAR = date('Y-m-d',strtotime($this->TGLDAFTAR));
		return parent::beforeSave();
	}

	protected function afterFind()
	{

		$this->TGLDAFTAR = date('d/m/Y',strtotime($this->TGLDAFTAR));
		return parent::afterFind();
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BPendaftaran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
