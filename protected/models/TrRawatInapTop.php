<?php

/**
 * This is the model class for table "tr_rawat_inap_top".
 *
 * The followings are the available columns in table 'tr_rawat_inap_top':
 * @property integer $id_ri_top
 * @property integer $id_top
 * @property double $biaya
 * @property string $created
 * @property integer $id_rawat_inap
 *
 * The followings are the available model relations:
 * @property TindakanMedisOperatif $idTop
 * @property TrRawatInap $idRawatInap
 */
class TrRawatInapTop extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_top';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_top, biaya_irna, biaya_ird,jumlah_tindakan', 'required','message'=>'{attribute} harus diisi'),
			array('id_top, id_rawat_inap', 'numerical', 'integerOnly'=>true),
			array('biaya_ird, biaya_irna', 'numerical'),
		
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ri_top, id_top, biaya_irna, created, id_rawat_inap,biaya_ird, jumlah_tindakan,id_dokter_jm, id_dokter_anas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'top' => array(self::BELONGS_TO, 'TindakanMedisOperatif', 'id_top'),
			'rawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'id_rawat_inap'),
			'dokterJm' => array(self::BELONGS_TO, 'DmDokter', 'id_dokter_jm'),
			'dokterAnas' => array(self::BELONGS_TO, 'DmDokter', 'id_dokter_anas'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ri_top' => 'Id Ri Top',
			'id_top' => 'Jenis Tindakan Operatif',
			'biaya_irna' => 'Biaya Tindakan',
			'biaya_ird' => 'Biaya IRD',
			'jumlah_tindakan' => 'Jumlah Tindakan',
			'created' => 'Created',
			'id_dokter_anas' => 'Dokter Anastesi',
			'id_dokter_jm' => 'Dokter Jasa Medik',
			'id_rawat_inap' => 'Rawat Inap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ri_top',$this->id_ri_top);
		$criteria->compare('id_top',$this->id_top);
		$criteria->compare('biaya',$this->biaya);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('id_rawat_inap',$this->id_rawat_inap);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapTop the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
