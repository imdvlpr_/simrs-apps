<?php

/**
 * This is the model class for table "tr_rawat_inap_penunjang".
 *
 * The followings are the available columns in table 'tr_rawat_inap_penunjang':
 * @property integer $id_ri_penunjang
 * @property integer $id_penunjang
 * @property double $biaya_ird
 * @property double $biaya_irna
 * @property integer $jumlah_tindakan
 * @property string $created
 * @property integer $id_rawat_inap
 *
 * The followings are the available model relations:
 * @property TrRawatInap $idRawatInap
 * @property TindakanMedisPenunjang $idPenunjang
 */
class TrRawatInapPenunjang extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_penunjang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_penunjang, biaya_ird, biaya_irna, jumlah_tindakan', 'required','message'=>'{attribute} harus diisi'),
			array('id_penunjang, jumlah_tindakan, id_rawat_inap', 'numerical', 'integerOnly'=>true),
			array('biaya_ird, biaya_irna', 'numerical'),
		
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ri_penunjang, id_penunjang, biaya_ird, biaya_irna, jumlah_tindakan, created, id_rawat_inap, dokter_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'id_rawat_inap'),
			'penunjang' => array(self::BELONGS_TO, 'TindakanMedisPenunjang', 'id_penunjang'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ri_penunjang' => 'Id Ri Penunjang',
			'id_penunjang' => 'Layanan Penunjang',
			'biaya_ird' => 'Biaya Ird',
			'biaya_irna' => 'Biaya Irna',
			'jumlah_tindakan' => 'Jumlah Tindakan',
			'created' => 'Created',
			'id_rawat_inap' => 'Rawat Inap',
			'dokter_id' => 'Dokter USG'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ri_penunjang',$this->id_ri_penunjang);
		$criteria->compare('id_penunjang',$this->id_penunjang);
		$criteria->compare('biaya_ird',$this->biaya_ird);
		$criteria->compare('biaya_irna',$this->biaya_irna);
		$criteria->compare('jumlah_tindakan',$this->jumlah_tindakan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('id_rawat_inap',$this->id_rawat_inap);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getBiayaPenunjang($id, $param)
	{

		$rawatInap = TrRawatInap::model()->findByPk($id);
		$listPenunjang = TindakanMedisPenunjang::model()->findAll();
     
        $total_ird = 0;
        $total_irna = 0;

        $listNama = array();


        foreach($listPenunjang as $supp)
        {

        	if(strtolower($supp->nama_tindakan) != $param)
        		continue;
          // if(strtolower($supp->nama_tindakan) != 'foto' && strtolower($supp->nama_tindakan) != 'ct scan')
          //     continue;
          
          $attr = array(
            'id_rawat_inap' => $rawatInap->id_rawat_inap,
            'id_penunjang' => $supp->id_tindakan_penunjang
          );
          $itemSupp = TrRawatInapPenunjang::model()->findByAttributes($attr);
          
          $supp_biaya_ird = 0;
          $supp_biaya_irna = 0 ;
          $supp_jumlah_tindakan = 0;

         
          if(!empty($itemSupp)){
            // $total_ird = $total_ird + $itemSupp->biaya_ird;
           // $total_irna = $total_irna + $supp->biaya_irna;
            // $biaya_supp_irna = $biaya_supp_irna + $itemSupp->biaya_irna * $itemSupp->jumlah_tindakan;

            // $total_irna = $total_irna + $biaya_supp_irna;

            $supp_biaya_ird = $itemSupp->biaya_ird;
            $supp_biaya_irna = $itemSupp->biaya_irna;
            $supp_jumlah_tindakan = $itemSupp->jumlah_tindakan;

            $total_ird += $supp_biaya_ird;
            $total_irna += $supp_biaya_irna;
          }  

       }

       return $total_ird + $total_irna;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapPenunjang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
