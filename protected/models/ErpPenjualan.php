<?php

/**
 * This is the model class for table "erp_penjualan".
 *
 * The followings are the available columns in table 'erp_penjualan':
 * @property integer $id
 * @property string $tanggal
 * @property integer $barang_id
 * @property double $qty
 * @property string $satuan
 * @property double $harga_satuan
 * @property double $harga_total
 * @property integer $departemen_id
 * @property string $created
 *
 * The followings are the available model relations:
 * @property ErpSalesMasterBarang $barang
 * @property ErpDepartemen $departemen
 */
class ErpPenjualan extends MyActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public function getDbConnection()
    {
        return self::getErpDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'erp_penjualan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tanggal, barang_id, departemen_id, created', 'required'),
			array('barang_id, departemen_id', 'numerical', 'integerOnly'=>true),
			array('qty, harga_satuan, harga_total', 'numerical'),
			array('satuan', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal, barang_id, qty, satuan, harga_satuan, harga_total, departemen_id, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'barang' => array(self::BELONGS_TO, 'ErpSalesMasterBarang', 'barang_id'),
			'departemen' => array(self::BELONGS_TO, 'ErpDepartemen', 'departemen_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal',
			'barang_id' => 'Barang',
			'qty' => 'Qty',
			'satuan' => 'Satuan',
			'harga_satuan' => 'Harga Satuan',
			'harga_total' => 'Harga Total',
			'departemen_id' => 'Departemen',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tanggal',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('barang_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('qty',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('satuan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('harga_satuan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('harga_total',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('departemen_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ErpPenjualan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
