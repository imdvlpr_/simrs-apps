<?php

/**
 * This is the model class for table "pegawai".
 *
 * The followings are the available columns in table 'pegawai':
 * @property string $KDPEGAWAI
 * @property string $NAMA
 * @property string $ALAMAT
 * @property string $KOTA
 * @property string $KODEPOS
 * @property string $TELEPON
 * @property string $PONSEL
 * @property string $EMAIL
 * @property string $STATUS
 * @property string $USERLOG
 * @property string $PASSWD
 *
 * The followings are the available model relations:
 * @property User[] $users
 */
class Pegawai extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tpegawai';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KDPEGAWAI', 'required'),
			array('KDPEGAWAI','unique'),
			array('KDPEGAWAI, TELEPON, PONSEL, STATUS, USERLOG, PASSWD', 'length', 'max'=>20),
			array('NAMA, EMAIL', 'length', 'max'=>50),
			array('ALAMAT, KOTA', 'length', 'max'=>100),
			array('KODEPOS', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KDPEGAWAI, NAMA, ALAMAT, KOTA, KODEPOS, TELEPON, PONSEL, EMAIL, STATUS, USERLOG, PASSWD', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'users' => array(self::HAS_ONE, 'User', 'PEGAWAI_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KDPEGAWAI' => 'Kdpegawai',
			'NAMA' => 'Nama',
			'ALAMAT' => 'Alamat',
			'KOTA' => 'Kota',
			'KODEPOS' => 'Kodepos',
			'TELEPON' => 'Telepon',
			'PONSEL' => 'Ponsel',
			'EMAIL' => 'Email',
			'STATUS' => 'Status',
			'USERLOG' => 'Userlog',
			'PASSWD' => 'Passwd',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$sort = new CSort();
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('KDPEGAWAI',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NAMA',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('ALAMAT',$this->SEARCH,true,'OR');
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));

		
	}

	public function searchByNama($q)
	{
	
		$criteria=new CDbCriteria;
	
		$criteria->addSearchCondition('NAMA',$q,true,'OR');
		$criteria->limit = 10;
		$criteria->order = 'NAMA ASC';
	
		return Pegawai::model()->findAll($criteria);
	
	}

	protected function afterFind()
	{

		$this->NAMA = strtoupper($this->NAMA);
		return parent::afterFind();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pegawai the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
