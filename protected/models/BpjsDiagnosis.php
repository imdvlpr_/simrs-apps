<?php

/**
 * This is the model class for table "bpjs_diagnosis".
 *
 * The followings are the available columns in table 'bpjs_diagnosis':
 * @property string $KODE_DIAG
 * @property string $DESKRIPSI
 * @property string $CREATED
 */
class BpjsDiagnosis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs_diagnosis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODE_DIAG, DESKRIPSI, CREATED', 'required'),
			array('KODE_DIAG', 'length', 'max'=>10),
			array('DESKRIPSI', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KODE_DIAG, DESKRIPSI, CREATED', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KODE_DIAG' => 'Kode Diag',
			'DESKRIPSI' => 'Deskripsi',
			'CREATED' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KODE_DIAG',$this->KODE_DIAG,true);
		$criteria->compare('DESKRIPSI',$this->DESKRIPSI,true);
		$criteria->compare('CREATED',$this->CREATED,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchDiagAwal($q)
	{
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('DESKRIPSI',$q,true,'OR');
		$criteria->addSearchCondition('KODE_DIAG',$q,true,'OR');
		$criteria->limit = 20;
		
		return BpjsDiagnosis::model()->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BpjsDiagnosis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
