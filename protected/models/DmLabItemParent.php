<?php

/**
 * This is the model class for table "dm_lab_item_parent".
 *
 * The followings are the available columns in table 'dm_lab_item_parent':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $item_id
 * @property integer $level
 * @property double $batas_bawah
 * @property double $batas_bawah_kritis
 * @property double $batas_atas
 * @property double $batas_atas_kritis
 * @property double $batas_bawah_p
 * @property double $batas_bawah_kritis_p
 * @property double $batas_atas_p
 * @property double $batas_atas_kritis_p
 * @property string $batas_dropdown
 * @property string $dropdown_value
 * @property string $metode
 * @property string $satuan
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property DmLabItem $parent
 * @property DmLabItem $item
 */
class DmLabItemParent extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_lab_item_parent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent_id, item_id', 'required'),
			array('parent_id, item_id, level', 'numerical', 'integerOnly'=>true),
			array('batas_bawah, batas_bawah_kritis, batas_atas, batas_atas_kritis, batas_bawah_p, batas_bawah_kritis_p, batas_atas_p, batas_atas_kritis_p', 'numerical'),
			array('batas_dropdown', 'length', 'max'=>50),
			array('dropdown_value, metode, satuan', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, item_id, level, batas_bawah, batas_bawah_kritis, batas_atas, batas_atas_kritis, batas_bawah_p, batas_bawah_kritis_p, batas_atas_p, batas_atas_kritis_p, batas_dropdown, dropdown_value, metode, satuan, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'DmLabItem', 'parent_id','alias'=>'p'),
			'item' => array(self::BELONGS_TO, 'DmLabItem', 'item_id','alias'=>'i'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'item_id' => 'Item',
			'batas_bawah' => 'Batas Bawah',
			'batas_bawah_kritis' => 'Batas Bawah Kritis',
			'batas_atas' => 'Batas Atas',
			'batas_atas_kritis' => 'Batas Atas Kritis',
			'batas_bawah_p' => 'Batas Bawah P',
			'batas_bawah_kritis_p' => 'Batas Bawah Kritis P',
			'batas_atas_p' => 'Batas Atas P',
			'batas_atas_kritis_p' => 'Batas Atas Kritis P',
			'batas_dropdown' => 'Batas Dropdown',
			'dropdown_value' => 'Dropdown',
			'metode' => 'Metode',
			'satuan' => 'Satuan',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$sort->attributes = [
			
			'parentName'=>[
				'asc'=>'p.nama',
				'desc'=>'p.nama desc',
			],
			'itemName'=>[
				'asc'=>'i.nama',
				'desc'=>'i.nama desc',
			],
			'satuan'=>[
				'asc'=>'t.satuan',
				'desc'=>'t.satuan desc',
			],
			'batas_bawah'=>[
				'asc'=>'t.batas_bawah',
				'desc'=>'t.batas_bawah desc',
			],
			'batas_atas'=>[
				'asc'=>'t.batas_atas',
				'desc'=>'t.batas_atas desc',
			],
			'batas_bawah_kritis'=>[
				'asc'=>'t.batas_bawah_kritis',
				'desc'=>'t.batas_bawah_kritis desc',
			],
			'batas_atas_kritis'=>[
				'asc'=>'t.batas_atas_kritis',
				'desc'=>'t.batas_atas_kritis desc',
			],
		];

		$criteria->addSearchCondition('p.nama',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('i.nama',$this->SEARCH,true,'OR');
		$criteria->with = ['parent','item'];

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function searchAll()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = ['parent'];
		$criteria->together = true;
		$criteria->order = 'p.nama ASC';
		return DmLabItemParent::model()->findAll($criteria);
	}

	public function getParentName()
	{
		return $this->parent->nama;
	}

	public function getItemName()
	{
		return $this->item->nama;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmLabItemParent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
