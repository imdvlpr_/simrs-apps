<?php

/**
 * This is the model class for table "bpjs_vclaim".
 *
 * The followings are the available columns in table 'bpjs_vclaim':
 * @property integer $id
 * @property string $noSep
 * @property string $tglMasuk
 * @property string $tglKeluar
 * @property integer $jaminan
 * @property string $poli
 * @property string $ruangRawat
 * @property string $kelasRawat
 * @property string $spesialistik
 * @property string $caraKeluar
 * @property string $kondisiPulang
 * @property string $tindakLanjut
 * @property string $kodePPK
 * @property string $tglKontrol
 * @property string $kode_poli
 * @property string $DPJP
 * @property string $user
 * @property string $kode_lpk
 * @property string $created
 *
 * The followings are the available model relations:
 * @property BpjsVclaimDiagnosa[] $bpjsVclaimDiagnosas
 * @property BpjsVclaimProcedure[] $bpjsVclaimProcedures
 */
class BpjsVclaim extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs_vclaim';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('noSep, tglMasuk, tglKeluar, jaminan, poli, ruangRawat, kelasRawat, spesialistik, caraKeluar, kondisiPulang, tindakLanjut, kodePPK, tglKontrol, kode_poli, DPJP, user', 'required'),
			array('jaminan', 'numerical', 'integerOnly'=>true),
			array('noSep, poli', 'length', 'max'=>50),
			array('ruangRawat, kelasRawat, spesialistik, caraKeluar, kondisiPulang', 'length', 'max'=>20),
			array('tindakLanjut, kodePPK, kode_poli, DPJP, user, kode_lpk', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, noSep, tglMasuk, tglKeluar, jaminan, poli, ruangRawat, kelasRawat, spesialistik, caraKeluar, kondisiPulang, tindakLanjut, kodePPK, tglKontrol, kode_poli, DPJP, user, kode_lpk, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bpjsVclaimDiagnosas' => array(self::HAS_MANY, 'BpjsVclaimDiagnosa', 'id_bpjs_vclaim'),
			'bpjsVclaimProcedures' => array(self::HAS_MANY, 'BpjsVclaimProcedure', 'id_bpjs_vclaim'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'noSep' => 'No Sep',
			'tglMasuk' => 'Tgl Masuk',
			'tglKeluar' => 'Tgl Keluar',
			'jaminan' => 'Jaminan',
			'poli' => 'Poli',
			'ruangRawat' => 'Ruang Rawat',
			'kelasRawat' => 'Kelas Rawat',
			'spesialistik' => 'Spesialistik',
			'caraKeluar' => 'Cara Keluar',
			'kondisiPulang' => 'Kondisi Pulang',
			'tindakLanjut' => 'Tindak Lanjut',
			'kodePPK' => 'Dirujuk ke PPK',
			'tglKontrol' => 'Tgl Kontrol',
			'kode_poli' => 'Poli Kontrol',
			'DPJP' => 'DPJP',
			'user' => 'User',
			'kode_lpk' => 'Kode Lpk',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('noSep',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tglMasuk',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tglKeluar',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jaminan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('poli',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('ruangRawat',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kelasRawat',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('spesialistik',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('caraKeluar',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kondisiPulang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tindakLanjut',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kodePPK',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tglKontrol',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kode_poli',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('DPJP',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('user',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kode_lpk',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BpjsVclaim the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
