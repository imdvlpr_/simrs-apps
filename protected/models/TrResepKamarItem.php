<?php

/**
 * This is the model class for table "tr_resep_kamar_item".
 *
 * The followings are the available columns in table 'tr_resep_kamar_item':
 * @property integer $id
 * @property string $obat_id
 * @property double $jumlah
 * @property double $harga
 * @property integer $tr_resep_kamar_id
 * @property double $signa1
 * @property double $signa2
 * @property integer $hari
 * @property double $jml_ke_apotik
 * @property double $jml_ke_bpjs
 * @property double $dosis_permintaan
 * @property string $created
 * @property double $jml_stok
 *
 * The followings are the available model relations:
 * @property MObatAkhp $obat
 * @property TrResepKamar $trResepKamar
 */
class TrResepKamarItem extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_resep_kamar_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('obat_id, jumlah, harga, tr_resep_kamar_id, jml_stok', 'required'),
			array('tr_resep_kamar_id, hari', 'numerical', 'integerOnly'=>true),
			array('jumlah, harga, signa1, signa2, jml_ke_apotik, jml_ke_bpjs, dosis_permintaan, jml_stok', 'numerical'),
			array('obat_id', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, obat_id, jumlah, harga, tr_resep_kamar_id, signa1, signa2, hari, jml_ke_apotik, jml_ke_bpjs, dosis_permintaan, created, jml_stok', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'obat' => array(self::BELONGS_TO, 'MObatAkhp', 'obat_id'),
			'trResepKamar' => array(self::BELONGS_TO, 'TrResepKamar', 'tr_resep_kamar_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'obat_id' => 'Obat',
			'jumlah' => 'Jumlah',
			'harga' => 'Harga',
			'tr_resep_kamar_id' => 'Tr Resep Kamar',
			'signa1' => 'Signa1',
			'signa2' => 'Signa2',
			'hari' => 'Hari',
			'jml_ke_apotik' => 'Jml Ke Apotik',
			'jml_ke_bpjs' => 'Jml Ke Bpjs',
			'dosis_permintaan' => 'Dosis Permintaan',
			'created' => 'Created',
			'jml_stok' => 'Jml Stok',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($tr_resep_kamar_id = 0)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('obat_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jumlah',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('harga',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('signa1',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('signa2',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('hari',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jml_ke_apotik',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jml_ke_bpjs',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dosis_permintaan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jml_stok',$this->SEARCH,true,'OR');

		if(!empty($tr_resep_kamar_id))
			$criteria->compare('tr_resep_kamar_id',$tr_resep_kamar_id);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrResepKamarItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
