<?php

/**
 * This is the model class for table "pasien".
 *
 * The followings are the available columns in table 'pasien':
 * @property integer $NoMedrec
 * @property string $NAMA
 * @property string $ALAMAT
 * @property integer $KodeKec
 * @property string $TMPLAHIR
 * @property string $TGLLAHIR
 * @property string $PEKERJAAN
 * @property string $AGAMA
 * @property string $JENSKEL
 * @property string $GOLDARAH
 * @property string $TELP
 * @property string $JENISIDENTITAS
 * @property string $NOIDENTITAS
 * @property string $STATUSPERKAWINAN
 * @property integer $BeratLahir
 * @property string $Desa
 * @property integer $KodeGol
 * @property string $TglInput
 * @property string $created
 * @property string $AlmIp
 * @property integer $NoMedrecLama
 * @property string $NoKpst
 * @property integer $KodePisa
 * @property string $KdPPK
 * @property string $NamaOrtu
 * @property string $NamaSuamiIstri
 */
class Pasien extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $CEKBY = 0;
	public $KecNama = '';
	public $KotaNama = '';
	public $FULLADDRESS = '';
	public $NAMAAGAMA = '';
	public $NAMAGOLDARAH = '';
	public $rt, $rw, $jalan = '';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'a_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//
			array('NoMedrec,NAMA, Desa,TGLLAHIR, JENSKEL,NOIDENTITAS,ALAMAT', 'required','message'=>'{attribute} harus diisi.'),
			array('KodeKec, KodeGol, NoMedrecLama, KodePisa', 'numerical', 'integerOnly'=>true),
			array('PEKERJAAN, NoKpst, NamaOrtu, NamaSuamiIstri', 'length', 'max'=>30),
			array('BeratLahir','numerical'),
			array('TMPLAHIR', 'length', 'max'=>15),

			array('NOIDENTITAS','unique', 'message'=>'No Identitas ini sudah dipakai.'),
			array('NOIDENTITAS', 'length', 'max'=>25),
			array('JENSKEL', 'length', 'max'=>1),
			array('GOLDARAH', 'length', 'max'=>10),
			array('TELP, Desa', 'length', 'max'=>40),
			array('AlmIp', 'length', 'max'=>20),
			array('KdPPK', 'length', 'max'=>8),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NoMedrec, NAMA,  ALAMAT,KodeKec, TMPLAHIR, TGLLAHIR, PEKERJAAN, AGAMA, JENSKEL, GOLDARAH, TELP, JENISIDENTITAS, NOIDENTITAS, STATUSPERKAWINAN, BeratLahir, Desa, KodeGol, TglInput, created, AlmIp, NoMedrecLama, NoKpst, KodePisa, KdPPK, NamaOrtu, NamaSuamiIstri', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'kOTA' => array(self::BELONGS_TO, 'Refkabkota', 'Kota'),
			// 'kEC' => array(self::BELONGS_TO, 'Refkec', 'Kecamatan'),
			'bpjsPasien' => array(self::HAS_MANY, 'BpjsPasien', 'PASIEN_ID'),
			'bpjsSeps' => array(self::HAS_MANY, 'BpjsSep', 'NO_RM'),
			'trPendaftaranRjalans' => array(self::HAS_MANY, 'TrPendaftaranRjalan', 'NoMedrec'),
			'trRawatInaps' => array(self::HAS_MANY, 'TrRawatInap', 'pasien_id'),
			'pasienAlamat' => array(self::HAS_ONE, 'PasienAlamat', 'pasien_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NoMedrec' => 'No RM',
			
			'NAMA' => 'Nama',
			'ALAMAT' => 'Alamat',
			// 'JALAN' => 'Jalan',
			'KodeKec' => 'Kecamatan',
			'TMPLAHIR' => 'Tmp Lahir',
			'TGLLAHIR' => 'Tgl Lahir',
			'PEKERJAAN' => 'Pekerjaan',
			'AGAMA' => 'Agama',
			'JENSKEL' => 'Jenis Kelamin',
			'GOLDARAH' => 'Gol Darah',
			'TELP' => 'Telp',
			'JENISIDENTITAS' => 'Jenis Identitas',
			'NOIDENTITAS' => 'No Identitas',
			'STATUSPERKAWINAN' => 'Status Kawin',
			'BeratLahir' => 'Berat',
			// 'RT' => 'RT',
			// 'RW' => 'RW',
			'Desa' => 'Desa',
			// 'Kecamatan' => 'Kecamatan',
   //          'Kota' => 'Kab - Prov',
			'KodeGol' => 'Kode Gol',
			'TglInput' => 'Tgl Input',
			'created' => 'Jam Input',
			'AlmIp' => 'Alm Ip',
			'NoMedrecLama' => 'No Medrec Lama',
			'NoKpst' => 'No Kepesertaan',
			'KodePisa' => 'Kode Pisa',
			'KdPPK' => 'Kd Ppk',
			'NamaOrtu' => 'Nama Ayah',
			'NamaSuamiIstri' => 'Nama Suami/Istri',
			// 'CETAK_KARTU' => 'Status Cetak',
		);
	}

	public function searchPasienBy()
	{

		$param = $this->CEKBY;
		$result = array();
		switch($param)
		{
			case 0 :
				$result = $this->searchByNama($this->SEARCH);
				
				break;
			
			case 1 : 
			case 2 :
			case 3 :
				$result = $this->searchByNomor($this->SEARCH);
				break;
		}

		return $result;
	}

	private function searchByNama($q)
	{
		$sort = new CSort();
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('NAMA',$q,true,'OR');
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
		
	}

	private function searchByNomor($q)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$sort = new CSort();
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('NoMedrec',$q,true,'OR');
		$criteria->addSearchCondition('NOIDENTITAS',$q,true,'OR');
		$criteria->addSearchCondition('bpjsPasien.NoKartu',$q,true,'OR');
		$criteria->with = array('bpjsPasien');
		$criteria->together = true;
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
		
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$sort = new CSort();
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('NoMedrec',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NAMA',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('DESA',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('TMPLAHIR',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('PEKERJAAN',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NamaOrtu',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NamaSuamiIstri',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
	}

	

	protected function afterFind()
	{

		$this->NoMedrec = Yii::app()->helper->appendZeros($this->NoMedrec, 6);
			


		// $this->JENSKEL = ($this->JENSKEL == 'L' ? 'Laki-Laki' : 'Perempuan');
		$this->NAMA = strtoupper($this->NAMA);
		$this->NamaSuamiIstri = strtoupper($this->NamaSuamiIstri);
		// $this->JALAN = strtoupper($this->JALAN);
		$this->Desa = strtoupper($this->Desa);
		
		// if(!empty($this->NamaKec))
		// 	$this->FULLADDRESS = strtoupper($this->Desa.' '.$this->ALAMAT);
		// else
		$this->FULLADDRESS = strtoupper($this->ALAMAT);
		return parent::afterFind();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
