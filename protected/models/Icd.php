<?php

/**
 * This is the model class for table "m_icd".
 *
 * The followings are the available columns in table 'm_icd':
 * @property string $kode
 * @property string $deskripsi
 * @property string $dtd_kode
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property MDtd $dtdKode
 */
class Icd extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'm_icd';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode', 'required'),
			array('kode, dtd_kode', 'length', 'max'=>100),
			array('deskripsi', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kode, deskripsi, dtd_kode, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dtdKode' => array(self::BELONGS_TO, 'Dtd', 'dtd_kode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kode' => 'Kode',
			'deskripsi' => 'Deskripsi',
			'dtd_kode' => 'Dtd Kode',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('kode',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('deskripsi',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dtd_kode',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function searchDiagAwal($q)
	{
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('kode',$q,true,'OR');
		$criteria->addSearchCondition('deskripsi',$q,true,'OR');
		$criteria->limit = 20;
		
		return Icd::model()->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Icd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
