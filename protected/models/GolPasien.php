<?php

/**
 * This is the model class for table "gol_pasien".
 *
 * The followings are the available columns in table 'gol_pasien':
 * @property integer $KodeGol
 * @property string $NamaGol
 * @property integer $a_kpid
 * @property string $Inisial
 * @property integer $KodeKlsHak
 * @property integer $JenisKlsHak
 * @property integer $KodeAturan
 * @property string $NoAwal
 * @property integer $IsPBI
 * @property integer $MblAmbGratis
 * @property integer $MblJnhGratis
 * @property integer $KDJNSKPST
 * @property integer $KDJNSPESERTA
 * @property integer $IsKaryawan
 */
class GolPasien extends CActiveRecord
{
	public $SEARCH;
	public $PAGE_SIZE = 10;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'a_golpasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('a_kpid, KodeKlsHak, JenisKlsHak, KodeAturan, IsPBI, MblAmbGratis, MblJnhGratis, KDJNSKPST, KDJNSPESERTA, IsKaryawan', 'numerical', 'integerOnly'=>true),
			array('NamaGol', 'length', 'max'=>80),
			array('Inisial', 'length', 'max'=>2),
			array('NoAwal', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KodeGol, NamaGol, a_kpid, Inisial, KodeKlsHak, JenisKlsHak, KodeAturan, NoAwal, IsPBI, MblAmbGratis, MblJnhGratis, KDJNSKPST, KDJNSPESERTA, IsKaryawan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trRawatInaps' => array(self::HAS_MANY, 'TrRawatInap', 'jenis_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KodeGol' => 'Kode Gol',
			'NamaGol' => 'Nama Gol',
			'a_kpid' => 'A Kpid',
			'Inisial' => 'Inisial',
			'KodeKlsHak' => 'Kode Kls Hak',
			'JenisKlsHak' => 'Jenis Kls Hak',
			'KodeAturan' => 'Kode Aturan',
			'NoAwal' => 'No Awal',
			'IsPBI' => 'Is Pbi',
			'MblAmbGratis' => 'Mbl Amb Gratis',
			'MblJnhGratis' => 'Mbl Jnh Gratis',
			'KDJNSKPST' => 'Kdjnskpst',
			'KDJNSPESERTA' => 'Kdjnspeserta',
			'IsKaryawan' => 'Is Karyawan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('KodeGol',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NamaGol',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('a_kpid',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('Inisial',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('KodeKlsHak',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('JenisKlsHak',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('KodeAturan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NoAwal',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('IsPBI',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('MblAmbGratis',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('MblJnhGratis',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('KDJNSKPST',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('IsKaryawan',$this->SEARCH,true,'OR');

		$criteria->addCondition('IsKaryawan',1);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchUmum()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->addSearchCondition('KodeGol',1001,true,'OR');
		$criteria->addSearchCondition('KodeGol',1002,true,'OR');
		$criteria->addSearchCondition('KodeGol',2401,true,'OR');
		

		return $this->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GolPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
