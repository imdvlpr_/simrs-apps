<?php

/**
 * This is the model class for table "dm_dokter".
 *
 * The followings are the available columns in table 'dm_dokter':
 * @property integer $id_dokter
 * @property string $nama_depan
 * @property string $nama_tengah
 * @property string $nama_belakang
 * @property string $gelar_depan
 * @property string $gelar_belakang
 * @property integer $jenis_dokter
 * @property double $prosentasi_jasa
 * @property string $alamat_praktik
 * @property string $telp
 * @property string $created
 */
class DmDokter extends CActiveRecord
{

	public $FULLNAME = '';
	public $NICKNAME = '';
	public $SEARCH;
	public $PAGE_SIZE = 10;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_dokter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_dokter, jenis_dokter, prosentasi_jasa, alamat_praktik, telp, nama_panggilan', 'required','message'=>'{attribute} harus diisi.'),
			array('prosentasi_jasa', 'numerical'),
			array('alamat_praktik', 'length', 'max'=>255),
			array('telp', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_dokter, nama_dokter, jenis_dokter, prosentasi_jasa, alamat_praktik, telp, created, nama_panggilan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tdOkBiayas' => array(self::HAS_MANY, 'TdOkBiaya', 'dr_operator'),
			'dmDokterKelases' => array(self::HAS_MANY, 'DmDokterKelas', 'dokter_id'),
			'trPendaftaranRjalans' => array(self::HAS_MANY, 'TrPendaftaranRjalan', 'dokter_id'),
			'trRawatInaps' => array(self::HAS_MANY, 'TrRawatInap', 'dokter_id'),
			'trRawatInapRincians' => array(self::HAS_MANY, 'TrRawatInapRincian', 'dokter_ird'),
			'trRawatInapTops' => array(self::HAS_MANY, 'TrRawatInapTop', 'id_dokter_jm'),
			'trRawatInapTops1' => array(self::HAS_MANY, 'TrRawatInapTop', 'id_dokter_anas'),
			'trRawatInapVisiteDokters' => array(self::HAS_MANY, 'TrRawatInapVisiteDokter', 'id_dokter'),
			'trRawatInapVisiteDokters1' => array(self::HAS_MANY, 'TrRawatInapVisiteDokter', 'id_dokter_irna'),
			'trRawatJalanRincians' => array(self::HAS_MANY, 'TrRawatJalanRincian', 'dokter_ird'),
			'trRawatJalanVisiteDokters' => array(self::HAS_MANY, 'TrRawatJalanVisiteDokter', 'id_dokter'),
			'trRawatInapTops' => array(self::HAS_MANY, 'TrRawatInapTop', 'id_dokter_jm'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_dokter' => 'Id Dokter',
			'nama_dokter' => 'Nama Dokter',
			'jenis_dokter' => 'Jenis Dokter',
			'prosentasi_jasa' => 'Prosentasi Jasa',
			'alamat_praktik' => 'Alamat Praktik',
			'telp' => 'Telp',
			'created' => 'Created',
			'nama_panggilan' => 'Nama Panggilan'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$sort = new CSort();
		$criteria=new CDbCriteria;

		$criteria->addSearchCondition('nama_dokter',$this->SEARCH,true,'OR');


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));

		// $criteria=new CDbCriteria;
		//
		// $criteria->compare('id_dokter',$this->id_dokter);
		// $criteria->compare('nama_dokter',$this->nama_dokter,true);
		// $criteria->compare('jenis_dokter',$this->jenis_dokter);
		// $criteria->compare('prosentasi_jasa',$this->prosentasi_jasa);
		// $criteria->compare('alamat_praktik',$this->alamat_praktik,true);
		// $criteria->compare('telp',$this->telp,true);
		// $criteria->compare('created',$this->created,true);
		//
		// return new CActiveDataProvider($this, array(
		// 	'criteria'=>$criteria,
		// ));
	}

	public function searchByNama($q)
	{
	
		$criteria=new CDbCriteria;
	
		$criteria->addSearchCondition('nama_dokter',$q,true,'OR');
		$criteria->limit = 10;
		$criteria->order = 'nama_dokter ASC';
	
		return DmDokter::model()->findAll($criteria);
	
	}
	
	protected function afterFind()
	{
	
		$this->FULLNAME = $this->nama_dokter;
	
		$fname = explode(' ',trim($this->nama_dokter));
		$this->NICKNAME = $this->nama_panggilan;
	
		return parent::afterFind();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmDokter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
