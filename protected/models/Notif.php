<?php

/**
 * This is the model class for table "notif".
 *
 * The followings are the available columns in table 'notif':
 * @property integer $id
 * @property string $keterangan
 * @property integer $unit_from_id
 * @property integer $unit_to_id
 * @property integer $is_read_from
 * @property integer $is_read_to
 * @property integer $is_hapus
 * @property integer $item_id
 * @property string $created_at
 * @property string $updated_at
 */
class Notif extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('keterangan, unit_from_id, unit_to_id', 'required'),
			array('unit_from_id, unit_to_id, is_read_from, is_read_to, is_hapus, item_id', 'numerical', 'integerOnly'=>true),
			array('keterangan', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, keterangan, unit_from_id, unit_to_id, is_read_from, is_read_to, is_hapus, item_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'keterangan' => 'Keterangan',
			'unit_from_id' => 'Unit From',
			'unit_to_id' => 'Unit To',
			'is_read_from' => 'Is Read From',
			'is_read_to' => 'Is Read To',
			'is_hapus' => 'Is Hapus',
			'item_id' => 'Item',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	public static function getLatestNotif()
    {
        $notif = Notif::model()->findByAttributes([
        	'unit_to_id' => Yii::app()->user->unit,
        	'is_read_to' => '0'
        ],['order'=>'created_at DESC']);
        return $notif;
    }

	public static function countNotif()
    {
        $notif = Notif::model()->findAllByAttributes([
        	'unit_to_id' => Yii::app()->user->unit,
        	'is_read_to' => '0'
        ]);
        $total = count($notif);
        return $total;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('keterangan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('unit_from_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('unit_to_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_read_from',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_read_to',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_hapus',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('item_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Notif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
