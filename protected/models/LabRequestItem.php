<?php

/**
 * This is the model class for table "lab_request_item".
 *
 * The followings are the available columns in table 'lab_request_item':
 * @property integer $id
 * @property integer $request_id
 * @property integer $parent_id
 * @property double $bhp
 * @property double $japel
 * @property double $biaya
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property LabRequest $request
 * @property DmLabItemParent $parent
 * @property LabRequestItemKomponen[] $labRequestItemKomponens
 */
class LabRequestItem extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $pasien_id ;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lab_request_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('request_id', 'required'),
			array('request_id, parent_id', 'numerical', 'integerOnly'=>true),
			array('bhp, japel, biaya', 'numerical'),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, request_id, parent_id, bhp, japel, biaya, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'request' => array(self::BELONGS_TO, 'LabRequest', 'request_id'),
			'parent' => array(self::BELONGS_TO, 'DmLabItemParent', 'parent_id'),
			'labRequestItemKomponens' => array(self::HAS_MANY, 'LabRequestItemKomponen', 'lab_request_item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'request_id' => 'Request',
			'parent_id' => 'Parent',
			'bhp' => 'Bhp',
			'japel' => 'Japel',
			'biaya' => 'Biaya',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');
		
		$criteria->addCondition('request.pasien_id = '.$this->pasien_id);
		$criteria->with = ['request'];
		$criteria->together = true;

		if(!empty($this->request_id))
		{
			$criteria->addCondition('request_id = '.$this->request_id);
		}


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function countTindakan()
	{
		$criteria = new CDbCriteria; 
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->addBetweenCondition('request.tanggal', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR);
		$criteria->with = array('request');
		$criteria->order = 'request.tanggal ASC';
		$criteria->together = true;
		$count = count(LabRequestItem::model()->findAll($criteria));
		return $count;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LabRequestItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
