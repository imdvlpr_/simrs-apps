<?php

/**
 * This is the model class for table "bpjs_ppk".
 *
 * The followings are the available columns in table 'bpjs_ppk':
 * @property integer $ID_PPK
 * @property string $KODE_KC
 * @property string $KODE_PPK
 * @property string $NAMA_PPK
 * @property string $DATI2PPK
 * @property string $CREATED
 */
class BpjsPpk extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs_ppk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODE_KC, KODE_PPK, NAMA_PPK, DATI2PPK, CREATED', 'required'),
			array('KODE_KC, KODE_PPK', 'length', 'max'=>10),
			array('NAMA_PPK, DATI2PPK', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID_PPK, KODE_KC, KODE_PPK, NAMA_PPK, DATI2PPK, CREATED', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_PPK' => 'Id Ppk',
			'KODE_KC' => 'Kode Kc',
			'KODE_PPK' => 'Kode Ppk',
			'NAMA_PPK' => 'Nama Ppk',
			'DATI2PPK' => 'Dati2 Ppk',
			'CREATED' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_PPK',$this->ID_PPK);
		$criteria->compare('KODE_KC',$this->KODE_KC,true);
		$criteria->compare('KODE_PPK',$this->KODE_PPK,true);
		$criteria->compare('NAMA_PPK',$this->NAMA_PPK,true);
		$criteria->compare('DATI2PPK',$this->DATI2PPK,true);
		$criteria->compare('CREATED',$this->CREATED,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchPpkJatim($q)
	{
		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('NAMA_PPK',$q,true,'AND');
		// $criteria->addSearchCondition('KODE_KC','13',true,'AND');
		$criteria->limit = 20;
		
		return BpjsPpk::model()->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BpjsPpk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
