<?php

/**
 * This is the model class for table "jenis_visite".
 *
 * The followings are the available columns in table 'jenis_visite':
 * @property integer $id_jenis_visite
 * @property string $nama_visite
 * @property string $created
 *
 * The followings are the available model relations:
 * @property TrRawatInapVisiteDokter[] $trRawatInapVisiteDokters
 */
class JenisVisite extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenis_visite';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_visite,kode_visite, kelas_tingkat, biaya', 'required'),
			array('nama_visite', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jenis_visite, nama_visite, kode_visite, kelas_tingkat, biaya, created, urutan, is_deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trRawatInapVisiteDokters' => array(self::HAS_MANY, 'TrRawatInapVisiteDokter', 'id_jenis_visite'),
			'kelas' => array(self::BELONGS_TO, 'DmKelas', 'kelas_tingkat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jenis_visite' => 'Id Jenis Visite',
			'nama_visite' => 'Nama Visite',
			'kode_visite' => 'Kode Visite',
			'biaya' => 'Biaya',
			'kelas_tingkat' => 'Tingkat Kelas',
			'created' => 'Created',
			'is_deleted' => 'Hapus'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$sort = new CSort();
		
		$criteria->addSearchCondition('nama_visite',$this->SEARCH,true,'OR');

		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
	}

	public static function getBiaya($nama_visite, $kelas_id){
		$model = JenisVisite::model()->findByAttributes([
			'kelas_tingkat'=>$kelas_id,
			'nama_visite' => $nama_visite
		]);
		return $model->biaya;
	} 

	public function searchByNama($q,$tingkat)
	{

		$criteria=new CDbCriteria;
		
		$criteria->addSearchCondition('nama_visite',$q,true,'OR');
		$criteria->addSearchCondition('kelas_tingkat',$tingkat,true,'AND');
		$criteria->addCondition('is_deleted = 0');

		return JenisVisite::model()->findAll($criteria);
		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JenisVisite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
