<?php

/**
 * This is the model class for table "dm_kamar_master".
 *
 * The followings are the available columns in table 'dm_kamar_master':
 * @property integer $id
 * @property string $nama_kamar
 * @property string $kode_kamar
 *
 * The followings are the available model relations:
 * @property DmKamar[] $dmKamars
 */
class DmKamarMaster extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_kamar_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_kamar,unit_id', 'required'),
			array('nama_kamar', 'length', 'max'=>50),
			array('kode_kamar', 'length', 'max'=>20),
			array('kode_kamar, nama_kamar','unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_kamar, kode_kamar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dmKamars' => array(self::HAS_MANY, 'DmKamar', 'kamar_master_id'),
			'unit' => array(self::BELONGS_TO, 'Unit', 'unit_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'unit_id' => 'Unit',
			'nama_kamar' => 'Nama Kamar',
			'kode_kamar' => 'Kode Kamar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama_kamar',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kode_kamar',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmKamarMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
