<?php

/**
 * This is the model class for table "dm_dokter_kelas".
 *
 * The followings are the available columns in table 'dm_dokter_kelas':
 * @property integer $id_dokter_kelas
 * @property integer $kelas_id
 * @property integer $dokter_id
 * @property double $biaya_dokter
 * @property string $created
 *
 * The followings are the available model relations:
 * @property DmDokter $dokter
 * @property DmKelas $kelas
 */
class DmDokterKelas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_dokter_kelas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kelas_id, dokter_id, biaya_dokter, created', 'required'),
			array('kelas_id, dokter_id', 'numerical', 'integerOnly'=>true),
			array('biaya_dokter', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_dokter_kelas, kelas_id, dokter_id, biaya_dokter, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dokter' => array(self::BELONGS_TO, 'DmDokter', 'dokter_id'),
			'kelas' => array(self::BELONGS_TO, 'DmKelas', 'kelas_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_dokter_kelas' => 'Id Dokter Kelas',
			'kelas_id' => 'Kelas',
			'dokter_id' => 'Dokter',
			'biaya_dokter' => 'Biaya Dokter',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_dokter_kelas',$this->id_dokter_kelas);
		$criteria->compare('kelas_id',$this->kelas_id);
		$criteria->compare('dokter_id',$this->dokter_id);
		$criteria->compare('biaya_dokter',$this->biaya_dokter);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmDokterKelas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
