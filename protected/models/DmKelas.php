<?php

/**
 * This is the model class for table "dm_kelas".
 *
 * The followings are the available columns in table 'dm_kelas':
 * @property integer $id_kelas
 * @property string $kode_kelas
 * @property string $nama_kelas
 * @property string $kode_kelas_bpjs
 *
 * The followings are the available model relations:
 * @property DmDokterKelas[] $dmDokterKelases
 * @property DmKamar[] $dmKamars
 * @property ObatAlkes[] $obatAlkes
 * @property TarifAlat[] $tarifAlats
 * @property TindakanMedisAll[] $tindakanMedisAlls
 * @property TindakanMedisLain[] $tindakanMedisLains
 * @property TindakanMedisNonOperatif[] $tindakanMedisNonOperatifs
 * @property TindakanMedisOperatif[] $tindakanMedisOperatifs
 * @property TindakanMedisOperatifKhusus[] $tindakanMedisOperatifKhususes
 * @property TindakanMedisPenunjang[] $tindakanMedisPenunjangs
 */
class DmKelas extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_kelas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_kelas, nama_kelas, biaya_rr_askep, biaya_rr_monitor', 'required'),
			array('kode_kelas', 'length', 'max'=>5),
			array('nama_kelas', 'length', 'max'=>100),
			array('kode_kelas_bpjs', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_kelas, kode_kelas, nama_kelas, kode_kelas_bpjs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dmDokterKelases' => array(self::HAS_MANY, 'DmDokterKelas', 'kelas_id'),
			'dmKamars' => array(self::HAS_MANY, 'DmKamar', 'kelas_id'),
			'obatAlkes' => array(self::HAS_MANY, 'ObatAlkes', 'kelas_id'),
			'tarifAlats' => array(self::HAS_MANY, 'TarifAlat', 'kelas_id'),
			'tindakanMedisAlls' => array(self::HAS_MANY, 'TindakanMedisAll', 'kelas_id'),
			'tindakanMedisLains' => array(self::HAS_MANY, 'TindakanMedisLain', 'kelas_id'),
			'tindakanMedisNonOperatifs' => array(self::HAS_MANY, 'TindakanMedisNonOperatif', 'kelas_id'),
			'tindakanMedisOperatifs' => array(self::HAS_MANY, 'TindakanMedisOperatif', 'kelas_id'),
			'tindakanMedisOperatifKhususes' => array(self::HAS_MANY, 'TindakanMedisOperatifKhusus', 'kelas_id'),
			'tindakanMedisPenunjangs' => array(self::HAS_MANY, 'TindakanMedisPenunjang', 'kelas_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kelas' => 'Id Kelas',
			'kode_kelas' => 'Kode Kelas',
			'nama_kelas' => 'Nama Kelas',
			'kode_kelas_bpjs' => 'Kode Kelas Bpjs',
			'biaya_rr_monitor' => 'Biaya RR Monitor',
			'biaya_rr_askep' => 'Biaya RR Askep'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id_kelas',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kode_kelas',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama_kelas',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kode_kelas_bpjs',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmKelas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
