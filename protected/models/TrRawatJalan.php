<?php

/**
 * This is the model class for table "tr_rawat_jalan".
 *
 * The followings are the available columns in table 'tr_rawat_jalan':
 * @property integer $id
 * @property double $reg_id
 * @property double $reg_jalan_id
 * @property integer $no_medrec
 * @property integer $unit_id
 * @property string $tanggal
 * @property integer $unit_tujuan_id
 * @property integer $status_rujukan
 * @property string $keterangan
 * @property string $tanggal_rujukan
 * @property integer $is_locked
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property BPendaftaran $reg
 * @property APasien $noMedrec
 * @property AUnit $unit
 * @property ARefcarapulangdtl $statusRujukan
 * @property BPendaftaranRjalan $regJalan
 * @property TrRawatJalanTindakan[] $trRawatJalanTindakans
 */
class TrRawatJalan extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_jalan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reg_id, no_medrec, unit_id, tanggal', 'required'),
			array('no_medrec, unit_id, unit_tujuan_id, status_rujukan, is_locked', 'numerical', 'integerOnly'=>true),
			array('reg_id, reg_jalan_id', 'numerical'),
			array('keterangan', 'length', 'max'=>255),
			array('tanggal_rujukan, created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, reg_id, reg_jalan_id, no_medrec, unit_id, tanggal, unit_tujuan_id, status_rujukan, keterangan, tanggal_rujukan, is_locked, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reg' => array(self::BELONGS_TO, 'BPendaftaran', 'reg_id'),
			'noMedrec' => array(self::BELONGS_TO, 'Pasien', 'no_medrec'),
			'unit' => array(self::BELONGS_TO, 'Unit', 'unit_id'),
			'statusRujukan' => array(self::BELONGS_TO, 'Refcarapulangdtl', 'status_rujukan'),
			'regJalan' => array(self::BELONGS_TO, 'BPendaftaranRjalan', 'reg_jalan_id'),
			'trRawatJalanTindakans' => array(self::HAS_MANY, 'TrRawatJalanTindakan', 'tr_rawat_jalan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'reg_id' => 'Reg',
			'reg_jalan_id' => 'Reg Jalan',
			'no_medrec' => 'No Medrec',
			'unit_id' => 'Unit',
			'tanggal' => 'Tanggal',
			'unit_tujuan_id' => 'Unit Tujuan',
			'status_rujukan' => 'Status Rujukan',
			'keterangan' => 'Keterangan',
			'tanggal_rujukan' => 'Tanggal Rujukan',
			'is_locked' => 'Is Locked',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('reg_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('no_medrec',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('unit_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tanggal',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('unit_tujuan_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('status_rujukan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('keterangan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tanggal_rujukan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_locked',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');

		$unitUser = !empty(Yii::app()->user->getState('unit')) ? Yii::app()->user->getState('unit') : 0;
		$criteria->compare('unit_id',$unitUser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatJalan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave()
	{
		
		$this->tanggal = date('Y-m-d',strtotime($this->tanggal));
		$this->tanggal_rujukan = !empty($this->tanggal_rujukan) ? date('Y-m-d',strtotime($this->tanggal_rujukan)) : date('Y-m-d');
		return parent::beforeSave();
	}

	protected function afterFind()
	{
		$this->tanggal = date('d/m/Y',strtotime($this->tanggal));
		$this->tanggal_rujukan = !empty($this->tanggal_rujukan) ? date('d/m/Y',strtotime($this->tanggal_rujukan)) : date('d/m/Y');
		return parent::afterFind();
	}
}
