<?php

/**
 * This is the model class for table "lab_request".
 *
 * The followings are the available columns in table 'lab_request':
 * @property integer $id
 * @property integer $pasien_id
 * @property integer $unit_id
 * @property integer $kelas_id
 * @property double $reg_id
 * @property integer $rawat_inap_id
 * @property string $tanggal
 * @property string $tanggal_selesai_uji
 * @property string $tanggal_terima_sampel
 * @property integer $status_pelayanan
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property APasien $pasien
 * @property AUnit $unit
 * @property BPendaftaran $reg
 * @property DmKelas $kelas
 * @property TrRawatInap $rawatInap
 * @property LabRequestItem[] $labRequestItems
 */
class LabRequest extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lab_request';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pasien_id, unit_id, tanggal', 'required'),
			array('pasien_id, unit_id, kelas_id, rawat_inap_id, status_pelayanan', 'numerical', 'integerOnly'=>true),
			array('reg_id', 'numerical'),
			array('tanggal_selesai_uji, tanggal_terima_sampel, created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pasien_id, unit_id, kelas_id, reg_id, rawat_inap_id, tanggal, tanggal_selesai_uji, tanggal_terima_sampel, status_pelayanan, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pasien' => array(self::BELONGS_TO, 'Pasien', 'pasien_id'),
			'unit' => array(self::BELONGS_TO, 'Unit', 'unit_id'),
			'reg' => array(self::BELONGS_TO, 'BPendaftaran', 'reg_id'),
			'kelas' => array(self::BELONGS_TO, 'DmKelas', 'kelas_id'),
			'rawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'rawat_inap_id'),
			'labRequestItems' => array(self::HAS_MANY, 'LabRequestItem', 'request_id','order'=>'parent_id ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pasien_id' => 'Pasien',
			'unit_id' => 'Unit',
			'kelas_id' => 'Kelas',
			'reg_id' => 'Reg',
			'rawat_inap_id' => 'Rawat Inap',
			'tanggal' => 'Tanggal',
			'tanggal_selesai_uji' => 'Tanggal Selesai Uji',
			'tanggal_terima_sampel' => 'Tanggal Terima Sampel',
			'status_pelayanan' => 'Status Pelayanan',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('pasien_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('unit_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('reg_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tanggal',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('status_pelayanan',$this->SEARCH,true,'OR');
		
		if(!empty($this->pasien_id))
			$criteria->condition = 'pasien_id='.$this->pasien_id;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,

			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function searchAll()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;
		$sort->defaultOrder ='tanggal DESC';
		$criteria->addSearchCondition('pasien_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('unit_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('reg_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tanggal',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('status_pelayanan',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,

			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LabRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
