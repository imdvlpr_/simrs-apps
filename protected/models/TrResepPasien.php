<?php

/**
 * This is the model class for table "tr_resep_pasien".
 *
 * The followings are the available columns in table 'tr_resep_pasien':
 * @property integer $id
 * @property double $nodaftar_id
 * @property integer $pasien_id
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property TrResepKamar[] $trResepKamars
 * @property APasien $pasien
 * @property BPendaftaran $nodaftar
 */
class TrResepPasien extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_resep_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pasien_id', 'required','message'=>'Data {attribute} tidak ditemukan'),
			array('nodaftar_id', 'required','message'=>'{attribute} tidak ditemukan'),
			array('pasien_id', 'numerical', 'integerOnly'=>true,'message'=>'Format {attribute} salah'),
			array('nodaftar_id', 'numerical'),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nodaftar_id, pasien_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trResepKamars' => array(self::HAS_MANY, 'TrResepKamar', 'tr_resep_pasien_id'),
			'pasien' => array(self::BELONGS_TO, 'Pasien', 'pasien_id'),
			'nodaftar' => array(self::BELONGS_TO, 'BPendaftaran', 'nodaftar_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nodaftar_id' => 'No Pendaftaran',
			'pasien_id' => 'No RM',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nodaftar_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('pasien_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrResepPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
