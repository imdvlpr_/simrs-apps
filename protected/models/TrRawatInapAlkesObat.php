<?php

/**
 * This is the model class for table "tr_rawat_inap_alkes_obat".
 *
 * The followings are the available columns in table 'tr_rawat_inap_alkes_obat':
 * @property integer $id
 * @property integer $id_rawat_inap
 * @property string $kode_alkes
 * @property string $keterangan
 * @property double $nilai
 * @property string $created
 * @property string $id_m_obat_akhp
 * @property string $tanggal_input
 * @property integer $id_dokter
 * @property integer $jumlah
 *
 * The followings are the available model relations:
 * @property DmDokter $idDokter
 * @property TrRawatInap $idRawatInap
 */
class TrRawatInapAlkesObat extends CActiveRecord
{
	public $SEARCH;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_alkes_obat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_rawat_inap, kode_alkes, tanggal_input, id_dokter,jumlah', 'required'),
			array('id_rawat_inap, id_dokter, jumlah', 'numerical', 'integerOnly'=>true),
			array('nilai', 'numerical'),
			array('kode_alkes', 'length', 'max'=>20),
			array('keterangan', 'length', 'max'=>255),
			array('id_m_obat_akhp, satuan', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_rawat_inap, kode_alkes, keterangan, nilai, created, id_m_obat_akhp, tanggal_input, id_dokter, jumlah,satuan,resep_racik_id, resep_non_racik_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dokter' => array(self::BELONGS_TO, 'DmDokter', 'id_dokter'),
			'idRawatInap' => array(self::BELONGS_TO, 'TrRawatInap', 'id_rawat_inap'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_rawat_inap' => 'Id Rawat Inap',
			'kode_alkes' => 'Kode Alkes',
			'keterangan' => 'Nama Obat',
			'nilai' => 'Nilai',
			'created' => 'Created',
			'id_m_obat_akhp' => 'Id M Obat Akhp',
			'tanggal_input' => 'Tanggal Input',
			'id_dokter' => 'Dokter DPJP',
			'jumlah' => 'Jumlah',
			'satuan' => 'Unit'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$sort = new CSort();
		$criteria=new CDbCriteria;


		$criteria->addSearchCondition('kode_alkes',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('keterangan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nilai',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dokter.nama_dokter',$this->SEARCH,true,'OR');
		$criteria->addCondition("id_rawat_inap=".$id);
		$criteria->with = array('dokter');
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>100,
				
			),
		));
	}

	protected function afterFind()
	{

		// $this->nilai = Yii::app()->helper->formatRupiah($this->nilai);
		$this->tanggal_input = Yii::app()->helper->convertIDDate($this->tanggal_input);
		return parent::afterFind();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapAlkesObat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
