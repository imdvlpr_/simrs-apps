<?php

/**
 * This is the model class for table "tindakan_medis_penunjang".
 *
 * The followings are the available columns in table 'tindakan_medis_penunjang':
 * @property integer $id_tindakan_penunjang
 * @property string $nama_tindakan
 * @property integer $kelas_id
 * @property double $bhp
 * @property double $jrs
 * @property double $japel
 * @property double $tarip
 * @property string $param
 * @property string $catatan
 * @property string $created
 *
 * The followings are the available model relations:
 * @property DmKelas $kelas
 */
class TindakanMedisPenunjang extends CActiveRecord
{
	public $SEARCH;
	public $PAGE_SIZE = 10;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tindakan_medis_penunjang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_tindakan, kelas_id, jrs, japel, tarip, param', 'required'),
			array('kelas_id', 'numerical', 'integerOnly'=>true),
			array('bhp, jrs, japel, tarip', 'numerical'),
			array('nama_tindakan, param, catatan', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tindakan_penunjang, nama_tindakan, kelas_id, bhp, jrs, japel, tarip, param, catatan,jenis_penunjang, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kelas' => array(self::BELONGS_TO, 'DmKelas', 'kelas_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tindakan_penunjang' => 'Id Tindakan Penunjang',
			'nama_tindakan' => 'Nama Tindakan',
			'kelas_id' => 'Kelas',
			'bhp' => 'Bhp',
			'jrs' => 'Jrs',
			'japel' => 'Japel',
			'tarip' => 'Tarip',
			'param' => 'Param',
			'catatan' => 'Catatan',
			'jenis_penunjang' => 'Jenis Penunjang',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$sort = new CSort();
		$criteria=new CDbCriteria;

		$criteria->addSearchCondition('nama_tindakan',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));

		// $criteria->compare('id_tindakan_penunjang',$this->id_tindakan_penunjang);
		// $criteria->compare('nama_tindakan',$this->nama_tindakan,true);
		// $criteria->compare('kelas_id',$this->kelas_id);
		// $criteria->compare('bhp',$this->bhp);
		// $criteria->compare('jrs',$this->jrs);
		// $criteria->compare('japel',$this->japel);
		// $criteria->compare('tarip',$this->tarip);
		// $criteria->compare('param',$this->param,true);
		// $criteria->compare('catatan',$this->catatan,true);
		// $criteria->compare('created',$this->created,true);
		//
		// return new CActiveDataProvider($this, array(
		// 	'criteria'=>$criteria,
		// ));
	}

	public function searchByNama($q)
	{

		$criteria=new CDbCriteria;

		$criteria->addSearchCondition('nama_tindakan',$q,true,'OR');

		return TindakanMedisPenunjang::model()->findAll($criteria);

	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TindakanMedisPenunjang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
