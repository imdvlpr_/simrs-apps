<?php

/**
 * This is the model class for table "refprov".
 *
 * The followings are the available columns in table 'refprov':
 * @property integer $KodeProv
 * @property string $NamaProv
 * @property string $KodeIso
 * @property string $IbuKota
 * @property string $Populasi
 * @property string $Luas
 * @property string $StatusKhusus
 * @property string $Pulau
 * @property integer $idprovgos
 */
class Refprov extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'refprov';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idprovgos', 'numerical', 'integerOnly'=>true),
			array('NamaProv, IbuKota, StatusKhusus, Pulau', 'length', 'max'=>40),
			array('KodeIso', 'length', 'max'=>5),
			array('Populasi, Luas', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KodeProv, NamaProv, KodeIso, IbuKota, Populasi, Luas, StatusKhusus, Pulau, idprovgos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kOTA' => array(self::HAS_MANY, 'Refkabkota', 'KodeProv'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KodeProv' => 'Kode Prov',
			'NamaProv' => 'Nama Prov',
			'KodeIso' => 'Kode Iso',
			'IbuKota' => 'Ibu Kota',
			'Populasi' => 'Populasi',
			'Luas' => 'Luas',
			'StatusKhusus' => 'Status Khusus',
			'Pulau' => 'Pulau',
			'idprovgos' => 'Idprovgos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KodeProv',$this->KodeProv);
		$criteria->compare('NamaProv',$this->NamaProv,true);
		$criteria->compare('KodeIso',$this->KodeIso,true);
		$criteria->compare('IbuKota',$this->IbuKota,true);
		$criteria->compare('Populasi',$this->Populasi,true);
		$criteria->compare('Luas',$this->Luas,true);
		$criteria->compare('StatusKhusus',$this->StatusKhusus,true);
		$criteria->compare('Pulau',$this->Pulau,true);
		$criteria->compare('idprovgos',$this->idprovgos);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Refprov the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
