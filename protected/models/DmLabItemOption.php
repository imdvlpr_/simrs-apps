<?php

/**
 * This is the model class for table "dm_lab_item_option".
 *
 * The followings are the available columns in table 'dm_lab_item_option':
 * @property integer $id
 * @property integer $item_id
 * @property double $nilai
 * @property string $label
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property DmLabItem $item
 */
class DmLabItemOption extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_lab_item_option';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, nilai, label', 'required'),
			array('item_id', 'numerical', 'integerOnly'=>true),
			array('nilai', 'numerical'),
			array('label', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item_id, nilai, label, created_at, updated_at, itemName', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'item' => array(self::BELONGS_TO, 'DmLabItem', 'item_id','alias'=>'i'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_id' => 'Item',
			'nilai' => 'Nilai',
			'itemName' => 'Item',
			'label' => 'Label',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;
		$sort->attributes = [
			'itemName'=>[
				'asc'=>'i.nama',
				'desc'=>'i.nama desc',
			],
			'nilai'=>[
				'asc'=>'nilai',
				'desc'=>'nilai desc',
			],
			'label'=>[
				'asc'=>'label',
				'desc'=>'label desc',
			],
		];
		$criteria->addSearchCondition('i.nama',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nilai',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('label',$this->SEARCH,true,'OR');
		$criteria->with = ['item'];
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function getItemName()
	{
		return $this->item->nama;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmLabItemOption the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
