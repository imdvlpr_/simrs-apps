<?php

/**
 * This is the model class for table "a_unit".
 *
 * The followings are the available columns in table 'a_unit':
 * @property integer $KodeUnit
 * @property string $NamaUnit
 * @property integer $unit_tipe
 */
class Unit extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'a_unit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unit_tipe', 'numerical', 'integerOnly'=>true),
			array('NamaUnit', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KodeUnit, NamaUnit, unit_tipe', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bpendaftaranrjalans' => array(self::HAS_MANY, 'BPendaftaranRjalan', 'KodePoli'),
			'unitUsers' => array(self::HAS_ONE, 'UnitUser', 'unit_id'),
			'kepala' => array(self::HAS_ONE, 'UnitKepala', 'pegawai_id'),
			// 'countRJ'	=> array(self::STAT, 'BPendaftaranRjalan', 'KodePoli', 'select'=>'count(*)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KodeUnit' => 'Kode Unit',
			'NamaUnit' => 'Nama Unit',
			'unit_tipe' => 'Unit Tipe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort();
		
		$criteria->addSearchCondition('KodeUnit',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NamaUnit',$this->SEARCH,true,'OR');


		$criteria->compare('unit_tipe',2);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
	}

	public function searchAll()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort();
		
		$criteria->addSearchCondition('KodeUnit',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NamaUnit',$this->SEARCH,true,'OR');
		$criteria->addCondition('unit_tipe <> 0');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Unit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
