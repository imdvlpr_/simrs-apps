<?php

/**
 * This is the model class for table "erp_departemen_stok".
 *
 * The followings are the available columns in table 'erp_departemen_stok':
 * @property integer $id
 * @property integer $barang_id
 * @property integer $departemen_id
 * @property double $stok_akhir
 * @property double $stok_awal
 * @property string $created
 * @property integer $bulan
 * @property integer $tahun
 * @property string $tanggal
 * @property double $stok_bulan_lalu
 * @property double $stok
 * @property integer $ro_item_id
 * @property string $exp_date
 * @property string $batch_no
 *
 * The followings are the available model relations:
 * @property ErpSalesMasterBarang $barang
 * @property ErpRequestOrderItem $roItem
 * @property ErpDepartemen $departemen
 * @property ErpDistribusiBarangItem[] $erpDistribusiBarangItems
 */
class ErpDepartemenStok extends MyActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public function getDbConnection()
    {
        return self::getErpDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'erp_departemen_stok';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barang_id, departemen_id, created, tanggal', 'required'),
			array('barang_id, departemen_id, bulan, tahun, ro_item_id', 'numerical', 'integerOnly'=>true),
			array('stok_akhir, stok_awal, stok_bulan_lalu, stok', 'numerical'),
			array('batch_no', 'length', 'max'=>30),
			array('exp_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, barang_id, departemen_id, stok_akhir, stok_awal, created, bulan, tahun, tanggal, stok_bulan_lalu, stok, ro_item_id, exp_date, batch_no', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'barang' => array(self::BELONGS_TO, 'ErpSalesMasterBarang', 'barang_id'),
			'roItem' => array(self::BELONGS_TO, 'ErpRequestOrderItem', 'ro_item_id'),
			'departemen' => array(self::BELONGS_TO, 'ErpDepartemen', 'departemen_id'),
			'erpDistribusiBarangItems' => array(self::HAS_MANY, 'ErpDistribusiBarangItem', 'stok_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'barang_id' => 'Barang',
			'departemen_id' => 'Departemen',
			'stok_akhir' => 'Stok Akhir',
			'stok_awal' => 'Stok Awal',
			'created' => 'Created',
			'bulan' => 'Bulan',
			'tahun' => 'Tahun',
			'tanggal' => 'Tanggal',
			'stok_bulan_lalu' => 'Stok Bulan Lalu',
			'stok' => 'Stok',
			'ro_item_id' => 'Ro Item',
			'exp_date' => 'Exp Date',
			'batch_no' => 'Batch No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('barang_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('departemen_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('stok_akhir',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('stok_awal',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('bulan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tahun',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tanggal',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('stok_bulan_lalu',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('stok',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('ro_item_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('exp_date',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('batch_no',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ErpDepartemenStok the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
