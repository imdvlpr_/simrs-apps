<?php

/**
 * This is the model class for table "dm_ok_tindakan".
 *
 * The followings are the available columns in table 'dm_ok_tindakan':
 * @property integer $id
 * @property string $nama
 * @property string $kode
 * @property double $biaya_jrs
 * @property double $biaya_drop
 * @property double $biaya_dran
 * @property double $biaya_prwt_op
 * @property double $biaya_prwt_an
 * @property integer $dm_kelas_id
 * @property integer $dm_ok_jenis_tindakan_id
 * @property integer $dm_gol_operasi_id
 *
 * The followings are the available model relations:
 * @property DmOkJenisTindakan $dmOkJenisTindakan
 * @property DmKelas $dmKelas
 * @property DmOkGolOperasi $dmGolOperasi
 */
class DmOkTindakan extends CActiveRecord
{

	public $namaGol;
	public $namaJenis;

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dm_ok_tindakan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('biaya_jrs, biaya_drop, biaya_dran, biaya_prwt_op, biaya_prwt_an, dm_kelas_id, dm_ok_jenis_tindakan_id, dm_gol_operasi_id', 'required'),
			array('dm_kelas_id, dm_ok_jenis_tindakan_id, dm_gol_operasi_id', 'numerical', 'integerOnly'=>true),
			array('biaya_jrs, biaya_drop, biaya_dran, biaya_prwt_op, biaya_prwt_an', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, biaya_jrs, biaya_drop, biaya_dran, biaya_prwt_op, biaya_prwt_an, dm_kelas_id, dm_ok_jenis_tindakan_id, dm_gol_operasi_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dmOkJenisTindakan' => array(self::BELONGS_TO, 'DmOkJenisTindakan', 'dm_ok_jenis_tindakan_id'),
			'dmKelas' => array(self::BELONGS_TO, 'DmKelas', 'dm_kelas_id'),
			'dmGolOperasi' => array(self::BELONGS_TO, 'DmOkGolOperasi', 'dm_gol_operasi_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'namaGol' => 'Gol Op',
			'namaJenis'=> 'Jenis Op',
			
			'biaya_jrs' => 'Biaya JRS',
			'biaya_drop' => 'Biaya Dr OP',
			'biaya_dran' => 'Biaya Dr AN',
			'biaya_prwt_op' => 'Biaya Prwt OP',
			'biaya_prwt_an' => 'Biaya Prwt AN',
			'dm_kelas_id' => 'Kelas Perawatan',
			'dm_ok_jenis_tindakan_id' => 'Jenis Tindakan',
			'dm_gol_operasi_id' => 'Gol Operasi',
			'is_hapus' => 'Dihapus',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('t.nama',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kode',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('biaya_jrs',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('biaya_drop',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('biaya_dran',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('biaya_prwt_op',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('biaya_prwt_an',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dmKelas.nama_kelas',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dmOkJenisTindakan.nama',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dmGolOperasi.nama',$this->SEARCH,true,'OR');
		$criteria->compare('is_hapus',0);
		$criteria->with = array('dmOkJenisTindakan','dmGolOperasi','dmKelas');
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function searchTindakan($q='')
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('is_hapus',0);
		// $criteria->addSearchCondition('t.nama',$q,true,'OR');
		// $criteria->addSearchCondition('kode',$q,true,'OR');
		$criteria->with = ['dmGolOperasi'];
		$criteria->together = true;
		// $criteria->limit = 10;
		$criteria->order = 'dmGolOperasi.nama ASC';
		
		return DmOkTindakan::model()->findAll($criteria);
	}


	protected function afterFind()
	{

		$this->biaya_jrs = Yii::app()->helper->formatRupiah($this->biaya_jrs);
		$this->biaya_drop = Yii::app()->helper->formatRupiah($this->biaya_drop);
		$this->biaya_dran = Yii::app()->helper->formatRupiah($this->biaya_dran);
		$this->biaya_prwt_op = Yii::app()->helper->formatRupiah($this->biaya_prwt_op);
		$this->biaya_prwt_an = Yii::app()->helper->formatRupiah($this->biaya_prwt_an);

		$this->namaGol = $this->dmGolOperasi->nama;
		$this->namaJenis = $this->dmOkJenisTindakan->nama;
		return parent::afterFind();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DmOkTindakan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
