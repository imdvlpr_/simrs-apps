<?php

/**
 * This is the model class for table "td_register_ok".
 *
 * The followings are the available columns in table 'td_register_ok':
 * @property integer $id_ok
 * @property integer $no_urut
 * @property integer $no_rm
 * @property string $tgl_operasi
 * @property string $jam_ok
 * @property string $diagnosa
 * @property string $tindakan
 * @property string $upf
 * @property string $jenis_operasi
 * @property string $jenis_anastesi
 * @property string $ruang
 * @property string $kelas
 * @property integer $dr_operator
 * @property integer $dr_anastesi
 * @property string $asisten_dokter1
 * @property string $asisten_dokter2
 * @property string $cito_elektif
 * @property string $spesimen
 * @property integer $id_rawat_inap
 * @property string $created
 * @property integer $status_ok
 *
 * The followings are the available model relations:
 * @property APasien $noRm
 * @property DmDokter $drOperator
 * @property DmDokter $drAnastesi
 * @property TrRawatInap $idRawatInap
 */
class TdRegisterOk extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $TANGGAL_AWAL = '';
	public $TANGGAL_AKHIR = '';
	

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'td_register_ok';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_rm, tgl_operasi, tindakan, upf, jenis_operasi, jenis_anastesi, unit_id, dr_anastesi, asisten_dokter1, asisten_dokter2, cito_elektif, spesimen', 'required','message'=>'{attribute} harus diisi'),
			array('no_rm, dr_anastesi,status_ok', 'numerical', 'integerOnly'=>true,'message'=>'{attribute} harus diisi angka'),
			array('diagnosa', 'length', 'max'=>100),
			
			array('unit_id', 'length', 'max'=>15),
			array('asisten_dokter1, asisten_dokter2', 'length', 'max'=>150),
			array('spesimen', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ok,no_rm, tgl_operasi, diagnosa, tindakan, upf, jenis_operasi, jenis_anastesi, unit_id,  dr_anastesi, asisten_dokter1, asisten_dokter2, cito_elektif, spesimen, created, status_ok', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tdOkBiayas' => array(self::HAS_ONE, 'TdOkBiaya', 'td_register_ok_id'),
			'kodeDaftar' => array(self::BELONGS_TO, 'BPendaftaran', 'kode_daftar'),
			'pASIEN' => array(self::BELONGS_TO, 'Pasien', 'no_rm'),
			'dataUpf' => array(self::BELONGS_TO, 'TdUpf', 'upf'),
			'unit' => array(self::BELONGS_TO, 'Unit', 'unit_id'),
			'drAnastesi' => array(self::BELONGS_TO, 'DmDokter', 'dr_anastesi'),
			'jenisOperasi' => array(self::BELONGS_TO, 'DmOkGolOperasi', 'jenis_operasi'),
            'tindakan0' => array(self::BELONGS_TO, 'DmOkTindakan', 'tindakan'),
            'jenisAnastesi' => array(self::BELONGS_TO, 'DmOkAnastesi', 'jenis_anastesi'),
            'dataUpf' => array(self::BELONGS_TO, 'TdUpf', 'upf'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ok' => 'Id Ok',
			'no_rm' => 'No Rm',
			'tgl_operasi' => 'Tgl Operasi',
			'diagnosa' => 'Diagnosa',
			'tindakan' => 'Tindakan',
			'upf' => 'UPF',
			'jenis_operasi' => 'Jenis OP',
			'jenis_anastesi' => 'Jenis AN',
			'unit_id' => 'Unit',
			'dr_anastesi' => 'Dr Anastesi',
			'asisten_dokter1' => 'Asisten Dokter1',
			'asisten_dokter2' => 'Asisten Dokter2',
			'cito_elektif' => 'Cito Elektif',
			'spesimen' => 'Spesimen',
			'created' => 'Created',
			'status_ok' => 'Status OK',
		);
	}

	protected function afterFind()
	{

		// $this->tgl_operasi = date('d/m/Y',strtotime($this->tgl_operasi));
		return parent::afterFind();
	}


	public function searchRekap(){
		$criteria=new CDbCriteria;

		$criteria->addBetweenCondition('tgl_operasi', $this->TANGGAL_AWAL.' 00:00:00', $this->TANGGAL_AKHIR.' 23:59:59', 'AND');
		$criteria->order = 'tgl_operasi ASC';

		return TdRegisterOk::model()->findAll($criteria);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id_ok',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('no_rm',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tgl_operasi',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('diagnosa',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tindakan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('upf',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jenis_operasi',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jenis_anastesi',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('unit_id',$this->SEARCH,true,'OR');
		// $criteria->addSearchCondition('dr_operator',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dr_anastesi',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('asisten_dokter1',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('asisten_dokter2',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('cito_elektif',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('spesimen',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');

		if(!empty($this->status_ok))
			$criteria->compare('status_ok',$this->status_ok);

		$criteria->order = 'tgl_operasi ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TdRegisterOk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
