<?php

/**
 * This is the model class for table "tindakan_medis_non_operatif".
 *
 * The followings are the available columns in table 'tindakan_medis_non_operatif':
 * @property integer $id_tindakan
 * @property string $nama_tindakan
 * @property integer $kelas_id
 * @property double $jrs
 * @property double $drOP
 * @property double $drANAS
 * @property double $perawatOK
 * @property double $perawatANAS
 * @property double $total
 * @property double $tarip
 * @property string $created
 *
 * The followings are the available model relations:
 * @property DmKelas $kelas
 * @property TrRawatInapTnop[] $trRawatInapTnops
 */
class TindakanMedisNonOperatif extends CActiveRecord
{
	public $SEARCH;
	public $PAGE_SIZE = 10;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tindakan_medis_non_operatif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_tindakan, nama_tindakan, kelas_id, jrs, drOP, drANAS, perawatOK, perawatANAS, total, tarip', 'required'),
			array('kelas_id', 'numerical', 'integerOnly'=>true),
			array('jrs, drOP, drANAS, perawatOK, perawatANAS, total, tarip', 'numerical'),
			array('nama_tindakan', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tindakan, kode_tindakan, nama_tindakan, kelas_id, jrs, drOP, drANAS, perawatOK, perawatANAS, total, tarip, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kelas' => array(self::BELONGS_TO, 'DmKelas', 'kelas_id'),
			'trRawatInapTnops' => array(self::HAS_MANY, 'TrRawatInapTnop', 'id_tnop'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tindakan' => 'Id Tindakan',
			'nama_tindakan' => 'Nama Tindakan',
			'kelas_id' => 'Kelas',
			'jrs' => 'Jrs',
			'drOP' => 'Dr Op',
			'drANAS' => 'Dr Anas',
			'perawatOK' => 'Perawat Ok',
			'perawatANAS' => 'Perawat Anas',
			'total' => 'Total',
			'tarip' => 'Tarip',
			'created' => 'Created',
		);
	}

	public function searchByNama($q)
	{

		$criteria=new CDbCriteria;

		$criteria->addSearchCondition('nama_tindakan',$q,true,'OR');

		return TindakanMedisNonOperatif::model()->findAll($criteria);

	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$sort = new CSort();
		$criteria=new CDbCriteria;

		$criteria->addSearchCondition('nama_tindakan',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
		// $criteria->compare('id_tindakan',$this->id_tindakan);
		// $criteria->compare('nama_tindakan',$this->nama_tindakan,true);
		// $criteria->compare('kelas_id',$this->kelas_id);
		// $criteria->compare('jrs',$this->jrs);
		// $criteria->compare('drOP',$this->drOP);
		// $criteria->compare('drANAS',$this->drANAS);
		// $criteria->compare('perawatOK',$this->perawatOK);
		// $criteria->compare('perawatANAS',$this->perawatANAS);
		// $criteria->compare('total',$this->total);
		// $criteria->compare('tarip',$this->tarip);
		// $criteria->compare('created',$this->created,true);
		//
		// return new CActiveDataProvider($this, array(
		// 	'criteria'=>$criteria,
		// ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TindakanMedisNonOperatif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
