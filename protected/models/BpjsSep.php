<?php

/**
 * This is the model class for table "bpjs_sep".
 *
 * The followings are the available columns in table 'bpjs_sep':
 * @property string $NoSEP
 * @property integer $JENIS_RAWAT
 * @property integer $KELAS_RAWAT
 * @property integer $NO_RM
 * @property integer $NO_RUJUKAN
 * @property string $kdCabang
 * @property string $kdProvider
 * @property string $nmCabang
 * @property string $nmProvider
 * @property string $TGL_RUJUKAN
 * @property string $TGL_SEP
 * @property string $DIAG_AWAL
 * @property string $namaDiagnosa
 * @property string $POLI_TUJUAN
 * @property string $nmPoli
 * @property string $CATATAN
 * @property integer $LAKA_LANTAS
 * @property string $CREATED
 * @property string $lokasi_laka
 *
 * The followings are the available model relations:
 * @property BpjsJenisRawat $jENISRAWAT
 * @property APasien $nORM
 */
class BpjsSep extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public $NAMA_KELAS_RAWAT = '';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs_sep';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NoSEP, JENIS_RAWAT, KELAS_RAWAT, NO_RUJUKAN, kdProvider, nmProvider, TGL_RUJUKAN, TGL_SEP, DIAG_AWAL, namaDiagnosa, POLI_TUJUAN, nmPoli, LAKA_LANTAS', 'required'),
			array('JENIS_RAWAT, KELAS_RAWAT, NO_RM, NO_RUJUKAN, LAKA_LANTAS', 'numerical', 'integerOnly'=>true),
			array('NoSEP', 'length', 'max'=>100),
			array('kdProvider', 'length', 'max'=>50),
			array('nmProvider, namaDiagnosa, nmPoli, CATATAN, lokasi_laka', 'length', 'max'=>255),
			array('DIAG_AWAL', 'length', 'max'=>10),
			array('POLI_TUJUAN', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NoSEP, JENIS_RAWAT, KELAS_RAWAT, NO_RM, NO_RUJUKAN, kdProvider,  nmProvider, TGL_RUJUKAN, TGL_SEP, DIAG_AWAL, namaDiagnosa, POLI_TUJUAN, nmPoli, CATATAN, LAKA_LANTAS, CREATED, lokasi_laka', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jENISRAWAT' => array(self::BELONGS_TO, 'BpjsJenisRawat', 'JENIS_RAWAT'),
			'nORM' => array(self::BELONGS_TO, 'Pasien', 'NO_RM'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NoSEP' => 'No Sep',
			'JENIS_RAWAT' => 'Jenis Rawat',
			'KELAS_RAWAT' => 'Kelas Rawat',
			'NO_RM' => 'No Rm',
			'NO_RUJUKAN' => 'No Rujukan',
			'kdProvider' => 'Kd Provider',
			
			'nmProvider' => 'Nm Provider',
			'TGL_RUJUKAN' => 'Tgl Rujukan',
			'TGL_SEP' => 'Tgl Sep',
			'DIAG_AWAL' => 'Diag Awal',
			'namaDiagnosa' => 'Nama Diagnosa',
			'POLI_TUJUAN' => 'Poli Tujuan',
			'nmPoli' => 'Nm Poli',
			'CATATAN' => 'Catatan',
			'LAKA_LANTAS' => 'Laka Lantas',
			'CREATED' => 'Created',
			'lokasi_laka' => 'Lokasi Laka',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('NoSEP',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('JENIS_RAWAT',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('KELAS_RAWAT',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NO_RM',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NO_RUJUKAN',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kdProvider',$this->SEARCH,true,'OR');

		$criteria->addSearchCondition('nmProvider',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('TGL_RUJUKAN',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('TGL_SEP',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('DIAG_AWAL',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('namaDiagnosa',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('POLI_TUJUAN',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nmPoli',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('CATATAN',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('LAKA_LANTAS',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('CREATED',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('lokasi_laka',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	protected function afterFind()
	{
		 $kelasoptions = array(
	        '1' => 'Kelas I',
	        '2' => 'Kelas II',
	        '3' => 'Kelas III'
	      );
		$this->NAMA_KELAS_RAWAT = $kelasoptions[$this->KELAS_RAWAT];
		return parent::afterFind();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BpjsSep the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
