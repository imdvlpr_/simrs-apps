<?php

/**
 * This is the model class for table "tarif_kamar".
 *
 * The followings are the available columns in table 'tarif_kamar':
 * @property integer $id_tarif_kamar
 * @property string $nama_item
 * @property double $biaya_kelas3
 * @property double $biaya_kelas2
 * @property string $created
 */
class TarifKamar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif_kamar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_item, biaya_kelas3, biaya_kelas2, created', 'required'),
			array('biaya_kelas3, biaya_kelas2', 'numerical'),
			array('nama_item', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tarif_kamar, nama_item, biaya_kelas3, biaya_kelas2, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tarif_kamar' => 'Id Tarif Kamar',
			'nama_item' => 'Nama Item',
			'biaya_kelas3' => 'Biaya Kelas3',
			'biaya_kelas2' => 'Biaya Kelas2',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tarif_kamar',$this->id_tarif_kamar);
		$criteria->compare('nama_item',$this->nama_item,true);
		$criteria->compare('biaya_kelas3',$this->biaya_kelas3);
		$criteria->compare('biaya_kelas2',$this->biaya_kelas2);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TarifKamar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
