<?php

/**
 * This is the model class for table "tr_obat_racikan".
 *
 * The followings are the available columns in table 'tr_obat_racikan':
 * @property integer $id
 * @property string $kode
 * @property string $nama
 *
 * The followings are the available model relations:
 * @property TrObatRacikanItem[] $trObatRacikanItems
 * @property TrResepKamarRacikan[] $trResepKamarRacikans
 */
class TrObatRacikan extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_obat_racikan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode, nama, kekuatan', 'required'),
			array('kode', 'length', 'max'=>20),
			array('nama', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, kode, nama,kekuatan, obat_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trObatRacikanItems' => array(self::HAS_MANY, 'TrObatRacikanItem', 'tr_obat_racikan_id'),
			'trResepKamarRacikans' => array(self::HAS_MANY, 'TrResepKamarRacikan', 'tr_obat_racikan_id'),
			'sumObatItem'	=> array(self::STAT, 'TrObatRacikanItem', 'tr_obat_racikan_id', 'select'=>'SUM(harga)'),
			'sumObatQty'	=> array(self::STAT, 'TrObatRacikanItem', 'tr_obat_racikan_id', 'select'=>'SUM(qty)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'kode' => 'Kode',
			'nama' => 'Nama',
			'kekuatan' => 'Kekuatan',
			'obat_id' => 'Obat ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('kode',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function searchRacikanBy($q)
	{
		$criteria=new CDbCriteria;
		$criteria->addSearchCondition('kode',$q,true,'OR');
		$criteria->addSearchCondition('nama',$q,true,'OR');
		// $criteria->addSearchCondition('nama_generik',$q,true,'OR');
		$criteria->order = 'nama';
		
		$criteria->limit = 20;
		

		$data = TrObatRacikan::model()->findAll($criteria);
		
		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrObatRacikan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
