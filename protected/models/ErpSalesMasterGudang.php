<?php

/**
 * This is the model class for table "erp_sales_master_gudang".
 *
 * The followings are the available columns in table 'erp_sales_master_gudang':
 * @property integer $id_gudang
 * @property string $nama
 * @property string $alamat
 * @property string $telp
 * @property double $kapasitas
 * @property integer $id_perusahaan
 * @property string $created
 * @property integer $is_sejenis
 * @property integer $is_hapus
 * @property integer $is_penuh
 *
 * The followings are the available model relations:
 * @property ErpBarangDatang[] $erpBarangDatangs
 * @property ErpBarangStokOpname[] $erpBarangStokOpnames
 * @property ErpSalesFakturBarang[] $erpSalesFakturBarangs
 * @property ErpPerusahaan $idPerusahaan
 * @property ErpSalesStokGudang[] $erpSalesStokGudangs
 * @property ErpStokAwal[] $erpStokAwals
 */
class ErpSalesMasterGudang extends MyActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public function getDbConnection()
    {
        return self::getErpDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'erp_sales_master_gudang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, alamat, telp, created', 'required'),
			array('id_perusahaan, is_sejenis, is_hapus, is_penuh', 'numerical', 'integerOnly'=>true),
			array('kapasitas', 'numerical'),
			array('nama, alamat, telp', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_gudang, nama, alamat, telp, kapasitas, id_perusahaan, created, is_sejenis, is_hapus, is_penuh', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'erpBarangDatangs' => array(self::HAS_MANY, 'ErpBarangDatang', 'gudang_id'),
			'erpBarangStokOpnames' => array(self::HAS_MANY, 'ErpBarangStokOpname', 'gudang_id'),
			'erpSalesFakturBarangs' => array(self::HAS_MANY, 'ErpSalesFakturBarang', 'id_gudang'),
			'idPerusahaan' => array(self::BELONGS_TO, 'ErpPerusahaan', 'id_perusahaan'),
			'erpSalesStokGudangs' => array(self::HAS_MANY, 'ErpSalesStokGudang', 'id_gudang'),
			'erpStokAwals' => array(self::HAS_MANY, 'ErpStokAwal', 'gudang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_gudang' => 'Id Gudang',
			'nama' => 'Nama',
			'alamat' => 'Alamat',
			'telp' => 'Telp',
			'kapasitas' => 'Kapasitas',
			'id_perusahaan' => 'Id Perusahaan',
			'created' => 'Created',
			'is_sejenis' => 'Is Sejenis',
			'is_hapus' => 'Is Hapus',
			'is_penuh' => 'Is Penuh',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id_gudang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('nama',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('alamat',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('telp',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kapasitas',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('id_perusahaan',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_sejenis',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_hapus',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_penuh',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ErpSalesMasterGudang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
