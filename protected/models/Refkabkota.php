<?php

/**
 * This is the model class for table "refkabkota".
 *
 * The followings are the available columns in table 'refkabkota':
 * @property integer $KodeKabKota
 * @property integer $KodeProv
 * @property string $NamaKabKota
 * @property string $IbuKota
 */
class Refkabkota extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'refkabkota';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KodeProv', 'required'),
			array('KodeProv', 'numerical', 'integerOnly'=>true),
			array('NamaKabKota, IbuKota', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KodeKabKota, KodeProv, NamaKabKota, IbuKota', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kEC' => array(self::HAS_MANY, 'Refkec', 'KodeKabKota'),
			'pROV' => array(self::BELONGS_TO, 'Refprov', 'KodeProv'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KodeKabKota' => 'Kode Kab Kota',
			'KodeProv' => 'Kode Prov',
			'NamaKabKota' => 'Nama Kab Kota',
			'IbuKota' => 'Ibu Kota',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KodeKabKota',$this->KodeKabKota);
		$criteria->compare('KodeProv',$this->KodeProv);
		$criteria->compare('NamaKabKota',$this->NamaKabKota,true);
		$criteria->compare('IbuKota',$this->IbuKota,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Refkabkota the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
