<?php

/**
 * This is the model class for table "tr_rawat_inap_darurat".
 *
 * The followings are the available columns in table 'tr_rawat_inap_darurat':
 * @property integer $id_rawat_darurat
 * @property integer $id_ri
 * @property integer $id_tindakan_darurat
 * @property string $created
 *
 * The followings are the available model relations:
 * @property TindakanRawatDarurat $idTindakanDarurat
 */
class TrRawatInapDarurat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_darurat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_ri, id_tindakan_darurat', 'required'),
			array('id_ri, id_tindakan_darurat', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_rawat_darurat, id_ri, id_tindakan_darurat, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tINDAKANDARURAT' => array(self::BELONGS_TO, 'TindakanRawatDarurat', 'id_tindakan_darurat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_rawat_darurat' => 'Id Rawat Darurat',
			'id_ri' => 'Id Ri',
			'id_tindakan_darurat' => 'Id Tindakan Darurat',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_rawat_darurat',$this->id_rawat_darurat);
		$criteria->compare('id_ri',$this->id_ri);
		$criteria->compare('id_tindakan_darurat',$this->id_tindakan_darurat);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapDarurat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
