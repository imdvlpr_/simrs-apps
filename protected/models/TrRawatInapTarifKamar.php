<?php

/**
 * This is the model class for table "tr_rawat_inap_tarif_kamar".
 *
 * The followings are the available columns in table 'tr_rawat_inap_tarif_kamar':
 * @property integer $id_tarif_ri
 * @property integer $id_ri
 * @property integer $id_tarif_kamar
 * @property integer $created
 *
 * The followings are the available model relations:
 * @property TarifKamar $idTarifKamar
 * @property TrRawatInap $idRi
 */
class TrRawatInapTarifKamar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap_tarif_kamar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_ri, id_tarif_kamar', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tarif_ri, id_ri, id_tarif_kamar, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tARIFKAMAR' => array(self::BELONGS_TO, 'TarifKamar', 'id_tarif_kamar'),
			'idRi' => array(self::BELONGS_TO, 'TrRawatInap', 'id_ri'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tarif_ri' => 'Id Tarif Ri',
			'id_ri' => 'Id Ri',
			'id_tarif_kamar' => 'Id Tarif Kamar',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tarif_ri',$this->id_tarif_ri);
		$criteria->compare('id_ri',$this->id_ri);
		$criteria->compare('id_tarif_kamar',$this->id_tarif_kamar);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInapTarifKamar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
