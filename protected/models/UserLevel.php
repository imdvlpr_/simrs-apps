<?php

/**
 * This is the model class for table "user_level".
 *
 * The followings are the available columns in table 'user_level':
 * @property integer $KODE_LEVEL
 * @property string $NAMA_LEVEL
 * @property integer $STATUS_LEVEL
 *
 * The followings are the available model relations:
 * @property User[] $users
 */
class UserLevel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_level';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODE_LEVEL, NAMA_LEVEL', 'required'),
			array('KODE_LEVEL, STATUS_LEVEL', 'numerical', 'integerOnly'=>true),
			array('NAMA_LEVEL', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KODE_LEVEL, NAMA_LEVEL, STATUS_LEVEL', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'users' => array(self::HAS_MANY, 'User', 'LEVEL'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KODE_LEVEL' => 'Kode Level',
			'NAMA_LEVEL' => 'Nama Level',
			'STATUS_LEVEL' => 'Status Level',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KODE_LEVEL',$this->KODE_LEVEL);
		$criteria->compare('NAMA_LEVEL',$this->NAMA_LEVEL,true);
		$criteria->compare('STATUS_LEVEL',$this->STATUS_LEVEL);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserLevel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
