<?php

/**
 * This is the model class for table "b_pendaftaran_rjalan".
 *
 * The followings are the available columns in table 'b_pendaftaran_rjalan':
 * @property double $Id
 * @property double $NoDaftar
 * @property integer $KodePoli
 * @property integer $KodeSubPlg
 * @property string $KetPlg
 * @property integer $UrutanPoli
 * @property integer $KodeTdkLanjut
 * @property integer $StatusKunj
 * @property string $KetTdkL1
 * @property string $KetTdkL2
 * @property string $KetTdkL3
 * @property string $KetTdkL4
 * @property string $KetTdkL5
 * @property integer $NoAntriPoli
 * @property string $PostMRS
 *
 * The followings are the available model relations:
 * @property BPendaftaran $noDaftar
 */
class BPendaftaranRjalan extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;
	public $SEARCH_BY = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b_pendaftaran_rjalan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KodePoli,NoDaftar','required'),
			array('KodePoli, KodeSubPlg, UrutanPoli, KodeTdkLanjut, StatusKunj, NoAntriPoli', 'numerical', 'integerOnly'=>true),
			array('NoDaftar', 'numerical'),
			array('KetPlg, PostMRS', 'length', 'max'=>30),
			array('KetTdkL1, KetTdkL2, KetTdkL3, KetTdkL4, KetTdkL5', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, NoDaftar, KodePoli, KodeSubPlg, KetPlg, UrutanPoli, KodeTdkLanjut, StatusKunj, KetTdkL1, KetTdkL2, KetTdkL3, KetTdkL4, KetTdkL5, NoAntriPoli, PostMRS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'noDaftar' => array(self::BELONGS_TO, 'BPendaftaran', 'NoDaftar'),
			'unit' => array(self::BELONGS_TO, 'Unit', 'KodePoli'),
			'kodeSubPulang' => array(self::BELONGS_TO, 'Refcarapulangdtl', 'KodeSubPlg'),
			'rjalanTindakans' => array(self::HAS_MANY, 'BPendaftaranRjalanTindakan', 'Id'),
			'sumBiaya'	=> array(self::STAT, 'BPendaftaranRjalanTindakan', 'Id', 'select'=>'SUM(biaya)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'NoDaftar' => 'No Daftar',
			'KodePoli' => 'Kode Poli',
			'KodeSubPlg' => 'Kode Sub Plg',
			'KetPlg' => 'Ket Plg',
			'UrutanPoli' => 'Urutan Poli',
			'KodeTdkLanjut' => 'Kode Tdk Lanjut',
			'StatusKunj' => 'Status Kunj',
			'KetTdkL1' => 'Ket Tdk L1',
			'KetTdkL2' => 'Ket Tdk L2',
			'KetTdkL3' => 'Ket Tdk L3',
			'KetTdkL4' => 'Ket Tdk L4',
			'KetTdkL5' => 'Ket Tdk L5',
			'NoAntriPoli' => 'No Antri Poli',
			'PostMRS' => 'Post Mrs',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;
		$sort->defaultOrder = 't.NoDaftar ASC';

		if(Yii::app()->user->checkAccess([WebUser::R_OP_POLI]))
		{

			$unitUser = Yii::app()->user->getState('unit');
			
			if(!empty($unitUser)){
				$criteria->compare('noDaftar.TGLDAFTAR',date('Y-m-d'));
				$criteria->with = ['noDaftar'];
				$criteria->together = true;
				$criteria->compare('KodePoli',$unitUser);
			}
			else{
				$criteria->compare('KodePoli','-');
			}
		}


		switch ($this->SEARCH_BY) {
		case 1:
			$criteria->compare('noDaftar.NoMedrec',trim($this->SEARCH));
			$criteria->with = ['noDaftar'];
			$criteria->together = true;
			
	
			break;
		case 2:
	
		
			$criteria->with = ['noDaftar.noMedrec'];
			$criteria->together = true;
			$criteria->addSearchCondition('NAMA',$this->SEARCH,true,'OR');
			break;
		}

		if(!empty($this->KodePoli)){
			$criteria->compare('KodePoli',$this->KodePoli);
		}
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	public function searchLimit()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;
		$criteria->addSearchCondition('t.NoDaftar',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('NAMA',$this->SEARCH,true,'OR');
		

		$criteria->with = ['noDaftar.noMedrec'];
		$criteria->together = true;
		$criteria->order = 't.NoDaftar DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	protected function afterFind()
	{
		$pasien = $this->noDaftar->noMedrec;
		$listData = BPendaftaran::model()->findAllByAttributes([
			'NoMedrec'=>$pasien->NoMedrec
		],['order'=>'NODAFTAR DESC']);
		$size = count($listData);
			
		if($size > 1){
			$item = $listData[1];

			$this->PostMRS = $item->TGLDAFTAR;
			
		}
		return parent::afterFind();
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BPendaftaranRjalan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
