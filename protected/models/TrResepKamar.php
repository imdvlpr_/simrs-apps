<?php

/**
 * This is the model class for table "tr_resep_kamar".
 *
 * The followings are the available columns in table 'tr_resep_kamar':
 * @property integer $id
 * @property integer $tr_resep_pasien_id
 * @property string $no_resep
 * @property integer $dokter_id
 * @property string $petugas_id
 * @property string $tgl_resep
 * @property integer $kamar_id
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property DmDokter $dokter
 * @property Tpegawai $petugas
 * @property DmKamar $kamar
 * @property TrResepPasien $trResepPasien
 * @property TrResepKamarItem[] $trResepKamarItems
 * @property TrResepKamarRacikan[] $trResepKamarRacikans
 */
class TrResepKamar extends CActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public $PASIEN_NAMA='';

	public $TANGGAL_AWAL;
	public $TANGGAL_AKHIR;

	

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_resep_kamar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tr_resep_pasien_id, dokter_id, petugas_id, tgl_resep, kamar_id', 'required'),
			array('tr_resep_pasien_id, dokter_id, kamar_id', 'numerical', 'integerOnly'=>true),
			array('no_resep', 'length', 'max'=>50),
			array('petugas_id', 'length', 'max'=>20),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tr_resep_pasien_id, no_resep, dokter_id, petugas_id, tgl_resep, kamar_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dokter' => array(self::BELONGS_TO, 'DmDokter', 'dokter_id'),
			'petugas' => array(self::BELONGS_TO, 'Pegawai', 'petugas_id'),
			'kamar' => array(self::BELONGS_TO, 'DmKamar', 'kamar_id'),
			'trResepPasien' => array(self::BELONGS_TO, 'TrResepPasien', 'tr_resep_pasien_id'),
			'trResepKamarItems' => array(self::HAS_MANY, 'TrResepKamarItem', 'tr_resep_kamar_id'),
			'sumObatItem' => array(self::STAT, 'TrResepKamarItem', 'tr_resep_kamar_id','select'=>'SUM(harga)'),
			'trResepKamarRacikans' => array(self::HAS_MANY, 'TrResepKamarRacikan', 'tr_resep_kamar_id'),
			'sumObatRacikan' => array(self::STAT, 'TrResepKamarRacikan', 'tr_resep_kamar_id','select'=>'SUM(harga)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tr_resep_pasien_id' => 'Resep Pasien',
			'no_resep' => 'No Resep',
			'dokter_id' => 'Dokter',
			'petugas_id' => 'Petugas',
			'tgl_resep' => 'Tgl Resep',
			'kamar_id' => 'Kamar',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($tr_resep_pasien_id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		
		$criteria->addSearchCondition('no_resep',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('dokter_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('petugas_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('tgl_resep',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('kamar_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');
		$criteria->compare('tr_resep_pasien_id',$tr_resep_pasien_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrResepKamar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave(){
		$this->tgl_resep = Yii::app()->helper->convertSQLDate($this->tgl_resep);

		return parent::beforeSave();
	}

	protected function afterFind(){
		
		// $this->PASIEN_NAMA = $this->pasien->NAMA;
		$this->tgl_resep = date('d/m/Y',strtotime($this->tgl_resep));
		return parent::afterFind();
	}


	public function searchRincian($kode_daftar)
	{
		$criteria=new CDbCriteria;
		$criteria->compare('kode_daftar',$kode_daftar);
		
		return TrResepKamar::model()->findAll($criteria);
	}

	public function searchResep(){
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tgl_resep', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR, 'AND');
		// if(!empty($this->kamar_id))
		// 	$criteria->addCondition("kamar_id=".$this->kamar_id);

		// if(!empty($this->TIPE_PASIEN))
		// 	$criteria->compare("jenis_pasien",$this->TIPE_PASIEN);

		// if(!empty($this->KAMAR_PASIEN))
		// 	$criteria->compare("kamar_id",$this->KAMAR_PASIEN);			
		


		
		return TrResepKamar::model()->findAll($criteria);
	}

}
