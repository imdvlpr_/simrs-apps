<?php

/**
 * This is the model class for table "tr_rawat_inap".
 *
 * The followings are the available columns in table 'tr_rawat_inap':
 * @property integer $id_rawat_inap
 * @property string $tanggal_masuk
 * @property string $jam_masuk
 * @property string $tanggal_keluar
 * @property string $jam_keluar
 * @property string $datetime_masuk
 * @property string $datetime_keluar
 * @property string $created
 * @property integer $pasien_id
 * @property integer $ri_item_id
 *
 * The followings are the available model relations:
 * @property TrRawatInapLayanan[] $trRawatInapLayanans
 */
class TrRawatInap extends CActiveRecord
{

	public $SUBTOTAL = 0;
	public $nama_dokter = '';
	public $PASIEN_NAMA='';
	public $KAMAR_SEKARANG = '';
	public $Kamar = null;

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public $TANGGAL_AWAL = '';
	public $TANGGAL_AKHIR = '';
	public $TIPE_PASIEN = '';
	public $KAMAR_PASIEN = '';
	public $STATUS_INAP_PASIEN = '';
	public $SEARCH_BY = 0;

	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_inap';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tanggal_masuk, datetime_masuk, pasien_id, kamar_id,jenis_pasien,is_naik_kelas, kode_rawat', 'required','message'=>'{attribute} harus diisi'),
			array('pasien_id, dokter_id', 'numerical', 'integerOnly'=>true),
			array('pasien_id','checkSudahInap','on'=>'insert'),
			array('pasien_id','cekRawatHariSama','on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_rawat_inap, kode_rawat, tanggal_masuk, jam_masuk, tanggal_masuk_ird, jam_masuk_ird,datetime_masuk_ird,
			,tanggal_keluar_ird, jam_keluar_ird,datetime_keluar_ird, tanggal_keluar, jam_keluar, datetime_masuk, datetime_keluar, created, pasien_id, dokter_id,kamar_id,jenis_pasien,status_inap, biaya_paket_1, biaya_paket_2, biaya_paket_3,status_rawat, prev_kamar, next_kamar, datetime_pulang, jam_pulang, tanggal_pulang, jenis_ird, status_pasien, is_naik_kelas,biaya_total_kamar,biaya_total_ird,biaya_dibayar,biaya_kamar, is_tarif_kamar_penuh', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			'jenisPasien' => array(self::BELONGS_TO, 'GolPasien', 'jenis_pasien'),
			'kamar' => array(self::BELONGS_TO, 'DmKamar', 'kamar_id'),
			'pASIEN' => array(self::BELONGS_TO, 'Pasien', 'pasien_id'),
			'trRawatInapAlkesObats' => array(self::HAS_MANY, 'TrRawatInapAlkesObat', 'id_rawat_inap'),
			'trRawatInapKamars' => array(self::HAS_MANY, 'TrRawatInapKamarHistory', 'tr_ri_id'),
			'trRawatInapLayanans' => array(self::HAS_MANY, 'TrRawatInapLayanan', 'id_ri'),
			'trRawatInapTarifKamars' => array(self::HAS_MANY, 'TrRawatInapTarifKamar', 'id_ri'),
			'dokter' => array(self::BELONGS_TO, 'DmDokter', 'dokter_id'),
			'trRawatInapAlkes' => array(self::HAS_MANY, 'TrRawatInapAlkes', 'id_rawat_inap'),
			'sumAlkesObat'	=> array(self::STAT, 'TrRawatInapAlkes', 'id_rawat_inap', 'select'=>'SUM(biaya_irna)',
				'join' => 'INNER JOIN obat_alkes o ON o.id_obat_alkes = t.id_alkes',
				'condition'=>'o.kode_alkes = "OBAT"'
			),
			'sumAlkesObatIrd'	=> array(self::STAT, 'TrRawatInapAlkes', 'id_rawat_inap', 'select'=>'SUM(biaya_ird)',
				'join' => 'INNER JOIN obat_alkes o ON o.id_obat_alkes = t.id_alkes',
				'condition'=>'o.kode_alkes = "OBAT"'
			),
			'sumAlkesBhp'	=> array(self::STAT, 'TrRawatInapAlkes', 'id_rawat_inap', 'select'=>'SUM(biaya_irna)',
				'join' => 'INNER JOIN obat_alkes o ON o.id_obat_alkes = t.id_alkes',
				'condition'=>'o.kode_alkes = "BHP"'
			),
			'sumAlkesBhpIrd'	=> array(self::STAT, 'TrRawatInapAlkes', 'id_rawat_inap', 'select'=>'SUM(biaya_ird)',
				'join' => 'INNER JOIN obat_alkes o ON o.id_obat_alkes = t.id_alkes',
				'condition'=>'o.kode_alkes = "BHP"'
			),
			'sumObat'	=> array(self::STAT, 'TrRawatInapAlkesObat', 'id_rawat_inap', 'select'=>'SUM(nilai)','condition'=>'kode_alkes = "OBAT"'),
			'sumObatIrd'	=> array(self::STAT, 'TrRawatInapAlkesObatIrd', 'id_rawat_inap', 'select'=>'SUM(nilai)','condition'=>'kode_alkes = "OBAT"'),
			'sumAlkes'	=> array(self::STAT, 'TrRawatInapAlkesObat', 'id_rawat_inap', 'select'=>'SUM(nilai)','condition'=>'kode_alkes = "BHP"'),
			'sumAlkesIrd'	=> array(self::STAT, 'TrRawatInapAlkesObatIrd', 'id_rawat_inap', 'select'=>'SUM(nilai)','condition'=>'kode_alkes = "BHP"'),
			'sumBHPTop'	=> array(self::STAT, 'TrRawatInapTop', 'id_rawat_inap', 'select'=>'SUM(biaya_irna)',
				'join' => 'INNER JOIN tindakan_medis_operatif tmo ON tmo.id_tindakan = t.id_top',
				'condition'=>'kode_tindakan = "BHP"'),
			'sumTindRRA'	=> array(self::STAT, 'TrRawatInapTop', 'id_rawat_inap', 'select'=>'SUM(biaya_irna)',
				'join' => 'INNER JOIN tindakan_medis_operatif tmo ON tmo.id_tindakan = t.id_top',
				'condition'=>'kode_tindakan = "RRA"'),
			'sumTindRRM'	=> array(self::STAT, 'TrRawatInapTop', 'id_rawat_inap', 'select'=>'SUM(biaya_irna)',
				'join' => 'INNER JOIN tindakan_medis_operatif tmo ON tmo.id_tindakan = t.id_top',
				'condition'=>'kode_tindakan = "RRM"'),
			'trRawatInapTops' => array(self::HAS_MANY, 'TrRawatInapTop', 'id_rawat_inap'),
			'trRawatInapTnops' => array(self::HAS_MANY, 'TrRawatInapTnop', 'id_rawat_inap'),
			'trRawatInapRincians' => array(self::HAS_ONE, 'TrRawatInapRincian', 'id_rawat_inap'),
			'tRIVisiteDokters' => array(self::HAS_MANY, 'TrRawatInapVisiteDokter', 'id_rawat_inap'),
			'sumVisite'	=> array(self::STAT, 'TrRawatInapVisiteDokter', 'id_rawat_inap', 'select'=>'SUM(biaya_visite) * jumlah_visite'),
			
			'trRawatInapPenunjangs' => array(self::HAS_MANY, 'TrRawatInapPenunjang', 'id_rawat_inap'),
			'trRawatInapLains' => array(self::HAS_MANY, 'TrRawatInapLain', 'id_rawat_inap'),
			'trRawatInapTopJm' => array(self::HAS_MANY, 'TrRawatInapTopJm', 'id_rawat_inap'),
			'sumTopJm'	=> array(self::STAT, 'TrRawatInapTopJm', 'id_rawat_inap', 'select'=>'SUM(nilai)'),
			'sumTopJa'	=> array(self::STAT, 'TrRawatInapTopJa', 'id_rawat_inap', 'select'=>'SUM(nilai)'),
			'trRawatInapTopJa' => array(self::HAS_MANY, 'TrRawatInapTopJa', 'id_rawat_inap'),
			'trRawatInapTnopJrm' => array(self::HAS_MANY, 'TrRawatInapTnopJrm', 'id_rawat_inap'),
			'trRawatInapTnopJm' => array(self::HAS_MANY, 'TrRawatInapTnopJm', 'id_rawat_inap'),
			'trRawatInapTnopJapel' => array(self::HAS_MANY, 'TrRawatInapTnopJapel', 'id_rawat_inap'),
			'trRawatInapTnopIrdJrm' => array(self::HAS_MANY, 'TrRawatInapTnopIrdJrm', 'id_rawat_inap'),
			'trRawatInapTnopIrdJm' => array(self::HAS_MANY, 'TrRawatInapTnopIrdJm', 'id_rawat_inap'),
			'trRawatInapTnopIrdJapel' => array(self::HAS_MANY, 'TrRawatInapTnopIrdJapel', 'id_rawat_inap'),
			'trRawatInapAlkesObat' => array(self::HAS_MANY, 'TrRawatInapAlkesObat', 'id_rawat_inap'),
			'trRawatInapAlkesObatIrd' => array(self::HAS_MANY, 'TrRawatInapAlkesObatIrd', 'id_rawat_inap'),
			'trRawatInapTopTindakan' => array(self::HAS_MANY, 'TrRawatInapTopTindakan', 'id_rawat_inap'),
			'sumPrwtOP'	=> array(self::STAT, 'TrRawatInapTopTindakan', 'id_rawat_inap', 'select'=>'SUM(nilai)',
				'join' => 'INNER JOIN tindakan_medis_operatif tmo ON tmo.id_tindakan = t.id_tindakan',
				'condition'=>'kode_tindakan = "JPO"'),
			'sumPrwtOK'	=> array(self::STAT, 'TrRawatInapTop', 'id_rawat_inap', 'select'=>'SUM(biaya_irna)',
				'join' => 'INNER JOIN tindakan_medis_operatif tmo ON tmo.id_tindakan = t.id_top',
				'condition'=>'kode_tindakan = "JPO"'),
			'sumPrwtANLama'	=> array(self::STAT, 'TrRawatInapTopTindakan', 'id_rawat_inap', 'select'=>'SUM(nilai)',
				'join' => 'INNER JOIN tindakan_medis_operatif tmo ON tmo.id_tindakan = t.id_tindakan',
				'condition'=>'kode_tindakan = "JPA"'),
			'sumPrwtAN'	=> array(self::STAT, 'TrRawatInapTop', 'id_rawat_inap', 'select'=>'SUM(biaya_irna)',
				'join' => 'INNER JOIN tindakan_medis_operatif tmo ON tmo.id_tindakan = t.id_top',
				'condition'=>'kode_tindakan = "JPA"'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'is_locked' => 'Terkunci',
			'kode_rawat' => 'Kode Rawat',
			'id_rawat_inap' => 'Id Rawat Inap',
			'tanggal_masuk' => 'Tanggal Masuk',
			'jam_masuk' => 'Jam Masuk',
			'tanggal_keluar' => 'Tanggal Keluar',
			'jam_keluar' => 'Jam Keluar',
			'datetime_masuk' => 'Datetime Masuk',
			'datetime_keluar' => 'Datetime Keluar',
			'tanggal_masuk_ird' => 'Tangal Masuk IRD',
			'jam_masuk_ird' => 'Jam Masuk IRD',
			'datetime_masuk_ird' => 'Datetime Masuk IRD',
			'tanggal_keluar_ird' => 'Tangal Keluar IRD',
			'jam_keluar_ird' => 'Jam Keluar IRD',
			'datetime_keluar_ird' => 'Datetime Keluar IRD',
			'created' => 'Created',
			'kamar_id' => 'Kamar',
			'pasien_id' => 'Pasien',
			'jenis_pasien' => 'Jenis Pasien',
			'dokter_id' => 'Dokter',
			'biaya_paket_1' => 'Paket 1',
			'biaya_paket_2' => 'Paket 2',
			'biaya_paket_3' => 'Paket 3',
			'status_inap' => 'Status Inap',
			'TANGGAL_AWAL' => 'tanggal awal',
			'TANGGAL_AKHIR' => 'tanggal akhir',
			'status_rawat' => 'Status Rawat',
			'tanggal_pulang' => 'Tanggal Pulang',
			'jam_pulang' => 'Jam Pulang',
			'datetime_pulang' => 'Datetime Pulang',
			'prev_kamar' => 'Prev Kamar',
			'next_kamar' => 'Next Kamar',
			'jenis_ird' => 'IRD',
			'status_pasien' => 'Status Pasien',
			'is_tarif_kamar_penuh' => 'Tarif Penuh'
		);
	}

	public function sumOp($dokter_id)
	{
		$dpjp = $dokter_id;
		$biaya_top_jm = 0;
		foreach($this->trRawatInapTops as $top)
		{
			if($top->top->kode_tindakan == 'JMO')
			{
				foreach($this->trRawatInapTopJm as $vi)
		        {
		          if(!empty($vi->id_dokter) && $vi->id_dokter == $dpjp)
		          { 	

		            $biaya_top_jm = $biaya_top_jm + $vi->nilai;
		           	
		          } 
		        }	

		        
		    }
		}
		
		
		return $biaya_top_jm;
	}

	public function countRaber($dokter_id)
	{
		$count = 0;
		$dpjp = $this->dokter_id;

		if($dokter_id == $dpjp)
		{

			foreach($this->tRIVisiteDokters as $item)
			{
				if(!empty($item->dokterIrna) && $dpjp != $item->id_dokter_irna)
				{
					if($item->dokterIrna->jenis_dokter == 'UMUM' && $item->jenisVisite->nama_visite == 'Visite Dokter Ahli')
					{
						$count++;
					}
				}	
			}
		}
		return $count;
	}

	public function cekVisiteAhli($id_rawat_inap,$id_dokter)
	{
		$dataVisite = JenisVisite::model()->findAllByAttributes(array('nama_visite'=>'Visite Dokter Ahli'));
	    $listVisite = array();
	    foreach($dataVisite as $dv)
		{
			$listVisite[] = $dv->id_jenis_visite;
		}
		$criteria=new CDbCriteria;

		
		$str = implode (", ", $listVisite);
		$criteria->condition = "id_rawat_inap = :p1 AND id_dokter_irna = :p2 AND id_jenis_visite IN (".$str.")";

		$criteria->params = array(
			':p1'=>$id_rawat_inap,
			':p2' => $id_dokter,
			
		);
		// print_r($criteria->condition);exit;

		$visiteDokter = TrRawatInapVisiteDokter::model()->findAll($criteria);

		$isnotfound = empty($visiteDokter);
		
		
		return $isnotfound;
	}

	public function cekDokterVisite($id_rawat_inap,$id_dokter)
	{
		// $dataVisite = JenisVisite::model()->findAllByAttributes(array('nama_visite'=>'Visite Dokter Ahli'));
	 //    $listVisite = array();
	 //    foreach($dataVisite as $dv)
		// {
		// 	$listVisite[] = $dv->id_jenis_visite;
		// }

		$visiteDokter = TrRawatInapVisiteDokter::model()->findAllByAttributes(array('id_rawat_inap'=>$id_rawat_inap,'id_dokter_irna'=>$id_dokter));

		$isnotfound = empty($visiteDokter);
		

		return $isnotfound;
	}

	
	public function getDataVisite($tindakan)
	{
		
		$hasil = [];

		$criteria = new CDbCriteria; 
		$criteria->addCondition("id_rawat_inap=".$this->id_rawat_inap);
		$criteria->order = 'dokterIrna.jenis_dokter ASC';
		$criteria->with = array('dokterIrna');
		$criteria->together = true;

		$trVisiteDokter = TrRawatInapVisiteDokter::model()->findAll($criteria);

		foreach($trVisiteDokter as $visite)
      	{
      		if($visite->jenisVisite->kode_visite == $tindakan)
      			$hasil[] = $visite;
      	}

      	return $hasil;
	}

	public function searchMinimized()
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tanggal_keluar', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR, 'AND');
		$criteria->compare('status_pasien',1);
		// $criteria->with = array('kamar','kamar.kelas');
		// $criteria->together = true;
		$criteria->order = 'tanggal_keluar ASC';
		

		return TrRawatInap::model()->findAll($criteria);
	}


	public function searchDokter()
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tanggal_keluar', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR, 'AND');
		$criteria->compare('status_pasien',1);
		$criteria->order = 'kamar.nama_kamar ASC, kelas.nama_kelas ASC';
		$criteria->with = array('kamar','kamar.kelas');
		$criteria->together = true;
		

		return TrRawatInap::model()->findAll($criteria);
	}

	public function searchDokterPerKamar()
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tanggal_keluar', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR, 'AND');
		$criteria->compare('status_pasien',1);
		if(!empty($this->kamar_id))
			$criteria->addCondition("kamar_id=".$this->kamar_id);



		return TrRawatInap::model()->findAll($criteria);
	}

	public function searchKonsulDokter($jenis_pasien,$id_dokter, $tgl_awal, $tgl_akhir)
	{
		 $db  = Yii::app()->db->createCommand();
               $db->select('SUM(jm.biaya_visite_irna) as nilai, t.id_rawat_inap');
               $db->from('tr_rawat_inap t');
               $db->join("tr_rawat_inap_visite_dokter jm","jm.id_rawat_inap = t.id_rawat_inap");
               $db->join("jenis_visite v","jm.id_jenis_visite = v.id_jenis_visite");
               if($jenis_pasien!='-')
               		$db->join("gol_pasien g","g.KodeGol=t.jenis_pasien");
               $db->where("t.tanggal_keluar between '".$tgl_awal."' and '".$tgl_akhir."'");
               $db->andWhere("jm.id_dokter_irna = ".$id_dokter);
               $db->andWhere("v.kode_visite='KONSUL'");
               if($jenis_pasien!='-')
               	$db->andWhere("g.NamaGol='".$jenis_pasien."'");

               $db->andWhere('t.status_pasien=1');
         $result = $db->queryRow();

        return (object)$result;
	}

	public function searchVisiteDokter($jenis_pasien,$id_dokter, $tgl_awal, $tgl_akhir)
	{
		 $db = Yii::app()->db->createCommand();
               $db->select('SUM(jm.biaya_visite_irna) as nilai, jm.id_dokter_irna');
               $db->from('tr_rawat_inap t');
               $db->join("tr_rawat_inap_visite_dokter jm","jm.id_rawat_inap = t.id_rawat_inap");
               $db->join("jenis_visite v","jm.id_jenis_visite = v.id_jenis_visite");
               if($jenis_pasien!='-')
	               $db->join("gol_pasien g","g.KodeGol=t.jenis_pasien");

               $db->where("t.tanggal_keluar between '".$tgl_awal."' and '".$tgl_akhir."'");
               $db->andWhere("jm.id_dokter_irna = ".$id_dokter);
               $db->andWhere("v.kode_visite='VISITE'");
               $db->andWhere('t.status_pasien=1');
               if($jenis_pasien!='-')
               		$db->andWhere("g.NamaGol='".$jenis_pasien."'");
           $result =    $db->queryRow();

        return (object)$result;
	}

	public function searchRekapDokter($jenis_pasien,$id_dokter, $tgl_awal, $tgl_akhir)
	{
		 $db = Yii::app()->db->createCommand();
               $db->select('t.*, d.`nama_dokter`, SUM(jm.nilai) as nilai,jm.id_dokter');
               $db->from('tr_rawat_inap t');
               $db->join("tr_rawat_inap_top_jm jm","jm.id_rawat_inap = t.id_rawat_inap");
               if($jenis_pasien!='-')
               		$db->join("gol_pasien g","g.KodeGol=t.jenis_pasien");
               
               $db->leftJoin("dm_dokter d","d.id_dokter = jm.id_dokter");
               $db->where("t.tanggal_keluar between '".$tgl_awal."' and '".$tgl_akhir."'");
               $db->andWhere("jm.id_dokter = ".$id_dokter);
               if($jenis_pasien!='-')
               		$db->andWhere("g.NamaGol='".$jenis_pasien."'");
             
             	$db->andWhere('t.status_pasien=1');
             $result =  $db->queryRow();

        return $result;
	}



	public function searchLaporanPerDokter($id_dokter=0)
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tanggal_keluar', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR, 'AND');
		if(!empty($this->kamar_id))
			$criteria->addCondition("kamar_id=".$this->kamar_id);

		if(!empty($this->TIPE_PASIEN))
			$criteria->compare("jenis_pasien",$this->TIPE_PASIEN);

		$criteria->addCondition("status_pasien=1");
		if($id_dokter != 0)
			$criteria->addCondition("dokter.id_dokter=".$id_dokter);
		$criteria->order = 'nama_dokter ASC';
		$criteria->with = array('trRawatInapTopJm.dokter');
		$criteria->together = true;

		return TrRawatInap::model()->findAll($criteria);
	}

	public function searchLaporan()
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tanggal_keluar', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR, 'AND');
		if(!empty($this->kamar_id))
			$criteria->addCondition("kamar_id=".$this->kamar_id);

		if(!empty($this->TIPE_PASIEN))
			$criteria->compare("jenis_pasien",$this->TIPE_PASIEN);

		if(!empty($this->KAMAR_PASIEN))
			$criteria->compare("kamar_id",$this->KAMAR_PASIEN);			
		


		$criteria->addCondition("status_pasien=1");
		$criteria->order = 'jenis_pasien ASC, nama_kelas ASC';
		$criteria->with = array('kamar','kamar.kelas');
		$criteria->together = true;
		return TrRawatInap::model()->findAll($criteria);
	}

	public function searchLaporanAll()
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tanggal_keluar', $this->TANGGAL_AWAL, $this->TANGGAL_AKHIR, 'AND');

		$criteria->addCondition("status_pasien=1 ");
		
		$criteria->with = array('kamar','pASIEN');
		$criteria->together = true;
		$criteria->order = 'pASIEN.NAMA ASC';
		return TrRawatInap::model()->findAll($criteria);
	}

	public function searchLaporanAllPerKamar($tgl_awal, $tgl_akhir, $kamar_id)
	{
		$criteria = new CDbCriteria; 
		$criteria->addBetweenCondition('tanggal_keluar', $tgl_awal, $tgl_akhir, 'AND');

		if(!empty($kamar_id))
			$criteria->addCondition("status_pasien=1 AND kamar.kamar_master_id=".$kamar_id);
			
		$criteria->with = array('kamar','pASIEN','kamar.kamarMaster');
		$criteria->together = true;
		$criteria->order = 'tanggal_keluar ASC';
		return TrRawatInap::model()->findAll($criteria);
	}

	public function searchAlkesObat($id)
	{
		$criteria = new CDbCriteria; 
		
		$criteria->addCondition("id_rawat_inap=".$id." AND kode_alkes='OBAT'");
		return TrRawatInapAlkesObat::model()->findAll($criteria);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$akses = array(
			WebUser::R_SA,
			WebUser::R_OP_KASIR,
			WebUser::R_OP_KEUANGAN,
			WebUser::R_OP_FARMASI,
			WebUser::R_OP_RADIOLOGI,
			WebUser::R_OP_RR
		);

		

		// @todo Please modify the following code to remove attributes that should not be searched.
		$sort = new CSort();
		$criteria=new CDbCriteria;
		$user_kelas = Yii::app()->user->getState('username');
		
		// $criteria->addSearchCondition('kamar.nama_kamar',$this->SEARCH,true,'OR');
		// $user_kelas = Yii::app()->user->getState('username');
		switch ($this->SEARCH_BY) {
			case 1:

				$criteria->compare('pasien_id',trim($this->SEARCH));
				
				break;
			case 2:
				// if(!Yii::app()->user->checkAccess($akses))
				// {
					
				// 	$criteria->with = ['pASIEN','kamar'];
				// 	$criteria->together = true;
				// 	$criteria->addSearchCondition('pASIEN.NAMA',$this->SEARCH,true,'OR');
				// 	$criteria->compare('kamar.user_kamar',$user_kelas);
				// }

				// else
				// {

					$criteria->with = array('pASIEN');
					$criteria->together = true;
					$criteria->addSearchCondition('pASIEN.NAMA',$this->SEARCH,true,'OR');
				// }

				// $criteria->addSearchCondition('pASIEN.NAMA',$this->SEARCH,true,'OR');
			
				
				
				
			break;
			default:
				// if(!Yii::app()->user->checkAccess($akses))
				// {
				// 	$user_kelas = Yii::app()->user->getState('username');
				// 	$criteria->with = ['kamar'];
				// 	$criteria->compare('kamar.user_kamar',$user_kelas);
				// }
				// $criteria->compare('pasien_id',trim($this->SEARCH));
			break;

		}

		


		if(!empty($this->TIPE_PASIEN))
			$criteria->compare("jenis_pasien",$this->TIPE_PASIEN);

		if(!empty($this->kamar_id))
			$criteria->compare("kamar_id",$this->kamar_id);

		

		$criteria->addCondition("status_pasien=1");
		$criteria->order = 'datetime_masuk DESC';
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,
				
			),
		));
	}

	public function searchDistinct()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		
		$criteria->addSearchCondition('pASIEN.NoMedrec',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('datetime_masuk',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('datetime_keluar',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('pASIEN.NAMA',$this->SEARCH,true,'OR');
		$criteria->compare('status_inap',1);
		$criteria->compare('status_pasien',1);
		$criteria->with = array('pASIEN');
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function checkSudahInap($attribute,$params)
	{
	    $model = TrRawatInap::model()->findByAttributes(array('pasien_id' =>$this->pasien_id,'status_inap'=>1,'status_pasien'=>1));
	    if(!empty($model)){
	    	$this->addError($attribute, 'Pasien ini sedang dirawat di kamar '.$model->kamar->nama_kamar);
	    }
	}

	public function cekRawatHariSama($attribute,$params)
	{
	    $model = TrRawatInap::model()->findByAttributes([
	    	'pasien_id' =>$this->pasien_id,
	    	'tanggal_masuk'=>$this->tanggal_masuk,
	    	'kamar_id'=>$this->kamar_id,
	    	'status_pasien' => 1
	    ]);
	    if(!empty($model)){
	    	$this->addError($attribute, 'Data Pasien ini sudah diinput di hari yang sama');
	    }
	}

	public function getPostMRS($rm)
	{
		$model = TrRawatInap::model()->findByAttributes(array('pasien_id'=>$rm),array('order'=>'tanggal_keluar DESC'));

		return $model;	
	}

	protected function afterFind()
	{

		// $criteria = new CDbCriteria;
		// $criteria->order = 'created DESC';
		// $criteria->limit = 1;

		// $kamar = TrRawatInapKamarHistory::model()->find($criteria);

		// if(!empty($kamar)){
		// 	$this->Kamar = $kamar->kamar;
		// 	$this->KAMAR_SEKARANG = $kamar->kamar->nama_kamar;
		// }

		// if(!empty($this->trRawatInapRincians))
		// 	$this->SUBTOTAL = Yii::app()->helper->formatRupiah($this->trRawatInapRincians->subtotal);


		$this->tanggal_masuk = date('d/m/Y', strtotime($this->tanggal_masuk));
		$this->tanggal_masuk_ird = date('d/m/Y', strtotime($this->tanggal_masuk_ird));

		switch ($this->status_inap) {
			case 0:
				$this->STATUS_INAP_PASIEN = 'Keluar Kamar';
				break;
			case 1 :
				$this->STATUS_INAP_PASIEN = 'Sedang dirawat';
				break;
			case 2 :
				$this->STATUS_INAP_PASIEN = 'Pulang';
				break;
			
		}

		if(!empty($this->tanggal_keluar))
			$this->tanggal_keluar = date('d/m/Y', strtotime($this->tanggal_keluar));

		if(!empty($this->tanggal_keluar_ird))
			$this->tanggal_keluar_ird = date('d/m/Y', strtotime($this->tanggal_keluar_ird));
		
		return parent::afterFind();
	}

	public function updateHargaObatPasien($id)
	{
		$total = 0;
		foreach(self::searchAlkesObat($id) as $obat)
		{
			$total += $obat->nilai;
			
		}

		$rit = TrRawatInapAlkes::model()->findByAttributes(array('id_rawat_inap'=>$id));
		$rit->biaya_irna = $total;
		$rit->save();

	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatInap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
