<?php

/**
 * This is the model class for table "tr_rawat_jalan_alkes".
 *
 * The followings are the available columns in table 'tr_rawat_jalan_alkes':
 * @property integer $id_rj_alkes
 * @property integer $id_alkes
 * @property double $biaya_ird
 * @property integer $jumlah_tindakan
 * @property string $created
 * @property integer $id_rawat_jalan
 *
 * The followings are the available model relations:
 * @property ObatAlkes $idAlkes
 * @property TrPendaftaranRjalan $idRawatJalan
 */
class TrRawatJalanAlkes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tr_rawat_jalan_alkes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_alkes, biaya_ird, jumlah_tindakan, id_rawat_jalan', 'required'),
			array('id_alkes, jumlah_tindakan, id_rawat_jalan', 'numerical', 'integerOnly'=>true),
			array('biaya_ird', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_rj_alkes, id_alkes, biaya_ird, jumlah_tindakan, created, id_rawat_jalan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'alkes' => array(self::BELONGS_TO, 'ObatAlkes', 'id_alkes'),
			'rawatJalan' => array(self::BELONGS_TO, 'TrPendaftaranRjalan', 'id_rawat_jalan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_rj_alkes' => 'Id Rj Alkes',
			'id_alkes' => 'Id Alkes',
			'biaya_ird' => 'Biaya Ird',
			'jumlah_tindakan' => 'Jumlah Tindakan',
			'created' => 'Created',
			'id_rawat_jalan' => 'Id Rawat Jalan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_rj_alkes',$this->id_rj_alkes);
		$criteria->compare('id_alkes',$this->id_alkes);
		$criteria->compare('biaya_ird',$this->biaya_ird);
		$criteria->compare('jumlah_tindakan',$this->jumlah_tindakan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('id_rawat_jalan',$this->id_rawat_jalan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrRawatJalanAlkes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
