<?php

/**
 * This is the model class for table "erp_sales_stok_gudang".
 *
 * The followings are the available columns in table 'erp_sales_stok_gudang':
 * @property integer $id_stok
 * @property integer $id_gudang
 * @property integer $id_barang
 * @property double $jumlah
 * @property string $created
 * @property integer $is_hapus
 * @property string $exp_date
 * @property string $batch_no
 * @property string $faktur_barang_id
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property ErpRequestOrderItem[] $erpRequestOrderItems
 * @property ErpSalesMasterBarang $idBarang
 * @property ErpSalesMasterGudang $idGudang
 */
class ErpSalesStokGudang extends MyActiveRecord
{

	public $SEARCH;
	public $PAGE_SIZE = 10;

	public function getDbConnection()
    {
        return self::getErpDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'erp_sales_stok_gudang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_gudang, id_barang, jumlah, created', 'required'),
			array('id_gudang, id_barang, is_hapus', 'numerical', 'integerOnly'=>true),
			array('jumlah', 'numerical'),
			array('exp_date', 'length', 'max'=>60),
			array('batch_no, faktur_barang_id', 'length', 'max'=>50),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_stok, id_gudang, id_barang, jumlah, created, is_hapus, exp_date, batch_no, faktur_barang_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'erpRequestOrderItems' => array(self::HAS_MANY, 'ErpRequestOrderItem', 'stok_id'),
			'idBarang' => array(self::BELONGS_TO, 'ErpSalesMasterBarang', 'id_barang'),
			'idGudang' => array(self::BELONGS_TO, 'ErpSalesMasterGudang', 'id_gudang'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_stok' => 'Id Stok',
			'id_gudang' => 'Id Gudang',
			'id_barang' => 'Id Barang',
			'jumlah' => 'Jumlah',
			'created' => 'Created',
			'is_hapus' => 'Is Hapus',
			'exp_date' => 'Exp Date',
			'batch_no' => 'Batch No',
			'faktur_barang_id' => 'Faktur Barang',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$sort = new CSort;

		$criteria->addSearchCondition('id_stok',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('id_gudang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('id_barang',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('jumlah',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('is_hapus',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('exp_date',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('batch_no',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('faktur_barang_id',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('created_at',$this->SEARCH,true,'OR');
		$criteria->addSearchCondition('updated_at',$this->SEARCH,true,'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>$this->PAGE_SIZE,

			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ErpSalesStokGudang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
