<?php

/**
 * This is the model class for table "td_pasien_masuk".
 *
 * The followings are the available columns in table 'td_pasien_masuk':
 * @property string $Kode_Detail
 * @property string $Kode_TMPM
 * @property string $No_RM
 * @property string $Nama_pasien
 * @property string $Alamat
 * @property string $Ruang
 * @property string $bed
 * @property string $Dokter
 * @property string $Kelas
 * @property string $UPF
 * @property string $Jenis_Pasien
 * @property string $Tgl_MRS
 * @property string $waktu_daftar
 * @property string $Tgl_KRS
 * @property string $Status_Pasien
 * @property string $Ket_Pulang
 * @property string $Ket_Masuk
 * @property string $Jenis_kelamin
 * @property integer $Lama_Dirawat
 * @property integer $Hari_Perawatan
 * @property string $Tgl_Setor
 * @property string $Status_Berkas
 * @property string $umur
 * @property string $rjri
 * @property string $tgl_kunj_trakhir
 * @property string $post_mrs
 * @property string $status_bpjs
 * @property string $tgl_pindah
 * @property string $kelas_kemkes
 * @property string $kelas_bpjs
 * @property string $ruang_kemkes
 * @property string $antrian
 * @property string $pindahan_titipan
 * @property string $kelas_pasien
 * @property string $jenis_klaim
 * @property string $diagnosa
 */
class TdPasienMasuk extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'td_pasien_masuk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Kode_Detail, Kode_TMPM, No_RM, Nama_pasien, Alamat, Ruang, bed, Dokter, Kelas, UPF, Jenis_Pasien, Tgl_MRS, waktu_daftar, Status_Pasien, Ket_Pulang, Ket_Masuk, Jenis_kelamin, Status_Berkas, umur, tgl_kunj_trakhir, post_mrs, status_bpjs, tgl_pindah', 'required'),
			array('Lama_Dirawat, Hari_Perawatan', 'numerical', 'integerOnly'=>true),
			array('Kode_Detail, Kode_TMPM, Ruang, bed, Kelas, UPF, Jenis_Pasien, Ket_Pulang, Ket_Masuk, post_mrs, status_bpjs, kelas_kemkes, kelas_bpjs, ruang_kemkes, kelas_pasien, jenis_klaim', 'length', 'max'=>100),
			array('No_RM, Jenis_kelamin, rjri, pindahan_titipan', 'length', 'max'=>50),
			array('Nama_pasien', 'length', 'max'=>220),
			array('Alamat', 'length', 'max'=>80),
			array('Dokter', 'length', 'max'=>250),
			array('Status_Pasien, Status_Berkas, umur', 'length', 'max'=>25),
			array('antrian', 'length', 'max'=>10),
			array('diagnosa', 'length', 'max'=>300),
			array('Tgl_KRS, Tgl_Setor', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Kode_Detail, Kode_TMPM, No_RM, Nama_pasien, Alamat, Ruang, bed, Dokter, Kelas, UPF, Jenis_Pasien, Tgl_MRS, waktu_daftar, Tgl_KRS, Status_Pasien, Ket_Pulang, Ket_Masuk, Jenis_kelamin, Lama_Dirawat, Hari_Perawatan, Tgl_Setor, Status_Berkas, umur, rjri, tgl_kunj_trakhir, post_mrs, status_bpjs, tgl_pindah, kelas_kemkes, kelas_bpjs, ruang_kemkes, antrian, pindahan_titipan, kelas_pasien, jenis_klaim, diagnosa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Kode_Detail' => 'Kode Detail',
			'Kode_TMPM' => 'Kode Tmpm',
			'No_RM' => 'No Rm',
			'Nama_pasien' => 'Nama Pasien',
			'Alamat' => 'Alamat',
			'Ruang' => 'Ruang',
			'bed' => 'Bed',
			'Dokter' => 'Dokter',
			'Kelas' => 'Kelas',
			'UPF' => 'Upf',
			'Jenis_Pasien' => 'Jenis Pasien',
			'Tgl_MRS' => 'Tgl Mrs',
			'waktu_daftar' => 'Waktu Daftar',
			'Tgl_KRS' => 'Tgl Krs',
			'Status_Pasien' => 'Status Pasien',
			'Ket_Pulang' => 'Ket Pulang',
			'Ket_Masuk' => 'Ket Masuk',
			'Jenis_kelamin' => 'Jenis Kelamin',
			'Lama_Dirawat' => 'Lama Dirawat',
			'Hari_Perawatan' => 'Hari Perawatan',
			'Tgl_Setor' => 'Tgl Setor',
			'Status_Berkas' => 'Status Berkas',
			'umur' => 'Umur',
			'rjri' => 'Rjri',
			'tgl_kunj_trakhir' => 'Tgl Kunj Trakhir',
			'post_mrs' => 'Post Mrs',
			'status_bpjs' => 'Status Bpjs',
			'tgl_pindah' => 'Tgl Pindah',
			'kelas_kemkes' => 'Kelas Kemkes',
			'kelas_bpjs' => 'Kelas Bpjs',
			'ruang_kemkes' => 'Ruang Kemkes',
			'antrian' => 'Antrian',
			'pindahan_titipan' => 'Pindahan Titipan',
			'kelas_pasien' => 'Kelas Pasien',
			'jenis_klaim' => 'Jenis Klaim',
			'diagnosa' => 'Diagnosa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */

	public function searchLimit($limit=10)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		// $criteria->compare('StatusPasien','1(PasienMasuk)'):
		$criteria->limit = $limit;
		$criteria->order = 'Tgl_MRS DESC';
		
		return TdPasienMasuk::model()->findAll($criteria);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Kode_Detail',$this->Kode_Detail,true);
		$criteria->compare('Kode_TMPM',$this->Kode_TMPM,true);
		$criteria->compare('No_RM',$this->No_RM,true);
		$criteria->compare('Nama_pasien',$this->Nama_pasien,true);
		$criteria->compare('Alamat',$this->Alamat,true);
		$criteria->compare('Ruang',$this->Ruang,true);
		$criteria->compare('bed',$this->bed,true);
		$criteria->compare('Dokter',$this->Dokter,true);
		$criteria->compare('Kelas',$this->Kelas,true);
		$criteria->compare('UPF',$this->UPF,true);
		$criteria->compare('Jenis_Pasien',$this->Jenis_Pasien,true);
		$criteria->compare('Tgl_MRS',$this->Tgl_MRS,true);
		$criteria->compare('waktu_daftar',$this->waktu_daftar,true);
		$criteria->compare('Tgl_KRS',$this->Tgl_KRS,true);
		$criteria->compare('Status_Pasien',$this->Status_Pasien,true);
		$criteria->compare('Ket_Pulang',$this->Ket_Pulang,true);
		$criteria->compare('Ket_Masuk',$this->Ket_Masuk,true);
		$criteria->compare('Jenis_kelamin',$this->Jenis_kelamin,true);
		$criteria->compare('Lama_Dirawat',$this->Lama_Dirawat);
		$criteria->compare('Hari_Perawatan',$this->Hari_Perawatan);
		$criteria->compare('Tgl_Setor',$this->Tgl_Setor,true);
		$criteria->compare('Status_Berkas',$this->Status_Berkas,true);
		$criteria->compare('umur',$this->umur,true);
		$criteria->compare('rjri',$this->rjri,true);
		$criteria->compare('tgl_kunj_trakhir',$this->tgl_kunj_trakhir,true);
		$criteria->compare('post_mrs',$this->post_mrs,true);
		$criteria->compare('status_bpjs',$this->status_bpjs,true);
		$criteria->compare('tgl_pindah',$this->tgl_pindah,true);
		$criteria->compare('kelas_kemkes',$this->kelas_kemkes,true);
		$criteria->compare('kelas_bpjs',$this->kelas_bpjs,true);
		$criteria->compare('ruang_kemkes',$this->ruang_kemkes,true);
		$criteria->compare('antrian',$this->antrian,true);
		$criteria->compare('pindahan_titipan',$this->pindahan_titipan,true);
		$criteria->compare('kelas_pasien',$this->kelas_pasien,true);
		$criteria->compare('jenis_klaim',$this->jenis_klaim,true);
		$criteria->compare('diagnosa',$this->diagnosa,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TdPasienMasuk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
