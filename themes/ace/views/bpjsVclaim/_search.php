<?php
/* @var $this BpjsVclaimController */
/* @var $model BpjsVclaim */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'noSep'); ?>
		<?php echo $form->textField($model,'noSep',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tglMasuk'); ?>
		<?php echo $form->textField($model,'tglMasuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tglKeluar'); ?>
		<?php echo $form->textField($model,'tglKeluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jaminan'); ?>
		<?php echo $form->textField($model,'jaminan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'poli'); ?>
		<?php echo $form->textField($model,'poli',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ruangRawat'); ?>
		<?php echo $form->textField($model,'ruangRawat',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kelasRawat'); ?>
		<?php echo $form->textField($model,'kelasRawat',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'spesialistik'); ?>
		<?php echo $form->textField($model,'spesialistik',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'caraKeluar'); ?>
		<?php echo $form->textField($model,'caraKeluar',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kondisiPulang'); ?>
		<?php echo $form->textField($model,'kondisiPulang',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tindakLanjut'); ?>
		<?php echo $form->textField($model,'tindakLanjut',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kodePPK'); ?>
		<?php echo $form->textField($model,'kodePPK',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tglKontrol'); ?>
		<?php echo $form->textField($model,'tglKontrol'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_poli'); ?>
		<?php echo $form->textField($model,'kode_poli',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DPJP'); ?>
		<?php echo $form->textField($model,'DPJP',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user'); ?>
		<?php echo $form->textField($model,'user',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_lpk'); ?>
		<?php echo $form->textField($model,'kode_lpk',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->