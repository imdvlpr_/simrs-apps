<?php 
$this->breadcrumbs=array(
  array('name' => 'BpjsVclaim','url'=>Yii::app()->createUrl('BpjsVclaim/index')),
                            array('name' => 'Update')
);
?>
<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>


<div class="row">
<div class="col-xs-12">
	
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
</div>
