<?php
/* @var $this BpjsVclaimController */
/* @var $model BpjsVclaim */
$this->breadcrumbs=array(
   array('name' => 'BpjsVclaim','url'=>Yii::app()->createUrl('BpjsVclaim/index')),
                            array('name' => 'List')
);
?>

<h1>Manage Bpjs Vclaim</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#bpjs-vclaim-grid').yiiGridView.update('bpjs-vclaim-grid', {
			    url:'<?php echo Yii::app()->createUrl("BpjsVclaim/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        <!-- <div class="span3" id="sidebar">


        </div> -->

        <!--/span-->
    
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>


                        <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('BpjsVclaim/create'),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('BpjsVclaim[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size','size'=>1)); 
                            ?> 
                             <?php        echo CHtml::textField('BpjsVclaim[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                 

<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'bpjs-vclaim-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		'id',
		'noSep',
		'tglMasuk',
		'tglKeluar',
		'jaminan',
		'poli',
		/*
		'ruangRawat',
		'kelasRawat',
		'spesialistik',
		'caraKeluar',
		'kondisiPulang',
		'tindakLanjut',
		'kodePPK',
		'tglKontrol',
		'kode_poli',
		'DPJP',
		'user',
		'kode_lpk',
		'created',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									    'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>

</div>
