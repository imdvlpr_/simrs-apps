<?php
/* @var $this BpjsVclaimController */
/* @var $model BpjsVclaim */

$this->breadcrumbs=array(
	'Bpjs Vclaims'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List BpjsVclaim', 'url'=>array('index')),
	array('label'=>'Create BpjsVclaim', 'url'=>array('create')),
	array('label'=>'Update BpjsVclaim', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BpjsVclaim', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BpjsVclaim', 'url'=>array('admin')),
);
?>

<h1>View BpjsVclaim #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'noSep',
		'tglMasuk',
		'tglKeluar',
		'jaminan',
		'poli',
		'ruangRawat',
		'kelasRawat',
		'spesialistik',
		'caraKeluar',
		'kondisiPulang',
		'tindakLanjut',
		'kodePPK',
		'tglKontrol',
		'kode_poli',
		'DPJP',
		'user',
		'kode_lpk',
		'created',
	),
)); ?>
