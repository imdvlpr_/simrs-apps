<?php
/* @var $this BpjsVclaimController */
/* @var $model BpjsVclaim */
/* @var $form CActiveForm */
?>

<style type="text/css">
	 .tgl_picker{
    background : url(<?php echo Yii::app()->baseUrl;?>/images/calendar-icon.png) no-repeat scroll 7px 7px;
    text-indent:25px;
</style>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpjs-vclaim-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class' => 'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="col-xs-4">
		<div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Form 1</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
          	<div class="form-group">
		<?php echo $form->labelEx($model,'noSep',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'noSep',array('class'=>'input-sm')); ?>
		<?php echo $form->error($model,'noSep'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tglMasuk',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		
		echo $form->textField($model,'tglMasuk',array('class'=>'date-picker input-sm','data-date-format'=>'dd/mm/yyyy')); 
		?>
		<?php echo $form->error($model,'tglMasuk'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tglKeluar',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 

		echo $form->textField($model,'tglKeluar',array('class'=>'date-picker input-sm','data-date-format'=>'dd/mm/yyyy')); 
		
		 ?>
		<?php echo $form->error($model,'tglKeluar'); ?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'kelasRawat',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		

		echo $form->dropDownList($model,'kelasRawat',CHtml::ListData(BpjsMasterRef::model()->findAllByAttributes(array('jenis'=>'kelasrawat')),'kode','nama'),array('class'=>'input-sm')); 
		?>
		<?php echo $form->error($model,'kelasRawat'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'ruangRawat',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'ruangRawat',CHtml::ListData(BpjsMasterRef::model()->findAllByAttributes(array('jenis'=>'ruangrawat')),'kode','nama'),array('class'=>'input-sm')); 
		?>
		<?php echo $form->error($model,'ruangRawat'); ?>
		</div>
	</div>
	
          </div>
      </div>
  </div>
	

</div>
<div class="col-xs-4">
		<div class="widget-box widget-color-blue2">
	        <div class="widget-header">
	          <h4 class="widget-title lighter smaller">Form 2</h4>
	        </div>

	        <div class="widget-body">
	          <div class="widget-main">
	          	<div class="form-group">
		<?php echo $form->labelEx($model,'jaminan',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jaminan'); ?>
		<?php echo $form->error($model,'jaminan'); ?>
		</div>
	</div>
		<div class="form-group">
		<?php echo $form->labelEx($model,'poli',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
	    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
          'value' =>!empty($_POST['poli_NAMA'])? $_POST['poli_NAMA'] : '',
          'name' => 'poli_NAMA',
          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('WSBpjs/getRefVclaim').'",
                    dataType: "json",
                    data: {
                        param: request.term,
                        ref : "poli"
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
          'htmlOptions'=>array('placeholder'=>'Ketik Nama/Kode Poli'),    
          'options' => array(
              'minLength' => 3,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 
                isiDataPasien(ui,"poli");
              }',
      ),
      ));
		echo $form->hiddenField($model,'poli'); 

		?>
		<?php echo $form->error($model,'poli'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'spesialistik',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'spesialistik',CHtml::ListData(BpjsMasterRef::model()->findAllByAttributes(array('jenis'=>'spesialistik')),'kode','nama'),array('class'=>'input-large')); 
		?>
		<?php echo $form->error($model,'spesialistik'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'caraKeluar',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'caraKeluar',CHtml::ListData(BpjsMasterRef::model()->findAllByAttributes(array('jenis'=>'caraKeluar')),'kode','nama'),array('class'=>'input-large')); 
		?>
		<?php echo $form->error($model,'caraKeluar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'DPJP',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php
		 $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
          'value' =>!empty($_POST['dpjp_NAMA'])? $_POST['dpjp_NAMA'] : '',
          'name' => 'dpjp_NAMA',
          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('WSBpjs/getRefVclaim').'",
                    dataType: "json",
                    data: {
                        param: request.term,
                        ref : "dokter"
                    },
                    beforeSend: function(){
                    	toggleLoading("#dpjp_nama",true);
                    },
                    success: function (data) {
                    	toggleLoading("#dpjp_nama",false);
                        response(data);
                    }
                })
            }',
          'htmlOptions'=>array('placeholder'=>'Ketik Nama Dokter','id'=>'dpjp_nama'),    
          'options' => array(
              'minLength' => 3,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 
                isiDataPasien(ui,"DPJP");
              }',
      ),
      ));
		 echo $form->hiddenField($model,'DPJP',array('size'=>60,'maxlength'=>255)); 
		 ?>

		<?php echo $form->error($model,'DPJP'); ?>
		</div>
	</div>
	          </div>
	      </div>
	  </div>
	

	
</div>
<div class="col-xs-4">
<div class="widget-box widget-color-blue2">
    <div class="widget-header">
      <h4 class="widget-title lighter smaller">Form 3</h4>
    </div>

    <div class="widget-body">
      <div class="widget-main">
      	<div class="form-group">
		<?php echo $form->labelEx($model,'kondisiPulang',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'kondisiPulang',CHtml::ListData(BpjsMasterRef::model()->findAllByAttributes(array('jenis'=>'pascapulang')),'kode','nama'),array('class'=>'input-md')); 
		?>
		<?php echo $form->error($model,'kondisiPulang'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tindakLanjut',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'tindakLanjut',CHtml::ListData(BpjsMasterRef::model()->findAllByAttributes(array('jenis'=>'tindaklanjut')),'kode','nama'),array('class'=>'input-md')); 
		?>
		<?php echo $form->error($model,'tindakLanjut'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'kodePPK',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kodePPK',array('class'=>'input-md')); ?>
		<?php echo $form->error($model,'kodePPK'); ?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'tglKontrol',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		
		echo $form->textField($model,'tglKontrol',array('class'=>'date-picker input-sm','data-date-format'=>'dd/mm/yyyy')); 
		?>
		<?php echo $form->error($model,'tglKontrol'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'kode_poli',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		 $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
          'value' =>!empty($_POST['kode_poli_NAMA'])? $_POST['kode_poli_NAMA'] : '',
          'name' => 'kode_poli_NAMA',
          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('WSBpjs/getRefVclaim').'",
                    dataType: "json",
                    data: {
                        param: request.term,
                        ref : "poli"
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
          'htmlOptions'=>array('placeholder'=>'Ketik Nama/Kode Poli'),    
          'options' => array(
              'minLength' => 3,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 
                isiDataPasien(ui,"kode_poli");
              }',
      ),
      ));
		echo $form->hiddenField($model,'kode_poli'); 
		?>
		<?php echo $form->error($model,'kode_poli'); ?>
		</div>
	</div>
      </div>
  </div>
</div>
	

	
</div>
<div class="row">
	<div class="col-xs-6">
		<div class="widget-box widget-color-blue2">
		    <div class="widget-header">
		      <h4 class="widget-title lighter smaller">Form 4</h4>
		    </div>

		    <div class="widget-body">
		      <div class="widget-main">
		      	<div class="form-group">
		<?php echo CHtml::label('Diagnosis','',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
			<div class="col-sm-9">
				<input type="text" id="nama-diagnosa" class="input-sm"/>
				<input type="hidden" id="kode-diagnosa"/>
			</div>
		</div>
		
		
		<!-- <a href="javascript:void(0)" id="btn-add-diagnosa" class="btn btn-success">Tambah</a> -->
		<table class="table  table-bordered table-hover" id="tabel-diagnosa">
			<thead>
			<tr>
			
				<th>Diagnosis</th>
				<th>Opsi</th>
			</tr>
			<thead>
			<tbody>
				
			</tbody>
		</table>
		      </div>
		  </div>
		</div>
		
	</div>
	<div class="col-xs-6">
		<div class="widget-box widget-color-blue2">
		    <div class="widget-header">
		      <h4 class="widget-title lighter smaller">Form 5</h4>
		    </div>

		    <div class="widget-body">
		      <div class="widget-main">
		      	<div class="form-group">
		<?php echo CHtml::label('Prosedur','',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
			<div class="col-sm-9">
				<input type="text" id="nama-prosedur"/>
				<input type="hidden" id="kode-prosedur"/>
			</div>
		</div>
		
		<!-- <a href="javascript:void(0)" id="btn-add-prosedur" class="btn btn-success">Tambah</a>	 -->
	<table class="table  table-bordered table-hover" id="tabel-prosedur">
		<thead>
		<tr>
				
			<th>Prosedur</th>
			<th>Opsi</th>
		</tr>
	</thead>
	</table>
		      </div>
		  </div>
		</div>
		
	
</div>
<div class="col-xs-12">
	 <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Submit
          </button>

        
        </div>
      </div>
</div>
</div>
<?php $this->endWidget(); ?>
<?php 
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();

$cs->registerCssFile($baseUrl.'/assets/css/bootstrap-datepicker3.min.css');
$cs->registerScriptFile($baseUrl.'/assets/js/bootstrap-datepicker.min.js');
?>

<script type="text/javascript">

function fillRow(selector){
	var row = '<tr>';

	// row += '<td>'+$('#kode-'+selector).val()+'</td>';
	row += '<td>'+$('#nama-'+selector).val()+'<input type="hidden" name="list-kode-'+selector+'[]" value="'+$('#kode-'+selector).val()+'"/></td>';
	row += '<td><a class="hapus-row" href="javascript:void(0)">Hapus</a></td>';
	row += '</tr>';

	$('#tabel-'+selector+' > tbody').append(row);
}

function isiDataPasien(ui, selector)
{
 
   $("#BpjsVclaim_"+selector).val(ui.item.id);
   $("#"+selector+"_NAMA").val(ui.item.value); 

}

$(document).on('click','.hapus-row',function(){
	$(this).parent().parent().remove();
});

$(document).bind("keyup.autocomplete",function(){
	$('#nama-diagnosa').autocomplete({
      minLength:3,
      select:function(event, ui){
       
        $('#kode-diagnosa').val(ui.item.id);
        $('#nama-diagnosa').val(ui.item.value);
         fillRow('diagnosa');        
      },
      
      focus: function (event, ui) {
        $('#kode-diagnosa').val(ui.item.id);
       
      },

      source:function(request, response) {
        $.ajax({
                url: "<?=Yii::app()->createUrl('WSBpjs/GetRefDiagnosa');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                beforeSend : function(){
                	toggleLoading('#nama-diagnosa',true);

                },
                success: function (data) {
                	toggleLoading('#nama-diagnosa',false);
                	// $('#loading-diagnosa').hide();
                    response(data);
                   
                }
            })
        },
       
  	}); 

  	$('#nama-prosedur').autocomplete({
      minLength:3,
      select:function(event, ui){
       
        $('#kode-prosedur').val(ui.item.id);
        $('#nama-prosedur').val(ui.item.value);
        fillRow('prosedur');        
      },
      
      focus: function (event, ui) {
        $('#kode-prosedur').val(ui.item.id);
       
      },
      source:function(request, response) {
        $.ajax({
                url: "<?=Yii::app()->createUrl('WSBpjs/GetRefProsedur');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                beforeSend : function(){
                	toggleLoading('#nama-prosedur',true);
                },
                success: function (data) {
                	toggleLoading('#nama-prosedur',false);
                    response(data);

                }
            })
        },
       
  	});
});

</script>