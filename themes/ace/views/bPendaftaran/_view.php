<?php
/* @var $this BPendaftaranController */
/* @var $data BPendaftaran */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('NODAFTAR')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->NODAFTAR), array('view', 'id'=>$data->NODAFTAR)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoMedrec')); ?>:</b>
	<?php echo CHtml::encode($data->NoMedrec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodePtgs')); ?>:</b>
	<?php echo CHtml::encode($data->KodePtgs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TGLDAFTAR')); ?>:</b>
	<?php echo CHtml::encode($data->TGLDAFTAR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JamDaftar')); ?>:</b>
	<?php echo CHtml::encode($data->JamDaftar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('umurthn')); ?>:</b>
	<?php echo CHtml::encode($data->umurthn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('umurbln')); ?>:</b>
	<?php echo CHtml::encode($data->umurbln); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('UmurHr')); ?>:</b>
	<?php echo CHtml::encode($data->UmurHr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeJnsUsia')); ?>:</b>
	<?php echo CHtml::encode($data->KodeJnsUsia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeMasuk')); ?>:</b>
	<?php echo CHtml::encode($data->KodeMasuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KetMasuk')); ?>:</b>
	<?php echo CHtml::encode($data->KetMasuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JnsRawat')); ?>:</b>
	<?php echo CHtml::encode($data->JnsRawat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DUtama')); ?>:</b>
	<?php echo CHtml::encode($data->DUtama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('D1')); ?>:</b>
	<?php echo CHtml::encode($data->D1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('D2')); ?>:</b>
	<?php echo CHtml::encode($data->D2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('D3')); ?>:</b>
	<?php echo CHtml::encode($data->D3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('D4')); ?>:</b>
	<?php echo CHtml::encode($data->D4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('D5')); ?>:</b>
	<?php echo CHtml::encode($data->D5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('P1')); ?>:</b>
	<?php echo CHtml::encode($data->P1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('P2')); ?>:</b>
	<?php echo CHtml::encode($data->P2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('P3')); ?>:</b>
	<?php echo CHtml::encode($data->P3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('P4')); ?>:</b>
	<?php echo CHtml::encode($data->P4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('P5')); ?>:</b>
	<?php echo CHtml::encode($data->P5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PxBaruLama')); ?>:</b>
	<?php echo CHtml::encode($data->PxBaruLama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdAntri')); ?>:</b>
	<?php echo CHtml::encode($data->IdAntri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KunjunganKe')); ?>:</b>
	<?php echo CHtml::encode($data->KunjunganKe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KeluarRS')); ?>:</b>
	<?php echo CHtml::encode($data->KeluarRS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TglKRS')); ?>:</b>
	<?php echo CHtml::encode($data->TglKRS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JamKRS')); ?>:</b>
	<?php echo CHtml::encode($data->JamKRS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LamaRawat')); ?>:</b>
	<?php echo CHtml::encode($data->LamaRawat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeUnitDaftar')); ?>:</b>
	<?php echo CHtml::encode($data->KodeUnitDaftar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KunjTerakhirRJ')); ?>:</b>
	<?php echo CHtml::encode($data->KunjTerakhirRJ); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeGol')); ?>:</b>
	<?php echo CHtml::encode($data->KodeGol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeStatusRM')); ?>:</b>
	<?php echo CHtml::encode($data->KodeStatusRM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IsRGT')); ?>:</b>
	<?php echo CHtml::encode($data->IsRGT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoSEP')); ?>:</b>
	<?php echo CHtml::encode($data->NoSEP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeSubPulang')); ?>:</b>
	<?php echo CHtml::encode($data->KodeSubPulang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DPJP')); ?>:</b>
	<?php echo CHtml::encode($data->DPJP); ?>
	<br />

	*/ ?>

</div>