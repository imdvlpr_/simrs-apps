<?php
/* @var $this BPendaftaranController */
/* @var $model BPendaftaran */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'NODAFTAR'); ?>
		<?php echo $form->textField($model,'NODAFTAR'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NoMedrec'); ?>
		<?php echo $form->textField($model,'NoMedrec'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodePtgs'); ?>
		<?php echo $form->textField($model,'KodePtgs'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TGLDAFTAR'); ?>
		<?php echo $form->textField($model,'TGLDAFTAR'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'JamDaftar'); ?>
		<?php echo $form->textField($model,'JamDaftar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'umurthn'); ?>
		<?php echo $form->textField($model,'umurthn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'umurbln'); ?>
		<?php echo $form->textField($model,'umurbln'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UmurHr'); ?>
		<?php echo $form->textField($model,'UmurHr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeJnsUsia'); ?>
		<?php echo $form->textField($model,'KodeJnsUsia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeMasuk'); ?>
		<?php echo $form->textField($model,'KodeMasuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KetMasuk'); ?>
		<?php echo $form->textField($model,'KetMasuk',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'JnsRawat'); ?>
		<?php echo $form->textField($model,'JnsRawat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DUtama'); ?>
		<?php echo $form->textField($model,'DUtama',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'D1'); ?>
		<?php echo $form->textField($model,'D1',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'D2'); ?>
		<?php echo $form->textField($model,'D2',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'D3'); ?>
		<?php echo $form->textField($model,'D3',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'D4'); ?>
		<?php echo $form->textField($model,'D4',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'D5'); ?>
		<?php echo $form->textField($model,'D5',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'P1'); ?>
		<?php echo $form->textField($model,'P1',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'P2'); ?>
		<?php echo $form->textField($model,'P2',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'P3'); ?>
		<?php echo $form->textField($model,'P3',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'P4'); ?>
		<?php echo $form->textField($model,'P4',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'P5'); ?>
		<?php echo $form->textField($model,'P5',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PxBaruLama'); ?>
		<?php echo $form->textField($model,'PxBaruLama',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdAntri'); ?>
		<?php echo $form->textField($model,'IdAntri'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KunjunganKe'); ?>
		<?php echo $form->textField($model,'KunjunganKe'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KeluarRS'); ?>
		<?php echo $form->textField($model,'KeluarRS',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TglKRS'); ?>
		<?php echo $form->textField($model,'TglKRS'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'JamKRS'); ?>
		<?php echo $form->textField($model,'JamKRS'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LamaRawat'); ?>
		<?php echo $form->textField($model,'LamaRawat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeUnitDaftar'); ?>
		<?php echo $form->textField($model,'KodeUnitDaftar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KunjTerakhirRJ'); ?>
		<?php echo $form->textField($model,'KunjTerakhirRJ'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeGol'); ?>
		<?php echo $form->textField($model,'KodeGol'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeStatusRM'); ?>
		<?php echo $form->textField($model,'KodeStatusRM'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IsRGT'); ?>
		<?php echo $form->textField($model,'IsRGT'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NoSEP'); ?>
		<?php echo $form->textField($model,'NoSEP',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KodeSubPulang'); ?>
		<?php echo $form->textField($model,'KodeSubPulang'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DPJP'); ?>
		<?php echo $form->textField($model,'DPJP',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->