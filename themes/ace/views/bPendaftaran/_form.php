<?php
/* @var $this BPendaftaranController */
/* @var $model BPendaftaran */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpendaftaran-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>
<?php 
   foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Pasien</h4>
           <img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
        </div>
        <div class="widget-body">
          <div class="widget-main">

          	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>
			<div class="profile-user-info profile-user-info-striped">
				<div class="profile-info-row">
					<div class="profile-info-name"> No RM </div>

					<div class="profile-info-value">
						<?php echo $form->hiddenField($model,'NODAFTAR'); ?>	
						<?php echo $form->hiddenField($model,'NoMedrec'); ?>
						<span ><?=$model->NoMedrec;?></span>
					</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name"> Nama </div>

					<div class="profile-info-value">
						<span id="nama_pasien"></span>
					</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name"> Alamat </div>

					<div class="profile-info-value">
						<span id="alamat"></span>
					</div>
				</div>

				<div class="profile-info-row">
					<div class="profile-info-name"> JK </div>

					<div class="profile-info-value">
						<span id="jk"></span>
					</div>
				</div>

				

				<div class="profile-info-row">
					<div class="profile-info-name"> Tgl MRS </div>

					<div class="profile-info-value">
						<span ><?=$model->TGLDAFTAR;?></span>
					</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name"> Usia </div>

					<div class="profile-info-value">
						<span ><?=$model->umurthn.' thn '.$model->umurbln.' bln '.$model->UmurHr.' hr';?></span>
					</div>
				</div>
			</div>


          </div>
      </div>
    </div>
</div>
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	      <div class="widget-box widget-color-blue2">
	        <div class="widget-header">
	          <h4 class="widget-title lighter smaller">Data History Perawatan</h4>
	        </div>
	        <div class="widget-body">
	          <div class="widget-main">
	   	<div class="profile-user-info profile-user-info-striped">
	   		<div class="profile-info-row">
					<div class="profile-info-row">
						<div class="profile-info-name"> Kunjungan Ke </div>

						<div class="profile-info-value">
							<span ><?=$model->KunjunganKe;?></span>
						</div>
					</div>
					<div class="profile-info-row">
						<div class="profile-info-name"> Cara Masuk </div>

						<div class="profile-info-value">
							<span ><?=$model->kodeMasuk->NamaMasuk;?></span>
						</div>
					</div>
				
					<div class="profile-info-row">
						<div class="profile-info-name"> Jenis Px </div>

						<div class="profile-info-value">
							<span ><?=$model->PxBaruLama;?></span>
						</div>
					</div>
					<div class="profile-info-row">
						<div class="profile-info-name"> Tgl Daftar</div>

						<div class="profile-info-value">
							<span ><?=$model->TGLDAFTAR;?></span>
						</div>
					</div>
					<div class="profile-info-row">
						<div class="profile-info-name"> Jam Daftar </div>

						<div class="profile-info-value">
							<span ><?=$model->JamDaftar;?></span>
						</div>
					</div>
					<?php 

					foreach($model->trRawatJalans as $q => $item)
					{
					?>
					<div class="profile-info-row">
						<div class="profile-info-name"> Poli <?=$q+1;?> </div>

						<div class="profile-info-value">
							<span ><?=$item->unit->NamaUnit;?></span>
						</div>
					</div>
					<div class="profile-info-row">
						<div class="profile-info-name"> Status Rujukan</div>

						<div class="profile-info-value">
							<span ><?=!empty($item->statusRujukan) ? $item->statusRujukan->NamaSub : '-';?></span>
						</div>
					</div>
					<div class="profile-info-row">
						<div class="profile-info-name"> Keterangan</div>

						<div class="profile-info-value">
							<span ><?=$item->keterangan;?></span>
						</div>
					</div>
					<?php
					}
					?>
				</div>

	          </div>
	      </div>
	    </div>
	</div>

</div>
	</div>
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	      <div class="widget-box widget-color-blue2">
	        <div class="widget-header">
	          <h4 class="widget-title lighter smaller">Data Diagnosis</h4>
	        </div>
	        <div class="widget-body">
	          <div class="widget-main">
		   	<?php
		   	$index = 0;
		   	foreach($model->listDiagnosis as $key => $item)
		   	{

		   		

		   	?>
		   	<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Diagnosis <span class="numbering"><?=$index+1;?></span></label>
				<div class="col-sm-8">
				<input name="diagnosis[]" class="diagnosis form-control" value="<?=$item->keterangan;?>"/>
				<label class="error_diagnosis"></label>

				</div>
				<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>
			</div>
			<?php 
				$index = $key + 1;
		}
			?>
			<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Diagnosis <span class="numbering"><?=$index+1;?></span></label>
				<div class="col-sm-10">
				<input name="diagnosis[]" class="diagnosis form-control"/>
				<label class="error_diagnosis"></label>
				</div>
			</div>
	          </div>
	      </div>
	    </div>
	</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	      <div class="widget-box widget-color-blue2">
	        <div class="widget-header">
	          <h4 class="widget-title lighter smaller">Data Coding (ICD 10)</h4>
	        </div>
	        <div class="widget-body">
	          <div class="widget-main">
	   	<?php
		   	$index = 0;
		   	foreach($model->listIcd10 as $key => $item)
		   	{

		   		

		   	?>
		   	<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">D<span class="icd_numbering"><?=$index+1;?></span></label>
				<div class="col-sm-8">
				<input name="icd10[]" class="icd10 form-control" value="<?=$item->kode_icd.' - '.$item->kodeIcd->deskripsi;?>"/>
				<input type="hidden" name="kode_icd10[]" class="kode_icd10 form-control" value="<?=$item->kode_icd;?>"/>
				<label class="error_icd10"></label>

				</div>
				<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger icd_remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>
			</div>
			<?php 
				$index = $key + 1;
		}
			?>
			<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">D<span class="icd_numbering"><?=$index+1;?></span></label>
				<div class="col-sm-10">
				<input name="icd10[]" class="icd10 form-control"/>
				<input type="hidden" name="kode_icd10[]" class="kode_icd10 form-control"/>
				<label class="error_icd10"></label>
				</div>
			</div>
	
	          </div>
	      </div>
	    </div>
	</div>
</div>
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	      <div class="widget-box widget-color-blue2">
	        <div class="widget-header">
	          <h4 class="widget-title lighter smaller">Data Tindakan</h4>
	        </div>
	        <div class="widget-body">
	          <div class="widget-main">
		   	<?php
		   	$index = 0;
		   	foreach($model->listTindakan as $key => $item)
		   	{

		   		

		   	?>
		   	<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Tindakan <span class="tnumbering"><?=$index+1;?></span></label>
				<div class="col-sm-8">
				<input name="tindakan[]" class="tindakan form-control" value="<?=$item->keterangan;?>"/>
				<label class="error_tindakan"></label>

				</div>
				<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger tremove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>
			</div>
			<?php 
				$index = $key + 1;
		}
			?>
			<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Tindakan <span class="tnumbering"><?=$index+1;?></span></label>
				<div class="col-sm-10">
				<input name="tindakan[]" class="tindakan form-control"/>
				<label class="error_diagnosis"></label>
				</div>
			</div>
	          </div>
	      </div>
	    </div>
	</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	      <div class="widget-box widget-color-blue2">
	        <div class="widget-header">
	          <h4 class="widget-title lighter smaller">Data Coding Procedures (ICD 9 CM)</h4>
	        </div>
	        <div class="widget-body">
	          <div class="widget-main">
	   	   	<?php
		   	$index = 0;
		   	foreach($model->listProsedur as $key => $item)
		   	{

		   		

		   	?>
		   	<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">P<span class="prosedur_numbering"><?=$index+1;?></span></label>
				<div class="col-sm-8">
				<input name="prosedur[]" class="prosedur form-control" value="<?=$item->kode_proc.' - '.$item->kodeProc->description;?>"/>
				<input type="hidden" name="kode_prosedur[]" class="kode_prosedur form-control" value="<?=$item->kode_proc;?>"/>
				<label class="error_prosedur"></label>

				</div>
				<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger prosedur_remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>
			</div>
			<?php 
				$index = $key + 1;
		}
			?>
			<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">P<span class="prosedur_numbering"><?=$index+1;?></span></label>
				<div class="col-sm-10">
				<input name="prosedur[]" class="prosedur form-control"/>
				<input type="hidden" name="kode_prosedur[]" class="kode_prosedur form-control"/>
				<label class="error_prosedur"></label>
				</div>
			</div>
	
	          </div>
	      </div>
	    </div>
	</div>
</div>
	<div class="clearfix form-actions">
        <div class="col-md-offset-10 col-md-12">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

           

<?php $this->endWidget(); ?>

<script type="text/javascript">
function getDataPasien(rm){
	$.ajax({
		type : 'POST',
		url : '<?=Yii::app()->createUrl('ajaxRequest/getPasienByRM');?>',
		data : 'norm='+rm,
		beforeSend : ()=>{
			$('#loading').show();
		},
		error : (e)=>{
			$('#loading').hide();
			console.log(e);
		},
		success : (data)=>{
			$('#loading').hide();
			let hasil = $.parseJSON(data);
			
			$('#nama_pasien').html(hasil.pasien.NAMA);
			$('#alamat').html(hasil.pasien.ALAMAT);
			$('#jk').html(hasil.pasien.JENSKEL);
		}
	});
}

function refreshNumbering(){
	

	$('span.numbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});

	

	$('span.tnumbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});

	

	$('span.icd_numbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});

	

	$('span.prosedur_numbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});
}

$(document).bind("keyup.autocomplete",function(){
	$('.icd10').autocomplete({
      minLength:1,
      select:function(event, ui){
       
        $(this).next().val(ui.item.id);
                
      },
      
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);

      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetIcd');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

$(document).bind("keyup.autocomplete",function(){
	$('.prosedur').autocomplete({
      minLength:1,
      select:function(event, ui){
       
        $(this).next().val(ui.item.id);
                
      },
      
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);

      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetProcedure');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

$(document).on('keydown','input.diagnosis', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">Diagnosis <span class="numbering"></span></label>';
		row += '<div class="col-sm-8">';
		row += '<input name="diagnosis[]" class="diagnosis form-control"/>';
		row += '<label class="error_diagnosis"></label>';
		row += '</div>';
		row += '<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.diagnosis').last().focus();
	}
});

$(document).on('keydown','input.tindakan', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">Tindakan <span class="tnumbering"></span></label>';
		row += '<div class="col-sm-8">';
		row += '<input name="tindakan[]" class="tindakan form-control"/>';
		row += '<label class="error_tindakan"></label>';
		row += '</div>';
		row += '<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger tremove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.tindakan').last().focus();
	}
});

$(document).on('keydown','input.icd10', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">D<span class="icd_numbering"></span></label>';
		row += '<div class="col-sm-8">';
		row += '<input name="icd10[]" class="icd10 form-control"/>';
		row += '<input type="hidden" name="kode_icd10[]" class="kode_icd10 form-control"/>';
		row += '<label class="error_icd10"></label>';
		row += '</div>';
		row += '<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger icd_remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.icd10').last().focus();
	}
});

$(document).on('keydown','input.prosedur', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">P<span class="prosedur_numbering"></span></label>';
		row += '<div class="col-sm-8">';
		row += '<input name="prosedur[]" class="prosedur form-control"/>';
		row += '<input type="hidden" name="kode_prosedur[]" class="kode_prosedur form-control"/>';
		row += '<label class="error_prosedur"></label>';
		row += '</div>';
		row += '<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger prosedur_remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.prosedur').last().focus();
	}
});



$(document).on('click','a.tremove, a.remove, a.icd_remove, a.prosedur_remove',function(e){
	e.preventDefault();
	
	$(this).parent().parent().remove();
	refreshNumbering();
});


$(document).ready(()=>{
	getDataPasien('<?=$model->NoMedrec;?>');

	
});
</script>