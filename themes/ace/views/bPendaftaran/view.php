<?php
/* @var $this BPendaftaranController */
/* @var $model BPendaftaran */

$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran','url'=>array('admin')),
	array('name'=>'Bpendaftaran'),
);

$this->menu=array(
	array('label'=>'List BPendaftaran', 'url'=>array('index')),
	array('label'=>'Create BPendaftaran', 'url'=>array('create')),
	array('label'=>'Update BPendaftaran', 'url'=>array('update', 'id'=>$model->NODAFTAR)),
	array('label'=>'Delete BPendaftaran', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->NODAFTAR),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BPendaftaran', 'url'=>array('admin')),
);
?>

<h1>View BPendaftaran #<?php echo $model->NODAFTAR; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'NODAFTAR',
		'NoMedrec',
		'KodePtgs',
		'TGLDAFTAR',
		'JamDaftar',
		'umurthn',
		'umurbln',
		'UmurHr',
		'KodeJnsUsia',
		'KodeMasuk',
		'KetMasuk',
		'JnsRawat',
		'DUtama',
		'D1',
		'D2',
		'D3',
		'D4',
		'D5',
		'P1',
		'P2',
		'P3',
		'P4',
		'P5',
		'PxBaruLama',
		'IdAntri',
		'KunjunganKe',
		'KeluarRS',
		'TglKRS',
		'JamKRS',
		'LamaRawat',
		'KodeUnitDaftar',
		'KunjTerakhirRJ',
		'KodeGol',
		'KodeStatusRM',
		'IsRGT',
		'NoSEP',
		'KodeSubPulang',
		'DPJP',
	),
)); ?>
	</div>
</div>
