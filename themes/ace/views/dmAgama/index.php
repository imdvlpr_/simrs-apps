<?php
/* @var $this DmAgamaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Dm Agamas'),
);

$this->menu=array(
	array('label'=>'Create DmAgama', 'url'=>array('create')),
	array('label'=>'Manage DmAgama', 'url'=>array('admin')),
);
?>

<h1>Dm Agamas</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
