<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';

$this->breadcrumbs=array(
   array('name' => 'Kamar','url'=>Yii::app()->createUrl('DmKamar')),
                            array('name' => 'Update')
);

?>

<div class="page-header">
  <h1>Data Kamar</h1>
</div>
<style>
  .errorMessage, .errorSummary{
    color:red;
  }

</style>

  <div class="row">
          <div class="col-xs-12">
              <?php $this->renderPartial('_form',array('model'=> $model));?>
            </div>
          </div>

      