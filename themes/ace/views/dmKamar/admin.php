<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Kamar','url'=>Yii::app()->createUrl('DmKamar/index')),
                            array('name' => 'List')
);

?>
<script type="text/javascript">

	$(document).ready(function(){

		$('#search, #size').change(function(){

	        $('#tabelkamar').yiiGridView.update('tabelkamar', {
	            url:'<?php echo Yii::app()->createUrl("DmKamar/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
	        });

	     });

	     
	});
</script>
<div class="row">
    <div class="col-xs-12">

            <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
              
                        <div class="pull-left"> <?php
	echo CHtml::link('Tambah Kamar Baru',array('DmKamar/create'),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                            <?php echo CHtml::dropDownList('DmKamar[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size','size'=>1)); ?> <?php
        echo CHtml::textField('DmKamar[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                  


<?php $this->widget('application.components.ComplexGridView', array(
  'id'=>'tabelkamar',
  'dataProvider'=>$model->search(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
    array(
			'header' => 'Tingkat/Kelas',
			'value' => '$data->kelas->nama_kelas'
		),
		'nama_kamar',
    
		'biaya_kamar',
    'biaya_makan',
		'biaya_askep',
    'biaya_asnut',
    'user_kamar',
    array(
      'class'=>'CButtonColumn',
      'template'=>'{update} {delete}',

    ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
 'pager'=>array(
                  'class'=>'SimplePager',
                  'header'=>'',
                  'firstPageLabel'=>'Pertama',
                  'prevPageLabel'=>'Sebelumnya',
                  'nextPageLabel'=>'Selanjutnya',
                  'lastPageLabel'=>'Terakhir',
                  'firstPageCssClass'=>'btn btn-info',
                  'previousPageCssClass'=>'btn btn-info',
                  'nextPageCssClass'=>'btn btn-info',
                  'lastPageCssClass'=>'btn btn-info',
                  'hiddenPageCssClass'=>'disabled',
                  'internalPageCssClass'=>'btn btn-info',
                  'selectedPageCssClass'=>'btn btn-sky',
                  'maxButtonCount'=>5
                ),
                'itemsCssClass'=>'table  table-bordered table-hover',
                'summaryCssClass'=>'table-message-info',
                'filterCssClass'=>'filter',
                'summaryText'=>'menampilkan {start} - {end} dari {count} data',
                'template'=>'{items}{summary}{pager}',
                'emptyText'=>'Data tidak ditemukan',
                'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>

                    </div>
                </div>
           