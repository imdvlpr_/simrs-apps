<?php
/* @var $this DmKamarController */
/* @var $data DmKamar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kamar')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_kamar), array('view', 'id'=>$data->id_kamar)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelas_id')); ?>:</b>
	<?php echo CHtml::encode($data->kelas_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tingkat_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->tingkat_kamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_kamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_askep')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_askep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_asnut')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_asnut); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_kasur')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_kasur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terpakai')); ?>:</b>
	<?php echo CHtml::encode($data->terpakai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>