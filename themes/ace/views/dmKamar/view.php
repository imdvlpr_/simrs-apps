<?php
/* @var $this DmKamarController */
/* @var $model DmKamar */

$this->breadcrumbs=array(
	'Dm Kamars'=>array('index'),
	$model->id_kamar,
);

$this->menu=array(
	array('label'=>'List DmKamar', 'url'=>array('index')),
	array('label'=>'Create DmKamar', 'url'=>array('create')),
	array('label'=>'Update DmKamar', 'url'=>array('update', 'id'=>$model->id_kamar)),
	array('label'=>'Delete DmKamar', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_kamar),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmKamar', 'url'=>array('admin')),
);
?>

<h1>View DmKamar #<?php echo $model->id_kamar; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_kamar',
		'kelas_id',
		'nama_kamar',
		'tingkat_kamar',
		'biaya_kamar',
		'biaya_askep',
		'biaya_asnut',
		'jumlah_kasur',
		'terpakai',
		'created',
	),
)); ?>
