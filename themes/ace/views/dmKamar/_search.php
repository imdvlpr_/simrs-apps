<?php
/* @var $this DmKamarController */
/* @var $model DmKamar */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_kamar'); ?>
		<?php echo $form->textField($model,'id_kamar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kelas_id'); ?>
		<?php echo $form->textField($model,'kelas_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_kamar'); ?>
		<?php echo $form->textField($model,'nama_kamar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tingkat_kamar'); ?>
		<?php echo $form->textField($model,'tingkat_kamar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_kamar'); ?>
		<?php echo $form->textField($model,'biaya_kamar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_askep'); ?>
		<?php echo $form->textField($model,'biaya_askep'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_asnut'); ?>
		<?php echo $form->textField($model,'biaya_asnut'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jumlah_kasur'); ?>
		<?php echo $form->textField($model,'jumlah_kasur'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'terpakai'); ?>
		<?php echo $form->textField($model,'terpakai'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->