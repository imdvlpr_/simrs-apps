
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dm-kamar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class' => 'form-horizontal'
	)
)); ?>


	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>'); ?>
	<div class="form-group">
		<?php echo $form->labelEx($model,'kamar_master_id',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
			<?php 
			
			$listKamar = CHtml::listData(DmKamarMaster::model()->findAll(),'id','nama_kamar');

			echo $form->dropDownList($model,'kamar_master_id',$listKamar); 

			?>
			<?php echo $form->error($model,'kamar_master_id'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_kamar',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_kamar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama_kamar'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_id',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
			<?php 
			
			$listKelas = CHtml::listData(DmKelas::model()->findAll(),'id_kelas','nama_kelas');

			echo $form->dropDownList($model,'kelas_id',$listKelas); 

			?>
			<?php echo $form->error($model,'kelas_id'); ?>
		</div>
	</div>

	

	

	
	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_kamar',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_kamar'); ?>
		<?php echo $form->error($model,'biaya_kamar'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_makan',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_makan'); ?>
		<?php echo $form->error($model,'biaya_makan'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_askep',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_askep'); ?>
		<?php echo $form->error($model,'biaya_askep'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_asnut',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_asnut'); ?>
		<?php echo $form->error($model,'biaya_asnut'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'user_kamar',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
			<?php 
			
			$listUser = CHtml::listData(User::model()->findAllByAttributes(array('LEVEL'=>WebUser::R_OP_KAMAR)),'USERNAME','USERNAME');

			echo $form->dropDownList($model,'user_kamar',$listUser); 

			?>
			<?php echo $form->error($model,'user_kamar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jumlah_kasur',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jumlah_kasur'); ?>
		<?php echo $form->error($model,'jumlah_kasur'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tingkat_kamar',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tingkat_kamar'); ?>
		<?php echo $form->error($model,'tingkat_kamar'); ?>
		</div>
	</div>

	
<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>

        
        </div>
      </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#DmKamar_kamar_master_id').change(function(){
			$('#DmKamar_nama_kamar').val($(this).find('option:selected').text());
		});
	});
</script>