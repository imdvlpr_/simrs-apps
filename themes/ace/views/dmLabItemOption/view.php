<?php
/* @var $this DmLabItemOptionController */
/* @var $model DmLabItemOption */

$this->breadcrumbs=array(
	array('name'=>'Dm Lab Item Option','url'=>array('admin')),
	array('name'=>'Dm Lab Item Option'),
);

$this->menu=array(
	array('label'=>'List DmLabItemOption', 'url'=>array('index')),
	array('label'=>'Create DmLabItemOption', 'url'=>array('create')),
	array('label'=>'Update DmLabItemOption', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmLabItemOption', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmLabItemOption', 'url'=>array('admin')),
);
?>

<h1>View DmLabItemOption #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_id',
		'nilai',
		'label',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
