<?php
/* @var $this TrResepKamarItemController */
/* @var $model TrResepKamarItem */

$this->breadcrumbs=array(
	array('name'=>'Tr Resep Kamar Item','url'=>array('admin')),
	array('name'=>'Tr Resep Kamar Item'),
);

$this->menu=array(
	array('label'=>'List TrResepKamarItem', 'url'=>array('index')),
	array('label'=>'Create TrResepKamarItem', 'url'=>array('create')),
	array('label'=>'Update TrResepKamarItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrResepKamarItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrResepKamarItem', 'url'=>array('admin')),
);
?>

<h1>View TrResepKamarItem #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'obat_id',
		'jumlah',
		'harga',
		'jenis_obat',
		'kode_racikan',
		'created',
		'tr_resep_kamar_id',
	),
)); ?>
	</div>
</div>
