 <?php
$this->breadcrumbs=array(
	array('name'=>'Item Resep ','url'=>array('admin')),
	array('name'=>'Update'),
);

?>



<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>
<div class="row">
	<div class="col-xs-12">
	<?php $this->renderPartial('_form_part', array('model'=>$model)); ?>
	</div>
</div>
