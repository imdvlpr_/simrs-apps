<?php
/* @var $this TrResepKamarItemController */
/* @var $data TrResepKamarItem */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obat_id')); ?>:</b>
	<?php echo CHtml::encode($data->obat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_obat')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_obat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_racikan')); ?>:</b>
	<?php echo CHtml::encode($data->kode_racikan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tr_resep_kamar_id')); ?>:</b>
	<?php echo CHtml::encode($data->tr_resep_kamar_id); ?>
	<br />

	*/ ?>

</div>