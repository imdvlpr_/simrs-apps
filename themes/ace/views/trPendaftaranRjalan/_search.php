<?php
/* @var $this TrPendaftaranRjalanController */
/* @var $model TrPendaftaranRjalan */
/* @var $form CActiveForm */
?>

<div class="span12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<fieldset>	
	 <div class="control-group">
        <?php echo $form->labelEx($model,'Id',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo $form->textField($model,'Id'); ?>
        </div>
     </div>
     <div class="control-group">
        <?php echo $form->labelEx($model,'NoDaftar',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo $form->textField($model,'NoDaftar'); ?>
        </div>
     </div>
	<div class="control-group">
        <?php echo $form->labelEx($model,'NoMedrec',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo $form->textField($model,'NoMedrec'); ?>
        </div>
     </div>
     <div class="control-group">
        <?php echo $form->labelEx($model,'GolPasien',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo $form->textField($model,'GolPasien'); ?>
        </div>
     </div>
	<div class="control-group">
        <?php echo $form->labelEx($model,'KodePoli',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo $form->textField($model,'KodePoli'); ?>
        </div>
     </div>
     <div class="control-group">
        <?php echo $form->labelEx($model,'KodeSubPlg',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo $form->textField($model,'KodeSubPlg'); ?>
        </div>
     </div>
	<div class="control-group">
        <?php echo $form->labelEx($model,'KodeTdkLanjut',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo $form->textField($model,'KodeTdkLanjut'); ?>
        </div>
     </div>
     <div class="control-group">
        <?php echo $form->labelEx($model,'WAKTU_DAFTAR',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo $form->textField($model,'WAKTU_DAFTAR'); ?>
        </div>
     </div>

	
	<div class="form-actions" align="center">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>
</fieldset>

<?php $this->endWidget(); ?>

</div><!-- search-form -->