<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#search').change(function(){
    
            $('#tabel-rawat-jalan').yiiGridView.update('tabel-rawat-jalan', {
                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()   
            });
          
         });

         $('#size').change(function(){
        
            $('#tabel-rawat-jalan').yiiGridView.update('tabel-rawat-jalan', {
                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()    
            });
        
         });     
    });
</script>
<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span3" id="sidebar">
    <?php //echo $this->renderPartial('nav-left');?>
        </div>
         -->
        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trPendaftaranRjalan/index')),
                            // array('name' => 'Data Tindakan','url'=>Yii::app()->createUrl('trPendaftaranRjalan/tindakan',array('id'=>$rawatJalan->Id))),
                            array('name' => 'Input Tindakan')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Data Rawat Jalan</div>
                      <div class="pull-right"> </div> 
                    </div>
                    <div class="block-content collapse in">
                        

<?php $this->renderPartial('_input', 
    array(
        'rawatJalan' => $rawatJalan,
        'pasien' => $pasien,
        'listDokter' => $listDokter,
        'jenisVisite' => $jenisVisite,
        'rawatRincian' => $rawatRincian
        )
    ); ?>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>