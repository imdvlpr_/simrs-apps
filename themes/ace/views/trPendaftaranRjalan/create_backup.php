<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }


    .errorMessage{
      color: red;
    }

</style>
<div class="container-fluid">
    <div class="row-fluid">
       
         
        <!--/span-->
        <div class="span10" id="content" style="margin-left: 100px">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('registrasi/rawatJalan')),
                            array('name' => 'Pendaftaran')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Formulir Data Pasien</div>
                      <!--   <div class="pull-right"><span class="badge badge-info">1,462</span>

                        </div> -->
                    </div>
                    <div class="block-content collapse in">

<div class="row-fluid">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($kunjungan,'<div class="alert alert-error">Kunjungan: Silakan perbaiki beberapa eror berikut:','</div>');

?>                           <!-- block -->
        
        <div class="block-content collapse in">
            <div class="span12">
                
                
            <fieldset>
        
  <div class="control-group">
    <?php echo $form->labelEx($model,'NoMedrec',array('class'=>'control-label'));?>
      <div class="controls">
        <?php 

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
          'model' => $model,
          'attribute' => 'PASIEN_NAMA',

          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('AjaxRequest/getPasienLikeParam').'",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
          'htmlOptions'=>array('placeholder'=>'Ketik Nama | No Reg'),    
          'options' => array(
              'minLength' => 1,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 
                isiDataPasien(ui);
              }',
      ),
      ));
          echo $form->hiddenField($model,'NoMedrec');

          echo $form->error($model,'NoMedrec');
         ?>
      </div>
    </div>
            <div class="control-group">
        <?php echo $form->labelEx($model,'GolPasien',array('class'=>'control-label'));?>
          <div class="controls">
            <?php 
       $kepesertaan_list = CHtml::ListData(GolPasien::model()->findAll(array('order'=>'NamaGol DESC')),'KodeGol','NamaGol');
         echo $form->dropDownList($model, 'GolPasien',$kepesertaan_list);
         echo $form->error($model,'GolPasien'); 
          echo $form->hiddenField($model,'NoDaftar');
               ?>
          </div>
        </div>
        <div class="control-group" id="div-nokartu" style="display:none">
        <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'control-label'));?>
          <div class="controls">
            <?php 
              echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-large'));

              
            ?>
            <span id="badge"></span>
            <a id='carinomor' href="javascript:void(0);"><i class="icon-search"></i> Cari Nomor</a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
            <?php echo $form->error($bpjsPasien,'NoKartu'); ?>
          </div>
        </div>
           
            <div class="control-group">
              <?php echo $form->labelEx($model,'WAKTU_DAFTAR',array('class'=>'control-label'));?>
              <div class="controls">
               <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'WAKTU_DAFTAR',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'onClose' => 'js:function(dtText,dtI) {
                            //getBatasSEP(dtText);
                        }',
                    ),
        ));

              echo $form->error($model,'WAKTU_DAFTAR');
              ?>
                
                </div>
              </div>
            <div class="control-group" id="div-batas-sep" style="display:none">
            <?php echo $form->labelEx($model,'BATAS_CETAK_SEP',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->textField($model,'BATAS_CETAK_SEP',array('class'=>'span6'));
                  echo $form->error($model,'BATAS_CETAK_SEP'); 
                  ?>
              </div>
            </div>
            <div class="control-group">
            <?php echo $form->labelEx($model,'CaraMasuk',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                          
       $list = CHtml::ListData(Caramasuk::model()->findAll(),'KodeMasuk','NamaMasuk');
         echo $form->dropDownList($model, 'CaraMasuk',$list);
                  echo $form->error($model,'CaraMasuk'); 
                  ?>
              </div>
            </div>
            <div class="control-group">
            <?php echo $form->labelEx($model,'KetCaraMasuk',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->textArea($model,'KetCaraMasuk',array('class'=>'span6'));
                  echo $form->error($model,'KetCaraMasuk'); 
                  ?>
              </div>
            </div>
            <hr>
             <div class="control-group">
            <?php echo $form->labelEx($model,'poli1_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  $level_list = CHtml::ListData(Poli::model()->findAll(),'KODE_POLI','NAMA');
                  echo $form->dropDownList($model, 'poli1_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));
                 
                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan1" id="tindakan1">Tindakan</button>
                  <?php  echo $form->error($model,'poli1_id'); ;?>
              </div>
            </div>
          
             <div class="control-group">
            <?php echo $form->labelEx($model,'poli2_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->dropDownList($model, 'poli2_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));

                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan2" id="tindakan2">Tindakan</button>
                  <?php  echo $form->error($model,'poli2_id'); ;?>
              </div>
            </div>
              <div class="control-group">
            <?php echo $form->labelEx($model,'poli3_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->dropDownList($model, 'poli3_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));
                  
                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan3" id="tindakan3">Tindakan</button>
                  <?php  echo $form->error($model,'poli3_id'); ;?>
              </div>
            </div>  
              <div class="control-group">
                <?php echo $form->labelEx($model,'PostMRS',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      echo $form->textField($model,'PostMRS',array('class'=>'span6'));
                      echo $form->error($model,'PostMRS'); 
                      ?>
                  </div>
                </div>
                <div style="margin-top: 10px;" class="form-actions">
                     <button type="submit" class="btn btn-primary" name="simpan_pendaftaran">Simpan Data Baru</button>
                   <!--    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn dropdown-toggle">Cetak <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                        <li><a href="#"><i class="icon-print"></i> Treser</a></li>
                        <li><a href="#"><i class="icon-print"></i> E-Tiket</a></li>
                        <li><a href="#"><i class="icon-print"></i> Kartu</a></li>
                        
                        </ul>
                      </div><!-- /btn-group -->                      
                      </div>
        </fieldset>
    
        </div>
    </div>
<div id="myModal" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">&times;</button>
        <h3>Tindak Lanjut Rawat Jalan</h3>
    </div>
    <div class="modal-body">
       
    <div class="control-group">
      <?php echo CHtml::label('Tindak Lanjut','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 
          $list_tdl = CHtml::ListData(TindakLanjutRj::model()->findAll(),'NamaTdkLanjut','NamaTdkLanjut');
                  // echo $form->dropDownList($model, 'poli1_id',$level_list,array('class'=>'span6'));
            echo CHtml::dropDownList('tindak_lanjut','',$list_tdl,array('prompt'=>'.: Pilih Tindak Lanjut :.','class'=>'input-large'));

            
          ?>

        </div>
      </div>
    
     <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 1','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

          $list_unit = CHtml::ListData(Unit::model()->findAllByAttributes(array('unit_tipe'=>2)),'KodeUnit','NamaUnit');
                  // echo $form->dropDownList($model, 'poli1_id',$level_list,array('class'=>'span6'));
            echo CHtml::dropDownList('rujukan_intern1','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div>
      <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 2','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

            echo CHtml::dropDownList('rujukan_intern2','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 3','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

          echo CHtml::dropDownList('rujukan_intern3','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 4','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

            echo CHtml::dropDownList('rujukan_intern4','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 5','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

     
            echo CHtml::dropDownList('rujukan_intern5','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div>
   
  </fieldset>
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" class="btn" href="#">Tutup</a>
    </div>
</div>
<!-- /block -->

    <!--
=====================================================================================================================
==============================================form kedua========================================================
=====================================================================================================================
=====================================================================================================================

    -->
      
    </div>
    <!-- /block -->
              </div>
              
   <?php $this->endWidget();?>
</div>

<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
  
  $cs->registerScriptFile($baseUrl.'/assets/enter-keypress.js');
?>
<script>


$(document).ready(function(){

  $('.btn-tindakan').click(function(){
      $('#myModal').modal();  
  });

  $('#TrPendaftaranRjalan_GolPasien').change(function(){
      var txt = $('#TrPendaftaranRjalan_GolPasien option:selected').text();

      if(txt.includes('BPJS')) {
        $('#div-nokartu').show();
        $('#div-batas-sep').show();
      }
      else {
        $('#div-nokartu').hide();
        $('#div-batas-sep').hide();
      }
    });

   $('#carinomor').click(function(){
      carinomor();
    });

});
 function carinomor()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];


    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("AjaxRequest/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NoKartu').val()+'&jenis=kartu',
        beforeSend : function(){
          $('#loading1').show();

        },
        error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
        },
        timeout : 3000,
        async: false,
        success : function(data){


          $('#loading1').hide();
          
          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200){
            $('#badge').html('<span class="badge badge-success">Ok</span>');
            var peserta = output.response.peserta;
           
             $('#NoKartu').val(peserta.noKartu);
            $('#NIK').val(peserta.nik);
            $('#NAMAPESERTA').val(peserta.nama);
           $('#KELAMIN').val(peserta.sex);
            $('#PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);
            $('#TGLLAHIR').val(peserta.tglLahir);
            $('#PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);

            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
          else
          {
              $('#BpjsPasien_NoKartu').val('');
              $('#badge').html('<span class="badge badge-important">Nomor Kartu tidak ada</span>');
          }
        }
      });
   
  }

function getBatasSEP(tgl)
{
    $.ajax({
        url: '<?php echo Yii::app()->createUrl("ajaxRequest/GetBatasSEP");?>',
        type: 'POST', // Send post data
        data: 'tgl='+tgl,
        async: false,

        success: function(data){
            var hasil = JSON.parse(data);
          
            $('#TrPendaftaranRjalan_BATAS_CETAK_SEP').val(hasil.tanggal);
        }
    });
}

  jQuery(document).ready(function() {   
     FormValidation.init();

    
  });
  

        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();

            
        });



<?php 
  if(!empty($model->pASIEN)){
?>
$('#TrPendaftaranRjalan_PASIEN_NAMA').val('<?php echo $model->pASIEN->NAMA;?>');
<?php 
}
?>
function isiDataPasien(ui)
{
 
   $("#TrPendaftaranRjalan_NoMedrec").val(ui.item.id);
   $("#TrPendaftaranRjalan_PASIEN_NAMA").val(ui.item.value); 
   

}


 </script>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>