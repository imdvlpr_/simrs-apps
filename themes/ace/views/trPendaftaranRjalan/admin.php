<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trPendaftaranRjalan/index')),
                            array('name' => 'List')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#search, #size, #poli').change(function(){
    
            $('#tabel-rawat-jalan').yiiGridView.update('tabel-rawat-jalan', {
                url:'?r=trPendaftaranRjalan&filter='+$('#search').val()+'&size='+$('#size').val()+'&poli='+$('#poli').val()   
            });
          
         });
     
    });
</script>
<div class="row">
    <div class="col-xs-12">

                            <div class="pull-left">
  
                        </div>
                      <div class="pull-right">Data per halaman
<?php echo CHtml::dropDownList('TrPendaftaranRJalan[PAGESIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,20=>20,30=>30,),array('id'=>'size','size'=>1)); ?>
<?php
 $poli_list = CHtml::ListData(Poli::model()->findAll(),'KODE_POLI','NAMA');
 echo CHtml::dropDownList('TrPendaftaranRJalan[POLI]',isset($_GET['poli'])?$_GET['poli']:'',$poli_list,array('id'=>'poli')); ?>    
<?php  echo CHtml::textField('TrPendaftaranRJalan[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));   ?>   
</div>  
                   
  <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>                      
                       
<?php 


$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabel-rawat-jalan',
  'dataProvider'=>$model->search(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),

    'NoMedrec',
    array(
        'header' => 'Nama Pasien',
        'value' => '$data->pASIEN->NAMA'
    ),
    array(
        'header' => 'Gol Pasien',
        'value' => '$data->gOLPASIEN->NamaGol'
    ),
    array(
        'header' => 'Poli 1',
        'value' => '!empty($data->poli1) ? $data->poli1->NAMA : "-"'
    ),
    'WAKTU_DAFTAR',

    array(
      'class'=>'CButtonColumn',
      'template'=>'{etiket} {gelang} {sep} {update} {delete}',
      // 'template'=>'{input} {kwitansi} {laporan} {edit} {sep}',
      'buttons'=>array(
            'etiket' => array(
              'url'=>'Yii::app()->createUrl("pasien/CetakETiket/", array("id"=>$data->NoMedrec))',   
              'label'=>'<i class="icon-print" style="color:#fff"></i>',
              'options' => array('title'=>'Cetak Etiket'),      
            ),

            'gelang' => array(
              'url'=>'Yii::app()->createUrl("pasien/CetakGelang/", array("id"=>$data->NoMedrec))',   
              'label'=>'<i class="icon-print"></i>',
              'options' => array('title'=>'Cetak Gelang','target'=>'_blank'),      
            ),
             'sep' => array(
              'url'=>'Yii::app()->createUrl("bpjs/sepPopup/", array("kartu"=>$data->no_sep,"id"=>$data->Id,"norm"=>$data->NoMedrec))',   
              'label'=>'<i class="icon-print"></i>',
              'type' => 'raw',
              'options' => array('title'=>'Cetak SEP','target'=>'_blank'),      
            ),
            'update' => array(
              'url'=>'Yii::app()->createUrl("trPendaftaranRjalan/updateRJ/", array("id"=>$data->Id))',   
              'options' => array('title'=>'Edit Data RJ'),      
            ),
            // 'treser' => array(
            //   'url'=>'Yii::app()->createUrl("trPendaftaranRjalan/CetakTreser/", array("id"=>$data->NoDaftar))',   
            //   'label'=>'<i class="icon-print"></i>',
            //   'options' => array('title'=>'Cetak Treser'),
            //   'click' => "function( e ){
            //          e.preventDefault();
            //          $('#pesan').hide();
            //           $('#modal-konfirmasi').modal('show');
            //           var id = getURLParameter($(this).attr('href'), 'id');
            //           $('#modal-idprogram').val(id);
            //       }",      
            // ),

            // 'input' => array(
            //   'url'=>'Yii::app()->createUrl("trPendaftaranRjalan/input/", array("id"=>$data->Id))',   
            //   'label'=>'<i class="icon-edit"></i>',
            //   'options' => array('title'=>'Input Data Kasir'),      
            // ),

            // 'kwitansi' => array(
            //   'url'=>'Yii::app()->createUrl("trPendaftaranRjalan/cetakKwitansi/", array("id"=>$data->Id))',   
            //   'label'=>'<i class="icon-print"></i>',
            //   'options' => array('title'=>'Cetak','target'=>'_blank'),      
            // ),
            // 'laporan' => array(
            //   'url'=>'Yii::app()->createUrl("trPendaftaranRjalan/laporan/", array("id"=>$data->Id))',   
            //   'label'=>'<i class="icon-align-justify"></i>',
            //   'options' => array('title'=>'Laporan'),      
            // ),

            // 'sep'=>array(
            //   'url'=>'Yii::app()->createUrl("bpjs/sep/", array("tid"=>$data->Id))',      
            //   'label'=>'<i class="icon-print"></i>',
            //   'options' => array('title'=>'Cetak SEP'),
            //   'visible' => '$data->IS_BPJS == 1',
            // ),
            
            // 'edit' => array(
            //   'url'=>'Yii::app()->createUrl("trPendaftaranRjalan/update/", array("id"=>$data->Id))',   
            //   'label'=>'<i class="icon-pencil"></i>',
            //   'options' => array('title'=>'Ubah Data Rawat'),      
            // ),
      ),     
      
    ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
  'pager'=>array(
                        'class'=>'SimplePager',                  
                        'header'=>'',                      
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',        
                        'lastPageLabel'=>'Terakhir',  
           'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
            'hiddenPageCssClass'=>'disabled',
            'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table  table-bordered table-hover',
  'summaryCssClass'=>'table-message-info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
   'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>
                    </div>
                </div>
            
<script type="text/javascript">
function getURLParameter(url, name) {
    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
}


$(document).delegate("#confirm", "click", function(event){

   kid=$('#modal-idprogram').val();

   var param = new Object;
   param.id = kid;
   param.printer = $('#lokasi_printer').val();
   
   if(param.printer == '')
   {
     alert('Silakan Pilih Printer');
     return;
   }

   var jsonObj = JSON.stringify(param);

   $.ajax({
     url : '<?php echo Yii::app()->createUrl('TrPendaftaranRJalan/CetakTreser');?>',
     type : 'post',
     data : {'param':param},
     success : function(data){
      var jsonData = $.parseJSON(data);
      if(jsonData.code == '200'){
        $('#pesan').show();

        // $('#modal-konfirmasi').modal('hide');

          
      }

    else{
      alert(jsonData.message);
    }
     }
   });
  

});
</script>
<div id="modal-konfirmasi" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog ">
    <div class="modal-content">
    <div class="modal-header modal-header-konfirmasi">Anda ingin mencetak Treser data ini?
    <input type="hidden" name="idprogram" id="modal-idprogram"/>
   
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="color:#25ade3;"><span id="modal-program"></span> </h4>
      </div>
       <div class="modal-body" align="center">
       <div class="row-fluid">

    <div class="form-group" id="pesan" style="background-color: #81db81;padding:3px 4px;color:#fff;display: none">
      Treser Berhasil Dicetak
    </div>

   
  
</div>
</div>
<div class="modal-footer" align="center" style="text-align: center">

        <button id="kembali" type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
        <input type="hidden" id="modal-is_paket"/>
        <input type="hidden" id="modal-dp"/>
        <button id="confirm" type="button" class="btn btn-primary">Cetak Treser</button>
      </div>

    </div>
  </div>
</div>