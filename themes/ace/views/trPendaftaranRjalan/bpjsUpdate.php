<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }


    .errorMessage{
      color: red;
    }

    .form-horizontal .control-label{
      width: 100px;
    }

    .form-horizontal .controls{
      margin-left: 110px;
    }

    .navbar{
      margin-bottom: 0px;
    }
</style>
<div class="container-fluid">
    <div class="row-fluid">
       
         
        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('registrasi/rawatJalan')),
                            array('name' => 'Pendaftaran')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->

<?php 
foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }    

$form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>
              <?php 
// echo $form->errorSummary($kunjungan,'<div class="alert alert-error">Kunjungan: Silakan perbaiki beberapa eror berikut:','</div>');
           
?>                           <!-- block -->
      
       
 <div class="span4">
<?php echo $form->errorSummary($pasien,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>'); 
;?>
      <div class="row-fluid">
        <div class="block">
          <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs-icon.png"/> SEP BPJS</div>
          </div>
         <div class="block-content collapse in">
            <div class="span12">
             

<div style="display:none" class="alert alert-error" id="bpjs_error"></div>
<div style="display:none" class="alert alert-info" id="bpjs_info"></div>
              <fieldset>
            <?php 
echo $form->errorSummary($bpjsPasien,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

            ?>   
                             <!-- BPJS ID Number Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'control-label')); ?>
              <div class="controls">
                <?php echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-medium', 'onkeypress' => 'return isNumber(event)')); ?>
                <a id='carinomor' class="btn btn-info"  href="javascript:void(0);"><i class="icon-search"></i></a>
                <?php echo $form->error($bpjsPasien,'NoKartu'); ?>
              </div>
            </div>

            <!-- NIK Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'NIK',array('class'=>'control-label'));?>
              <div class="controls">
              <?php echo $form->textField($bpjsPasien,'NIK',array('class'=>'input-medium', 'onkeypress' => 'return isNumber(event)'));
              echo $form->error($bpjsPasien,'NIK'); ?>
              <a id='carinik' class="btn btn-info" href="javascript:void(0);"><i class="icon-search"></i></a><span id='loading2' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
              </div>
            </div>

            <!-- Patient's Name Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'NAMAPESERTA',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                echo $form->textField($bpjsPasien,'NAMAPESERTA',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->hiddenField($pasien,'NAMA');
                 echo $form->hiddenField($pasien,'TGLLAHIR');
                 echo $form->hiddenField($pasien,'NOIDENTITAS');
                 echo $form->hiddenField($pasien,'JENSKEL');
                echo $form->error($bpjsPasien,'NAMAPESERTA'); ?>
              </div>
            </div>

            <!-- Sex Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'KELAMIN',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                 echo $form->textField($bpjsPasien,'KELAMIN',array('class'=>'input-large','readonly'=>'readonly'));
                //echo $form->radioButtonList($bpjsPasien, 'KELAMIN', array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('labelOptions'=>array('style'=>'display:inline')));
                echo $form->error($bpjsPasien,'KELAMIN'); ?>
              </div>
            </div>

            <!-- PISAT Field -->
                <?php
                  echo CHtml::hiddenField('pisat_nama','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'PISAT',array('class'=>'input-large'));
                ?>

            <!-- Date of Birth Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                echo $form->textField($bpjsPasien,'TGLLAHIR',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));
                ?>
              </div>
            </div>

            <!-- PPKTK1 Field -->
            <!-- Autofill by  BPJS ID -->
                <?php echo $form->hiddenField($bpjsPasien,'PPKTK1',array('class'=>'input-large','readonly'=>'readonly'));
                 ?>

            <!-- BPJS Member's Name Field -->
            <!-- Autofill by BPJS ID -->
                <?php
                  echo CHtml::hiddenField('PESERTA_NAMA','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'PESERTA');
                
                ?>

            <!-- BPJS Class Field -->
            <!-- Autofill by BPJS ID -->
           
                <?php
                  echo CHtml::hiddenField('KELASRAWAT_NAMA','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'KELASRAWAT');
                 
                ?>
            

            <!-- BPJS Card Printing Date Field -->
            <!-- Autofill by BPJS ID -->
           
                <?php echo $form->hiddenField($bpjsPasien,'TGLCETAKKARTU',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TGLCETAKKARTU'); ?>
         
            <!-- TMT Field -->
            <!-- Autofill by BPJS ID -->
           
                <?php echo $form->hiddenField($bpjsPasien,'TMT',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TMT'); ?>
             

            <!-- TAT Field -->
            <!-- Autofill by BPJS ID -->
           
                <?php echo $form->hiddenField($bpjsPasien,'TAT',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TAT');?>
             
                <!-- Submit Action Button -->
               
              </fieldset>
             
            </div>
          </div>
        </div>
      </div>
      <div class="row-fluid">
        <div class="block">
          <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Alamat</div>
          </div>
         <div class="block-content collapse in">
            <div class="span12">
             

              <fieldset>
    <?php 
echo $form->errorSummary($pasien,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

    ?>
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'Desa',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php echo $form->textField($pasien,'Desa',array('class'=>'input-large'));
                  echo $form->error($pasien,'Desa'); ?>
                  </div>
                </div>

                  <div class="control-group">
              <?php echo $form->labelEx($pasien,'ALAMAT',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($pasien,'ALAMAT',array('class'=>'input-large'));
                echo $form->error($pasien,'ALAMAT'); ?>
              </div>
            </div>         
              </fieldset>
             
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="span4">
      <div class="row-fluid">
        <div class="block">
  <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Data Detil Pasien</div>
          </div>
  <div class="block-content collapse in">
            <div class="span12">
 <div class="control-group">
                  <?php echo $form->labelEx($pasien,'TMPLAHIR',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'TMPLAHIR',array('class'=>'input-large'));
                    echo $form->error($pasien,'TMPLAHIR'); ?>
                  </div>
                </div>

                <!-- Weight Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'BeratLahir',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'BeratLahir',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return weightNumber(event)',
                      'placeholder' => 'Berat dalam Kg. Contoh: 64.5'
                    ));
                    echo $form->error($pasien,'BeratLahir');?>
                  </div>
                </div>

                <!-- Blood Type Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'GOLDARAH',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($pasien,'GOLDARAH',CHtml::ListData(DmGoldarah::model()->findAll(),'id_goldarah','nama_goldarah'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->GOLDARAH=>array('selected'=>true))));
                      echo $form->error($pasien,'GOLDARAH');
                    ?>
                  </div>
                </div>

                <!-- Religion Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'AGAMA',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php
                     echo $form->dropDownList($pasien,'AGAMA',CHtml::ListData(DmAgama::model()->findAll(array('order'=>'id_agama ASC')),'id_agama','nama_agama'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->AGAMA=>array('selected'=>true))));
                     echo $form->error($pasien,'AGAMA');
                  ?>
                  </div>
                </div>

                <!-- Marital Status -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'STATUSPERKAWINAN',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($pasien,'STATUSPERKAWINAN',CHtml::ListData(DmStatuskawin::model()->findAll(array('order'=>'id_statuskawin ASC')),'id_statuskawin','nama_statuskawin'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->STATUSPERKAWINAN=>array('selected'=>true))));
                      echo $form->error($pasien,'STATUSPERKAWINAN');
                    ?>
                  </div>
                </div>

                <!-- Job Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'PEKERJAAN',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($pasien,'PEKERJAAN',CHtml::ListData(DmPekerjaan::model()->findAll(array('order'=>'id_pekerjaan ASC')),'nama_pekerjaan','nama_pekerjaan'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->PEKERJAAN=>array('selected'=>true))));
                      echo $form->error($pasien,'PEKERJAAN');
                    ?>
                  </div>
                </div>

                <!-- TEL. Number Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'TELP',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'TELP',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return isNumber(event)'
                      ));
                    echo $form->error($pasien,'TELP'); ?>
                  </div>
                </div>

                <!-- Parent's Name Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'NamaOrtu',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'NamaOrtu',array('class'=>'input-large'));
                    echo $form->error($pasien,'NamaOrtu'); ?>
                  </div>
                </div>

                <!-- Principal's Name (Husband / Wife) Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'NamaSuamiIstri',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'NamaSuamiIstri',array('class'=>'input-large'));
                    echo $form->error($pasien,'NamaSuamiIstri'); ?>
                  </div>
                </div>
        </div>
</div>
</div>
        </div>
</div>
    <div class="span4">
      <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

      ?>
      <div class="row-fluid">
        <div class="block">
          <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Tindakan Rawat Jalan</div>
          </div>
         <div class="block-content collapse in">
            <div class="span12">
             
              <fieldset>
               
   
          <div class="control-group">
        <?php echo $form->labelEx($model,'GolPasien',array('class'=>'control-label'));?>
          <div class="controls">
            <?php 
       $kepesertaan_list = CHtml::ListData(GolPasien::model()->findAll(array('order'=>'NamaGol DESC')),'KodeGol','NamaGol');
         echo $form->dropDownList($model, 'GolPasien',$kepesertaan_list,array('disabled'=>'disabled'));
         echo $form->error($model,'GolPasien'); 
          echo $form->hiddenField($model,'NoDaftar');
               ?>
          </div>
        </div>
            <div class="control-group">
              <?php echo $form->labelEx($model,'WAKTU_DAFTAR',array('class'=>'control-label'));?>
              <div class="controls">
               <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'WAKTU_DAFTAR',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'onClose' => 'js:function(dtText,dtI) {
                            //getBatasSEP(dtText);
                        }',
                    ),
        ));

              echo $form->error($model,'WAKTU_DAFTAR');
              ?>
                
                </div>
              </div>
            
            <div class="control-group">
            <?php echo $form->labelEx($model,'CaraMasuk',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                          
       $list = CHtml::ListData(Caramasuk::model()->findAll(),'KodeMasuk','NamaMasuk');
         echo $form->dropDownList($model, 'CaraMasuk',$list);
                  echo $form->error($model,'CaraMasuk'); 
                  ?>
              </div>
            </div>
           
            <hr>
             <div class="control-group">
            <?php echo $form->labelEx($model,'poli1_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  $level_list = CHtml::ListData(Poli::model()->findAll(),'KODE_POLI','NAMA');
                  echo $form->dropDownList($model, 'poli1_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));
                 
                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan1" id="tindakan1">Tindakan</button>
                  <?php  echo $form->error($model,'poli1_id'); ;?>
              </div>
            </div>
          
             <div class="control-group">
            <?php echo $form->labelEx($model,'poli2_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->dropDownList($model, 'poli2_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));

                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan2" id="tindakan2">Tindakan</button>
                  <?php  echo $form->error($model,'poli2_id'); ;?>
              </div>
            </div>
              <div class="control-group">
            <?php echo $form->labelEx($model,'poli3_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->dropDownList($model, 'poli3_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));
                  
                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan3" id="tindakan3">Tindakan</button>
                  <?php  echo $form->error($model,'poli3_id'); ;?>
              </div>
            </div>  
             
              </fieldset>
            
            </div>
          </div>
        </div>
      </div>
    </div>

<div id="myModal" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">&times;</button>
        <h3>Tindak Lanjut Rawat Jalan</h3>
    </div>
    <div class="modal-body">
       
    <div class="control-group">
      <?php echo CHtml::label('Tindak Lanjut','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 
          $list_tdl = CHtml::ListData(TindakLanjutRj::model()->findAll(),'NamaTdkLanjut','NamaTdkLanjut');
                  // echo $form->dropDownList($model, 'poli1_id',$level_list,array('class'=>'span6'));
            echo CHtml::dropDownList('tindak_lanjut','',$list_tdl,array('prompt'=>'.: Pilih Tindak Lanjut :.','class'=>'input-large'));

            
          ?>

        </div>
      </div>
    
     <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 1','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

          $list_unit = CHtml::ListData(Unit::model()->findAllByAttributes(array('unit_tipe'=>2)),'KodeUnit','NamaUnit');
                  // echo $form->dropDownList($model, 'poli1_id',$level_list,array('class'=>'span6'));
            echo CHtml::dropDownList('rujukan_intern1','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div>
      <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 2','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

            echo CHtml::dropDownList('rujukan_intern2','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 3','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

          echo CHtml::dropDownList('rujukan_intern3','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 4','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

            echo CHtml::dropDownList('rujukan_intern4','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 5','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

     
            echo CHtml::dropDownList('rujukan_intern5','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div>
   
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" class="btn" href="#">Tutup</a>
    </div>
</div>
<div class="row-fluid">
       <div style="margin-top: 0px;text-align: center;" class="form-actions">
        <img style="display: none" id="loading1" src="<?=Yii::app()->baseUrl;?>/images/loading.gif">
<?php echo CHtml::link('Cetak SEP',
        $this->createUrl('bpjs/sepPopup',array('kartu'=>$bpjsPasien->NoKartu,'jr'=>0,'idPasien'=>$pasien->NoMedrec)),
        // array(
        //     'beforeSend' => 'function(){$("#loading1").show();}',
        //     'success'=>'function(r){$("#loading1").hide();$("#juiDialog").html(r).dialog("open"); return false;}'
        // ),
        array('id'=>'showJuiDialog','class'=>'btn btn-success') // not very useful, but hey...
);?>

                      <!-- <a href="javascript:void(0)" class="btn btn-success" name="cetak_sep">Cetak SEP</a> -->
                            
                      </div>
</div>  
   <?php $this->endWidget();?>
</div>
<?php 
// $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
//                 'id'=>'juiDialog',
//                 'options'=>array(
//                     'title'=>'Cetak SEP',
//                     'autoOpen'=>false,
//                     'modal'=>'true',
//                     'width'=>'auto',
//                     'height'=>500,
//                     'buttons' => array("Tutup"=>'js:function(){$(this).dialog("close")}'),

//                 ),
//                 ));
// $this->endWidget();
?>
<script>
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }


$(document).ready(function(){

  $('.btn-tindakan').click(function(){
      $('#myModal').modal();  
  });

  $('#TrPendaftaranRjalan_GolPasien').change(function(){
      var txt = $('#TrPendaftaranRjalan_GolPasien option:selected').text();

      if(txt.includes('BPJS')) {
        $('#div-nokartu').show();
        $('#div-batas-sep').show();
      }
      else {
        $('#div-nokartu').hide();
        $('#div-batas-sep').hide();
      }
    });

   $('#carinomor').click(function(){
      carinomor();
    });

    $('#carinik').click(function(){
      carinik();
    });

});
 
  function carinomor()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];


    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NoKartu').val()+'&jenis=kartu',
        beforeSend : function(){
          $('#loading1').show();
           $('#bpjs_error').hide();
        },
        error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {
                 alert('Failed from timeout');
                   $('#loading1').hide();
                //do something. Try again perhaps?
            }
        },
        timeout : 3000,
        async: false,
        success : function(data){


          $('#loading1').hide();

          if(data == '') return;

          var output = data;
          if(output.metadata.code == 200){
             $('#bpjs_error').hide();
            var peserta = output.response.peserta;
            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#Pasien_NOIDENTITAS').val(peserta.nik);

            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);
            $('#Pasien_NAMA').val(peserta.nama);





            if(peserta.sex == 'L'){
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
              //$('#Pasien_JENSKEL_0').prop("checked",true);
            }
            else{
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);
              // $('#Pasien_JENSKEL_1').prop("checked",true);
            }

            $('#Pasien_JENSKEL').val(peserta.sex);
            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);

            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
             $('#Pasien_TGLLAHIR').val(peserta.tglLahir);

            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);
             $('#BpjsPasien_TGLCETAKKARTU').val(peserta.tglCetakKartu);
             $('#BpjsPasien_PPKTK1').val(peserta.provUmum.kdProvider+' - '+peserta.provUmum.nmProvider);
            $('#BpjsPasien_TMT').val(peserta.tglTMT);
            $('#BpjsPasien_TAT').val(peserta.tglTAT);
            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
          else{
            $('#bpjs_error').show();
                   $('#bpjs_error').html(output.metadata.message);
          }
        }
      });

  }
 function carinik()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];
    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NIK').val()+'&jenis=nik',
        beforeSend : function(){
          $('#loading2').show();
          $('#bpjs_error').hide();
        },
        success : function(data){


          $('#loading2').hide();

          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200){
             $('#bpjs_error').hide();
            var peserta = output.response.peserta;
            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#Pasien_NOIDENTITAS').val(peserta.nik);
            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);
             $('#Pasien_NAMA').val(peserta.nama);

            if(peserta.sex == 'L')
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
            else
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);

            $('#Pasien_JENSKEL').val(peserta.sex);

            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);

            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
             $('#Pasien_TGLLAHIR').val(peserta.tglLahir);

            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);
             $('#BpjsPasien_TGLCETAKKARTU').val(peserta.tglCetakKartu);
             $('#BpjsPasien_PPKTK1').val(peserta.provUmum.kdProvider+' - '+peserta.provUmum.nmProvider);
            $('#BpjsPasien_TMT').val(peserta.tglTMT);
            $('#BpjsPasien_TAT').val(peserta.tglTAT);
            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
          else{
             $('#bpjs_error').show();
                   $('#bpjs_error').html(output.metadata.message);
          }
        }
      });

  }




function getBatasSEP(tgl)
{
    $.ajax({
        url: '<?php echo Yii::app()->createUrl("ajaxRequest/GetBatasSEP");?>',
        type: 'POST', // Send post data
        data: 'tgl='+tgl,
        async: false,

        success: function(data){
            var hasil = JSON.parse(data);
          
            $('#TrPendaftaranRjalan_BATAS_CETAK_SEP').val(hasil.tanggal);
        }
    });
}




<?php 
  if(!empty($model->pASIEN)){
?>
$('#TrPendaftaranRjalan_PASIEN_NAMA').val('<?php echo $model->pASIEN->NAMA;?>');
<?php 
}
?>
function isiDataPasien(ui)
{
 
   $("#TrPendaftaranRjalan_NoMedrec").val(ui.item.id);
   $("#TrPendaftaranRjalan_PASIEN_NAMA").val(ui.item.value); 
   

}


 </script>
            </div>
        </div>
    </div>
    <hr>
  
</div>