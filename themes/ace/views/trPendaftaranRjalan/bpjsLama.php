<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('registrasi/rawatJalan')),
                            array('name' => 'Pendaftaran')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }


    .errorMessage{
      color: red;
    }

 
</style>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>

<div class="row">
    <?php 
echo $form->errorSummary($bpjsPasien,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($pasien,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

    ?>
  <div class="col-sm-4">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Pasien</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
          
                <!-- Medical Record Number Field -->
                 <!-- Medical Record Number Field -->
                   

<div style="display:none" class="alert alert-danger" id="bpjs_error"></div>
<div style="display:none" class="alert alert-info" id="bpjs_info"></div>
              <fieldset>
                       <!-- BPJS ID Number Field -->
            <div class="form-group">
              <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
              <div class="col-sm-9">
                <?php echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-medium', 'onkeypress' => 'return isNumber(event)')); ?>
                <a id='carinomor' class="btn btn-sm btn-info"  href="javascript:void(0);"><i class="fa fa-search"></i></a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
                <?php echo $form->error($bpjsPasien,'NoKartu'); ?>
              </div>
            </div>

            <!-- NIK Field -->
            <div class="form-group">
              <?php echo $form->labelEx($bpjsPasien,'NIK',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
              <?php echo $form->textField($bpjsPasien,'NIK',array('class'=>'input-medium', 'onkeypress' => 'return isNumber(event)'));
              echo $form->error($bpjsPasien,'NIK'); ?>
              <a id='carinik' class="btn btn-info btn-sm" href="javascript:void(0);"><i class="fa fa-search"></i></a><span id='loading2' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
              </div>
            </div>

            <!-- Patient's Name Field -->
            <div class="form-group">
              <?php echo $form->labelEx($bpjsPasien,'NAMAPESERTA',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php 
                echo $form->textField($bpjsPasien,'NAMAPESERTA',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->hiddenField($pasien,'NAMA');
                 echo $form->hiddenField($pasien,'TGLLAHIR');
                 echo $form->hiddenField($pasien,'NOIDENTITAS');
                 echo $form->hiddenField($pasien,'JENSKEL');
                echo $form->error($bpjsPasien,'NAMAPESERTA'); ?>
              </div>
            </div>

            <!-- Sex Field -->
            <div class="form-group">
              <?php echo $form->labelEx($bpjsPasien,'KELAMIN',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php 
                 echo $form->textField($bpjsPasien,'KELAMIN',array('class'=>'input-large','readonly'=>'readonly'));
                //echo $form->radioButtonList($bpjsPasien, 'KELAMIN', array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('labelOptions'=>array('style'=>'display:inline')));
                echo $form->error($bpjsPasien,'KELAMIN'); ?>
              </div>
            </div>

            <!-- PISAT Field -->
                <?php
                  echo CHtml::hiddenField('pisat_nama','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'PISAT',array('class'=>'input-large'));
                ?>

            <!-- Date of Birth Field -->
            <div class="form-group">
              <?php echo $form->labelEx($bpjsPasien,'TGLLAHIR',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php 
                echo $form->textField($bpjsPasien,'TGLLAHIR',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TGLLAHIR',array('class'=>'col-sm-3  control-label no-padding-right'));
                ?>
              </div>
            </div>

            <!-- PPKTK1 Field -->
            <!-- Autofill by  BPJS ID -->
                <?php echo $form->hiddenField($bpjsPasien,'PPKTK1',array('class'=>'input-large','readonly'=>'readonly'));
                 ?>

            <!-- BPJS Member's Name Field -->
            <!-- Autofill by BPJS ID -->
                <?php
                  echo CHtml::hiddenField('PESERTA_NAMA','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'PESERTA');
                
                ?>

            <!-- BPJS Class Field -->
            <!-- Autofill by BPJS ID -->
           
                <?php
                  echo CHtml::hiddenField('KELASRAWAT_NAMA','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'KELASRAWAT');
                 
                ?>
            

            <!-- BPJS Card Printing Date Field -->
            <!-- Autofill by BPJS ID -->
           
                <?php echo $form->hiddenField($bpjsPasien,'TGLCETAKKARTU',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TGLCETAKKARTU'); ?>
         
            <!-- TMT Field -->
            <!-- Autofill by BPJS ID -->
           
                <?php echo $form->hiddenField($bpjsPasien,'TMT',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TMT'); ?>
             

            <!-- TAT Field -->
            <!-- Autofill by BPJS ID -->
           
                <?php echo $form->hiddenField($bpjsPasien,'TAT',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TAT');?>
             

          </div>
        </div>
      </div>
    </div>
 <div class="col-sm-4">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Detil Pasien</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">

         <div class="form-group">
                  <?php echo $form->labelEx($pasien,'TMPLAHIR',array('class'=>'col-sm-3  control-label  no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'TMPLAHIR',array('class'=>'input-large'));
                    echo $form->error($pasien,'TMPLAHIR'); ?>
                  </div>
                </div>

                <!-- Weight Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'BeratLahir',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'BeratLahir',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return weightNumber(event)',
                      'placeholder' => 'Berat dalam Kg. Contoh: 64.5'
                    ));
                    echo $form->error($pasien,'BeratLahir');?>
                  </div>
                </div>

                <!-- Blood Type Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'GOLDARAH',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php
                      echo $form->dropDownList($pasien,'GOLDARAH',CHtml::ListData(DmGoldarah::model()->findAll(),'id_goldarah','nama_goldarah'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->GOLDARAH=>array('selected'=>true))));
                      echo $form->error($pasien,'GOLDARAH');
                    ?>
                  </div>
                </div>

                <!-- Religion Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'AGAMA',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php
                     echo $form->dropDownList($pasien,'AGAMA',CHtml::ListData(DmAgama::model()->findAll(array('order'=>'id_agama ASC')),'id_agama','nama_agama'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->AGAMA=>array('selected'=>true))));
                     echo $form->error($pasien,'AGAMA');
                  ?>
                  </div>
                </div>

                <!-- Marital Status -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'STATUSPERKAWINAN',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php
                      echo $form->dropDownList($pasien,'STATUSPERKAWINAN',CHtml::ListData(DmStatuskawin::model()->findAll(array('order'=>'id_statuskawin ASC')),'id_statuskawin','nama_statuskawin'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->STATUSPERKAWINAN=>array('selected'=>true))));
                      echo $form->error($pasien,'STATUSPERKAWINAN');
                    ?>
                  </div>
                </div>

                <!-- Job Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'PEKERJAAN',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php
                      echo $form->dropDownList($pasien,'PEKERJAAN',CHtml::ListData(DmPekerjaan::model()->findAll(array('order'=>'id_pekerjaan ASC')),'nama_pekerjaan','nama_pekerjaan'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->PEKERJAAN=>array('selected'=>true))));
                      echo $form->error($pasien,'PEKERJAAN');
                    ?>
                  </div>
                </div>

                <!-- TEL. Number Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'TELP',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'TELP',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return isNumber(event)'
                      ));
                    echo $form->error($pasien,'TELP'); ?>
                  </div>
                </div>

                <!-- Parent's Name Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NamaOrtu',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'NamaOrtu',array('class'=>'input-large'));
                    echo $form->error($pasien,'NamaOrtu'); ?>
                  </div>
                </div>

                <!-- Principal's Name (Husband / Wife) Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NamaSuamiIstri',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'NamaSuamiIstri',array('class'=>'input-large'));
                    echo $form->error($pasien,'NamaSuamiIstri'); ?>
                  </div>
                </div>
        
      
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'Desa',array('class'=>'col-sm-3  control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php echo $form->textField($pasien,'Desa',array('class'=>'input-large'));
                  echo $form->error($pasien,'Desa'); ?>
                  </div>
                </div>

                  <div class="form-group">
              <?php echo $form->labelEx($pasien,'ALAMAT',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php echo $form->textField($pasien,'ALAMAT',array('class'=>'input-large'));
                echo $form->error($pasien,'ALAMAT'); ?>
              </div>
            </div>         
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Form Detil Pasien</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
        
          <div class="form-group">
        <?php echo $form->labelEx($model,'GolPasien',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php 
       $kepesertaan_list = CHtml::ListData(GolPasien::model()->findAll(array('order'=>'NamaGol DESC')),'KodeGol','NamaGol');
         echo $form->dropDownList($model, 'GolPasien',$kepesertaan_list,array('disabled'=>'disabled'));
         echo $form->error($model,'GolPasien'); 
          echo $form->hiddenField($model,'NoDaftar');
               ?>
          </div>
        </div>
            <div class="form-group">
              <?php echo $form->labelEx($model,'WAKTU_DAFTAR',array('class'=>'col-sm-3 control-label no-padding-right'));?>
              <div class="col-sm-9">
               <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'WAKTU_DAFTAR',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'onClose' => 'js:function(dtText,dtI) {
                            //getBatasSEP(dtText);
                        }',
                    ),
        ));

              echo $form->error($model,'WAKTU_DAFTAR');
              ?>
                
                </div>
              </div>
            
            <div class="form-group">
            <?php echo $form->labelEx($model,'CaraMasuk',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php 
                          
       $list = CHtml::ListData(Caramasuk::model()->findAll(),'KodeMasuk','NamaMasuk');
         echo $form->dropDownList($model, 'CaraMasuk',$list);
                  echo $form->error($model,'CaraMasuk'); 
                  ?>
              </div>
            </div>
           
            <hr>
             <div class="form-group">
            <?php echo $form->labelEx($model,'poli1_id',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php 
                  $level_list = CHtml::ListData(Poli::model()->findAll(),'KODE_POLI','NAMA');
                  echo $form->dropDownList($model, 'poli1_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));
                 
                  ?>
               
                  <?php  echo $form->error($model,'poli1_id'); ;?>
              </div>
            </div>
          
             <div class="form-group">
            <?php echo $form->labelEx($model,'poli2_id',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php 
                  echo $form->dropDownList($model, 'poli2_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));

                  ?>
                  <?php  echo $form->error($model,'poli2_id'); ;?>
              </div>
            </div>
              <div class="form-group">
            <?php echo $form->labelEx($model,'poli3_id',array('class'=>'col-sm-3  control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php 
                  echo $form->dropDownList($model, 'poli3_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));
                  
                  ?>
                 <?php  echo $form->error($model,'poli3_id'); ;?>
              </div>
            </div>  

               <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Submit
          </button>

        
        </div>
      </div>
          </div>
        </div>
      </div>
    </div>

</div>
  <?php $this->endWidget();?>

<?php 
if(strpos($model->gOLPASIEN->NamaGol, 'BPJS') !== false)
{
?>
<div class="row-fluid">
    
<div style="display:none" class="alert alert-error" id="bpjs_error"></div>
<div style="display:none" class="alert alert-info" id="bpjs_info"></div>
<div class="widget-box widget-color-blue2">
  <div class="widget-header ">
    <h4 class="smaller">
      <img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs-icon.png"/> Riwayat Pelayanan
    </h4>
  </div>

  <div class="widget-body">
    <div class="widget-main">
        <span id='loading2' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
          <table id="tabel_riwayat" class="table  table-bordered table-hover">
            <thead>
              <tr>
                <th>Tgl SJP</th>
                <th>No SJP</th>
                <th>Jns Pel SJP</th>
                <th>Pol Tuj SJP</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
           
            </tbody>
            
          </table>
    </div>
  </div>
                    </div>
     
</div>
<?php 
}
?>
<script>
function riwayatTerakhir(noKartu) {
  $.ajax({
      type : 'POST',
      url  : '<?php echo Yii::app()->createUrl("WSBpjs/getRiwayatSEP");?>',
      data : 'nokartu='+noKartu,
      beforeSend : function(){
        $('#loading1').show();
      },

      error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
      },
      timeout : 3000,
      async: false,
      success : function(data){
          $('#tabel_riwayat > tbody').empty();
          $('#loading1').hide();
          
          if(data == '') return;

          var output = data;
          if(output.metadata.code == 200) {
              $('#bpjs_info').hide();
              
              $.each(output.response.list, function(i, item) {
                  
                  var row = '<tr>';

                  row += '<td>'+convertDate(item.tglSEP)+'</td>';
                  row += '<td>'+item.noSEP+'</td>';
                  row += '<td>'+item.jnsPelayanan+'</td>';
                  row += '<td>'+item.poliTujuan.nmPoli+'</td>';
                  row += '<td><input type="hidden" value="'+item.noSEP+'"><a href="javascript:void(0)" class="btn btn-danger btn-mini hapus_sep">Hapus SEP</a></td>';
                  row += '</tr>';
                  $('#tabel_riwayat > tbody').append(row);
                 // console.log(item.noSEP);
              });
             
          }

          else {
             $('#bpjs_info').show();
              $('#bpjs_info').html("<strong>Info !</strong> " + output.metadata.message);

          }
      },
  });
}

$(document).ready(function(){

  <?php 
  if(strpos($model->gOLPASIEN->NamaGol, 'BPJS') !== false)
  {
    ?>
    riwayatTerakhir();
    <?php
  }
  ?>

  $('.btn-tindakan').click(function(){
      $('#myModal').modal();  
  });

  $('#TrPendaftaranRjalan_GolPasien').change(function(){
      var txt = $('#TrPendaftaranRjalan_GolPasien option:selected').text();

      if(txt.includes('BPJS')) {
        $('#div-nokartu').show();
        $('#div-batas-sep').show();
      }
      else {
        $('#div-nokartu').hide();
        $('#div-batas-sep').hide();
      }
    });

   $('#carinomor').click(function(){
      carinomor();
    });

});
 function carinomor()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];


    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NoKartu').val()+'&jenis=kartu',
        beforeSend : function(){
          $('#loading1').show();

        },
        error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
        },
        timeout : 3000,
        async: false,
        success : function(data){


          $('#loading1').hide();
          
          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200){
            $('#badge').html('<span class="badge badge-success">Ok</span>');
            var peserta = output.response.peserta;
           
             $('#NoKartu').val(peserta.noKartu);
            $('#NIK').val(peserta.nik);
            $('#NAMAPESERTA').val(peserta.nama);
           $('#KELAMIN').val(peserta.sex);
            $('#PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);
            $('#TGLLAHIR').val(peserta.tglLahir);
            $('#PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);

            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
          else
          {
              $('#BpjsPasien_NoKartu').val('');
              $('#badge').html('<span class="badge badge-important">Nomor Kartu tidak ada</span>');
          }
        }
      });
   
  }

function getBatasSEP(tgl)
{
    $.ajax({
        url: '<?php echo Yii::app()->createUrl("ajaxRequest/GetBatasSEP");?>',
        type: 'POST', // Send post data
        data: 'tgl='+tgl,
        async: false,

        success: function(data){
            var hasil = JSON.parse(data);
          
            $('#TrPendaftaranRjalan_BATAS_CETAK_SEP').val(hasil.tanggal);
        }
    });
}




<?php 
  if(!empty($model->pASIEN)){
?>
$('#TrPendaftaranRjalan_PASIEN_NAMA').val('<?php echo $model->pASIEN->NAMA;?>');
<?php 
}
?>
function isiDataPasien(ui)
{
 
   $("#TrPendaftaranRjalan_NoMedrec").val(ui.item.id);
   $("#TrPendaftaranRjalan_PASIEN_NAMA").val(ui.item.value); 
   

}


 </script>