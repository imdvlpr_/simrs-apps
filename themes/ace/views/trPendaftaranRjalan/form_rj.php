<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }


    .errorMessage{
      color: red;
    }

    .form-horizontal .control-label{
      width: 100px;
    }

    .form-horizontal .controls{
      margin-left: 110px;
    }

    .navbar{
      margin-bottom: 0px;
    }
</style>
<div class="container-fluid">
    <div class="row-fluid">
       
         
        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('registrasi/rawatJalan')),
                            array('name' => 'Pendaftaran')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
               
<h3>Pendaftaran Pasien Lama <?php echo $golpasien->NamaGol;?></h3>

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>
              <?php 
// echo $form->errorSummary($kunjungan,'<div class="alert alert-error">Kunjungan: Silakan perbaiki beberapa eror berikut:','</div>');
               
?>                           <!-- block -->
 
 <div class="span12">
   <div class="row-fluid">
      <div class="control-group">
          <?php echo CHtml::label('No RM','',array('class'=>'control-label'));?>
          <div class="controls">
            <?php
              $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
          'name' => 'PASIEN_NAMA',
          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('AjaxRequest/getPasienLikeParam').'",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
          'htmlOptions'=>array('placeholder'=>'Ketik Nama | No Reg'),    
          'options' => array(
              'minLength' => 1,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 
                isiDataPasien(ui);
              }',
      ),
      ));
                 
            ?>
            
            
          </div>
        </div>
      <div class="control-group">
        <label class="control-label">No RM</label>
        <div class="controls">
          <input type="text" name="cari_no_rm" id="cari_no_rm"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">&nbsp;</label>
        <div class="controls">
          <button type="submit" class="btn btn-primary" name="cari_pasien">Cari Pasien</button>
        </div>
         
      </div>
   </div>

 </div>     
       
 <div class="span4">
<?php echo $form->errorSummary($pasien,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>'); 
;?>
      <div class="row-fluid">
        <div class="block">
          <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Data RM Pasien</div>
          </div>
         <div class="block-content collapse in">
            <div class="span12">
             
              <fieldset>
               
                

                <!-- Medical Record Number Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'NoMedrec',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'NoMedrec',array('class'=>'input-large'));
                        echo $form->error($pasien,'NoMedrec');
                    ?>
                     <!--  <a id='carirm' href="javascript:void(0);"><i class="icon-search"></i> Cari RM</a><span id='loading3' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span> -->
                  </div>
                </div>

                <!-- Identification Number Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'NOIDENTITAS',array('class'=>'control-label'));?>
                  <div class="controls">
                    <!-- TODO: Input Number Only! -->
                    <?php echo $form->textField($pasien,'NOIDENTITAS',array(
                      'class'=>'input-large',
                      'tabindex'=>'1',
                      'placeholder' => '16 digit Nomor KTP',
                      'minLength' => '16',
                      'maxLength' => '16',
                      'onkeypress' => 'return isNumber(event)'));?>
                    <?php echo $form->error($pasien,'NOIDENTITAS'); ?>
                  </div>
                </div>

                <!-- Patient's Name Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'NAMA',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'NAMA',array('class'=>'input-large','tabindex'=>'2')); ?>
                    <?php echo $form->error($pasien,'NAMA'); ?>
                  </div>
                </div>

                <!-- Patient's Sex Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'JENSKEL',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->dropDownList($pasien, 'JENSKEL',array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('tabindex'=>'4'));
                    // echo $form->radioButtonList($model, 'JENSKEL', array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('labelOptions'=>array('style'=>'display:inline','tabindex'=>'3')));
                    echo $form->error($pasien,'JENSKEL');
                    ?>
                  </div>
                </div>

                <!-- Patient's Date of Birth -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'TGLLAHIR',array('class'=>'control-label'));?>
                  <div class="controls">
                    <div id="datePickerComponent" class="input-append date margin-00" data-date="<?php echo date('d-m-Y')?>" data-date-format="dd-mm-yyyy">
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                      'model'=>$pasien,
                      'value'=>'TGLLAHIR',
                      'attribute'=>'TGLLAHIR',
                      // additional JavaScript options for the date picker plugin
                      'options'=>array(
                      'showAnim'=>'fold',
                      'showButtonPanel'=>true,
                      'autoSize'=>true,
                      'dateFormat'=>'dd/mm/yy',
                        'changeMonth'=>true,
                        'changeYear'=>true,
                        'yearRange'=>'1900:2099',
                       ),

                      'htmlOptions'=>array(
                        'class'=>'input-medium',
                      ),
                    ));
                    echo $form->error($pasien,'TGLLAHIR',array('class'=>'control-label')); ?>
                    </div>
                  </div>
                </div>

                <!-- Address Details -->
                <div class="control-group">
                  <?php echo CHtml::label('<strong>Alamat Lengkap:</strong>','',array('class'=>'control-label'));?>
                  <!-- TODO: Why Kosongan ada class control nya ? -->
                  <div class="controls"></div>
                </div>

                <!-- Address Street's Name Field -->
              
                <!-- Address District (Desa) Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'Desa',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php echo $form->textField($pasien,'Desa',array('class'=>'input-large'));
                  echo $form->error($pasien,'Desa'); ?>
                  </div>
                </div>

                  <div class="control-group">
              <?php echo $form->labelEx($pasien,'ALAMAT',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($pasien,'ALAMAT',array('class'=>'input-large'));
                echo $form->error($pasien,'ALAMAT'); ?>
              </div>
            </div>         

               

                <!-- Submit Action Button -->
               
              </fieldset>
             
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="span4">
      <div class="row-fluid">
        <div class="block">
  <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Data Detil Pasien</div>
          </div>
  <div class="block-content collapse in">
            <div class="span12">
 <div class="control-group">
                  <?php echo $form->labelEx($pasien,'TMPLAHIR',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'TMPLAHIR',array('class'=>'input-large'));
                    echo $form->error($pasien,'TMPLAHIR'); ?>
                  </div>
                </div>

                <!-- Weight Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'BeratLahir',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'BeratLahir',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return weightNumber(event)',
                      'placeholder' => 'Berat dalam Kg. Contoh: 64.5'
                    ));
                    echo $form->error($pasien,'BeratLahir');?>
                  </div>
                </div>

                <!-- Blood Type Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'GOLDARAH',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($pasien,'GOLDARAH',CHtml::ListData(DmGoldarah::model()->findAll(),'id_goldarah','nama_goldarah'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->GOLDARAH=>array('selected'=>true))));
                      echo $form->error($pasien,'GOLDARAH');
                    ?>
                  </div>
                </div>

                <!-- Religion Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'AGAMA',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php
                     echo $form->dropDownList($pasien,'AGAMA',CHtml::ListData(DmAgama::model()->findAll(array('order'=>'id_agama ASC')),'id_agama','nama_agama'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->AGAMA=>array('selected'=>true))));
                     echo $form->error($pasien,'AGAMA');
                  ?>
                  </div>
                </div>

                <!-- Marital Status -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'STATUSPERKAWINAN',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($pasien,'STATUSPERKAWINAN',CHtml::ListData(DmStatuskawin::model()->findAll(array('order'=>'id_statuskawin ASC')),'id_statuskawin','nama_statuskawin'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->STATUSPERKAWINAN=>array('selected'=>true))));
                      echo $form->error($pasien,'STATUSPERKAWINAN');
                    ?>
                  </div>
                </div>

                <!-- Job Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'PEKERJAAN',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($pasien,'PEKERJAAN',CHtml::ListData(DmPekerjaan::model()->findAll(array('order'=>'id_pekerjaan ASC')),'nama_pekerjaan','nama_pekerjaan'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->PEKERJAAN=>array('selected'=>true))));
                      echo $form->error($pasien,'PEKERJAAN');
                    ?>
                  </div>
                </div>

                <!-- TEL. Number Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'TELP',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'TELP',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return isNumber(event)'
                      ));
                    echo $form->error($pasien,'TELP'); ?>
                  </div>
                </div>

                <!-- Parent's Name Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'NamaOrtu',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'NamaOrtu',array('class'=>'input-large'));
                    echo $form->error($pasien,'NamaOrtu'); ?>
                  </div>
                </div>

                <!-- Principal's Name (Husband / Wife) Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($pasien,'NamaSuamiIstri',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($pasien,'NamaSuamiIstri',array('class'=>'input-large'));
                    echo $form->error($pasien,'NamaSuamiIstri'); ?>
                  </div>
                </div>
        </div>
</div>
</div>
        </div>
</div>
    <div class="span4">
      <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

      ?>
      <div class="row-fluid">
        <div class="block">
          <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Tindakan Rawat Jalan</div>
          </div>
         <div class="block-content collapse in">
            <div class="span12">
             
              <fieldset>
               
   
          <div class="control-group">
        <?php echo $form->labelEx($model,'GolPasien',array('class'=>'control-label'));?>
          <div class="controls">
            <?php 
       $kepesertaan_list = CHtml::ListData(GolPasien::model()->findAll(array('order'=>'NamaGol DESC')),'KodeGol','NamaGol');
         echo $form->dropDownList($model, 'GolPasien',$kepesertaan_list,array('disabled'=>'disabled'));
         echo $form->error($model,'GolPasien'); 
          echo $form->hiddenField($model,'NoDaftar');
               ?>
          </div>
        </div>
            <div class="control-group">
              <?php echo $form->labelEx($model,'WAKTU_DAFTAR',array('class'=>'control-label'));?>
              <div class="controls">
               <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'WAKTU_DAFTAR',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'onClose' => 'js:function(dtText,dtI) {
                            //getBatasSEP(dtText);
                        }',
                    ),
        ));

              echo $form->error($model,'WAKTU_DAFTAR');
              ?>
                
                </div>
              </div>
            
            <div class="control-group">
            <?php echo $form->labelEx($model,'CaraMasuk',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                          
       $list = CHtml::ListData(Caramasuk::model()->findAll(),'KodeMasuk','NamaMasuk');
         echo $form->dropDownList($model, 'CaraMasuk',$list);
                  echo $form->error($model,'CaraMasuk'); 
                  ?>
              </div>
            </div>
           
            <hr>
             <div class="control-group">
            <?php echo $form->labelEx($model,'poli1_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  $level_list = CHtml::ListData(Poli::model()->findAll(),'KODE_POLI','NAMA');
                  echo $form->dropDownList($model, 'poli1_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));
                 
                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan1" id="tindakan1">Tindakan</button>
                  <?php  echo $form->error($model,'poli1_id'); ;?>
              </div>
            </div>
          
             <div class="control-group">
            <?php echo $form->labelEx($model,'poli2_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->dropDownList($model, 'poli2_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));

                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan2" id="tindakan2">Tindakan</button>
                  <?php  echo $form->error($model,'poli2_id'); ;?>
              </div>
            </div>
              <div class="control-group">
            <?php echo $form->labelEx($model,'poli3_id',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->dropDownList($model, 'poli3_id',$level_list,array('prompt'=>'.: Pilih Poli :.','class'=>'span6'));
                  
                  ?>
                  <button type="button" class="btn btn-primary btn-tindakan" name="tindakan3" id="tindakan3">Tindakan</button>
                  <?php  echo $form->error($model,'poli3_id'); ;?>
              </div>
            </div>  
             
              </fieldset>
            
            </div>
          </div>
        </div>
      </div>
    </div>
<div id="myModal" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">&times;</button>
        <h3>Tindak Lanjut Rawat Jalan</h3>
    </div>
    <div class="modal-body">
       
    <div class="control-group">
      <?php echo CHtml::label('Tindak Lanjut','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 
          $list_tdl = CHtml::ListData(TindakLanjutRj::model()->findAll(),'NamaTdkLanjut','NamaTdkLanjut');
                  // echo $form->dropDownList($model, 'poli1_id',$level_list,array('class'=>'span6'));
            echo CHtml::dropDownList('tindak_lanjut','',$list_tdl,array('prompt'=>'.: Pilih Tindak Lanjut :.','class'=>'input-large'));

            
          ?>

        </div>
      </div>
    
     <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 1','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

          $list_unit = CHtml::ListData(Unit::model()->findAllByAttributes(array('unit_tipe'=>2)),'KodeUnit','NamaUnit');
                  // echo $form->dropDownList($model, 'poli1_id',$level_list,array('class'=>'span6'));
            echo CHtml::dropDownList('rujukan_intern1','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div>
      <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 2','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

            echo CHtml::dropDownList('rujukan_intern2','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 3','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

          echo CHtml::dropDownList('rujukan_intern3','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 4','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

            echo CHtml::dropDownList('rujukan_intern4','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div> <div class="control-group">
      <?php echo CHtml::label('Rujukan Intern 5','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 

     
            echo CHtml::dropDownList('rujukan_intern5','',$list_unit,array('prompt'=>'.: Pilih Rujukan Intern :.','class'=>'input-large rujukan_intern'));

            
          ?>

        </div>
      </div>
   
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" class="btn" href="#">Tutup</a>
    </div>
</div>
<div class="row-fluid">
       <div style="margin-top: 0px;text-align: center;" class="form-actions">
                     <button type="submit" class="btn btn-primary" name="simpan_pendaftaran">Simpan Data Baru</button>
                            
                      </div>
</div>  
   <?php $this->endWidget();?>
</div>

<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
  
  $cs->registerScriptFile($baseUrl.'/assets/enter-keypress.js');
?>
<script>


$(document).ready(function(){

  $('.btn-tindakan').click(function(){
      $('#myModal').modal();  
  });

 

});

  jQuery(document).ready(function() {   
     FormValidation.init();

    
  });
  

        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();

            
        });



function isiDataPasien(ui)
{
 
   $("#cari_no_rm").val(ui.item.id);
   $("#PASIEN_NAMA").val(ui.item.value); 
   

}


 </script>
            </div>
        </div>
    </div>
    <hr>
  
</div>