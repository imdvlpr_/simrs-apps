<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('BPendaftaranRjalan/index')),
                            array('name' => 'Pendaftaran')
);

?>

<div class="row">
        <!--/span-->
        <div class="col-xs-12" id="content">
          
            
          
           
            
                <!-- block -->
               
<h3>Pendaftaran Pasien Lama <?php echo $golpasien->NamaGol;?></h3>
<p>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/cariPasien',array('jenis'=>1001));?>" class="btn btn-primary">Pasien UMUM</a>
<a class="btn btn-primary">Pasien BPJS</a>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/cariPasien',array('jenis'=>2401));?>" class="btn btn-primary">Pasien In-Health</a>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/cariPasien',array('jenis'=>1002));?>" class="btn btn-primary">Pasien JAMKESDA</a>
</p>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'registrasi-pasien-lama-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'method' => 'get',
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); 

?>                           <!-- block -->
    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>                      
       
      <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">No RM</label>
        <div class="col-sm-9">
          <input type="text" name="cari_no_rm" class="input" id="cari_no_rm" value="<?php echo !empty($_POST['cari_no_rm'])? $_POST['cari_no_rm'] : '';?>"/>
        </div>
      </div>
       <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-search bigger-110"></i>
            Cari Pasien
          </button>

        
        </div>
      </div>

  <?php $this->endWidget();?>

</div>
   </div>
        </div>
<script type="text/javascript">

  $('#cari_no_rm').focus();

  $(document).on('keydown','input#cari_no_rm', function(e) {
      var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      

      if (e.keyCode == 13) {
          $('form').submit();
      }
    
  });
</script>
  