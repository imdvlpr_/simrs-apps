<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Laporan Rawat Jalan','url'=>Yii::app()->createUrl('trPendaftaranRjalan/laporan')),
                            
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="row">
    <div class="col-xs-12">
        
                                 <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  'method' => 'GET',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",

    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>  
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>

        
        </div>
      </div>
  
  </fieldset>
  <?php $this->endWidget();?>

<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>
<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No REG</th>
      <th>Nama Pasien</th>
      <th>Status</th>
      <th>Kelas</th>
      <th>Ruang</th>
      <th>Alamat</th>
      <th>Tgl Masuk</th>
      <th>Tgl Keluar</th>
      <th>Lm<br>Dirawat</th>
      <th>D.Y Merawat</th>
      <th>TOTAL</th>
     
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;
    foreach($model->searchLaporan() as $item)
    {

      $selisih_hari = 1;

      if(!empty($item->tanggal_pulang))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($item->tanggal_masuk), Yii::app()->helper->convertSQLDate($item->tanggal_pulang));
      }

      $rawatJalan = $item;
      $rawatRincian = $rawatJalan->trRawatJalanRincians;
      $total_ird = 0;
      
      
      if(!empty($rawatRincian))
      {
        $total_ird = $total_ird + $rawatRincian->obs_ird;
        $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;
      }

      

      foreach($rawatJalan->trRawatJalanVisiteDokters as $visite)
      {
        if(!empty($visite))
          $total_ird = $total_ird + $visite->biaya_visite * $visite->jumlah_visite_ird;
      }   
     

      $biaya_ird_tnop_total = 0;
      foreach($rawatJalan->trRawatJalanTnops as $tnop)
      {
          $jumlah_tindakan = $tnop->jumlah_tindakan;
          $biaya_ird = $tnop->biaya_ird;
        

          $biaya_ird_tnop_total = $biaya_ird_tnop_total + $biaya_ird * $jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_ird_tnop_total;

      foreach($rawatJalan->trRawatJalanPenunjangs as $supp)
      {
          $total_ird = $total_ird + $supp->biaya_ird * $supp->jumlah_tindakan;
      }

      $biaya_obat_ird_total = 0;
      foreach($rawatJalan->trRawatJalanAlkes as $obat)
      {

        $biaya_obat_ird_total = $biaya_obat_ird_total + $obat->biaya_ird * $obat->jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_obat_ird_total;

      $biaya_ird_lain = 0;
      foreach($rawatJalan->trRawatJalanLains as $lain)
      {

        $biaya_ird_lain = $biaya_ird_lain + $lain->biaya_ird * $lain->jumlah_tindakan;
        
      }

      $total_ird = $total_ird + $biaya_ird_lain;
  ?>
    <tr>
     <td><?php echo $i++;?></td>
      <td><?php echo $item->pASIEN->NoMedrec;?></td>
      <td><?php echo $item->pASIEN->NAMA;?></td>
      <td><?php echo $item->gOLPASIEN->NamaGol;?></td>
      <td><?php //echo $item->kamar->kelas->nama_kelas;?></td>
      <td><?php //echo $item->kamar->nama_kamar;?></td>
      <td><?php echo $item->pASIEN->FULLADDRESS;?></td>
      <td><?php echo $item->tanggal_masuk;?></td>
      <td><?php echo $item->tanggal_pulang;?></td> 
      <td>
<?php 
       $selisih_hari = 1;

        if(!empty($item->tanggal_pulang))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($item->tanggal_masuk), Yii::app()->helper->convertSQLDate($item->tanggal_pulang));
        }

        echo $selisih_hari;
?>
      </td>
      <td>
      <?php 
      
      if(!empty($item->dokter))
        echo $item->dokter->NICKNAME;
      ?></td>
      <td>
       <?php 
        
        echo number_format($total_ird,0,',','.');
      ?>
      </td>
     
      
     
    </tr>
    <?php 
    }

    foreach($ird->searchLaporan() as $ugd)
    {


      $selisih_hari = 1;

      if(!empty($item->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($item->tanggal_masuk), Yii::app()->helper->convertSQLDate($item->tanggal_keluar));
      }

 

      $rawatInap = $ugd;
      $rawatRincian = $rawatInap->trRawatInapRincians;
      $total_ird = 0;
      
      
      if(!empty($rawatRincian)){
        $total_ird = $total_ird + $rawatRincian->obs_ird;
        $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;
      }

      

      foreach($rawatInap->tRIVisiteDokters as $visite)
      {
        if(!empty($visite))
          $total_ird = $total_ird + $visite->biaya_visite * $visite->jumlah_visite_ird;
      }
   

     
     

      $biaya_ird_tnop_total = 0;
      foreach($rawatInap->trRawatInapTnops as $tnop)
      {
           $jumlah_tindakan = $tnop->jumlah_tindakan;
          $biaya_ird = $tnop->biaya_ird;
        

          $biaya_ird_tnop_total = $biaya_ird_tnop_total + $biaya_ird;
      }

      $total_ird = $total_ird + $biaya_ird_tnop_total;

      foreach($rawatInap->trRawatInapPenunjangs as $supp)
      {
          $total_ird = $total_ird + $supp->biaya_ird;
      }

      $biaya_obat_ird_total = 0;
      foreach($rawatInap->trRawatInapAlkes as $obat)
      {

        $biaya_obat_ird_total = $biaya_obat_ird_total + $obat->biaya_ird;
      }

      $total_ird = $total_ird + $biaya_obat_ird_total;

      $biaya_ird_lain = 0;
      foreach($rawatInap->trRawatInapLains as $lain)
      {

        $biaya_ird_lain = $biaya_ird_lain + $lain->biaya_ird;
        
      }

      $total_ird = $total_ird + $biaya_ird_lain;
  ?>
    <tr>
     <td><?php echo $i++;?></td>
      <td><?php echo $ugd->pASIEN->NoMedrec;?></td>
      <td><?php echo $ugd->pASIEN->NAMA;?></td>
      <td><?php echo $ugd->jenisPasien->NamaGol;?></td>
      <td><?php //echo $item->kamar->kelas->nama_kelas;?></td>
      <td><?php //echo $item->kamar->nama_kamar;?></td>
      <td><?php echo $ugd->pASIEN->FULLADDRESS;?></td>
      <td><?php echo $ugd->tanggal_masuk;?></td>
      <td><?php echo $ugd->tanggal_keluar;?></td> 
      <td>
<?php 
       $selisih_hari = 1;

        if(!empty($ugd->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHari(Yii::app()->helper->convertSQLDate($ugd->tanggal_masuk), Yii::app()->helper->convertSQLDate($ugd->tanggal_keluar));
        }

        echo $selisih_hari;
?>
      </td>
      <td>
      <?php 
      
      if(!empty($ugd->dokter))
        echo $ugd->dokter->NICKNAME;
      ?></td>
      <td>
       <?php 
    
        echo number_format($total_ird,0,',','.');
      ?>
      </td>
     
    
     
    </tr>
    <?php 
    }
    ?>
  </tbody>
</table>
<?php
  }
?>

    </div>
  
</div>