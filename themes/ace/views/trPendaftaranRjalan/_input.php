  <?php
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),
));
 $baseUrl = Yii::app()->theme->baseUrl;
    $cs = Yii::app()->getClientScript();


              ?>
              <fieldset>
              <?php
echo $form->errorSummary($rawatJalan,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;

?>
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }


  .jumlah{
    width:30px;
  }
</style>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
 <table class="table">
   <tr>
     <td>Nama Pasien</td>
     <td>:</td>
     <td colspan="2"><strong><?php echo $pasien->NAMA.' / '.$rawatJalan->gOLPASIEN->NamaGol;?></strong></td>

     <td>No Reg</td>
     <td>:</td>
     <td><?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk</td>
     <td>:</td>
     <td colspan="2"><?php echo $rawatJalan->tanggal_masuk;?></td>

     <td>Tgl Pulang</td>
     <td>:</td>
     <td><?php echo !empty($rawatJalan->tanggal_keluar) ? $rawatJalan->tanggal_keluar : '-' ; ?></td>
   </tr>

   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td colspan="2"><?php echo $pasien->FULLADDRESS;?></td>

     <td>Dokter yang merawat</td>
     <td>:</td>
     <td>
    <?php
    $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatJalan->dokter)){
        $nama_dokter = $rawatJalan->dokter->FULLNAME;
     }


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
      ?>
      <input id="nama_dokter_perawat" placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter_merawat" name="nama_dokter_merawat" autocomplete="off" value="<?php echo $nama_dokter;?>"/>


     <?php

      echo $form->hiddenField($rawatJalan,'dokter_id');?>
     </td>

     </td>
   </tr>
 </table>


 <table class="table table-condensed">
   <thead>
   <tr>
     <th>A</th>
     <th>LAYANAN</th>
     <th >RAWAT DARURAT</th>

     <th colspan="3" >TARIF</th>
   </tr>
   </thead>
   <tbody>
    <tr>
     <td>&nbsp;</td>
     <td>1. Observasi</td>
     <td>&nbsp;</td>
     <td colspan="3">Rp&nbsp;<?php

     echo $form->textField($rawatRincian,'obs_ird',array('class'=>'input-medium uang'));
     $total_ird = $total_ird + $rawatRincian->obs_ird;
     ?></td>


   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>2. Pengawasan Dokter</td>
     <td>
      <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter_ird" name="nama_dokter_ird" autocomplete="off" value="<?php echo $nama_dokter_ird;?>"/>


     <?php echo $form->hiddenField($rawatRincian,'dokter_ird');?>
     </td>
     <td colspan="3">Rp <?php

     echo $form->textField($rawatRincian,'biaya_pengawasan',array('class'=>'input-medium uang'));

     if(!empty($rawatRincian))
        $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;
     ?>
     </td>

   </tr>

   </tbody>
   <thead>
   <tr>
     <th>B</th>
     <th colspan="5">PELAYANAN MEDIK</th>

   </tr>
   </thead>
   <tbody id="tbody-dokter">
     <?php

   if(!is_null($rawatJalan->getError('ERROR_VISITE'))){
    ?>
    <tr>
    <td colspan="6">
   <div class="alert alert-error">
   <?php
      echo $form->error($rawatJalan,'ERROR_VISITE');
?>
</div>
</td>
</tr>
<?php

}

    $urutan = 1;
      if(!empty($rawatJalan->trRawatJalanVisiteDokters)){

      foreach($rawatJalan->trRawatJalanVisiteDokters as $visite)
      {

        if(!empty($visite))
          $total_ird = $total_ird + $visite->biaya_visite * $visite->jumlah_visite_ird;





        $dokterVisiteIRD = '';
        if(!empty($visite->dokterIrd))
            $dokterVisiteIRD = $visite->dokterIrd->FULLNAME;


    ?>
    <tr class="row-dokter">
     <td>&nbsp;</td>
     <td>
     <span class="number"><?php echo $urutan++;?>. </span>
     <input placeholder="Ketik Tindakan Dokter" type="text" class="input-medium nama_jenis_visite" name="nama_jenis_visite" autocomplete="off" value="<?php echo $visite->jenisVisite->nama_visite;?>"/>
        <input type="hidden" class="id_jenis_visite" name="id_jenis_visite[]" value="<?php echo $visite->id_jenis_visite;?>"/>


     </td>
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" value="<?php echo $dokterVisiteIRD;?>"/>
     <input type="hidden" class="id_dokter" name="id_dokter_visite_ird[]" value="<?php echo $visite->id_dokter;?>"/>

    </td>
     <td colspan="3">
     Rp&nbsp;<input type="text" name="biaya_visite_ird[]" class="input-medium uang" placeholder="-" value="<?php echo $visite->biaya_visite;?>"/>
     <input type="text" name="jumlah_visite_ird[]" class="input-small jumlah" placeholder="Jumlah Visite" value="<?php echo $visite->jumlah_visite_ird;?>"/>
      <a href="javascript:void(0)" class="hapus-dokter btn btn-warning" >Hapus</a>
     </td>


   </tr>
   <?php
      }
    }

   ?>
    <tr id="row-dokter" class="row-dokter">
     <td>&nbsp;</td>
     <td>
     <span class="number"><?php echo $urutan++;?>. </span><input placeholder="Ketik Tindakan Dokter" type="text" class="input-medium nama_jenis_visite" name="nama_jenis_visite" autocomplete="off" />
        <input type="hidden" class="id_jenis_visite" name="id_jenis_visite[]" />


     </td>
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" />
     <input type="hidden" class="id_dokter" name="id_dokter_visite_ird[]" />

    </td>
     <td colspan="3">
     Rp&nbsp;<input type="text" name="biaya_visite_ird[]" class="input-medium uang" placeholder="-" value="0"/>
     <input type="text" name="jumlah_visite_ird[]" class="input-small jumlah" placeholder="Jumlah Visite" value="1"/>

     <a href="javascript:void(0)" class="hapus-dokter btn btn-warning" >Hapus</a>

     </td>


   </tr>
  <tr class="row-tambah-dokter">
  <td>&nbsp;</td>
    <td colspan="5"><a id="tambah-dokter" href="javascript:void(0)" class=" btn btn-success"> Tambah Item </a></td>
  </tr>

  </tbody>

<?php ?>
  <tr>
     <td>&nbsp;</td>
     <td colspan="2"><strong>I. Tindakan Medik Non-Operasi</strong></td>
     <td colspan="3"><strong><div class="span3">Biaya</div><div class="span4" align="center">Biaya Satuan</div><div class="span5">Jml</div></strong></td>

   </tr>
  </tbody>
   <tbody>
   <?php

   if(!is_null($rawatJalan->getError('ERROR_TNOP'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php
      echo $form->error($rawatJalan,'ERROR_TNOP');
?>
</div>
</td>
</tr>
<?php
}

   $listTnop = TindakanMedisNonOperatif::model()->findAll();
   $i = 0;

   foreach($listTnop as $tnop){
      $attr = array(
        'id_rawat_jalan' => $rawatJalan->Id,
        'id_tnop' => $tnop->id_tindakan
      );
      $itemTnop = TrRawatJalanTnop::model()->findByAttributes($attr);


      $jumlah_tindakan = 0;
      $biaya_ird = 0;

		$total_tnop_ird = 0;
      if(!empty($itemTnop)){
        $jumlah_tindakan = $itemTnop->jumlah_tindakan;

        $biaya_ird = $itemTnop->biaya_ird;
		$total_tnop_ird = $total_tnop_ird + $biaya_ird * $jumlah_tindakan;

      }

      $total_ird = $total_ird + $total_tnop_ird;
    ?>
   <tr>
     <td>&nbsp;</td>
     <td><span class="number-tnop"><?php echo strtolower(chr(64 + ($i+1)));?>. </span><?php echo $tnop->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td colspan="3">Rp&nbsp;<input type="text" name="biaya_ird_tnop[]" value="<?php echo $biaya_ird;?>"  class="input-medium uang" placeholder="-" />
      <?php

     // if($obat->nama_obat_alkes == 'Obat-obatan')
        echo CHtml::textField('nilai_per_tnop','0',array('class'=>'input-small uang  per_item'));

     ?>
 <input type="text" name="jumlah_tindakan_tnop[]" class="input-small jumlah" placeholder="Jml Tndk" value="<?php echo $jumlah_tindakan;?>"/>
     <input type="hidden" name="id_tindakan_tnop[]" value="<?php echo $tnop->id_tindakan;?>"/>
     </td>
   </tr>
     <?
 $i++;
    }

    ?>


   </tbody>
   <thead>
   <tr>
     <th>C</th>
     <th colspan="6">LAYANAN PENUNJANG</th>

   </tr>
   </thead>
   <tbody id="tbody-supp">
    <?php
   if(!is_null($rawatJalan->getError('ERROR_SUPP'))){
    ?>
    <tr>
    <td colspan="6">
   <div class="alert alert-error">
   <?php
      echo $form->error($rawatJalan,'ERROR_SUPP');
?>
</div>
</td>
</tr>
<?php

}

    $urutan = 1;
    $biaya_ird_supp = 0;
      foreach($rawatJalan->trRawatJalanPenunjangs as $supp)
      {


          $biaya_ird_supp = $biaya_ird_supp + $supp->biaya_ird * $supp->jumlah_tindakan;
          $total_ird = $total_ird + $biaya_ird_supp;
    ?>
     <tr class="row-supp">
     <td>&nbsp;</td>
     <td>
       <span class="number-supp"><?php echo $urutan++;?>. </span>
       <input value="<?php echo $supp->penunjang->nama_tindakan;?>" type="text" class="input-medium jenis-supp" name="jenis-supp" autocomplete="off" />
        <input value="<?php echo $supp->id_penunjang;?>" type="hidden" class="id_jenis_supp" name="id_jenis_supp[]" />
     </td>
     <td>&nbsp;</td>
     <td colspan="3">Rp&nbsp;<input value="<?php echo $supp->biaya_ird;?>" type="text" name="biaya_ird_supp[]" class="input-medium uang" placeholder="-" />
          <input type="text" value="<?php echo $supp->jumlah_tindakan;?>" name="jumlah_tindakan_supp[]" class="input-small jumlah" placeholder="Jml Lyn"/>
     <a href="javascript:void(0)" class="hapus-supp btn btn-warning" >Hapus</a>
</td>
   </tr>
    <?php

    }

    ?>
    <tr id="row-supp" class="row-supp">
     <td>&nbsp;</td>
     <td>
       <span class="number-supp"><?php echo $urutan++;?>. </span><input placeholder="Jenis Layanan Penunjang" type="text" class="input-medium jenis-supp" name="jenis-supp" autocomplete="off" />
        <input type="hidden" class="id_jenis_supp" name="id_jenis_supp[]" />
     </td>
     <td>&nbsp;</td>
     <td colspan="3">Rp&nbsp;<input type="text" value="0" name="biaya_ird_supp[]" class="input-medium uang" placeholder="-" />
     <input type="text" name="jumlah_tindakan_supp[]" class="input-small jumlah" placeholder="Jml Lyn" value="1"/>
     <a href="javascript:void(0)" class="hapus-supp btn btn-warning" >Hapus</a>

     </td>
   </tr>
  <tr class="row-tambah-supp">
  <td>&nbsp;</td>
    <td colspan="6"><a id="tambah-supp" href="javascript:void(0)" class=" btn btn-success"> Tambah Layanan </a></td>
  </tr>
   </tbody>
   <thead>
   <tr>
     <th>D</th>
     <th colspan="2"><strong>OBAT dan ALAT KESEHATAN</strong></th>
      <th colspan="4">
      <div class="span3">Biaya</div>
      <div class="span4" align="center">Biaya Satuan</div>
      <div class="span5">Jml</div>
      </th>
   </tr>
   </thead>
   <tbody>
   <?php
   if(!is_null($rawatJalan->getError('ERROR_ALKES')))
   {
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php
      echo $form->error($rawatJalan,'ERROR_ALKES');
?>
</div>
</td>
</tr>
<?php
}
   $listObat = ObatAlkes::model()->findAll();
   $i = 0;
   foreach($listObat as $obat){
      $attr = array(
        'id_rawat_jalan' => $rawatJalan->Id,
        'id_alkes' => $obat->id_obat_alkes
      );
      $itemObat = TrRawatJalanAlkes::model()->findByAttributes($attr);

      $jumlah_tindakan = 0;
      $biaya_ird = 0;

      $biaya_obat_ird_total = 0;

      if(!empty($itemObat)){
        $jumlah_tindakan = $itemObat->jumlah_tindakan;

        $biaya_ird = $itemObat->biaya_ird;
        $biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird * $jumlah_tindakan;

      }

      $total_ird = $total_ird + $biaya_obat_ird_total;

    ?>
    <tr>
     <td>&nbsp;</td>
     <td><span><?php echo ($i+1);?>. </span><?php echo $obat->nama_obat_alkes;?></td>
     <td></td>
     <td colspan="3">
Rp
     <input type="text" name="biaya_ird_alkes[]" class="input-medium" value="<?php echo $biaya_ird;?>" />

     <?php

     // if($obat->nama_obat_alkes == 'Obat-obatan')
        echo CHtml::textField('nilai_per_obat','0',array('class'=>'input-small uang  per_item'));

     ?>

        <input type="text" name="jumlah_obat[]" class="input-small jumlah" placeholder="Jml Obat" value="<?php echo $jumlah_tindakan;?>"/>
     <input type="hidden" name="id_tindakan_obat[]" value="<?php echo $obat->id_obat_alkes;?>"/>
     </td>


   </tr>
   <?php
   $i++;
   }

   ?>

   </tbody>
   <thead>
   <tr>
   <th>E.</th>
     <th colspan="2"><strong>Lain-lain</strong></th>
     <th colspan="4">
      <div class="span5">Biaya</div>

      <div class="span7">Jml</div>
      </th>

   </tr>
   </thead>
   <tbody >
   <?php
   if(!is_null($rawatJalan->getError('ERROR_LAIN')))
   {
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php
      echo $form->error($rawatJalan,'ERROR_LAIN');
?>
</div>
</td>
</tr>
<?php
}

   $listLain = TindakanMedisLain::model()->findAll();
   $i = 0;
   foreach($listLain as $lain){
       $attr = array(
        'id_rawat_jalan' => $rawatJalan->Id,
        'id_lain' => $lain->id_tindakan
      );

      $itemLain = TrRawatJalanLain::model()->findByAttributes($attr);
      $jumlah_tindakan = 0;

      $biaya_ird = 0;
      $biaya_ird_lain = 0;
      if(!empty($itemLain)){
        $jumlah_tindakan = $itemLain->jumlah_tindakan;

        $biaya_ird = $itemLain->biaya_ird;
        $biaya_ird_lain= $biaya_ird_lain + $biaya_ird * $jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_ird_lain;
    ?>
    <tr>
     <td>&nbsp;</td>
     <td> <span class="number-rm"><?php echo $i+1;?>. </span><?php echo $lain->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td colspan="3">Rp&nbsp;<input type="text" name="biaya_ird_lain[]" value="<?php echo $biaya_ird;?>" class="input-medium uang" placeholder="-" />
<input type="text" name="jumlah_rm[]" class="input-small jumlah" value="<?php echo $jumlah_tindakan;?>" placeholder="Jml Item"/>
  <input type="hidden" name="id_tindakan_lain[]" value="<?php echo $lain->id_tindakan;?>"/>

     </td>

   </tr>
   <?php
   $i++;
      }
   ?>

   </tbody>
      <thead>
      <tr>
       <th>&nbsp;</th>
       <th colspan="6"><strong>TOTAL</strong></th>

     </tr>
     </thead>
     <tbody >

       <tr >
          <td colspan="2"></td>
         <td><strong>IRD</strong></td>
         <td><div class="span6" style="text-align: right"><strong>Rp <?php echo number_format($total_ird,2,',','.');?></strong></div></td>
       </tr>
        <tr >
          <td colspan="2"></td>
         <td><strong>Sub Total</strong></td>
         <td><div class="span6" style="text-align: right"><strong>Rp <?php
         $subtotal = $total_ird;
         echo number_format($subtotal,2,',','.');
         ?></strong></div></td>
       </tr>
       <tr >
         <td colspan="2"></td>
         <td><strong>Paket 1</strong></td>
         <td><div class="span6" style="text-align: right">
         <?php
         echo $form->textField($rawatJalan,'biaya_paket_1',array('class' => 'input-small uang'));
         ?>
           </div>
         </td>
       </tr>
        <tr >
         <td colspan="2"></td>
         <td><strong>Paket 2</strong></td>
         <td><div class="span6" style="text-align: right">
          <?php
         echo $form->textField($rawatJalan,'biaya_paket_2',array('class' => 'input-small uang'));
         ?>
           </div>
         </td>
       </tr>
       <tr style="background-color: #25ade3;color:white;font-size: 16px">
          <td colspan="2"></td>
         <td><strong>TOTAL BIAYA</strong></td>
         <td ><div class="span6" style="text-align: right"><strong>Rp <?php
        $total_all = $subtotal + $rawatJalan->biaya_paket_1 + $rawatJalan->biaya_paket_2;
         echo number_format($total_all,2,',','.');

         ?></strong></div></td>
       </tr>
     </tbody>
    </table>

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
      <a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/cetakKwitansi',array('id'=>$rawatJalan->Id));?>" class="btn btn-warning" ><i class="icon-print"></i>&nbsp;CETAK</a>
    </div>
  </fieldset>
  <?php
  $this->endWidget();
  ?>


<?php
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');
  $cs->registerCssFile($baseUrl.'/vendors/jquery-ui.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-ui.min.js');

?>

<script>

$('#nama_dokter_perawat').focus();

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});



$('#tambah-dokter').on("click",function(){


  if($('span.number').length < 7)
  {
    var clone = $('#row-dokter').clone();
    $('#row-dokter').after(clone);

    var i = 1;
    $('span.number').each(function(){
      $(this).html(i+". ");
      i++;

    });
  }
});



$('#tambah-supp').on("click",function(){


  if($('span.number-supp').length < 10)
  {
    var clone = $('#row-supp').clone();
    $('#row-supp').after(clone);

    var i = 1;
    $('span.number-supp').each(function(){
      $(this).html(i+". ");
      i++;

    });
  }
});




$(document).bind("keyup.autocomplete",function(){


  $('.jenis-supp').autocomplete({
      minLength:1,
      select:function(event, ui){

           $(this).next().val(ui.item.id);
      },
      focus: function (event, ui) {
         $(this).next().val(ui.item.id);

      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetLayananPenunjang');?>",
                dataType: "json",
                data: {
                    term: request.term,

                },
                success: function (data) {
                    response(data);
                }
            })
        },

  });


   $('.nama_dokter_merawat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#TrPendaftaranRjalan_dokter_id').val(ui.item.id);

      },
      focus: function (event, ui) {
        $('#TrPendaftaranRjalan_dokter_id').val(ui.item.id);

      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,

                },
                success: function (data) {
                    response(data);
                }
            })
        },

  });


$('.nama_dokter_ird').autocomplete({
      minLength:1,
      select:function(event, ui){

        $('#TrRawatJalanRincian_dokter_ird').val(ui.item.id);

      },

      focus: function (event, ui) {
        $('#TrRawatJalanRincian_dokter_ird').val(ui.item.id);

      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,

                },
                success: function (data) {
                    response(data);
                }
            })
        },

  });


  $('.nama_dokter').autocomplete({
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);

      },
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);

      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,

                },
                success: function (data) {
                    response(data);
                }
            })
        },

  });

  $('.nama_jenis_visite').autocomplete({
      minLength:1,
      select:function(event, ui){

           $(this).next().val(ui.item.id);
      },
       focus: function (event, ui) {
        $(this).next().val(ui.item.id);

      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetJenisVisiteDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,

                },
                success: function (data) {
                    response(data);
                }
            })
        },

  });
});


$(document).on("click",".hapus-supp",function(){

  var ukuran = $(this).closest("#tbody-supp").children('.row-supp').size();

  if(ukuran>1){

      $(this).parent().parent().remove();

      var i = 1;
      $('span.number-supp').each(function(){
        $(this).html(i+". ");
        i++;

      });
      return false;
  }

});





function isiTindakan(ui)
{
   $("#TrRawatJalanLayanan_id_tindakan").val(ui.item.id);
   $("#TrRawatJalanLayanan_nama_tindakan").val(ui.item.value);


}

</script>
