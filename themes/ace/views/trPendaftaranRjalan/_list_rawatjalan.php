<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tr-pendaftaran-rjalan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'tr-pendaftaran-rjalan-grid',
	'itemsCssClass'=>'table table-hover table-striped table-bordered table-condensed',
	
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
		array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
		'NoDaftar',
		'NoMedrec',
		array(
			'value' => '$data->pASIEN->NAMA',
			'name' => 'Nama Pasien'
		),
		'GolPasien',
		'KodePoli',
		'KodeSubPlg',
		/*
		'KetPlg',
		'UrutanPoli',
		'KodeTdkLanjut',
		'CaraMasuk',
		'KetCaraMasuk',
		'StatusKunj',
		'KetTdkL1',
		'KetTdkL2',
		'KetTdkL3',
		'KetTdkL4',
		'KetTdkL5',
		'NoAntriPoli',
		'PostMRS',
		'WAKTU_DAFTAR',
		'created',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
