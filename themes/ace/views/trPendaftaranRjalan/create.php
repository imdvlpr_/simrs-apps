<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('BPendaftaranRjalan/index')),
                            array('name' => 'Pendaftaran')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
   
    .errorMessage{
      color: red;
    }

  
</style>
<div class="row">
        <!--/span-->
        <div class="col-xs-12" id="content">
          
            
          
           
            
                <!-- block -->
               
<h3>Pendaftaran Pasien Baru <?php echo $golpasien->NamaGol;?></h3>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/create',array('jenis'=>1001));?>" class="btn btn-primary">Pasien Baru UMUM</a>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/create',array('jenis'=>111));?>" class="btn btn-primary">Pasien Baru BPJS</a>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/create',array('jenis'=>2401));?>" class="btn btn-primary">Pasien Baru In-Health</a>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/create',array('jenis'=>1002));?>" class="btn btn-primary">Pasien Baru JAMKESDA</a>
<?php  
$this->renderPartial('_form_registrasi',[
  'model'=>$model,
  'pasien' => $pasien,
  'golpasien' => $golpasien,
  'rjalan' =>$rjalan,
  'jenis' => $_GET['jenis']
]);
?>

</div>
   </div>
  
