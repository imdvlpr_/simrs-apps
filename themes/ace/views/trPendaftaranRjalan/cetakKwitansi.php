  <?php 


$total_ird = 0;

?>   
 <table widtd="100%" style="padding: 2px 3px">
   <tr>
     <td>Nama Pasien</td>
     <td colspan="3">: <strong><?php echo $pasien->NAMA.' / '.$rawatJalan->gOLPASIEN->NamaGol;?></strong></td>
     
     <td>No Reg</td>
     <td colspan="2">: <?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk</td>
     <td colspan="3">: <?php echo $rawatJalan->tanggal_masuk;?></td>
     
     <td>Tgl Pulang</td>
     
     <td colspan="2">: <?php echo !empty($rawatJalan->tanggal_pulang) ? $rawatJalan->tanggal_pulang : '-' ; ?></td>
   </tr>
  
   <tr>
     <td>Alamat</td>

     <td colspan="3">: <?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Dokter yang merawat</td>
     <td colspan="2">: 
    <?php 
    $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatJalan->dokter)){
        $nama_dokter = $rawatJalan->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
     echo $nama_dokter;
     ?>

     </td>

   </tr>
 </table>


 <table style="padding:3px 4px">
   
   <tr>
     <td colspan="2" align="left">A. LAYANAN</td>
     <td colspan="2" align="left">RAWAT DARURAT</td>
    
     <td colspan="2" align="left">TARIF</td>
   </tr>
   
    <tr>
     <td colspan="2">1. Observasi</td>
     <td>&nbsp;</td>
     <td colspan="3" align="right">Rp&nbsp;<?php 
     echo Yii::app()->helper->formatRupiah($rawatRincian->obs_ird);
     $total_ird = $total_ird + $rawatRincian->obs_ird;
     ?></td>
     
     
   </tr>
   <tr>
     <td colspan="2">2. Pengawasan Dokter</td>
     <td>
      <?php echo $nama_dokter_ird;?>


     </td>
     <td colspan="3" align="right">Rp <?php 

     echo Yii::app()->helper->formatRupiah($rawatRincian->biaya_pengawasan);

     if(!empty($rawatRincian))
        $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;
     ?>
     </td>
    
   </tr>
  
   
   <tr>
     <td colspan="6">B. PELAYANAN MEDIK</td>
     
   </tr>
   

<?php 



    $urutan = 1;
      if(!empty($rawatJalan->trRawatJalanVisiteDokters)){

      foreach($rawatJalan->trRawatJalanVisiteDokters as $visite)
      {
       
        if(!empty($visite))
          $total_ird = $total_ird + $visite->biaya_visite * $visite->jumlah_visite_ird;

       



        $dokterVisiteIRD = '';
        if(!empty($visite->dokterIrd))
            $dokterVisiteIRD = $visite->dokterIrd->FULLNAME;


    ?>
    <tr>
     <td  colspan="2">
     <span class="number"><?php echo $urutan++;?>. </span>
    <?php echo $visite->jenisVisite->nama_visite;?>
     </td>
     <td>
     <?php echo $dokterVisiteIRD;?>
    </td>
     <td colspan="3" align="right">
     <?php echo $visite->jumlah_visite_ird;?> x Rp&nbsp;<?php echo Yii::app()->helper->formatRupiah($visite->biaya_visite);?> 

     </td>
     
     
   </tr>
   <?php 
      }
    }

   ?>
 
  
   
  <tr>
     <td  colspan="6"><strong>I. Tindakan Medik Non-Operasi</strong></td>
     
   </tr>
<?php 

   $listTnop = TindakanMedisNonOperatif::model()->findAll();
   $i = 0;

   foreach($listTnop as $tnop){
      $attr = array(
        'id_rawat_jalan' => $rawatJalan->Id,
        'id_tnop' => $tnop->id_tindakan
      );
      $itemTnop = TrRawatJalanTnop::model()->findByAttributes($attr);


      $jumlah_tindakan = 0;
      $biaya_ird = 0;

      $biaya_ird_tnop = 0;
      if(!empty($itemTnop)){
        $jumlah_tindakan = $itemTnop->jumlah_tindakan;

        $biaya_ird = $itemTnop->biaya_ird;
        $biaya_ird_tnop = $biaya_ird_tnop + $biaya_ird * $jumlah_tindakan;

      }

      if($biaya_ird == 0) continue;

      $total_ird = $total_ird + $biaya_ird_tnop;
        
    ?>
   <tr>
     <td colspan="2"><span class="number-tnop"><?php echo strtolower(chr(64 + ($i+1)));?>. </span><?php echo $tnop->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td colspan="3" align="right">
     <?php echo $jumlah_tindakan;?> x Rp&nbsp;<?php echo Yii::app()->helper->formatRupiah($biaya_ird);?>
     </td>
   </tr>
     <?php
 $i++;
    }

    ?>
   
   
   
   <tr>
     <td colspan="6">C. LAYANAN PENUNJANG</td>
     
   </tr>
   
<?php 



    $urutan = 1;
    $biaya_ird_supp = 0;
      foreach($rawatJalan->trRawatJalanPenunjangs as $supp)
      {
          $biaya_ird_supp = $biaya_ird_supp + $supp->biaya_ird * $supp->jumlah_tindakan;
          $total_ird = $total_ird + $biaya_ird_supp;

    ?>
     <tr>
     <td colspan="2">
       <span class="number-supp"><?php echo $urutan++;?>. </span>
       <?php echo $supp->penunjang->nama_tindakan;?>
        
     </td>
     <td>&nbsp;</td>
     <td colspan="3" align="right">
     <?php echo $supp->jumlah_tindakan;?> x Rp&nbsp;<?php echo Yii::app()->helper->formatRupiah($supp->biaya_ird);?>
</td>
   </tr>
    <?php 
    
    }

    ?>

   <tr>
     <td colspan="6">D. <strong>OBAT dan ALAT KESEHATAN</strong></td>
     
   </tr>
<?php 
   $listObat = ObatAlkes::model()->findAll();
   $i = 0;
   foreach($listObat as $obat){
      $attr = array(
        'id_rawat_jalan' => $rawatJalan->Id,
        'id_alkes' => $obat->id_obat_alkes
      );
      $itemObat = TrRawatJalanAlkes::model()->findByAttributes($attr);

      $jumlah_tindakan = 0;
      $biaya_ird = 0;

      $biaya_obat_ird_total = 0;

      if(!empty($itemObat)){
        $jumlah_tindakan = $itemObat->jumlah_tindakan;

        $biaya_ird = $itemObat->biaya_ird;
        $biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird * $jumlah_tindakan;

      }

      if($biaya_obat_ird_total == 0) continue;

      $total_ird = $total_ird + $biaya_obat_ird_total;
        
    ?>
    <tr>
     <td colspan="2"><span><?php echo ($i+1);?>. </span><?php echo $obat->nama_obat_alkes;?></td>
     <td></td>
     <td colspan="3" align="right">
<?php echo $jumlah_tindakan;?> x Rp <?php echo Yii::app()->helper->formatRupiah($biaya_ird); ?>

     </td>
    
   
   </tr>
   <?php 
   $i++;
   }

   ?>
   
  
   <tr>
     <td colspan="6"><strong>E. Lain-lain</strong></td>
     
     
   </tr>
<?php 

   $listLain = TindakanMedisLain::model()->findAll();
   $i = 0;
   foreach($listLain as $lain){
       $attr = array(
        'id_rawat_jalan' => $rawatJalan->Id,
        'id_lain' => $lain->id_tindakan
      );

      $itemLain = TrRawatJalanLain::model()->findByAttributes($attr);
      $jumlah_tindakan = 0;
      
      $biaya_ird = 0;
      $biaya_ird_lain = 0;
      if(!empty($itemLain)){
        $jumlah_tindakan = $itemLain->jumlah_tindakan;
      
        $biaya_ird = $itemLain->biaya_ird;
        $biaya_ird_lain = $biaya_ird_lain + $biaya_ird * $jumlah_tindakan;
      }

      if($biaya_ird == 0) continue;

      $total_ird = $total_ird + $biaya_ird_lain;
    ?>
    <tr>
     <td colspan="2"> <span class="number-rm"><?php echo $i+1;?>. </span><?php echo $lain->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td colspan="3" align="right"><?php echo $jumlah_tindakan;?> x Rp&nbsp;<?php echo Yii::app()->helper->formatRupiah($biaya_ird);?>

     
     </td>
     
   </tr>
   <?php 
   $i++;
      }
   ?>
      <tr>
       <td colspan="6"><strong>TOTAL</strong></td>
       
     </tr>
     
      
       <tr >
          <td colspan="2"></td>
         <td><strong>IRD</strong></td>
         <td colspan="3" align="right"><strong>Rp <?php echo number_format($total_ird,2,',','.');?></strong>
         </td>
       </tr>
        <tr >
          <td colspan="2"></td>
         <td><strong>Sub Total</strong></td>
         <td colspan="3" align="right"><strong>Rp <?php 
         $subtotal = $total_ird;
         echo number_format($subtotal,2,',','.');
         ?></strong></td>
       </tr>
       <tr >
         <td colspan="2"></td>
         <td><strong>Paket 1</strong></td>
         <td colspan="3" align="right">
        Rp <?php 
         echo Yii::app()->helper->formatRupiah($rawatJalan->biaya_paket_1);
         ?>
           
         </td>
       </tr>
        <tr >
         <td colspan="2"></td>
         <td><strong>Paket 2</strong></td>
         <td colspan="3" align="right">
          Rp <?php 
         echo Yii::app()->helper->formatRupiah($rawatJalan->biaya_paket_2);
         ?>

         </td>
       </tr>
       <tr >
          <td colspan="2"></td>
         <td><strong>TOTAL BIAYA</strong></td>
         <td colspan="3" align="right"><strong>Rp <?php 
        $total_all = $subtotal + $rawatJalan->biaya_paket_1 + $rawatJalan->biaya_paket_2;
         echo number_format($total_all,2,',','.');

         ?></strong></td>
       </tr>
     </tbody>
    </table>
