<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('BPendaftaranRjalan/index')),
                            array('name' => 'Pendaftaran')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
   

    .errorMessage{
      color: red;
    }

   
</style>
<div class="row">
        <!--/span-->
        <div class="col-xs-12" id="content">
          
            
          
           
            
                <!-- block -->
               
<h3>Pendaftaran Pasien Lama <?php echo $golpasien->NamaGol;?></h3>
<p>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/cariPasien',array('jenis'=>1001));?>" class="btn btn-primary">Pasien UMUM</a>
<a class="btn btn-primary">Pasien BPJS</a>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/cariPasien',array('jenis'=>2401));?>" class="btn btn-primary">Pasien In-Health</a>
<a href="<?php echo Yii::app()->createUrl('trPendaftaranRjalan/cariPasien',array('jenis'=>1002));?>" class="btn btn-primary">Pasien JAMKESDA</a>
</p>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'registrasi-pasien-lama-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); 

?>                           <!-- block -->



<?php  
$this->renderPartial('_form_registrasi',[
  'model'=>$model,
  'pasien' => $pasien,
  'golpasien' => $golpasien,
  'rjalan' =>$rjalan,
  'jenis' => $_GET['jenis']
]);
?>


  <?php $this->endWidget();?>

</div>
   </div>
        </div>
