<?php
/* @var $this TrackingRmController */
/* @var $model TrackingRm */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'b_pendaftaran_id'); ?>
		<?php echo $form->textField($model,'b_pendaftaran_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status_rm_id'); ?>
		<?php echo $form->textField($model,'status_rm_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit_penerima'); ?>
		<?php echo $form->textField($model,'unit_penerima',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'keterangan'); ?>
		<?php echo $form->textField($model,'keterangan',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'durasi'); ?>
		<?php echo $form->textField($model,'durasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'durasi_label'); ?>
		<?php echo $form->textField($model,'durasi_label',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_at'); ?>
		<?php echo $form->textField($model,'updated_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->