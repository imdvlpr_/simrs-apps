<?php
/* @var $this TrackingRmController */
/* @var $data TrackingRm */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('b_pendaftaran_id')); ?>:</b>
	<?php echo CHtml::encode($data->b_pendaftaran_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_rm_id')); ?>:</b>
	<?php echo CHtml::encode($data->status_rm_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit_penerima')); ?>:</b>
	<?php echo CHtml::encode($data->unit_penerima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('durasi')); ?>:</b>
	<?php echo CHtml::encode($data->durasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('durasi_label')); ?>:</b>
	<?php echo CHtml::encode($data->durasi_label); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	*/ ?>

</div>