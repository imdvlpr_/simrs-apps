<?php
use yii\helpers\Url;
use yii\helpers\Html;
$fontfamily = 'Tahoma';
$fontSize = '8px';
$fontSizeBawah = '10px';
?>
<table width="100%" style="height: 1px;margin: 0px">
    <tr>
        <td width="10%"></td>
        <td width="80%" style="text-align: center">
            <strong style="font-size: <?=$fontSize;?>;font-family: <?=$fontfamily;?>">RSUD KABUPATEN KEDIRI</strong><br>
            <span style="font-size:<?=$fontSize;?>;font-family: <?=$fontfamily;?>">Jl. PAHLAWAN KUSUMA BANGSA NO 1 TLP (0354) 391718, 391169, 394956 FAX. 391833<BR>
            PARE KEDIRI (64213) email : rsud.pare@kedirikab.go.id</span>
        </td>
        <td width="10%"></td>
    </tr>
</table>
<hr style="height: 1px;margin: 0px">
<div style="text-align: center;margin: 0px;font-size:<?=$fontSize;?>;font-family: <?=$fontfamily;?>">NO KWITANSI : <?=Yii::app()->helper->appendZeros($model->id,8);?></div>
<table width="100%">
    <tr>
        <td width="55%" valign="top" style="border:1px solid black;" >
            

<table style="font-size: <?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
    
     <tr>
        <td width="28%">No RM</td>
        <td width="3%">:</td>
        <td style="width: 250px"><?=$pasien->NoMedrec;?></td>
    </tr>
     <tr>
        <td >Jenis Px</td>
        <td>:</td>
        <td><?=$reg->kodeGol->NamaGol;?></td>
    </tr>
    <tr>
        <td >Nama Px</td>
        <td>:</td>
        <td><?=$pasien->NAMA;?></td>
    </tr>

    <tr>
        <td >Alamat</td>
        <td>:</td>
        <td><?=!empty($pasien) ? $pasien->ALAMAT : '-';?></td>
    </tr>
   
    <tr>
        <td >No Trx</td>
        <td>:</td>
        <td><?=$model->kode_trx;?></td>
    </tr>
    <tr>
        <td >Tgl</td>
        <td>:</td>
        <td><?=date('d/m/Y',strtotime($model->trRawatJalan->tanggal));?></td>
    </tr>
    <tr>
        <td >Dokter</td>
        <td>:</td>
        <td><?=$model->dokter->FULLNAME;?></td>
    </tr>
    <tr>
        <td >Untuk Pembayaran</td>
        <td>:</td>
        <td><?=$model->dmTindakan->dmTindakan->nama;?></td>
    </tr>
</table>
        </td>
        <td width="45%"  valign="top" style="border:1px solid black;">
    <table style="font-size: <?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
    
    
     <tr>
        <td width="20%">Nominal</td>
        <td width="3%">:</td>
        <td width="67%" style="font-weight: bold">Rp <?php
         $total = $model->nilai;
        $total = ceil($total/100) * 100;
        echo  Yii::app()->helper->formatRupiah($total);
        
        ?></td>
    </tr>
    <tr>
        <td >Terbilang</td>
        <td >:</td>
        <td ><?=ucwords(Yii::app()->helper->terbilang($total));?></td>
    </tr>
</table>
<br>
<table width="100%">
    <tr>
        <td width="50%" style="text-align: center;font-size:<?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
            <br><br>Penyetor
           
            <br>
            <br>
            <br>
            <u><b>(........................)</b></u><br>
        </td>
        <td width="50%" style="text-align: center;font-size:<?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
            
            Pare, <?=date('d M Y');?>
            <br>

            Petugas Poli
           
            <br>
            <br>
            <br>
            <u><b>(...........................)</b></u><br>
            
            
        </td>
    </tr>
</table>
</td>
        
    </tr>
</table>

<table width="100%" >
    <tr>
        <td width="55%" valign="top">
        </td>
        <td width="45%" valign="center" style="text-align: center;font-size: 8px">
          <h1 style="border:1px solid black;">  <?=$model->trRawatJalan->unit->NamaUnit;?></h1>
        </td>
    </tr>
</table>