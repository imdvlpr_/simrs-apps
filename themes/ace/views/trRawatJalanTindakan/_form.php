<?php
/* @var $this TrRawatJalanTindakanController */
/* @var $model TrRawatJalanTindakan */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tr-rawat-jalan-tindakan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
));

 $nama_dokter = !empty($model->dokter) ? $model->dokter->FULLNAME : '';



$pasien = $model->trRawatJalan->noMedrec;
$reg = $model->trRawatJalan->reg;

?>
<h1>Manage Tindakan Rawat Jalan</h1>
<div class="row">
<div class="col-lg-12">
	
	<div class="profile-user-info profile-user-info-striped ">
		<div class="profile-info-row">
			<div class="profile-info-name"> Nama / Reg</div>

			<div class="profile-info-value">
				<span class="editable" id="username"><h4>
<?php echo $pasien->NAMA.' / '.$reg->kodeGol->NamaGol;?> / <?php echo $pasien->NoMedrec;?>
			
					</h4></span>
			</div>
		</div>

		
		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl Masuk </div>

			<div class="profile-info-value">
<?= $reg->TGLDAFTAR;?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name"> Jam Masuk </div>

			<div class="profile-info-value">
<?= $reg->JamDaftar;?>
			</div>
		</div>
	

		<div class="profile-info-row">
			<div class="profile-info-name"> Alamat </div>

			<div class="profile-info-value">
				<?= $pasien->FULLADDRESS;?>
			</div>
		</div>

	</div>
</div>
 </div>
 <div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box widget-color-red">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Item Tindakan Poli</h4>
        </div>
        <div class="widget-body">
          <div class="widget-main">
   			<?php $index = 0;?>
			<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Item <span class="numbering"><?=$index+1;?></span></label>
				<div class="col-sm-10">
				<?=CHtml::dropDownList('dm_tindakan_id[]','',CHtml::listData($listTindakan,'id',function($model){
			return $model->dmTindakan->kode.' - '.$model->dmTindakan->nama;
		}),['empty' => 'Pilih Tindakan','class'=>'items']); ?>
				<label class="error_item"></label>
				</div>
			</div>
          </div>
      </div>
    </div>
</div>
</div>

	<?php echo $form->hiddenField($model,'tr_rawat_jalan_id'); ?>



	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#TrRawatJalanTindakan_dm_tindakan_id').change(function(){
			var item = new Object;
			item.poli_tindakan_id = $(this).val();
			$.ajax({
				type : 'post',
				url : '<?=Yii::app()->createUrl('ajaxRequest/getItemTindakanPoli');?>',

				data : {dataItem:item},
				beforeSend : function(){
				  $('#loading').show();
				},
				success : function(res){
				  var res = $.parseJSON(res);
				  var item = res.result;
				  $('#loading').hide();
				  $('#TrRawatJalanTindakan_akhp').val(item.akhp);
				  $('#TrRawatJalanTindakan_jrs').val(item.jrs);
				  $('#TrRawatJalanTindakan_jaspel').val(item.jaspel);
				  $('#TrRawatJalanTindakan_nilai').val(item.nilai);
				  
				  
				},
			});
		});
	});


function refreshNumbering(){
	

	$('span.numbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});

}


$(document).on('change','select.items', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	// if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">Item <span class="numbering"></span></label>';
		row += '<div class="col-sm-6">';
		row += '<select class="items" name="dm_tindakan_id[]">';
		row += '<option value="">Pilih Tindakan</option>';
		<?php 
		foreach($listTindakan as $t)
		{
		?>
		row += '<option value="<?=$t->id;?>"><?=$t->dmTindakan->kode;?> - <?=$t->dmTindakan->nama;?></option>';
		<?php
		}
		?>
		row += '</select>';
		row += '<label class="error_item"></label>&nbsp;<a href="javascript:void(0)" class="btn btn-danger btn-sm remove"><i class="fa fa-trash"></i>&nbsp;Remove</a>';
		row += '</div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.item').last().focus();
	// }
});

$(document).on('click','a.remove',function(e){
	e.preventDefault();
	
	$(this).parent().parent().remove();
	refreshNumbering();
});

$(document).bind("keyup.autocomplete",function(){

	$('.item').autocomplete({
      	minLength:1,
      	select:function(event, ui){
       
        	$(this).next().val(ui.item.id);
                
      	},
      
      	focus: function (event, ui) {
        	$(this).next().val(ui.item.id);
       
      	},
      	source:function(request, response) {
        	$.ajax({
                url: "<?php echo Yii::app()->createUrl('DmLabItem/ajaxGetItem');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
  	}); 
});

</script>