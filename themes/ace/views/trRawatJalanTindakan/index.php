 <?php
$this->breadcrumbs=array(
	array('name'=>'Rawat Jalan','url'=>array('/trRawatJalan/index')),
	array('name'=>'Tindakan','url'=>array('index','id'=>$model->tr_rawat_jalan_id)),
	array('name'=>'Manage'),
);

$pasien = $model->trRawatJalan->noMedrec;
$reg = $model->trRawatJalan->reg;

?>
<h1>Manage Tindakan Rawat Jalan</h1>
<div class="row">
<div class="col-lg-12">
	
	<div class="profile-user-info profile-user-info-striped ">
		<div class="profile-info-row">
			<div class="profile-info-name"> Nama / Reg</div>

			<div class="profile-info-value">
				<span class="editable" id="username"><h4>
<?php echo $pasien->NAMA.' / '.$reg->kodeGol->NamaGol;?> / <?php echo $pasien->NoMedrec;?>
			
					</h4></span>
			</div>
		</div>

		
		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl Masuk </div>

			<div class="profile-info-value">
<?= $reg->TGLDAFTAR;?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name"> Jam Masuk </div>

			<div class="profile-info-value">
<?= $reg->JamDaftar;?>
			</div>
		</div>
	

		<div class="profile-info-row">
			<div class="profile-info-name"> Alamat </div>

			<div class="profile-info-value">
				<?= $pasien->FULLADDRESS;?>
			</div>
		</div>

	</div>
</div>
 </div>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                        <div class="pull-left"> <?php	echo CHtml::link('Tambah Tindakan',array('TrRawatJalanTindakan/create','id'=>$model->tr_rawat_jalan_id),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('TrRawatJalanTindakan[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('TrRawatJalanTindakan[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'tr-rawat-jalan-tindakan-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		'kode_trx',
		'trRawatJalan.unit.NamaUnit',
		'dmTindakan.dmTindakan.nama',
		'dokter.FULLNAME',
		'nilai',
		[
	      'type'=>'raw',
	      'header' => 'Actions',
	      'value' => function($data){
	      	$st = '';
	      	$lb = '';

	      	switch ($data->status_bayar) {
	      		case 1:
	      			$st = 'success';
	      			$lb = 'LUNAS';
	      			break;
	      		case 2 :
	      			$st = 'warning';
	      			$lb = 'CICILAN';
	      		break;
	      		default:
	      			$st = 'danger';
	      			$lb = 'BELUM LUNAS';
	      			break;
	      	}
	        $html = '<button class="btn btn-sm btn-'.$st.'">'.$lb.'</button>';

			return $html;
	      }
	    ],
		[
      'type'=>'raw',
      'header' => 'Actions',
      'value' => function($data){

        $html = '<div class="btn-group">
  <button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle">
    Tindakan
    <span class="ace-icon fa fa-caret-down icon-on-right"></span>
  </button>

  <ul class="dropdown-menu dropdown-info dropdown-menu-right">
    ';
     
    if($data->status_bayar == 0){
    $html .= '<li>
      <a href="'.Yii::app()->createUrl("trRawatJalanTindakan/update/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-pencil bigger-120"></i> Update</a>
    </li><li class="divider"></li>';

    $html .= ' <li>
      <a class="print-pengantar" href="'.Yii::app()->createUrl("trRawatJalanTindakan/printPengantar/", ["id"=>$data->id]).'"><i class="ace-icon fa fa-print bigger-120"></i>Print Pengantar</a></li> ';
    }
    else if($data->status_bayar == 1){
    	$html .= ' <li>
      <a href="'.Yii::app()->createUrl("trRawatJalanTindakan/printBukti/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-print bigger-120"></i> Print Bukti</a></li> ';
    }
    $html .= '
  </ul>
</div>';

return $html;
      }
    ],
		/*
		'created_at',
		'updated_at',
		*/
		
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#search,#size').change(function(){
		$('#tr-rawat-jalan-tindakan-grid').yiiGridView.update('tr-rawat-jalan-tindakan-grid', {
		    url:'<?php echo Yii::app()->createUrl("TrRawatJalanTindakan/index"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
		});
	});
	
	$('a.print-pengantar').click(function(e){
		e.preventDefault();
		let url = $(this).attr('href');

		popitup(url,'Pengantar',0);
	});
});	

function popitup(url,label,pos) {
    var w = screen.width * 0.8;
    var h = 800;
    var left = pos == 1 ? screen.width - w : 0;
    var top = pos == 1 ? screen.height - h : 0;
    
    window.open(url,label,'height='+h+',width='+w+',top='+top+',left='+left);
    
}
</script>