 <?php
$this->breadcrumbs=array(
	array('name'=>'Rawat Jalan Tindakan','url'=>['index','id'=>$model->tr_rawat_jalan_id]),
	array('name'=>'Create'),
);

?>

<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>
<div class="row">
	<div class="col-xs-12">
<?php $this->renderPartial('_form', [
	'model'=>$model,
	'listTindakan' => $listTindakan,
	'listDokter' => $listDokter
]); ?>
	</div>
</div>
