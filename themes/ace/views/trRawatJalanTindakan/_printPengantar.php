<?php
use yii\helpers\Url;
use yii\helpers\Html;
$fontfamily = 'Tahoma';
$fontSize = '8px';
$fontSizeBawah = '8px';
?>
<table width="100%" style="height: 1px;margin: 0px">
    <tr>
        <td width="10%"></td>
        <td width="80%" style="text-align: center">
            <strong style="font-size: 8px;font-family: <?=$fontfamily;?>">RSUD KABUPATEN KEDIRI</strong><br>
            <span style="font-size:8px;font-family: <?=$fontfamily;?>">Jl. PAHLAWAN KUSUMA BANGSA NO 1 TLP (0354) 391718 FAX. 391833<BR>
            PARE KEDIRI (64213) email : rsud.pare@kedirikab.go.id</span>
        </td>
        <td width="10%"></td>
    </tr>
</table>
<hr style="height: 1px;margin: 0px">
<div style="text-align: center;margin: 0px;font-size:8px;font-family: <?=$fontfamily;?>">SURAT PENGANTAR</div>
<table style="margin-bottom: 2px;font-size: <?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
    <tr>
        <td style="width: 20%">No Trx</td>
        <td  style="width: 10%">:</td>
        <td style="width: 70%"><?=$model->kode_trx;?></td>
    </tr>
    <tr>
        <td >Tgl</td>
        <td>:</td>
        <td><?=date('d-m-Y',strtotime($model->trRawatJalan->tanggal));?></td>
    </tr>

    <tr>
        <td >Tgl Cetak</td>
        <td>:</td>
        <td><?=date('d-m-Y');?></td>
    </tr>
     
</table>
<table style="font-size: <?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
    
     <tr>
        <td style="width: 20%" >No RM</td>
        <td style="width: 10%">:</td>
        <td style="width: 70%"><?=$pasien->NoMedrec;?></td>
    </tr>
     <tr>
        <td >Jenis Px</td>
        <td>:</td>
        <td><?=$reg->kodeGol->NamaGol;?></td>
    </tr>
    <tr>
        <td >Nama Px</td>
        <td>:</td>
        <td><?=$pasien->NAMA;?></td>
    </tr>
    <tr>
        <td >Unit</td>
        <td>:</td>
        <td><?=$model->trRawatJalan->unit->NamaUnit;?></td>
    </tr>
    <tr>
        <td >Dokter</td>
        <td>:</td>
        <td><?=$model->dokter->FULLNAME;?></td>
    </tr>
    <tr>
        <td >Nominal</td>
        <td>:</td>
        <td style="font-weight: bold">Rp <?=Yii::app()->helper->formatRupiah($model->nilai,2);?></td>
    </tr>
</table>
<table width="100%" >
      <tr>
        <td style="text-align: center">
             <barcode code="<?=$pasien->NoMedrec;?>" type="C128A" size="1.5" height="0.5"/>
        </td>
        
    </tr>
</table>
<table width="100%">
    <tr>
        
        <td width="100%" style="text-align: center;font-size:8px;font-family: <?=$fontfamily;?>">
            <br><br>
            Pare, <?=date('d-m-Y');?>
            <br>

            Petugas Poli
           
            <br>
            <br>
            <u><b>(........................)</b></u>
            <br>
            
            
        </td>
    </tr>
</table>
