<?php
/* @var $this BPendaftaranCodingController */
/* @var $model BPendaftaranCoding */

$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Coding','url'=>array('admin')),
	array('name'=>'Bpendaftaran Coding'),
);

$this->menu=array(
	array('label'=>'List BPendaftaranCoding', 'url'=>array('index')),
	array('label'=>'Create BPendaftaranCoding', 'url'=>array('create')),
	array('label'=>'Update BPendaftaranCoding', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BPendaftaranCoding', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BPendaftaranCoding', 'url'=>array('admin')),
);
?>

<h1>View BPendaftaranCoding #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'reg_id',
		'kode_icd',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
