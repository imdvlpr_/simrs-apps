<?php
/* @var $this DmDokterController */
/* @var $data DmDokter */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_dokter')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_dokter), array('view', 'id'=>$data->id_dokter)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_dokter')); ?>:</b>
	<?php echo CHtml::encode($data->nama_dokter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_dokter')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_dokter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prosentasi_jasa')); ?>:</b>
	<?php echo CHtml::encode($data->prosentasi_jasa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_praktik')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_praktik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telp')); ?>:</b>
	<?php echo CHtml::encode($data->telp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_panggilan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_panggilan); ?>
	<br />

	*/ ?>

</div>