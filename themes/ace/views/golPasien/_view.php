<?php
/* @var $this GolPasienController */
/* @var $data GolPasien */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeGol')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->KodeGol), array('view', 'id'=>$data->KodeGol)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NamaGol')); ?>:</b>
	<?php echo CHtml::encode($data->NamaGol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('a_kpid')); ?>:</b>
	<?php echo CHtml::encode($data->a_kpid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Inisial')); ?>:</b>
	<?php echo CHtml::encode($data->Inisial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeKlsHak')); ?>:</b>
	<?php echo CHtml::encode($data->KodeKlsHak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JenisKlsHak')); ?>:</b>
	<?php echo CHtml::encode($data->JenisKlsHak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeAturan')); ?>:</b>
	<?php echo CHtml::encode($data->KodeAturan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('NoAwal')); ?>:</b>
	<?php echo CHtml::encode($data->NoAwal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IsPBI')); ?>:</b>
	<?php echo CHtml::encode($data->IsPBI); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MblAmbGratis')); ?>:</b>
	<?php echo CHtml::encode($data->MblAmbGratis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MblJnhGratis')); ?>:</b>
	<?php echo CHtml::encode($data->MblJnhGratis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KDJNSKPST')); ?>:</b>
	<?php echo CHtml::encode($data->KDJNSKPST); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KDJNSPESERTA')); ?>:</b>
	<?php echo CHtml::encode($data->KDJNSPESERTA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IsKaryawan')); ?>:</b>
	<?php echo CHtml::encode($data->IsKaryawan); ?>
	<br />

	*/ ?>

</div>