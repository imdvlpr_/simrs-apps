<?php
/* @var $this GolPasienController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Gol Pasiens'),
);

$this->menu=array(
	array('label'=>'Create GolPasien', 'url'=>array('create')),
	array('label'=>'Manage GolPasien', 'url'=>array('admin')),
);
?>

<h1>Gol Pasiens</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
