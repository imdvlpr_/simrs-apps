  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model);

?>          
    <legend>
      

    </legend>
    
    <div class="control-group">
    <?php echo $form->labelEx($model,'event_id',array('class'=>'control-label'));?>
      <div class="controls">
        <?php 

          $listdata = DmKalenderEvents::model()->findAll();
          echo $form->dropDownList($model,'event_id',CHtml::listdata($listdata,'id','nama_event'),array('class'=>'input-large'));

          
        ?>
       
        <?php echo $form->error($model,'event_id'); ?>
      </div>
    </div>
   
    <div class="control-group">
    <?php echo $form->labelEx($model,'title',array('class'=>'control-label'));?>
      <div class="controls">
        <?php 
          echo $form->textField($model,'title',array('class'=>'input-large'));

          
        ?>
       
        <?php echo $form->error($model,'title'); ?>
      </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model,'startdate',array('class'=>'control-label'));?>
        <div class="controls">
         <?php 

         $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
              'model'=>$model,
              'attribute'=>'startdate',
              'options'=>array(
                 'showAnim'=>'slide',
                  'showSecond'=>true,
                  'timeFormat' => 'hh:mm:ss',
                  'dateFormat'=>'yy-mm-dd',
                  'changeMonth' => true,
                  'changeYear' => true,
              ),
  ));

        echo $form->error($model,'startdate');
        ?>
          
          </div>
        </div>
     
    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan Baru' : 'Save',array('class'=>'btn btn-success','name'=>'saveonly')); ?>
      <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan Baru dan Input Lagi' : 'Save',array('class'=>'btn btn-success','name'=>'saverepeat')); ?>
    </div>
  </fieldset>
  <?php $this->endWidget();?>