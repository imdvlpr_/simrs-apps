<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';


  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="page-header">
  <h1>Formulir Data Pasien</h1>
</div>
<style>
  .errorMessage, .errorSummary{
    color:red;
  }

</style>

<div class="row-fluid">
    <div class="span3" id="sidebar">
      <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                <li>
					<a href="<?php echo Yii::app()->createUrl('DmKalenderEvents');?>"><i class="icon-chevron-right"></i> Event</a>
				</li>
				<li>
					<a href="<?php echo Yii::app()->createUrl('DmKalender');?>"><i class="icon-chevron-right"></i> Kalender</a>
				</li>
                       
                    </ul>
    </div>
    <div id="content" class="span9">
    	 <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Kalender','url'=>Yii::app()->createUrl('DmKalender')),
                            array('name' => 'Update')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
      <div class="row-fluid">
        <div class="block">
          <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Kalender</div>
        </div>
         <div class="block-content collapse in">
            <div class="span12">
              <?php $this->renderPartial('_form',array('model'=> $model));?>
            </div>
          </div>

        </div>
      </div>
    </div>
</div>


    <!--
}
=====================================================================================================================
==============================================form kedua========================================================
=====================================================================================================================
=====================================================================================================================

    -->
           
             

<?php 
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
?>
