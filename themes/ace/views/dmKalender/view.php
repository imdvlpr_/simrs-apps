<?php
/* @var $this DmKalenderController */
/* @var $model DmKalender */

$this->breadcrumbs=array(
	'Dm Kalenders'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List DmKalender', 'url'=>array('index')),
	array('label'=>'Create DmKalender', 'url'=>array('create')),
	array('label'=>'Update DmKalender', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmKalender', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmKalender', 'url'=>array('admin')),
);
?>

<h1>View DmKalender #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'startdate',
		'enddate',
		'allday',
		'created',
	),
)); ?>
