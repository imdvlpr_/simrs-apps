<?php
/* @var $this DmKalenderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dm Kalenders',
);

$this->menu=array(
	array('label'=>'Create DmKalender', 'url'=>array('create')),
	array('label'=>'Manage DmKalender', 'url'=>array('admin')),
);
?>

<h1>Dm Kalenders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
