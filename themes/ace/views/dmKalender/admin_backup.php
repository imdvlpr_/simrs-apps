<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);

?>

<?php 
 $baseUrl = Yii::app()->theme->baseUrl; 
      $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
    $cs->registerScriptFile($baseUrl.'/vendors/fullcalendar/fullcalendar.js');
    $cs->registerScriptFile($baseUrl.'/vendors/fullcalendar/gcal.js');
	$cs->registerScriptFile($baseUrl.'/vendors/jquery-ui-1.10.3.js');

	 $cs->registerCssFile($baseUrl.'/vendors/fullcalendar/fullcalendar.css');
?>
<style>
        #external-events {
            float: left;
            width: 110px;
            padding: 0 10px;
            border: 1px solid #ccc;
            background: #eee;
            text-align: left;
            }
            
        #external-events h4 {
            font-size: 16px;
            margin-top: 0;
            padding-top: 1em;
            }
            
        .external-event { /* try to mimick the look of a real event */
            margin: 10px 0;
            padding: 2px 4px;
            background: #3366CC;
            color: #fff;
            font-size: .85em;
            cursor: pointer;
            z-index: 99999999;
            }
            
        #external-events p {
            margin: 1.5em 0;
            font-size: 11px;
            color: #666;
            }
            
        #external-events p input {
            margin: 0;
            vertical-align: middle;
            }

        </style>
<script type="text/javascript">

	$(document).ready(function(){

		$.ajax({
			url: '<?php echo Yii::app()->createUrl("ajaxRequest/GetAllCalendarEvents");?>',
	        type: 'POST', // Send post data
	        data: 'type=fetch',
	        async: false,
	        success: function(s){
	        	json_events = s;
	        }
		});

		$('#calendar').fullCalendar({
			events: JSON.parse(json_events),
			//events: [{"id":"14","title":"New Event","start":"2015-01-24T16:00:00+04:00","allDay":false}],
			utc: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: ''
			},
			selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
            	$('#myModal').modal();	
            	$('#event_id').val('');
            	$('#title').val('');
            	
            	var df = $.fullCalendar.formatDate(start,'yyyy-MM-dd HH:mm:ss');
            	$('#startdate').val(df);	

                $(this).fullCalendar('unselect');
            },
			editable: true,
			// droppable: true, 

			
			eventClick: function(event, jsEvent, view) {
		    	  
		          var teks = confirm('Hapus Event Ini?:');
		          if (teks){
		          	$.ajax({
						url: '<?php echo Yii::app()->createUrl("ajaxRequest/GetAllCalendarEvents");?>',
				        type: 'POST', // Send post data
				        data: 'type=delete&id='+event.id,
				        async: false,
				        success: function(s){
				        	$('#calendar').fullCalendar('removeEvents');
				        	getFreshEvents();
							
				        }
					});
		          }
			},
		
		});

		$('#external-events div.external-event').each(function() {
        
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999999999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
            
        });

		function getFreshEvents(){
			$.ajax({
				url: '<?php echo Yii::app()->createUrl("ajaxRequest/GetAllCalendarEvents");?>',
		        type: 'POST', // Send post data
		        data: 'type=fetch',
		        async: false,
		        success: function(s){
		        	fresh_events = s;
		        }
			});
			$('#calendar').fullCalendar('addEventSource', JSON.parse(fresh_events));
		}


		$('#confirm-send').click(function(){

			$.ajax({
				url: '<?php echo Yii::app()->createUrl("ajaxRequest/GetAllCalendarEvents");?>',
		        type: 'POST', // Send post data
		        data: 'type=new&id='+$('#event_id').val()+'&ket='+$('#title').val()+'&startdate='+$('#startdate').val(),
		        async: false,
		        beforeSend: function(){
		        	$('#loading').show();
		        },
		        success: function(s){
		        	$('#loading').hide();
		        	$('#myModal').modal().hide();
		        	$('#calendar').fullCalendar('removeEvents');
		        	getFreshEvents();
					
		        }
			});
		});
	});
</script>
<style type="text/css"></style>
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        	 <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
				<li>
					<a href="<?php echo Yii::app()->createUrl('DmKalenderEvents');?>"><i class="icon-chevron-right"></i> Event</a>
				</li>
			<li>
					<a href="<?php echo Yii::app()->createUrl('DmKalender/create');?>"><i class="icon-chevron-right"></i> Input Event ke Kalender</a>
				</li>
			</ul>
            
        </div>
        
        <!--/span-->
        <div class="span9" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Kalender','url'=>Yii::app()->createUrl('DmKalender/index')),
                            array('name' => 'List')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Kalender</div>
                         <!-- <div class="pull-right">Data per halaman
                        </div>  -->
                    </div>
                    <div class="block-content collapse in">
                     			
                    <div class="span12">

                        <div id='calendar'></div>
                    </div>
                   <!-- Konten -->
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
 <div id="myModal" class="modal hide">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button">&times;</button>
		<h3>Input Event pada Kalender</h3>
	</div>
	<div class="modal-body">
       
    
    
    <div class="control-group">
    	Event
      <div class="controls">
        <?php 
          $listdata = CHtml::ListData(DmKalenderEvents::model()->findAll(),'id','nama_event');
         echo CHtml::dropDownList('event_id', '',$listdata);
        ?>
       
       
      </div>
    </div>
   
    <div class="control-group">
    	Keterangan
      <div class="controls">
        <?php 
          echo CHtml::textField('title','',array('class'=>'input-large'));
          	 echo CHtml::hiddenField('startdate','',array('class'=>'input-large'));

          
        ?>

      </div>
    </div>
  	
     
   
  </fieldset>
	</div>
	<div class="modal-footer">
		<img style="display:none" id="loading" src="<?php echo Yii::app()->request->baseUrl;?>/images/loading.gif"/><a id="confirm-send" data-dismiss="modal" class="btn btn-primary" href="#">Confirm</a>
		<a data-dismiss="modal" class="btn" href="#">Cancel</a>
	</div>
</div>
</div>