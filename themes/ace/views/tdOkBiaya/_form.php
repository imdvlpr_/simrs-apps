<?php
/* @var $this TdOkBiayaController */
/* @var $model TdOkBiaya */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'td-ok-biaya-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	  <div class="form-group">
      <?php echo $form->labelEx($model,'dr_operator',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
      <div class="col-sm-9">
        <?php 
        echo CHtml::textField('nama_dr_operator',!empty($model->drOperator) ? $model->drOperator->FULLNAME : '',array('class'=>'input-large nama_dokter_merawat','placeholder'=>'Ketik Nama Dokter'));
        echo $form->hiddenField($model,'dr_operator');
        echo $form->error($model,'dr_operator');?>
      </div>
    </div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_dr_op',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_dr_op',array('class'=>'uang')); ?>
		<?php echo $form->error($model,'biaya_dr_op'); ?>
		</div>
	</div>

    <div class="form-group">
		<?php echo $form->labelEx($model,'biaya_dr_anas',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_dr_anas',array('class'=>'uang')); ?>
		<?php echo $form->error($model,'biaya_dr_anas'); ?>
		</div>
	</div>              	
	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_jrs',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_jrs',array('class'=>'uang')); ?>
		<?php echo $form->error($model,'biaya_jrs'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_bhn',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_bhn',array('class'=>'uang')); ?>
		<?php echo $form->error($model,'biaya_bhn'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_prwt_ok',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_prwt_ok',array('class'=>'uang')); ?>
		<?php echo $form->error($model,'biaya_prwt_ok'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_prwt_anas',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_prwt_anas',array('class'=>'uang')); ?>
		<?php echo $form->error($model,'biaya_prwt_anas'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_rr_askep',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_rr_askep',array('class'=>'uang')); ?>
		<?php echo $form->error($model,'biaya_rr_askep'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_rr_monitor',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_rr_monitor',array('class'=>'uang')); ?>
		<?php echo $form->error($model,'biaya_rr_monitor'); ?>
		</div>
	</div>
	
	    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>

        
        </div>
      </div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	

$(document).bind("keyup.autocomplete",function(){
   $('.nama_dokter_merawat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
        
      },
      focus: function (event, ui) {
       $(this).next().val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

</script>