<?php 
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_OK_'.$tgl.'.xls';
header('Content-type: application/excel');

header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="18">
      LAPORAN OK <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="3">
	<tr>
		<TH>NO</TH>
		<TH>NAMA PX</TH>
		<th>GOL PX</th>
		<TH>REG</TH>
		<TH>TGL MRS</TH>
		<TH>TGL OP</TH>
		<TH>UNIT</TH>
		<TH>KELAS<br>TINDAKAN</TH>
		<th>UPF</th>
		<th>TINDAKAN</th>
		<TH>JRS</TH>
		<TH>DR OP</TH>
		<TH>BIAYA</TH>
		<TH>DR. ANAS</TH>
		<TH>BIAYA</TH>
		<TH>PRWT OK</TH>
		<TH>PRWT ANAS</TH>
		<TH>KET</TH>
		<TH>Diagnosa</TH>
	</tr>
	<?php
	$i=0;
	$totalbhn=0;
	$totalop=0;
	$totalanas=0;
	$totalprwtok=0;
	$totalprwtanas=0;
	foreach($models as $data){
		$i++;
		$totalbhn+=$data->biaya_bhn;
		$totalop=$totalop+$data->biaya_dr_op;
		$totalanas+=$data->biaya_dr_anas;
		$totalprwtok+=$data->biaya_prwt_ok;
		$totalprwtanas+=$data->biaya_prwt_anas;

	?>
	<tr>
		<td><?=$i;?></td>
		<td><?php echo $data->tdRegisterOk->pASIEN->NAMA ?></td>
		<td><?=$data->tdRegisterOk->kodeDaftar->kodeGol->NamaGol?></td>
		
		<td><?=$data->tdRegisterOk->pASIEN->NoMedrec?></td>
		<td><?=$data->tdRegisterOk->kodeDaftar->TGLDAFTAR?></td>
		<td><?=$data->tdRegisterOk->tgl_operasi?></td>
		<td><?=$data->tdRegisterOk->unit->NamaUnit?></td>
		<td><?=$data->tdRegisterOk->tindakan0->dmKelas->nama_kelas?></td>
		<td><?=$data->tdRegisterOk->dataUpf->nama?></td>
		<td><?=$data->tdRegisterOk->jenisOperasi->nama?></td>
		<td style="text-align: right;"><?=$data->biaya_jrs?></td>
		<td><?=$data->drOperator->FULLNAME?></td>
		<td style="text-align: right;"><?=$data->biaya_dr_op?></td>
		<td><?=$data->tdRegisterOk->drAnastesi->FULLNAME?></td>
		<td style="text-align: right;"><?=$data->biaya_dr_anas?></td>
		<td style="text-align: right;"><?=$data->biaya_prwt_ok?></td>
		<td style="text-align: right;"><?=$data->biaya_prwt_anas?></td>
		<td><?=$data->tdRegisterOk->tindakan0->dmOkJenisTindakan->nama?></td>
		<td><?=$data->tdRegisterOk->diagnosa?></td>
	</tr>
<?php } ?>
	<tr>
		<td colspan="10">TOTAL</td>
		<td  style="text-align: right;"><?=$totalbhn?></td>
		<td></td>
		<td  style="text-align: right;"><?=$totalop?></td>
		<td></td>
		<td  style="text-align: right;"><?=$totalanas?></td>
		<td style="text-align: right;"><?=$totalprwtok?></td>
		<td style="text-align: right;"><?=$totalprwtanas?></td>
		<td style="text-align: right;"></td>
		<td style="text-align: right;"></td>
	</tr>
</table>