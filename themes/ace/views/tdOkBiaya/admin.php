<?php
/* @var $this TdOkBiayaController */
/* @var $model TdOkBiaya */
$this->breadcrumbs=array(
   array('name' => 'Tindakan OK','url'=>Yii::app()->createUrl('tdRegisterOk/admin')),
                            array('name' => 'List')
);
?>

<h1>Rekapitulasi Biaya</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size,#UPF,#DOKTER').change(function(){
			$('#td-ok-biaya-grid').yiiGridView.update('td-ok-biaya-grid', {
			    url:'<?php echo Yii::app()->createUrl("TdOkBiaya/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()+'&UPF='+$('#UPF').val()+'&DOKTER='+$('#DOKTER').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        <!-- <div class="span3" id="sidebar">


        </div> -->

            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }


?>


                <!-- block -->
    
                         <div class="pull-right">Data per halaman
<?php                        
echo CHtml::dropDownList('TdOkBiaya[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size','size'=>1)); 
?> 
UPF
 <?php                        
$cdb = new CDbCriteria;
$cdb->join = 'JOIN td_register_ok o ON t.id = o.upf';
    echo CHtml::dropDownList('TdOkBiaya[UPF]',isset($_GET['UPF'])?$_GET['UPF']:'',CHtml::listData(TdUpf::model()->findAll($cdb),'id','nama'),array('id'=>'UPF','empty'=>'.:Semua:.')); 
                            ?> 
Dr OP
 <?php                        
  $cdb = new CDbCriteria;
            $cdb->join = 'JOIN td_ok_biaya b ON t.id_dokter = b.dr_operator';
    echo CHtml::dropDownList('TdOkBiaya[DOKTER]',isset($_GET['DOKTER'])?$_GET['DOKTER']:'',CHtml::listData(DmDokter::model()->findAll($cdb),'id_dokter','nama_dokter'),array('id'=>'DOKTER','empty'=>'.:Semua:.')); 
                            ?> 
                             <?php        echo CHtml::textField('TdOkBiaya[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    </div>
                    <div class="block-content collapse in">
<p><a href="<?=Yii::app()->createUrl('tdOkBiaya/export');?>" class="btn btn-success"><i class="icon-edit"></i> Export XLS</a>
</p>
<?php 

$this->widget('application.components.ComplexGridView', array(
	'id'=>'td-ok-biaya-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		array('header'=>'Nama Px','value'=>'$data->tdRegisterOk->pASIEN->NAMA'),
		array('header'=>'Gol Px','value'=>'$data->tdRegisterOk->kodeDaftar->kodeGol->NamaGol'),
		array('header'=>'No RM','value'=>'$data->tdRegisterOk->pASIEN->NoMedrec'),
		array('header'=>'Tgl MRS','value'=>'$data->tdRegisterOk->kodeDaftar->TGLDAFTAR'),
		array('header'=>'Tgl OP','value'=>'$data->tdRegisterOk->tgl_operasi'),
		array('header'=>'Ruang','value'=>'$data->tdRegisterOk->unit->NamaUnit'),
		array('header'=>'Kls','value'=>'$data->tdRegisterOk->tindakan0->dmKelas->nama_kelas'),
		array('header'=>'UPF','value'=>'$data->tdRegisterOk->dataUpf->nama'),
		array('header'=>'Tindakan','value'=>'$data->tdRegisterOk->jenisOperasi->nama'),
		'biaya_jrs',
		array('header'=>'Dr OP','value'=>'$data->drOperator->FULLNAME'),
		'biaya_dr_op',
		array('header'=>'Dr AN','value'=>'$data->tdRegisterOk->drAnastesi->FULLNAME'),
		'biaya_dr_anas',
		'biaya_prwt_ok',
		'biaya_prwt_anas',
		array('header'=>'KET','value'=>'$data->tdRegisterOk->tindakan0->dmOkJenisTindakan->nama'),
		array('header'=>'Diagnosa','value'=>'$data->tdRegisterOk->diagnosa'),
		/*
		'biaya_prwt_anas',
		'created',
		'td_ok_tindakan_id',
		*/
		// array(
		// 	'class'=>'CButtonColumn',
		// ),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									   'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									 'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								
  'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


                    </div>
                </div>
            