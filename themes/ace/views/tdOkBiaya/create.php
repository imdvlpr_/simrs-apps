
<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */
$this->breadcrumbs=array(
    array('name' => 'OK Biaya','url'=>Yii::app()->createUrl('tdRegisterOk/view',['id'=>$model->td_register_ok_id])),
                            array('name' => 'Create')
);
?>

<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>


<div class="row">
	<div class="col-xs-12">
	
<?php $this->renderPartial('_form', array('model'=>$model)); ?>					
	</div>
</div>

