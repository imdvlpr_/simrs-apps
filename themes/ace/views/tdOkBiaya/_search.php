<?php
/* @var $this TdOkBiayaController */
/* @var $model TdOkBiaya */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_bhn'); ?>
		<?php echo $form->textField($model,'biaya_bhn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'td_register_ok_id'); ?>
		<?php echo $form->textField($model,'td_register_ok_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_dr_op'); ?>
		<?php echo $form->textField($model,'biaya_dr_op'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_dr_anas'); ?>
		<?php echo $form->textField($model,'biaya_dr_anas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_pwrt_ok'); ?>
		<?php echo $form->textField($model,'biaya_pwrt_ok'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_prwt_anas'); ?>
		<?php echo $form->textField($model,'biaya_prwt_anas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'td_ok_tindakan_id'); ?>
		<?php echo $form->textField($model,'td_ok_tindakan_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->