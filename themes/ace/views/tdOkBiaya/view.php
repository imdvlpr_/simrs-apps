<?php
/* @var $this TdOkBiayaController */
/* @var $model TdOkBiaya */

$this->breadcrumbs=array(
array('name' => 'OK Biaya','url'=>Yii::app()->createUrl('tdRegisterOk/view',['id'=>$model->td_register_ok_id])),
                            array('name' => 'View')
);

$this->menu=array(
	array('label'=>'List TdOkBiaya', 'url'=>array('index')),
	array('label'=>'Create TdOkBiaya', 'url'=>array('create')),
	array('label'=>'Update TdOkBiaya', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TdOkBiaya', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TdOkBiaya', 'url'=>array('admin')),
);
?>

<h1>View TdOkBiaya #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'biaya_bhn',
		'td_register_ok_id',
		'biaya_dr_op',
		'biaya_dr_anas',
		'biaya_pwrt_ok',
		'biaya_prwt_anas',
		'created',
		'td_ok_tindakan_id',
	),
)); ?>
