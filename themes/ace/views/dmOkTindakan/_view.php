<?php
/* @var $this DmOkTindakanController */
/* @var $data DmOkTindakan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode')); ?>:</b>
	<?php echo CHtml::encode($data->kode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_jrs')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_jrs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_drop')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_drop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_dran')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_dran); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_prwt_op')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_prwt_op); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya_prwt_an')); ?>:</b>
	<?php echo CHtml::encode($data->biaya_prwt_an); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dm_kelas_id')); ?>:</b>
	<?php echo CHtml::encode($data->dm_kelas_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dm_ok_jenis_tindakan_id')); ?>:</b>
	<?php echo CHtml::encode($data->dm_ok_jenis_tindakan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dm_gol_operasi_id')); ?>:</b>
	<?php echo CHtml::encode($data->dm_gol_operasi_id); ?>
	<br />

	*/ ?>

</div>