<?php
/* @var $this DmOkTindakanController */
/* @var $model DmOkTindakan */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dm-ok-tindakan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>
	<div class="form-group">
		<?php echo $form->labelEx($model,'dm_kelas_id', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'dm_kelas_id',CHtml::listData(DmKelas::model()->findAll(),'id_kelas','nama_kelas'),array('empty'=>'.. Pilih Kelas ..'));

		?>
		<?php echo $form->error($model,'dm_kelas_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dm_ok_jenis_tindakan_id', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'dm_ok_jenis_tindakan_id',CHtml::listData(DmOkJenisTindakan::model()->findAll(),'id','nama'),array('empty'=>'.. Pilih Jenis Tindakan ..'));

		?>
		<?php echo $form->error($model,'dm_ok_jenis_tindakan_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dm_gol_operasi_id', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'dm_gol_operasi_id',CHtml::listData(DmOkGolOperasi::model()->findAll(),'id','nama'),array('empty'=>'.. Pilih Gol Operasi ..'));

		?>
		<?php echo $form->error($model,'dm_gol_operasi_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_jrs', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_jrs'); ?>
		<?php echo $form->error($model,'biaya_jrs'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_drop', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_drop'); ?>
		<?php echo $form->error($model,'biaya_drop'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_dran', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_dran'); ?>
		<?php echo $form->error($model,'biaya_dran'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_prwt_op', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_prwt_op'); ?>
		<?php echo $form->error($model,'biaya_prwt_op'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya_prwt_an', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya_prwt_an'); ?>
		<?php echo $form->error($model,'biaya_prwt_an'); ?>
		</div>
	</div>

	

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
