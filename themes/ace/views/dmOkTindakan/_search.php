<?php
/* @var $this DmOkTindakanController */
/* @var $model DmOkTindakan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode'); ?>
		<?php echo $form->textField($model,'kode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_jrs'); ?>
		<?php echo $form->textField($model,'biaya_jrs'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_drop'); ?>
		<?php echo $form->textField($model,'biaya_drop'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_dran'); ?>
		<?php echo $form->textField($model,'biaya_dran'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_prwt_op'); ?>
		<?php echo $form->textField($model,'biaya_prwt_op'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya_prwt_an'); ?>
		<?php echo $form->textField($model,'biaya_prwt_an'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dm_kelas_id'); ?>
		<?php echo $form->textField($model,'dm_kelas_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dm_ok_jenis_tindakan_id'); ?>
		<?php echo $form->textField($model,'dm_ok_jenis_tindakan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dm_gol_operasi_id'); ?>
		<?php echo $form->textField($model,'dm_gol_operasi_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->