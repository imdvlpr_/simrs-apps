<?php
/* @var $this DmOkTindakanController */
/* @var $model DmOkTindakan */

$this->breadcrumbs=array(
	array('name'=>'Dm Ok Tindakan','url'=>array('admin')),
	array('name'=>'Dm Ok Tindakan'),
);

$this->menu=array(
	array('label'=>'List DmOkTindakan', 'url'=>array('index')),
	array('label'=>'Create DmOkTindakan', 'url'=>array('create')),
	array('label'=>'Update DmOkTindakan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmOkTindakan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmOkTindakan', 'url'=>array('admin')),
);
?>

<h1>View DmOkTindakan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'kode',
		'biaya_jrs',
		'biaya_drop',
		'biaya_dran',
		'biaya_prwt_op',
		'biaya_prwt_an',
		'dm_kelas_id',
		'dm_ok_jenis_tindakan_id',
		'dm_gol_operasi_id',
	),
)); ?>
	</div>
</div>
