<?php
/* @var $this TindakanMedisOperatifController */
/* @var $model TindakanMedisOperatif */

$this->breadcrumbs=array(
	array('name'=>'Tindakan Medis Operatif','url'=>array('admin')),
	array('name'=>'Tindakan Medis Operatif'),
);

$this->menu=array(
	array('label'=>'List TindakanMedisOperatif', 'url'=>array('index')),
	array('label'=>'Create TindakanMedisOperatif', 'url'=>array('create')),
	array('label'=>'Update TindakanMedisOperatif', 'url'=>array('update', 'id'=>$model->id_tindakan)),
	array('label'=>'Delete TindakanMedisOperatif', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tindakan),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TindakanMedisOperatif', 'url'=>array('admin')),
);
?>

<h1>View TindakanMedisOperatif #<?php echo $model->id_tindakan; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tindakan',
		'nama_tindakan',
		'kode_tindakan',
		'kelas_id',
		'jrs',
		'drOP',
		'drANAS',
		'perawatOK',
		'perawatANAS',
		'total',
		'tarip',
		'created',
	),
)); ?>
	</div>
</div>
