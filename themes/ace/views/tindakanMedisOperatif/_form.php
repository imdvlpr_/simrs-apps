<?php
/* @var $this TindakanMedisOperatifController */
/* @var $model TindakanMedisOperatif */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tindakan-medis-operatif-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_tindakan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_tindakan',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama_tindakan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kode_tindakan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kode_tindakan',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'kode_tindakan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_id', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kelas_id'); ?>
		<?php echo $form->error($model,'kelas_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jrs', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jrs'); ?>
		<?php echo $form->error($model,'jrs'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'drOP', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'drOP'); ?>
		<?php echo $form->error($model,'drOP'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'drANAS', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'drANAS'); ?>
		<?php echo $form->error($model,'drANAS'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'perawatOK', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'perawatOK'); ?>
		<?php echo $form->error($model,'perawatOK'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'perawatANAS', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'perawatANAS'); ?>
		<?php echo $form->error($model,'perawatANAS'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'total', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'total'); ?>
		<?php echo $form->error($model,'total'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tarip', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tarip'); ?>
		<?php echo $form->error($model,'tarip'); ?>
		</div>
	</div>


	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
