<?php

 $cid = Yii::app()->getController()->getAction()->id;
 $id = Yii::app()->request->getQuery('id');

$aksesFarm = Yii::app()->user->checkAccess(array(WebUser::R_OP_FARMASI));
$aksesRad = Yii::app()->user->checkAccess(array(WebUser::R_OP_RADIOLOGI));


$this->widget('zii.widgets.CMenu',array(

    'htmlOptions'=>array('id'=>'','class'=>'nav nav-tabs padding-12 tab-color-blue background-blue'),
    'items'=>array(
        array('label'=>'DATA PASIEN', 'url'=>array('/trRawatInap/dataPasien','id'=>$id),'active'=>$cid == 'dataPasien'),
        array('label'=>'PELAYANAN MEDIK', 'url'=>array('/trRawatInap/pelMedik','id'=>$id),'active'=>$cid == 'pelMedik','visible' => !$aksesFarm && !$aksesRad),
        array('label'=>'T. MEDIK OPERASI', 'url'=>array('/trRawatInap/medikTOP','id'=>$id),'active'=>$cid == 'medikTOP','visible' => !$aksesFarm && !$aksesRad),
        array('label'=>'T. MEDIK NON-OPERASI', 'url'=>array('/trRawatInap/medikTNOP','id'=>$id),'active'=>$cid == 'medikTNOP','visible' => !$aksesFarm && !$aksesRad),
        array('label'=>'PENUNJANG', 'url'=>array('/trRawatInap/penunjang','id'=>$id),'active'=>$cid == 'penunjang','visible' => !$aksesFarm),
        array('label'=>'OBAT DAN ALKES', 'url'=>array('/trRawatInap/alkes','id'=>$id),'active'=>$cid == 'alkes','visible'=>!$aksesRad ),
        array('label'=>'LAIN-LAIN', 'url'=>array('/trRawatInap/lainnya','id'=>$id),'active'=>$cid == 'lainnya','visible' => !$aksesFarm && !$aksesRad),
        array('label'=>'TOTAL', 'url'=>array('/trRawatInap/total','id'=>$id),'active'=>$cid == 'total','visible' => !$aksesFarm && !$aksesRad),

       
    ),
)); 