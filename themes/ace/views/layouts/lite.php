  <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();
   
  ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		 <title><?php echo $this->title;?></title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="shortcut icon" href="<?=Yii::app()->baseUrl;?>/images/favicon.jpg" />

		<!-- bootstrap & fontawesome -->
		<?php
		$cs->registerCssFile($baseUrl.'/assets/css/bootstrap.min.css');
		$cs->registerCssFile($baseUrl.'/assets/font-awesome/4.5.0/css/font-awesome.min.css');
		$cs->registerCssFile($baseUrl.'/assets/css/fonts.googleapis.com.css');
		$cs->registerCssFile($baseUrl.'/assets/css/ace.min.css');
		$cs->registerCssFile($baseUrl.'/assets/css/ace-skins.min.css');
		$cs->registerCssFile($baseUrl.'/assets/css/ace-rtl.min.css');
		$cs->registerCssFile($baseUrl.'/assets/css/loading.css');
			// 
		?>
		

	</head>

	<body class="no-skin">

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<?=$content;?>
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		

		<!-- <![endif]-->

		<!--[if IE]>
<script src="<?=$baseUrl;?>/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<?php 
		Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->clientScript->registerCoreScript('jquery.ui');	
		$cs->registerScriptFile($baseUrl.'/assets/js/bootstrap.min.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/assets/js/jquery.maskedinput.min.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/assets/scripts.js',CClientScript::POS_END);

		$cs->registerScriptFile($baseUrl.'/assets/js/ace-elements.min.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/assets/js/ace.min.js',CClientScript::POS_END);
		// $cs->registerScriptFile($baseUrl.'/assets/jquery.dateentry/jquery.plugin.js');
		$cs->registerScriptFile($baseUrl.'/assets/js/jquery.datetextentry.js',CClientScript::POS_END);
		
		// 
		?>

	</body>
</html>
