<div id="navbar" class="navbar navbar-default    navbar-collapse       h-navbar ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?=Yii::app()->createUrl('site/index');?>" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							SIMRS
						</small>
					</a>
					<div class="navbar-brand"><?=!empty(Yii::app()->user->getState('unitNama')) ? Yii::app()->user->getState('unitNama') : '';?></div>
					<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu">
						<span class="sr-only">Toggle user menu</span>

						<img src="<?=$baseUrl;?>/assets/images/avatars/avatar2.png" alt="Jason's Photo" />
					</button>

					<button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#sidebar">
						<span class="sr-only">Toggle sidebar</span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
					<ul class="nav ace-nav">
						<?php 
						$countNotif = Notif::countNotif();
						?>
						<li class="blue dropdown-modal">
							<?php 
                            
                            
                            ?>
							 <a id="notif-toggle" data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell icon-animated-bell"></i>
                                <span class="badge badge-important" id="count-notif">
                                    <?=$countNotif;?>
                                </span>
							</a>
							<?php 
							
							?>
							 <ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
                                <li class="dropdown-header">
                                    <i class="ace-icon fa fa-exclamation-triangle"></i>
                                    <?=$countNotif;?> Notification<?=$countNotif > 1 ? 's' : '';?>
                                </li>

                                <li class="dropdown-content">
                                    <ul class="dropdown-menu dropdown-navbar navbar-pink" id="notif-content">
                                       
                                     
                                     
                                    </ul>
                                </li>

                                <li class="dropdown-footer">
                                    <a href="<?=Yii::app()->createUrl('notif/index');?>">
                                        See all notifications
                                        <i class="ace-icon fa fa-arrow-right"></i>
                                    </a>
                                </li>
                            </ul>
						</li>

						<li class="light-blue dropdown-modal user-min">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?=$baseUrl;?>/assets/images/avatars/avatar2.png" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									<?=strtoupper(Yii::app()->user->getState('nama'));?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<?php
							 $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					// 'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(
                        array('label'=>'<i class="ace-icon fa fa-user"></i>Profil', 'url'=>array('/pegawai/view','id'=>Yii::app()->user->getState('pegawai_id'))),
                            array('label'=> '','itemOptions'=>array('class'=>'divider'),'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA))),
                            array('label'=>'Pengguna', 'url'=>array('/user/index'),'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA))),
                            array('label'=> '','itemOptions'=>array('class'=>'divider')),
							array('label'=>'Logout', 'url'=>array('/site/logout'),'visible'=>!Yii::app()->user->isGuest),
                    


                    ),
                )); ?>
						
						</li>
					</ul>
				</div>

			<!-- 	<nav role="navigation" class="navbar-menu pull-left collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								Overview
	  		
								<i class="ace-icon fa fa-angle-down bigger-110"></i>
							</a>

							<ul class="dropdown-menu dropdown-light-blue dropdown-caret">
								<li>
									<a href="#">
										<i class="ace-icon fa fa-eye bigger-110 blue"></i>
										Monthly Visitors
									</a>
								</li>

								<li>
									<a href="#">
										<i class="ace-icon fa fa-user bigger-110 blue"></i>
										Active Users
									</a>
								</li>

								<li>
									<a href="#">
										<i class="ace-icon fa fa-cog bigger-110 blue"></i>
										Settings
									</a>
								</li>
							</ul>
						</li>

						<li>
							<a href="#">
								<i class="ace-icon fa fa-envelope"></i>
								Messages
								<span class="badge badge-warning">5</span>
							</a>
						</li>
					</ul>

					
				</nav> -->
			</div><!-- /.navbar-container -->
		</div>
