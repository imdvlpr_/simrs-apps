  <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();
   
  ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		 <title><?php echo $this->title;?></title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="shortcut icon" href="<?=Yii::app()->baseUrl;?>/images/favicon.jpg" />

		<!-- bootstrap & fontawesome -->
		<?php
		$cs->registerCssFile($baseUrl.'/assets/css/bootstrap.min.css');
		$cs->registerCssFile($baseUrl.'/assets/font-awesome/4.5.0/css/font-awesome.min.css');
		$cs->registerCssFile($baseUrl.'/assets/css/fonts.googleapis.com.css');
		$cs->registerCssFile($baseUrl.'/assets/css/ace.min.css');
		$cs->registerCssFile($baseUrl.'/assets/css/ace-skins.min.css');
		$cs->registerCssFile($baseUrl.'/assets/css/ace-rtl.min.css');
		$cs->registerCssFile($baseUrl.'/assets/css/jquery.datetextentry.css');
		$cs->registerCssFile($baseUrl.'/assets/css/loading.css');
			// 
		?>
		

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?=$baseUrl;?>/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<?php 
		 $cs->registerScriptFile($baseUrl.'/assets/js/ace-extra.min.js');
		 $cs->registerScriptFile(Yii::app()->baseUrl.'/node_modules/push.js/bin/push.min.js');
		 // $cs->registerScriptFile($baseUrl.'/assets/js/ace-extra.min.js');
		?>
	<!-- 	<script src="<?=$baseUrl;?>/assets/js/ace-extra.min.js"></script>
		<script src="<?=$baseUrl;?>/assets/js/jquery-2.1.4.min.js"></script> -->
		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?=$baseUrl;?>/assets/js/html5shiv.min.js"></script>
		<script src="<?=$baseUrl;?>/assets/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<?php require_once('nav_main.php');?>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<?php require_once('nav_submain.php');?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<?php 
						$this->widget('application.components.BreadCrumb', array(
    'links'=>$this->breadcrumbs,
));
						?>
					</div>
					<div class="page-content">
					

						<?=$content;?>
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php require_once('tpl_footer.php');?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		

		<!-- <![endif]-->

		<!--[if IE]>
<script src="<?=$baseUrl;?>/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?=$baseUrl;?>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<?php 
		Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->clientScript->registerCoreScript('jquery.ui');	
		$cs->registerScriptFile($baseUrl.'/assets/js/bootstrap.min.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/assets/js/jquery.maskedinput.min.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/assets/scripts.js',CClientScript::POS_END);

		$cs->registerScriptFile($baseUrl.'/assets/js/ace-elements.min.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/assets/js/ace.min.js',CClientScript::POS_END);
		// $cs->registerScriptFile($baseUrl.'/assets/jquery.dateentry/jquery.plugin.js');
		$cs->registerScriptFile($baseUrl.'/assets/js/jquery.datetextentry.js',CClientScript::POS_END);
		
		// 
		?>

		<!-- page specific plugin scripts -->
		
		
		<!-- ace scripts -->
		
		<!-- inline scripts related to this page -->
<script type="text/javascript">
function playSound(url,ket)
{
    var audio = new Audio('<?=Yii::app()->baseUrl;?>/sounds/notif.mp3');
    // if(isBlur)
    	audio.play();
    
    // Push.create('New Lab Request',{
    // 	tag: 'notif',
    // 	body: ket,
    // 	onClick: function () {
	        
	   //      window.open(url,'myreq').focus();
	   //      Push.close('notif');
	   //      this.close();
	   //  }
    // });


}

function ajaxCountNotif(){
    $.ajax({
        async : true,
        url : '<?=Yii::app()->createUrl('notif/ajaxCountNotif');?>',
        beforeSend: function(){
            
        },
        success : function(data){
            var hsl = jQuery.parseJSON(data);
          
            if(hsl.jumlah > 0){

            	playSound(hsl.url,hsl.ket);
            }
            $('#count-notif').html(hsl.jumlah);
        }
    });
}

function ajaxLoadNotif(){
     $.ajax({
        async : true,
        url : '<?=Yii::app()->createUrl('notif/ajaxNotif');?>',
        beforeSend: function(){
            
        },
        success : function(data){
            $('#notif-content').empty();
            var hsl = jQuery.parseJSON(data);
            var row = '';
            $.each(hsl, function(i, obj){
                console.log(obj.url);
               row += '<li>';
               row +=     '<a href="'+obj.url+'">';
               row +=     '<div class="clearfix"><span class="pull-left"><i class="btn btn-xs no-hover btn-success fa fa-shopping-cart"></i>'+obj.keterangan+'</span><span class="pull-right badge badge-success">';
                // row += obj.jumlah;
                row += '</span></div></a></li>';
            });

            $('#notif-content').append(row);
        }
    });

}
  // Let's check if the browser supports notifications
   


jQuery(function($) {
   $('#sidebar2').insertBefore('.page-content');
   $('#navbar').addClass('h-navbar');
   $('.footer').insertAfter('.page-content');
   
   $('.page-content').addClass('main-content');
   
   $('.menu-toggler[data-target="#sidebar2"]').insertBefore('.navbar-brand');
   
   
   $(document).on('settings.ace.two_menu', function(e, event_name, event_val) {
	 if(event_name == 'sidebar_fixed') {
		 if( $('#sidebar').hasClass('sidebar-fixed') ) $('#sidebar2').addClass('sidebar-fixed')
		 else $('#sidebar2').removeClass('sidebar-fixed')
	 }
   }).triggerHandler('settings.ace.two_menu', ['sidebar_fixed' ,$('#sidebar').hasClass('sidebar-fixed')]);
   
   $('#sidebar2[data-sidebar-hover=true]').ace_sidebar_hover('reset');
   $('#sidebar2[data-sidebar-scroll=true]').ace_sidebar_scroll('reset', true);
   
   setInterval(function() {
        ajaxCountNotif();
    }, 5000);
    $('#notif-toggle').on('click',function(){
         ajaxLoadNotif();
    });

   

});

</script>
	</body>
</html>
