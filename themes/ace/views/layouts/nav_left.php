

 <?php 
  $this->widget('zii.widgets.CMenu', array(
  	'encodeLabel'=>false,
    'items'=>array(
       array('label'=>'<i class="icon-chevron-right"></i> Pasien Masuk', 'url'=>array('TrRawatInap/create')),
       array('label'=>'<i class="icon-chevron-right"></i> Rawat Inap', 'url'=>array('TrRawatInap/index')),
       array('label'=>'<i class="icon-chevron-right"></i> Data Rawat Per Pasien', 'url'=>array('TrRawatInap/datarawat')), 
       	 
    ),
    'htmlOptions' => array(
        'class' =>"nav nav-list bs-docs-sidenav nav-collapse collapse"
      )
));
?>