<?php

 $id = Yii::app()->request->getQuery('id');


$rawatInap = TrRawatInap::model()->findByPk($id);

$pasien = $rawatInap->pASIEN;
// print_r(strtotime($rawatInap->tanggal_masuk_ird));exit;
?>


<table class="table">
   <tr>
     <td>Nama Pasien</td>
     <td>:</td>
     <td colspan="2"><strong><?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?></strong></td>
     
     <td>No Reg</td>
     <td>:</td>
     <td><?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk Kamar / IGD</td>
     <td>:</td>
     <td colspan="2"><?php echo $rawatInap->tanggal_masuk;?>
       <?php 
       
       echo is_null($rawatInap->tanggal_masuk_ird) ? ' / '.$rawatInap->tanggal_masuk_ird : '';
       ?>
     </td>
     
     <td>Tgl Keluar</td>
     <td>:</td>
     <td>

       <?php 
        echo !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');


       // echo CHtml::textField('tanggal_keluar',!empty($rawatInap->tanggal_keluar) ? $rawatInap->tanggal_keluar : date('d/m/Y'));
       // echo !empty($rawatInap->tanggal_keluar) ? $rawatInap->tanggal_keluar : '' ; 
       ?>
     </td>
   </tr>
   <tr>
     <td>Dirawat/R. Kelas</td>
     <td>:</td>
     <td><?php 

     echo $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;

     ?></td>
     <td></td>
     <td>Lm. Dirawat</td>
     <td>:</td>
     <td>
        <?php 

        $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
          $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), $tgl_keluar);
        }
        else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }


       

        echo $selisih_hari.' hari';

        ?>

     </td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td colspan="2"><?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Dokter yang merawat</td>
     <td>:</td>
     <td>
    <?php 
    $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
      ?>
      <strong><?php echo $nama_dokter;?></strong>

     </td>

     </td>
   </tr>
 </table>
