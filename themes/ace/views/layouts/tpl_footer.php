<div class="footer">
	<div class="footer-inner">
		<div class="footer-content">
			<span class="bigger-120">
				Developed by <span class="blue bolder"><a target="_blank" href="http://keltech.co.id">KelTech</a></span>
				&copy; 2016-<?=date('Y');?> RSUD Pare Kabupaten Kediri. All Rights Reserved
			</span>

			&nbsp; &nbsp;
			<!-- <span class="action-buttons">
				<a href="#">
					<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
				</a>

				<a href="#">
					<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
				</a>

				<a href="#">
					<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
				</a>
			</span> -->
		</div>
	</div>
</div>	

