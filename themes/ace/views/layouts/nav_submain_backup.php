<style>
    li.divider{
        border-top: 1px #d1dfea solid;
    }
</style>
<div id="sidebar" class="sidebar      h-sidebar                navbar-collapse collapse          ace-save-state">
                <script type="text/javascript">
                    try{ace.settings.loadState('sidebar')}catch(e){}
                </script>

                <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                        <button class="btn btn-success">
                            <i class="ace-icon fa fa-signal"></i>
                        </button>

                        <button class="btn btn-info">
                            <i class="ace-icon fa fa-pencil"></i>
                        </button>

                        <button class="btn btn-warning">
                            <i class="ace-icon fa fa-users"></i>
                        </button>

                        <button class="btn btn-danger">
                            <i class="ace-icon fa fa-cogs"></i>
                        </button>
                    </div>

                    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                        <span class="btn btn-success"></span>

                        <span class="btn btn-info"></span>

                        <span class="btn btn-warning"></span>

                        <span class="btn btn-danger"></span>
                    </div>
                </div><!-- /.sidebar-shortcuts -->
                  <?php 

                 $itemLayanan = array(
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> UMUM', 'url'=>array('trPendaftaranRjalan/create','jenis'=>1001)),
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> BPJS', 'url'=>array('trPendaftaranRjalan/create','jenis'=>111)),
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> In-Health', 'url'=>array('trPendaftaranRjalan/create','jenis'=>2401)),
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> JAMKESDA', 'url'=>array('trPendaftaranRjalan/create','jenis'=>1002))
                        );
                 
                $submenuItems['baru'] = array(
                        'label'=>'<i class="menu-icon fa fa-caret-right"></i> Baru <b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                        'items'=>$itemLayanan);


                $itemLayanan = array(
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> UMUM', 'url'=>array('trPendaftaranRjalan/cariPasien','jenis'=>1001)),
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> BPJS', 'url'=>array('trPendaftaranRjalan/cariPasien','jenis'=>111)),
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> In-Health', 'url'=>array('trPendaftaranRjalan/cariPasien','jenis'=>2401)),
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> JAMKESDA', 'url'=>array('trPendaftaranRjalan/cariPasien','jenis'=>1002))
                        );

               
                $submenuItems['lama'] = array(
                        'label'=>'<i class="menu-icon fa fa-caret-right"></i> Lama <b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                        'items'=>$itemLayanan);

             $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'nav nav-list'),
                    'submenuHtmlOptions'=>array('class'=>'submenu'),

                    'itemCssClass'=>'hover',
                    'encodeLabel'=>false,
                    'items'=>array(
                        [
                            'label'=>'<i class="menu-icon fa fa-tachometer"></i><span class="menu-text"> Beranda </span>', 
                            'url'=>array('/site/index'),
                        ],
                        [
                            'label'=>'<i class="menu-icon fa fa-tachometer"></i><span class="menu-text"> Dashboard </span><i class="caret"></i>', 
                            'itemOptions'=>array('class'=>'dropdown-toggle','tabindex'=>"-1"),
                            'url'=>'#',
                            'visible' => Yii::app()->user->checkAccess([WebUser::R_SA,WebUser::R_DIREKTUR,WebUser::R_OP_RM,WebUser::R_OP_OPERASI]),
                            'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                            'items'=>[
                                [
                                    'label'=>'<i class="menu-icon fa fa-caret-right"></i> Kunjungan', 
                                    'url'=>['/site/dashboard'],
                                    'visible' => !Yii::app()->user->checkAccess([WebUser::R_OP_OPERASI])
                                ],
                                [
                                    'label'=>'<i class="menu-icon fa fa-caret-right"></i> Pendapatan', 
                                    'url'=>['/site/dashboardIncome'],
                                    'visible' => Yii::app()->user->checkAccess([WebUser::R_SA,WebUser::R_DIREKTUR])
                                ],
                                [
                                    'label'=>'<i class="menu-icon fa fa-caret-right"></i> Medik', 
                                    'url'=>['/site/dashboardrm'],
                                    'visible' => !Yii::app()->user->checkAccess([WebUser::R_OP_OPERASI])
                                ],
                                [
                                    'label'=>'<i class="menu-icon fa fa-caret-right"></i> OK', 
                                    'url'=>['/site/dashboardok'],
                                    'visible' => Yii::app()->user->checkAccess([WebUser::R_SA,WebUser::R_DIREKTUR,WebUser::R_OP_OPERASI])
                                ],
                            ],
                        ],
                        ['label'=>'<i class="menu-icon fa fa-wheelchair"></i> <span class="menu-text">Rekam Medik</span> <i class="caret"></i>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown-toggle','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                            'items'=>array(
                                array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Data Pasien', 'url'=>array('/pasien/index')
                                    ),
                                array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Data RM', 'url'=>array('/bPendaftaran/index'),
                                    
                                ),
                                array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Tracking Dokumen', 'url'=>array('/trackingRm/index'),
                                    
                                ),
                          
                               
                                


                            ),
                            'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_RM,WebUser::R_OP_DAFTAR))
                        ],
                        array(
                            'label'=>'<i class="menu-icon fa fa-medkit"></i> <span class="menu-text">Pendaftaran Pasien </span><i class="caret"></i>', 'url'=>'#',
                            'itemOptions'=>array('class'=>'dropdown-toggle','tabindex'=>"-1"),
                            'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                        'items'=>array(

                            
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Rawat Jalan <b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                'items'=>array(
                                    $submenuItems['baru'],
                                    $submenuItems['lama']
                                )),
                            array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Rawat Inap', 
                                'url'=>Yii::app()->createUrl('trRawatInap/create'),
                                
                                ),

                            
                            ),

                            'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_DAFTAR)),
                        ),
                        array('label'=>'<i class="menu-icon fa fa-wheelchair"></i> <span class="menu-text">Data Rawat</span> <i class="caret"></i>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown-toggle','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                            'items'=>array(
                                array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Rawat Jalan', 'url'=>array('/BPendaftaranRjalan/index'),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_RM,WebUser::R_OP_KASIR,WebUser::R_OP_KEUANGAN, WebUser::R_OP_IGD,WebUser::R_KA_KASIR))
                                    ),
                                [
                                    'label'=>'<i class="menu-icon fa fa-caret-right"></i> Kunjungan Hari Ini', 'url'=>['/BPendaftaranRjalan/kunjungan'],
                                    'visible'=>Yii::app()->user->checkAccess([WebUser::R_OP_POLI])
                                ],
                                // [
                                //     'label'=>'<i class="menu-icon fa fa-caret-right"></i> Dokumen', 'url'=>['/BPendaftaranRjalan/terima'],
                                //     'visible'=>Yii::app()->user->checkAccess([WebUser::R_OP_POLI])
                                // ],
                                [
                                    'label'=>'<i class="menu-icon fa fa-caret-right"></i> Pelayanan', 'url'=>['/trRawatJalan/index'],
                                    'visible'=>Yii::app()->user->checkAccess([WebUser::R_OP_POLI])
                                ],
                                // array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Terima Dokumen', 'url'=>array('/BPendaftaranRjalan/terima'),
                                //     'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_RM,WebUser::R_OP_IGD,WebUser::R_OP_POLI))
                                //     ),
                                
                                array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Rawat Inap', 'url'=>array('/trRawatInap/admin'),
                                     'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_RM,WebUser::R_OP_KASIR,WebUser::R_OP_KAMAR,WebUser::R_OP_KEUANGAN,WebUser::R_OP_FARMASI,WebUser::R_OP_RADIOLOGI, WebUser::R_OP_IGD,WebUser::R_KA_KASIR)),
                                    
                                ),
                          
                               
                                


                            ),
                            'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_RM,WebUser::R_OP_KASIR,WebUser::R_OP_KAMAR,WebUser::R_OP_KEUANGAN,WebUser::R_OP_RADIOLOGI,WebUser::R_OP_OPERASI, WebUser::R_OP_IGD,WebUser::R_KA_KASIR,WebUser::R_OP_POLI))
                        ),
                        array('label'=>'<i class="menu-icon fa fa-file"></i> <span class="menu-text">Penerimaan Dokumen</span>', 'url'=>['BPendaftaran/terima'],'itemOptions'=>array('class'=>'dropdown-toggle','tabindex'=>"-1"),
                            'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_RM,WebUser::R_OP_KAMAR,WebUser::R_OP_RADIOLOGI,WebUser::R_OP_OPERASI, WebUser::R_OP_IGD,WebUser::R_OP_POLI))
                        ),
                         array('label'=>'<i class="menu-icon fa fa-wheelchair"></i> <span class="menu-text">Data OK</span><i class="caret"></i>', 'url'=>'#',
                                 'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR,WebUser::R_OP_KEUANGAN,WebUser::R_OP_OPERASI)),
                                 'itemOptions'=>array('class'=>'dropdown-submenu'),
                                 'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'items'=>array(
                                       
                                        array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Register', 'url'=>array('/tdRegisterOk/listPasien')),
                                        array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Daftar Pasien OK ', 'url'=>array('/tdRegisterOk/admin')),
                                        array('label'=>'<li class="divider"></li>'),
                                        array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Rekap OK', 'url'=>array('/tdOkBiaya/admin')),
                                        // array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Export Laporan', 'url'=>array('/tdOkBiaya/export')),
                                         array('label'=>'<li class="divider"></li>'),
                                         array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Infografis', 'url'=>array('/tdRegisterOk/infografis')),
                                       
                                        // array('label'=>'Laporan UPF', 'url'=>array('/tdOkBiaya/upf')),
                                        
                                    )
                            ),
                         array('label'=>'<i class="menu-icon fa fa-heartbeat"></i> Farmasi <i class="caret"></i>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown-toggle','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                            'items'=>array(
                                array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Resep<b class="arrow fa fa-angle-down"></b>', 
                                    'url'=>array('/trResepKamar/admin'),
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'items'=>array(
                                        array('label'=>'Manage', 'url'=>['trResepPasien/admin']),
                                        array('label'=>'Baru', 'url'=>['trResepPasien/create'])
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_FARMASI))
                                ),
                                array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Racikan<b class="arrow fa fa-angle-down"></b>', 
                                    'url'=>array('/trObatRacikan/admin'),
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'items'=>array(
                                        array('label'=>'Manage', 'url'=>['trObatRacikan/admin']),
                                        array('label'=>'Baru', 'url'=>['trObatRacikan/create'])
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_FARMASI))
                                ),
                                array('label'=> '','itemOptions'=>array('class'=>'divider')),
                                array('label'=>'<i class="menu-icon fa fa-caret-right"></i> Laporan<b class="arrow fa fa-angle-down"></b>', 
                                    'url'=>array('/trObatRacikan/admin'),
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'items'=>array(
                                        array('label'=>'Rekap Bulanan', 'url'=>['laporan/resep']),
                                        // array('label'=>'Baru', 'url'=>['trObatRacikan/create'])
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_FARMASI))
                                ),
                            ),
                            'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_FARMASI))
                        ),
                        array('label'=>'<i class="menu-icon fa fa-medkit"></i> BPJS <i class="caret"></i>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown-toggle','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                            'items'=>array(
                               
                                 array('label'=>'<i class="fa fa-caret-right"></i> VClaim <b class="arrow fa fa-angle-down"></b>', 'url'=>'#', 
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'items'=>array(
                                        array('label'=>'<i class="fa fa-caret-right"></i> SEP <b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                            'itemOptions'=>array('class'=>'dropdown-submenu'),
                                            'items'=>array(
                                                array('label'=>'<i class="fa fa-caret-right"></i> Data SEP', 'url'=>array('/bpjsSep/admin')),
                                                array('label'=>'<i class="fa fa-caret-right"></i> Cetak Baru', 'url'=>array('/bpjs/sep')),
                                            ),),
                                        array('label'=>'<i class="fa fa-caret-right"></i> LPK <b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                            'itemOptions'=>array('class'=>'dropdown-submenu'),
                                            'items'=>array(
                                                array('label'=>'<i class="fa fa-caret-right"></i> Data LPK', 'url'=>array('/bpjsVclaim/admin')),
                                                array('label'=>'<i class="fa fa-caret-right"></i> Input LPK', 'url'=>array('/bpjsVclaim/create')),
                                            ),),
                                        
                                    ),
                                ),
                                 array('label'=>'<i class="fa fa-caret-right"></i> Aplicare <b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'items'=>array(
                                        array('label'=>'<i class="fa fa-caret-right"></i> Ketersediaan TT', 'url'=>array('/bpjsAplicare/admin')),
                                        array('label'=>'<i class="fa fa-caret-right"></i> Input TT', 'url'=>array('/bpjsAplicare/create')),
                                    ),
                                ),

                                ),
                             'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_RM,WebUser::R_OP_KASIR))
                        ),
                        array('label'=>'<i class="menu-icon fa fa-newspaper-o"></i> Laporan <i class="caret"></i>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                            'items'=>array(
                                // array('label'=>'Laporan Rawat Inap', 'url'=>array('/trRawatInap/laporan')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Data Rawat <b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR)),
                                    'items'=>array(
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Rawat Jalan', 
                                            'url'=>array('/trPendaftaranRjalan/laporan'),

                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Rawat Inap', 
                                            'url'=>array('/laporan/rawatInap')
                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> IRD', 
                                            'url'=>array('/laporan/ird')
                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Kunjungan', 
                                            'url'=>array('/laporan/kunjungan')
                                        ),
                                    )    ,
                                ),
                                array('label'=>'<i class="fa fa-caret-right"></i> Kamar', 
                                    'url'=>array('/laporan/kamar'),

                                ),
                                array('label'=> '','itemOptions'=>array('class'=>'divider'),'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR))),
                                
                                array('label'=>'<i class="fa fa-caret-right"></i> Gizi', 
                                    'url'=>array('/laporan/gizi'),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR))
                                ),
                                array('label'=> '','itemOptions'=>array('class'=>'divider')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Patologi <b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR)),
                                    'items'=>array(
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Patologi Klinik', 
                                            'url'=>array('/laporan/patologiKlinik')
                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Patologi Anatomi', 
                                            'url'=>array('/laporan/patologiAnatomi')
                                        ),
                                    )    ,
                                ),
                                array('label'=>'<i class="fa fa-caret-right"></i> Laboratorium<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR, WebUser::R_OP_RADIOLOGI)),
                                    'items'=>array(
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Radiologi', 
                                            'url'=>array('/laporan/radiologi')
                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Hemodialisis', 
                                            'url'=>array('/laporan/hd')
                                        ),
                                    )    ,
                                ),

                                array('label'=> '','itemOptions'=>array('class'=>'divider')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Operasi<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR, WebUser::R_OP_OPERASI)),
                                    'items'=>
                                    [ 
                                        [
                                            'label'=>'<i class="fa fa-caret-right"></i> Data OK', 
                                            'url'=>['/tdOkBiaya/export'],
                                            
                                        ],
                                        [
                                            'label'=>'<i class="fa fa-caret-right"></i> Rekap Jumlah Tindakan', 
                                            'url'=>['/laporan/okRekap'],
                                        ],
                                        [
                                            'label'=>'<i class="fa fa-caret-right"></i> Data OK Rekap Kasir', 
                                            'url'=>['/laporan/operasi'],
                                            
                                        ],
                                        [
                                            'label'=>'<i class="fa fa-caret-right"></i> CSSD', 
                                            'url'=>['/laporan/cssd']
                                        ],
                                    ],
                                ),
                                
                                array('label'=> '','itemOptions'=>array('class'=>'divider')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Jaspel Px', 'url'=>array('/laporan/dokterPerKamar')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Dokter<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR, WebUser::R_OP_KEUANGAN)),
                                    'items'=>array(
                                      
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Per Dokter', 
                                            'url'=>array('/laporan/dokter')
                                        ),
                                         
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Jasa Dokter', 
                                            'url'=>array('/laporan/jasaDokter')
                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Rekap Jasa Dokter', 
                                            'url'=>array('/laporan/rekapDokter')
                                        ),
                                    ),
                                ),
                                array('label'=>'<i class="fa fa-caret-right"></i> Perawat<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'items'=>array(
                                       
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> RR', 
                                            'url'=>array('/laporan/rr')
                                        ),
                                        // array(
                                        //     'label'=>'<i class="fa fa-caret-right"></i> Rekap Japel Ruang', 
                                        //     'url'=>array('/laporan/rekapKamar')
                                        // ),
                                    )    ,
                                ),
                               array('label'=> '','itemOptions'=>array('class'=>'divider')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Obat & Alkes<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                    'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR,WebUser::R_OP_FARMASI)),
                                    'items'=>array(
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Obat', 
                                            'url'=>array('/laporan/obat')
                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Alkes', 
                                            'url'=>array('/laporan/alkes')
                                        ),
                                    )    ,
                                ),
                              

                            ),
                            'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KEUANGAN,WebUser::R_OP_KASIR,WebUser::R_KA_KASIR,WebUser::R_OP_KAMAR))
                        ),
                            array('label'=>'<i class="menu-icon fa fa-users"></i> Data Master <i class="caret"></i>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown-toggle','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                        'items'=>array(
                            [
                                'label'=>'<i class="fa fa-caret-right"></i> Rekam Medik<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                'linkOptions'=>['class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'],
                                 'itemOptions'=>['class'=>'dropdown-submenu'],
                                    'items'=>[
                                        ['label'=>'<i class="fa fa-caret-right"></i> Pasien', 'url'=>['pasien/index']],
                                        ['label'=>'<i class="fa fa-caret-right"></i> Status RM', 'url'=>['refstatusrm/index']],
                                        ['label'=>'<i class="fa fa-caret-right"></i> DTD', 'url'=>['Dtd/index']],
                                        ['label'=>'<i class="fa fa-caret-right"></i> ICD-10', 'url'=>['Icd/index']],
                                        ['label'=>'<i class="fa fa-caret-right"></i> ICD-9 CM', 'url'=>['ProcedureIcd/index']],
                                    ]    ,
                            ],
                            
                            array('label'=>'<i class="fa fa-caret-right"></i> Pegawai', 'url'=>array('pegawai/index')),
                            array('label'=>'<i class="fa fa-caret-right"></i> Agama', 'url'=>array('DmAgama/index')),
                            array('label'=> '','itemOptions'=>array('class'=>'divider')),
                            array('label'=>'<i class="fa fa-caret-right"></i> Kelas', 'url'=>array('DmKelas/index')),
                            array(
                                'label'=>'<i class="fa fa-caret-right"></i> Kamar<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                 'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'items'=>array(
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Data', 
                                            'url'=>array('/DmKamarMaster/admin')
                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Kelas Kamar', 
                                            'url'=>array('/DmKamar/admin')
                                        ),
                                    )    ,
                            ),
                            array('label'=> '','itemOptions'=>array('class'=>'divider')),
                            array('label'=>'<i class="fa fa-caret-right"></i> Dokter', 'url'=>array('DmDokter/index')),
                            array('label'=>'<i class="fa fa-caret-right"></i> Pelayanan Medik', 'url'=>array('jenisVisite/index')),
                            // array('label'=>'<i class="fa fa-caret-right"></i> Poli', 'url'=>array('poli/index')),
                            [
                                'label'=>'<i class="fa fa-caret-right"></i> Unit', 
                                'url'=>['unit/all'],
                                
                            ],
                            array(
                                'label'=>'<i class="fa fa-caret-right"></i> Poli<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                 'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'items'=>array(
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Data', 
                                            'url'=>array('/unit/admin')
                                        ),
                                        array(
                                            'label'=>'<i class="fa fa-caret-right"></i> Tindakan', 
                                            'url'=>array('/unitTindakan/index')
                                        ),
                                        // array(
                                        //     'label'=>'<i class="fa fa-caret-right"></i> Biaya Tindakan', 
                                        //     'url'=>array('/dmPoliTindakan/admin')
                                        // ),
                                    )    ,
                            ),
                             array('label'=> '','itemOptions'=>array('class'=>'divider')),
                            array('label'=>'<i class="fa fa-caret-right"></i> Tindakan<b class="arrow fa fa-angle-down"></b>', 'url'=>'#',
                                'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",'role' =>'button'),
                                    'itemOptions'=>array('class'=>'dropdown-submenu'),
                                    'items'=>array(
                                ['label'=>'<i class="fa fa-caret-right"></i> Tindakan Poli', 'url'=>['DmTindakan/index']],
                                ['label'=> '','itemOptions'=>['class'=>'divider']],
                                array('label'=>'<i class="fa fa-caret-right"></i> Tindakan Medis Lain', 'url'=>array('TindakanMedisLain/index')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Tindakan Medis Non Operatif', 'url'=>array('TindakanMedisNonOperatif/index')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Tindakan Medis Operatif', 'url'=>array('TindakanMedisOperatif/index')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Tindakan Medis Penunjang', 'url'=>array('TindakanMedisPenunjang/index')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Tindakan Rawat Darurat', 'url'=>array('TindakanRawatDarurat/index')),
                                array('label'=> '','itemOptions'=>array('class'=>'divider')),
                                  array('label'=>'<i class="fa fa-caret-right"></i> Jenis Tindakan OK', 'url'=>array('dmOkJenisTindakan/index')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Golongan Operasi', 'url'=>array('dmOkGolOperasi/index')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Tindakan OK', 'url'=>array('dmOkTindakan/index')),
                                array('label'=>'<i class="fa fa-caret-right"></i> Tindakan Anastesi', 'url'=>array('dmOkAnastesi/index')),
                                        )    ,
                                ),
                             array('label'=> '','itemOptions'=>array('class'=>'divider')),
                            array('label'=>'<i class="fa fa-caret-right"></i> Golongan Pasien', 'url'=>array('GolPasien/index')),
                            array('label'=>'<i class="fa fa-caret-right"></i> Obat', 'url'=>array('/MObatAkhp/index')),
          //                   array('label'=>'Master Kalender', 'url'=>array('DmKalender/index')),
                            array('label'=>'<i class="fa fa-caret-right"></i> User', 'url'=>array('user/index')),
                            ['label'=> '','itemOptions'=>['class'=>'divider']],
                            ['label'=>'<i class="fa fa-caret-right"></i> Setting', 'url'=>['setting/index']],
                        ),
                            'visible'=>Yii::app()->user->checkAccess(array(WebUser::R_SA))

                        ),
                    ),
                )); 
            ?>
            
            </div>