<?php
/* @var $this DmKamarMasterController */
/* @var $data DmKamarMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->kode_kamar); ?>
	<br />


</div>