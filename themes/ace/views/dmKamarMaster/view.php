<?php
/* @var $this DmKamarMasterController */
/* @var $model DmKamarMaster */

$this->breadcrumbs=array(
	array('name'=>'Dm Kamar Master','url'=>array('admin')),
	array('name'=>'Dm Kamar Master'),
);

$this->menu=array(
	array('label'=>'List DmKamarMaster', 'url'=>array('index')),
	array('label'=>'Create DmKamarMaster', 'url'=>array('create')),
	array('label'=>'Update DmKamarMaster', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmKamarMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmKamarMaster', 'url'=>array('admin')),
);
?>

<h1>View DmKamarMaster #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama_kamar',
		'kode_kamar',
	),
)); ?>
	</div>
</div>
