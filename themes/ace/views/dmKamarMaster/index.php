<?php
/* @var $this DmKamarMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Dm Kamar Masters'),
);

$this->menu=array(
	array('label'=>'Create DmKamarMaster', 'url'=>array('create')),
	array('label'=>'Manage DmKamarMaster', 'url'=>array('admin')),
);
?>

<h1>Dm Kamar Masters</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
