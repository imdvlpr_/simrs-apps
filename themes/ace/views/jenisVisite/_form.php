<?php
/* @var $this JenisVisiteController */
/* @var $model JenisVisite */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jenis-visite-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_tingkat', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php
	$listUser = CHtml::listData(DmKelas::model()->findAll(),'id_kelas','nama_kelas');

			echo $form->dropDownList($model,'kelas_tingkat',$listUser); 
		?>
		<?php echo $form->error($model,'kelas_tingkat'); ?>
		</div>
	</div>
<div class="form-group">
		<?php echo $form->labelEx($model,'kode_visite', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'kode_visite',array('VISITE'=>'VISITE','VISITE IRD' => 'VISITE IRD','KONSUL'=>'KONSUL','KONSUL IRD'=>'KONSUL IRD')); 
		// echo $form->textField($model,'kode_visite',array('size'=>10,'maxlength'=>10)); 
		?>
		<?php echo $form->error($model,'kode_visite'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_visite', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_visite',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama_visite'); ?>
		</div>
	</div>

	



	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'biaya'); ?>
		<?php echo $form->error($model,'biaya'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'is_deleted', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'is_deleted'); ?>
		<?php echo $form->error($model,'is_deleted'); ?>
		</div>
	</div>


	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
