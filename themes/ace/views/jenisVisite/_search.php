<?php
/* @var $this JenisVisiteController */
/* @var $model JenisVisite */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_jenis_visite'); ?>
		<?php echo $form->textField($model,'id_jenis_visite'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_visite'); ?>
		<?php echo $form->textField($model,'nama_visite',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_visite'); ?>
		<?php echo $form->textField($model,'kode_visite',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kelas_tingkat'); ?>
		<?php echo $form->textField($model,'kelas_tingkat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'biaya'); ?>
		<?php echo $form->textField($model,'biaya'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->