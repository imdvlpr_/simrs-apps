<?php
/* @var $this BpjsSepController */
/* @var $data BpjsSep */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoSEP')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->NoSEP), array('view', 'id'=>$data->NoSEP)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JENIS_RAWAT')); ?>:</b>
	<?php echo CHtml::encode($data->JENIS_RAWAT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KELAS_RAWAT')); ?>:</b>
	<?php echo CHtml::encode($data->KELAS_RAWAT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NO_RM')); ?>:</b>
	<?php echo CHtml::encode($data->NO_RM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NO_RUJUKAN')); ?>:</b>
	<?php echo CHtml::encode($data->NO_RUJUKAN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ASAL_RUJUKAN')); ?>:</b>
	<?php echo CHtml::encode($data->ASAL_RUJUKAN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TGL_RUJUKAN')); ?>:</b>
	<?php echo CHtml::encode($data->TGL_RUJUKAN); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TGL_SEP')); ?>:</b>
	<?php echo CHtml::encode($data->TGL_SEP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DIAG_AWAL')); ?>:</b>
	<?php echo CHtml::encode($data->DIAG_AWAL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('POLI_TUJUAN')); ?>:</b>
	<?php echo CHtml::encode($data->POLI_TUJUAN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CATATAN')); ?>:</b>
	<?php echo CHtml::encode($data->CATATAN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LAKA_LANTAS')); ?>:</b>
	<?php echo CHtml::encode($data->LAKA_LANTAS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CREATED')); ?>:</b>
	<?php echo CHtml::encode($data->CREATED); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lokasi_laka')); ?>:</b>
	<?php echo CHtml::encode($data->lokasi_laka); ?>
	<br />

	*/ ?>

</div>