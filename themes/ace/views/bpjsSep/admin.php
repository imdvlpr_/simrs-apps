<?php
/* @var $this BpjsSepController */
/* @var $model BpjsSep */
$this->breadcrumbs=array(
    array('name' => 'BpjsSep','url'=>Yii::app()->createUrl('BpjsSep/index')),
     array('name' => 'List')
);
?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#bpjs-sep-grid').yiiGridView.update('bpjs-sep-grid', {
			    url:'BpjsSep/admin&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">

            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>	<div class="pull-left">
<h4>Manage Bpjs Sep</h4>
</div>

                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('BpjsSep[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('BpjsSep[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                   

<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'bpjs-sep-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		
		array(
			'header'=>'No Reg.',
			'value' => '$data->nORM->NoMedrec'
		),
		array(
			'header'=>'Pasien',
			'value' => '$data->nORM->NAMA'
		),
		'NoSEP',
		array(
			'header'=>'Jenis Rawat',
			'value' => '$data->jENISRAWAT->NAMA_JENIS_RAWAT'
		),

		'NAMA_KELAS_RAWAT',
		
		'NO_RUJUKAN',
		
		

		/*
		'TGL_RUJUKAN',
		'TGL_SEP',
		'DIAG_AWAL',
		'POLI_TUJUAN',
		'CATATAN',
		'LAKA_LANTAS',
		'CREATED',
		*/
		array(
	      'class'=>'CButtonColumn',
	      'template'=>'{print} {view} {update} {delete}',
	      'buttons' => array(
	      	 'print' => array(
	              'url'=>'Yii::app()->createUrl("bpjs/printSep/", array("id"=>$data->NoSEP))',   
	              'label'=>'<i class="icon-print"></i>',
	              'options' => array('title'=>'Cetak SEP','target'=>'_blank'),      
	            ),
	      )
	    ),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									  'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									     'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


                    </div>
                </div>
