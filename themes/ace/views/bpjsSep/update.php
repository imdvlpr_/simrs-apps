<div class="row-fluid">
	<div class="navbar">
		<div class="navbar-inner">
			<?php
			$this->widget('application.components.BreadCrumb', array(
				'links'=>array(
					array('name'=>'Sep','url'=>Yii::app()->createUrl('BpjsSep/admin')),
					array('name'=>'Update SEP')
				),
				'delimiter'=>'&raquo;'
			));
			?>
		</div>
	</div>
</div>

<?php $this->renderPartial('_form', array(
	'model'=>$model,
	'pasien' => $pasien,
	'bpjsPasien' => $bpjsPasien,
	'bpjsData' => $bpjsData,
	'bpjsSep' => $bpjsSep,
	'riwayatTerakhir' => $riwayatTerakhir,
	'jenisRawat' => $model->JENIS_RAWAT
	)); ?>	