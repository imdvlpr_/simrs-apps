<?php
/* @var $this DmDokterController */
/* @var $model DmDokter */
$this->pageTitle=Yii::app()->name . ' - Forms';
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
?>

<h1>View BpjsSep #<?php echo $model->NoSEP; ?></h1>

<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>

<div class="row-fluid">
	<div id="content" class="span12">
		<div class="row-fluid">
			<div class="navbar">
				<div class="navbar-inner">
					<?php
					$this->widget('application.components.BreadCrumb', array(
						'links'=>array(
							array('name'=>'Sep','url'=>Yii::app()->createUrl('BpjsSep/admin')),
							array('name'=>'List')
						),
						'delimiter'=>'&raquo;'
					));
					?>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="block">
				<div class="navbar navbar-inner block-header">
					<div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Detil SEP</div>
				</div>
				<div class="block-content collapse in">
					<div class="span12">
						
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'NoSEP',
		'JENIS_RAWAT',
		'KELAS_RAWAT',
		'NO_RM',
		'NO_RUJUKAN',
		'kdProvider',
		'nmProvider',
		'TGL_RUJUKAN',
		'TGL_SEP',
		'DIAG_AWAL',
		'namaDiagnosa',
		'POLI_TUJUAN',
		'nmPoli',
		'CATATAN',
		'LAKA_LANTAS',
		'CREATED',
		'lokasi_laka',
	),
)); ?>


					</div>
				</div>

			</div>
		</div>
		<div class="row-fluid">
			<div class="block">
				<div class="navbar navbar-inner block-header">
					<div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs.png"/> Detil SEP dari data BPJS</div>
				</div>
				<div class="block-content collapse in">
					<?php 
					if($hasil->metadata->code == '200')
					{
						$ds = $hasil->response;
					?>
					<table class="table table-condensed">
						<tr>
							<th>No. Rujukan</th>
							<td>: <?=$ds->noRujukan;?></td>
							<th>No. SEP</th>
							<td>: <?=$ds->noSep;?></td>
						</tr>
						<tr>
							<th>Tgl. Rujukan</th>
							<td>: <?=$ds->tglRujukan;?></td>
							<th>Tgl. SEP</th>
							<td>: <?=$ds->tglSep;?></td>
						</tr>
						<tr>
							<th>Tagihan</th>
							<td>: <?=$ds->byTagihan;?></td>
							<th>Catatan</th>
							<td>: <?=$ds->catatan;?></td>
						</tr>
						<tr>
							<th>Kd. Diag Awal</th>
							<td>: <?=$ds->diagAwal->kdDiag;?></td>
							<th>Nm. Diag Awal</th>
							<td>: <?=$ds->diagAwal->nmDiag;?></td>
						</tr>
						<tr>
							<th>Jns Pelayanan</th>
							<td>: <?=$ds->jnsPelayanan;?></td>
							<th>Kls Rawat</th>
							<td>: <?=$ds->klsRawat->nmKelas;?></td>
						</tr>
						<tr>
							<th>Laka Lantas</th>
							<td>: <?=$ds->lakaLantas->keterangan;?></td>
							<th>Status</th>
							<td>: <?=$ds->lakaLantas->status;?></td>
						</tr>
						<tr>
							<th>Tgl Pulang</th>
							<td>: <?=$ds->tglPulang;?></td>
							<th></th>
							<td></td>
						</tr>
					</table>
					
					<?php 
					}

					else
					{
						echo '<div class="alert alert-error">' . $hasil->metadata->message . '</div>';
					}	
					?>
				</div>
				
			</div>
		</div>
		<div class="row-fluid">
			<div class="block">
				<div class="navbar navbar-inner block-header">
					<div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs.png"/> Data Kunjungan Peserta SEP dari data BPJS</div>
				</div>
				<div class="block-content collapse in">
					<span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
					<div style="display:none" class="alert alert-error" id="bpjs_error"></div>
<div style="display:none" class="alert alert-info" id="bpjs_info"></div>

					<table class="table table-condensed" id="table-data-kunjungan">
						<tr>
							<th>Kd Inacbg</th>
							<td>: <span id="kdInacbg"></span></td>
							<th>Nm Inacbg</th>
							<td>: <span id="nmInacbg"></span></td>
						</tr>
						<tr>
							<th>Tgl. Pulang</th>
							<td>: <span id="tglPulang"></span></td>
							<th>Tgl. SEP</th>
							<td>: <span id="tglSep"></span></td>
						</tr>
						<tr>
							<th>Tagihan</th>
							<td>: <span id="byTagihan"></span></td>
							<th>Tarif RS</th>
							<td>: <span id="byTarifRS"></span></td>
						</tr>
						<tr>
							<th>Tarif Gruper</th>
							<td>: <span id="byTarifGruper"></span></td>
							<th>Topup</th>
							<td>: <span id="byTopup"></span></td>
						</tr>
						<tr>
							<th>Jns Pelayanan</th>
							<td>: <span id="jnsPelayanan"></span></td>
							<th>No MR</th>
							<td>: <span id="noMR"></span></td>
						</tr>
						<tr>
							<th>Nm Pst</th>
							<td>: <span id="nama"></span></td>
							<th>No Kartu</th>
							<td>: <span id="noKartu"></span></td>
						</tr>
						<tr>
							<th>Kd Stat Sep</th>
							<td>: <span id="kdStatSep"></span></td>
							<th>Nm Stat Sep</th>
							<td>: <span id="nmStatSep"></span></td>
						</tr>
						
					</table>
					
				</div>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
function dataKunjungan() {
  $.ajax({
      type : 'POST',
      url  : '<?php echo Yii::app()->createUrl("WSBpjs/getDataKunjungan");?>',
      data : 'noSep=<?=$model->NoSEP;?>',
      beforeSend : function(){
        $('#loading1').show();
      },

      error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
      },
      timeout : 3000,
      async: false,
      success : function(data){
          
          $('#loading1').hide();
          
          // console.log(data);

          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200) {
              $('#bpjs_info').hide();
              
              $.each(output.response.list, function(i, item) {
                  
                 $('#kdInacbg').html(item.kdInacbg);
                 $('#nmInacbg').html(item.nmInacbg);
              	 $('#byTagihan').html(item.byTagihan);
              	 $('#byTarifGruper').html(item.byTarifGruper);
              	 $('#byTopup').html(item.byTopup);
              	 $('#byTarifRS').html(item.byTarifRS);
              	 $('#jnsPelayanan').html(item.jnsPelayanan);
              	 $('#noMR').html(item.noMR);
              	 $('#nama').html(item.peserta.nama);
              	 $('#noKartu').html(item.peserta.noKartu);	
            	 $('#tglPulang').html(item.tglPulang);
              	 $('#tglSep').html(item.tglSep);	
              	 $('#kdStatSep').html(item.statSep.kdStatSep);
              	 $('#nmStatSep').html(item.statSep.nmStatSep);	
              });
             
          }

          else {
             $('#bpjs_info').show();
              $('#bpjs_info').html("<strong>Info !</strong> " + output.metadata.message);

          }
      },
  });
}

$(document).ready(function(){
	dataKunjungan();
});
</script>