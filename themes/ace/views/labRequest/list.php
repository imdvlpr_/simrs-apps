 <?php
$this->breadcrumbs=array(
	array('name'=>'Lab Request','url'=>['list']),
	array('name'=>'Manage'),
);

?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#lab-request-grid').yiiGridView.update('lab-request-grid', {
			    url:'<?php echo Yii::app()->createUrl("LabRequest/list"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="pull-left"> <h4>Manage Lab Request</h4></div>
<div class="pull-right">Data per halaman
  <?= CHtml::dropDownList('LabRequest[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',[10=>10,50=>50,100=>100,],['id'=>'size']); 
?> 
 <?php        echo CHtml::textField('LabRequest[SEARCH]','',['placeholder'=>'Cari','id'=>'search']);?>
  	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'lab-request-grid',
	'dataProvider'=>$model->searchAll(),
	
	'columns'=>[
	[
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
	],
		'pasien.NAMA',
		'pasien.NoMedrec',
		'unit.NamaUnit',
		'reg_id',
		'tanggal',
		[
			'header' => 'Status Pelayanan',
			'type' => 'raw',
			'value' => function($data){
				$st = $data->status_pelayanan;

				switch ($st) {
					case 1:
						return '<span class="label-lg label label-success arrowed">Sudah Dilayani</span>';
						break;
					
					default:
						return '<span class="label-lg label label-danger arrowed-in">Belum Dilayani</span>';
						break;
				}
			}
		],
		/*
		'created_at',
		'updated_at',
		*/
		[
			'class'=>'CButtonColumn',
			'template' => '{update} {delete}',
			'buttons' => array(
				
				// 'printPengantar' => array(
				// 	'url'=>'Yii::app()->createUrl("labRequest/cetakHasil/", array("id"=>$data->id))',   
				// 	'label'=>'<i class="fa fa-print"></i>',
				// 	'options' => array('title'=>'Cetak Hasil','class'=>'print'),      
	   //          ),
				'view' => array(
					'url'=>'Yii::app()->createUrl("labRequest/view/", ["id"=>$data->id])',   
					// 'label'=>'<i class="fa fa-magnifier"></i>',
					'options' => array('title'=>'View','class'=>'view'),      
	            ),
				

			),
		],
	],
	'htmlOptions'=>[
		'class'=>'table'
	],
	'pager'=>[
		'class'=>'SimplePager',
		'header'=>'',
		'firstPageLabel'=>'Pertama',
		'prevPageLabel'=>'Sebelumnya',
		'nextPageLabel'=>'Selanjutnya',
		'lastPageLabel'=>'Terakhir',
		'firstPageCssClass'=>'btn btn-info',
		'previousPageCssClass'=>'btn btn-info',
		'nextPageCssClass'=>'btn btn-info',
		'lastPageCssClass'=>'btn btn-info',
		'hiddenPageCssClass'=>'disabled',
		'internalPageCssClass'=>'btn btn-info',
		'selectedPageCssClass'=>'btn btn-sky',
		'maxButtonCount'=>5
	],
	'itemsCssClass'=>'table  table-bordered table-hover',
	'summaryCssClass'=>'table-message-info',
	'filterCssClass'=>'filter',
	'summaryText'=>'menampilkan {start} - {end} dari {count} data',
	'template'=>'{items}{summary}{pager}',
	'emptyText'=>'Data tidak ditemukan',
	'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

<script type="text/javascript">
	
jQuery(function($) {
	ajaxLoadNotif();
});
</script>