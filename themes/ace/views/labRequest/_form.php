<?php
/* @var $this LabRequestController */
/* @var $model LabRequest */
/* @var $form CActiveForm */
?>

 <?php
$this->breadcrumbs=array(
	array('name'=>'Lab Request','url'=>array('labRequest/index')),
	array('name'=>'Create'),
);

?>

<style type="text/css">
	/* RADIOS & CHECKBOXES STYLES */
/* SOURCE: https://gist.github.com/dciccale/1367918 */
/* No more blue blur border */
input {
  outline: none;
}

/* base styles */
input[type="checkbox"] {
    height: 20px;
    width: 20px;
    vertical-align: middle;
    margin: 0 0.4em 0.4em 0;
    background: rgba(255, 255, 255, 1);
    border: 1px solid #AAAAAA;
    -webkit-appearance: none;
}

/* border radius for radio*/


/* border radius for checkbox */
input[type="checkbox"] {
    border-radius: 2px;
}

/* hover state */
input[type="checkbox"]:not(:disabled):hover {
    border: 1px solid rgba(58, 197, 201, 1);
}

/* active state */
input[type="checkbox"]:active:not(:disabled) {
    border: 1px solid rgba(58, 197, 201, 1);
}

/* input checked border color */

input[type="checkbox"]:checked {
    border: 1px solid rgba(58, 197, 201, 1);
}

input[type="checkbox"]:checked:not(:disabled) {
    background: rgba(58, 197, 201, 1);
}


/* checkbox checked */
input[type="checkbox"]:checked:before {
    font-weight: bold;
    color: white;
    content: '\2713';
    margin-left: 2px;
    font-size: 14px;
}


</style>
<h1>Permohonan Cek Lab</h1>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lab-request-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>
<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>
<div class="row">
<div class="col-lg-6 col-xs-12">
	<div class="profile-user-info profile-user-info-striped ">
		<div class="profile-info-row">
			<div class="profile-info-name"> Nama / Reg</div>

			<div class="profile-info-value">
				<span class="editable" id="username"><strong>
					<?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?> / <?php echo $pasien->NoMedrec;?>
			<?php echo $form->hiddenField($model,'pasien_id'); ?>
			<?php echo $form->hiddenField($model,'unit_id'); ?>
			<?php echo $form->hiddenField($model,'reg_id'); ?>	
			<?php echo $form->hiddenField($model,'kelas_id'); ?>
			<?php echo $form->hiddenField($model,'rawat_inap_id'); ?>			
					</strong></span>
			</div>
		</div>

		
		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl Masuk </div>

			<div class="profile-info-value">
				<?php 
	     
	     echo $rawatInap->tanggal_masuk;?>
	     <?php
	     echo is_null($rawatInap->tanggal_masuk_ird) ? ' / '.$rawatInap->tanggal_masuk_ird : ''; 
	     // echo $rawatInap->tanggal_masuk_ird;
	     ?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name"> Dirawat di </div>

			<div class="profile-info-value">
				<?php 

	     echo $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;

	     ?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl Permohonan </div>

			<div class="profile-info-value">
				<?php 
		         $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'tanggal',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        // 'top' => '100'
                        
                    ),
                    

        ));
   
		 ?>&nbsp;<i class="fa fa-calendar"></i>
		<?php echo $form->error($model,'tanggal'); ?>
			</div>
		</div>
	</div>
</div>
<div class="col-lg-6 col-xs-12">
	<div class="profile-user-info profile-user-info-striped ">
		

		

		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl Keluar </div>

			<div class="profile-info-value">
				<?= $rawatInap->datetime_keluar ?:  '-'; ?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name">Lama dirawat </div>

			<div class="profile-info-value">
				<?php 

        $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
           $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }
        else
        {
          $dnow = date('Y-m-d');

          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }
		 echo $selisih_hari.' hari';

        ?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name"> DPJP </div>

			<div class="profile-info-value">
				 <?php 
    $nama_dokter = '';

    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 

      ?><?php echo $nama_dokter;?>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Alamat </div>

			<div class="profile-info-value">
				<?php echo $pasien->FULLADDRESS;?>
			</div>
		</div>
		
		
	</div>
</div>
 </div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box widget-color-red2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Item Uji Lab</h4>
        </div>
        <div class="widget-body">
          <div class="widget-main">
   			<?php $index = 0;?>
   			<div class="form-group">
   			<?php 
   			foreach($listGrup as $key => $grup):
   			?>
   			
			<?php 
			if($key >= 0 && $key <=6){
				?>
				<div class="col-sm-4">
					<h4><?=$grup->nama;?></h4>
				<?php
				foreach($grup->dmLabItems as $q => $item){
			?>
		   		<div class="col-sm-12">
		   			<label>
				<input type="checkbox" name="item_id[]" value="<?=$item->id;?>"/>
				<strong><?=$item->nama;?></strong>
			</label>
				</div>
			<?php
				}
				?>
				</div>
				<?php
			}

			else if($key > 6 && $key <=12){
				?>
				<div class="col-sm-4">
					<h4><?=$grup->nama;?></h4>
				<?php
				foreach($grup->dmLabItems as $q => $item){
			?>
		   		<div class="col-sm-12">
		   			<label>
				<input type="checkbox" name="item_id[]>" value="<?=$item->id;?>"/>
				<strong><?=$item->nama;?></strong>
			</label>
				</div>
			<?php
				}
				?></div><?php
			}
			else
			{
				?>
				<div class="col-sm-4">
					<h4><?=$grup->nama;?></h4>
				<?php
				foreach($grup->dmLabItems as $q => $item){
			?>
				<div class="col-sm-12">
				<label>
				<input type="checkbox" name="item_id[]" value="<?=$item->id;?>"/>
				<strong><?=$item->nama;?></strong>
				</label>
				</div>
			<?php
				}
				?></div><?php
			}
			 ?>
			<?php 
		endforeach;
			?>
			</div>
          </div>
      </div>
    </div>
</div>
</div>
	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" id="btn-submit" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>

<script type="text/javascript">
$(document).ready(function(){
	$('#btn-submit').click(function(e){
		e.preventDefault();

		var dataPost = $('#lab-request-form').serialize();

		$.ajax({
			url : '<?=Yii::app()->createUrl('labRequest/ajaxSimpan');?>',
			type : 'POST',
			data : {
				dataPost : dataPost
			},
			error : function(e){
				console.log(e.responseText);
			},
			beforeSend : function(){

			},
			success : function(hasil){
				var hasil = $.parseJSON(hasil);
				if(hasil.code == 200){
					alert(hasil.message);
				}

				else{
					alert(hasil.message);	
				}
			}
		});
	});
});

function refreshNumbering(){
	

	$('span.numbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});

}


$(document).on('keydown','input.item', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">Item <span class="numbering"></span></label>';
		row += '<div class="col-sm-8">';
		row += '<input name="item[]" class="item form-control"/>';
		row += '<input type="hidden" name="item_id[]" class="item_id"/>';
		row += '<label class="error_item"></label>';
		row += '</div>';
		row += '<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.item').last().focus();
	}
});

$(document).on('click','a.remove',function(e){
	e.preventDefault();
	
	$(this).parent().parent().remove();
	refreshNumbering();
});

$(document).bind("keyup.autocomplete",function(){

	$('.item').autocomplete({
      	minLength:1,
      	select:function(event, ui){
       
        	$(this).next().val(ui.item.id);
                
      	},
      
      	focus: function (event, ui) {
        	$(this).next().val(ui.item.id);
       
      	},
      	source:function(request, response) {
        	$.ajax({
                url: "<?php echo Yii::app()->createUrl('DmLabItem/ajaxGetItem');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
  	}); 
});

</script>