 <?php
$this->breadcrumbs=array(
	array('name'=>'Lab Request','url'=>['index']),
	array('name'=>'Manage'),
);

?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#lab-request-grid').yiiGridView.update('lab-request-grid', {
			    url:'<?php echo Yii::app()->createUrl("LabRequest/index"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                        <div class="pull-left"> <h4>Manage Lab Request</h4></div>
                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('LabRequest[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',[10=>10,50=>50,100=>100,],['id'=>'size']); 
                            ?> 
                             <?php        echo CHtml::textField('LabRequest[SEARCH]','',['placeholder'=>'Cari','id'=>'search']);
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'lab-request-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>[
	[
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
	],
		'pasien.NAMA',
		'pasien.NoMedrec',
		'unit.NamaUnit',
		[
			'header' =>'Kelas',
			'value' => function($data){
				return !empty($data->rawatInap) ? $data->rawatInap->kamar->kelas->nama_kelas : '';
			}
		],
		'reg_id',
		'tanggal',
		[
			'header' => 'Status Pelayanan',
			'type' => 'raw',
			'value' => function($data){
				$st = $data->status_pelayanan;

				switch ($st) {
					case 1:
						return '<span class="label label-success arrowed">Sudah Dilayani</span>';
						break;
					
					default:
						return '<span class="label label-danger arrowed-in">Belum Dilayani</span>';
						break;
				}
			}
		],
		/*
		'created_at',
		'updated_at',
		*/
		[
			'class'=>'CButtonColumn',
			'template' => '{view}',
			'buttons' => array(
				
				// 'printPengantar' => array(
				// 	'url'=>'Yii::app()->createUrl("BPendaftaranRjalanTindakan/cetakPengantar/", array("id"=>$data->id))',   
				// 	'label'=>'<i class="fa fa-print"></i>',
				// 	'options' => array('title'=>'Cetak Pengantar','class'=>'print'),      
	   //          ),
				'view' => array(
					'url'=>'Yii::app()->createUrl("labRequest/view/", ["id"=>$data->id])',   
					// 'label'=>'<i class="fa fa-magnifier"></i>',
					'options' => array('title'=>'View','class'=>'view'),      
	            ),
				

			),
		],
	],
	'htmlOptions'=>[
		'class'=>'table'
	],
	'pager'=>[
		'class'=>'SimplePager',
		'header'=>'',
		'firstPageLabel'=>'Pertama',
		'prevPageLabel'=>'Sebelumnya',
		'nextPageLabel'=>'Selanjutnya',
		'lastPageLabel'=>'Terakhir',
		'firstPageCssClass'=>'btn btn-info',
		'previousPageCssClass'=>'btn btn-info',
		'nextPageCssClass'=>'btn btn-info',
		'lastPageCssClass'=>'btn btn-info',
		'hiddenPageCssClass'=>'disabled',
		'internalPageCssClass'=>'btn btn-info',
		'selectedPageCssClass'=>'btn btn-sky',
		'maxButtonCount'=>5
	],
	'itemsCssClass'=>'table  table-bordered table-hover',
	'summaryCssClass'=>'table-message-info',
	'filterCssClass'=>'filter',
	'summaryText'=>'menampilkan {start} - {end} dari {count} data',
	'template'=>'{items}{summary}{pager}',
	'emptyText'=>'Data tidak ditemukan',
	'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>


<script type="text/javascript">
	
function popitup(url,label) {
    var w = screen.width * 0.8;
    var h = 800;
    var left = (screen.width - w) / 2;
    var top = (screen.height - h) / 2;
    
    window.open(url,label,'height='+h+',width='+w+',top='+top+',left='+left);
    
}

$(document).ready(function(){
	$('.view').click(function(e){
		e.preventDefault();
		var url = $(this).attr('href');
        popitup(url,'Rincian');
	});
});
</script>