<style type="text/css">
  td.border_atas{
    border-top-color:#000000;
    border-top-width:1px;
    border-top-style:solid;
  }

  td.border_bawah{
    border-bottom-color:#000000;
    border-bottom-width:1px;
    border-bottom-style:solid;
  }

  td.border_kiri{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:solid;
  }
  td.border_kanan{
    border-right-color:#000000;
    border-right-width:1px;
    border-right-style:solid;
  }

  td.border_kiri_dotted{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:dotted;
  }
</style>
<table width="100%" style="padding: 2px 3px">
   <tr >
     <td colspan="7" align="center">
       LABORATORIUM RSUD PARE
     </td>
     
   </tr>
 </table>
 <table width="100%">
   <tr>
     <td width="12%">Tanggal</td>
     <td width="3%">:</td>
     <td width="35%"><?=date('d/m/Y',strtotime($model->tanggal));?></td>
     
     <td width="22%">Dokter / Ruangan</td>
     <td width="3%">:</td>
     <td  width="25%"><?php
     $nama_dokter = !empty($rawatInap->dokter) ? $rawatInap->dokter->FULLNAME : '';
     echo $nama_dokter?> / <?=$rawatInap->kamar->nama_kamar;?></td>
   </tr>
   <tr>
     <td>Nama</td>
     <td>:</td>
     <td><?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?></td>
     <td>No Reg</td>
     <td>:</td>
     <td><?php echo $pasien->NoMedrec;?>    
     </td>
   </tr>
   <tr>
     <td>Umur</td>
     <td>:</td>
     <td><?php 
     if(!empty($reg))
     {
     	echo $reg->umurthn.' '.$pasien->umurbln;	
     }
     

     ?></td>
     <td>Jam diambil</td>
     <td>:</td>
     <td><?=date('H:i:s',strtotime($model->tanggal));?></td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td><?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Jam diserahkan</td>
     <td>:</td>
     <td><?=date('H:i:s');?></td>

   </tr>
 </table>



<table cellpadding="1" >
	<tr>
		<td colspan="4" class="border_atas" style="text-align: center">RINCIAN BIAYA</td>
		
	</tr>
	<tr>
		<td class="border_atas border_bawah" style="text-align: center;" width="30%">Pemeriksaan</td>
		<td class="border_atas border_kiri border_bawah" width="23.333%" style="text-align: center;" >BHP+JRS</td>
    <td class="border_atas border_kiri border_bawah" width="23.333%" style="text-align: center;" >JAPEL</td>
    <td class="border_atas border_kiri border_bawah" width="23.333%" style="text-align: center;" >JUMLAH</td>
	</tr>
	<?php 
  $total_bhp =0;
  $total_japel=0;
  $total_biaya = 0;
			foreach($model->labRequestItems as $q => $item)
			{

        $total_bhp += $item->bhp;
        $total_japel += $item->japel;
        $total_biaya += $item->biaya;
        

       ?>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=$item->parent->parent->nama;?></strong></td>
		<td class="border_kiri" style="text-align: right;" ><?=Yii::app()->helper->formatRupiah($item->bhp);?></td>
    <td class="border_kiri" style="text-align: right;" ><?=Yii::app()->helper->formatRupiah($item->japel);?></td>
    <td class="border_kiri" style="text-align: right;" ><?=Yii::app()->helper->formatRupiah($item->biaya);?></td>
	</tr>

	<?php 
		}
	?>	
  <tr>
    <td class="border_atas" >&nbsp;</td>
    <td class="border_kiri border_atas" style="text-align: right;" ><?=Yii::app()->helper->formatRupiah($total_bhp);?></td>
    <td class="border_kiri border_atas" style="text-align: right;" ><?=Yii::app()->helper->formatRupiah($total_japel);?></td>
    <td class="border_kiri border_atas" style="text-align: right;" ><?=Yii::app()->helper->formatRupiah($total_biaya);?></td>
  </tr>	
	<tr>
		<td colspan="4" class="border_atas"></td>
		
	</tr>
</table>

<table width="100%">
	<tr>
		<td style="text-align: center;">
			Petugas Pelaksana
			<br><br><br><br>
			.......................
		</td>
		<td style="text-align: center;">
			Ka. Ins. Laboratorium
			<br><br><br><br>
			<u>dr. Erwin Ichsan</u><br>
			NIP. 196411151990031011
		</td>
	</tr>
</table>