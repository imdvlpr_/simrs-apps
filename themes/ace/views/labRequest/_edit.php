<?php
/* @var $this LabRequestController */
/* @var $model LabRequest */
/* @var $form CActiveForm */
?>

 <?php
$this->breadcrumbs=array(
	array('name'=>'Lab Request','url'=>array('labRequest/list')),
	array('name'=>'Create'),
);

?>
<h1>Permohonan Cek Lab</h1>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lab-request-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>
<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>
<div class="row">
<div class="col-lg-6 col-xs-12">
	<div class="profile-user-info profile-user-info-striped ">
		<div class="profile-info-row">
			<div class="profile-info-name"> Nama / Reg</div>

			<div class="profile-info-value">
				<span class="editable" id="username"><h4>
<?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?> / <?php echo $pasien->NoMedrec;?>
<?php echo $form->hiddenField($model,'id'); ?>
<?php echo $form->hiddenField($model,'pasien_id'); ?>
<?php echo $form->hiddenField($model,'unit_id'); ?>
<?php echo $form->hiddenField($model,'reg_id'); ?>
<?php echo $form->hiddenField($model,'kelas_id'); ?>			
					</h4></span>
			</div>
		</div>

		
		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl Masuk </div>

			<div class="profile-info-value">
<?= $rawatInap->tanggal_masuk;?>
<?= is_null($rawatInap->tanggal_masuk_ird) ? ' / '.$rawatInap->tanggal_masuk_ird : ''; ?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name"> Dirawat di </div>

			<div class="profile-info-value">
				<?= $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name"> Alamat </div>

			<div class="profile-info-value">
				<?= $pasien->FULLADDRESS;?>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl Permohonan </div>

			<div class="profile-info-value">
				<?=$model->tanggal;?>
			</div>
		</div>
		
	</div>
</div>
<div class="col-lg-6 col-xs-12">
	<div class="profile-user-info profile-user-info-striped ">
		

		

		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl Keluar </div>

			<div class="profile-info-value">
				<?= $rawatInap->datetime_keluar ?:  '-'; ?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name">Lama dirawat </div>

			<div class="profile-info-value">
				<?php 

        $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
           $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }
        else
        {
          $dnow = date('Y-m-d');

          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }
		 echo $selisih_hari.' hari';

        ?>
			</div>
		</div>

		<div class="profile-info-row">
			<div class="profile-info-name"> DPJP </div>

			<div class="profile-info-value">
				 <?php 
    $nama_dokter = '';

    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 

      ?><?php echo $nama_dokter;?>
			</div>
		</div>
			<div class="profile-info-row">
			<div class="profile-info-name"> Tgl/Jam Terima Sampel </div>

			<div class="profile-info-value">
				<?php 
		         $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'tanggal_terima_sampel',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        // 'top' => '100'
                        
                    ),
                    

        ));
   
		 ?>&nbsp;<i class="fa fa-calendar"></i>
		<?php echo $form->error($model,'tanggal_terima_sampel'); ?>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Tgl/Jam Selesai Uji Lab </div>

			<div class="profile-info-value">
				<?php 
		         $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'tanggal_selesai_uji',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        // 'top' => '100'
                        
                    ),
                    

        ));
   
		 ?>&nbsp;<i class="fa fa-calendar"></i>
		<?php echo $form->error($model,'tanggal_selesai_uji'); ?>
			</div>
		</div>
	
	</div>
</div>
 </div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="widget-box widget-color-red2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Item Uji Lab</h4>
        </div>
        <div class="widget-body">
          <div class="widget-main">
   			<?php $index = 0;?>
   			
				<div class="form-group">
<div class="alert alert-success" id="message-alert" style="display: none">
	<i class="fa fa-check"></i> Data tersimpan
</div>

<?php 

$listRequestItem = [];

foreach($model->labRequestItems as $item)
{

?>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="20%">Item Pemeriksaan</th>
								<th width="20%">Hasil</th>
								<th width="20%">Nilai Normal</th>
								<th width="20%">Kondisi</th>
								<th width="20%">Metode</th>
							</tr>
						</thead>
						<tbody>
						<?php 

							$checker1 = [];
							$checker2 = [];

			   				echo '<tr><td colspan="5" style="text-align:center"><h4>'.$item->parent->parent->grup->nama.'</h4></td></tr>';
							// echo '<tr><td colspan="5"><h4>'.$item->parent->parent->nama.'</h4></td></tr>';
							echo '<input type="hidden" name="lab_request_item_id_'.$item->id.'" value="'.$item->id.'"/>';
							
							// $items = $childs_labRequestItems[$item->id]['items'];
							// $subitems = $childs_labRequestItems[$item->id]['subitems'];
							$items = $item->labRequestItemKomponens;
			   				foreach($items as $q => $child):
			   				
			   					// echo $q.' ##';
			   					$ch = $child;
			   					// $subch = $child['subitems'];

				   				$itemMapping = $ch->item;

				   				// print_r($ch);exit;

				   				$batas_bawah = 0;
								$batas_atas = 0;
								$batas_bawah_kritis = 0;
								$batas_atas_kritis = 0;
								if($pasien->JENSKEL == 'L'){
									$batas_bawah = $itemMapping->batas_bawah;
									$batas_atas = $itemMapping->batas_atas;
									$batas_bawah_kritis = $itemMapping->batas_bawah_kritis;
									$batas_atas_kritis = $itemMapping->batas_atas_kritis;
								}
								else if($pasien->JENSKEL == 'P')
								{
									$batas_bawah = $itemMapping->batas_bawah_p;
									$batas_atas = $itemMapping->batas_atas_p;
									$batas_bawah_kritis = $itemMapping->batas_bawah_kritis_p;
									$batas_atas_kritis = $itemMapping->batas_atas_kritis_p;
								}
								
								

				?>			
						<tr>
							<td>

								<input type="hidden" name="komponen_item_id_<?=$ch->id;?>" value="<?=$ch->id;?>"/>
								<h4><?=$ch->item->item->nama;?></h4>
								
							</td>
							<td>
								<?php 
								if(!empty($itemMapping->dropdown_value)){
								?>
								<select name="hasil_<?=$ch->id;?>"class="hasil input-lg col-xs-12">
									<?php 
									$items = $itemMapping->item->dmLabItemOptions;
									foreach($items as $it)
									{
										$selected = $it->nilai == $ch->hasil ? 'selected' : '';
										echo '<option '.$selected.' value="'.$it->nilai.'">'.$it->label.'</option>';
									}
									?>
								</select>
								<?php
								}

								else
								{
								?>
								<input type="text" name="hasil_<?=$ch->id;?>" value="<?=$ch->hasil ?: '';?>" class="hasil input-lg col-xs-12"/>
								<?php 
								}
								?>
							</td>
							<td>
								<h4>
									<?php 
									if(!empty($itemMapping->dropdown_value))
									{
										echo 'Negatif';
									}
									
									else
									{
										if($pasien->JENSKEL == 'L'){
											
										?>
										<?=$itemMapping->batas_bawah;?> - 
										<?=$itemMapping->batas_atas;?> 
										<?=$itemMapping->satuan;?>
										<?php 
										}
										else if($pasien->JENSKEL == 'P')
										{
											
										?>
										
										<?=$itemMapping->batas_bawah_p;?> - 
										<?=$itemMapping->batas_atas_p;?> 
										<?=$itemMapping->satuan;?>
										<?php 
										}
									}
									?>
							</h4></td>
							<td><h4>
<?php 


							$hasil = $ch->hasil;
							if(!empty($hasil))
							{
								if(!empty($itemMapping->batas_dropdown))
								{
								
									if($item->hasil < 0)
										echo '<span class="label label-xlg label-success btn-block arrowed"><strong>Normal</strong></span>';
									else
										echo '<span class="label label-xlg label-danger btn-block arrowed-in"><strong>** Kritis</strong></span>';	
								}
								else
								{
									if($hasil >= $batas_bawah && $hasil <= $batas_atas )
									{
										?>
										<span class="label label-xlg label-success btn-block arrowed"><strong>Normal</strong></span>
										<?php
									}

									else if($hasil <= $batas_bawah_kritis || $hasil >= $batas_atas_kritis )
									{
										?>
										<span class="label label-xlg label-danger btn-block arrowed-in"><strong>** Kritis</strong></span>
										<?php
									}

									else if($hasil > $batas_bawah_kritis && $hasil <= $batas_bawah)
									{
										?>
										<span class="label label-xlg label-warning btn-block arrowed-in"><strong>* Di bawah nilai normal</strong></span>
										<?php
									}
									else if($hasil > $batas_atas && $hasil <= $batas_atas_kritis)
									{
										?>
										<span class="label label-xlg label-warning btn-block arrowed-in"><strong>* Di atas nilai normal</strong></span>
										<?php
									}
								}
							}
								?>
							

							</h4>

						</td>
							<td><h4><?=$itemMapping->metode;?></h4></td>
						</tr>

						<?php 
						endforeach;
						?>		
						</tbody>
					</table>
					<?php 
}
					?>
				</div>

          </div>
      </div>
    </div>
</div>
	</div>
	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-success" id="btn-submit" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
         </button>

         <a class="btn btn-info" id="btn-print">
            <i class="ace-icon fa fa-print bigger-110"></i>
            Cetak Hasil
         </a>
         <a class="btn btn-info" id="btn-print-biaya">
            <i class="ace-icon fa fa-print bigger-110"></i>
            <i class="ace-icon fa fa-money bigger-110"></i>
            Cetak Rincian Biaya
         </a>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>

<script type="text/javascript">


function popitup(url,label) {
    var w = screen.width * 0.8;
    var h = 800;
    var left = (screen.width - w) / 2;
    var top = (screen.height - h) / 2;
    
    window.open(url,label,'height='+h+',width='+w+',top='+top+',left='+left);
    
}

$(document).ready(function(){
	$('#btn-submit').click(function(e){
		e.preventDefault();

		var dataPost = $('#lab-request-form').serialize();

		$.ajax({
			url : '<?=Yii::app()->createUrl('labRequest/ajaxSimpanHasil');?>',
			type : 'POST',
			data : {
				dataPost : dataPost
			},
			error : function(e){
				$('#message-alert').hide();
				console.log(e.responseText);
			},
			beforeSend : function(){
				$('#message-alert').hide();
			},
			success : function(hasil){
				var hasil = $.parseJSON(hasil);
				if(hasil.code == 200){
					$('#message-alert').show();
					alert(hasil.message);
					window.location = '<?=Yii::app()->createUrl('labRequest/update',['id'=>$model->id]);?>';
				}

				else{
					alert(hasil.message);	
				}
			}
		});
	});

	$('#btn-print').click(function(e){
		e.preventDefault();

		popitup('<?=Yii::app()->createUrl('labRequest/printKimiaKlinik',['id'=>$model->id]);?>','Hasil Lab');
	});

	$('#btn-print-biaya').click(function(e){
		e.preventDefault();

		popitup('<?=Yii::app()->createUrl('labRequest/printRincian',['id'=>$model->id]);?>','Rincian Lab');
	});
});

function refreshNumbering(){
	

	$('span.numbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});

}


$(document).on('keydown','input.item', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">Item <span class="numbering"></span></label>';
		row += '<div class="col-sm-8">';
		row += '<input name="item[]" class="item form-control"/>';
		row += '<input type="hidden" name="komponen_item_id[]" class="item_id"/>';
		row += '<label class="error_item"></label>';
		row += '</div>';
		row += '<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.item').last().focus();
	}
});

$(document).on('click','a.remove',function(e){
	e.preventDefault();
	
	$(this).parent().parent().remove();
	refreshNumbering();
});

$(document).bind("keyup.autocomplete",function(){

	$('.item').autocomplete({
      	minLength:1,
      	select:function(event, ui){
       
        	$(this).next().val(ui.item.id);
                
      	},
      
      	focus: function (event, ui) {
        	$(this).next().val(ui.item.id);
       
      	},
      	source:function(request, response) {
        	$.ajax({
                url: "<?php echo Yii::app()->createUrl('DmLabItem/ajaxGetItem');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
  	}); 
});

</script>