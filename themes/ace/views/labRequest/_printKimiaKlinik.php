<style type="text/css">
  td.border_atas{
    border-top-color:#000000;
    border-top-width:1px;
    border-top-style:solid;
  }

  td.border_bawah{
    border-bottom-color:#000000;
    border-bottom-width:1px;
    border-bottom-style:solid;
  }

  td.border_kiri{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:solid;
  }
  td.border_kanan{
    border-right-color:#000000;
    border-right-width:1px;
    border-right-style:solid;
  }

  td.border_kiri_dotted{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:dotted;
  }
</style>
<table width="100%" style="padding: 2px 3px">
   <tr >
     <td colspan="7" align="center">
       LABORATORIUM RSUD PARE
     </td>
     
   </tr>
 </table>
 <table width="100%">
   <tr>
     <td width="12%">Tanggal</td>
     <td width="3%">:</td>
     <td width="35%"><?=date('d/m/Y',strtotime($model->tanggal));?></td>
     
     <td width="22%">Dokter / Ruangan</td>
     <td width="3%">:</td>
     <td  width="25%"><?php
     $nama_dokter = !empty($rawatInap->dokter) ? $rawatInap->dokter->FULLNAME : '';
     echo $nama_dokter?> / <?=$rawatInap->kamar->nama_kamar;?></td>
   </tr>
   <tr>
     <td>Nama</td>
     <td>:</td>
     <td><?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?></td>
     <td>No Reg</td>
     <td>:</td>
     <td><?php echo $pasien->NoMedrec;?>    
     </td>
   </tr>
   <tr>
     <td>Umur</td>
     <td>:</td>
     <td><?php 
     if(!empty($reg))
     {
     	echo $reg->umurthn.' '.$pasien->umurbln;	
     }
     

     ?></td>
     <td>Jam diambil</td>
     <td>:</td>
     <td><?=date('H:i:s',strtotime($model->tanggal));?></td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td><?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Jam diserahkan</td>
     <td>:</td>
     <td><?=date('H:i:s');?></td>

   </tr>
 </table>


<?php 
$listRequestItem = [];
foreach($model->labRequestItems as $item)
{



?>
<table cellpadding="1" >
	<?php
  if(!in_array($item->parent->parent->grup->nama, $listRequestItem))
  {
    $listRequestItem[] = $item->parent->parent->grup->nama;
  
  ?>
  <tr>
		<td colspan="5" class="border_atas" style="text-align: center"><?=$item->parent->parent->grup->nama;?>

    </td>
		
	</tr>
	<?php

   ?>
  <tr>
		<td class="border_atas border_bawah" style="text-align: center;" width="30%">Pemeriksaan</td>
		<td class="border_atas border_kiri border_bawah" width="10%" style="text-align: center;" >Hasil</td>
		<td class="border_atas border_kiri border_bawah" width="25%" style="text-align: center;" >Nilai Normal</td>
    <td class="border_atas border_kiri border_bawah" width="18%" style="text-align: center;" >Kondisi</td>
		<td class="border_atas border_kiri border_bawah" width="17%" style="text-align: center;" >Metode</td>
	</tr>
	<?php 
  }
    $checker1 = [];
    $checker2 = [];
    $itemOptions = DmLabItemOption::model()->findAll();
    $list_itemOptions = [];
    foreach ($itemOptions as $v) {
      $list_itemOptions[$v->nilai] = $v->label;
    }
			$items = $item->labRequestItemKomponens;
      foreach($items as $q => $child)
      {
        $ch = $child;
        if(empty($ch->hasil)) continue;
                  // $subch = $child['subitems'];

                  $itemMapping = $ch->item;
          $batas_bawah = 0;
        $batas_atas = 0;
        $batas_bawah_kritis = 0;
        $batas_atas_kritis = 0;
        if($pasien->JENSKEL == 'L'){
          $batas_bawah = $itemMapping->batas_bawah;
          $batas_atas = $itemMapping->batas_atas;
          $batas_bawah_kritis = $itemMapping->batas_bawah_kritis;
          $batas_atas_kritis = $itemMapping->batas_atas_kritis;
        }
        else if($pasien->JENSKEL == 'P')
        {
          $batas_bawah = $itemMapping->batas_bawah_p;
          $batas_atas = $itemMapping->batas_atas_p;
          $batas_bawah_kritis = $itemMapping->batas_bawah_kritis_p;
          $batas_atas_kritis = $itemMapping->batas_atas_kritis_p;
        }
if(!in_array($itemMapping->parent->nama, $checker2)){
    $checker2[] = $itemMapping->parent->nama;
     echo '<tr>';
    echo '<td class="border_atas border_bawah" width="30%" ></td>';
    echo '<td  class="border_atas border_kiri border_bawah" width="10%" ></td>';
    echo '<td  class="border_atas border_kiri border_bawah" width="25%" ></td>';
    echo '<td  class="border_atas border_kiri border_bawah" width="18%" ></td>';
    echo '<td  class="border_atas border_kiri border_bawah" width="17%" ></td>';
    echo '</tr>';
  }

  ?>

	<tr>
		<td>&nbsp;&nbsp;<strong><?=$ch->item->item->nama;?></strong></td>
		<td class="border_kiri" style="text-align: center;" >
        <?php 

        if(!empty($itemMapping->dropdown_value)){
            echo $list_itemOptions[$ch->hasil];
        }
        else{
          echo $ch->hasil;
        }

        ?>
      </td>
		<td class="border_kiri" style="text-align: center;" >
      <?php 
                  
                  if(!empty($itemMapping->dropdown_value))
                  {
                    echo 'Negatif';
                  }
                  
                  else
                  {
                    if($pasien->JENSKEL == 'L'){
                      
                    ?>
                    <?=$itemMapping->batas_bawah;?> - 
                    <?=$itemMapping->batas_atas;?> 
                    <?=$itemMapping->satuan;?>
                    <?php 
                    }
                    else if($pasien->JENSKEL == 'P')
                    {
                      
                    ?>
                    
                    <?=$itemMapping->batas_bawah_p;?> - 
                    <?=$itemMapping->batas_atas_p;?> 
                    <?=$itemMapping->satuan;?>
                    <?php 
                    }
                  }
                  ?>
    </td>
    <td class="border_kiri" style="text-align: center;" >
      <?php 
              $hasil = $ch->hasil;
              if(!empty($hasil))
              {
                if($hasil >= $batas_bawah && $hasil <= $batas_atas )
                {
                  ?>
                  <strong>Normal</strong>
                  <?php
                }

                else if($hasil <= $batas_bawah_kritis || $hasil >= $batas_atas_kritis )
                {
                  ?>
                  <strong style="color:red">** Kritis</strong>
                  <?php
                }

                else if($hasil > $batas_bawah_kritis && $hasil <= $batas_bawah)
                {
                  ?>
                  <strong   style="color:orange">* &lt; normal</strong>
                  <?php
                }
                else if($hasil > $batas_atas && $hasil <= $batas_bawah_kritis)
                {
                  ?>
                  <strong  style="color:orange">* &rt; normal</strong>
                  <?php
                }
              }
                ?>
    </td>
		<td class="border_kiri" style="text-align: center;" ><?=$itemMapping->metode ?: '';?></td>
	</tr>

	<?php 
		}

	?>		
	<tr>
		<td colspan="5" class="border_atas"></td>
		
	</tr>
</table>
<?php
}

?>
<table width="100%">
	<tr>
		<td style="text-align: center;">
			Petugas Pelaksana
			<br><br><br><br>
			.......................
		</td>
		<td style="text-align: center;">
			Ka. Ins. Laboratorium
			<br><br><br><br>
			<u>dr. Erwin Ichsan</u><br>
			NIP. 196411151990031011
		</td>
	</tr>
</table>