<?php
/* @var $this DmLabItemController */
/* @var $model DmLabItem */

$this->breadcrumbs=array(
	array('name'=>'Dm Lab Item','url'=>array('admin')),
	array('name'=>'Dm Lab Item'),
);

$this->menu=array(
	array('label'=>'List DmLabItem', 'url'=>array('index')),
	array('label'=>'Create DmLabItem', 'url'=>array('create')),
	array('label'=>'Update DmLabItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmLabItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmLabItem', 'url'=>array('admin')),
);
?>

<h1>View DmLabItem #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kategori_id',
		'kelas_id',
		'nama',
		'bhp',
		'jrs',
		'japel',
		'jumlah',
		'created_at',
		'updated_at',
		'is_removed',
	),
)); ?>
	</div>
</div>
