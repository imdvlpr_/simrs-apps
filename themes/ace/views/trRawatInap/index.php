<?php
/* @var $this TdRegisterOkController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Td Register Oks',
);

$this->menu=array(
	array('label'=>'Create TdRegisterOk', 'url'=>array('create')),
	array('label'=>'Manage TdRegisterOk', 'url'=>array('admin')),
);
?>

<h1>Rawat Inap</h1>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'pasien-inap-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>
 <div class="row-fluid">
  <div class="block">
    <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Cari Data Pasien</div>
          </div>
           <div class="block-content collapse in">
      <div class="control-group">
          <?php echo CHtml::label('Nama Pasien','',array('class'=>'control-label'));?>
          <div class="controls">
            <?php
              $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'value' =>!empty($_POST['PASIEN_NAMA'])? $_POST['PASIEN_NAMA'] : '',
          'name' => 'PASIEN_NAMA',
          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('AjaxRequest/getPasienInRawatInap').'",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
          'htmlOptions'=>array('placeholder'=>'Ketik Nama | No Reg'),    
          'options' => array(
              'minLength' => 1,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 
                isiDataPasien(ui);
                $("#pasien-inap-form").submit();
              }',
      ),
      ));
                 
            ?>
            
            
          </div>
        </div>
      <div class="control-group">
        <label class="control-label">No RM</label>
        <div class="controls">
          <input type="text" name="cari_no_rm" id="cari_no_rm" value="<?php echo !empty($_POST['cari_no_rm'])? $_POST['cari_no_rm'] : '';?>"/>
          <input type="hidden" name="id_rawat_inap" id="id_rawat_inap" value="<?php echo !empty($_POST['id_rawat_inap'])? $_POST['id_rawat_inap'] : '';?>"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">&nbsp;</label>
        <div class="controls">
          <button type="submit" class="btn btn-primary" id="cari_pasien" name="cari_pasien">Cari Pasien</button>
        </div>
         
      </div>
    </div>
   </div>

 </div>   
    <?php

    $this->endWidget();?>
<script type="text/javascript">
	
	    function isiDataPasien(ui)
{
 
   $("#cari_no_rm").val(ui.item.id);
   $("#PASIEN_NAMA").val(ui.item.value); 
   $('#id_rawat_inap').val(ui.item.id_rawat_inap);

}

</script>
