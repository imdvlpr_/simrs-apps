  <?php 
    $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
            <div class="control-group">
  <label class="control-label">No Registrasi/No RM</label>
      <div class="controls">
        <?php 

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
          'name' => 'nama_pasien',
          // 'attribute' => 'PASIEN_NAMA',

          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('AjaxRequest/getPasienRawatInap').'",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
          'htmlOptions'=>array('placeholder'=>'Ketik Nama | No Reg','id'=>'nama_pasien'),    
          'options' => array(
              'minLength' => 1,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 
                isiDataPasien(ui);
              }',
      ),
      ));
                   ?>
<input type="hidden" name="no_rm" id="no_rm">

                   <?php echo CHtml::submitButton('CARI',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
      </div>
    </div>
    
  </fieldset>
  <?php 
  $this->endWidget();
  ?>
<script type="text/javascript">
function isiDataPasien(ui)
{
   $("#nama_pasien").val(ui.item.value);
   $("#no_rm").val(ui.item.id); 
   

}
</script>