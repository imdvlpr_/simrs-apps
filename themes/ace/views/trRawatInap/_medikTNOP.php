  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>
<?php

  $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
        }else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }

          $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }

    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>


 <table class="table table-condensed" id="table_ri_rj">
    <thead>
   <tr>
     <th>II</th>
     <th>Tindakan Medik Non-Operasi</th>
     <th colspan="2"  align="center">RAWAT DARURAT</th>
     <!-- <th>&nbsp;</th> -->
     <th colspan="2" align="center">RAWAT INAP</th>
     <!-- <th>&nbsp;</th> -->
     <th width="25%">TARIF</th>
   </tr>
   </thead>
 
   <tbody>
   <?php 

   if(!is_null($rawatInap->getError('ERROR_TNOP'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_TNOP'); 
?>
</div>
</td>
</tr>
<?php 
}

   $listTnop = TindakanMedisNonOperatif::model()->findAll();
   $i = 0;

   foreach($listTnop as $tnop){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_tnop' => $tnop->id_tindakan
      );
      $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);


      $jumlah_tindakan = 1;
      $biaya_irna = 0;
      $biaya_ird = 0;
      $biaya_irna_tnop_total = 0;

      if(!empty($itemTnop)){
        $jumlah_tindakan = $itemTnop->jumlah_tindakan;
        $biaya_irna = $itemTnop->biaya_irna;
        $biaya_ird = $itemTnop->biaya_ird;

        $biaya_irna_tnop_total = $biaya_irna_tnop_total + $biaya_irna * $jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_ird;
      $total_irna = $total_irna + $biaya_irna_tnop_total;
        
    ?>
   <tr>
     <td>&nbsp;</td>
     <td><span class="number-tnop"><?php echo strtolower(chr(64 + ($i+1)));?>. </span><?php echo $tnop->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td>
    <!-- <a id="input-tnop-ird-<?php echo $tnop->kode_tindakan;?>" class="input-tnop" href="javascript:void(0)" title="Input Tindakan">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
    </a> -->


<?php 
   $jml_tnop_ird = $biaya_ird ;
  
   ?>
     <input type="text" id="biaya_ird_tnop_<?php echo $tnop->kode_tindakan;?>" name="biaya_ird_tnop[]" value="<?php echo Yii::app()->helper->formatRupiah($jml_tnop_ird);?>"  class="input-medium uang" placeholder="-" />
       <?php 

  

     ?>
       
     </td>
     <td></td>
     <td style="text-align: right">
   
    
    </td>
     <td>
   <?php 
   $jml_tnop_irna = $biaya_irna ;
   
   ?>
     <input type="text" id="biaya_irna_tnop_<?php echo $tnop->kode_tindakan;?>" name="biaya_irna_tnop[]" value="<?php echo Yii::app()->helper->formatRupiah($jml_tnop_irna);?>"  class="input-medium uang" placeholder="-"/>
     
     <input type="hidden" name="id_tindakan_tnop[]" value="<?php echo $tnop->id_tindakan;?>"/>
    <!--  <a id="input-tnop_<?= $tnop->kode_tindakan;?>" class="input-tnop" href="javascript:void(0)" title="Input Tindakan">
        <img src="<?= Yii::app()->baseUrl;?>/images/icon-form.png"/>
    </a> -->
     </td>
   </tr>
     <?php
 $i++;
    }

    ?>
   
   
   </tbody>
  
    </table>

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
      <?php echo $this->renderPartial('_buttonCetak',array('rawatInap'=>$rawatInap));?>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>


<script>
$(document).ready(function(){
   $('.input-tnop').click(function(){
      $('#myModal').modal();  
  });
});
$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});





</script>
