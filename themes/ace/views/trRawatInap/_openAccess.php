
<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<?php $form = $this->beginWidget( 'CActiveForm', array(
  'id' => 'location-delete-form',
  'enableAjaxValidation' => false,
  'focus' => '#confirmApprove',
)); ?>

<div class="row-fluid">

		
	<div class="form-actions">	
		<?php echo CHtml::submitButton('Buka Akses',array('name'=>'confirmApprove','class'=>"btn btn-success",'id'=>'confirmApprove')); ?>
		<?php echo CHtml::submitButton('Batal',array('name'=>'denyDelete','class'=>"btn",'id'=>'cancel')); ?>
	</div>

</div>

<?php $this->endWidget(); ?>