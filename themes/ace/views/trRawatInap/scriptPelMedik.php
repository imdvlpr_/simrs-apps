<script>

$(document).on("click",".hapus-dokter",function(){
  
    var ukuran = $(this).closest("#tbody-dokter").children('.row-dokter').size();
    


    if(ukuran>1){
        
        $(this).parent().parent().remove();

        var i = 1;
        $('span.number').each(function(){
          $(this).html(i+". ");
          i++;
         
        });

         var parent = $(this).parent().parent();
        var fr = $('tbody#tbody-dokter tr:first');

        var fr_biaya = fr.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
        var fr_biayaTunggal = fr.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
        
        var biaya = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
        var biayaTunggal = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
        var jenisDokter = parent.find('td.biaya_visite_irna').find('input.jenis_dokter');
        
        var fr_biayaAsli = fr.find('td.biaya_visite_irna').find('input.biaya_asli_dokter');
        var jt = fr.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna');
        // fr_biaya.val('aa');
        var jenisVisite = parent.find('td:nth-child(2) input.nama_jenis_visite');
        

        if(jenisDokter.val() == 'UMUM' && jenisVisite.val() == 'Visite Dokter Ahli'){

          
          var bt = fr_biayaTunggal.val();
          
          var input = jt.val();
          var item = bt;
          item = item.replace(".", "");
          var value = eval(item) / 2;
          value = eval(value) * eval(input);
          biaya.val(value);
          biaya.val(reformatRupiah(biaya));


          var counter = 0;
          $("tr.row-dokter").each(function() {
            $this = $(this);
            
            var jd = $this.find('td.biaya_visite_irna').find('input.jenis_dokter');
             var jv = $this.find('td:nth-child(2) input.nama_jenis_visite');
            if(jd.val() == 'UMUM' && jv.val() == 'Visite Dokter Ahli'){
              var jmlTindakan =  $this.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna').val();
              counter += eval(jmlTindakan);
            }

          });



          var trueVal = eval(item) / 2;

          dFr_biaya = fr_biayaTunggal.val();
          dFr_biaya = dFr_biaya.replace(".", "");
          
          dFr_biaya = eval(dFr_biaya) * eval(jt.val()) + (counter * trueVal);
          
          fr_biaya.val(dFr_biaya);
          fr_biaya.val(reformatRupiah(fr_biaya));
          
         
          
        }
        return false;
    }



    


  });

$('#nama_dokter_perawat').focus();

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});

$('#tambah-dokter').on('click',function(){

  var row = '<tr id="row-dokter" class="row-dokter">';
     row += '<td>&nbsp;</td>';
     row += '<td>';
     row += '<span class="number"></span><input placeholder="Ketik Tindakan Dokter" type="text"';
     row += 'class="input-medium nama_jenis_visite" name="nama_jenis_visite" autocomplete="off" />';
     row += '   <input type="hidden" class="id_jenis_visite" name="id_jenis_visite[]" />';
      row += '   <input type="text" class="kode_jenis_visite" name="kode_jenis_visite[]" />';
     row += '</td>';
   row += '  <td>';
    row += ' <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" />';
   row += '  <input type="hidden" class="id_dokter" name="id_dokter_visite_ird[]" />';
   
   row += ' </td>';
   row += '  <td>';
   row += '  &nbsp;<input type="text" name="biaya_visite_ird[]" style="width:100px" class="input-medium uang" placeholder="-" value="0"/>';
  row += '   <input type="text" name="jumlah_visite_ird[]" class="input-small jumlah jumlah_tindakan_ird" placeholder="Jumlah Visite" value="1"/>';
  row += '   </td>';
     
  row += '  <td>&nbsp;</td>';
     
   row += '  <td>';
   row += '  <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" />';
   row += '  <input type="hidden" class="id_dokter" name="id_dokter_visite_irna[]" />';
  
  row += ' </td>';
   row += '  <td class="biaya_visite_irna">&nbsp;<input type="text" name="biaya_visite_irna[]" class="input-medium uang biaya_visite_irna" placeholder="-" value="0"/> ';

  row += '   <input type="text" name="jumlah_visite[]" class="input-small jumlah jumlah_tindakan_irna" placeholder="Jumlah Visite" value="1"/>';
  row += '<input type="hidden" name="biaya_tunggal_irna[]" class="input-medium biaya_tunggal_irna"/>';
   row += '  <input type="hidden" class="jenis_dokter" />';
   row += '  <input type="hidden" class="biaya_asli_dokter" />';
   
  row += '   <a href="javascript:void(0)" class="hapus-dokter btn btn-warning" >Hapus</a>';
  row += '   </td>';

  row += ' </tr>';

  $('#table_ri_rj > tbody#tbody-dokter tr:last').before(row);
      var i = 1;

      $('span.number').each(function(){
        $(this).html(i+". ");
        i++;

      });

   addCalculator();   
});



function reformatRupiah(item){
  var value = item.val();
  value = value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return value;
}



$(document).bind("keyup.autocomplete",function(){




$('.nama_dokter_ird').autocomplete({
      minLength:1,
      select:function(event, ui){
       
        $('#TrRawatInapRincian_dokter_ird').val(ui.item.id);
                
      },
      
      focus: function (event, ui) {
        $('#TrRawatInapRincian_dokter_ird').val(ui.item.id);
       
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 


  $('.nama_dokter').autocomplete({
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
        // alert(ui.item.jenis);
        
        var parent = $(this).parent().parent(); 

        // fr_biaya.val('aa');

        var fr = $('tbody#tbody-dokter tr:first');

        var fr_biaya = fr.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
        var fr_biayaTunggal = fr.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
        
        var biaya = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
        var biayaTunggal = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
        var jenisDokter = ui.item.jenis;
        // fr_biaya.val('aa');
        var fr_biayaAsli = fr.find('td.biaya_visite_irna').find('input.biaya_asli_dokter');
        var jD = parent.find('td.biaya_visite_irna').find('input.jenis_dokter');
        
        jD.val(jenisDokter);

        var biayaAsli = parent.find('td.biaya_visite_irna').find('input.biaya_asli_dokter');
        biayaAsli.val(biaya.val());
        var jenisVisite = parent.find('td:nth-child(2) input.kode_jenis_visite');
        // var kodeVisite = parent.find('td:nth-child(2) input.kode_jenis_visite');
        // kodeVisite.val(ui.item.kode);
        <?php 
        // if(Yii::app()->helper->contains($rawatInap->jenisPasien->NamaGol,'BPJS')){
        ?>
        if(jenisDokter == 'UMUM' && jenisVisite.val() == 'VISITE')
        {

          var bt = fr_biayaTunggal.val();
          

          var item = bt;
          item = item.replace(".", "");
          var value = eval(item) / 2;
          value = eval(value) ;
          biaya.val(value);
          biaya.val(reformatRupiah(biaya));

          // var fr_item = fr_biayaAsli.val();
          // fr_item = fr_item.replace(".","");
          var counter = 0;
          $("tr.row-dokter").each(function() {
            $this = $(this);
            
            var jd = $this.find('td.biaya_visite_irna').find('input.jenis_dokter');
            var jv = $this.find('td:nth-child(2) input.kode_jenis_visite');
            if(jd.val() == 'UMUM' && jv.val() == 'VISITE'){
              var jmlTindakan =   $this.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna').val();
              // console.log(jd.prev().prev().val());
              counter += eval(jmlTindakan);

            }

          });

          var jt = fr.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna');

          var trueVal = eval(item) / 2;
          dFr_biaya = fr_biayaTunggal.val();
          dFr_biaya = dFr_biaya.replace(".", "");


          dFr_biaya = eval(dFr_biaya) * eval(jt.val()) + (counter * trueVal);
          
          // console.log(dFr_biaya+' '+jt.val()+' '+counter+ ' '+trueVal);

          fr_biaya.val(dFr_biaya);
          fr_biaya.val(reformatRupiah(fr_biaya));
        }
        <?php 
      // }
      ?>
        
           
        
        // alert(bt);        
      },
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 

  $('.nama_jenis_visite').autocomplete({
      minLength:1,
      select:function(event, ui){
          
        $(this).next().val(ui.item.id);
        var parent = $(this).parent().parent(); 
        var biaya = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
        var biayaTunggal = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
        biayaTunggal.val(ui.item.biaya);
        biaya.val(ui.item.biaya);

        var fr = $('tbody#tbody-dokter tr:first');



        var fr_biaya = fr.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
        var fr_biayaTunggal = fr.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
        
        var biaya = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
        var biayaTunggal = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
         var jenisDokter = parent.find('td.biaya_visite_irna').find('input.jenis_dokter');
        // fr_biaya.val('aa');
        var fr_biayaAsli = fr.find('td.biaya_visite_irna').find('input.biaya_asli_dokter');
        // var jD = parent.find('td.biaya_visite_irna').find('input.jenis_dokter');
        
        
        var biayaAsli = parent.find('td.biaya_visite_irna').find('input.biaya_asli_dokter');
        biayaAsli.val(biaya.val());
        var jenisVisite = parent.find('td:nth-child(2) input.nama_jenis_visite');
        var kodeVisite = parent.find('td:nth-child(2) input.kode_jenis_visite');
        kodeVisite.val(ui.item.kode);

         <?php 
        if(Yii::app()->helper->contains($rawatInap->jenisPasien->NamaGol,'BPJS')){
        ?>
        if(jenisDokter.val() == 'UMUM' && kodeVisite.val() == 'VISITE')
        {
          

          var bt = fr_biayaTunggal.val();
          

          var item = bt;
          item = item.replace(".", "");
          var value = eval(item) / 2;
          value = eval(value) ;
          biaya.val(value);
          biaya.val(reformatRupiah(biaya));

          // var fr_item = fr_biayaAsli.val();
          // fr_item = fr_item.replace(".","");
          var counter = 0;
          $("tr.row-dokter").each(function() {
            $this = $(this);
            
            var jd = $this.find('td.biaya_visite_irna').find('input.jenis_dokter');
            var jv = $this.find('td:nth-child(2) input.kode_jenis_visite');
            if(jd.val() == 'UMUM' && jv.val() == 'VISITE'){
              var jmlTindakan =   $this.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna').val();
              counter += eval(jmlTindakan);

            }

          });

          var jt = fr.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna');

          var trueVal = eval(item) / 2;


          dFr_biaya = fr_biayaTunggal.val();
          dFr_biaya = dFr_biaya.replace(".", "");


          dFr_biaya = eval(dFr_biaya) * eval(jt.val()) + (counter * trueVal);
          
          fr_biaya.val(dFr_biaya);
          fr_biaya.val(reformatRupiah(fr_biaya));
        }
        <?php }?>
      },
       focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetJenisVisiteDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    tkt : <?php echo $rawatInap->kamar->kelas_id;?>,
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});



$('table#table_ri_rj tbody#tbody-dokter').on('keyup','input.jumlah_tindakan_irna',function(e){
    

    var input = $(this).val();

    if(jQuery.isNumeric(input))
    {
      var parent = $(this).parent().parent();
      var fr = $('tbody#tbody-dokter tr:first');
      var dr_dpjp = fr.find('td.biaya_visite_irna').prev().find('input.id_dokter');
      var fr_biaya = fr.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
      var fr_biayaTunggal = fr.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
      
      var biaya = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna');
      var biayaTunggal = parent.find('td.biaya_visite_irna').find('input.biaya_visite_irna').next().next();
      var jenisDokter = parent.find('td.biaya_visite_irna').find('input.jenis_dokter');
      
      var fr_biayaAsli = fr.find('td.biaya_visite_irna').find('input.biaya_asli_dokter');
      // fr_biaya.val('aa');

      var jenisVisite = parent.find('td:nth-child(2) input.nama_jenis_visite');
      

      if(jenisDokter.val() == 'UMUM' && jenisVisite.val() == 'Visite Dokter Ahli')
      {

        
        var bt = fr_biayaTunggal.val();
        

        var item = bt;
        item = item.replace(".", "");
        var value = eval(item) / 2;
        value = eval(value) * eval(input);
        biaya.val(value);
        biaya.val(reformatRupiah(biaya));


        var counter = 0;
        $("tr.row-dokter").each(function() {
          $this = $(this);
          
          var jd = $this.find('td.biaya_visite_irna').find('input.jenis_dokter');
           var jv = $this.find('td:nth-child(2) input.nama_jenis_visite');
          if(jd.val() == 'UMUM' && jv.val() == 'Visite Dokter Ahli'){
            var jmlTindakan =  $this.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna').val();
            counter += eval(jmlTindakan);
          }

        });

        var trueVal = eval(item) / 2;
        
        dFr_biaya = fr_biayaTunggal.val();
        dFr_biaya = dFr_biaya.replace(".", "");
        
        var jt = fr.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna');

        dFr_biaya = eval(dFr_biaya) * eval(jt.val()) + (counter * trueVal);
        


        fr_biaya.val(dFr_biaya);
        fr_biaya.val(reformatRupiah(fr_biaya));
        

        
      }

      else
      {
        var item = $(this).next().val();
        item = item.replace(".", "");
        var counter = 0;
        $("tr.row-dokter").each(function() {
          $this = $(this);
          
          var jd = $this.find('td.biaya_visite_irna').find('input.jenis_dokter');
           var jv = $this.find('td:nth-child(2) input.nama_jenis_visite');
          if(jd.val() == 'UMUM' && jv.val() == 'Visite Dokter Ahli'){
            var jmlTindakan =  $this.find('td.biaya_visite_irna').find('input.jumlah_tindakan_irna').val();
            counter += eval(jmlTindakan);
          }

        });

        var trueVal = eval(item) / 2;
      
        var dr_ahli = parent.find('td.biaya_visite_irna').prev().find('input.id_dokter');

        if(dr_ahli.val() != dr_dpjp.val())
          trueVal = 0;

        var value = eval(item) * eval(input) + (counter * trueVal);

        $(this).prev().val(value);
        var value = $(this).prev().val();
        value = value.replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $(this).prev().val(value);
        // $(this).val('1');
      }


     

      // dFr_biaya = fr_biaya.val();
      // dFr_biaya = dFr_biaya.replace(".", "");

      // dFr_biaya = eval(dFr_biaya) + value;
      // fr_biaya.val(dFr_biaya);
      // fr_biaya.val(reformatRupiah(fr_biaya));
        // alert(value);

        
    }    
    
        
});

$('table#table_ri_rj tbody#tbody-dokter').on('keyup','input.jumlah_tindakan_ird',function(e){
    
    var code = e.which; // recommended to use e.which, it's normalized across browsers
    if(code==13)
      e.preventDefault();
    if(code==32||code==13||code==188||code==186){
      var input = $(this).val();

      if(jQuery.isNumeric(input))
      {
          var item = $(this).prev().val();
          item = item.replace(".", "");
          var value = eval(item) * eval(input);
          
          $(this).prev().val(value);
          var value = $(this).prev().val();
          value = value.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          $(this).prev().val(value);
          $(this).val('1');
      }    
    } // missing closing if brace
    
        
});



$('#TrRawatInapRincian_jumlah_askep').keyup(function(e){
    
    var input = $(this).val();

    if(jQuery.isNumeric(input))
    {
        var item = $('#nilai_askep').val();
      
        var value = eval(item) * eval(input);
        $('#TrRawatInapRincian_askep_kamar').val(value);
    }
        
});


$('#TrRawatInapRincian_jumlah_asnut').keyup(function(e){
    
    var input = $(this).val();

    if(jQuery.isNumeric(input))
    {
        var item = $('#nilai_asnut').val();
      
        var value = eval(item) * eval(input);
        $('#TrRawatInapRincian_asuhan_nutrisi').val(value);
    }
        
});


function isiTindakan(ui)
{
   $("#TrRawatInapLayanan_id_tindakan").val(ui.item.id);
   $("#TrRawatInapLayanan_nama_tindakan").val(ui.item.value); 
   

}

$(document).ready(function(){

    $("#TrRawatInap_is_tarif_kamar_penuh").click(function(){
      var bkamar = eval($('#kamar_biaya_kamar').val());
        var bmakan = eval($('#kamar_biaya_makan').val());
      if($(this).is(':checked')){
        
        $('#biaya_kamar_per_malam').val(eval(bkamar + bmakan));
      }
      else{
        $('#biaya_kamar_per_malam').val(bkamar);
      }  
    });

    $('#pop-biaya-kamar-simpan').click(function(){

      var keterangan = $('input.pop-biaya-kamar-ket');
      var ri = <?php echo $rawatInap->id_rawat_inap;?>;
      
      var biaya = $('input.pop-biaya-kamar-biaya');
      var objects = [];
      var total_biaya = 0;


      for (var i = 0; i < keterangan.length; i++) {
        var d = keterangan[i];
        var b = biaya[i];
        var json_data = new Object();
        json_data.ri = ri;
        json_data.keterangan = d.value;
        json_data.biaya = b.value;
        var v = b.value;
        json_data.biaya = v.replace(".","");

        total_biaya = eval(total_biaya) + eval(json_data.biaya);
        objects[i] = json_data;
       
      }

      $.ajax({
        type : 'POST',
        url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputBiayaKamar');?>',
        dataType: 'json',
        data : JSON.stringify(objects),
        success : function(data){
          $('#biaya_kamar_per_malam').val(addCommas(total_biaya));
          $('#pop-biaya-kamar-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
          
        }
      });
    });

   $('#input-biaya-kamar').click(function(){
      $( '#dialog-biaya-kamar' )
            .dialog( { title: 'Input Biaya Kamar ' } )
            .dialog( 'open' );
   });

   $('#pop-tambah-biaya-kamar').on("click",function(){
      
    var row = '<tr class="pop-row-biaya-kamar">';
      row += '<td><span class="order-biaya-kamar"></span></td>';
      row += '<td><input type="text" class="input-medium pop-biaya-kamar-ket" name="pop-biaya-kamar-ket[]"/></td>';
      row += '<td><input type="text" class="input-medium uang pop-biaya-kamar-biaya" name="pop-biaya-kamar-biaya[]" value="0"/></td>';
      row += '<td><a href="javascript:void(0)" class="hapus-pop-biaya-kamar btn btn-warning">Hapus</a></td>';
      row += '</tr>';


    $('#table-popup-biaya-kamar > tbody tr:last').before(row);



    var i = 1;
    $('span.order-biaya-kamar').each(function(){
        $(this).html(i+". ");
        i++;

      });
      
    });
});

$(document).on("click",".hapus-pop-biaya-kamar",function(){
     $(this).parent().parent().remove();

    var i = 1;
    $('span.order-biaya-kamar').each(function(){
      $(this).html(i+". ");
      i++;
     
    });
    return false;
  

});

</script>