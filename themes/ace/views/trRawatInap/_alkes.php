  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>
<?php

$selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
           $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }

          $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
 

 <table class="table table-condensed" id="table_ri_rj" width="100%">
   <thead>
   <tr>
     <th width="3%">&nbsp;</th>
     <th width="27%">OBAT dan ALAT KESEHATAN</th>
     <th width="35%"  style="text-align: right">RAWAT DARURAT</th>
     <!-- <th>&nbsp;</th> -->
     <th width="35%"  style="text-align: right">RAWAT INAP</th>
     <!-- <th>&nbsp;</th> -->
     <!-- <th width="30%">TARIF</th> -->
   </tr>
   </thead>

   <tr>
     <th>D</th>
     <th><strong>#</strong></th>
     <th  style="text-align: right"><div class="span8">Biaya</div><div class="span4"></div></th>
     <th  style="text-align: right"><div class="span6">Biaya</div><div class="span6"></div></th>
   </tr>
   <tbody>
   <?php 
   if(!is_null($rawatInap->getError('ERROR_ALKES')))
   {
    ?>
    <tr>
    <td colspan="4">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_ALKES'); 
?>
</div>
</td>
</tr>
<?php 
}
   $listObat = ObatAlkes::model()->findAll();
   $i = 0;
   foreach($listObat as $obat){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_alkes' => $obat->id_obat_alkes
      );
      $itemObat = TrRawatInapAlkes::model()->findByAttributes($attr);

      $jumlah_tindakan = 1;
      $biaya_irna = $obat->tarip;
      $biaya_ird = 0;

      $biaya_obat_ird_total = 0;
      $biaya_obat_irna_total = 0;
      if(!empty($itemObat)){
        $jumlah_tindakan = $itemObat->jumlah_tindakan;
        $biaya_irna = $itemObat->biaya_irna;
        $biaya_ird = $itemObat->biaya_ird;
        $biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird;
        $biaya_obat_irna_total = $biaya_obat_irna_total + $biaya_irna * $jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_obat_ird_total;
      $total_irna = $total_irna + $biaya_obat_irna_total;
        // echo ($biaya_irna).'<br>';
    ?>
    <tr>
     <td>&nbsp;</td>
     <td><span><?php echo ($i+1);?>. </span><?php echo $obat->nama_obat_alkes;?></td>
    
     <td align="right">
    <?php 


          // $jml_obat_ird = 0 ;
          // $t = 0;

          //  $attr = array(
          //     'id_rawat_inap' => $rawatInap->id_rawat_inap,
          //     'kode_alkes' => $obat->kode_alkes,
          //   );
          // $model_alkes = TrRawatInapAlkesObatIrd::model()->findAllByAttributes($attr);
          // foreach($model_alkes as $item)
          // {
          //     $t = $t + $item->nilai;
          // }

          // $jml_obat_ird = $t;

          // $biaya_ird = $jml_obat_ird;

          // $jml_obat_irna = 0 ;
          // $t = 0;

          // $attr = array(
          //     'id_rawat_inap' => $rawatInap->id_rawat_inap,
          //     'kode_alkes' => $obat->kode_alkes,
          //   );
          // $model_alkes = TrRawatInapAlkesObat::model()->findAllByAttributes($attr);

          // // print_r($model_alkes);

          // foreach($model_alkes as $item)
          // {
          //     $t = $t + $item->nilai;
          // }

          // $jml_obat_irna = $t;

          // $biaya_irna = $jml_obat_irna;
     
          // print_r($jml_obat_irna);
    ?>

<?php
if($obat->kode_alkes == 'OBAT'){
  ?>
 <a id="input-obat-ird" href="javascript:void(0)" title="Input Tindakan <?php echo $obat->nama_obat_alkes;?>">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
        </a>
  <?php
}
?>
      Rp 
     <input type="text" id="biaya_ird_alkes_<?php echo $obat->kode_alkes;?>" name="biaya_ird_alkes[]" class="input-medium uang" value="<?php echo Yii::app()->helper->formatRupiah($biaya_ird);?>" />


 
     </td>
    
     
     <td align="right">
        <?php 
     if($obat->kode_alkes == 'OBAT')
     {
        ?>


   <!--  <a id="popup-obat" href="javascript:void(0)" title="Lihat Obat">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/popup.png"/>

        
    </a> -->
       <a id="input-obat" href="javascript:void(0)" title="Input Tindakan <?php echo $obat->nama_obat_alkes;?>">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
        </a>


        <?php

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'obat-dialog',
    // additional javascript options for the dialog plugin
    'options'=>array(
        'title'=>'Obat Pasien',
        'autoOpen'=>false,
        'modal'=>'true',
        'width'=>'800',
        'height'=>'auto',


    ),
));
?>
<table class="table table-bordered" id="popup-tables">
<thead>
  <tr>
    <th>No</th>
    <th>Nama Obat</th>
    <th>Harga</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>No</td>
    <td>Nama Obat</td>
    <td>Harga</td>
  </tr>
</tbody>
</table>

<?php

$this->endWidget('zii.widgets.jui.CJuiDialog');

     }
     ?>
 
     Rp
     <input type="text" id="biaya_irna_alkes_<?php echo $obat->kode_alkes;?>" name="biaya_irna_alkes[]" class="input-medium uang" value="<?php echo Yii::app()->helper->formatRupiah($biaya_irna);?>"/>
    
  
    
     <input type="hidden" name="id_tindakan_obat[]" value="<?php echo $obat->id_obat_alkes;?>"/>
 


     </td>
   </tr>
   

   <?php 
   $i++;
   }

   ?>
   
   </tbody>
   
    </table>

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
      <?php echo $this->renderPartial('_buttonCetak',
  array(
    'rawatInap'=>$rawatInap
  )
);?>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>


<?php 

?>

<script>

function addRow(selector, data){
  var row = '<tr>';
  row += '<td class="number">1</td>';
  row += '<td>';
  row +=  data.nama;
  row +=  '</td>';
  row += '<td style="text-align:right">'+addCommas(data.harga);
  row += '</td></tr>';

  $(selector+' > tbody').append(row);
  var i = 1;

  $('td.number').each(function(){
      $(this).html(i+". ");
    i++;

  });

}

$('#nama_dokter_perawat').focus();

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});

$(document).on("click",".hapus-pop-obat",function(){
          
    $(this).parent().parent().remove();

    var i = 1;
    $('span.order-obat').each(function(){
      $(this).html(i+". ");
      i++;
     
    });
    return false;
}); 

$(document).on("click",".hapus-pop-obat-ird",function(){
          
    $(this).parent().parent().remove();

    var i = 1;
    $('span.order-obat-ird').each(function(){
      $(this).html(i+". ");
      i++;
     
    });
    return false;
  

}); 

 $(document).ready(function(){


    $('#input-obat').click(function(){

      $( '#dialog-obat' )
            .dialog( { title: 'Input Obat' } )
            .dialog( 'open' );
    });

    $('#input-obat-ird').click(function(){

      $( '#dialog-obat-ird' )
            .dialog( { title: 'Input Obat' } )
            .dialog( 'open' );
    });

    $('#pop-obat-simpan-ird').click(function(){

      var keterangan = $('input.pop-obat-ket-ird');
      var ri = <?php echo $rawatInap->id_rawat_inap;?>;
      var kode_alkes = 'OBAT';
      var biaya = $('input.pop-obat-biaya-ird');
      var objects = [];
      var total_biaya = 0;

      // console.log(keterangan);

      for (var i = 0; i < biaya.length; i++) {
        var d = keterangan[i];
        var b = biaya[i];
        var json_data = new Object();
        json_data.ri = ri;
        json_data.keterangan = d.value;
        json_data.biaya = b.value;
        var v = b.value;
        json_data.biaya = v.replace(".","");
        json_data.kode_alkes = kode_alkes;
        
        total_biaya = eval(total_biaya) + eval(json_data.biaya);
        objects[i] = json_data;
       
      }

      $.ajax({
        type : 'POST',
        url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputAlkesObatIrd');?>',
        dataType: 'json',
        data : JSON.stringify(objects),
        success : function(data){
          $('#biaya_ird_alkes_OBAT').val(addCommas(total_biaya));
          $('#pop-obat-info-ird').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
          
        }
      });
    });

    $('#pop-obat-simpan').click(function(){

      var keterangan = $('input.pop-obat-ket');
      var ri = <?php echo $rawatInap->id_rawat_inap;?>;
      var kode_alkes = 'OBAT';
      var biaya = $('input.pop-obat-biaya');
      var objects = [];
      var total_biaya = 0;

      // console.log(keterangan);

      for (var i = 0; i < biaya.length; i++) {
        var d = keterangan[i];
        var b = biaya[i];
        var json_data = new Object();
        json_data.ri = ri;
        json_data.keterangan = d.value;
        json_data.biaya = b.value;
        var v = b.value;
        json_data.biaya = v.replace(".","");
        json_data.kode_alkes = kode_alkes;
        json_data.id_dokter = '<?php echo !empty($rawatInap->dokter) ? $rawatInap->dokter_id : '';?>';
        total_biaya = eval(total_biaya) + eval(json_data.biaya);
        objects[i] = json_data;
       
      }

      $.ajax({
        type : 'POST',
        url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputAlkesObat');?>',
        dataType: 'json',
        data : JSON.stringify(objects),
        success : function(data){
          $('#biaya_irna_alkes_OBAT').val(addCommas(total_biaya));
          $('#pop-obat-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
          
        }
      });
    });

     $('#pop-obat-add-item').on('click',function(){

           var row = '<tr class="pop-row-obat">';
              row += '<td><span class="order-obat"></span></td>';
              row += '<td><input type="text" class="input-medium pop-obat-ket" name="pop-obat-ket[]"/></td>';
              row += '<td><input type="text" class="input-medium uangnocalc pop-obat-biaya" name="pop-obat-biaya[]" value="0"/></td>';
              row += '<td><a href="javascript:void(0)" class="hapus-pop-obat btn btn-warning">Hapus</a></td>';
              row += '</tr>';


            $('#table-popup-obat > tbody tr:last').before(row);
            var i = 1;
            $('span.order-obat').each(function(){
              $(this).html(i+". ");
              i++;

            });
      });

      $('#pop-obat-add-item-ird').on('click',function(){

           var row = '<tr class="pop-row-obat-ird">';
              row += '<td><span class="order-obat-ird"></span></td>';
              row += '<td><input type="text" class="input-medium pop-obat-ket-ird" name="pop-obat-ket-ird[]"/></td>';
              row += '<td><input type="text" class="input-medium uangnocalc pop-obat-biaya-ird" name="pop-obat-biaya-ird[]" value="0"/></td>';
              row += '<td><a href="javascript:void(0)" class="hapus-pop-obat-ird btn btn-warning">Hapus</a></td>';
              row += '</tr>';


            $('#table-popup-obat-ird > tbody tr:last').before(row);
            var i = 1;
            $('span.order-obat-ird').each(function(){
              $(this).html(i+". ");
              i++;

            });
      });



    $('#popup-obat').on('click',function(){
      
      $.ajax({
        type : 'post',
        url : '<?=Yii::app()->createUrl('trRawatInap/ajaxObatPasienList');?>',
        
        data : {rid:<?=$rawatInap->id_rawat_inap;?>},
        beforeSend : function(){
          $('#loading').show();
        },
        success : function(res){
          var res = $.parseJSON(res);
          $('#popup-tables > tbody').empty();

          var totalNilai = 0;
          $.each(res, function(index, value) {
              obj = new Object;
              obj.id = value.id;
              obj.nama = value.nama;
              obj.harga = removePoints(value.harga);
              totalNilai += eval(obj.harga);
              addRow('#popup-tables',obj);
              

              
          });

          var row = '<tr>';
          row += '<td colspan="2"><strong>Total</strong></td>';
          row += '<td style="text-align:right"><strong>'+addCommas(totalNilai);
          row += '</strong></td></tr>';

          $('#popup-tables > tbody').append(row);

          $('#obat-dialog').dialog('open');
        },
      });
    });
  });



</script>
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-obat-ird',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 600,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-obat-info-ird"></div>
<table id="table-popup-obat-ird" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Keterangan</th>
  <th>Biaya</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-obat-tbody-ird">

<?php
 $order = 1;
$total = 0;

$attr = array(
      'id_rawat_inap' => $rawatInap->id_rawat_inap,
       'kode_alkes' => 'OBAT',
      // 'id_tindakan' => $obat->id_tindakan,
    );
  $model_obat = TrRawatInapAlkesObatIrd::model()->findAllByAttributes($attr);



 foreach($model_obat as $item)
 {
  
  $total = $total + $item->nilai;
?>

<tr class="pop-row-ird">
  <td><span class="order-obat-ird"><?php echo $order++;?>. </span></td>
  <td><input type="text" class="input-medium pop-obat-ket-ird" name="pop-obat-ket-ird[]" value="<?php echo $item->keterangan;?>"/></td>
  <td><input type="text" class="input-medium uangnocalc pop-obat-biaya-ird" name="pop-obat-biaya-ird[]" value="<?php echo $item->nilai;?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-obat-ird btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-obat-add-item-ird" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-obat-simpan-ird" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">HITUNG</a>
</div>
<?php 
$i++;
$this->endWidget(); 
?>
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-obat',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 600,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-obat-info"></div>
<table id="table-popup-obat" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Keterangan</th>
  <th>Biaya</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-obat-tbody">

<?php
 $order = 1;
$total = 0;

$attr = array(
      'id_rawat_inap' => $rawatInap->id_rawat_inap,
      'kode_alkes' => 'OBAT',
    );
  $model_obat = TrRawatInapAlkesObat::model()->findAllByAttributes($attr);



 foreach($model_obat as $item)
 {
// print_r($item);

  $total = $total + $item->nilai;
?>

<tr  class="pop-row-obat">
  <td><span class="order-obat"><?php echo $order++;?>. </span></td>
  <td><input type="text" class="input-medium pop-obat-ket" name="pop-obat-ket[]" value="<?php echo $item->keterangan;?>"/></td>
  <td><input type="text" class="input-medium uangnocalc pop-obat-biaya" name="pop-obat-biaya[]" value="<?php echo $item->nilai;?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-obat btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-obat-add-item" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-obat-simpan" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">HITUNG</a>
</div>
<?php 
$i++;
$this->endWidget(); 
?>