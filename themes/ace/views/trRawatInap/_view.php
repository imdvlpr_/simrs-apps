<?php
/* @var $this TrRawatInapController */
/* @var $data TrRawatInap */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_rawat_inap')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_rawat_inap), array('view', 'id'=>$data->id_rawat_inap)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_masuk')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_masuk')); ?>:</b>
	<?php echo CHtml::encode($data->jam_masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_keluar')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_keluar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_keluar')); ?>:</b>
	<?php echo CHtml::encode($data->jam_keluar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetime_masuk')); ?>:</b>
	<?php echo CHtml::encode($data->datetime_masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetime_keluar')); ?>:</b>
	<?php echo CHtml::encode($data->datetime_keluar); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pasien_id')); ?>:</b>
	<?php echo CHtml::encode($data->pasien_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ri_item_id')); ?>:</b>
	<?php echo CHtml::encode($data->ri_item_id); ?>
	<br />

	*/ ?>

</div>