  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .uangnocalc{
    text-align: right;
  }
</style>
<?php

  $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
        }else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }

          $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }


    
?>



 <table class="table table-condensed" id="table_ri_rj">
   <thead>
   <tr>
     <th>I</th>
     <th>Tindakan Medik</th>
     <th colspan="2"  align="center">RAWAT DARURAT</th>
     <!-- <th>&nbsp;</th> -->
     <th colspan="2" align="center">RAWAT INAP</th>
     <!-- <th>&nbsp;</th> -->
     <th width="25%">TARIF</th>
   </tr>
   </thead>
   
  
   <tbody id="tbody-top" style="text-align: right">
   <?php 
   if(!is_null($rawatInap->getError('ERROR_TOP'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_TOP'); 
?>
</div>
</td>
</tr>
<?php 

}

   $listTop = TindakanMedisOperatif::model()->findAll(['order'=>'nama_tindakan ASC']);
   $i = 0;


   foreach($listTop as $top){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_top' => $top->id_tindakan
      );
      $itemTop = TrRawatInapTop::model()->findByAttributes($attr);

      $jumlah_tindakan = 0;
      $biaya_irna = 0;

      $valueJm = '';
      $valueAnas = '';
      $biaya_irna_top_total = 0;
      if(!empty($itemTop)){
        $jumlah_tindakan = $itemTop->jumlah_tindakan;
        $biaya_irna = $itemTop->biaya_irna;

        $biaya_irna_top_total = $biaya_irna_top_total + $biaya_irna * $jumlah_tindakan;
        
      }

      $total_irna = $total_irna + $biaya_irna_top_total;
        
       

    ?>
   
   <tr>
     <td>&nbsp;</td>
     <td colspan="3" align="left"><span class="number-top"><?php echo strtolower(chr(64 + ($i+1)));?>.&nbsp;</span><?php echo $top->nama_tindakan;?></td>
     <td></td>
     <td>&nbsp;</td>
     <td style="text-align: right">
       <?php 



        if($top->kode_tindakan == 'JMO')
        {

          $drJM = '';

          $idDrJM = '';
          if(!empty($itemTop) && !empty($itemTop->dokterJm)){
            $drJM = $itemTop->dokterJm->nama_dokter;
            $idDrJM = $itemTop->dokterJm->id_dokter;
          }


       ?>
        <a id="input-top-jm" href="javascript:void(0)" title="Input Dokter">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
        </a>


       <?php 

       }

       else if($top->kode_tindakan == "JMA")
       {
          $drAnas = '';
          $idDrAnas = '';
          if(!empty($itemTop) && !empty($itemTop->dokterAnas)){
            $drAnas= $itemTop->dokterAnas->nama_dokter;
             $idDrAnas = $itemTop->dokterAnas->id_dokter;
          }



       ?>

       <a id="input-top-ja" href="javascript:void(0)" title="Input JM Anestesi">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
        </a>

       <?php 
     }

     else{
      ?>

      <a id="input-top-tindakan-<?php echo $top->id_tindakan;?>" href="javascript:void(0)" title="Input Tindakan <?php echo $top->nama_tindakan;?>">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
        </a>

     <script type="text/javascript">
       $(document).ready(function(){

          $('#pop-top-simpan-<?php echo $top->id_tindakan;?>').click(function(){

            var keterangan = $('input.pop-top-<?php echo $top->id_tindakan;?>-ket');
            var ri = <?php echo $rawatInap->id_rawat_inap;?>;
             var tid = '<?php echo $top->id_tindakan;?>';
            var biaya = $('input.pop-top-<?php echo $top->id_tindakan;?>-biaya');
            var objects = [];
            var total_biaya = 0;

            // console.log(keterangan);

            for (var i = 0; i < keterangan.length; i++) {
              var d = keterangan[i];
              var b = biaya[i];
              var json_data = new Object();
              json_data.ri = ri;
              json_data.keterangan = d.value;
              json_data.biaya = b.value;
              var v = b.value;
              json_data.biaya = v.replace(".","");
              json_data.id_tindakan = tid;
              total_biaya = eval(total_biaya) + eval(json_data.biaya);
              objects[i] = json_data;
             
            }

            $.ajax({
              type : 'POST',
              url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputTOP');?>',
              dataType: 'json',
              data : JSON.stringify(objects),
              success : function(data){
                $('#biaya_irna_top_<?php echo $top->id_tindakan;?>').val(addCommas(total_biaya));
                $('#pop-top-<?php echo $top->id_tindakan;?>-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
                
              }
            });
          });

          $('#input-top-tindakan-<?php echo $top->id_tindakan;?>').click(function(){

            $( '#dialog-top-<?php echo $top->id_tindakan;?>' )
                  .dialog( { title: 'Input Tindakan <?php echo $top->nama_tindakan;?>' } )
                  .dialog( 'open' );
          });

          $('#pop-top-add-item-<?php echo $top->id_tindakan;?>').on('click',function(){
               var row = '<tr class="pop-row-top-<?php echo $top->id_tindakan;?>">';
                  row += '<td><span class="order-top-<?php echo $top->id_tindakan;?>"></span></td>';
                  row += '<td><input type="text" class="input-medium pop-top-<?php echo $top->id_tindakan;?>-ket" name="pop-top-<?php echo $top->id_tindakan;?>-ket[]"/></td>';
                  row += '<td><input type="text" class="input-medium uangnocalc pop-top-<?php echo $top->id_tindakan;?>-biaya" name="pop-top-<?php echo $top->id_tindakan;?>-biaya[]" value="0"/></td>';
                  row += '<td><a href="javascript:void(0)" class="hapus-pop-top-<?php echo $top->id_tindakan;?> btn btn-warning">Hapus</a></td>';
                  row += '</tr>';


                $('#table-popup-top-<?php echo $top->id_tindakan;?> > tbody tr:last').before(row);
                var i = 1;
                $('span.order-top-<?php echo $top->id_tindakan;?>').each(function(){
                  $(this).html(i+". ");
                  i++;

                });
          });
       });


        $(document).on("click",".hapus-pop-top-<?php echo $top->id_tindakan;?>",function(){
          
            $(this).parent().parent().remove();

            var i = 1;
            $('span.order-top-<?php echo $top->id_tindakan;?>').each(function(){
              $(this).html(i+". ");
              i++;
             
            });
            return false;
          

        }); 

     </script>   
      <?php
     }



       ?>
   
     <?php 

     if($top->kode_tindakan == 'JMO')
      {
        $total_top_jm = 0;
        foreach($rawatInap->trRawatInapTopJm as $item)
        {

          $total_top_jm = $total_top_jm + $item->nilai;
        }
        ?>
     Rp&nbsp;<input type="text" name="biaya_irna_top[]" id="biaya_irna_top_jm" value="<?php echo Yii::app()->helper->formatRupiah($total_top_jm);?>" class="input-medium uangnocalc" placeholder="Biaya Layanan"/>
     
     <input type="hidden" name="id_tindakan_top[]" value="<?php echo $top->id_tindakan;?>"/>

        <?php 
      }

      else if($top->kode_tindakan == "JMA")
      {
        $total_top_ja = 0;
        foreach($rawatInap->trRawatInapTopJa as $item)
        {

          $total_top_ja = $total_top_ja + $item->nilai;
        }
        ?>
          Rp&nbsp;<input type="text" name="biaya_irna_top[]" id="biaya_irna_top_ja" value="<?php echo Yii::app()->helper->formatRupiah($total_top_ja);?>" class="input-medium uangnocalc" placeholder="Biaya Layanan"/>
    
     <input type="hidden" name="id_tindakan_top[]" value="<?php echo $top->id_tindakan;?>"/>
        <?php 
      }

    else{
         // $attr = array(
         //      'id_rawat_inap' => $rawatInap->id_rawat_inap,
         //      'id_tindakan' => $top->id_tindakan,
         //    );
         //  $model_tindakan = TrRawatInapTopTindakan::model()->findAllByAttributes($attr);
         //   $t = 0;
         //  foreach($model_tindakan as $item)
         //  {
         //      $t = $t + $item->nilai;
         //  }

         //   $biaya_irna = $t;


     ?>
     Rp&nbsp;<input type="text" name="biaya_irna_top[]" id="biaya_irna_top_<?php echo $top->id_tindakan;?>" value="<?php echo Yii::app()->helper->formatRupiah($biaya_irna);?>" class="input-medium uangnocalc" placeholder="Biaya Layanan"/>
   
     <input type="hidden" name="id_tindakan_top[]" value="<?php echo $top->id_tindakan;?>"/>
     <?php }?>
     </td>
   </tr>
    <?php
 $i++;
    }

    ?>
 
  </tbody>
 
  
    </table>

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
     <?php echo $this->renderPartial('_buttonCetak',array('rawatInap'=>$rawatInap));?>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>



<script>

$('#nama_dokter_perawat').focus();

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});


$(document).bind("keyup.autocomplete",function(){

 
   $('.nama_dokter_merawat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#TrRawatInap_dokter_id').val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $('#TrRawatInap_dokter_id').val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 


});




function isiTindakan(ui)
{
   $("#TrRawatInapLayanan_id_tindakan").val(ui.item.id);
   $("#TrRawatInapLayanan_nama_tindakan").val(ui.item.value); 
   

}

$(document).on("click",".hapus-pop-top-jm",function(){
  
  var ukuran = $(this).closest("#table-popup-top-jm-tbody").children('tr.pop-row-dokter-top').size();
  // alert(ukuran);
  if(ukuran>1){
      
      $(this).parent().parent().remove();

      var i = 1;
      $('span.order').each(function(){
        $(this).html(i+". ");
        i++;
       
      });
      return false;
  }

});

$(document).on("click",".hapus-pop-top-ja",function(){
  
  var ukuran = $(this).closest("#table-popup-top-ja-tbody").children('tr.pop-row-dokter-top-ja').size();
  // alert(ukuran);
  if(ukuran>1){
      
      $(this).parent().parent().remove();

      var i = 1;
      $('span.order-ja').each(function(){
        $(this).html(i+". ");
        i++;
       
      });
      return false;
  }

});


$(document).on("keydown.autocomplete",".pop-top-dokter",function(e){
  $('.pop-top-dokter').autocomplete({
      appendTo : '#dialog-top-jasa-medik',
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

$(document).on("keydown.autocomplete",".pop-top-dokter-ja",function(e){
  $('.pop-top-dokter-ja').autocomplete({
      appendTo : '#dialog-top-jasa-anastesi',
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

$(document).ready(function(){
  


  $('#input-top-jm').click(function(){
    // alert('$');
    $( '#dialog-top-jasa-medik' )
          .dialog( { title: 'Input Tindakan Jasa Medik' } )
          .dialog( 'open' );
  });

  $('#input-top-ja').click(function(){
    // alert('$');
    $( '#dialog-top-jasa-anastesi' )
          .dialog( { title: 'Input Tindakan Jasa Anastesi' } )
          .dialog( 'open' );
  });

  
  $('#pop-tambah-top-jm').on("click",function(){


    if($('span.order').length < 7)
    {
      
      var row = '<tr class="pop-row-dokter-top">';
        row += '<td><span class="order"></span></td>';
        row += '<td><input type="text" placeholder="Ketik Nama Dokter" class="input-large pop-top-dokter" name="pop-top-dokter[]" /><input type="hidden" class="pop-top-dokter-id" name="pop-top-dokter-id[]" /></td>';
        row += '<td><input type="text" class="input-medium uangnocalc pop-top-dokter-biaya" name="pop-top-dokter-biaya[]" value="0"/></td>';
        row += '<td><a href="javascript:void(0)" class="hapus-pop-top-jm btn btn-warning" >Hapus</a></td>';
        row += '</tr>';


      $('#table-popup-top-jm > tbody tr:last').before(row);



      var i = 1;
      $('span.order').each(function(){
        $(this).html(i+". ");
        i++;

      });
    }
  });

   $('#pop-tambah-top-ja').on("click",function(){


    if($('span.order-ja').length < 7)
    {
      
      var row = '<tr class="pop-row-dokter-top-ja">';
        row += '<td><span class="order-ja"></span></td>';
        row += '<td><input type="text" placeholder="Ketik Nama Dokter" class="input-large pop-top-dokter-ja" name="pop-top-dokter[]" /><input type="hidden" class="pop-top-dokter-ja-id" name="pop-top-dokter-ja-id[]" /></td>';
        row += '<td><input type="text" class="input-medium uangnocalc pop-top-dokter-ja-biaya" name="pop-top-dokter-ja-biaya[]" value="0"/></td>';
        row += '<td><a href="javascript:void(0)" class="hapus-pop-top-ja btn btn-warning" >Hapus</a></td>';
        row += '</tr>';


      $('#table-popup-top-ja > tbody tr:last').before(row);



      var i = 1;
      $('span.order-ja').each(function(){
        $(this).html(i+". ");
        i++;

      });
    }
  });

  $('#pop-top-jm-simpan').click(function(){

    var dokter = $('input.pop-top-dokter-id');
    var ri = <?php echo $rawatInap->id_rawat_inap;?>;
    
    var biaya = $('input.pop-top-dokter-biaya');
    var objects = [];
    var total_biaya = 0;
    for (var i = 0; i < dokter.length; i++) {
      var d = dokter[i];
      var b = biaya[i];

      if(d != '' && b != '')
      {
          var json_data = new Object();
          json_data.ri = ri;
          json_data.dokter = d.value;
          var v = b.value;

          json_data.biaya = v.replace(".","");
          total_biaya = eval(total_biaya) + eval(json_data.biaya );
          objects[i] = json_data;
      }

      
    }

    $.ajax({
      type : 'POST',
      url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputTopJM');?>',
      dataType: 'json',
      data : JSON.stringify(objects),
      success : function(data){
        $('#biaya_irna_top_jm').val(addCommas(total_biaya));
        $('#pop-jm-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
        
      }
    });


   
  });


   $('#pop-top-ja-simpan').click(function(){

    var dokter = $('input.pop-top-dokter-ja-id');
    var ri = <?php echo $rawatInap->id_rawat_inap;?>;
    
    var biaya = $('input.pop-top-dokter-ja-biaya');
    var objects = [];
    var total_biaya = 0;
    for (var i = 0; i < dokter.length; i++) {
      var d = dokter[i];
      var b = biaya[i];

      if(d != '' && b != '')
      {
          var json_data = new Object();
          json_data.ri = ri;
          json_data.dokter = d.value;
          var v = b.value;
          json_data.biaya = v.replace(".","");
          total_biaya = eval(total_biaya) + eval(json_data.biaya );
          objects[i] = json_data;
      }

      
    }

    $.ajax({
      type : 'POST',
      url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputTopJA');?>',
      dataType: 'json',
      data : JSON.stringify(objects),
      success : function(data){
        $('#biaya_irna_top_ja').val(addCommas(total_biaya));
        $('#pop-ja-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
        
      }
    });


   
  });
});


</script>

<!-- 
########### Jasa Medik
 -->
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-top-jasa-medik',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 700,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-jm-info"></div>
<table id="table-popup-top-jm" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Nama Dokter</th>
  <th>Jumlah</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-top-jm-tbody">

<?php
 $order = 1;
$total = 0;
 foreach($rawatInap->trRawatInapTopJm as $item)
 {

  $total = $total + $item->nilai;
?>

<tr id="pop-row-dokter-top" class="pop-row-dokter-top">
  <td><span class="order"><?php echo $order++;?>. </span></td>
  <td><input type="text" placeholder="Ketik Nama Dokter" class="input-large pop-top-dokter" name="pop-top-dokter" id="pop-top-dokter" value="<?php echo $item->dokter->nama_dokter;?>"/>
  <input type="hidden" class="pop-top-dokter-id" name="pop-top-dokter-id[]" value="<?php echo $item->id_dokter;?>"//>
  </td>
  <td><input type="text" class="input-medium uangnocalc pop-top-dokter-biaya" name="pop-top-dokter-biaya[]" id="pop-top-dokter-biaya" value="<?php echo Yii::app()->helper->formatRupiah($item->nilai);?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-top-jm btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-tambah-top-jm" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-top-jm-simpan" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">HITUNG</a>
</div>
<?php $this->endWidget(); ?>



<!-- 
########### Jasa Anestesi
 -->
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-top-jasa-anastesi',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 700,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-ja-info"></div>
<table id="table-popup-top-ja" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Nama Dokter</th>
  <th>Jumlah</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-top-ja-tbody">

<?php
 $order = 1;
$total = 0;

 foreach($rawatInap->trRawatInapTopJa as $item)
 {

  $total = $total + $item->nilai;

  $nama_dokter = !empty($item->dokter) ? $item->dokter->FULLNAME : '';

?>

<tr id="pop-row-dokter-top-ja" class="pop-row-dokter-top-ja">
  <td><span class="order-ja"><?php echo $order++;?>. </span></td>
  <td><input type="text" placeholder="Ketik Nama Dokter" class="input-large pop-top-dokter-ja" name="pop-top-dokter-ja" id="pop-top-dokter" value="<?php echo $nama_dokter;?>"/>
  <input type="hidden" class="pop-top-dokter-ja-id" name="pop-top-dokter-ja-id[]" value="<?php echo $item->id_dokter;?>"//>
  </td>
  <td><input type="text" class="input-medium uangnocalc pop-top-dokter-ja-biaya" name="pop-top-dokter-ja-biaya[]" id="pop-top-dokter-ja-biaya" value="<?php echo Yii::app()->helper->formatRupiah($item->nilai);?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-top-ja btn btn-warning" >Hapus</a></td>
</tr>
<?php

}
?>
<tr>
  <td colspan="3"><a id="pop-tambah-top-ja" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-top-ja-simpan" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">HITUNG</a>
</div>
<?php $this->endWidget(); ?>




<?php

$listTop = TindakanMedisOperatif::model()->findAll();
   $i = 0;


foreach($listTop as $top)
{

if($top->kode_tindakan == 'JMO' || $top->kode_tindakan=='JMA')
  continue;

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-top-'.$top->id_tindakan,
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 500,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-top-<?php echo $top->id_tindakan;?>-info"></div>
<table id="table-popup-top-<?php echo $top->id_tindakan;?>" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Keterangan</th>
  <th>Biaya</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-top-<?php echo $top->id_tindakan;?>-tbody">

<?php
 $order = 1;
$total = 0;

$attr = array(
      'id_rawat_inap' => $rawatInap->id_rawat_inap,
      'id_tindakan' => $top->id_tindakan,
    );
  $model_top = TrRawatInapTopTindakan::model()->findAllByAttributes($attr);



 foreach($model_top as $item)
 {

  $total = $total + $item->nilai;
?>

<tr id="pop-row-top-<?php echo $top->id_tindakan;?>" class="pop-row-<?php echo $top->id_tindakan;?>">
  <td><span class="order-top-<?php echo $top->id_tindakan;?>"><?php echo $order++;?>. </span></td>
  <td><input type="text" class="input-medium pop-top-<?php echo $top->id_tindakan;?>-ket" name="pop-top-<?php echo $top->id_tindakan;?>-ket[]" value="<?php echo $item->keterangan;?>"/></td>
  <td><input type="text" class="input-medium uangnocalc pop-top-<?php echo $top->id_tindakan;?>-biaya" name="pop-top-<?php echo $top->id_tindakan;?>-biaya[]" value="<?php echo $item->nilai;?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-top-<?php echo $top->id_tindakan;?> btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-top-add-item-<?php echo $top->id_tindakan;?>" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-top-simpan-<?php echo $top->id_tindakan;?>" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">HITUNG</a>
</div>
<?php 
$i++;
$this->endWidget(); 
}
?>

