

  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));

           

echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
 <div class="row">  
  <div class="col-xs-6">
     <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data dari Pendaftaran</h4>
        </div>
         <div class="widget-body">
            <div class="widget-main">
                  <div class="form-group">
                <?php echo $form->labelEx($model,'pasien_id',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 

                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'model' => $model,
                      'attribute' => 'PASIEN_NAMA',

                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getPasienInMaster').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',
                      'htmlOptions'=>array('placeholder'=>'Ketik No RM','class'=>'form-control'),    
                      'options' => array(
                          'minLength' => 1,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiDataPasien(ui);
                          }',
                  ),
                  ));
                      echo $form->hiddenField($model,'pasien_id');
                      

                      echo $form->error($model,'pasien_id');
                     ?>
                  </div>
                </div>
                <div id="data-pasien" style="display: none">
                  <table class="table" id="tabel-data-pasien">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tgl MRS</th>
                        <th>Gol</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
            </div>
         </div>
     </div>

  </div>
<div class="col-xs-6">
	<div class="widget-box widget-color-blue2">
    <div class="widget-header">
      <h4 class="widget-title lighter smaller">Data untuk Rawat Inap</h4>
    </div>
     <div class="widget-body">
        <div class="widget-main">
           <div class="form-group">
      <?php echo CHtml::label('Jenis Pasien','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
        <div class="col-sm-9">
          <?php 
          $criteria=new CDbCriteria;
    $criteria->addCondition('IsKaryawan',1);
      $list_gol = CHtml::listData(GolPasien::model()->findAll($criteria),'KodeGol','NamaGol');

       echo $form->dropDownList($model, 'jenis_pasien',$list_gol);
             ?>
        </div>
      </div>
   <div class="form-group">
      <?php echo CHtml::label('Kamar','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
        <div class="col-sm-9">
          <?php 

      $criteria=new CDbCriteria;
      $criteria->order = 'nama_kamar ASC';
      if(!Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_OP_KASIR,WebUser::R_OP_KEUANGAN, WebUser::R_OP_IGD)))
      {
        $user_kelas = Yii::app()->user->getState('username');

        $criteria->compare('user_kamar',$user_kelas);
      }

      $criteria->addCondition('is_hapus=0');

      $list_kamar = CHtml::listData(DmKamar::model()->findAll($criteria),'id_kamar',function($kelas){
      $full = $kelas->nama_kamar.' - '.$kelas->kelas->nama_kelas;
      return CHtml::encode($full);
    });

       echo $form->dropDownList($model, 'kamar_id',$list_kamar);
             ?>
        </div>
      </div>
 
  <div class="form-group">
    <?php echo $form->labelEx($model,'tanggal_masuk',array('class'=>'col-sm-3 control-label no-padding-right'));?>
      <div class="col-sm-9">
      <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'datetime_masuk',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'dd/mm/yy',
                        'changeMonth' => true,
                        'changeYear' => true,
                        
                    ),
        ));
   
        ?>
       
        <?php echo $form->error($model,'tanggal_masuk'); ?>
      </div>
    </div>
     <div class="form-group">
      <?php echo CHtml::label('Naik Kelas','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
        <div class="col-sm-9">
          <?php 

       echo $form->dropDownList($model, 'is_naik_kelas',array('Tidak','Ya'));
             ?>
        </div>
      </div>
     <div class="form-group">
      <?php echo $form->label($model,'jenis_ird',array('class'=>'col-sm-3 control-label no-padding-right'));?>
        <div class="col-sm-9">
          <?php 

     
      $attr = array(
        'UMUM' => 'UMUM',
        'PONEK' => 'PONEK'
      );
       echo $form->dropDownList($model, 'jenis_ird',$attr);
             ?>
        </div>
      </div>
    <div class="form-group">
    <?php echo $form->labelEx($model,'tanggal_masuk_ird',array('class'=>'col-sm-3 control-label no-padding-right'));?>
      <div class="col-sm-9">
      <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'datetime_masuk_ird',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'dd/mm/yy',
                        'changeMonth' => true,
                        'changeYear' => true,
                        
                    ),
        ));
   
        ?>
       
        <?php echo $form->error($model,'tanggal_masuk_ird'); ?>
      </div>
    </div>
    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Submit
          </button>

        
        </div>
      </div>
        </div>
      </div>
    </div>

   
   </div>

</div>
 
  <?php $this->endWidget();?>


<script>


<?php 
  if(!empty($model->pASIEN)){
?>
$('#TrRawatInap_PASIEN_NAMA').val('<?php echo $model->pASIEN->NAMA;?>');
<?php 
}
?>
function isiDataPasien(ui)
{
	 $("#TrRawatInap_pasien_id").val(ui.item.id);
	 $("#TrRawatInap_PASIEN_NAMA").val(ui.item.value); 
	 
  $.ajax({
    url : '<?php echo Yii::app()->createUrl('AjaxRequest/GetPasienPendaftaran');?>',
    type : 'POST',
    data : 'norm='+$("#TrRawatInap_pasien_id").val(),
    async : true,
    success : function(data){
       var data = $.parseJSON(data);
       $('#data-pasien').show();
       $('#tabel-data-pasien > tbody').empty();
       var row = '';
       $.each(data,function(i, obj){
         
          row += '<tr>';
          row += '<td>'+(i+1)+'</td>';
          row += '<td>'+obj.tgl_daftar+'</td>';
          row += '<td>'+obj.nama_gol+'</td>';
          row += '<td><a href="javascript:void(0)" class="data_pasien_pilih btn btn-info">Pilih</a>';
          row += '<input type="hidden" value="'+obj.kode_daftar+'" class="kode_daftar"/>'
          row += '</td>';
          row += '</tr>';
       });

       $('#tabel-data-pasien > tbody').append(row);
      
      // $("#TrRawatInap_kode_rawat").val(hsl.kode_daftar); 
     // $('#TrRawatInap_jenis_pasien').val(hsl.kode_gol);
     // $('#jenis_pasien').val(hsl.nama_gol);
     // $('#tgl_daftar').val(hsl.tgl_daftar);

    }
  });

}

$(document).ready(function(){

  $('.data_pasien_pilih').on('click',function(){
    alert('ok');
    $('#jenis_pasien').val($(this).next().val());
  });
});

</script>