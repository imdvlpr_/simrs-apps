<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="container-fluid" >
    <div class="row-fluid">
        
        
        <!--/span-->
        <div class="span12" >
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Laporan Rawat Inap','url'=>Yii::app()->createUrl('trRawatInap/laporan')),
                            array('name' => 'Laporan Dokter','url'=>Yii::app()->createUrl('trRawatInap/laporanDokter')),
                            
                          ),
                          'delimiter' => ' || ', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Laporan Rawat Inap</div>
                      <div class="pull-right"> </div> 
                    </div>
                    <div class="block-content collapse in">
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    <legend>
      

    </legend>
    
  <div class="control-group">
    <?php echo CHtml::label('Antara Tanggal','',array('class'=>'control-label'));?>
      <div class="controls">

      <div class="span12">
      <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>
       
       &nbsp;sampai tanggal&nbsp;
        <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>
        &nbsp; <?php echo CHtml::submitButton('Lihat Laporan',array('class'=>'btn btn-success')); ?>
      </div>
      </div>
    </div>
  
  </fieldset>
  <?php $this->endWidget();?>

<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>
<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No REG</th>
      <th>Nama Pasien</th>
      <th>Status</th>
      <th>Kelas</th>
      <th>Ruang</th>
      <th>Alamat</th>
      <th>Tgl Masuk</th>
      <th>Tgl Keluar</th>
      <th>Lm<br>Dirawat</th>
      <th>D.Y Merawat</th>
      
      <th>TOTAL</th>
   
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;
    foreach($model->searchLaporan() as $item)
    {

      $selisih_hari = 1;

      if(!empty($item->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($item->tanggal_masuk), Yii::app()->helper->convertSQLDate($item->tanggal_keluar));
      }

 

      $rawatInap = $item;
      $rawatRincian = $rawatInap->trRawatInapRincians;
      $total_irna = 0;
      $total_irna = $total_irna + $rawatInap->kamar->biaya_kamar * $selisih_hari;
      $total_irna = $total_irna + $rawatRincian->askep_kamar;
      
      if(!empty($rawatRincian))
        $total_irna = $total_irna + $rawatRincian->asuhan_nutrisi;

      

      foreach($rawatInap->tRIVisiteDokters as $visite)
      {
        if(!empty($visite))
          $total_irna = $total_irna + $visite->biaya_visite_irna * $visite->jumlah_visite;
      }
   

     
      $biaya_irna_top_total = 0;
      foreach($rawatInap->trRawatInapTops as $top)
      {
          $jumlah_tindakan = $top->jumlah_tindakan;
          $biaya_irna = $top->biaya_irna;

          $biaya_irna_top_total = $biaya_irna_top_total + $biaya_irna * $jumlah_tindakan;
      }

      $total_irna = $total_irna + $biaya_irna_top_total;

      $biaya_irna_tnop_total = 0;
      foreach($rawatInap->trRawatInapTnops as $tnop)
      {
          $jumlah_tindakan = $tnop->jumlah_tindakan;
          $biaya_irna = $tnop->biaya_irna;
        

          $biaya_irna_tnop_total = $biaya_irna_tnop_total + $biaya_irna * $jumlah_tindakan;
      }

      $total_irna = $total_irna + $biaya_irna_tnop_total;

      foreach($rawatInap->trRawatInapPenunjangs as $supp)
      {
          $total_irna = $total_irna + $supp->biaya_irna * $supp->jumlah_tindakan;
      }

      $biaya_obat_irna_total = 0;
      foreach($rawatInap->trRawatInapAlkes as $obat)
      {
        $jumlah_tindakan = $obat->jumlah_tindakan;
        $biaya_irna = $obat->biaya_irna;
        $biaya_obat_irna_total = $biaya_obat_irna_total + $biaya_irna * $jumlah_tindakan;
      }

      $total_irna = $total_irna + $biaya_obat_irna_total;

      $biaya_irna_lain = 0;
      foreach($rawatInap->trRawatInapLains as $lain)
      {
        $jumlah_tindakan = $lain->jumlah_tindakan;
        $biaya_irna = $lain->biaya_irna;
        $biaya_irna_lain = $biaya_irna_lain + $biaya_irna * $jumlah_tindakan;
        
      }

      $total_irna = $total_irna + $biaya_irna_lain;
  ?>
    <tr>
     <td><?php echo $i++;?></td>
      <td><?php echo $item->pASIEN->NoMedrec;?></td>
      <td><?php echo $item->pASIEN->NAMA;?></td>
      <td><?php echo $item->jenisPasien->NamaGol;?></td>
      <td><?php echo $item->kamar->kelas->nama_kelas;?></td>
      <td><?php echo $item->kamar->nama_kamar;?></td>
      <td><?php echo $item->pASIEN->FULLADDRESS;?></td>
      <td><?php echo $item->tanggal_masuk;?></td>
      <td><?php echo $item->tanggal_keluar;?></td> 
      <td>
<?php 
      
        echo $selisih_hari;
?>
      </td>
      <td>
      <?php 
      
      if(!empty($item->dokter))
        echo $item->dokter->NICKNAME;
      ?></td>
     
    
      <td><a href="<?php echo Yii::app()->createUrl('trRawatInap/input',array('id'=>$item->id_rawat_inap));?>">Rp <?php echo Yii::app()->helper->formatRupiah($total_irna);?></a></td>
    </tr>
    <?php 
    }
    ?>
  </tbody>
</table>
<?php
  }
?>

<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');

  $cs->registerScriptFile($baseUrl.'/assets/enter-keypress.js');
?>

                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>