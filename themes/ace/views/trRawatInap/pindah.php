<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/index')),
                            array('name' => 'List')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }
  .ui-datepicker { 
  margin-top:  100px;
  z-index: 1000;
}
</style>

<div class="row">
    <div class="col-xs-12">
        
                        
  <?php 

  foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
    

                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
             
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?>    

	<div class="form-group">
    <?php echo $form->labelEx($model,'pasien_id',array('class'=>'col-sm-3 control-label no-padding-right'));?>
      <div class="col-sm-9">
        <input type="text" readonly value="<?php echo $model->pASIEN->NAMA;?>"/>
        <?php
          echo $form->hiddenField($model,'pasien_id');

        
         ?>
      </div>
    </div>
 <div class="form-group">
      <?php echo CHtml::label('Jenis Pasien','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
        <div class="col-sm-9">
        <input type="text" readonly value="<?php echo $model->jenisPasien->NamaGol;?>"/>
        <?php echo $form->hiddenField($model,'jenis_pasien');?>
       
        </div>
      </div>
     <div class="form-group">
    <?php echo $form->labelEx($model,'tanggal_masuk',array('class'=>'col-sm-3 control-label no-padding-right'));?>
      <div class="col-sm-9">
      <input type="text" readonly value="<?php echo $model->tanggal_masuk.' '.$model->jam_masuk;?>"/>
      

      </div>
    </div>
   <div class="form-group">
      <?php echo CHtml::label('Dari Kamar','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
        <div class="col-sm-9">
        <input type="text" readonly value="<?php echo $model->kamar->nama_kamar.' | '.$model->kamar->kelas->nama_kelas;?>"/>
        </div>
      </div>
      <div class="form-group">
      <?php echo CHtml::label('Pindah ke Kamar','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
        <div class="col-sm-9">
          <?php 
      $list_kamar = CHtml::listData(DmKamar::model()->findAll(array('order'=>'nama_kamar ASC')),'id_kamar',function($kelas){
      $full = $kelas->nama_kamar.' - '.$kelas->kelas->nama_kelas;
      return CHtml::encode($full);
    });

       echo $form->dropDownList($model, 'kamar_id',$list_kamar);
             ?>
        </div>
      </div>
      <div class="form-group">
      <?php echo CHtml::label('Naik Kelas','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
        <div class="col-sm-9">
          <?php 

       echo $form->dropDownList($model, 'is_naik_kelas',['Tidak','Ya']);
             ?>
        </div>
      </div>
     <div class="form-group">
    <?php echo CHtml::label('Tanggal Pindah','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
      <div class="col-sm-9">

      <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'datetime_keluar',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        
                    ),
        ));
   
        ?>
       
        <?php echo $form->error($model,'tanggal_keluar'); ?>
      </div>
    </div>
   <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Submit
          </button>

        
        </div>
      </div>
   
  <?php $this->endWidget();?>

<script>

function isiDataPasien(ui)
{
	 $("#TrRawatInap_pasien_id").val(ui.item.id);
	 $("#TrRawatInap_PASIEN_NAMA").val(ui.item.value); 
	 

}

</script>
                    </div>
                </div>
