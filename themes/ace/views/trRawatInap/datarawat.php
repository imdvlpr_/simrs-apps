<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>


<script type="text/javascript">
    
    $(document).ready(function(){
        $('#search').change(function(){
    
            $('#tabel-rawat-inap').yiiGridView.update('tabel-rawat-inap', {
                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()   
            });
          
         });

         $('#size').change(function(){
        
            $('#tabel-rawat-inap').yiiGridView.update('tabel-rawat-inap', {
                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()    
            });
        
         });     
    });
</script>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3" id="sidebar">

    <?php echo $this->renderPartial('/layouts/nav_left');?>
        </div>
        
        <!--/span-->
        <div class="span9" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/index')),
                            array('name' => 'List')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Data Rawat Inap</div>
                      <div class="pull-right">Data per halaman
                            <?php echo CHtml::dropDownList('TrPendaftaranRJalan[PAGESIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,20=>20,30=>30,),array('id'=>'size','size'=>1)); ?> <?php
        echo CHtml::textField('TrPendaftaranRJalan[SEARCH]','',array('placeholder'=>'Cari','id'=>'search')); 
        ?>   </div> 
                    </div>
                    <div class="block-content collapse in">
                        
 
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
    

$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabel-rawat-inap',
  'dataProvider'=>$model->searchDistinct(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),

     array(
        'header' => 'No RM',
        'value' => '$data->pASIEN->NoMedrec'
    ),
    array(
        'header' => 'Nama Pasien',
        'value' => '$data->pASIEN->NAMA." / ".$data->jenisPasien->NamaGol'
    ),
    array(
        'header' => 'Kamar',
        'value' => '$data->kamar->nama_kamar." | ".$data->kamar->kelas->nama_kelas'
    ),
    'tanggal_masuk',
    'tanggal_keluar',
    'jam_masuk',
    'jam_keluar',
    array(
      'class'=>'CButtonColumn',
      'template'=>'{kwitansi} {pulang}',
      'buttons'=>array(
           
           	
           	'kwitansi' => array(
              'url'=>'Yii::app()->createUrl("trRawatInap/kwitansi/", array("id"=>$data->kode_rawat))',   
              'label'=>'<span class="label label-info"><i class="icon-print"></i> Cetak Kwitansi</span>',
              'options' => array('title'=>'Cetak Kwitansi','target'=>'_blank'),      
            ),
           
            'pulang' => array(
              'url'=>'Yii::app()->createUrl("trRawatInap/pulang/", array("id"=>$data->id_rawat_inap))',   
              'label'=>'<span class="label label-warning"><i class="icon-remove-sign"></i> Pasien PULANG</span>',
              'options' => array('title'=>'Pasien Pulang'),      
            ),
        ),
      
    ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
  'pager'=>array(
                        'class'=>'SimplePager',                  
                        'header'=>'',                      
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',        
                        'lastPageLabel'=>'Terakhir',  
            'firstPageCssClass'=>'btn',
                        'previousPageCssClass'=>'btn',
                        'nextPageCssClass'=>'btn',        
                        'lastPageCssClass'=>'btn',
            'hiddenPageCssClass'=>'disabled',
            'internalPageCssClass'=>'btn',
            'selectedPageCssClass'=>'btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
  'summaryCssClass'=>'table-message-info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
  'pagerCssClass'=>'btn-group paging_full_numbers',
)); ?>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>