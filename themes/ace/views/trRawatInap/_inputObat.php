
              <fieldset>
            
<style type="text/css">

  table > tfoot{
    text-align: right;
  }

  .jumlah{
    width:30px;
  }
</style>
 <div id="alert"></div>
 <div class="row">
    <div class="form-group">
      <div class="col-xs-1"><strong>Tgl</strong></div>
      <div class="col-xs-3">

        <input type="text" name="tgl_input_obat" class="date-picker input-sm" id="tgl_input_obat" data-date-format="dd/mm/yyyy"/>
      </div>
  </div>
    <div class="form-group">
    <div class="col-xs-1"><strong>Obat</strong></div>
    <div class="col-xs-3">
      <input type="text" name="nama_obat" placeholder="Ketik Kode/Nama Obat" class="input-sm" id="nama_obat"/>
      <input type="hidden" name="id_obat" class="input-sm" id="id_obat"/>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-1"><strong>Satuan</strong></div>
    <div class="col-xs-3">
      <input type="text" name="satuan_obat" class="input-sm" id="satuan_obat"/>
    
    </div>
  </div>
  
</div>
<div class="row">
  <div class="form-group">
    <div class="col-xs-1"><strong>Dokter</strong></div>
    <div class="col-xs-3">
      <input type="text" name="nama_dokter" placeholder="Ketik Nama Dokter" class="nama_dokter input-sm" id="nama_dokter"/>
      <input type="hidden" name="id_dokter"  id="id_dokter"/>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-1"><strong>Harga</strong></div>
    <div class="col-xs-3">
      <input type="hidden" name="harga_obat_satuan" class="input-sm uangnocalc" id="harga_obat_satuan"/>
      <input type="text" name="harga_obat_total" class="input-sm uangnocalc" id="harga_obat_total"/>
  </div>
      </div>
   <div class="form-group">
    <div class="col-xs-1"><strong>Jumlah</strong></div>
    <div class="col-xs-3">
      <input type="text" name="jumlah_obat" class="input-sm" id="jumlah_obat"/>
      <img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
    </div>
  </div>

</div>
   <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="button" id="btn-simpan">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>

        
        </div>
      </div>
<!-- <a class="initialism basic_open btn btn-success" href="#basic">Basic</a> -->


 

<?php 


 
$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabel-rawat-inap-obat',
  'dataProvider'=>$alkesObat->search($rawatInap->id_rawat_inap),

  // 'filter'=>$model,
  'columns'=>array(
    array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            
        ),

    
    'keterangan',

    'tanggal_input',
    [
      'header' => 'Qty',
      'value' => '$data->jumlah'
    ],
    'satuan',
    'dokter.nama_dokter',
      array(
          'name'=>'Nilai',
          'type'=>'raw',
          'value' => 'Yii::app()->helper->formatRupiah($data->nilai)',
          'footer'=>'<div style="text-align:right;width:100%;font-weight:bold">'.Yii::app()->helper->formatRupiah($rawatInap->sumObat).'</div>',
          'htmlOptions'=>array('style' => 'text-align: right;'),
          
    ),
    array(
      'class'=>'CButtonColumn',
      'template'=>' {delete}',
      'buttons' => array(
          // 'update' => array(
            
          //   'type' => 'raw',
          //   'click' => new CJavaScriptExpression('function(e) {
          //         e.preventDefault();
          //         jQuery.yii.submitForm(document.body, $(this).attr("href"), {});
          //         return false;
          //     }'),
          // ),
          'delete' => array(
            'url'=>'Yii::app()->createUrl("trRawatInap/deletePasienObat/", array("id"=>$data->id))',   
          ),
      ),
      
    ),

  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),

  'itemsCssClass'=>'table  table-bordered table-hover',
  'summaryCssClass'=>'table-message-info',
  
  'summaryText'=>'',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
  
));
?>    
  </fieldset>

<script>


$(document).bind("keyup.autocomplete",function(){
   $('.nama_dokter').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#id_dokter').val(ui.item.id);
        $('#nama_obat').focus();        
      },
      focus: function (event, ui) {
        $('#id_dokter').val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});


function saveObat(){
  var obats = [];

  var i = 0;

  var tgl = $('#tgl_input_obat').val();
  var dokter = $('#id_dokter').val();
  var jumlah = $('#jumlah_obat').val();
  var harga = $('#harga_obat_total').val();
  var idobat = $('#id_obat').val();
  var namaobat = $('#nama_obat').val();
  var satuan = $('#satuan_obat').val();
   
  obat = new Object;
  obat.id = idobat;
  obat.nama = namaobat;
  obat.harga = harga;
  obat.dokter = dokter;
  obat.jumlah = jumlah;
  obat.tanggal = tgl;
  obat.satuan = satuan;

  $.ajax({
    type : 'post',
    url : '<?=Yii::app()->createUrl('trRawatInap/ajaxObatPasien');?>',
    
    data : {dataObat:obat,rid:<?=$rawatInap->id_rawat_inap;?>},
    beforeSend : function(){
      $('#loading').show();
    },
    success : function(res){
      var res = $.parseJSON(res);
     
      $('#alert').fadeIn(100);
      $('#alert').html('<div class="alert alert-'+res.shortmsg+'">'+res.message+'</div>');
      $('#loading').hide();
      
      if(res.shortmsg=='success'){
        $('#alert').fadeOut(2000);
        updateTable();
      }

      
    },
  });
}

function updateTable(){
  $('#tabel-rawat-inap-obat').yiiGridView.update('tabel-rawat-inap-obat', {

      url:'<?=Yii::app()->createUrl('trRawatInap/inputObat',array('id'=>$rawatInap->id_rawat_inap));?>&filter='+$('#search').val(),
     
  });
}

$(document).on("keydown","#jumlah_obat",function(e){
  if(e.which=='13'){
    var jml = $('#jumlah_obat').val();
    var harga = $('#harga_obat_satuan').val();
    $('#harga_obat_total').val(eval(harga * jml));
    $('#btn-simpan').focus();
    $('#btn-simpan').click();  
  }
  
  else{
    var jml = $('#jumlah_obat').val();
    if(!isNaN(jml)){
      var harga = $('#harga_obat_satuan').val();
      $('#harga_obat_total').val(eval(harga * jml));
    }
  }
});

$(document).on("keyup","#jumlah_obat",function(e){
  var jml = $('#jumlah_obat').val();
  if(!isNaN(jml)){
    var harga = $('#harga_obat_satuan').val();
    $('#harga_obat_total').val(eval(harga * jml));
  }

});

$(document).ready(function(){
  
  $('#search').change(function(){
            
     updateTable();
    
   });



  $('#btn-simpan').on('click',function(){

    saveObat();
    return false;
  });


});

$(document).on("keydown.autocomplete","#nama_obat",function(e){
  $('#nama_obat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#id_obat').val(ui.item.id);
        $('#harga_obat_satuan').val(ui.item.hj);
        $('#harga_obat_total').val(ui.item.hj);
        $('#satuan_obat').val(ui.item.satuan);
        $('#jumlah_obat').focus();
                
      },
      focus: function (event, ui) {
        $('#id_obat').val(ui.item.id);
        $('#harga_obat_satuan').val(ui.item.hj);
        $('#harga_obat_total').val(ui.item.hj);
        $('#satuan_obat').val(ui.item.satuan);
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetObatLikeParam');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});




</script>

