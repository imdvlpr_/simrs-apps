<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/obatPasien')),
                           
                            array('name' => 'Input Obat')
);


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>

<div class="row">
    <div class="col-xs-12">
                        <div class="muted pull-left">Input Obat Pasien</div>
                    
<?php 

  $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts.nav_dataPasien'); 
$this->renderPartial('_inputObat', 
    array(
        'model'=>$model,
        'rawatInap' => $rawatInap,
        'pasien' => $pasien,
        'listDokter' => $listDokter,
        'jenisVisite' => $jenisVisite,
        'rawatRincian' => $rawatRincian,
        'rits' => $rits,
        'alkesObat' =>$alkesObat
        )
    ); ?>
                    </div>
                </div>
<?php 
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();

$cs->registerCssFile($baseUrl.'/assets/css/bootstrap-datepicker3.min.css');
$cs->registerScriptFile($baseUrl.'/assets/js/bootstrap-datepicker.min.js');
?>
