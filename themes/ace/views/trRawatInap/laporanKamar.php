<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="container-fluid">
    <div class="row-fluid">
        
        
        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Laporan Kamar','url'=>Yii::app()->createUrl('trRawatInap/laporanKamar')),
                            array('name' => 'Laporan Dokter','url'=>Yii::app()->createUrl('trRawatInap/laporanDokter')),
                            
                          ),
                          'delimiter' => ' || ', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Data Rawat Inap</div>
                      <div class="pull-right"> </div> 
                    </div>
                    <div class="block-content collapse in">
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    <legend>
      

    </legend>
    
  <div class="control-group">
      <?php echo CHtml::label('Kamar','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 
      $list_kamar = CHtml::listData(DmKamar::model()->findAll(array('order'=>'nama_kamar ASC')),'id_kamar',function($kelas){
      $full = $kelas->nama_kamar.' - '.$kelas->kelas->nama_kelas;
      return CHtml::encode($full);
    });

       echo $form->dropDownList($model, 'kamar_id',$list_kamar);
             ?>
        </div>
      </div>
    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('Lihat Laporan',array('class'=>'btn btn-success')); ?>
    </div>
  </fieldset>
  <?php $this->endWidget();?>


<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
?>

                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>