<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>

<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span3" id="sidebar">
    <?php //echo $this->renderPartial('nav-left');?>
        </div>
         -->
        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/index')),
                           
                            array('name' => 'Input Tindakan')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Data Rawat Inap</div>
                      <div class="pull-right"> </div> 
                    </div>
                    <div class="block-content collapse in">

<?php $this->renderPartial('_formBpjs', 
    array(
        'model'=>$model,
        'rawatInap' => $rawatInap,
        'pasien' => $pasien,
        'listDokter' => $listDokter,
        'jenisVisite' => $jenisVisite,
        'rawatRincian' => $rawatRincian
        )
    ); ?>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>