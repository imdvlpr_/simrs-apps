              <?php 

$total_ird = 0;
$total_irna = 0;

?>          


<table width="100%" style="padding: 2px 3px">
   <tr >
     <td colspan="7" align="center">
       RUMAH SAKIT UMUM DAERAH KABUPATEN KEDIRI<br>
       RINCIAN BIAYA PERAWATAN
     </td>
     
   </tr>
 </table>
 <table width="100%">
   <tr >
     <td>Nama Pasien</td>
     <td colspan="3">: <strong><?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?></strong></td>
     <td>No Reg</td>
     <td colspan="2">: <?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk</td>
     <td colspan="3">: <?php echo $rawatInap->tanggal_masuk;?></td>
     <td>Tgl Keluar</td>
     <td colspan="2">: <?php echo !empty($rawatInap->tanggal_keluar) ? $rawatInap->tanggal_keluar : '-' ; ?></td>
   </tr>
   <tr>
     <td>Dirawat/R. Kelas</td>
     <td colspan="3">: 
     <?php 
     $nama_kelas = $rawatInap->kamar->kelas->nama_kelas;
     echo $rawatInap->kamar->nama_kamar.' / '.$nama_kelas;

     ?>
       
     </td>

     <td>Lm. Dirawat</td>
     <td colspan="2">: 
        <?php 

        $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
           $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }
        else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }


       

        echo $selisih_hari.' hari';

       

        

        ?>

     </td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td colspan="3">: <?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Dokter yang merawat</td>
     <td colspan="2">: 
    <?php 
    $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->NICKNAME;
    }
      ?>
      <strong><?php echo $nama_dokter;?></strong>

        </td>

     
   </tr>
 </table>

<style type="text/css">
  td.border_atas{
    border-top-color:#000000;
    border-top-width:1px;
    border-top-style:solid;
  }

  td.border_bawah{
    border-bottom-color:#000000;
    border-bottom-width:1px;
    border-bottom-style:solid;
  }

  td.border_kiri{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:solid;
  }
  td.border_kanan{
    border-right-color:#000000;
    border-right-width:1px;
    border-right-style:solid;
  }

  td.border_kiri_dotted{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:dotted;
  }
</style>
 <table style="padding-left: :2px">
   
   <tr >
     <td class="border_atas border_kiri border_bawah" colspan="2" align="left"><strong>A. LAYANAN</strong></td>
     <td class="border_atas border_bawah" colspan="2"  align="left">RAWAT DARURAT</td>
     <!-- <td>&nbsp;</td> -->
     <td class="border_atas border_bawah border_kiri" colspan="2" align="left">RAWAT INAP</td>
     <!-- <td>&nbsp;</td> -->
     <td class="border_atas border_bawah border_kanan" align="left">TARIF</td>
   </tr>
   
   
    <tr>
     <td colspan="2">1. Observasi</td>
     <td>&nbsp;</td>
     <td align="right"><?php 

     echo Yii::app()->helper->formatRupiah($rawatRincian->obs_ird);?></td>
     <td colspan="2" class="border_kiri_dotted">1. Biaya Kamar</td>
     <td align="right"><?php 

     if(!empty($rawatRincian))
        $total_ird = $total_ird + $rawatRincian->obs_ird;
   //  echo number_format($rawatInap->kamar->biaya_kamar * $selisih_hari,0,',','.');
       
     $isneonatus = $rawatInap->kamar->nama_kamar == 'NEONATUS';

      if($isneonatus)
      {


        $biaya_neonatus = $rawatInap->biaya_kamar;
        
         echo Yii::app()->helper->formatRupiah($biaya_neonatus);
        $total_irna = $total_irna + $biaya_neonatus;
      }

      else
      {
         echo Yii::app()->helper->formatRupiah($rawatInap->kamar->biaya_kamar * $selisih_hari);
          $total_irna = $total_irna + $rawatInap->kamar->biaya_kamar * $selisih_hari;
      } 
    
     //$total_irna += $rawatRincian->askep_kamar;

      $total_irna += $rawatInap->kamar->biaya_askep * $selisih_hari;
      
     if(!empty($rawatRincian))
        $total_irna += $rawatRincian->asuhan_nutrisi;
      
     ?>
     </td>
   </tr>
   <tr>
     <td colspan="2">2. Pengawasan Dokter</td>
     <td ><strong>
    <?php echo $nama_dokter_ird;?></strong></td>
     <td align="right"> <?php 

     echo Yii::app()->helper->formatRupiah($rawatRincian->biaya_pengawasan);

     if(!empty($rawatRincian))
        $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;
     ?>
     </td>
     <td colspan="2" class="border_kiri_dotted">2. Askep <?php echo $rawatInap->kamar->nama_kamar;?></td>
     <td align="right"> <?php

    //  echo number_format($rawatInap->kamar->biaya_askep * $selisih_hari,0,',','.');
    echo Yii::app()->helper->formatRupiah($rawatInap->kamar->biaya_askep * $selisih_hari); 
    
    ?>

    <!-- <a href="javascript:void(0)" id="input_askep" class="btn btn-warning" title="Input Askep Harian">Input</a> -->
     </td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td colspan="2" class="border_kiri_dotted">3. Asuhan Nutrisi</td>
     <td align="right">
     
     <?php 
     echo Yii::app()->helper->formatRupiah($rawatRincian->asuhan_nutrisi);
    
     ?>
  
     </td>
   </tr>
   
   
   <tr>
     <td colspan="4" align="left"><strong>B. PELAYANAN MEDIK</strong></td>
     <td colspan="3"  class="border_kiri_dotted"></td>
   </tr>
   
   <?php 



    $urutan = 1;
      if(!empty($rawatInap->tRIVisiteDokters)){

      foreach($rawatInap->tRIVisiteDokters as $visite)
      {
       
        $biaya_visite_ird = 0;
        if(!empty($visite)){
          $total_ird = $total_ird + $visite->biaya_visite ;//* $visite->jumlah_visite_ird;
          $biaya_visite_ird = $biaya_visite_ird + $visite->biaya_visite;// * $visite->jumlah_visite_ird;
        }

        $biaya_visite_irna = 0;
        if(!empty($visite)){
          $total_irna = $total_irna + $visite->biaya_visite_irna; //* $visite->jumlah_visite;
          $biaya_visite_irna = $biaya_visite_irna + $visite->biaya_visite_irna ;//* $visite->jumlah_visite;
        }



        $dokterVisiteIRD = '';
        $dokterVisiteIRNA = '';
        if(!empty($visite->dokterIrd))
            $dokterVisiteIRD = ' || <strong>'.$visite->dokterIrd->FULLNAME.'</strong>';

        if(!empty($visite->dokterIrna))
            $dokterVisiteIRNA = $visite->dokterIrna->FULLNAME;


    ?>
    <tr>
     <td colspan="3" >
     <?php echo $urutan++;?>. 
    <?php echo $visite->jenisVisite->nama_visite;?>
    <strong><?php echo $dokterVisiteIRD;?></strong>
    </td>
     <td align="right">
     <?php echo Yii::app()->helper->formatRupiah($biaya_visite_ird);?>
    
     </td>
     
    <td colspan="2"  class="border_kiri_dotted">
     <strong><?php echo $dokterVisiteIRNA;?></strong>
   
    </td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_visite_irna);?> 
    
     </td>

   </tr>
   <?php 
      }
    }

   ?>
   
 
  <tr>
     <td colspan="4" align="left"><strong>I. Tindakan Medik Operasi</strong></td>
     <td colspan="3"  class="border_kiri_dotted"></td>
   </tr>
  
<?php 



   $listTop = TindakanMedisOperatif::model()->findAll();
   $i = 0;


   foreach($listTop as $top){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_top' => $top->id_tindakan
      );
      $itemTop = TrRawatInapTop::model()->findByAttributes($attr);

      $jumlah_tindakan = 0;
      $biaya_irna = 0;

      $valueJm = '';
      $valueAnas = '';
      $biaya_irna_top_total = 0;
      if(!empty($itemTop)){
        $jumlah_tindakan = $itemTop->jumlah_tindakan;
        $biaya_irna = $itemTop->biaya_irna;

        $biaya_irna_top_total = $biaya_irna_top_total + $biaya_irna;// * $jumlah_tindakan;
       
        
      }


      if($biaya_irna == 0)
            continue; 
      $total_irna = $total_irna + $biaya_irna_top_total;
        
       

    ?>
   
   <tr>
     
     <td colspan="4">
     <?php echo strtolower(chr(64 + ($i+1)));?>. <?php echo $top->nama_tindakan;?></td>
     
     <td colspan="2"  class="border_kiri_dotted">
       <?php 



        if($top->kode_tindakan == 'JMO')
        {

          $drJM = '';

          $idDrJM = '';

          foreach($rawatInap->trRawatInapTopJm as $topjm)
          {
              $drJM = !empty($topjm->dokter) ? $topjm->dokter->FULLNAME : '-';
          }

        

       ?>
        <strong><?php echo $drJM;?></strong>

       <?php 

       }

       else if($top->kode_tindakan == "JMA")
       {
          $drAnas = '';
          foreach($rawatInap->trRawatInapTopJa as $topja)
          {
              $drAnas = !empty($topja->dokter) ? $topja->dokter->FULLNAME : '-';
          }
          

       ?>
       <strong><?php echo $drAnas;?></strong>

       <?php 
     }


       ?>
     </td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_irna);?>
     </td>
   </tr>
     <?php
 $i++;
    }

    ?>
  <tr>
     <td colspan="4" align="left"><strong>II. Tindakan Medik Non-Operasi</strong></td>
     <td colspan="3"  class="border_kiri_dotted"></td>
   </tr>
  
   
   
<?php 


   $listTnop = TindakanMedisNonOperatif::model()->findAll();
   $i = 0;

   foreach($listTnop as $tnop){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_tnop' => $tnop->id_tindakan
      );
      $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);


      $jumlah_tindakan = 0;
      $biaya_irna = 0;
      $biaya_ird = 0;
      $biaya_irna_tnop_total = 0;

      if(!empty($itemTnop)){
        $jumlah_tindakan = $itemTnop->jumlah_tindakan;
        $biaya_irna = $itemTnop->biaya_irna;
        $biaya_ird = $itemTnop->biaya_ird;

        $biaya_irna_tnop_total = $biaya_irna_tnop_total + $biaya_irna ;//* $jumlah_tindakan;
      }


      if($biaya_ird == 0 && $biaya_irna == 0)
        continue;

      $total_ird = $total_ird + $biaya_ird;
      $total_irna = $total_irna + $biaya_irna_tnop_total;
        
    ?>
   <tr>
     <td colspan="2"><?php echo strtolower(chr(64 + ($i+1)));?>. <?php echo $tnop->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_ird);?></td>
     <td  class="border_kiri_dotted"></td>
     <td>&nbsp;</td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_irna_tnop_total);?>
     </td>
   </tr>
     <?php
 $i++;
    }

    ?>
   
   
   
   
   <tr>
     <td colspan="4" align="left"><strong>C. LAYANAN PENUNJANG</strong></td>
     <td colspan="3"  class="border_kiri_dotted"></td>
   </tr>
   
   
<?php 



    $urutan = 1;

    $biaya_supp_irna = 0;
      foreach($rawatInap->trRawatInapPenunjangs as $supp)
      {
         
         if($supp->biaya_ird ==00 && $supp->biaya_irna ==0) continue;

          $total_ird = $total_ird + $supp->biaya_ird;
          // $total_irna = $total_irna + $supp->biaya_irna;
          $biaya_supp_irna = $biaya_supp_irna + $supp->biaya_irna;// * $supp->jumlah_tindakan;

         $dokter_usg = '';
        

          if(!empty($supp->dokter_id) || $supp->dokter_id == 0)
          {
             $dokter = DmDokter::model()->findByPk($supp->dokter_id);

             $dokter_usg = !empty($dokter) ? $dokter->FULLNAME : '';
          }
        
    ?>
     <tr>
     <td colspan="2"><?php echo $urutan++;?>. 
       <?php echo $supp->penunjang->nama_tindakan;?>

     </td>
     <td>&nbsp;</td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($supp->biaya_ird);?></td>
     <td  class="border_kiri_dotted"> <strong><?php echo $dokter_usg;?></strong></td>
     <td >&nbsp;</td>
     <td align="right">

      <?php 
      echo Yii::app()->helper->formatRupiah($supp->biaya_irna);
      ?>
     </td>
   </tr>
    <?php 
    
    }

     $total_irna = $total_irna + $biaya_supp_irna;

    ?>
  
   
   
   <tr>
     <td colspan="4" align="left"><strong>D. OBAT dan ALAT KESEHATAN</strong></td>
     <td colspan="3"  class="border_kiri_dotted"></td>
   </tr>
   
   

<?php 

   $listObat = ObatAlkes::model()->findAll();
   $i = 0;
   foreach($listObat as $obat){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_alkes' => $obat->id_obat_alkes
      );
      $itemObat = TrRawatInapAlkes::model()->findByAttributes($attr);

      $jumlah_tindakan = 0;
      $biaya_irna = 0;
      $biaya_ird = 0;

      $biaya_obat_ird_total = 0;
      $biaya_obat_irna_total = 0;
      if(!empty($itemObat)){
        $jumlah_tindakan = $itemObat->jumlah_tindakan;
        $biaya_irna = $itemObat->biaya_irna;
        $biaya_ird = $itemObat->biaya_ird;
        $biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird;
        $biaya_obat_irna_total = $biaya_obat_irna_total + $biaya_irna ;//* $jumlah_tindakan;
      }

      if($biaya_ird == 0 && $biaya_irna == 0)
        continue;

      $total_ird = $total_ird + $biaya_obat_ird_total;
      $total_irna = $total_irna + $biaya_obat_irna_total;
        
    ?>
    <tr>
     <td colspan="2"><span><?php echo ($i+1);?>. </span><?php echo $obat->nama_obat_alkes;?></td>
     <td></td>
     <td align="right">

     <?php echo Yii::app()->helper->formatRupiah($biaya_ird);?>

       
     </td>
    <td  class="border_kiri_dotted"></td>
     <td></td>
     <td align="right">
    <?php echo Yii::app()->helper->formatRupiah($biaya_obat_irna_total);?>
     </td>
   </tr>
   <?php 
   $i++;
   }

   ?>
   
   
   
   <tr>
    <td colspan="4" align="left"><strong>E. Lain-lain</strong></td>
     <td colspan="3"  class="border_kiri_dotted"></td>
     
   </tr>
   
   
   
<?php 

   $listLain = TindakanMedisLain::model()->findAll();
   $i = 0;
   foreach($listLain as $lain){
       $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_lain' => $lain->id_tindakan
      );



      $itemLain = TrRawatInapLain::model()->findByAttributes($attr);
      $jumlah_tindakan = 0;
      $biaya_irna = 0;
      $biaya_ird = 0;
      $biaya_irna_lain = 0;
      if(!empty($itemLain)){
        $jumlah_tindakan = $itemLain->jumlah_tindakan;
        $biaya_irna = $itemLain->biaya_irna;
        $biaya_ird = $itemLain->biaya_ird;
        $biaya_irna_lain = $biaya_irna;// * $jumlah_tindakan;
      }


      if($biaya_ird == 0 && $biaya_irna == 0)
        continue;

      $total_ird = $total_ird + $biaya_ird;
      $total_irna = $total_irna + $biaya_irna_lain;
    ?>
    <tr>
     <td colspan="2"><?php echo $i+1;?>. <?php echo $lain->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_ird);?></td>
     <td  class="border_kiri_dotted"></td>
     <td>&nbsp;</td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_irna);?>
     </td>
   </tr>
   <?php 
   $i++;
      }
   ?>
     
   
      
      <tr>
       <td>&nbsp;</td>
       <td colspan="6"><strong>TOTAL</strong></td>
       
     </tr>
       <tr>

         <td align="right" colspan="2"><strong>IRD</strong></td>
         <td align="right" colspan="2"><strong> <?php echo number_format($total_ird,2,',','.');?></strong></td>
         

        <td colspan="2"><strong><?php echo $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;?></strong></td>
         
         <td align="right"><strong> <?php echo number_format($total_irna,2,',','.');?></strong></td>
       </tr>
       <tr>
          <td colspan="5"></td>
         <td><strong>IRD</strong></td>
         <td  align="right"><strong> <?php echo number_format($total_ird,2,',','.');?></strong></td>
       </tr>
        <tr>
          <td colspan="5"></td>
         <td><strong>Sub Total</strong></td>
         <td align="right"><strong> <?php 
         $subtotal = $total_ird + $total_irna;
         echo number_format($subtotal,2,',','.');
         ?></strong></td>
       </tr>
       <tr>
         <td colspan="5"></td>
         <td><strong>Paket 1</strong></td>
         <td align="right"><strong> <?php echo Yii::app()->helper->formatRupiah($rawatInap->biaya_paket_1,2);?></strong></td>
       </tr>
        <tr>
         <td colspan="5"></td>
         <td><strong>Paket 2</strong></td>
         <td  align="right"><strong> <?php 
         echo Yii::app()->helper->formatRupiah($rawatInap->biaya_paket_2,2);
         ?></strong>
         </td>
       </tr>
       <tr>
         <td colspan="5"></td>
         <td><strong>Paket 3</strong></td>
         <td  align="right"><strong> <?php 
         echo Yii::app()->helper->formatRupiah($rawatInap->biaya_paket_3,2);
         ?></strong>
         </td>
       </tr>
       <tr>
          <td colspan="5"></td>
         <td><strong>BIAYA DIBAYAR</strong></td>
         <td  align="right"><strong> <?php 
         $nilai_total = 0;

        
        if(strpos($rawatInap->jenisPasien->NamaGol, 'BPJS') !== false )
        {
          $nama_kelas = $rawatInap->kamar->kelas->nama_kelas;
          // Khusus VIP
          if($nama_kelas == 'VIP' && $rawatInap->biaya_paket_1 != 0)
          {
            
            $selisih_maks = 0;
            // nilai real VIP > 175 % dari tarif INA CBG kelas 1
            if(($subtotal / $rawatInap->biaya_paket_1) > 1.75)
            {

              $selisih_maks = 0.75 * $rawatInap->biaya_paket_1;
              
            }
            
            // nilai real VIP < 175 % dari tarif INA CBG kelas 1 dan real > paket 1
            else if(($subtotal / $rawatInap->biaya_paket_1) <= 1.75 && $subtotal >= $rawatInap->biaya_paket_1)
            {
              $selisih_maks = $subtotal - $rawatInap->biaya_paket_1;
              
            }

            $selisih_inacbg = 0;
            if($rawatInap->biaya_paket_3 != 0)
            {
              $selisih_inacbg = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_3;     
            }

            else if($rawatInap->biaya_paket_2 != 0)
            {
              $selisih_inacbg = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_2;
            }

            $tambahan_pasien = $selisih_maks + $selisih_inacbg;
            $pendapatan_rs = $rawatInap->biaya_paket_1 + $tambahan_pasien;

            $nilai_total = $tambahan_pasien;
          }

          // NON VIP
          else
          {
            if($rawatInap->biaya_paket_1 != 0 && $rawatInap->biaya_paket_3 != 0)
            {
              $selisih_paket = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_3; 
            } 

            else if($rawatInap->biaya_paket_2 != 0 && $rawatInap->biaya_paket_3 != 0)
            {
              $selisih_paket = $rawatInap->biaya_paket_2 - $rawatInap->biaya_paket_3;
            }
            
            else if($rawatInap->biaya_paket_1 != 0 && $rawatInap->biaya_paket_2 != 0)
            {
              $selisih_paket = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_2;
            }

            else{
              $selisih_paket = 0;//$subtotal;
            }

            $nilai_total = $selisih_paket;
            // $nilai_total = $subtotal - $selisih_paket;
          }
        }

        else
        {
          $nilai_total = $subtotal;
        }


        $total_all = $nilai_total;
        
         echo number_format($total_all,2,',','.');

         ?></strong></td>
       </tr>
      <tr>
         <td colspan="5"></td>
         <td colspan="2"><br><br>Pare, <?php echo date('d-M-Y');?>
         </td>
       </tr>
        <tr>
         <td colspan="5"></td>
         <td colspan="2">
           Bagian Perinci&nbsp;:&nbsp;<?php echo Yii::app()->user->getState('nama');?>
         
         </td>
       </tr>
    </table>

<table width="100%" >
      <tr>
        <td width="10%">No Kwitansi</td>
        <td width="90%">:&nbsp;<?php
         //echo Yii::app()->helper->appendZeros($rawatInap->kode_rawat,6);
         ?></td>
      </tr>
      <tr>
        <td>Tanggal</td>
        <td>:&nbsp;<?php echo date('d/m/Y');?></td>
      </tr>
      <tr>
        <td>Terbilang</td>
        <td>:&nbsp;<?php echo Yii::app()->helper->terbilang($total_all);?> rupiah</td>
      </tr>
    </table>
    