  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>


              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }


        $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
          $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }

       

 
        ?>


<table class="table">
   <tr>
     <td>Nama Pasien</td>
     <td>:</td>
     <td colspan="2"><strong><?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?></strong></td>
     
     <td>No Reg</td>
     <td>:</td>
     <td><?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk Kamar / IGD</td>
     <td>:</td>
     <td colspan="2"><?php echo $rawatInap->tanggal_masuk;?>
       <?php 
       
       echo is_null($rawatInap->tanggal_masuk_ird) ? ' / '.$rawatInap->tanggal_masuk_ird : '';
       ?>
     </td>
     
     <td>Tgl Keluar</td>
     <td>:</td>
     <td>

       <?php 
        $rawatInap->datetime_keluar = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
 $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$rawatInap,
                    'attribute'=>'datetime_keluar',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        
                    ),
        ));

       // echo CHtml::textField('tanggal_keluar',!empty($rawatInap->tanggal_keluar) ? $rawatInap->tanggal_keluar : date('d/m/Y'));
       // echo !empty($rawatInap->tanggal_keluar) ? $rawatInap->tanggal_keluar : '' ; 
       ?>
     </td>
   </tr>
   <tr>
     <td>Dirawat/R. Kelas</td>
     <td>:</td>
     <td><?php 

     echo $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;

     ?></td>
     <td></td>
     <td>Lm. Dirawat</td>
     <td>:</td>
     <td>
        <?php 

       

       

        echo $selisih_hari.' hari';

        ?>

     </td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td colspan="2"><?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Dokter yang merawat</td>
     <td>:</td>
     <td>
    <?php 
    $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
      ?>
      <strong><?php echo $nama_dokter;?></strong>

     </td>

     </td>
   </tr>
 </table>


 <table class="table table-condensed" id="table_ri_rj">
     
      <thead>
      <tr>
       <th>&nbsp;</th>
       <th colspan="6"><strong>TOTAL</strong></th>
       
     </tr>
     </thead>
     <tbody >
       <tr >
         <td>&nbsp;</td>
         <td></td>
         <td><strong>IRD</strong></td>
         <td><div class="span11" style="text-align: right;"><strong>Rp <?php echo number_format($total_ird,2,',','.');?></strong></div></td>
         <td></td>
         <td><strong><?php echo $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;?></strong></td>
         <td><div class="span6" style="text-align: right"><strong>
          Rp <?php echo number_format($rawatInap->biaya_total_kamar,2,',','.');?>
            <?php 
             // $rawatInap->biaya_total_kamar = $total_irna;
         echo $form->hiddenField($rawatInap,'biaya_total_kamar');
         ?>  
          </strong></div></td>
       </tr>
       <tr >
          <td colspan="5"></td>
         <td><strong>IRD</strong></td>
         <td><div class="span6" style="text-align: right">
          <strong>
            Rp <?php echo number_format($rawatInap->biaya_total_ird,2,',','.');?>
             <?php 
             // $rawatInap->biaya_total_ird = $total_ird;
         echo $form->hiddenField($rawatInap,'biaya_total_ird');
         ?>  
            </strong>
        </div>
        </td>
       </tr>
        <tr >
          <td colspan="5"></td>
         <td><strong>Sub Total</strong></td>
         <td><div class="span6" style="text-align: right"><strong>Rp <?php 
         $subtotal = $rawatInap->biaya_total_kamar + $rawatInap->biaya_total_ird;
         echo number_format($subtotal,2,',','.');
         ?></strong></div></td>
       </tr>
       <tr >
         <td colspan="5"></td>
         <td><strong>Paket 1</strong></td>
         <td><div class="span6" style="text-align: right">
         <?php 
         echo $form->textField($rawatInap,'biaya_paket_1',array('class' => 'input-small uang'));
         ?>
           </div>
         </td>
       </tr>
        <tr >
         <td colspan="5"></td>
         <td><strong>Paket 2</strong></td>
         <td><div class="span6" style="text-align: right">
          <?php 
         echo $form->textField($rawatInap,'biaya_paket_2',array('class' => 'input-small uang'));
         ?>
           </div>
         </td>
       </tr>
       <tr >
         <td colspan="5"></td>
         <td><strong>Paket 3</strong></td>
         <td><div class="span6" style="text-align: right">
          <?php 
         echo $form->textField($rawatInap,'biaya_paket_3',array('class' => 'input-small uang'));
         ?>
           </div>
         </td>
       </tr>
       <tr style="background-color: #25ade3;color:white;font-size: 16px">
          <td colspan="5"></td>
         <td><strong>BIAYA DIBAYAR</strong></td>
         <td ><div class="span6" style="text-align: right"><strong>Rp <?php 
        
        $nilai_total = 0;


        if(strpos($rawatInap->jenisPasien->NamaGol, 'BPJS') !== false )
        {
          $nama_kelas = $rawatInap->kamar->kelas->nama_kelas;


          // Khusus VIP
          if($nama_kelas == 'VIP' && $rawatInap->biaya_paket_1 != 0)
          {
            
            $selisih_maks = 0;
            // nilai real VIP > 175 % dari tarif INA CBG kelas 1
            if(($subtotal / $rawatInap->biaya_paket_1) > 1.75)
            {

              $selisih_maks = 0.75 * $rawatInap->biaya_paket_1;
              
            }
            
            // nilai real VIP < 175 % dari tarif INA CBG kelas 1 dan real > paket 1
            else if(($subtotal / $rawatInap->biaya_paket_1) <= 1.75 && $subtotal >= $rawatInap->biaya_paket_1)
            {
              $selisih_maks = $subtotal - $rawatInap->biaya_paket_1;
              
            }

            $selisih_inacbg = 0;
            if($rawatInap->biaya_paket_3 != 0)
            {
              $selisih_inacbg = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_3;     
            }

            else if($rawatInap->biaya_paket_2 != 0)
            {
              $selisih_inacbg = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_2;
            }



            $tambahan_pasien = $selisih_maks + $selisih_inacbg;
            $pendapatan_rs = $rawatInap->biaya_paket_1 + $tambahan_pasien;

            $nilai_total = $tambahan_pasien;
          }

          // NON VIP
          else
          {
            if($rawatInap->biaya_paket_1 != 0 && $rawatInap->biaya_paket_3 != 0)
            {
              $selisih_paket = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_3; 
            } 

            else if($rawatInap->biaya_paket_2 != 0 && $rawatInap->biaya_paket_3 != 0)
            {
              $selisih_paket = $rawatInap->biaya_paket_2 - $rawatInap->biaya_paket_3;
            }
            
            else if($rawatInap->biaya_paket_1 != 0 && $rawatInap->biaya_paket_2 != 0)
            {
              $selisih_paket = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_2;
            }

            else{
              $selisih_paket = 0;//$subtotal;

            }

            // $nilai_total = $subtotal - $selisih_paket;
            $nilai_total = $selisih_paket;


          }
        }

        else if(strpos($rawatInap->jenisPasien->NamaGol, 'JAMKESDA') !== false )
        {
          $nilai_total = 0;
        }

        else if(strpos($rawatInap->jenisPasien->NamaGol, 'JR') !== false )
        {
          
            $nilai_total = $subtotal > 20000000 ? $subtotal - 20000000 : 0;
         
        }

        else
        {
          $nilai_total = $subtotal;
        }



        echo CHtml::hiddenField('subtotal',$subtotal);
        echo CHtml::hiddenField('dibayar',$nilai_total);
         $total_all = $nilai_total;
         echo number_format($total_all,2,',','.');
         $rawatInap->biaya_dibayar = $total_all;
         echo $form->hiddenField($rawatInap,'biaya_dibayar');
         
         ?></strong></div></td>
       </tr>
     </tbody>
    </table>

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit','name'=>'btn-submit')); ?>
      <?php echo CHtml::submitButton('SIMPAN & PASIEN KELUAR',array('class'=>'btn btn-danger','id'=>'btn-keluar','name'=>'btn-keluar')); ?>
     
     <?php echo $this->renderPartial('_buttonCetak',array('rawatInap'=>$rawatInap));?>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>


<?php 

?>

<script>

$('#nama_dokter_perawat').focus();

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});
</script>