  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
 <table class="table">
   <tr>
     <td>Nama Pasien</td>
     <td>:</td>
     <td colspan="2"><strong><?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?></strong></td>
     
     <td>No Reg</td>
     <td>:</td>
     <td><?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk Kamar / IGD</td>
     <td>:</td>
     <td colspan="2"><?php 
     
     echo $rawatInap->tanggal_masuk;?>
     <?php
     echo is_null($rawatInap->tanggal_masuk_ird) ? ' / '.$rawatInap->tanggal_masuk_ird : ''; 
     // echo $rawatInap->tanggal_masuk_ird;
     ?></td>
     
     <td>Tgl Keluar</td>
     <td>:</td>
     <td>
 <?php 
          $rawatInap->datetime_keluar = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');


               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$rawatInap,
                    'attribute'=>'datetime_keluar',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        // 'top' => '100'
                        
                    ),
                    

        ));
   
        ?>
     
     </td>
   </tr>
   <tr>
     <td>Dirawat/R. Kelas</td>
     <td>:</td>
     <td><?php 

     echo $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;

     ?></td>
     <td></td>
     <td>Lm. Dirawat</td>
     <td>:</td>
     <td>
        <?php 

        $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
           $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }
        else
        {
          $dnow = date('Y-m-d');

          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }


       

        echo $selisih_hari.' hari';

        ?>

     </td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td colspan="2"><?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Dokter yang merawat</td>
     <td>:</td>
     <td>
    <?php 
    $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
      ?>
      <input id="nama_dokter_perawat" placeholder="Ketik Nama Dokter" type="text" class="input-large nama_dokter_merawat" name="nama_dokter_merawat" autocomplete="off" value="<?php echo $nama_dokter;?>"/>

   
     <?php

      echo $form->hiddenField($rawatInap,'dokter_id');?>
     </td>

     </td>
   </tr>
 </table>


    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit','name'=>'btn-submit')); ?>
      <?php echo CHtml::submitButton('SIMPAN & PASIEN PULANG',array('class'=>'btn btn-danger','id'=>'btn-keluar','name'=>'btn-keluar')); ?>
     <?php echo $this->renderPartial('_buttonCetak',
  array(
    'rawatInap'=>$rawatInap
  )
);?>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>


<script>

$('#nama_dokter_perawat').focus();

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});



$(document).bind("keyup.autocomplete",function(){
   $('.nama_dokter_merawat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#TrRawatInap_dokter_id').val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $('#TrRawatInap_dokter_id').val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});




function isiTindakan(ui)
{
   $("#TrRawatInapLayanan_id_tindakan").val(ui.item.id);
   $("#TrRawatInapLayanan_nama_tindakan").val(ui.item.value); 
   

}




</script>
