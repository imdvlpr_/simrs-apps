<?php
/* @var $this SiteController */


  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<style type="text/css">
  span.label {
    margin : 1px 2px;
    padding : 3px 4px;
    
  }

  table tr.odd td.notice,table tr.even td.notice {
    background-color:#51a351;
    color:white;
}


</style>
<script type="text/javascript">

    $(document).ready(function(){
        
        $('#search, #size, #jenis_pasien').change(function(){
            


            $('#tabel-rawat-inap').yiiGridView.update('tabel-rawat-inap', {

                url:'<?php echo Yii::app()->createUrl('trRawatInap/obatPasien');?>&filter='+$('#search').val()+'&size='+$('#size').val()+'&jenis_pasien='+$('#jenis_pasien').val(),
               
            });
          
         });

        
    });
</script>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
    
 
$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabel-rawat-inap',
  'dataProvider'=>$model->search(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            
        ),

     array(
        'header' => 'No RM',
        'value' => '$data->pASIEN->NoMedrec',
        // 'cssClassExpression' => '$data->is_naik_kelas == 1 ? "notice" : ""',
    ),
    array(
        'header' => 'Nama Pasien',
        'value' => '$data->pASIEN->NAMA." / ".$data->jenisPasien->NamaGol',
        'cssClassExpression' => '$data->is_naik_kelas == 1 ? "notice" : ""',
    ),
    array(
        'header' => 'Kamar',
        'value' => '$data->kamar->nama_kamar." | ".$data->kamar->kelas->nama_kelas',

    ),
    'tanggal_masuk',
    'tanggal_keluar',
    'jam_masuk',
    'jam_keluar',
    'STATUS_INAP_PASIEN',
    // 'SUBTOTAL',
    array(
      'class'=>'CButtonColumn',
      'template'=>'{alkes}',
      'buttons'=>array(
          
            'alkes' => array(
              'url'=>'Yii::app()->createUrl("trRawatInap/inputObat/", array("id"=>$data->id_rawat_inap))',   
              'label'=>'<span class="label label-info label-lg"><i class="fa fa-pencil"></i> Input Data Obat</span>',
              'options' => array('title'=>'Input Data Obat'),      
            ),

        ),
      
    ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
  'pager'=>array(
                        'class'=>'SimplePager',                  
                        'header'=>'',                      
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',        
                        'lastPageLabel'=>'Terakhir',  
            'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
            'hiddenPageCssClass'=>'disabled',
          'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
  'summaryCssClass'=>'table-message-info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
   'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>