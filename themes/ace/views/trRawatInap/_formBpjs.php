
              <fieldset>

<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>

<?php 
 $list_data = BpjsDataForm::model()->findAll();

 $selisih_hari = 1;

if(!empty($rawatInap->tanggal_keluar))
{
  $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
}
else
{
  $dnow = date('Y-m-d');
    $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
}


?>

<table border="0" width="100%">
  <tr>
    <td>No RM</td>
    <td>: <?php echo $pasien->NoMedrec;?></td>
    <td>Tgl. Masuk</td>
    <td>: <?php echo $rawatInap->tanggal_masuk;?></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>: <?php echo $pasien->NAMA;?></td>
    <td>Tgl. Keluar</td>
    <td>: <?php echo $rawatInap->tanggal_keluar;?></td>
  </tr>
</table>
<br><br>
 <table class="table" border="1" cellpadding="2">
   <tr>
     <th width="5%">No</th>
     <th width="25%">Uraian</th>
     <th>Jumlah</th>
     <th width="5%">No</th>
     <th width="25%">Uraian</th>
     <th>Jumlah</th>
   </tr>
<?php 
$count = count($list_data);
$i2 = $count / 2;
$total_all = 0;
for($i=0;$i<$count/2;$i++)
{
   $obj = $list_data[$i];
   $obj2 = $list_data[$i2];

   $obj->jumlah = 0;
   $obj2->jumlah = 0;
   
   switch ($i+1) {
      case 1: // Prosedur Non Bedah
        
        $listTnop = TindakanMedisNonOperatif::model()->findAll();
        $total_ird = 0;
        $total_irna = 0;
        foreach($listTnop as $tnop)
        {

          
            $attr = array(
              'id_rawat_inap' => $rawatInap->id_rawat_inap,
              'id_tnop' => $tnop->id_tindakan
            );
            $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);

            $jumlah_tindakan = 1;
            $biaya_irna = 0;
            $biaya_ird = 0;
            $biaya_irna_tnop_total = 0;

            if(!empty($itemTnop))
            {
              $jumlah_tindakan = $itemTnop->jumlah_tindakan;
              $biaya_irna = $itemTnop->biaya_irna;
              $biaya_ird = $itemTnop->biaya_ird;

              $biaya_irna_tnop_total = $biaya_irna_tnop_total + $biaya_irna;
            }

            $total_ird = $total_ird + $biaya_ird;
            $total_irna = $total_irna + $biaya_irna_tnop_total;
          
        }
        $obj->jumlah = $total_ird + $total_irna + $rawatRincian->obs_ird;

        break;
      
      case 2:
        
        $listTop = TindakanMedisOperatif::model()->findAll();

        $total_irna = 0;
        foreach($listTop as $top)
        {
            $attr = array(
              'id_rawat_inap' => $rawatInap->id_rawat_inap,
              'id_top' => $top->id_tindakan
            );
            $itemTop = TrRawatInapTop::model()->findByAttributes($attr);

            if($top->kode_tindakan == 'RRA' || $top->kode_tindakan == 'RRM')
                continue;

            $jumlah_tindakan = 0;
            $biaya_irna = 0;

           
            $biaya_irna_top_total = 0;
            if(!empty($itemTop)){
              $jumlah_tindakan = $itemTop->jumlah_tindakan;
              $biaya_irna = $itemTop->biaya_irna;

              $biaya_irna_top_total = $biaya_irna_top_total + $biaya_irna;// * $jumlah_tindakan;
             
              if($biaya_irna == 0)
                  continue; 
            }


            $total_irna = $total_irna + $biaya_irna_top_total;
              
             
        }
        $obj->jumlah = $total_irna;
        break;

      case 3:

        $total_ird = 0;
        $total_irna = 0;
        if(!empty($rawatRincian))
          $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;

        if(!empty($rawatInap->tRIVisiteDokters))
        {

          foreach($rawatInap->tRIVisiteDokters as $visite)
          {
           
            if(!empty($visite))
            {
              $total_ird = $total_ird + $visite->biaya_visite;
              $total_irna = $total_irna + $visite->biaya_visite_irna;
            }
             
          }
        }

        $obj->jumlah = $total_ird+$total_irna;
        break;
      case 4:
        $obj->jumlah = $rawatRincian->asuhan_nutrisi;
        break;
      
      case 5:
       $listTop = TindakanMedisOperatif::model()->findAll();

        $total_irna = 0;
        foreach($listTop as $top)
        {
            $attr = array(
              'id_rawat_inap' => $rawatInap->id_rawat_inap,
              'id_top' => $top->id_tindakan
            );
            $itemTop = TrRawatInapTop::model()->findByAttributes($attr);

            if($top->kode_tindakan != 'RRA')
                continue;

            $biaya_irna = 0;

            $biaya_irna_top_total = 0;
            if(!empty($itemTop)){
              $jumlah_tindakan = $itemTop->jumlah_tindakan;
              $biaya_irna = $itemTop->biaya_irna;

              $biaya_irna_top_total = $biaya_irna_top_total + $biaya_irna;// * $jumlah_tindakan;
             
              if($biaya_irna == 0)
                  continue; 
            }


            $total_irna = $total_irna + $biaya_irna_top_total;
              
             
        }

        
        $obj->jumlah = $total_irna + $rawatInap->kamar->biaya_askep * $selisih_hari;
        break;

      case 6:
        $listPenunjang = TindakanMedisPenunjang::model()->findAll();
     
        $total_ird = 0;
        $total_irna = 0;
        $listExclusion = ['EKG','PA','HD','Rehab Medik','VISUM'];
        foreach($listPenunjang as $supp)
        {
          if(!in_array($supp->nama_tindakan, $listExclusion))
              continue;
          
          $attr = array(
            'id_rawat_inap' => $rawatInap->id_rawat_inap,
            'id_penunjang' => $supp->id_tindakan_penunjang
          );
          $itemSupp = TrRawatInapPenunjang::model()->findByAttributes($attr);
          
          $supp_biaya_ird = 0;
          $supp_biaya_irna = 0 ;
          $supp_jumlah_tindakan = 0;

         
          if(!empty($itemSupp)){
            // $total_ird = $total_ird + $itemSupp->biaya_ird;
           // $total_irna = $total_irna + $supp->biaya_irna;
            // $biaya_supp_irna = $biaya_supp_irna + $itemSupp->biaya_irna * $itemSupp->jumlah_tindakan;

            // $total_irna = $total_irna + $biaya_supp_irna;

            $supp_biaya_ird = $itemSupp->biaya_ird;
            $supp_biaya_irna = $itemSupp->biaya_irna;
            $supp_jumlah_tindakan = $itemSupp->jumlah_tindakan;

            $total_ird += $supp_biaya_ird;
            $total_irna += $supp_biaya_irna;
          }  

       }

        $obj->jumlah = $total_ird + $total_irna;
        break;
      case 7:
        $biaya_usg = TrRawatInapPenunjang::model()->getBiayaPenunjang($rawatInap->id_rawat_inap, 'usg');
        $biaya_foto = TrRawatInapPenunjang::model()->getBiayaPenunjang($rawatInap->id_rawat_inap, 'foto');
        $biaya_ctscan = TrRawatInapPenunjang::model()->getBiayaPenunjang($rawatInap->id_rawat_inap, 'ct scan');

        $obj->jumlah = $biaya_ctscan + $biaya_usg + $biaya_foto;
        break;
      
      case 8:
        $listPenunjang = TindakanMedisPenunjang::model()->findAll();
     
        $total_ird = 0;
        $total_irna = 0;
        foreach($listPenunjang as $supp)
        {

          if(strtolower(trim($supp->nama_tindakan)) != 'laboratorium')
              continue;
          
          $attr = array(
            'id_rawat_inap' => $rawatInap->id_rawat_inap,
            'id_penunjang' => $supp->id_tindakan_penunjang
          );
          $itemSupp = TrRawatInapPenunjang::model()->findByAttributes($attr);
          
          $supp_biaya_ird = 0;
          $supp_biaya_irna = 0 ;
          $supp_jumlah_tindakan = 0;

         
          if(!empty($itemSupp)){
            // $total_ird = $total_ird + $itemSupp->biaya_ird;
           // $total_irna = $total_irna + $supp->biaya_irna;
            // $biaya_supp_irna = $biaya_supp_irna + $itemSupp->biaya_irna * $itemSupp->jumlah_tindakan;

            // $total_irna = $total_irna + $biaya_supp_irna;

            $supp_biaya_ird = $itemSupp->biaya_ird;
            $supp_biaya_irna = $itemSupp->biaya_irna;
            $supp_jumlah_tindakan = $itemSupp->jumlah_tindakan;

            $total_ird += $supp_biaya_ird;
            $total_irna += $supp_biaya_irna;
          }  

       }

        $obj->jumlah = $total_ird + $total_irna;
        break;


    }

    switch($i2+1)
    {
      case 9 :
      $listObat = ObatAlkes::model()->findAll();
     
      $total_ird = 0;
      $total_irna = 0;
      foreach($listObat as $obat){
        $attr = array(
          'id_rawat_inap' => $rawatInap->id_rawat_inap,
          'id_alkes' => $obat->id_obat_alkes
        );
        $itemObat = TrRawatInapAlkes::model()->findByAttributes($attr);

        if(!empty($itemObat->alkes))
          if($itemObat->alkes->kode_alkes != 'DARAH')
            continue;

        $jumlah_tindakan = 1;
        $biaya_irna = $obat->tarip;
        $biaya_ird = 0;

        $biaya_obat_ird_total = 0;
        $biaya_obat_irna_total = 0;
        if(!empty($itemObat)){
          $jumlah_tindakan = $itemObat->jumlah_tindakan;
          $biaya_irna = $itemObat->biaya_irna;
          $biaya_ird = $itemObat->biaya_ird;
          $biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird;
          $biaya_obat_irna_total = $biaya_obat_irna_total + $biaya_irna * $jumlah_tindakan;
        }

        $total_ird = $total_ird + $biaya_obat_ird_total;
        $total_irna = $total_irna + $biaya_obat_irna_total;
      } 

      $obj2->jumlah = $total_ird + $total_irna;

      break;

      case 10 :
      $biaya_rehab_medik = TrRawatInapPenunjang::model()->getBiayaPenunjang($rawatInap->id_rawat_inap, 'rehab medik');
       $obj2->jumlah = $biaya_rehab_medik;
      break;

      case 11 :


        $biaya_kamar = TrRawatInapKamarHistory::model()->getBiayaKamar($rawatInap->id_rawat_inap);
        $isneonatus = $rawatInap->kamar->nama_kamar == 'NEONATUS';
        
        if($isneonatus)
        {
          $biaya_kamar = $rawatInap->biaya_kamar;
        }
        $biaya_rm = TrRawatInapLain::model()->getBiayaLainBy($rawatInap->id_rawat_inap, 'Rekam Medis');
        $penunggu_px = TrRawatInapLain::model()->getBiayaLainBy($rawatInap->id_rawat_inap, 'Penunggu Px');
        $adm = TrRawatInapLain::model()->getBiayaLainBy($rawatInap->id_rawat_inap, 'Administrasi');
        $materai = TrRawatInapLain::model()->getBiayaLainBy($rawatInap->id_rawat_inap, 'Materai');
        $rohani = TrRawatInapLain::model()->getBiayaLainBy($rawatInap->id_rawat_inap, 'Pembinaan Rohani');
        $gelang = TrRawatInapLain::model()->getBiayaLainBy($rawatInap->id_rawat_inap, 'Gelang');
        $obj2->jumlah = $biaya_kamar + $biaya_rm + $penunggu_px + $adm + $materai + $rohani + $gelang;
      break;

      case 12 :
      break;

      case 13 :
        $biaya_obat = TrRawatInapAlkes::model()->getBiayaAlkes($rawatInap->id_rawat_inap, 'OBAT');
        $biaya_infus = TrRawatInapAlkes::model()->getBiayaAlkes($rawatInap->id_rawat_inap, 'INFUS');
        $obj2->jumlah = $biaya_obat + $biaya_infus;
      break;
      case 14:
        $biaya_bhp = TrRawatInapAlkes::model()->getBiayaAlkes($rawatInap->id_rawat_inap, 'BHP');
        $obj2->jumlah = $biaya_bhp;
      break;
      case 15 :
       $biaya_bhp = TrRawatInapAlkes::model()->getBiayaAlkes($rawatInap->id_rawat_inap, 'O2');
        $obj2->jumlah = $biaya_bhp;
      break;

      case 16:
         $listTop = TindakanMedisOperatif::model()->findAll();

        $total_irna = 0;
        foreach($listTop as $top)
        {

            if($top->kode_tindakan != 'RRM')
                continue;
            $attr = array(
              'id_rawat_inap' => $rawatInap->id_rawat_inap,
              'id_top' => $top->id_tindakan
            );
            $itemTop = TrRawatInapTop::model()->findByAttributes($attr);

            

            $biaya_irna = 0;

            $biaya_irna_top_total = 0;
            if(!empty($itemTop)){
              $jumlah_tindakan = $itemTop->jumlah_tindakan;
              $biaya_irna = $itemTop->biaya_irna;

              $biaya_irna_top_total = $biaya_irna_top_total + $biaya_irna;// * $jumlah_tindakan;
             
              if($biaya_irna == 0)
                  continue; 
            }


            $total_irna = $total_irna + $biaya_irna_top_total;
              
             
        }

        
        $obj2->jumlah = $total_irna;
      break;

    } 

    $subtotal_all = $obj->jumlah + $obj2->jumlah;
  
?>
   <tr>
     <td style="text-align: justify;"><?=$i+1 ?></td>
     <td><?=$obj->nama_layanan;?></td>
     <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($obj->jumlah);?></td>
     
     <td style="text-align: justify;"><?=$i2+1;?></td>
     <td><?=$obj2->nama_layanan;?></td>
     <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($obj2->jumlah);?></td>
   </tr>
<?php 

  $total_all = $total_all + $subtotal_all;

  $i2++;
}
?>
<tr>
     <td>&nbsp;</td>
     <td></td>
     <td></td>
     
     <td></td>
     <td>Total (1 s/d 16)</td>
     <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_all);?></td>
   </tr>
 </table>


