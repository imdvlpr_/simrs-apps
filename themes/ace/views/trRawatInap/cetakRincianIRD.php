              <?php 

$total_ird = 0;


?>          


<table width="100%" style="padding: 2px 3px">
   <tr >
     <td colspan="7" align="center">
       RUMAH SAKIT UMUM DAERAH KABUPATEN KEDIRI<br>
       RINCIAN BIAYA PERAWATAN
     </td>
     
   </tr>
 </table>
 <table width="100%">
   <tr >
     <td>Nama Pasien</td>
     <td colspan="3">: <strong><?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?></strong></td>
     <td>No Reg</td>
     <td colspan="2">: <?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk</td>
     <td colspan="3">: <?php echo $rawatInap->tanggal_masuk;?></td>
     <td>Tgl Keluar</td>
     <td colspan="2">: <?php echo !empty($rawatInap->tanggal_keluar) ? $rawatInap->tanggal_keluar : '-' ; ?></td>
   </tr>
   <tr>
     <td>Dirawat/R. Kelas</td>
     <td colspan="3">: 
     <?php 
     $nama_kelas = $rawatInap->kamar->kelas->nama_kelas;
     echo $rawatInap->kamar->nama_kamar.' / '.$nama_kelas;

     ?>
       
     </td>

     <td>Lm. Dirawat</td>
     <td colspan="2">: 
        <?php 

        $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
           $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }
        else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }
        echo $selisih_hari.' hari';
        ?>

     </td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td colspan="3">: <?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Dokter yang merawat</td>
     <td colspan="2">: 
    <?php 
    $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->NICKNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->NICKNAME;
    }
      ?>
      <strong><?php echo $nama_dokter;?></strong>

        </td>

     
   </tr>
 </table>

<style type="text/css">
  td.border_atas{
    border-top-color:#000000;
    border-top-width:1px;
    border-top-style:solid;
  }

  td.border_bawah{
    border-bottom-color:#000000;
    border-bottom-width:1px;
    border-bottom-style:solid;
  }

  td.border_kiri{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:solid;
  }
  td.border_kanan{
    border-right-color:#000000;
    border-right-width:1px;
    border-right-style:solid;
  }

  
</style>
 <table style="padding-left: :2px" width="100%">
   
   <tr >
     <td class="border_atas border_kiri border_bawah" width="50%" align="left"><strong>A. LAYANAN</strong></td>
     <td class="border_atas border_bawah" align="left" width="20%">RAWAT DARURAT</td>
     <td class="border_atas border_bawah border_kanan" align="right" width="30%">TARIF</td>
   </tr>
   
   
    <tr>
     <td>1. Observasi</td>
     <td align="right"></td>
     <td align="right"><?php 
     echo Yii::app()->helper->formatRupiah($rawatRincian->obs_ird);
     if(!empty($rawatRincian))
        $total_ird = $total_ird + $rawatRincian->obs_ird;
   //  echo number_format($rawatInap->kamar->biaya_kamar * $selisih_hari,0,',','.');
       
   
     ?>
     </td>
   </tr>
   <tr>
     <td>2. Pengawasan Dokter</td>
     <td ><strong>
    <?php echo $nama_dokter_ird;?></strong></td>
     <td align="right"> <?php
      echo Yii::app()->helper->formatRupiah($rawatRincian->biaya_pengawasan);

     if(!empty($rawatRincian))
        $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;
    
    ?>

    <!-- <a href="javascript:void(0)" id="input_askep" class="btn btn-warning" title="Input Askep Harian">Input</a> -->
     </td>
   </tr>

   
   <tr>
     <td colspan="2" align="left"><strong>B. PELAYANAN MEDIK</strong></td>
     <td class="border_kiri_dotted"></td>
   </tr>
   
   <?php 



    $urutan = 1;
      if(!empty($rawatInap->tRIVisiteDokters)){

      foreach($rawatInap->tRIVisiteDokters as $visite)
      {
       
        $biaya_visite_ird = 0;
        if(!empty($visite)){
          $total_ird = $total_ird + $visite->biaya_visite ;//* $visite->jumlah_visite_ird;
          $biaya_visite_ird = $biaya_visite_ird + $visite->biaya_visite;// * $visite->jumlah_visite_ird;
        }

        

        $dokterVisiteIRD = '';
        if(!empty($visite->dokterIrd))
            $dokterVisiteIRD = ' || <strong>'.$visite->dokterIrd->NICKNAME.'</strong>';

        

    ?>
    <tr>
     <td>
     <?php echo $urutan++;?>. 
    <?php echo $visite->jenisVisite->nama_visite;?>
    </td>
    <td><strong><?php echo $dokterVisiteIRD;?></strong>
    </td>
     <td align="right">
     <?php echo Yii::app()->helper->formatRupiah($biaya_visite_ird);?>
    
     </td>
     
    
   </tr>
   <?php 
      }
    }

   ?>
   

  <tr>
     <td colspan="2" align="left"><strong>II. Tindakan Medik Non-Operasi</strong></td>
     <td  class="border_kiri_dotted"></td>
   </tr>
  
   
   
<?php 


   $listTnop = TindakanMedisNonOperatif::model()->findAll();
   $i = 0;

   foreach($listTnop as $tnop){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_tnop' => $tnop->id_tindakan
      );
      $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);


      $jumlah_tindakan = 0;
      $biaya_ird = 0;

      if(!empty($itemTnop)){
        $jumlah_tindakan = $itemTnop->jumlah_tindakan;
        // $biaya_irna = $itemTnop->biaya_irna;
        $biaya_ird = $itemTnop->biaya_ird;

      }


      if($biaya_ird == 0)
        continue;

      $total_ird = $total_ird + $biaya_ird;
        
    ?>
   <tr>
     <td><?php echo strtolower(chr(64 + ($i+1)));?>. <?php echo $tnop->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_ird);?></td>
     
   </tr>
     <? 
 $i++;
    }

    ?>
   
   
   
   
   <tr>
     <td colspan="2" align="left"><strong>C. LAYANAN PENUNJANG</strong></td>
     <td  class="border_kiri_dotted"></td>
   </tr>
   
   
<?php 



    $urutan = 1;

    $biaya_supp_irna = 0;
      foreach($rawatInap->trRawatInapPenunjangs as $supp)
      {
         
         if($supp->biaya_ird ==0 && $supp->biaya_irna ==0) continue;

          $total_ird = $total_ird + $supp->biaya_ird;
          // $total_irna = $total_irna + $supp->biaya_irna;
          $biaya_supp_irna = $biaya_supp_irna + $supp->biaya_irna;// * $supp->jumlah_tindakan;

         $dokter_usg = '';
        

          if(!empty($supp->dokter_id) || $supp->dokter_id == 0)
          {
             $dokter = DmDokter::model()->findByPk($supp->dokter_id);

             $dokter_usg = !empty($dokter) ? $dokter->NICKNAME : '';
          }
        
    ?>
     <tr>
     <td><?php echo $urutan++;?>. <?php echo $supp->penunjang->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($supp->biaya_ird);?></td>
     
   </tr>
    <?php 
    
    }


    ?>
  
   
   
   <tr>
     <td colspan="2" align="left"><strong>D. OBAT dan ALAT KESEHATAN</strong></td>
     <td  class="border_kiri_dotted"></td>
   </tr>
   
   

<?php 

   $listObat = ObatAlkes::model()->findAll();
   $i = 0;
   foreach($listObat as $obat){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_alkes' => $obat->id_obat_alkes
      );
      $itemObat = TrRawatInapAlkes::model()->findByAttributes($attr);

      $jumlah_tindakan = 0;
      // $biaya_irna = 0;
      $biaya_ird = 0;

      $biaya_obat_ird_total = 0;
      if(!empty($itemObat)){
        $jumlah_tindakan = $itemObat->jumlah_tindakan;
        // $biaya_irna = $itemObat->biaya_irna;
        $biaya_ird = $itemObat->biaya_ird;
        $biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird;
      }

      if($biaya_ird == 0 )
        continue;

      $total_ird = $total_ird + $biaya_obat_ird_total;
        
    ?>
    <tr>
     <td ><span><?php echo ($i+1);?>. </span><?php echo $obat->nama_obat_alkes;?></td>
     <td></td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_ird);?></td>
    
   </tr>
   <?php 
   $i++;
   }

   ?>
   
   
   
   <tr>
    <td colspan="2" align="left"><strong>E. Lain-lain</strong></td>
     <td class="border_kiri_dotted"></td>
     
   </tr>
   
   
   
<?php 

   $listLain = TindakanMedisLain::model()->findAll();
   $i = 0;
   foreach($listLain as $lain){
       $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_lain' => $lain->id_tindakan
      );



      $itemLain = TrRawatInapLain::model()->findByAttributes($attr);
      $jumlah_tindakan = 0;
      $biaya_irna = 0;
      $biaya_ird = 0;
      if(!empty($itemLain)){
        $jumlah_tindakan = $itemLain->jumlah_tindakan;
        $biaya_irna = $itemLain->biaya_irna;
        $biaya_ird = $itemLain->biaya_ird;
      }


      if($biaya_ird == 0 )
        continue;

      $total_ird = $total_ird + $biaya_ird;
    ?>
    <tr>
     <td><?php echo $i+1;?>. <?php echo $lain->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td align="right"><?php echo Yii::app()->helper->formatRupiah($biaya_ird);?></td>
    
   </tr>
   <?php 
   $i++;
      }
   ?>
     
   
      
      <tr>
       <td  align="right" colspan="2"><strong>TOTAL</strong></td>
       <td></td>
       
     </tr>
       <tr>

         <td align="right" colspan="2"><strong>IRD</strong></td>
         <td align="right" ><strong> <?php echo number_format($total_ird,2,',','.');?></strong></td>
         

       </tr>
      
      <tr>
         <td colspan="2"></td>
         <td><br><br>Pare, <?php echo date('d-M-Y');?></td>
       </tr>
        <tr>
         <td colspan="2"></td>
         <td >
           Bagian Perinci&nbsp;:&nbsp;<?php echo Yii::app()->user->getState('nama');?>
         
         </td>
       </tr>
    </table>

 <table width="100%" >
      <tr>
        <td width="10%">No Kwitansi</td>
        <td width="90%"></td>
      </tr>
      <tr>
        <td>Tanggal</td>
        <td>:&nbsp;<?php echo date('d/m/Y');?></td>
      </tr>
      <tr>
        <td>Terbilang</td>
        <td>:&nbsp;<?php echo Yii::app()->helper->terbilang($total_ird);?> rupiah</td>
      </tr>
    </table>
     