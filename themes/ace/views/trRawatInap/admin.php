<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/index')),
  array('name' => 'List'),
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<script type="text/javascript">

    $(document).ready(function(){
        
        $('#search, #size, #jenis_pasien,#kamar_id').change(function(){
            


            $('#tabel-rawat-inap').yiiGridView.update('tabel-rawat-inap', {

                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()+'&jenis_pasien='+$('#jenis_pasien').val()+'&kamar_id='+$('#kamar_id').val()+'&search_by='+$('#search_by').val(),
               
            });
          
         });

        
    });
</script>

<?php 


$updateJS = CHtml::ajax( array(
  'url' => "js:url",
  'data' => "js:form.serialize() + action",
  'type' => 'post',
  'dataType' => 'json',
  'success' => "function( data )
  {

    if( data.status == 'failure' )
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      $( '#update-dialog div.update-dialog-content ' )
        .off() // Stop from re-binding event handlers
        
        .on( 'click','form input[type=submit]', function( e ){ // Send clicked button value
          e.preventDefault();
          updateDialog( false, $( this ).attr( 'name' ) );
      });
    }
    else
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      if( data.status == 'success' ) // Update all grid views on success
      {
        $( '#tabel-rawat-inap' ).each( function(){ // Change the selector if you use different class or element
          $.fn.yiiGridView.update( $( this ).attr( 'id' ) );
        });

    
      }
      setTimeout( \"$( '#update-dialog' ).dialog( 'close' ).children( ':eq(0)' ).empty();\", 1000 );
    }
  }"
)); 

Yii::app()->clientScript->registerScript( 'updateDialog', "
function updateDialog( url, act )
{
  var action = '';
  var form = $( '#update-dialog div.update-dialog-content form' );
  if( url == false )
  {
    action = '&action=' + act;
    url = form.attr( 'action' );
  }
  {$updateJS}
}" );
?>
    <div class="row">
      <div class="col-xs-12">
         <div class="pull-left">
               <!-- block -->                         <?php
  echo CHtml::link('Pasien Masuk',array('TrRawatInap/create'),array('class'=>'btn btn-success'));
?>
  
                        </div>
                      <div class="pull-right">
                       Data per halaman
                            <?php echo CHtml::dropDownList('trRawatInap[PAGESIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,20=>20,30=>30,),array('id'=>'size','class'=>'input-small')); ?>
                      Jenis Pasien 
                            <?php 
$list_gol = CHtml::listData(GolPasien::model()->findAllByAttributes(array('IsKaryawan'=>1)),'KodeGol','NamaGol');
echo CHtml::dropDownList('trRawatInap[TIPE_PASIEN]',isset($_GET['jenis_pasien'])?$_GET['jenis_pasien']:'',$list_gol,array('id'=>'jenis_pasien','empty' => 'Semua','class'=>'input-small')); ?> 
<!-- Cari berdasar -->
Kamar
<?php 
// $akses = array(
//   WebUser::R_SA,
//   WebUser::R_OP_KASIR,
//   WebUser::R_OP_KEUANGAN,
//   WebUser::R_OP_FARMASI,
//   WebUser::R_OP_RADIOLOGI,
//   WebUser::R_OP_RR
// );

$criteria=new CDbCriteria;
$criteria->order = 'nama_kamar ASC';
$criteria->addCondition('is_hapus=0');
// if(!Yii::app()->user->checkAccess($akses))
// {
//   $user_kelas = Yii::app()->user->getState('username');
//   $criteria->compare('user_kamar',$user_kelas);
//   $params = [
//     'id'=>'kamar_id',
//     'class' => 'input-small'
//   ];
// }

// else{
  $params = [
    'id'=>'kamar_id',
    'empty' => 'Semua Kamar',
    'class' => 'input-small'
  ];
// }

$list_kamar = CHtml::listData(DmKamar::model()->findAll($criteria),'id_kamar',function($kelas){
      $full = $kelas->nama_kamar.' - '.$kelas->kelas->nama_kelas;
      return CHtml::encode($full);
    });

echo CHtml::dropDownList('trRawatInap[kamar_id]',isset($_GET['kamar_id'])?$_GET['kamar_id']:'',$list_kamar,$params); ?> 
<!-- Cari berdasar -->

Cari berdasar
                            <?php 
$search_by = array(
  1=>'No RM',2=>'Nama Pasien'
);
echo CHtml::dropDownList('trRawatInap[SEARCH_BY]',isset($_GET['search_by'])?$_GET['search_by']:'',$search_by,array('id'=>'search_by','class'=>'input-small')); ?> 
                      <?php
        echo CHtml::textField('trRawatInap[SEARCH]','',array('placeholder'=>'Cari','id'=>'search')); 
        ?>
          </div> 
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
                            
 
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
    
 
$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabel-rawat-inap',
  'dataProvider'=>$model->search(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            
        ),

     array(
        'header' => 'No RM',
        'value' => '$data->pASIEN->NoMedrec." / ".$data->kode_rawat',
        // 'cssClassExpression' => '$data->is_naik_kelas == 1 ? "notice" : ""',
    ),
    array(
        'header' => 'Nama Pasien',
        'value' => '$data->pASIEN->NAMA." / ".$data->jenisPasien->NamaGol',
       
    ),
    array(
        'header' => 'Kamar',
        'value' => '$data->kamar->nama_kamar." | ".$data->kamar->kelas->nama_kelas',

    ),
    'tanggal_masuk',
    'tanggal_keluar',
    'jam_masuk',
    'jam_keluar',
    'STATUS_INAP_PASIEN',
    array(
      'header' => 'Naik Kelas',
      'type' => 'raw',
      'value' => function($data){
        return $data->is_naik_kelas == 1 ? "<button class='btn btn-success btn-xs btn-block'>YA</button>" : "<button class='btn btn-warning btn-xs btn-block'>TIDAK</button>";
      }
    ),
    array(
      'type'=>'raw',
      'header' => 'Actions',
      'value' => function($data){

        $html = '<div class="btn-group">
  <button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle">
    Tindakan
    <span class="ace-icon fa fa-caret-down icon-on-right"></span>
  </button>

  <ul class="dropdown-menu dropdown-info dropdown-menu-right">
    ';

    if($data->is_locked == 0){
    $html .= '<li>
      <a href="'.Yii::app()->createUrl("trRawatInap/dataPasien/", array("id"=>$data->id_rawat_inap)).'"><i class="ace-icon fa fa-pencil-square-o bigger-120"></i> Input Tindakan</a>
    </li><li class="divider"></li>';
    }
    
    $html .= '<li>
      <a href="'.Yii::app()->createUrl("trRawatInap/inputDiagnosis/", array("id"=>$data->kode_rawat)).'"><i class="ace-icon fa fa-pencil-square-o bigger-120"></i> Input Diagnosis</a>
    </li><li class="divider"></li>';

    if(Yii::app()->user->checkAccess([WebUser::R_OP_KAMAR,WebUser::R_SA,WebUser::R_OP_KASIR,])){
      $html .= '<li>
        <a href="'.Yii::app()->createUrl("labRequest/create/", array("id"=>$data->id_rawat_inap)).'"><i class="ace-icon fa fa-pencil-square-o bigger-120"></i> Request Cek Lab</a>
      </li>';
      $html .= '<li>
        <a href="'.Yii::app()->createUrl("labRequest/index/", array("pid"=>$data->pasien_id)).'"><i class="ace-icon fa fa-list bigger-120"></i> Data Permohonan & Hasil Lab</a>
      </li>';
     
    }


    $html .= '<li>
      <a target="_blank" href="'.Yii::app()->createUrl("trRawatInap/kwitansi/", array("id"=>$data->kode_rawat)).'"><i class="ace-icon fa fa-print bigger-120"></i> Cetak Kwitansi</a>
    </li> 
    <li>
      <a target="_blank" href="'.Yii::app()->createUrl("trRawatInap/cetakKwitansi/", array("id"=>$data->id_rawat_inap)).'"><i class="ace-icon fa fa-print bigger-120"></i> Cetak Rincian</a>
    </li>';
    if($data->is_locked == 0){
    $html .= '<li class="divider"></li>
     <li>
      <a href="'.Yii::app()->createUrl("trRawatInap/keluarIrd/", array("id"=>$data->id_rawat_inap)).'"><i class="ace-icon fa fa-sign-out bigger-120"></i> Keluar IRD</a>
    </li> 
    <li>
      <a href="'.Yii::app()->createUrl("trRawatInap/pindah/", array("id"=>$data->id_rawat_inap,"kode"=>$data->kode_rawat)).'"><i class="ace-icon fa fa-sign-out bigger-120"></i> Pindah Kamar</a>
    </li>
      <li>
      <a href="'.Yii::app()->createUrl("trRawatInap/pulang/", array("id"=>$data->id_rawat_inap)).'"><i class="ace-icon fa fa-sign-out bigger-120"></i> Pasien Pulang</a>
    </li>';
    }
    $html .= '<li class="divider"></li>
     <li>
      <a href="'.Yii::app()->createUrl("trRawatInap/formBpjs/", array("id"=>$data->id_rawat_inap)).'"><i class="ace-icon fa fa-file bigger-120"></i> Form BPJS</a>
    </li>
  </ul>
</div>';

return $html;
      }
    ),
    // 'SUBTOTAL',
    array(
      'class'=>'CButtonColumn',
      'template' => '{unlock} {lock} {update} {delete}',
      'buttons' => array(
        'update' => [
          'visible' => '$data->is_locked == 0',
        ],
        'delete' => [
          'visible' => '$data->is_locked == 0',
        ],
        'unlock' => array(
            'visible' => '$data->is_locked == 1 && Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_KA_KASIR))',
            'url'=>'Yii::app()->createUrl("trRawatInap/openAccess/", array("id"=>$data->id_rawat_inap))', 
             'click' => "function( e ){
                  e.preventDefault();
                  $( '#update-dialog' ).children( ':eq(0)' ).empty(); // Stop auto POST
                  updateDialog( $( this ).attr( 'href' ) );
                  $( '#update-dialog' )
                    .dialog( { title: 'Buka Akses Data Rawat Inap' } )
                    .dialog( 'open' ); }",
            'label'=>'<span class="label label-warning"><i class="fa fa-lock"></i>&nbsp;</span>',
            'options'=>array(
                'title'=>'Buka Akses Data Rawat Inap',
              ),
        ),
        'lock' => array(
            'visible' => '$data->is_locked == 0 && Yii::app()->user->checkAccess(array(WebUser::R_SA,WebUser::R_KA_KASIR))',
            'url'=>'Yii::app()->createUrl("trRawatInap/lockAccess/", array("id"=>$data->id_rawat_inap))', 
             'click' => "function( e ){
                  e.preventDefault();
                  $( '#update-dialog' ).children( ':eq(0)' ).empty(); // Stop auto POST
                  updateDialog( $( this ).attr( 'href' ) );
                  $( '#update-dialog' )
                    .dialog( { title: 'Tutup Akses Data Inap' } )
                    .dialog( 'open' ); }",
            'label'=>'<span class="label label-info"><i class="fa fa-unlock"></i></span>',
            'options'=>array(
                'title'=>'Tutup Akses Data Rawat Inap',
              ),
          ),
      ),
    ),
  ),

  'htmlOptions'=>array(
    'class'=>'table '
  ),
  'pager'=>array(
                        'class'=>'SimplePager',                  
                        'header'=>'',                      
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',        
                        'lastPageLabel'=>'Terakhir',  
                        'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
            'hiddenPageCssClass'=>'disabled',
            'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table  table-bordered table-hover',
  'summaryCssClass'=>'table-message-info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
  'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>
      </div>
    </div>


<?php 

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'update-dialog',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 300,
    'resizable' => false,
  ),
)); ?>
<div class="update-dialog-content"></div>
<?php $this->endWidget(); ?>
