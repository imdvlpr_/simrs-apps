<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
 array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/index')),
                           
                            array('name' => 'Input Tindakan')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>

<div class="row">
    <div class="col-xs-12">

<?php 
    
 $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts.ri_navigation'); 
  $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts.nav_dataPasien'); 

?>    
<?php $this->renderPartial('_penunjang', 
    array(
        // 'penunjang' => $penunjang,
        'model'=>$model,
        'rawatInap' => $rawatInap,
        'pasien' => $pasien,
        'listDokter' => $listDokter,
        'jenisVisite' => $jenisVisite,
        'rawatRincian' => $rawatRincian
        )
    ); ?>
                    </div>
                </div>
      