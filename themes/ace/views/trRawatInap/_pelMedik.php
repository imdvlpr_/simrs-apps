  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>
<script type="text/javascript">


$(document).ready(function(){


});
</script>
<?php

  $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
          $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), $tgl_keluar);
        }else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }

          $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }

    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>

 <table class="table table-condensed" id="table_ri_rj">
   <thead>
   <tr>
     <th>A</th>
     <th>LAYANAN</th>
     <th colspan="2"  align="center">RAWAT DARURAT</th>
     <!-- <th>&nbsp;</th> -->
     <th colspan="2" align="center">RAWAT INAP</th>
     <!-- <th>&nbsp;</th> -->
     <th>TARIF</th>
   </tr>
   </thead>
   <tbody>
    <tr>
     <td>&nbsp;</td>
     <td>1. Observasi</td>
     <td>&nbsp;</td>
     <td><?php 
     $total_ird = $total_ird + $rawatRincian->obs_ird;
     echo $form->textField($rawatRincian,'obs_ird',array('class'=>'input-medium uang'));
     ?></td>
     <td>
       

     </td>
     <td>1. Biaya Kamar</td>
     <td><?php 
      $isneonatus = $rawatInap->kamar->nama_kamar == 'NEONATUS';
   
      if($isneonatus)
      {


      $biaya_neonatus = $rawatInap->biaya_kamar;

        
        echo CHtml::textField('biaya_per_malam',Yii::app()->helper->formatRupiah($biaya_neonatus),array('class'=>'input-medium uang','id'=>'biaya_kamar_per_malam')); 
        $total_irna = $total_irna + $biaya_neonatus;
        echo '&nbsp;';
         echo $form->checkBox($rawatInap,'is_tarif_kamar_penuh');
         echo CHtml::hiddenField('kamar_biaya_kamar',$rawatInap->kamar->biaya_kamar,['id'=>'kamar_biaya_kamar']);
         echo CHtml::hiddenField('kamar_biaya_makan',$rawatInap->kamar->biaya_makan,['id'=>'kamar_biaya_makan']); 

         echo '&nbsp;';
      echo $form->labelEx($rawatInap,'is_tarif_kamar_penuh');
      }

      else
      {

        if($rawatInap->kamar->kelas->kode_kelas != 'IRD')
        {
        echo CHtml::textField('biaya_per_malam',Yii::app()->helper->formatRupiah($rawatInap->kamar->biaya_kamar * $selisih_hari),array('class'=>'input-medium uang','id'=>'biaya_kamar_per_malam')); 

        
          $total_irna = $total_irna + $rawatInap->kamar->biaya_kamar * $selisih_hari;   
        }

        else{
          echo CHtml::textField('biaya_per_malam',0,array('class'=>'input-medium uang','id'=>'biaya_kamar_per_malam')); 
        }
      }

     
     $total_irna = $total_irna + $rawatRincian->askep_kamar;

     if(!empty($rawatRincian))
        $total_irna = $total_irna + $rawatRincian->asuhan_nutrisi;
     ?>

     <?php 

     
     if($isneonatus)
     {
     ?>
   <!--   <a id="input-biaya-kamar" href="javascript:void(0)" title="Input Biaya Kamar">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
    </a> -->

    <?php 
    }
    ?>
     </td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>2. Pengawasan Dokter</td>
     <td>
      <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter_ird" name="nama_dokter_ird" autocomplete="off" value="<?php echo $nama_dokter_ird;?>"/>

   
     <?php echo $form->hiddenField($rawatRincian,'dokter_ird');?>
     </td>
     <td><?php 

     echo $form->textField($rawatRincian,'biaya_pengawasan',array('class'=>'input-medium uang'));

     if(!empty($rawatRincian))
        $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;
     ?>
     </td>
     <td>&nbsp;</td>
     <td>2. Askep <?php echo $rawatInap->kamar->nama_kamar;?>
       
     </td>
     <td><?php
    echo CHtml::hiddenField('nilai_askep',$rawatInap->kamar->biaya_askep); 
    
    // if($rawatRincian->askep_kamar)
    // {
      $rawatRincian->askep_kamar = Yii::app()->helper->formatRupiah($rawatInap->kamar->biaya_askep * $selisih_hari); 
    // } 


    // echo 'Selisih: '.$rawatInap->kamar->biaya_askep; 
    //  echo number_format($rawatInap->kamar->biaya_askep * $selisih_hari,0,',','.');
    echo $form->textField($rawatRincian,'askep_kamar',array('class'=>'input-medium uang')); 
    echo ' ';
    $rawatRincian->jumlah_askep = $selisih_hari;
  // echo $form->textField($rawatRincian,'jumlah_askep',array('class'=>'input-small jumlah')); 

    // echo $rawatInap->is_tarif_kamar_penuh;
    

    ?>

    <!-- <a href="javascript:void(0)" id="input_askep" class="btn btn-warning" title="Input Askep Harian">Input</a> -->
     </td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>3. Asuhan Nutrisi</td>
     <td>
     <?php 

    echo CHtml::hiddenField('nilai_asnut',$rawatInap->kamar->biaya_asnut); 
     
    if(empty($rawatRincian->asuhan_nutrisi) && $rawatRincian->asuhan_nutrisi != 0)
    {
      $rawatRincian->asuhan_nutrisi = $rawatInap->kamar->biaya_asnut; 
    }  

     echo $form->textField($rawatRincian,'asuhan_nutrisi',array('class'=>'input-medium uang'));
     echo ' ';
     echo $form->textField($rawatRincian,'jumlah_asnut',array('class'=>'input-small jumlah')); 
    
     ?>
    <!-- <a href="javascript:void(0)" id="input_asnut" class="btn btn-warning" title="Input Asuhan Nutrisi Harian">Input</a> -->
    
     </td>
   </tr>
   </tbody>
   <thead>
   <tr>
     <th>B</th>
     <th colspan="6">PELAYANAN MEDIK</th>
     
   </tr>
   </thead>
   <tbody id="tbody-dokter">
     <?php 

   if(!is_null($rawatInap->getError('ERROR_VISITE'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_VISITE'); 
?>
</div>
</td>
</tr>
<?php 

}

    $urutan = 1;
      if(!empty($rawatInap->tRIVisiteDokters)){

    //  $criteria = new CDbCriteria; 
    // $criteria->addCondition("id_rawat_inap=".$rawatInap->id_rawat_inap);
    // $criteria->order = 'jenisVisite.urutan ASC, dokterIrna.jenis_dokter ASC';
    // $criteria->with = array('dokterIrna','jenisVisite');
    // $criteria->together = true;

    // $trVisiteDokter = TrRawatInapVisiteDokter::model()->findAll($criteria);

      foreach($rawatInap->tRIVisiteDokters as $visite)
      {
       
        if(!empty($visite))
          $total_ird = $total_ird + $visite->biaya_visite * $visite->jumlah_visite_ird;

        if(!empty($visite))
          $total_irna = $total_irna + $visite->biaya_visite_irna * $visite->jumlah_visite;



        $dokterVisiteIRD = '';
        $dokterVisiteIRNA = '';
        $jenisDokter = '';
        if(!empty($visite->dokterIrd))
            $dokterVisiteIRD = $visite->dokterIrd->FULLNAME;

        if(!empty($visite->dokterIrna))
        {
            $dokterVisiteIRNA = $visite->dokterIrna->FULLNAME;
            $jenisDokter = $visite->dokterIrna->jenis_dokter;
        }



        $jenis_visite = !empty($visite->jenisVisite) ? $visite->jenisVisite->nama_visite : ''; 
    ?>
    <tr class="row-dokter">
     <td>&nbsp;</td>
     <td>
     <span class="number"><?php echo $urutan++;?>. </span>
     <input placeholder="Ketik Tindakan Dokter" type="text" class="input-medium nama_jenis_visite" name="nama_jenis_visite" autocomplete="off" value="<?php echo $jenis_visite;?>"/>
        <input type="hidden" class="id_jenis_visite" name="id_jenis_visite[]" value="<?php echo $visite->id_jenis_visite;?>"/>

        <input type="hidden" class="kode_jenis_visite" name="kode_jenis_visite[]" value="<?php echo $visite->jenisVisite->kode_visite;?>"/>

   
   
     </td>
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" value="<?php echo $dokterVisiteIRD;?>"/>
     <input type="hidden" class="id_dokter" name="id_dokter_visite_ird[]" value="<?php echo $visite->id_dokter;?>"/>
   
    </td>
     <td><input type="text" name="biaya_visite_ird[]" style="width:100px" class="input-medium uang" placeholder="-" value="<?php echo Yii::app()->helper->formatRupiah($visite->biaya_visite);?>"/>
     <input type="text" name="jumlah_visite_ird[]" class="input-small jumlah jumlah_tindakan_ird" placeholder="Jumlah Visite" value="<?php echo $visite->jumlah_visite_ird;?>"/>
     </td>
     
    <td>&nbsp;</td>
     
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" value="<?php echo $dokterVisiteIRNA;?>"/>
     <input type="hidden" class="id_dokter" name="id_dokter_visite_irna[]" value="<?php echo $visite->id_dokter_irna;?>"/>
    
    </td>
     <td class="biaya_visite_irna">
     <input type="text" name="biaya_visite_irna[]" class="input-medium uang biaya_visite_irna" placeholder="-" value="<?php echo Yii::app()->helper->formatRupiah($visite->biaya_visite_irna);?>"/>
     

     <input type="text" name="jumlah_visite[]" class="input-small jumlah jumlah_tindakan_irna" placeholder="Jumlah Visite" value="<?php echo $visite->jumlah_visite;?>"/>
     <input type="hidden" name="biaya_tunggal_irna[]" class="input-medium biaya_tunggal_irna" value="<?php echo Yii::app()->helper->formatRupiah($visite->jenisVisite->biaya);?>"/> 
     <input type="hidden" class="jenis_dokter" value="<?php echo $jenisDokter;?>"/>
     <input type="hidden" class="biaya_asli_dokter" value="<?php echo Yii::app()->helper->formatRupiah($visite->biaya_visite_irna * $visite->jumlah_visite);?>"/>
     <a href="javascript:void(0)" class="hapus-dokter btn btn-sm btn-warning" >Hapus</a>
     </td>

   </tr>
   <?php 
      }
    }

   ?>
    <tr id="row-dokter" class="row-dokter">
     <td>&nbsp;</td>
     <td>
     <span class="number"><?php echo $urutan++;?>. </span><input placeholder="Ketik Tindakan Dokter" type="text" class="input-medium nama_jenis_visite" name="nama_jenis_visite" autocomplete="off" />
        <input type="hidden" class="id_jenis_visite" name="id_jenis_visite[]" />
         <input type="hidden" class="kode_jenis_visite" name="kode_jenis_visite[]"/>
   
     </td>
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" />
     <input type="hidden" class="id_dokter" name="id_dokter_visite_ird[]" />
   
    </td>
     <td>
     <input type="text" name="biaya_visite_ird[]" style="width:100px" class="input-medium uang" placeholder="-" value="0"/>
     <input type="text" name="jumlah_visite_ird[]" class="input-small jumlah jumlah_tindakan_ird" placeholder="Jumlah Visite" value="1"/>
     </td>
     
    <td>&nbsp;</td>
     
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" />
     <input type="hidden" class="id_dokter" name="id_dokter_visite_irna[]" />
   
    </td>
     <td class="biaya_visite_irna"><input type="text" name="biaya_visite_irna[]" class="input-medium uang biaya_visite_irna" placeholder="-" value="0"/> 

     <input type="text" name="jumlah_visite[]" class="input-small jumlah jumlah_tindakan_irna" placeholder="Jumlah Visite" value="1"/>
     <input type="hidden" name="biaya_tunggal_irna[]" class="input-medium biaya_tunggal_irna"/>
      <input type="hidden" class="jenis_dokter"/>
       <input type="hidden" class="biaya_asli_dokter"/>
     <a href="javascript:void(0)" class="hapus-dokter  btn-sm btn btn-warning" >Hapus</a>
     </td>

   </tr>
  <tr class="row-tambah-dokter">
  <td>&nbsp;</td>
    <td colspan="6"><a id="tambah-dokter" href="javascript:void(0)" class=" btn btn-success"> Tambah Item </a></td>
  </tr>
 
  </tbody>
   </table>
  

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
     
<?php echo $this->renderPartial('_buttonCetak',array('rawatInap'=>$rawatInap));?>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>


<?php echo $this->renderPartial('scriptPelMedik',
  array(
    'rawatInap'=>$rawatInap
  )
);?>