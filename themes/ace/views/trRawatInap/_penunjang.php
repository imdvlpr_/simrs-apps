  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>
<?php
$selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
        }else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }

          $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>



 <table class="table table-condensed" id="table_ri_rj" >
   <thead>
   <tr>
     <th>C</th>
     <th>LAYANAN PENUNJANG</th>
     <th colspan="2"  style="text-align: right">RAWAT DARURAT</th>
     <th style="text-align: center">DOKTER</th>
     <th style="text-align: right">RAWAT INAP</th>
     <!-- <th>&nbsp;</th> -->
     <!-- <th width="20%">TARIF</th> -->
   </tr>
   </thead>

 
   <tbody id="tbody-supp">
    <?php 
   if(!is_null($rawatInap->getError('ERROR_SUPP'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_SUPP'); 
?>
</div>
</td>
</tr>
<?php 

}


     $listPenunjang = TindakanMedisPenunjang::model()->findAll();
    $urutan = 1;
    // $biaya_supp_irna = 0;
      foreach($listPenunjang as $supp)
      {

        $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_penunjang' => $supp->id_tindakan_penunjang
      );
      $itemSupp = TrRawatInapPenunjang::model()->findByAttributes($attr);
      
      $supp_biaya_ird = 0;
      $supp_biaya_irna = 0 ;
      $supp_jumlah_tindakan = 0;

     
      if(!empty($itemSupp)){
        // $total_ird = $total_ird + $itemSupp->biaya_ird;
       // $total_irna = $total_irna + $supp->biaya_irna;
        // $biaya_supp_irna = $biaya_supp_irna + $itemSupp->biaya_irna * $itemSupp->jumlah_tindakan;

        // $total_irna = $total_irna + $biaya_supp_irna;

        $supp_biaya_ird = $itemSupp->biaya_ird;
        $supp_biaya_irna = $itemSupp->biaya_irna;
        $supp_jumlah_tindakan = $itemSupp->jumlah_tindakan;
      }  



    ?>
     <tr class="row-supp">
     <td>&nbsp;</td>
     <td>
       <span class="number-supp"><?php echo $urutan++;?>. </span>
       <?=$supp->nama_tindakan;?>
     </td>
   
     <td align="right">Rp&nbsp;<input value="<?php echo Yii::app()->helper->formatRupiah($supp_biaya_ird);?>" type="text" name="biaya_ird_supp[]" class="input-medium uang" placeholder="-" /></td>
     <td>&nbsp;</td>
      <td  align="center">
      <?php 

      $nama_dokter = '';
      $dokter_id = '';
      if($supp->nama_tindakan == 'USG' || $supp->nama_tindakan == 'EKG' || $supp->nama_tindakan == 'PA')
      {

        if(!empty($itemSupp->dokter_id))
        {

            
           $dokter = DmDokter::model()->findByPk($itemSupp->dokter_id);

           $dokter_id = $itemSupp->dokter_id;
           $nama_dokter = !empty($dokter) ? $dokter->FULLNAME : '';
        }
      ?>
       <input id="nama_dokter_perawat" placeholder="Ketik Nama Dokter" type="text" class="input-large nama_dokter_merawat" name="nama_dokter_merawat[]" autocomplete="off" value="<?php echo $nama_dokter;?>"/>
       <input type="hidden" name="dokter_pj_tindakan[]" class="dokter_pj_tindakan" value="<?php echo $dokter_id;?>"/>
       <?php 
     }
       ?>
     </td>
     <td  align="right">Rp&nbsp;<input value="<?php echo Yii::app()->helper->formatRupiah($supp_biaya_irna);?>" type="text" name="biaya_irna_supp[]" class="input-medium uang" placeholder="-"/>
    <!--  <input type="text" value="<?php echo $supp_jumlah_tindakan;?>" name="jumlah_tindakan_supp[]" class="input-small jumlah jumlah_tindakan_supp" placeholder="Jml Lyn"/>
     <a href="javascript:void(0)" class="hapus-supp btn btn-warning" >Hapus</a> -->
     </td>
   </tr>
    <?php 
    
    }

    
    ?>

  
   </tbody>
 
   
    </table>

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
     <?php echo $this->renderPartial('_buttonCetak',array('rawatInap'=>$rawatInap));?>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>


<script>




$(document).bind("keyup.autocomplete",function(){
   $('.nama_dokter_merawat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
        // $('#dokter_usg').val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        // $('#dokter_usg').val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});





</script>
