  <?php 
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }
</style>

 <table class="table">
   <tr>
     <td>Nama Pasien</td>
     <td>:</td>
     <td><strong><?php echo $pasien->NAMA;?></strong></td>
     <td></td>
     <td>No Reg</td>
     <td>:</td>
     <td><?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk</td>
     <td>:</td>
     <td><?php echo date('d-M-y',strtotime($rawatInap->tanggal_masuk));?></td>
     <td></td>
     <td>Tgl Keluar</td>
     <td>:</td>
     <td><?php echo !empty($rawatInap->tanggal_keluar) ? date('d-M-y',strtotime($rawatInap->tanggal_keluar)) : '-' ;?></td>
   </tr>
   <tr>
     <td>Dirawat/R. Kelas</td>
     <td>:</td>
     <td><?php echo $rawatInap->Kamar->nama_kamar;?></td>
     <td></td>
     <td>Lm. Dirawat</td>
     <td>:</td>
     <td>
        <?php 

        $selisih_hari = 0;

        if(!empty($rawatInap->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHari($rawatInap->tanggal_masuk, $rawatInap->tanggal_keluar);
        }

        else{
          $selisih_hari = Yii::app()->helper->getSelisihHari($rawatInap->tanggal_masuk, date('Y-m-d'));
        }

        echo $selisih_hari.' hari';

        ?>

     </td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td><?php echo $pasien->FULLADDRESS;?></td>
     <td></td>
     <td>Dokter yang merawat</td>
     <td>:</td>
     <td>
    <strong><?php echo $rawatInap->dokter->FULLNAME;?></strong>

     </td>

     </td>
   </tr>
 </table>


 <table class="table table-condensed">
   <thead>
   <tr>
     <th>A</th>
     <th>LAYANAN</th>
     <th colspan="2"  align="center">RAWAT DARURAT</th>
     <!-- <th>&nbsp;</th> -->
     <th colspan="2" align="center">RAWAT INAP</th>
     <!-- <th>&nbsp;</th> -->
     <th>TARIF</th>
   </tr>
   </thead>
   <tbody>
    <tr>
     <td>&nbsp;</td>
     <td>1. Observasi</td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($rawatRincian->obs_ird,0,',','.');?></td>
     <td>
       

     </td>
     <td>1. Biaya Kamar</td>
     <td>Rp&nbsp;<?php echo number_format($rawatInap->Kamar->biaya_kamar * $selisih_hari,0,',','.');?></td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>2. Pengawasan Dokter</td>
     <td>
     <?php echo $rawatRincian->dokterIrd->FULLNAME;?>


     </td>
     <td>Rp <?php echo number_format($rawatRincian->biaya_pengawasan,0,',','.');?>
     </td>
     <td>&nbsp;</td>
     <td>2. Askep <?php echo $rawatInap->Kamar->nama_kamar;?></td>
     <td>Rp&nbsp;<?php echo number_format($rawatInap->Kamar->biaya_askep * $selisih_hari,0,',','.');?>
       

     </td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>3. Jasa Perawat</td>
     <td>IRD</td>
     <td>Rp&nbsp;<?php echo number_format($rawatRincian->jasa_perawat,0,',','.');?></td>
     <td>&nbsp;</td>
     <td>3. Asuhan Nutrisi</td>
     <td>Rp&nbsp;<?php echo number_format($rawatRincian->asuhan_nutrisi,0,',','.');?>
       
        <?php echo ' x '.$rawatRincian->jumlah_asuhan;?>
     </td>
   </tr>
   </tbody>
   <thead>
   <tr>
     <th>B</th>
     <th colspan="6">PELAYANAN MEDIK</th>
     
   </tr>
   </thead>
   <tbody id="tbody-dokter">
    
   <?php

    $urutan = 1;
      foreach($rawatInap->tRIVisiteDokters as $visite)
      {
       

    ?>
    <tr class="row-dokter">
     <td>

</td>
     <td>
     <span class="number"><?php echo $urutan++;?>. </span><?php echo $visite->jenisVisite->nama_visite;?>
   
     </td>
     <td>
    </td>
     <td>
     <?php echo $visite->dokter->FULLNAME;?>
 

     </td>
     
    <td>&nbsp;</td>
     
     <td>
     &nbsp;
    </td>
     <td>Rp&nbsp;<?php echo number_format($visite->biaya_visite,0,',','.');?> 
     <?php echo ' x '.$visite->jumlah_visite;?>
     </td>

   </tr>
   <?php 
      }
    
   ?>
  <tr>
     <td>&nbsp;</td>
     <td colspan="6"><strong>I. Tindakan Medik Operasi</strong></td>
     
   </tr>
  </tbody>
   <tbody id="tbody-top">
     <?php

    $urutan = 1;
      foreach($rawatInap->trRawatInapTops as $top)
      {
       

    ?>
     <tr class="row-top">
     <td>&nbsp;</td>
     <td>
     
     <span class="number-top"><?php echo $urutan++;?>. </span>
     <?php echo $top->top->nama_tindakan;?>
   
     </td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($top->biaya_ird,0,',','.');?></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($top->biaya_irna,0,',','.');?>
     <?php echo ' x '.$top->jumlah_tindakan;?>
     </td>
   </tr>

    <?php }?>
   
   
  <tr>
     <td>&nbsp;</td>
     <td colspan="6"><strong>II. Tindakan Medik Non-Operasi</strong></td>
     
   </tr>
  </tbody>
   <tbody id="tbody-tnop">
   
    <?php

    $urutan = 1;
      foreach($rawatInap->trRawatInapTnops as $tnop)
      {
       

    ?>
     <tr class="row-tnop">
     <td>&nbsp;</td>
     <td>
         <span class="number-tnop"><?php echo $urutan++;?>. </span>
         <?php echo $tnop->tnop->nama_tindakan;?>

     </td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($tnop->biaya_ird,0,',','.');?></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($tnop->biaya_irna,0,',','.');?>
      <?php echo ' x '.$tnop->jumlah_tindakan;?>
     
     </td>
   </tr>
    <?php }?>
  
   </tbody>
   <thead>
   <tr>
     <th>C</th>
     <th colspan="6"><strong>LAYANAN PENUNJANG</strong></th>
     
   </tr>
   </thead>
   <tbody id="tbody-supp">
     <?php

    $urutan = 1;
      foreach($rawatInap->trRawatInapPenunjangs as $supp)
      {
       

    ?>
     <tr class="row-supp">
     <td>&nbsp;</td>
     <td>
       <span class="number-supp"><?php echo $urutan++;?>. </span>
       <?php echo $supp->penunjang->nama_tindakan;?>

     </td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($supp->biaya_ird,0,',','.');?></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($supp->biaya_irna,0,',','.');?>
     <?php echo ' x '.$supp->jumlah_tindakan;?>

     </td>
   </tr>
    <?php 
    }?>
  
   </tbody>
   <thead>
   <tr>
     <th>D</th>
     <th colspan="6"><strong>OBAT dan ALAT KESEHATAN</strong></th>
     
   </tr>
   </thead>
   <tbody id="tbody-obat">
  
    <?php

    $urutan = 1;
      foreach($rawatInap->trRawatInapAlkes as $alkes)
      {
       

    ?>
     <tr class="row-obat">
     <td>&nbsp;</td>
     <td>
       <span class="number-obat"><?php echo $urutan++;?>. </span>
       <?php echo $alkes->alkes->nama_obat_alkes;?>

     </td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($alkes->biaya_ird,0,',','.');?></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($alkes->biaya_irna,0,',','.');?>
     <?php echo ' x '.$alkes->jumlah_tindakan;?>
     </td>
   </tr>
    <?php 
  }
    ?>
   
   </tbody>
   <thead>
   <tr>
     <th colspan="7"><strong>E. Lain-lain</strong></th>
     
     
   </tr>
   </thead>
   <tbody id="tbody-rm">
    
    <?php

    $urutan = 1;
      foreach($rawatInap->trRawatInapLains as $lain)
      {
       

    ?>

     <tr class="row-rm">
     <td>&nbsp;</td>
     <td> <span class="number-rm"><?php echo $urutan++;?>. </span>
     <?php echo $lain->lain->nama_tindakan;?>
    
        </td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($lain->biaya_ird,0,',','.');?></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php echo number_format($lain->biaya_irna,0,',','.');?>
       <?php echo ' x '.$lain->jumlah_tindakan;?> 
     </td>
   </tr>
    <?php }?>
    
   </tbody>
 </table>      
    

    <div class="form-actions" align="center">

  <a class="btn btn-success" href="<?php echo Yii::app()->createUrl('trRawatInap/input',array('id'=>$rawatInap->id_rawat_inap));?>">Ubah</a>
  <a class="btn btn-success" href="<?php echo Yii::app()->createUrl('trRawatInap/laporan',array('id'=>$rawatInap->id_rawat_inap));?>">Laporan Pasien</a>
    </div>
  </fieldset>

<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');
  $cs->registerCssFile($baseUrl.'/vendors/jquery-ui.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
  
?>



</script>