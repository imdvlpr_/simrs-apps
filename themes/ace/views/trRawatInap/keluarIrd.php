<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

      .ui-datepicker { 
  margin-top:  100px;
  z-index: 1000;
}
</style>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#search').change(function(){
    
            $('#tabel-rawat-jalan').yiiGridView.update('tabel-rawat-jalan', {
                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()   
            });
          
         });

         $('#size').change(function(){
        
            $('#tabel-rawat-jalan').yiiGridView.update('tabel-rawat-jalan', {
                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()    
            });
        
         });     
    });
</script>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3" id="sidebar">

        <?php echo $this->renderPartial('/layouts/nav_left');?>
        </div>
        
        <!--/span-->
        <div class="span9" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/index')),
                            array('name' => 'List')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Data Rawat Inap</div>
                      <div class="pull-right"> </div> 
                    </div>
                    <div class="block-content collapse in">
                        
  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    <legend>
      

    </legend>
    

	<div class="control-group">
    <?php echo $form->labelEx($model,'pasien_id',array('class'=>'control-label'));?>
      <div class="controls">
        <input type="text" readonly value="<?php echo $model->pASIEN->NAMA;?>"/>
        <?php
          echo $form->hiddenField($model,'pasien_id');

        
         ?>
      </div>
    </div>
 <div class="control-group">
      <?php echo CHtml::label('Jenis Pasien','',array('class'=>'control-label'));?>
        <div class="controls">
        <input type="text" readonly value="<?php echo $model->jenisPasien->NamaGol;?>"/>
       
        </div>
      </div>

  <div class="control-group">
    <?php echo $form->labelEx($model,'tanggal_masuk_ird',array('class'=>'control-label'));?>
      <div class="controls">
      <input type="text" readonly value="<?php echo $model->tanggal_masuk_ird.' '.$model->jam_masuk_ird;?>"/>
      

      </div>
    </div>

  <div class="control-group">
    <?php echo $form->labelEx($model,'tanggal_keluar_ird',array('class'=>'control-label'));?>
      <div class="controls">

      <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'datetime_keluar_ird',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        
                    ),
                    
        ));
   
        ?>
       
        <?php echo $form->error($model,'tanggal_keluar_ird'); ?>
      </div>
    </div>
    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('Simpan',array('class'=>'btn btn-success')); ?>
    </div>
  </fieldset>
  <?php $this->endWidget();?>

<script>

function isiDataPasien(ui)
{
	 $("#TrRawatInap_pasien_id").val(ui.item.id);
	 $("#TrRawatInap_PASIEN_NAMA").val(ui.item.value); 
	 

}

</script>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>