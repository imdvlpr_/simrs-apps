<?php
/* @var $this TrRawatInapController */
/* @var $model TrRawatInap */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_rawat_inap'); ?>
		<?php echo $form->textField($model,'id_rawat_inap'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tanggal_masuk'); ?>
		<?php echo $form->textField($model,'tanggal_masuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jam_masuk'); ?>
		<?php echo $form->textField($model,'jam_masuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tanggal_keluar'); ?>
		<?php echo $form->textField($model,'tanggal_keluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jam_keluar'); ?>
		<?php echo $form->textField($model,'jam_keluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datetime_masuk'); ?>
		<?php echo $form->textField($model,'datetime_masuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datetime_keluar'); ?>
		<?php echo $form->textField($model,'datetime_keluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pasien_id'); ?>
		<?php echo $form->textField($model,'pasien_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ri_item_id'); ?>
		<?php echo $form->textField($model,'ri_item_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->