<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/index')),
                            array('name' => 'List')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#search').change(function(){
    
            $('#tabel-rawat-jalan').yiiGridView.update('tabel-rawat-jalan', {
                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()   
            });
          
         });

         $('#size').change(function(){
        
            $('#tabel-rawat-jalan').yiiGridView.update('tabel-rawat-jalan', {
                url:'?r=trRawatInap&filter='+$('#search').val()+'&size='+$('#size').val()    
            });
        
         });     
    });
</script>
<div class="row">
              <div class="col-xs-12">


<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div></div>