
<?php 

$biaya_kamar = array();
$biaya_ird = array();
$total_all =0;
$pasien = '';
$total_ird = 0;
$list_ri = array();
foreach($rawatInaps as $ri)
{
  $total_ird += $ri->biaya_total_ird;
  $list_ri[] = $ri;
   $pasien = $ri->pASIEN;
  $kode_rawat = $ri->kode_rawat;
  $biaya_kamar[] = $ri->biaya_total_kamar;
  $biaya_ird[] = $ri->biaya_total_ird;
  $total_all = $total_all + $ri->biaya_total_kamar + $ri->biaya_total_ird;
}



$terbilang = round($total_all);
   ?>  
<table width="100%" style="padding: 2px 3px">
 
<tr><td style="border-bottom: 1px solid ">
<div style="text-align: center">
PEMERINTAH KABUPATEN KEDIRI<br>
RUMAH SAKIT UMUM DAERAH (RSUD)
<br>
<small>JL. PAHLAWAN KUSUMA BANGSA NO. 1 TELP (0354) 391718 / 391169 FAX. 391833</small> 
<br>PARE KEDIRI (64213)
	
      
</div>
</td></tr>
 </table>
 <style type="text/css">
 	ul{
 		 list-style: none;
 	}
 </style>
 <style type="text/css">



  div.border_atas, td.border_atas{
    border-top-color:#000000;
    border-top-width:1px;
    border-top-style:solid;
  }

  div.border_bawah, td.border_bawah{
    border-bottom-color:#000000;
    border-bottom-width:1px;
    border-bottom-style:solid;
  }

  div.border_kiri,td.border_kiri{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:solid;
  }
  div.border_kanan, td.border_kanan{
    border-right-color:#000000;
    border-right-width:1px;
    border-right-style:solid;
  }

  td.border_kiri_dotted{
    border-left-color:#000000;
    border-left-width:1px;
    border-left-style:dotted;
  }
</style>
<table width="100%" style="padding: 2px 5px" >
 
<tr>
<td width="65%" align="right">TANDA BUKTI PEMBAYARAN
</td>
<td align="right" width="30%">
  Nomor bukti : <strong><?php 

  echo Yii::app()->helper->appendZeros($kode_rawat,6);

  ?></strong>
</td>

</tr>
 </table>

<table style="padding-left: 20px" width="100%">
   <tr>
    <td width="25%">Telah Terima dari </td><td width="85%">: <?php echo $pasien->NAMA;?></td>
  </tr>
  <tr>
    <td>Alamat</td><td>: <?php echo $pasien->FULLADDRESS;?></td>
  </tr>
  <tr>
    <td>Terbilang</td><td>: <strong><?php 

    echo ucwords(Yii::app()->helper->terbilang($terbilang));

    ?></strong>
    </td>
  </tr>
 <tr>
    <td>Untuk Pembayaran</td>
    <td>: 
    Sebagai pembayaran atas perawatan pasien selama 
  <?php 
  $selisih_hari = 1;

  $tgl_hingga = '';



  $rawal = $list_ri[0];
  $rakhir = $list_ri[count($list_ri)-1];

  if(!empty($rakhir->tanggal_keluar))
  {
     $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);

    $tgl_hingga = $ri->tanggal_keluar;
  }
  else
  {
    $dnow = date('Y-m-d');
      $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawal->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));

    $tgl_hingga = $dnow;
  }


       

        echo $selisih_hari.' hari';
  ?>
  dari tanggal <?php echo $rawal->tanggal_masuk;?> <br>hingga <?php echo date('d/m/Y',strtotime($tgl_hingga));?> 
    </td>
  </tr>
<tr>
    <td valign="top">Dengan rincian</td>
    <td>
      <table style="padding-left: 20px">
  
        <?php 
        

        if($total_ird != 0){
        ?>
        <tr>
          <td width="35%" style="text-align: right">IRD : </td>
          <td width="30%" style="text-align: right"><strong>Rp. 
          <?php 
          
          echo Yii::app()->helper->formatRupiah($total_ird);

          ?></strong></td>
        </tr>
        <?php 
      }
        ?>
        <?php 
        $i=0;
      foreach($rawatInaps as $ri){
        ?>
        <tr>
          <td width="35%" style="text-align: right"><strong><?php echo $ri->kamar->nama_kamar.' / '.$ri->kamar->kelas->nama_kelas;?> : </strong></td>
          <td width="30%" style="text-align: right"><strong>Rp. <?php echo Yii::app()->helper->formatRupiah($ri->biaya_total_kamar);?></strong></td>
        </tr>
        <?php 
        $i++;
      }
        ?>
        <tr>
          <td width="35%" style="text-align: right;"><br><br><strong>JUMLAH : </strong><br></td>
          <td width="30%" style="text-align: right;"><br><br>
<table cellpadding="3">
<tr>
<td class="border_atas border_kanan border_kiri border_bawah">
          <strong>Rp. <?php echo Yii::app()->helper->formatRupiah($total_all);?></strong>
</td>
</tr>
</table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>Tanggal terima uang </td>
    <td>
      <?php echo date('d-M-Y');?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
<table cellpadding="3">
  <tr>
    <td width="30%" style="padding: 5px;" class="border_atas border_kanan border_kiri border_bawah">KODE REKENING</td>
    <td width="30%" style="text-align: center" class="border_atas border_kanan border_kiri border_bawah">1.02.02.01.4.1.00.00.4.16.01</td>
  </tr>
</table>
    </td>
  </tr>
</table>
 <table>
	<tr>
		<td width="30%">Bendahara Penerimaan<br><br><br><br><br><br><u>HADI SUWIGNYO, SE</u><br>NIP. 196707301989111001
    </td>
		<td width="35%" style="padding: 5px;text-align: center;">
		Kasir Penerima<br><br><br><br><br><br><?php echo Yii::app()->user->getState('nama');?>
    </td>
		<td width="35%" style="text-align: center">Pembayar / Penyetor
		<br><br><br><br><br><br>..........................................
		</td>
	</tr>
</table>
<br><br>
 <table>
	<tr>
		<td width="15%" style="padding: 5px;">
		Lember Ket
		</td>
		<td width="35%" style="text-align: left">
		: Untuk Pembayar / Penyetor / Pihak Ketiga
		</td>
		<td width="15%" style="padding: 5px;">
		
		</td>
		<td width="35%" style="text-align: left">

		</td>
	</tr>
	<tr>
		<td width="15%" style="padding: 5px;">
		Salinan 1
		</td>
		<td width="35%" style="text-align: left">
		: Untuk Bendahara Penerima
		</td>
		<td width="15%" style="padding: 5px;">
		Salinan 3
		</td>
		<td width="35%" style="text-align: left">
		: Perbendaharaan & Akunansi
		</td>
	</tr>
	<tr>
		<td width="15%" style="padding: 5px;">
		Salinan 2 
		</td>
		<td width="35%" style="text-align: left">
		: Untuk Bank
		</td>
		<td width="15%" style="padding: 5px;">
		Salinan 4
		</td>
		<td width="35%" style="text-align: left">
		: Arsip
		</td>
	</tr>
</table>