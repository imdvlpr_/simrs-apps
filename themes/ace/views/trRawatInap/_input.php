  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
 <table class="table">
   <tr>
     <td>Nama Pasien</td>
     <td>:</td>
     <td colspan="2"><strong><?php echo $pasien->NAMA.' / '.$rawatInap->jenisPasien->NamaGol;?></strong></td>
     
     <td>No Reg</td>
     <td>:</td>
     <td><?php echo $pasien->NoMedrec;?></td>
   </tr>
   <tr>
     <td>Tgl Masuk</td>
     <td>:</td>
     <td colspan="2"><?php echo $rawatInap->tanggal_masuk;?></td>
     
     <td>Tgl Keluar</td>
     <td>:</td>
     <td><?php echo !empty($rawatInap->tanggal_keluar) ? $rawatInap->tanggal_keluar : '-' ; ?></td>
   </tr>
   <tr>
     <td>Dirawat/R. Kelas</td>
     <td>:</td>
     <td><?php 

     echo $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;

     ?></td>
     <td></td>
     <td>Lm. Dirawat</td>
     <td>:</td>
     <td>
        <?php 

        $selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
        }

       

        echo $selisih_hari.' hari';

        ?>

     </td>
   </tr>
   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td colspan="2"><?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Dokter yang merawat</td>
     <td>:</td>
     <td>
    <?php 
    $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
      ?>
      <input id="nama_dokter_perawat" placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter_merawat" name="nama_dokter_merawat" autocomplete="off" value="<?php echo $nama_dokter;?>"/>

   
     <?php

      echo $form->hiddenField($rawatInap,'dokter_id');?>
     </td>

     </td>
   </tr>
 </table>


 <table class="table table-condensed" id="table_ri_rj">
   <thead>
   <tr>
     <th>A</th>
     <th>LAYANAN</th>
     <th colspan="2"  align="center">RAWAT DARURAT</th>
     <!-- <th>&nbsp;</th> -->
     <th colspan="2" align="center">RAWAT INAP</th>
     <!-- <th>&nbsp;</th> -->
     <th>TARIF</th>
   </tr>
   </thead>
   <tbody>
    <tr>
     <td>&nbsp;</td>
     <td>1. Observasi</td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<?php 
     $total_ird = $total_ird + $rawatRincian->obs_ird;
     echo $form->textField($rawatRincian,'obs_ird',array('class'=>'input-medium uang'));
     ?></td>
     <td>
       

     </td>
     <td>1. Biaya Kamar</td>
     <td>Rp <?php 

   //  echo number_format($rawatInap->kamar->biaya_kamar * $selisih_hari,0,',','.');
     
     echo CHtml::textField('biaya_per_malam',$rawatInap->kamar->biaya_kamar * $selisih_hari,array('class'=>'input-medium uang','readonly' => 'readonly')); 
     $total_irna = $total_irna + $rawatInap->kamar->biaya_kamar * $selisih_hari;
     $total_irna = $total_irna + $rawatRincian->askep_kamar;

     if(!empty($rawatRincian))
        $total_irna = $total_irna + $rawatRincian->asuhan_nutrisi;
     ?>
     </td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>2. Pengawasan Dokter</td>
     <td>
      <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter_ird" name="nama_dokter_ird" autocomplete="off" value="<?php echo $nama_dokter_ird;?>"/>

   
     <?php echo $form->hiddenField($rawatRincian,'dokter_ird');?>
     </td>
     <td>Rp <?php 

     echo $form->textField($rawatRincian,'biaya_pengawasan',array('class'=>'input-medium uang'));

     if(!empty($rawatRincian))
        $total_ird = $total_ird + $rawatRincian->biaya_pengawasan;
     ?>
     </td>
     <td>&nbsp;</td>
     <td>2. Askep <?php echo $rawatInap->kamar->nama_kamar;?></td>
     <td>Rp <?php
    echo CHtml::hiddenField('nilai_askep',$rawatInap->kamar->biaya_askep); 
    if(empty($rawatRincian->askep_kamar))
    {
      $rawatRincian->askep_kamar = $rawatInap->kamar->biaya_askep; 
    }  
    //  echo number_format($rawatInap->kamar->biaya_askep * $selisih_hari,0,',','.');
    echo $form->textField($rawatRincian,'askep_kamar',array('class'=>'input-medium uang')); 
    echo ' ';
   echo $form->textField($rawatRincian,'jumlah_askep',array('class'=>'input-small jumlah')); 
    
    ?>

    <!-- <a href="javascript:void(0)" id="input_askep" class="btn btn-warning" title="Input Askep Harian">Input</a> -->
     </td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>3. Asuhan Nutrisi</td>
     <td>
     Rp
     <?php 

    echo CHtml::hiddenField('nilai_asnut',$rawatInap->kamar->biaya_asnut); 
     
    if(empty($rawatRincian->asuhan_nutrisi))
    {
      $rawatRincian->asuhan_nutrisi = $rawatInap->kamar->biaya_asnut; 
    }  

     echo $form->textField($rawatRincian,'asuhan_nutrisi',array('class'=>'input-medium uang'));
     echo ' ';
     echo $form->textField($rawatRincian,'jumlah_asnut',array('class'=>'input-small jumlah')); 
    
     ?>
    <!-- <a href="javascript:void(0)" id="input_asnut" class="btn btn-warning" title="Input Asuhan Nutrisi Harian">Input</a> -->
    
     </td>
   </tr>
   </tbody>
   <thead>
   <tr>
     <th>B</th>
     <th colspan="6">PELAYANAN MEDIK</th>
     
   </tr>
   </thead>
   <tbody id="tbody-dokter">
     <?php 

   if(!is_null($rawatInap->getError('ERROR_VISITE'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_VISITE'); 
?>
</div>
</td>
</tr>
<?php 

}

    $urutan = 1;
      if(!empty($rawatInap->tRIVisiteDokters)){

      foreach($rawatInap->tRIVisiteDokters as $visite)
      {
       
        if(!empty($visite))
          $total_ird = $total_ird + $visite->biaya_visite * $visite->jumlah_visite_ird;

        if(!empty($visite))
          $total_irna = $total_irna + $visite->biaya_visite_irna * $visite->jumlah_visite;



        $dokterVisiteIRD = '';
        $dokterVisiteIRNA = '';
        if(!empty($visite->dokterIrd))
            $dokterVisiteIRD = $visite->dokterIrd->FULLNAME;

        if(!empty($visite->dokterIrna))
            $dokterVisiteIRNA = $visite->dokterIrna->FULLNAME;


    ?>
    <tr class="row-dokter">
     <td>&nbsp;</td>
     <td>
     <span class="number"><?php echo $urutan++;?>. </span>
     <input placeholder="Ketik Tindakan Dokter" type="text" class="input-medium nama_jenis_visite" name="nama_jenis_visite" autocomplete="off" value="<?php echo $visite->jenisVisite->nama_visite;?>"/>
        <input type="hidden" class="id_jenis_visite" name="id_jenis_visite[]" value="<?php echo $visite->id_jenis_visite;?>"/>
   
   
     </td>
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" value="<?php echo $dokterVisiteIRD;?>"/>
     <input type="hidden" class="id_dokter" name="id_dokter_visite_ird[]" value="<?php echo $visite->id_dokter;?>"/>
   
    </td>
     <td>
     Rp&nbsp;<input type="text" name="biaya_visite_ird[]" style="width:100px" class="input-medium uang" placeholder="-" value="<?php echo $visite->biaya_visite;?>"/>
     <input type="text" name="jumlah_visite_ird[]" class="input-small jumlah" placeholder="Jumlah Visite" value="<?php echo $visite->jumlah_visite_ird;?>"/>
     </td>
     
    <td>&nbsp;</td>
     
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" value="<?php echo $dokterVisiteIRNA;?>"/>
     <input type="hidden" class="id_dokter" name="id_dokter_visite_irna[]" value="<?php echo $visite->id_dokter_irna;?>"/>
   
    </td>
     <td>Rp&nbsp;<input type="text" name="biaya_visite_irna[]" class="input-medium uang" placeholder="-" value="<?php echo $visite->biaya_visite_irna;?>"/> 
     <input type="text" name="jumlah_visite[]" class="input-small jumlah" placeholder="Jumlah Visite" value="<?php echo $visite->jumlah_visite;?>"/>
     <a href="javascript:void(0)" class="hapus-dokter btn btn-warning" >Hapus</a>
     </td>

   </tr>
   <?php 
      }
    }

   ?>
    <tr id="row-dokter" class="row-dokter">
     <td>&nbsp;</td>
     <td>
     <span class="number"><?php echo $urutan++;?>. </span><input placeholder="Ketik Tindakan Dokter" type="text" class="input-medium nama_jenis_visite" name="nama_jenis_visite" autocomplete="off" />
        <input type="hidden" class="id_jenis_visite" name="id_jenis_visite[]" />
   
   
     </td>
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" />
     <input type="hidden" class="id_dokter" name="id_dokter_visite_ird[]" />
   
    </td>
     <td>
     Rp&nbsp;<input type="text" name="biaya_visite_ird[]" style="width:100px" class="input-medium uang" placeholder="-" value="0"/>
     <input type="text" name="jumlah_visite_ird[]" class="input-small jumlah" placeholder="Jumlah Visite" value="1"/>
     </td>
     
    <td>&nbsp;</td>
     
     <td>
     <input placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter" name="nama_dokter" autocomplete="off" />
     <input type="hidden" class="id_dokter" name="id_dokter_visite_irna[]" />
   
    </td>
     <td>Rp&nbsp;<input type="text" name="biaya_visite_irna[]" class="input-medium uang" placeholder="-" value="0"/> 
     <input type="text" name="jumlah_visite[]" class="input-small jumlah" placeholder="Jumlah Visite" value="1"/>
     <a href="javascript:void(0)" class="hapus-dokter btn btn-warning" >Hapus</a>
     </td>

   </tr>
  <tr class="row-tambah-dokter">
  <td>&nbsp;</td>
    <td colspan="6"><a id="tambah-dokter" href="javascript:void(0)" class=" btn btn-success"> Tambah Item </a></td>
  </tr>
  <tr>
     <th>&nbsp;</th>
     <th colspan="5"><strong>I. Tindakan Medik Operasi</strong></th>
     
     <th><div class="span7">Biaya</div><div class="span5">Jml</div></th>
   </tr>
  </tbody>
   <tbody id="tbody-top">
   <?php 
   if(!is_null($rawatInap->getError('ERROR_TOP'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_TOP'); 
?>
</div>
</td>
</tr>
<?php 

}

   $listTop = TindakanMedisOperatif::model()->findAll();
   $i = 0;


   foreach($listTop as $top){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_top' => $top->id_tindakan
      );
      $itemTop = TrRawatInapTop::model()->findByAttributes($attr);

      $jumlah_tindakan = 0;
      $biaya_irna = 0;

      $valueJm = '';
      $valueAnas = '';
      $biaya_irna_top_total = 0;
      if(!empty($itemTop)){
        $jumlah_tindakan = $itemTop->jumlah_tindakan;
        $biaya_irna = $itemTop->biaya_irna;

        $biaya_irna_top_total = $biaya_irna_top_total + $biaya_irna * $jumlah_tindakan;
        
      }

      $total_irna = $total_irna + $biaya_irna_top_total;
        
       

    ?>
   
   <tr>
     <td>&nbsp;</td>
     <td colspan="3"><span class="number-top"><?php echo strtolower(chr(64 + ($i+1)));?>. </span><?php echo $top->nama_tindakan;?></td>
     <td></td>
     <td style="text-align: right">
       <?php 



        if($top->kode_tindakan == 'JMO')
        {

          $drJM = '';

          $idDrJM = '';
          if(!empty($itemTop) && !empty($itemTop->dokterJm)){
            $drJM = $itemTop->dokterJm->nama_dokter;
            $idDrJM = $itemTop->dokterJm->id_dokter;
          }


       ?>
        <a id="input-top-jm" href="javascript:void(0)" title="Input Dokter">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
        </a>
        <!-- <input value="" placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter_jasa_medik" name="nama_dokter" autocomplete="off" /> -->
         <!-- <input type="hidden" value="<?php echo $idDrJM;?>" class="id_dokter_jasa_medik" name="id_dokter_jasa_medik" /> -->
   

       <?php 

       }

       else if($top->kode_tindakan == "JMA")
       {
          $drAnas = '';
          $idDrAnas = '';
          if(!empty($itemTop) && !empty($itemTop->dokterAnas)){
            $drAnas= $itemTop->dokterAnas->nama_dokter;
             $idDrAnas = $itemTop->dokterAnas->id_dokter;
          }



       ?>

       <a id="input-top-ja" href="javascript:void(0)" title="Input JM Anestesi">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
        </a>

      <!--  <input value="<?php echo $drAnas;?>" placeholder="Ketik Nama Dokter" type="text" class="input-medium nama_dokter_jasa_anas" name="nama_dokter" autocomplete="off" />
         <input type="hidden" value="<?php echo $idDrAnas;?>" class="id_dokter_jasa_anas" name="id_dokter_jasa_anas" />
    -->

       <?php 
     }



       ?>
     </td>
     <td >
     <?php 

     if($top->kode_tindakan == 'JMO')
      {
        $total_top_jm = 0;
        foreach($rawatInap->trRawatInapTopJm as $item)
        {

          $total_top_jm = $total_top_jm + $item->nilai;
        }
        ?>
     Rp&nbsp;<input type="text" name="biaya_irna_top[]" id="biaya_irna_top_jm" value="<?php echo $total_top_jm;?>" class="input-medium uang" placeholder="Biaya Layanan"/>
     <input type="text" name="jumlah_tindakan_top[]" class="input-small jumlah" placeholder="Jml Tndk" value="<?php echo $jumlah_tindakan;?>"/>
     <input type="hidden" name="id_tindakan_top[]" value="<?php echo $top->id_tindakan;?>"/>

        <?php 
      }

      else if($top->kode_tindakan == "JMA")
      {
        $total_top_ja = 0;
        foreach($rawatInap->trRawatInapTopJa as $item)
        {

          $total_top_ja = $total_top_ja + $item->nilai;
        }
        ?>
          Rp&nbsp;<input type="text" name="biaya_irna_top[]" id="biaya_irna_top_ja" value="<?php echo $total_top_ja;?>" class="input-medium uang" placeholder="Biaya Layanan"/>
     <input type="text" name="jumlah_tindakan_top[]" class="input-small jumlah" placeholder="Jml Tndk" value="<?php echo $jumlah_tindakan;?>"/>
     <input type="hidden" name="id_tindakan_top[]" value="<?php echo $top->id_tindakan;?>"/>
        <?php 
      }

    else{
     ?>
     Rp&nbsp;<input type="text" name="biaya_irna_top[]" value="<?php echo $biaya_irna;?>" class="input-medium uang" placeholder="Biaya Layanan"/>
     <input type="text" name="jumlah_tindakan_top[]" class="input-small jumlah" placeholder="Jml Tndk" value="<?php echo $jumlah_tindakan;?>"/>
     <input type="hidden" name="id_tindakan_top[]" value="<?php echo $top->id_tindakan;?>"/>
     <?php }?>
     </td>
   </tr>
     <?php
 $i++;
    }

    ?>
  <tr>
     <th>&nbsp;</th>
     <th colspan="2"><strong>II. Tindakan Medik Non-Operasi</strong></th>
     <th colspan="3"><div class="span5">Biaya</div><div class="span7">Biaya Satuan</div></th>
     
     <th><div class="span7">Biaya</div><div class="span5">Jml</div></th>
   </tr>
  </tbody>
   <tbody>
   <?php 

   if(!is_null($rawatInap->getError('ERROR_TNOP'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_TNOP'); 
?>
</div>
</td>
</tr>
<?php 
}

   $listTnop = TindakanMedisNonOperatif::model()->findAll();
   $i = 0;

   foreach($listTnop as $tnop){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_tnop' => $tnop->id_tindakan
      );
      $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);


      $jumlah_tindakan = 1;
      $biaya_irna = 0;
      $biaya_ird = 0;
      $biaya_irna_tnop_total = 0;

      if(!empty($itemTnop)){
        $jumlah_tindakan = $itemTnop->jumlah_tindakan;
        $biaya_irna = $itemTnop->biaya_irna;
        $biaya_ird = $itemTnop->biaya_ird;

        $biaya_irna_tnop_total = $biaya_irna_tnop_total + $biaya_irna * $jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_ird;
      $total_irna = $total_irna + $biaya_irna_tnop_total;
        
    ?>
   <tr>
     <td>&nbsp;</td>
     <td><span class="number-tnop"><?php echo strtolower(chr(64 + ($i+1)));?>. </span><?php echo $tnop->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input type="text" name="biaya_ird_tnop[]" value="<?php echo $biaya_ird;?>"  class="input-medium uang" placeholder="-" />
       <?php 

     // if($obat->nama_obat_alkes == 'Obat-obatan')
        echo CHtml::textField('nilai_per_tnop','0',array('class'=>'input-small uang  per_item')); 

     ?>
       
     </td>
     <td></td>
     <td style="text-align: right">
     <script type="text/javascript">
       $(document).ready(function(){
         $('#input-tnop_<?php echo $tnop->kode_tindakan;?>').click(function(){
    // alert('$');
          $( '#dialog-tnop-jasa-<?php echo $tnop->kode_tindakan;?>' )
                .dialog( { title: 'Input Tindakan <?php echo $tnop->nama_tindakan;?>' } )
                .dialog( 'open' );
        });
       });
     </script>
    <a id="input-tnop_<?php echo $tnop->kode_tindakan;?>" class="input-tnop" href="javascript:void(0)" title="Input Tindakan">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/icon-form.png"/>
    </a>
    </td>
     <td>
   <?php 
   $jml_tnop_irna = 0 ;
    if($tnop->kode_tindakan == 'jrm')
    { 
        $t = 0;
        foreach($rawatInap->trRawatInapTnopJrm as $item)
        {
            $t = $t + $item->nilai;
        }

        $jml_tnop_irna = $t;
    }

    else if($tnop->kode_tindakan == 'jm')
    { 
        $t = 0;
        foreach($rawatInap->trRawatInapTnopJm as $item)
        {
            $t = $t + $item->nilai;
        }

        $jml_tnop_irna = $t;
    }

    else if($tnop->kode_tindakan == 'japel')
    { 
        $t = 0;
        foreach($rawatInap->trRawatInapTnopJapel as $item)
        {
            $t = $t + $item->nilai;
        }

        $jml_tnop_irna = $t;
    }
   ?>
     Rp&nbsp;<input type="text" id="biaya_irna_tnop_<?php echo $tnop->kode_tindakan;?>" name="biaya_irna_tnop[]" value="<?php echo $jml_tnop_irna;?>"  class="input-medium uang" placeholder="-"/>
      <input type="text" name="jumlah_tindakan_tnop[]" class="input-small jumlah" placeholder="Jml Tndk" value="<?php echo $jumlah_tindakan;?>"/>

     <input type="hidden" name="id_tindakan_tnop[]" value="<?php echo $tnop->id_tindakan;?>"/>
     
     </td>
   </tr>
     <?php
 $i++;
    }

    ?>
   
   
   </tbody>
   <thead>
   <tr>
     <th>C</th>
     <th colspan="6">LAYANAN PENUNJANG</th>
     
   </tr>
   </thead>
   <tbody id="tbody-supp">
    <?php 
   if(!is_null($rawatInap->getError('ERROR_SUPP'))){
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_SUPP'); 
?>
</div>
</td>
</tr>
<?php 

}

    $urutan = 1;
    $biaya_supp_irna = 0;
      foreach($rawatInap->trRawatInapPenunjangs as $supp)
      {
          
          $total_ird = $total_ird + $supp->biaya_ird;
         // $total_irna = $total_irna + $supp->biaya_irna;
          $biaya_supp_irna = $biaya_supp_irna + $supp->biaya_irna * $supp->jumlah_tindakan;

          $total_irna = $total_irna + $biaya_supp_irna;
        

    ?>
     <tr class="row-supp">
     <td>&nbsp;</td>
     <td>
       <span class="number-supp"><?php echo $urutan++;?>. </span>
       <input value="<?php echo $supp->penunjang->nama_tindakan;?>" type="text" class="input-medium jenis-supp" name="jenis-supp" autocomplete="off" />
        <input value="<?php echo $supp->id_penunjang;?>" type="hidden" class="id_jenis_supp" name="id_jenis_supp[]" />
     </td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input value="<?php echo $supp->biaya_ird;?>" type="text" name="biaya_ird_supp[]" class="input-medium uang" placeholder="-" /></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input value="<?php echo $supp->biaya_irna;?>" type="text" name="biaya_irna_supp[]" class="input-medium uang" placeholder="-"/>
     <input type="text" value="<?php echo $supp->jumlah_tindakan;?>" name="jumlah_tindakan_supp[]" class="input-small jumlah" placeholder="Jml Lyn"/>
     <a href="javascript:void(0)" class="hapus-supp btn btn-warning" >Hapus</a>
     </td>
   </tr>
    <?php 
    
    }

    ?>
    <tr id="row-supp" class="row-supp">
     <td>&nbsp;</td>
     <td>
       <span class="number-supp"><?php echo $urutan++;?>. </span><input placeholder="Jenis Layanan Penunjang" type="text" class="input-medium jenis-supp" name="jenis-supp" autocomplete="off" />
        <input type="hidden" class="id_jenis_supp" name="id_jenis_supp[]" />
     </td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input type="text" value="0" name="biaya_ird_supp[]" class="input-medium uang" placeholder="-" /></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input type="text" name="biaya_irna_supp[]" value="0"  class="input-medium uang" placeholder="-"/>
     <input type="text" name="jumlah_tindakan_supp[]" class="input-small jumlah" placeholder="Jml Lyn" value="1"/>
     <a href="javascript:void(0)" class="hapus-supp btn btn-warning" >Hapus</a>
     </td>
   </tr>
  <tr class="row-tambah-supp">
  <td>&nbsp;</td>
    <td colspan="6"><a id="tambah-supp" href="javascript:void(0)" class=" btn btn-success"> Tambah Layanan </a></td>
  </tr>
   </tbody>
   <thead>
   <tr>
     <th>D</th>
     <th colspan="2"><strong>OBAT dan ALAT KESEHATAN</strong></th>
       <th colspan="3"><div class="span5">Biaya</div><div class="span7">Biaya Satuan</div></th>
     
     <th><div class="span6">Biaya</div><div class="span4">Biaya Satuan</div><div class="span2">Jml</div></th>
   </tr>
   </thead>
   <tbody>
   <?php 
   if(!is_null($rawatInap->getError('ERROR_ALKES')))
   {
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_ALKES'); 
?>
</div>
</td>
</tr>
<?php 
}
   $listObat = ObatAlkes::model()->findAll();
   $i = 0;
   foreach($listObat as $obat){
      $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_alkes' => $obat->id_obat_alkes
      );
      $itemObat = TrRawatInapAlkes::model()->findByAttributes($attr);

      $jumlah_tindakan = 1;
      $biaya_irna = $obat->tarip;
      $biaya_ird = 0;

      $biaya_obat_ird_total = 0;
      $biaya_obat_irna_total = 0;
      if(!empty($itemObat)){
        $jumlah_tindakan = $itemObat->jumlah_tindakan;
        $biaya_irna = $itemObat->biaya_irna;
        $biaya_ird = $itemObat->biaya_ird;
        $biaya_obat_ird_total = $biaya_obat_ird_total + $biaya_ird;
        $biaya_obat_irna_total = $biaya_obat_irna_total + $biaya_irna * $jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_obat_ird_total;
      $total_irna = $total_irna + $biaya_obat_irna_total;
        
    ?>
    <tr>
     <td>&nbsp;</td>
     <td><span><?php echo ($i+1);?>. </span><?php echo $obat->nama_obat_alkes;?></td>
     <td></td>
     <td colspan="3">
Rp
     <input type="text" name="biaya_ird_alkes[]" class="input-medium" value="<?php echo $biaya_ird;?>" />

     <?php 

     // if($obat->nama_obat_alkes == 'Obat-obatan')
        echo CHtml::textField('nilai_per_obat','0',array('class'=>'input-small uang  per_item')); 

     ?>
       
     </td>
    
     
     <td>Rp
     <input type="text" name="biaya_irna_alkes[]" class="input-medium uang" value="<?php echo $biaya_irna;?>"/>
     <?php echo CHtml::textField('nilai_per_obat_irna','0',array('class'=>'input-small uang  per_item')); ?>
     
     <input type="text" name="jumlah_obat[]" class="input-small jumlah" placeholder="Jml Obat" value="<?php echo $jumlah_tindakan;?>"/>
     <input type="hidden" name="id_tindakan_obat[]" value="<?php echo $obat->id_obat_alkes;?>"/>
     
     </td>
   </tr>
   <?php 
   $i++;
   }

   ?>
   
   </tbody>
   <thead>
   <tr>
     <th colspan="7"><strong>E. Lain-lain</strong></th>
     
     
   </tr>
   </thead>
   <tbody >
   <?php 
   if(!is_null($rawatInap->getError('ERROR_LAIN')))
   {
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_LAIN'); 
?>
</div>
</td>
</tr>
<?php 
}

   $listLain = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>$rawatInap->kamar->kelas_id));
   $i = 0;
   foreach($listLain as $lain){
       $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_lain' => $lain->id_tindakan
      );

      $itemLain = TrRawatInapLain::model()->findByAttributes($attr);
      $jumlah_tindakan = 1;
      $biaya_irna = $lain->tarip;
      $biaya_ird = 0;

      $biaya_irna_lain_total = 0;
      if(!empty($itemLain)){
        $jumlah_tindakan = $itemLain->jumlah_tindakan;
        $biaya_irna = $itemLain->biaya_irna;
        $biaya_ird = $itemLain->biaya_ird;
        $biaya_irna_lain_total = $biaya_irna_lain_total + $itemLain->biaya_irna * $jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_ird;
      $total_irna = $total_irna + $biaya_irna_lain_total;
    ?>
    <tr>
     <td>&nbsp;</td>
     <td> <span class="number-rm"><?php echo $i+1;?>. </span><?php echo $lain->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input type="text" name="biaya_ird_lain[]" value="<?php echo $biaya_ird;?>" class="input-medium uang" placeholder="-" /></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input type="text" name="biaya_irna_lain[]" value="<?php echo $biaya_irna;?>" class="input-medium uang" placeholder="-"/>
       <input type="text" name="jumlah_rm[]" class="input-small jumlah" value="<?php echo $jumlah_tindakan;?>" placeholder="Jml Item"/>
  <input type="hidden" name="id_tindakan_lain[]" value="<?php echo $lain->id_tindakan;?>"/>
     
     </td>
   </tr>
   <?php 
   $i++;
      }
   ?>
     
   </tbody>
      <thead>
      <tr>
       <th>&nbsp;</th>
       <th colspan="6"><strong>TOTAL</strong></th>
       
     </tr>
     </thead>
     <tbody >
       <tr >
         <td>&nbsp;</td>
         <td></td>
         <td><strong>IRD</strong></td>
         <td><div class="span11" style="text-align: right;"><strong>Rp <?php echo number_format($total_ird,2,',','.');?></strong></div></td>
         <td></td>
         <td><strong><?php echo $rawatInap->kamar->nama_kamar.' / '.$rawatInap->kamar->kelas->nama_kelas;?></strong></td>
         <td><div class="span6" style="text-align: right"><strong>Rp <?php echo number_format($total_irna,2,',','.');?></strong></div></td>
       </tr>
       <tr >
          <td colspan="5"></td>
         <td><strong>IRD</strong></td>
         <td><div class="span6" style="text-align: right"><strong>Rp <?php echo number_format($total_ird,2,',','.');?></strong></div></td>
       </tr>
        <tr >
          <td colspan="5"></td>
         <td><strong>Sub Total</strong></td>
         <td><div class="span6" style="text-align: right"><strong>Rp <?php 
         $subtotal = $total_ird + $total_irna;
         echo number_format($subtotal,2,',','.');
         ?></strong></div></td>
       </tr>
       <tr >
         <td colspan="5"></td>
         <td><strong>Paket 1</strong></td>
         <td><div class="span6" style="text-align: right">
         <?php 
         echo $form->textField($rawatInap,'biaya_paket_1',array('class' => 'input-small uang'));
         ?>
           </div>
         </td>
       </tr>
        <tr >
         <td colspan="5"></td>
         <td><strong>Paket 2</strong></td>
         <td><div class="span6" style="text-align: right">
          <?php 
         echo $form->textField($rawatInap,'biaya_paket_2',array('class' => 'input-small uang'));
         ?>
           </div>
         </td>
       </tr>
       <tr style="background-color: #25ade3;color:white;font-size: 16px">
          <td colspan="5"></td>
         <td><strong>TOTAL BIAYA</strong></td>
         <td ><div class="span6" style="text-align: right"><strong>Rp <?php 
        $total_all = $subtotal + $rawatInap->biaya_paket_1 + $rawatInap->biaya_paket_2;
         echo number_format($total_all,2,',','.');

         ?></strong></div></td>
       </tr>
     </tbody>
    </table>

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
      <a href="<?php echo Yii::app()->createUrl('trRawatInap/cetakKwitansi',array('id'=>$rawatInap->id_rawat_inap));?>" class="btn btn-warning" ><i class="icon-print"></i>&nbsp;CETAK</a>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>


<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');
  $cs->registerCssFile($baseUrl.'/vendors/jquery-ui.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-ui.min.js');
 
?>

<script>

$('#nama_dokter_perawat').focus();

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});

// $('#input_askep').on("click",function(){

//     var input = $('#TrRawatInapRincian_jumlah_askep').val();

//     if(jQuery.isNumeric(input))
//     {
//         var nAskep = $('#nilai_askep').val();
//         var nAskep = eval(nAskep) + eval(input);
//         $('#TrRawatInapRincian_askep_kamar').val(nAskep);
//         $(this).val(0)
//     }
// });

// $('#input_asnut').on("click",function(){

//     var input = $('#TrRawatInapRincian_jumlah_asnut').val();

//     if(jQuery.isNumeric(input))
//     {
//         var nAsnut = $('#nilai_asnut').val();
//         var nAsnut = eval(nAsnut) + eval(input);
//         $('#TrRawatInapRincian_asuhan_nutrisi').val(nAsnut);
//         $(this).val(0)
//     }
// });

$('#tambah-dokter').on("click",function(){


  if($('span.number').length < 7)
  {
    var clone = $('#row-dokter').clone();
    
    clone.children('td').find('.nama_jenis_visite').val('');
    clone.children('td').find('.nama_dokter').val('');
    clone.children('td').find('.id_dokter').val('');
    clone.children('td').find('.uang').val('');
    clone.children('td').find('.jumlah').val('');
    

    $('.row-tambah-dokter').before(clone);



    var i = 1;
    $('span.number').each(function(){
      $(this).html(i+". ");
      i++;

    });
  }
});



$('#tambah-supp').on("click",function(){


  if($('span.number-supp').length < 10)
  {
    var clone = $('#row-supp').clone();
    $('#row-supp').after(clone);

    var i = 1;
    $('span.number-supp').each(function(){
      $(this).html(i+". ");
      i++;

    });
  }
});




$(document).bind("keyup.autocomplete",function(){


  $('.jenis-supp').autocomplete({
      minLength:1,
      select:function(event, ui){
        
           $(this).next().val(ui.item.id);
      },
      focus: function (event, ui) {
         $(this).next().val(ui.item.id);
       
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetLayananPenunjang');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 

  $('.nama_dokter_jasa_anas').autocomplete({
      minLength:1,
      select:function(event, ui){
         $(this).next().val(ui.item.id);
                
      },
      focus: function (event, ui) {
         $(this).next().val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 

  $('.nama_dokter_jasa_medik').autocomplete({
      minLength:1,
      select:function(event, ui){
         $(this).next().val(ui.item.id);
                
      },
      focus: function (event, ui) {
         $(this).next().val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 

   $('.nama_dokter_merawat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#TrRawatInap_dokter_id').val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $('#TrRawatInap_dokter_id').val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 


$('.nama_dokter_ird').autocomplete({
      minLength:1,
      select:function(event, ui){
       
        $('#TrRawatInapRincian_dokter_ird').val(ui.item.id);
                
      },
      
      focus: function (event, ui) {
        $('#TrRawatInapRincian_dokter_ird').val(ui.item.id);
       
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 


  $('.nama_dokter').autocomplete({
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 

  $('.nama_jenis_visite').autocomplete({
      minLength:1,
      select:function(event, ui){
        
           $(this).next().val(ui.item.id);
      },
       focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetJenisVisiteDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});


$(document).on("click",".hapus-supp",function(){
  
  var ukuran = $(this).closest("#tbody-supp").children('.row-supp').size();
  
  if(ukuran>1){
      
      $(this).parent().parent().remove();

      var i = 1;
      $('span.number-supp').each(function(){
        $(this).html(i+". ");
        i++;
       
      });
      return false;
  }

});


$('#TrRawatInapRincian_jumlah_askep').keyup(function(e){
    
    var input = $(this).val();

    if(jQuery.isNumeric(input))
    {
        var item = $('#nilai_askep').val();
      
        var value = eval(item) * eval(input);
        $('#TrRawatInapRincian_askep_kamar').val(value);
    }
        
});


$('#TrRawatInapRincian_jumlah_asnut').keyup(function(e){
    
    var input = $(this).val();

    if(jQuery.isNumeric(input))
    {
        var item = $('#nilai_asnut').val();
      
        var value = eval(item) * eval(input);
        $('#TrRawatInapRincian_asuhan_nutrisi').val(value);
    }
        
});


function isiTindakan(ui)
{
   $("#TrRawatInapLayanan_id_tindakan").val(ui.item.id);
   $("#TrRawatInapLayanan_nama_tindakan").val(ui.item.value); 
   

}

$(document).on("click",".hapus-pop-tnop-jrm",function(){
  
  var ukuran = $(this).closest("#table-popup-tnop-jrm-tbody").children('tr.pop-row-dokter-tnop-jrm').size();
  // alert(ukuran);
  if(ukuran>1){
      
      $(this).parent().parent().remove();

      var i = 1;
      $('span.order-jrm').each(function(){
        $(this).html(i+". ");
        i++;
       
      });
      return false;
  }

});

$(document).on("click",".hapus-pop-top-jm",function(){
  
  var ukuran = $(this).closest("#table-popup-top-jm-tbody").children('tr.pop-row-dokter-top').size();
  // alert(ukuran);
  if(ukuran>1){
      
      $(this).parent().parent().remove();

      var i = 1;
      $('span.order').each(function(){
        $(this).html(i+". ");
        i++;
       
      });
      return false;
  }

});

$(document).on("click",".hapus-pop-top-ja",function(){
  
  var ukuran = $(this).closest("#table-popup-top-ja-tbody").children('tr.pop-row-dokter-top-ja').size();
  // alert(ukuran);
  if(ukuran>1){
      
      $(this).parent().parent().remove();

      var i = 1;
      $('span.order-ja').each(function(){
        $(this).html(i+". ");
        i++;
       
      });
      return false;
  }

});


$(document).on("keydown.autocomplete",".pop-top-dokter",function(e){
  $('.pop-top-dokter').autocomplete({
      appendTo : '#dialog-top-jasa-medik',
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

$(document).on("keydown.autocomplete",".pop-top-dokter-ja",function(e){
  $('.pop-top-dokter-ja').autocomplete({
      appendTo : '#dialog-top-jasa-anastesi',
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
                
      },
      focus: function (event, ui) {
        $(this).next().val(ui.item.id);
        
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

$(document).ready(function(){
  


  $('#input-top-jm').click(function(){
    // alert('$');
    $( '#dialog-top-jasa-medik' )
          .dialog( { title: 'Input Tindakan Jasa Medik' } )
          .dialog( 'open' );
  });

  $('#input-top-ja').click(function(){
    // alert('$');
    $( '#dialog-top-jasa-anastesi' )
          .dialog( { title: 'Input Tindakan Jasa Anastesi' } )
          .dialog( 'open' );
  });

   $('#pop-tambah-tnop-japel').on("click",function(){
    if($('span.order-japel').length < 7)
    {
      
      var row = '<tr class="pop-row-dokter-tnop-japel">';
        row += '<td><span class="order-japel"></span></td>';
        row += '<td><input type="text" class="input-medium pop-tnop-ket-japel" name="pop-tnop-ket-japel"/></td>';
        row += '<td><input type="text" class="input-medium uang pop-tnop-japel-biaya" name="pop-tnop-japel-biaya[]" value="0"/></td>';
        row += '<td><a href="javascript:void(0)" class="hapus-pop-tnop-japel btn btn-warning" >Hapus</a></td>';
        row += '</tr>';


      $('#table-popup-tnop-japel > tbody tr:last').before(row);



      var i = 1;
      $('span.order-japel').each(function(){
        $(this).html(i+". ");
        i++;

      });
    }
  });

  $('#pop-tambah-tnop-jm').on("click",function(){
    if($('span.order-jm').length < 7)
    {
      
      var row = '<tr class="pop-row-dokter-tnop-jm">';
        row += '<td><span class="order-jm"></span></td>';
        row += '<td><input type="text" class="input-medium pop-tnop-ket-jm" name="pop-tnop-ket-jm"/></td>';
        row += '<td><input type="text" class="input-medium uang pop-tnop-jm-biaya" name="pop-tnop-jm-biaya[]" value="0"/></td>';
        row += '<td><a href="javascript:void(0)" class="hapus-pop-tnop-jm btn btn-warning" >Hapus</a></td>';
        row += '</tr>';


      $('#table-popup-tnop-jm > tbody tr:last').before(row);



      var i = 1;
      $('span.order-jm').each(function(){
        $(this).html(i+". ");
        i++;

      });
    }
  });


  $('#pop-tambah-tnop-jrm').on("click",function(){
    if($('span.order-jrm').length < 7)
    {
      
      var row = '<tr class="pop-row-dokter-tnop-jrm">';
        row += '<td><span class="order-jrm"></span></td>';
        row += '<td><input type="text" class="input-medium pop-tnop-ket-jrm" name="pop-tnop-ket-jrm"/></td>';
        row += '<td><input type="text" class="input-medium uang pop-tnop-jrm-biaya" name="pop-tnop-jrm-biaya[]" value="0"/></td>';
        row += '<td><a href="javascript:void(0)" class="hapus-pop-tnop-jrm btn btn-warning" >Hapus</a></td>';
        row += '</tr>';


      $('#table-popup-tnop-jrm > tbody tr:last').before(row);



      var i = 1;
      $('span.order-jrm').each(function(){
        $(this).html(i+". ");
        i++;

      });
    }
  });

  $('#pop-tambah-top-jm').on("click",function(){


    if($('span.order').length < 7)
    {
      
      var row = '<tr class="pop-row-dokter-top">';
        row += '<td><span class="order"></span></td>';
        row += '<td><input type="text" placeholder="Ketik Nama Dokter" class="input-medium pop-top-dokter" name="pop-top-dokter[]" /><input type="hidden" class="pop-top-dokter-id" name="pop-top-dokter-id[]" /></td>';
        row += '<td><input type="text" class="input-medium uang pop-top-dokter-biaya" name="pop-top-dokter-biaya[]" value="0"/></td>';
        row += '<td><a href="javascript:void(0)" class="hapus-pop-top-jm btn btn-warning" >Hapus</a></td>';
        row += '</tr>';


      $('#table-popup-top-jm > tbody tr:last').before(row);



      var i = 1;
      $('span.order').each(function(){
        $(this).html(i+". ");
        i++;

      });
    }
  });

   $('#pop-tambah-top-ja').on("click",function(){


    if($('span.order-ja').length < 7)
    {
      
      var row = '<tr class="pop-row-dokter-top-ja">';
        row += '<td><span class="order-ja"></span></td>';
        row += '<td><input type="text" placeholder="Ketik Nama Dokter" class="input-medium pop-top-dokter-ja" name="pop-top-dokter[]" /><input type="hidden" class="pop-top-dokter-ja-id" name="pop-top-dokter-ja-id[]" /></td>';
        row += '<td><input type="text" class="input-medium uang pop-top-dokter-ja-biaya" name="pop-top-dokter-ja-biaya[]" value="0"/></td>';
        row += '<td><a href="javascript:void(0)" class="hapus-pop-top-ja btn btn-warning" >Hapus</a></td>';
        row += '</tr>';


      $('#table-popup-top-ja > tbody tr:last').before(row);



      var i = 1;
      $('span.order-ja').each(function(){
        $(this).html(i+". ");
        i++;

      });
    }
  });

  $('#pop-tnop-japel-simpan').click(function(){

    var keterangan = $('input.pop-tnop-ket-japel');
    var ri = <?php echo $rawatInap->id_rawat_inap;?>;
    
    var biaya = $('input.pop-tnop-japel-biaya');
    var objects = [];
    var total_biaya = 0;

    // console.log(keterangan);

    for (var i = 0; i < keterangan.length; i++) {
      var d = keterangan[i];
      var b = biaya[i];
      var json_data = new Object();
      json_data.ri = ri;
      json_data.keterangan = d.value;
      json_data.biaya = b.value;
      total_biaya = eval(total_biaya) + eval(b.value);
      objects[i] = json_data;
     
    }

    $.ajax({
      type : 'POST',
      url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputTnopJapel');?>',
      dataType: 'json',
      data : JSON.stringify(objects),
      success : function(data){
        $('#biaya_irna_tnop_japel').val(total_biaya);
        $('#pop-japel-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
        
      }
    });
  });

  $('#pop-tnop-jm-simpan').click(function(){

    var keterangan = $('input.pop-tnop-ket-jm');
    var ri = <?php echo $rawatInap->id_rawat_inap;?>;
    
    var biaya = $('input.pop-tnop-jm-biaya');
    var objects = [];
    var total_biaya = 0;

    // console.log(keterangan);

    for (var i = 0; i < keterangan.length; i++) {
      var d = keterangan[i];
      var b = biaya[i];
      var json_data = new Object();
      json_data.ri = ri;
      json_data.keterangan = d.value;
      json_data.biaya = b.value;
      total_biaya = eval(total_biaya) + eval(b.value);
      objects[i] = json_data;
     
    }

    $.ajax({
      type : 'POST',
      url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputTnopJM');?>',
      dataType: 'json',
      data : JSON.stringify(objects),
      success : function(data){
        $('#biaya_irna_tnop_jm').val(total_biaya);
        $('#pop-tnop-jm-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
        
      }
    });
  });

  $('#pop-tnop-jrm-simpan').click(function(){

    var keterangan = $('input.pop-tnop-ket-jrm');
    var ri = <?php echo $rawatInap->id_rawat_inap;?>;
    
    var biaya = $('input.pop-tnop-jrm-biaya');
    var objects = [];
    var total_biaya = 0;

    // console.log(keterangan);

    for (var i = 0; i < keterangan.length; i++) {
      var d = keterangan[i];
      var b = biaya[i];
      var json_data = new Object();
      json_data.ri = ri;
      json_data.keterangan = d.value;
      json_data.biaya = b.value;
      total_biaya = eval(total_biaya) + eval(b.value);
      objects[i] = json_data;
     
    }

    $.ajax({
      type : 'POST',
      url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputTnopJRM');?>',
      dataType: 'json',
      data : JSON.stringify(objects),
      success : function(data){
        $('#biaya_irna_tnop_jrm').val(total_biaya);
        $('#pop-jrm-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
        
      }
    });
  });

  $('#pop-top-jm-simpan').click(function(){

    var dokter = $('input.pop-top-dokter-id');
    var ri = <?php echo $rawatInap->id_rawat_inap;?>;
    
    var biaya = $('input.pop-top-dokter-biaya');
    var objects = [];
    var total_biaya = 0;
    for (var i = 0; i < dokter.length; i++) {
      var d = dokter[i];
      var b = biaya[i];

      if(d != '' && b != '')
      {
          var json_data = new Object();
          json_data.ri = ri;
          json_data.dokter = d.value;
          json_data.biaya = b.value;
          total_biaya = eval(total_biaya) + eval(b.value);
          objects[i] = json_data;
      }

      
    }

    $.ajax({
      type : 'POST',
      url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputTopJM');?>',
      dataType: 'json',
      data : JSON.stringify(objects),
      success : function(data){
        $('#biaya_irna_top_jm').val(total_biaya);
        $('#pop-jm-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
        
      }
    });


   
  });


   $('#pop-top-ja-simpan').click(function(){

    var dokter = $('input.pop-top-dokter-ja-id');
    var ri = <?php echo $rawatInap->id_rawat_inap;?>;
    
    var biaya = $('input.pop-top-dokter-ja-biaya');
    var objects = [];
    var total_biaya = 0;
    for (var i = 0; i < dokter.length; i++) {
      var d = dokter[i];
      var b = biaya[i];

      if(d != '' && b != '')
      {
          var json_data = new Object();
          json_data.ri = ri;
          json_data.dokter = d.value;
          json_data.biaya = b.value;
          total_biaya = eval(total_biaya) + eval(b.value);
          objects[i] = json_data;
      }

      
    }

    $.ajax({
      type : 'POST',
      url : '<?php echo Yii::app()->createUrl('AjaxRequest/inputTopJA');?>',
      dataType: 'json',
      data : JSON.stringify(objects),
      success : function(data){
        $('#biaya_irna_top_ja').val(total_biaya);
        $('#pop-ja-info').html('<div class="alert alert-'+data.code+'">'+data.msg+'</div>');
        
      }
    });


   
  });
});


</script>

<!-- 
########### Jasa Medik
 -->
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-top-jasa-medik',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 500,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-jm-info"></div>
<table id="table-popup-top-jm" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Nama Dokter</th>
  <th>Jumlah</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-top-jm-tbody">

<?php
 $order = 1;
$total = 0;
 foreach($rawatInap->trRawatInapTopJm as $item)
 {

  $total = $total + $item->nilai;
?>

<tr id="pop-row-dokter-top" class="pop-row-dokter-top">
  <td><span class="order"><?php echo $order++;?>. </span></td>
  <td><input type="text" placeholder="Ketik Nama Dokter" class="input-medium pop-top-dokter" name="pop-top-dokter" id="pop-top-dokter" value="<?php echo $item->dokter->nama_dokter;?>"/>
  <input type="hidden" class="pop-top-dokter-id" name="pop-top-dokter-id[]" value="<?php echo $item->id_dokter;?>"//>
  </td>
  <td><input type="text" class="input-medium uang pop-top-dokter-biaya" name="pop-top-dokter-biaya[]" id="pop-top-dokter-biaya" value="<?php echo $item->nilai;?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-top-jm btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-tambah-top-jm" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-top-jm-simpan" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">SIMPAN</a>
</div>
<?php $this->endWidget(); ?>



<!-- 
########### Jasa Anestesi
 -->
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-top-jasa-anastesi',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 500,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-ja-info"></div>
<table id="table-popup-top-ja" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Nama Dokter</th>
  <th>Jumlah</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-top-ja-tbody">

<?php
 $order = 1;
$total = 0;
 foreach($rawatInap->trRawatInapTopJa as $item)
 {

  $total = $total + $item->nilai;
?>

<tr id="pop-row-dokter-top-ja" class="pop-row-dokter-top-ja">
  <td><span class="order-ja"><?php echo $order++;?>. </span></td>
  <td><input type="text" placeholder="Ketik Nama Dokter" class="input-medium pop-top-dokter-ja" name="pop-top-dokter-ja" id="pop-top-dokter" value="<?php echo $item->dokter->nama_dokter;?>"/>
  <input type="hidden" class="pop-top-dokter-ja-id" name="pop-top-dokter-ja-id[]" value="<?php echo $item->id_dokter;?>"//>
  </td>
  <td><input type="text" class="input-medium uang pop-top-dokter-ja-biaya" name="pop-top-dokter-ja-biaya[]" id="pop-top-dokter-ja-biaya" value="<?php echo $item->nilai;?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-top-ja btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-tambah-top-ja" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-top-ja-simpan" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">SIMPAN</a>
</div>
<?php $this->endWidget(); ?>




<!-- 
***************************************************************
 Jasa RUMAH SAKIT
***************************************************************
 -->
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-tnop-jasa-jrm',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 500,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-jrm-info"></div>
<table id="table-popup-tnop-jrm" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Keterangan</th>
  <th>Biaya</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-tnop-jrm-tbody">

<?php
 $order = 1;
$total = 0;
 foreach($rawatInap->trRawatInapTnopJrm as $item)
 {

  $total = $total + $item->nilai;
?>

<tr id="pop-row-dokter-tnop-jrm" class="pop-row-dokter-tnop-jrm">
  <td><span class="order-jrm"><?php echo $order++;?>. </span></td>
  <td><input type="text" class="input-medium pop-tnop-ket-jrm" name="pop-tnop-ket-jrm" value="<?php echo $item->keterangan;?>"/></td>
  <td><input type="text" class="input-medium uang pop-tnop-jrm-biaya" name="pop-tnop-jrm-biaya[]" value="<?php echo $item->nilai;?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-tnop-jrm btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-tambah-tnop-jrm" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-tnop-jrm-simpan" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">SIMPAN</a>
</div>
<?php $this->endWidget(); ?>


<!-- 
***************************************************************
 Jasa TNOP Jasa Medik
***************************************************************
 -->
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-tnop-jasa-jm',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 500,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-tnop-jm-info"></div>
<table id="table-popup-tnop-jm" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Keterangan</th>
  <th>Biaya</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-tnop-jm-tbody">

<?php
 $order = 1;
$total = 0;
 foreach($rawatInap->trRawatInapTnopJm as $item)
 {

  $total = $total + $item->nilai;
?>

<tr id="pop-row-dokter-tnop-jm" class="pop-row-dokter-tnop-jm">
  <td><span class="order-jm"><?php echo $order++;?>. </span></td>
  <td><input type="text" class="input-medium pop-tnop-ket-jm" name="pop-tnop-ket-jm" value="<?php echo $item->keterangan;?>"/></td>
  <td><input type="text" class="input-medium uang pop-tnop-jm-biaya" name="pop-tnop-jm-biaya[]" value="<?php echo $item->nilai;?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-tnop-jm btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-tambah-tnop-jm" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-tnop-jm-simpan" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">SIMPAN</a>
</div>
<?php $this->endWidget(); ?>


<!-- 
***************************************************************
 Jasa TNOP JAPEL
***************************************************************
 -->
<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'dialog-tnop-jasa-japel',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 500,
    'height' => 500,
    'resizable' => true,
  ),
)); ?>
<div class="update-dialog-content" align="center">
<div id="pop-japel-info"></div>
<table id="table-popup-tnop-japel" class="table table-condensed">
<thead>
<tr>
  <th>No</th>
  <th>Keterangan</th>
  <th>Biaya</th>
  <th>#</th>
</tr>
</thead>
<tbody id="table-popup-tnop-japel-tbody">

<?php
 $order = 1;
$total = 0;
 foreach($rawatInap->trRawatInapTnopJapel as $item)
 {

  $total = $total + $item->nilai;
?>

<tr id="pop-row-dokter-tnop-japel" class="pop-row-dokter-tnop-japel">
  <td><span class="order-japel"><?php echo $order++;?>. </span></td>
  <td><input type="text" class="input-medium pop-tnop-ket-japel" name="pop-tnop-ket-japel" value="<?php echo $item->keterangan;?>"/></td>
  <td><input type="text" class="input-medium uang pop-tnop-japel-biaya" name="pop-tnop-japel-biaya[]" value="<?php echo $item->nilai;?>"/></td>
  <td><a href="javascript:void(0)" class="hapus-pop-tnop-japel btn btn-warning" >Hapus</a></td>
</tr>
<?php 
}
?>
<tr>
  <td colspan="3"><a id="pop-tambah-tnop-japel" href="javascript:void(0)" class="btn btn-success"> Tambah Item </a></td>
</tr>
</tbody>
</table>

<a id="pop-tnop-japel-simpan" style="padding:10px 50px;color:white" href="javascript:void(0)" class="btn btn-info">SIMPAN</a>
</div>
<?php $this->endWidget(); ?>