  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($rawatInap,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($rawatRincian,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

$total_ird = 0;
$total_irna = 0;

?>          
<style type="text/css">
  .inner-numbering{
    padding-left: 15px;
  }

  .uang{
    text-align: right;
  }

  
  .jumlah{
    width:30px;
  }
</style>
<?php

$selisih_hari = 1;

        if(!empty($rawatInap->tanggal_keluar))
        {
            $dtm = !empty($rawatInap->datetime_keluar) ? $rawatInap->datetime_keluar : date('Y-m-d H:i:s');
          $tgl_keluar = date('Y-m-d',strtotime($dtm));
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk),$tgl_keluar);
        }else
        {
          $dnow = date('Y-m-d');
            $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
        }

          $nama_dokter = '';
    $nama_dokter_ird = '';


    if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->FULLNAME;
     } 


    if(!empty($rawatRincian->dokterIrd))
    {
       $nama_dokter_ird = $rawatRincian->dokterIrd->FULLNAME;
    }
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
 

 <table class="table table-condensed" id="table_ri_rj">
   <thead>
   <tr>
     <th>E.</th>
     <th >LAIN-LAIN</th>
     <th colspan="2"  align="center">RAWAT DARURAT</th>
     <!-- <th>&nbsp;</th> -->
     <th colspan="2" align="center">RAWAT INAP</th>
     <!-- <th>&nbsp;</th> -->
     <th width="30%">TARIF</th>
   </tr>
   </thead>
  
  
   <tbody id="tbody-lainnya">
   <?php 
   if(!is_null($rawatInap->getError('ERROR_LAIN')))
   {
    ?>
    <tr>
    <td colspan="7">
   <div class="alert alert-error">
   <?php 
      echo $form->error($rawatInap,'ERROR_LAIN'); 
?>
</div>
</td>
</tr>
<?php 
}

   $listLain = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>$rawatInap->kamar->kelas_id));
   $i = 0;
   foreach($listLain as $lain){
       $attr = array(
        'id_rawat_inap' => $rawatInap->id_rawat_inap,
        'id_lain' => $lain->id_tindakan
      );

      $itemLain = TrRawatInapLain::model()->findByAttributes($attr);
      $jumlah_tindakan = Yii::app()->helper->contains($lain->nama_tindakan,'Px') ? $selisih_hari : 1;
      $biaya_irna = $lain->tarip * $jumlah_tindakan;
      $biaya_ird = 0;

      $biaya_irna_lain_total = 0;
      if(!empty($itemLain)){
        $jumlah_tindakan = Yii::app()->helper->contains($lain->nama_tindakan,'Px') ? $selisih_hari : $itemLain->jumlah_tindakan;
        
        $biaya_irna = $itemLain->biaya_irna;
        $biaya_ird = $itemLain->biaya_ird;
        $biaya_irna_lain_total = $biaya_irna_lain_total + $itemLain->biaya_irna;// * $jumlah_tindakan;
      }

      $total_ird = $total_ird + $biaya_ird;
      $total_irna = $total_irna + $biaya_irna_lain_total;
    ?>
    <tr>
     <td>&nbsp;</td>
     <td> <span class="number-rm"><?php echo $i+1;?>. </span><?php echo $lain->nama_tindakan;?></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input type="text" name="biaya_ird_lain[]" value="<?php echo Yii::app()->helper->formatRupiah($biaya_ird);?>" class="input-medium uang" placeholder="-" /></td>
     <td></td>
     <td>&nbsp;</td>
     <td>Rp&nbsp;<input type="text" name="biaya_irna_lain[]" value="<?php echo Yii::app()->helper->formatRupiah($biaya_irna);?>" class="input-medium uang" placeholder="-"/>
       <input type="text" id="jml_<?php echo $lain->id_tindakan;?>" name="jumlah_rm[]" class="input-small jumlah jumlah_tindakan_lain" value="<?php echo $jumlah_tindakan;?>" placeholder="Jml Item"/>
  <input type="hidden" name="id_tindakan_lain[]" value="<?php echo $lain->id_tindakan;?>"/>
     <script type="text/javascript">
       


$('#jml_<?php echo $lain->id_tindakan;?>').on('keyup',function(e){

    var code = e.which; // recommended to use e.which, it's normalized across browsers
    if(code==13)
      e.preventDefault();
    if(code==32||code==13||code==188||code==186){
      var input = $(this).val();

      if(jQuery.isNumeric(input))
      {
          var item = $(this).prev().val();
          item = item.replace(".", "");
        
          var value = eval(<?php echo $lain->tarip;?> * input);
          
          $(this).prev().val(value);
          var value = $(this).prev().val();
          value = value.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          $(this).prev().val(value);
      }    
    } // missing closing if brace
    
        
});

     </script>
     </td>
   </tr>
   <?php 
   $i++;
      }
   ?>
     
   </tbody>
     
   
    </table>

    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton('SIMPAN',array('class'=>'btn btn-success','id'=>'btn-submit')); ?>
      <?php echo $this->renderPartial('_buttonCetak',array('rawatInap'=>$rawatInap));?>
    </div>
  </fieldset>
  <?php 
  $this->endWidget();
  ?>


<script>




$('#nama_dokter_perawat').focus();

$('#btn-submit').keydown(function(){
  var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {

      $('form').submit();
    }
});

