<?php
/* @var $this DtdController */
/* @var $model Dtd */

$this->breadcrumbs=array(
	array('name'=>'Dtd','url'=>array('admin')),
	array('name'=>'Dtd'),
);

$this->menu=array(
	array('label'=>'List Dtd', 'url'=>array('index')),
	array('label'=>'Create Dtd', 'url'=>array('create')),
	array('label'=>'Update Dtd', 'url'=>array('update', 'id'=>$model->kode)),
	array('label'=>'Delete Dtd', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kode),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dtd', 'url'=>array('admin')),
);
?>

<h1>View Dtd #<?php echo $model->kode; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'kode',
		'deskripsi',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
