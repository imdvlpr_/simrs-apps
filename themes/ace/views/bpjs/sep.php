<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'SEP','url'=>Yii::app()->createUrl('bpjs/sep')),
     array('name' => 'List')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();
?>

  <h1>Pembuatan SEP</h1>


<style>
  .errorMessage, .errorSummary{
    color:red;
  }

  #BpjsSep_TGL_SEP,#BpjsSep_TGL_RUJUKAN{
    background : url(<?php echo Yii::app()->baseUrl;?>/images/calendar-icon.png) no-repeat scroll 7px 7px;
padding-left:30px;

</style>
<div class="row">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>

<div class="col-xs-6">
   <?php 
echo $form->errorSummary($bpjsPasien,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa Data Pasien eror berikut:','</div>');

?>          
<div style="display:none" class="alert alert-error" id="bpjs_error"></div>
<div style="display:none" class="alert alert-info" id="bpjs_info"></div>
  <div class="widget-box widget-color-blue2">
     <div class="widget-header">
        <h4 class="widget-title lighter smaller">Data BPJS</h4>
      </div>
       <div class="widget-body">
         <div class="widget-main">
            <div class="form-group">
        <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php 
              echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-large'));

              
            ?>
             <a id='carinomor' class="btn btn-sm btn-info" href="javascript:void(0);"><i class="fa fa-search"></i> Cari Nomor</a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
            <?php echo $form->error($bpjsPasien,'NoKartu'); ?>
          </div>
        </div>
         <div class="form-group">
          <?php echo $form->labelEx($bpjsPasien,'NIK',array('class'=>'col-sm-3 control-label no-padding-right'));?>
            <div class="col-sm-9">
              <?php echo $form->textField($bpjsPasien,'NIK',array('class'=>'input-large','readonly'=>true)); 
                  echo $form->error($bpjsPasien,'NIK');
              ?>
                
            </div>
          </div>
               <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'NAMAPESERTA',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($bpjsPasien,'NAMAPESERTA',array('class'=>'input-large','readonly'=>true));

                      echo $form->error($bpjsPasien,'NAMAPESERTA');
                     ?>
                  </div>
                </div>
               <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'KELAMIN',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  
                  <?php 
                  echo $form->textField($bpjsPasien,'KELAMIN',array('class'=>'input-large','readonly'=>true));

                    echo $form->error($bpjsPasien,'KELAMIN');
                  ?>
                  </div>
                </div>
                 <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'PISAT',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    
                    <?php 

                      echo CHtml::textField('PISAT_NAMA',!empty($bpjsPasien) ? $bpjsPasien->NAMA_PISAT : '',array('class'=>'input-large','readonly'=>true));
                      echo $form->hiddenField($bpjsPasien,'PISAT');
                      echo $form->error($bpjsPasien,'PISAT');
                     ?>
                  </div>
                </div>
               
                <div class="form-group">
                  <?php echo $form->labelEx($bpjsPasien,'TGLLAHIR',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <div id="datePickerComponent" class="input-append date margin-00" data-date="<?php echo date('d-m-Y')?>" data-date-format="dd-mm-yyyy">
                  <?php  echo $form->textField($bpjsPasien,'TGLLAHIR',array('class'=>'input-large','readonly'=>true));

                  echo $form->error($bpjsPasien,'TGLLAHIR',array('class'=>'col-sm-3 control-label no-padding-right'));
                  ?>
                    
                    </div>
                  </div>
                </div>
                 <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'PPKTK1',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                      echo $form->textField($bpjsPasien,'PPKTK1',array('class'=>'input-large','readonly'=>true));
                      echo $form->error($bpjsPasien,'PPKTK1');
                     ?>
                  </div>
                </div>
                <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'PESERTA',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 

                      echo $form->textField($bpjsPasien,'NAMA_KEPESERTAAN',array('class'=>'input-large','readonly'=>true));
                      
                      echo $form->hiddenField($bpjsPasien,'PESERTA');
                      echo $form->error($bpjsPasien,'PESERTA');
                     ?>
                  </div>
                </div>
                 <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'KELASRAWAT',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                      echo CHtml::textField('KELASRAWAT_NAMA',!empty($bpjsPasien) ? $bpjsPasien->NAMA_KELASRAWAT : '',array('class'=>'input-large','readonly'=>true));
                      echo $form->hiddenField($bpjsPasien,'KELASRAWAT');
                      echo $form->error($bpjsPasien,'KELASRAWAT');
                     ?>
                  </div>
                </div>
                <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'TGLCETAKKARTU',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                      echo $form->textField($bpjsPasien,'TGLCETAKKARTU',array('class'=>'input-large','readonly'=>true));
                      


                      echo $form->error($bpjsPasien,'TGLCETAKKARTU');
                     ?>
                  </div>
                </div>
                <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'TMT',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                    echo $form->textField($bpjsPasien,'TMT',array('class'=>'input-large','readonly'=>true));
                    


                      echo $form->error($bpjsPasien,'TMT');
                     ?>
                  </div>
                </div>
                <div class="form-group">
                <?php echo $form->labelEx($bpjsPasien,'TAT',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                    echo $form->textField($bpjsPasien,'TAT',array('class'=>'input-large','readonly'=>true));
              

                      echo $form->error($bpjsPasien,'TAT');
                     ?>
                  </div>
                </div>
         </div>
       </div>
  </div>



     
           
    <!-- /block -->
      <div class="widget-box widget-color-blue2">
     <div class="widget-header">
        <h4 class="widget-title lighter smaller"><img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs-icon.png"/> Riwayat Pelayanan</h4>
      </div>
       <div class="widget-body">
         <div class="widget-main">
            <span id='loading2' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
          <table id="tabel_riwayat" class="table  table-bordered table-hover">
            <thead>
              <tr>
                <th>Tgl SJP</th>
                <th>No SJP</th>
                <th>Jns Pel SJP</th>
                <th>Pol Tuj SJP</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
           
            </tbody>
            
          </table>
         </div>
       </div>
     </div>
       
    </div>

    <!--
=====================================================================================================================
==============================================form kedua========================================================
=====================================================================================================================
=====================================================================================================================

    -->
      <div class="col-xs-6">

       <?php 
echo $form->errorSummary($bpjsSep,'<div class="alert alert-error">Silakan perbaiki beberapa eror SEP berikut:','</div>');

?> 
<div class="widget-box widget-color-blue2">
     <div class="widget-header">
        <h4 class="widget-title lighter smaller">Data BPJS</h4>
      </div>
       <div class="widget-body">
         <div class="widget-main">
            <div class="form-group">
                <?php 
                echo $form->labelEx($bpjsSep,'JENIS_RAWAT',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';
                echo     $form->dropDownList($bpjsSep,'JENIS_RAWAT',CHtml::ListData(BpjsJenisRawat::model()->findAll(),'KODE_JENIS_RAWAT','NAMA_JENIS_RAWAT'),array('class'=>'col-sm-3 control-label no-padding-right'));
               


                    echo '</div>';?>
            </div>
            <div class="form-group">
        
              <div class="form-group">
                <?php 
                echo CHtml::label('Kls Perawatan','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';


                    $htmlOptions = array(
                      'class'=>'input-large',


                    );

                    $kelasoptions = array(
                    '1' => 'Kelas I',
                    '2' => 'Kelas II',
                    '3' => 'Kelas III'
                  );

                    switch ($bpjsData['kelas']['id']) {
                      case 2:
                        unset($kelasoptions[1]);
                        break;
                      case 3 :
                        unset($kelasoptions[1]);
                        unset($kelasoptions[2]);

                    }
                    echo $form->dropDownList($bpjsSep, 'KELAS_RAWAT', $kelasoptions,$htmlOptions);
                    

                    echo '</div>';?>
                </div>
               
           
               <div class="form-group">
                <?php echo $form->labelEx($model,'NoMedrec',array('class'=>'col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php 
                    echo $form->textField($model,'NoMedrec',array('class'=>'input-large','readonly'=>true));
                    echo $form->error($model,'NoMedrec');
                  ?>
                  </div>
                </div>
                <div class="form-group">
                 <?php 
                echo CHtml::label('No Rujukan ','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';


                   echo $form->textField($bpjsSep,'NO_RUJUKAN',array('class'=>'input-large'));
                    echo $form->error($bpjsSep,'NO_RUJUKAN');
                    ?>

                
                   <?php 
                    echo '</div>';?>
                </div>
                <div class="form-group">
                   <?php 
                echo CHtml::label('Asal Rujukan ','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';
                   $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'id' => 'NamaPPK',
                      'name' => 'NamaPPK',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getRujukanPPK').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiNoRujukan(ui);
                          }',
                  ),
                  ));


                      // echo '&nbsp;'.CHtml::textField('ASAL_RUJUKAN','',array('class'=>'input-small','readonly'=>'readonly'));
                      // echo $form->hiddenField($bpjsSep,'ASAL_RUJUKAN');
                      // echo $form->error($bpjsSep,'ASAL_RUJUKAN');

                   ?>

                   <?php 
                    echo '</div>';?>

                </div>
               
                
                 
                <div class="form-group">
                 <?php 
                echo CHtml::label('Tgl Rujukan ','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';


                  $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$bpjsSep,
                    'attribute'=>'TGL_RUJUKAN',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                       
                    ),
        ));
                    
                   echo $form->error($bpjsSep,'TGL_RUJUKAN');
                    echo '</div>';?>
                </div>
               
                
                 <div class="form-group">
                 <?php 
                echo CHtml::label('Tgl SEP ','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';


                      $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$bpjsSep,
                    'attribute'=>'TGL_SEP',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        
                    ),
        ));
                    
                       echo $form->error($bpjsSep,'TGL_SEP');
                    echo '</div>';?>
                </div>
                <div class="form-group">
                 <?php 
                echo CHtml::label('Diagnosa Awal ','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';


                   $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'id' => 'NamaDIAG_AWAL',
                      'name' => 'NamaDIAG_AWAL',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getDiagAwal').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiDiagnosa(ui);
                          }',
                  ),
                  ));
                    echo $form->error($bpjsSep,'DIAG_AWAL');
                    echo $form->hiddenField($bpjsSep,'DIAG_AWAL');
                     

                    echo '</div>';?>
                </div>
               
                 <div class="form-group">
                 <?php 
                echo CHtml::label('Poli Tujuan ','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';


                      $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'id' => 'NamaPOLI',
                      'name' => 'NamaPOLI',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getPoli').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiPoli(ui);
                          }',
                  ),
                  ));
                       echo $form->error($bpjsSep,'POLI_TUJUAN');
                       echo $form->hiddenField($bpjsSep,'POLI_TUJUAN');

                    echo '</div>';?>
                </div>
                <div class="form-group">
                 <?php 
                echo CHtml::label('Catatan ','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';


                   echo $form->textField($bpjsSep,'CATATAN',array('class'=>'input-large'));
                    

                    echo '</div>';?>
                </div>
                 <div class="form-group">
                <?php 
                echo CHtml::label('<strong><i>Kasus</strong> ','',array('class'=>'col-sm-3 control-label no-padding-right'));
                echo   '<div class="col-sm-9">';


                   ?>
                    <label class="uniform">
          <div id="uniform-optionsCheckbox" class="checker">
          <span class="">

          <?php echo $form->checkBox($bpjsSep,'LAKA_LANTAS', array('value'=>1, 'uncheckValue'=>2,'class'=>'uniform_on')); ?>
          </span>
          </div>
          <strong><i>Pasien Kecelakaan lalu lintas</i></strong>
          </label>
                   <?php 
                    

                    echo '</div>';?>
                </div>
                   <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Cetak SEP
          </button>

        
        </div>
      </div>
         </div>
       </div>
     </div>
        
   
        
            </div>
        </div>
    </div>
    <!-- /block -->

             
   <?php $this->endWidget();?>
</div>

<script>

<?php 

  if(!empty($_POST['NamaPPK']) && !empty($_POST['ASAL_RUJUKAN'])){
?>
$('#NamaPPK').val('<?php echo $_POST['NamaPPK'];?>');
$('#ASAL_RUJUKAN').val('<?php echo $_POST['ASAL_RUJUKAN'];?>');

<?php 
}

  if(!empty($_POST['NamaDIAG_AWAL'])){
?>
$('#NamaDIAG_AWAL').val('<?php echo $_POST['NamaDIAG_AWAL'];?>');
<?php 
}

if(!empty($_POST['NamaPOLI'])){
?>
$('#NamaPOLI').val('<?php echo $_POST['NamaPOLI'];?>');
<?php 
}


if(!empty($bpjsPasien->NoKartu)){
?>

$('#BpjsPasien_NoKartu').attr('readonly','readonly');
$('#BpjsSep_KELAS_RAWAT').focus();
$('#BpjsSep_JENIS_RAWAT').attr('readonly','readonly').attr('disabled','disabled');
// riwayatTerakhir();
<?php
}
else{
?>
$('#BpjsPasien_NoKartu').focus(); 
<?php
}
?>



$(document).ready(function(){
  $('#carinomor').click(function(){
    carinomor();
    // riwayatTerakhir();
  });


  $('#tabel_riwayat').on("click",'.hapus_sep',function(){

      var r = confirm("Hapus data SEP ini?");
      if (r) {
          var sep = $(this).parent().find('input');
          var tuple = $(this).parent().parent();
          $.ajax({
            type : 'POST',
            url  : '<?php echo Yii::app()->createUrl("WSBpjs/hapusSEP");?>',
            data : 'nokartu='+sep.val(),
            beforeSend : function(){
              $('#loading2').show();
            },

            error: function(jqXHR, textStatus){
                if(textStatus === 'timeout')  {     
                     alert('Failed from timeout');     
                       $('#loading2').hide();    
                    //do something. Try again perhaps?
                }
            },
            timeout : 3000,
            async: true,
            success : function(data){

                $('#loading2').hide();
                
                if(data.includes("200")) {
                  $('#bpjs_error').hide();
                  tuple.remove();
                  alert("Data sep telah dihapus");
                }

                else {
                    $('#bpjs_error').show();
                   $('#bpjs_error').html(output.metadata.message);
                }
            },
        });
      } 

      
  });

});

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
}

function riwayatTerakhir() {
  $.ajax({
      type : 'POST',
      url  : '<?php echo Yii::app()->createUrl("WSBpjs/getRiwayatSEP");?>',
      data : 'nokartu='+$('#BpjsPasien_NoKartu').val(),
      beforeSend : function(){
        $('#loading1').show();
      },

      error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
      },
      timeout : 3000,
      async: true,
      success : function(data){
          $('#tabel_riwayat > tbody').empty();
          $('#loading1').hide();
          
          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200) {
              $('#bpjs_info').hide();
              
              $.each(output.response.list, function(i, item) {
                  
                  var row = '<tr>';

                  row += '<td>'+convertDate(item.tglSEP)+'</td>';
                  row += '<td>'+item.noSEP+'</td>';
                  row += '<td>'+item.jnsPelayanan+'</td>';
                  row += '<td>'+item.poliTujuan.nmPoli+'</td>';
                  row += '<td><input type="hidden" value="'+item.noSEP+'"><a href="javascript:void(0)" class="btn btn-danger btn-mini hapus_sep">Hapus SEP</a></td>';
                  row += '</tr>';
                  $('#tabel_riwayat > tbody').append(row);
                 // console.log(item.noSEP);
              });
             
          }

          else {
             $('#bpjs_info').show();
              $('#bpjs_info').html("<strong>Info !</strong> " + output.metadata.message);

          }
      },
  });
}

function carinomor() {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];


    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NoKartu').val()+'&jenis=kartu',
        beforeSend : function(){
          $('#loading1').show();

        },
        error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
        },
        timeout : 10000,
        async: true,
        success : function(data){


          $('#loading1').hide();
          
          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metaData.code == 200){
            var peserta = output.response.peserta;
            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#Pasien_NOIDENTITAS').val(peserta.nik);

            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);
            $('#Pasien_NAMA').val(peserta.nama);

            if(peserta.sex == 'L'){
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
              //$('#Pasien_JENSKEL_0').prop("checked",true);
            }
            else{
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);
              // $('#Pasien_JENSKEL_1').prop("checked",true);
            }
              
            $('#Pasien_JENSKEL').val(peserta.sex);
            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#PISAT_NAMA').val(pisat[peserta.pisa]);
            
            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
             $('#Pasien_TGLLAHIR').val(peserta.tglLahir);

            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.hakKelas.kode);
             $('#BpjsPasien_TGLCETAKKARTU').val(peserta.tglCetakKartu);
             $('#BpjsPasien_PPKTK1').val(peserta.provUmum.kdProvider+' - '+peserta.provUmum.nmProvider);
            $('#BpjsPasien_TMT').val(peserta.tglTMT);
            $('#BpjsPasien_TAT').val(peserta.tglTAT);
            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);

            $('#BpjsPasien_NAMA_KEPESERTAAN').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.hakKelas.keterangan);
            $('#bpjs_error').hide();
          }
          else {
            $('#bpjs_error').show();
              $('#bpjs_error').html(output.metaData.message);

          }
        }
      });
   
  }

jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keypress', 'input,select', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        // Get all focusable elements on the page
        var $canfocus = $(':focusable').not('a').not('input[readonly]');
        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        
        $canfocus.eq(index).focus();
         $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});

  function isiNoRujukan(ui)
  {
     $("#BpjsSep_ASAL_RUJUKAN").val(ui.item.key);
     $("#NamaPPK").val(ui.item.value);
     $("#ASAL_RUJUKAN").val(ui.item.id);
     

  }

  function isiDiagnosa(ui)
  {
     $("#BpjsSep_DIAG_AWAL").val(ui.item.id);
     $("#NamaDIAG_AWAL").val(ui.item.value);
     

  }

  function isiPoli(ui)
  {
     $("#BpjsSep_POLI_TUJUAN").val(ui.item.id);
     $("#NamaPOLI").val(ui.item.value);
     

  }



 </script>