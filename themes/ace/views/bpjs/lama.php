<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();
?>

<div class="page-header">
  <h1>Pembuatan SEP</h1>
</div>

<style>
  .errorMessage, .errorSummary{
    color:red;
  }

  #BpjsSep_TGL_SEP,#BpjsSep_TGL_RUJUKAN{
    background : url(<?php echo Yii::app()->baseUrl;?>/images/calendar-icon.png) no-repeat scroll 7px 7px;
padding-left:30px;

</style>
<div class="row-fluid">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>

<div class="span6">
 <?php 
echo $form->errorSummary($bpjsPasien,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs-icon.png"/> Data dari BPJS</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                
                
            <fieldset>
            

      <div class="control-group">
        <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'control-label'));?>
          <div class="controls">
            <?php 
              echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-large'));

              
            ?>
             <a id='carinomor' href="javascript:void(0);"><i class="icon-search"></i> Cari Nomor</a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
            <?php echo $form->error($bpjsPasien,'NoKartu'); ?>
          </div>
        </div>
         <div class="control-group">
          <?php echo $form->labelEx($bpjsPasien,'NIK',array('class'=>'control-label'));?>
            <div class="controls">
              <?php echo $form->textField($bpjsPasien,'NIK',array('class'=>'input-large','readonly'=>true)); 
                  echo $form->error($bpjsPasien,'NIK');
              ?>
                
            </div>
          </div>
               <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'NAMAPESERTA',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($bpjsPasien,'NAMAPESERTA',array('class'=>'input-large','readonly'=>true));

                      echo $form->error($bpjsPasien,'NAMAPESERTA');
                     ?>
                  </div>
                </div>
               <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'KELAMIN',array('class'=>'control-label'));?>
                  <div class="controls">
                  
                  <?php 
                  echo $form->textField($bpjsPasien,'KELAMIN',array('class'=>'input-large','readonly'=>true));

                    echo $form->error($bpjsPasien,'KELAMIN');
                  ?>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PISAT',array('class'=>'control-label'));?>
                  <div class="controls">
                    
                    <?php 

                      
                      echo $form->textField($bpjsPasien,'PISAT',array('class'=>'input-large','readonly'=>true));
                      echo $form->error($bpjsPasien,'PISAT');
                     ?>
                  </div>
                </div>
               
                <div class="control-group">
                  <?php echo $form->labelEx($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));?>
                  <div class="controls">
                    <div id="datePickerComponent" class="input-append date margin-00" data-date="<?php echo date('d-m-Y')?>" data-date-format="dd-mm-yyyy">
                  <?php  echo $form->textField($bpjsPasien,'TGLLAHIR',array('class'=>'input-large','readonly'=>true));

                  echo $form->error($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));
                  ?>
                    
                    </div>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PPKTK1',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      echo $form->textField($bpjsPasien,'PPKTK1',array('class'=>'input-large','readonly'=>true));
                      echo $form->error($bpjsPasien,'PPKTK1');
                     ?>
                  </div>
                </div>
                <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PESERTA',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 

                      echo $form->textField($bpjsPasien,'NAMA_KEPESERTAAN',array('class'=>'input-large','readonly'=>true));
                      
                      echo $form->hiddenField($bpjsPasien,'PESERTA');
                      echo $form->error($bpjsPasien,'PESERTA');
                     ?>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'KELASRAWAT',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      
                      echo $form->textField($bpjsPasien,'KELASRAWAT',array('class'=>'input-large','readonly'=>true));
                      echo $form->error($bpjsPasien,'KELASRAWAT');
                     ?>
                  </div>
                </div>
                <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'TGLCETAKKARTU',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                    	echo $form->textField($bpjsPasien,'TGLCETAKKARTU',array('class'=>'input-large','readonly'=>true));
                      


                      echo $form->error($bpjsPasien,'TGLCETAKKARTU');
                     ?>
                  </div>
                </div>
                <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'TMT',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                    echo $form->textField($bpjsPasien,'TMT',array('class'=>'input-large','readonly'=>true));
                    


                      echo $form->error($bpjsPasien,'TMT');
                     ?>
                  </div>
                </div>
                <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'TAT',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                    echo $form->textField($bpjsPasien,'TAT',array('class'=>'input-large','readonly'=>true));
              

                      echo $form->error($bpjsPasien,'TAT');
                     ?>
                  </div>
                </div>
            </fieldset>
        
            </div>
        </div>
    </div>
    <!-- /block -->
     <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs-icon.png"/> Riwayat Pelayanan</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                
                
            <fieldset>
            

     			<table class="table table-condensed">
     				<thead>
     					<tr>
     						<th>Tgl SJP</th>
     						<th>No SJP</th>
     						<th>Jns Pel SJP</th>
     						<th>Pol Tuj SJP</th>
     					</tr>
     				</thead>
     				<tbody>
            <?php 
              if(!empty($riwayatTerakhir)){
              if($riwayatTerakhir->metadata->code == '200')
              {
                
                $list = $riwayatTerakhir->response->list;

                foreach($list as $q => $v):
            ?>
     					<tr>
     						<td><?php 

                echo $v->tglSEP;?></td>
     						<td><?php echo $v->noSEP;?></td>
     						<td><?php echo $v->jnsPelayanan;?></td>
     						<td><?php echo $v->poliTujuan->nmPoli;?></td>
     					</tr>
            <?php 
                endforeach;
              }
            }
            ?>
     				</tbody>
     				
     			</table>
            </fieldset>
        
            </div>
        </div>
    </div>
    </div>

    <!--
=====================================================================================================================
==============================================form kedua========================================================
=====================================================================================================================
=====================================================================================================================

    -->
      <div class="span6">
       <?php 
echo $form->errorSummary($bpjsSep,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?> 
          <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Surat Eligibilitas Peserta</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                
                
            <fieldset>
            <div class="control-group">
                <?php 
                echo $form->labelEx($bpjsSep,'JENIS_RAWAT',array('class'=>'control-label'));
                echo   '<div class="controls">';
                echo     $form->dropDownList($bpjsSep,'JENIS_RAWAT',CHtml::ListData(BpjsJenisRawat::model()->findAll(),'KODE_JENIS_RAWAT','NAMA_JENIS_RAWAT'),array('class'=>'control-label'));
               


                    echo '</div>';?>
            </div>
            <div class="control-group">
        
              <div class="control-group">
                <?php 
                echo CHtml::label('Kls Perawatan','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                    $htmlOptions = array(
                    	'class'=>'input-large',


                    );

                    $kelasoptions = array(
                		'1' => 'Kelas I',
                		'2' => 'Kelas II',
                		'3' => 'Kelas III'
                	);

                    switch ($bpjsData['kelas']['id']) {
                      case 2:
                        unset($kelasoptions[1]);
                        break;
                      case 3 :
                        unset($kelasoptions[1]);
                        unset($kelasoptions[2]);

                    }
                    echo $form->dropDownList($bpjsSep, 'KELAS_RAWAT', $kelasoptions,$htmlOptions);
                    

                    echo '</div>';?>
                </div>
               
           
               <div class="control-group">
                <?php echo $form->labelEx($model,'NoMedrec',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php 
                  	echo $form->textField($model,'NoMedrec',array('class'=>'input-large','readonly'=>true));
                    echo $form->error($model,'NoMedrec');
                  ?>
                  </div>
                </div>
                <div class="control-group">
                 <?php 
                echo CHtml::label('No Rujukan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                   echo $form->textField($bpjsSep,'NO_RUJUKAN',array('class'=>'input-large'));
                    echo $form->error($bpjsSep,'NO_RUJUKAN');
                    ?>

                
                   <?php 
                    echo '</div>';?>
                </div>
                <div class="control-group">
                   <?php 
                echo CHtml::label('Asal Rujukan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';
                   $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'id' => 'NamaPPK',
                      'name' => 'NamaPPK',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getRujukanPPK').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiNoRujukan(ui);
                          }',
                  ),
                  ));
                      echo '&nbsp;'.$form->textField($bpjsSep,'ASAL_RUJUKAN',array('class'=>'input-small','readonly'=>'readonly'));
                      echo $form->error($bpjsSep,'ASAL_RUJUKAN');

                   ?>

                   <?php 
                    echo '</div>';?>

                </div>
               
                
                 
                <div class="control-group">
                 <?php 
                echo CHtml::label('Tgl Rujukan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                  $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$bpjsSep,
                    'attribute'=>'TGL_RUJUKAN',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                       
                    ),
        ));
                    
                   echo $form->error($bpjsSep,'TGL_RUJUKAN');
                    echo '</div>';?>
                </div>
               
                
                 <div class="control-group">
                 <?php 
                echo CHtml::label('Tgl SEP ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                      $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$bpjsSep,
                    'attribute'=>'TGL_SEP',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        
                    ),
        ));
                    
                       echo $form->error($bpjsSep,'TGL_SEP');
                    echo '</div>';?>
                </div>
                <div class="control-group">
                 <?php 
                echo CHtml::label('Diagnosa Awal ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                   $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'id' => 'NamaDIAG_AWAL',
                      'name' => 'NamaDIAG_AWAL',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getDiagAwal').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiDiagnosa(ui);
                          }',
                  ),
                  ));
                    echo $form->error($bpjsSep,'DIAG_AWAL');
                    echo $form->hiddenField($bpjsSep,'DIAG_AWAL');
                     

                    echo '</div>';?>
                </div>
               
                 <div class="control-group">
                 <?php 
                echo CHtml::label('Poli Tujuan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                      $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'id' => 'NamaPOLI',
                      'name' => 'NamaPOLI',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getPoli').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiPoli(ui);
                          }',
                  ),
                  ));
                       echo $form->error($bpjsSep,'POLI_TUJUAN');
                       echo $form->hiddenField($bpjsSep,'POLI_TUJUAN');

                    echo '</div>';?>
                </div>
                <div class="control-group">
                 <?php 
                echo CHtml::label('Catatan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                   echo $form->textField($bpjsSep,'CATATAN',array('class'=>'input-large'));
                    

                    echo '</div>';?>
                </div>
                 <div class="control-group">
                <?php 
                echo CHtml::label('<strong><i>Kasus</strong> ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                   ?>
                   	<label class="uniform">
					<div id="uniform-optionsCheckbox" class="checker">
					<span class="">

					<?php echo $form->checkBox($bpjsSep,'LAKA_LANTAS', array('value'=>1, 'uncheckValue'=>2,'class'=>'uniform_on')); ?>
					</span>
					</div>
					<strong><i>Pasien Kecelakaan lalu lintas</i></strong>
					</label>
                   <?php 
                    

                    echo '</div>';?>
                </div>
                 <div class="form-actions" align="center">

                  <button type="submit" class="btn btn-primary" name="cetak_sep">Cetak SEP</button>
                  
                  <button type="button" class="btn">Batal</button>
                </div>
            </fieldset>
        
            </div>
        </div>
    </div>
    <!-- /block -->
    </div>
    </div>
    <!-- /block -->
              </div>
             
   <?php $this->endWidget();?>
</div>

<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
?>
<script>


$(document).ready(function(){
  $('#carinomor').click(function(){
    carinomor();
  });

});

function carinomor()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];


    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NoKartu').val()+'&jenis=kartu',
        beforeSend : function(){
          $('#loading1').show();

        },
        error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
        },
        timeout : 3000,
        async: false,
        success : function(data){


          $('#loading1').hide();
          
          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200){
            var peserta = output.response.peserta;
            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#Pasien_NOIDENTITAS').val(peserta.nik);

            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);
            $('#Pasien_NAMA').val(peserta.nama);

            

            

            if(peserta.sex == 'L'){
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
              //$('#Pasien_JENSKEL_0').prop("checked",true);
            }
            else{
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);
              // $('#Pasien_JENSKEL_1').prop("checked",true);
            }
              
            $('#Pasien_JENSKEL').val(peserta.sex);
            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);
            
            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
             $('#Pasien_TGLLAHIR').val(peserta.tglLahir);

            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);
             $('#BpjsPasien_TGLCETAKKARTU').val(peserta.tglCetakKartu);
             $('#BpjsPasien_PPKTK1').val(peserta.provUmum.kdProvider+' - '+peserta.provUmum.nmProvider);
            $('#BpjsPasien_TMT').val(peserta.tglTMT);
            $('#BpjsPasien_TAT').val(peserta.tglTAT);
            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
        }
      });
   
  }

jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keypress', 'input,select', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        // Get all focusable elements on the page
        var $canfocus = $(':focusable');
        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
    }
});

  function isiNoRujukan(ui)
  {
     $("#BpjsSep_ASAL_RUJUKAN").val(ui.item.id);
     $("#NamaPPK").val(ui.item.value);
     

  }

  function isiDiagnosa(ui)
  {
     $("#BpjsSep_DIAG_AWAL").val(ui.item.id);
     $("#NamaDIAG_AWAL").val(ui.item.value);
     

  }

  function isiPoli(ui)
  {
     $("#BpjsSep_POLI_TUJUAN").val(ui.item.id);
     $("#NamaPOLI").val(ui.item.value);
     

  }


  jQuery(document).ready(function() {   
     FormValidation.init();

    
  });
  

        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();

            
        });



 </script>