
  

<style>
  .errorMessage, .errorSummary{
    color:red;
  }

    .ui-datepick/*er { 
  margin-top:  50px;
  z-index: 1000;
}*/
  #BpjsSep_TGL_SEP,#BpjsSep_TGL_RUJUKAN{
    background : url(<?php echo Yii::app()->baseUrl;?>/images/calendar-icon.png) no-repeat scroll 7px 7px;
padding-left:30px;

</style>
<div class="row-fluid">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
      'target' => '_blank'
    ),  
)); ?>

      <div class="span12">

<div style="display:none" class="alert alert-error" id="bpjs_error"></div>
       <?php 
echo $form->errorSummary($bpjsSep,'<div class="alert alert-error">Silakan perbaiki beberapa eror SEP berikut:','</div>');

?> 
          <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Surat Eligibilitas Peserta</div>
        </div>
        <div class="block-content collapse in">
            <div class="span6">
                
                
            <fieldset>
            <div class="control-group">
                <?php 
                echo $form->labelEx($bpjsSep,'JENIS_RAWAT',array('class'=>'control-label'));
                echo   '<div class="controls">';
                echo     $form->dropDownList($bpjsSep,'JENIS_RAWAT',CHtml::ListData(BpjsJenisRawat::model()->findAll(),'KODE_JENIS_RAWAT','NAMA_JENIS_RAWAT'),array('class'=>'control-label'));
               


                    echo '</div>';?>
            </div>
            <div class="control-group">
        
              <div class="control-group">
                <?php 
                echo CHtml::label('Kls Perawatan','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                    $htmlOptions = array(
                      'class'=>'input-large',


                    );

                    $kelasoptions = array(
                    '1' => 'Kelas I',
                    '2' => 'Kelas II',
                    '3' => 'Kelas III'
                  );

                    switch ($bpjsData['kelas']['id']) {
                      case 2:
                        unset($kelasoptions[1]);
                        break;
                      case 3 :
                        unset($kelasoptions[1]);
                        unset($kelasoptions[2]);

                    }
                    echo $form->dropDownList($bpjsSep, 'KELAS_RAWAT', $kelasoptions,$htmlOptions);
                    

                    echo '</div>';?>
                </div>
               
           
               <div class="control-group">
                <?php echo $form->labelEx($model,'NoMedrec',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php 
                    echo $form->textField($model,'NoMedrec',array('class'=>'input-large','readonly'=>true));
                    echo $form->error($model,'NoMedrec');
                  ?>
                  </div>
                </div>
                <div class="control-group">
                 <?php 
                echo CHtml::label('No Rujukan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';

                  echo CHtml::dropDownList('jenis_rujukan','',array('PCare','RS'),array('class'=>'input-small','id'=>'jenis_rujukan')).'&nbsp;';
                   echo $form->textField($bpjsSep,'NO_RUJUKAN',array('class'=>'input-large'));
                    echo $form->error($bpjsSep,'NO_RUJUKAN');
                    ?>
                    <span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
                <span id="loading1_text"></span>
                   <?php 
                    echo '</div>';?>

                </div>
                <div class="control-group">
                   <?php 
                echo CHtml::label('Asal Rujukan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';
                   $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'model' => $bpjsSep,
                      'attribute' => 'nmProvider',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('WSBpjs/getRujukanPPK').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiNoRujukan(ui);
                          }',
                  ),
                  ));


                      // echo '&nbsp;'.CHtml::textField('kdProvider','',array('class'=>'input-small','readonly'=>'readonly'));
                      echo $form->textField($bpjsSep,'kdProvider',array('class'=>'input-small','readonly'=>'readonly'));
                      echo $form->error($bpjsSep,'kdProvider');

                   ?>

                   <?php 
                    echo '</div>';?>

                </div>
               
                
                
                
                
             
            </fieldset>
        
            </div>
            <div class="span6">
               
                <div class="control-group">
                 <?php 
                echo CHtml::label('Tgl Rujukan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                  $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$bpjsSep,
                    'attribute'=>'TGL_RUJUKAN',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                       // 'numberOfMonths'=>2,
                       //  'showButtonPanel'=>true,
                    ),

        ));
                    
                   echo $form->error($bpjsSep,'TGL_RUJUKAN');
                    echo '</div>';?>
                </div>
               
               <div class="control-group">
                 <?php 
                echo CHtml::label('Tgl SEP ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                      $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$bpjsSep,
                    'attribute'=>'TGL_SEP',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        
                    ),
        ));
                    
                       echo $form->error($bpjsSep,'TGL_SEP');
                    echo '</div>';?>
                </div>
               <div class="control-group">
                 <?php 
                echo CHtml::label('Diagnosa Awal ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                   $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'model' => $bpjsSep,
                      'attribute' => 'namaDiagnosa',
                      // 'name' => 'NamaDIAG_AWAL',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getDiagAwal').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiDiagnosa(ui);
                          }',
                  ),
                  ));
                    echo $form->error($bpjsSep,'DIAG_AWAL');
                    echo $form->hiddenField($bpjsSep,'DIAG_AWAL');
                     

                    echo '</div>';?>
                </div>
               
                 <div class="control-group">
                 <?php 
                echo CHtml::label('Poli Tujuan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                      $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'model' => $bpjsSep,
                      'attribute' => 'nmPoli',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getPoli').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',

                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 
                            isiPoli(ui);
                          }',
                  ),
                  ));
                       echo $form->error($bpjsSep,'POLI_TUJUAN');
                       echo $form->hiddenField($bpjsSep,'POLI_TUJUAN');

                    echo '</div>';?>
                </div>
                <div class="control-group">
                 <?php 
                echo CHtml::label('Catatan ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                   echo $form->textField($bpjsSep,'CATATAN',array('class'=>'input-large'));
                    

                    echo '</div>';?>
                </div>
                 <div class="control-group">
                <?php 
                echo CHtml::label('<strong><i>Kasus</strong> ','',array('class'=>'control-label'));
                echo   '<div class="controls">';


                   ?>
                    <label class="uniform" for="BpjsSep_LAKA_LANTAS">
                      
          <?php echo $form->checkBox($bpjsSep,'LAKA_LANTAS', array('value'=>1, 'uncheckValue'=>2,'class'=>'uniform_on')); ?>
          <strong><i>Pasien Kecelakaan lalu lintas</i></strong>
          </label>
                   <?php 
                    

                    echo '</div>';?>
                </div>
                 <div class="control-group" id="lokasi_laka" style="display: none">
                   <?php 
                echo CHtml::label('Lokasi Laka','',array('class'=>'control-label'));
                ?>
                <div class="controls">
                  <?php  echo $form->textField($bpjsSep,'lokasi_laka',array('class'=>'input-large'));?>
                </div>
              </div>
                 <div align="center">

                  <button type="submit" class="btn btn-primary" name="cetak_sep">Cetak SEP</button>
                  
                  
                </div>
            </div>
        </div>
    </div>
    <!-- /block -->
    </div>

<div class="span12">
 <?php 
echo $form->errorSummary($bpjsPasien,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa Data Pasien eror berikut:','</div>');

?>          

<div style="display:none" class="alert alert-info" id="bpjs_info"></div>

     <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs-icon.png"/> Riwayat Pelayanan</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                
                
            <fieldset>
            
            <span id='loading2' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
          <table id="tabel_riwayat" class="table table-condensed">
            <thead>
              <tr>
                <th>Tgl SJP</th>
                <th>No SJP</th>
                <th>Jns Pel SJP</th>
                <th>Pol Tuj SJP</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
           
            </tbody>
            
          </table>
            </fieldset>
        
            </div>
        </div>
    </div>
    </div>

    <!--
=====================================================================================================================
==============================================form kedua========================================================
=====================================================================================================================
=====================================================================================================================

    -->

    </div>
    <!-- /block -->
   <?php $this->endWidget();?>
</div>

<script>

<?php 

  if(!empty($_POST['NamaPPK']) && !empty($_POST['nmProvider'])){
?>
$('#NamaPPK').val('<?php echo $_POST['NamaPPK'];?>');
$('#nmProvider').val('<?php echo $_POST['nmProvider'];?>');

<?php 
}

  if(!empty($_POST['NamaDIAG_AWAL'])){
?>
$('#NamaDIAG_AWAL').val('<?php echo $_POST['NamaDIAG_AWAL'];?>');
<?php 
}

if(!empty($_POST['NamaPOLI'])){
?>
$('#NamaPOLI').val('<?php echo $_POST['NamaPOLI'];?>');
<?php 
}


if(!empty($bpjsPasien->NoKartu)){
?>

$('#BpjsPasien_NoKartu').attr('readonly','readonly');
$('#BpjsSep_KELAS_RAWAT').focus();
// $('#BpjsSep_JENIS_RAWAT').attr('readonly','readonly').attr('disabled','disabled');

<?php
}
else{
?>
$('#BpjsPasien_NoKartu').focus(); 
<?php
}
?>

$(document).on("keyup","input#BpjsSep_NO_RUJUKAN",function(event){
    
    
  if(event.which == 13){
    carirujukan(2,$(this).val());
  }

  
});

$(document).ready(function(){
  carirujukan(1, '<?=$_GET['kartu'];?>');
  riwayatTerakhir();
 
  $("#BpjsSep_LAKA_LANTAS").change(function() {
    if(this.checked) {
      $('#lokasi_laka').show();
    }

    else{
      $('#lokasi_laka').hide();
    }
  });




  $('#tabel_riwayat').on("click",'.hapus_sep',function(){

      var r = confirm("Hapus data SEP ini?");
      if (r) {
          var sep = $(this).parent().find('input');
          var tuple = $(this).parent().parent();
          $.ajax({
            type : 'POST',
            url  : '<?php echo Yii::app()->createUrl("WSBpjs/hapusSEP");?>',
            data : 'nokartu='+sep.val(),
            beforeSend : function(){
              $('#loading2').show();
            },

            error: function(jqXHR, textStatus){
                if(textStatus === 'timeout')  {     
                     alert('Failed from timeout');     
                       $('#loading2').hide();    
                    //do something. Try again perhaps?
                }
            },
            timeout : 3000,
            async: false,
            success : function(data){

                $('#loading2').hide();
                
                if(data.includes("200")) {
                  $('#bpjs_error').hide();
                  tuple.remove();
                  alert("Data sep telah dihapus");
                }

                else {
                    $('#bpjs_error').show();

                   $('#bpjs_error').html(output.metadata.message);
                   
                }
            },
        });
      } 

      
  });

});

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
}

function riwayatTerakhir() {
  $.ajax({
      type : 'POST',
      url  : '<?php echo Yii::app()->createUrl("WSBpjs/getRiwayatSEP");?>',
      data : 'nokartu=<?=$_GET['kartu'];?>',
      beforeSend : function(){
        $('#loading1').show();

      },

      error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
      },
      timeout : 3000,
      async: false,
      success : function(data){
          $('#tabel_riwayat > tbody').empty();
          $('#loading1').hide();
          
          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200) {
              $('#bpjs_info').hide();
              
              $.each(output.response.list, function(i, item) {
                  
                  var row = '<tr>';

                  row += '<td>'+convertDate(item.tglSEP)+'</td>';
                  row += '<td>'+item.noSEP+'</td>';
                  row += '<td>'+item.jnsPelayanan+'</td>';
                  row += '<td>'+item.poliTujuan.nmPoli+'</td>';
                  row += '<td><input type="hidden" value="'+item.noSEP+'"><a href="javascript:void(0)" class="btn btn-danger btn-mini hapus_sep">Hapus SEP</a></td>';
                  row += '</tr>';
                  $('#tabel_riwayat > tbody').append(row);
                 // console.log(item.noSEP);
              });
             
          }

          else {
             $('#bpjs_info').show();
              $('#bpjs_info').html("<strong>Info !</strong> " + output.metadata.message);

          }
      },
  });
}

function carirujukan(origin,param) {
    // var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];

    var jns = $('#jenis_rujukan').val();

    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getDataRujukan");?>',
        data : 'origin='+origin+'&jns='+jns+'&param='+param,
        beforeSend : function(){
          $('#loading1').show();
          if(origin == 1){
            $('#loading1_text').html('Sedang mencari data berdasar No Kartu '+param);
          }

          else if(origin == 2){
            $('#loading1_text').html('Sedang mencari data berdasar No Rujukan '+param);
          }
          
        },
        error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
        },
        timeout : 10000,
        async: false,
        success : function(data){


          $('#loading1').hide();
          $('#loading1_text').html('');
          
          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200){
            var dataSep = output.response.item;
            var peserta = dataSep.peserta;

            $('#BpjsSep_kdProvider').val(dataSep.provKunjungan.kdProvider);
            $('#BpjsSep_nmProvider').val(dataSep.provKunjungan.nmProvider);
            $('#bpjs_error').hide();
          }
          else {
            $('#bpjs_error').show();
              $('#bpjs_error').html(output.metadata.message);

          }
        }
      });
   
  }

jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keypress', 'input,select', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        // Get all focusable elements on the page
        var $canfocus = $(':focusable').not('a').not('input[readonly]');
        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        
        $canfocus.eq(index).focus();
         $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});

  function isiNoRujukan(ui)
  {
     $("#BpjsSep_kdProvider").val(ui.item.key);
     $("#NamaPPK").val(ui.item.value);
     $("#kdProvider").val(ui.item.id);
     

  }

  function isiDiagnosa(ui)
  {
     $("#BpjsSep_DIAG_AWAL").val(ui.item.id);
     $("#NamaDIAG_AWAL").val(ui.item.value);
     

  }

  function isiPoli(ui)
  {
     $("#BpjsSep_POLI_TUJUAN").val(ui.item.id);
     $("#NamaPOLI").val(ui.item.value);
     

  }



 </script>