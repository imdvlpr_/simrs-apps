<?php
/* @var $this BpjsAplicareController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bpjs Aplicares',
);

$this->menu=array(
	array('label'=>'Create BpjsAplicare', 'url'=>array('create')),
	array('label'=>'Manage BpjsAplicare', 'url'=>array('admin')),
);
?>

<h1>Bpjs Aplicares</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
