<?php
/* @var $this BpjsAplicareController */
/* @var $model BpjsAplicare */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpjs-aplicare-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'class' => 'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kodekelas',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'kodekelas',CHtml::ListData(BpjsRuangKelas::model()->findAll(array('order'=>'urutan')),'kodekelas','namakelas'),array('class'=>'input-sm')); ?>
		<?php echo $form->error($model,'kodekelas'); ?>
		</div>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'namaruang',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php 
		echo $form->dropDownList($model,'namaruang',array(),array('class'=>'input-sm'));
		// echo $form->textField($model,'namaruang',array('size'=>60,'maxlength'=>255)); 

		?>
		<?php echo $form->error($model,'namaruang'); ?>
		</div>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'kapasitas',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kapasitas',array('class'=>'input-sm')); ?>
		<?php echo $form->error($model,'kapasitas'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tersedia',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tersedia',array('class'=>'input-sm')); ?>
		<?php echo $form->error($model,'tersedia'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tersediapria',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tersediapria',array('class'=>'input-sm')); ?>
		<?php echo $form->error($model,'tersediapria'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tersediawanita',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tersediawanita',array('class'=>'input-sm')); ?>
		<?php echo $form->error($model,'tersediawanita'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tersediapriawanita',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tersediapriawanita',array('class'=>'input-sm')); ?>
		<?php echo $form->error($model,'tersediapriawanita'); ?>
		</div>
	</div>

	

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Submit
          </button>

        
        </div>
      </div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
	function findKamar(kls){
	$.ajax({
		type : 'POST',
		data : 'q='+kls,
		url : '<?php echo Yii::app()->createUrl('AjaxRequest/getKamarByKelas');?>',
		success : function(data){
			$('#BpjsAplicare_namaruang').empty();

			var jsondata = JSON.parse(data);


			var row = '';
			$.each(jsondata,function(i,item){
				row += '<option value="'+i+'">'+item+'</option>';
			});

			$('#BpjsAplicare_namaruang').append(row);

			var kamar = $('#BpjsAplicare_namaruang').val();

			<?php 
				if(!$model->isNewRecord)
				{
			?>

				kamar = '<?php echo $model->kode_kamar;?>';
				// console.log(kamar);				
				$('#BpjsAplicare_namaruang').val(kamar);

			<?php
				}
			?>

		}

	});
}


$(document).ready(function(){
	findKamar($('#BpjsAplicare_kodekelas').val());	
	$('#BpjsAplicare_kodekelas').change(function(){
		findKamar($(this).val());
	});
});
</script>
