<?php
/* @var $this BpjsAplicareController */
/* @var $model BpjsAplicare */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'koderuang'); ?>
		<?php echo $form->textField($model,'koderuang',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'namaruang'); ?>
		<?php echo $form->textField($model,'namaruang',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kodekelas'); ?>
		<?php echo $form->textField($model,'kodekelas',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kapasitas'); ?>
		<?php echo $form->textField($model,'kapasitas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tersedia'); ?>
		<?php echo $form->textField($model,'tersedia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tersediapria'); ?>
		<?php echo $form->textField($model,'tersediapria'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tersediawanita'); ?>
		<?php echo $form->textField($model,'tersediawanita'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tersediapriawanita'); ?>
		<?php echo $form->textField($model,'tersediapriawanita'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->