<?php
/* @var $this MObatAkhpController */
/* @var $model MObatAkhp */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mobat-akhp-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kd_t', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kd_t',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'kd_t'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kd_barang', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kd_barang',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'kd_barang'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kd_js', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kd_js',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'kd_js'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_barang', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_barang',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nama_barang'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_generik', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_generik',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nama_generik'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kekuatan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kekuatan',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'kekuatan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'satuan_kekuatan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'satuan_kekuatan',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'satuan_kekuatan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'satuan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'satuan',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'satuan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jns_sediaan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jns_sediaan',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'jns_sediaan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'b_i_r', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'b_i_r',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'b_i_r'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'gen_non', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'gen_non',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'gen_non'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nar_p_non', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nar_p_non',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'nar_p_non'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'oakrl', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'oakrl',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'oakrl'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kronis', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kronis',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'kronis'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'stok', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'stok'); ?>
		<?php echo $form->error($model,'stok'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'stok_min', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'stok_min'); ?>
		<?php echo $form->error($model,'stok_min'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'hb', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'hb'); ?>
		<?php echo $form->error($model,'hb'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'hna', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'hna'); ?>
		<?php echo $form->error($model,'hna'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'diskon', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'diskon'); ?>
		<?php echo $form->error($model,'diskon'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'hj', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'hj'); ?>
		<?php echo $form->error($model,'hj'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
