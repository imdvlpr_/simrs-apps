<?php
/* @var $this MObatAkhpController */
/* @var $data MObatAkhp */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kd_barang')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kd_barang), array('view', 'id'=>$data->kd_barang)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kd_t')); ?>:</b>
	<?php echo CHtml::encode($data->kd_t); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kd_js')); ?>:</b>
	<?php echo CHtml::encode($data->kd_js); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_barang')); ?>:</b>
	<?php echo CHtml::encode($data->nama_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_generik')); ?>:</b>
	<?php echo CHtml::encode($data->nama_generik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kekuatan')); ?>:</b>
	<?php echo CHtml::encode($data->kekuatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('satuan_kekuatan')); ?>:</b>
	<?php echo CHtml::encode($data->satuan_kekuatan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('satuan')); ?>:</b>
	<?php echo CHtml::encode($data->satuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jns_sediaan')); ?>:</b>
	<?php echo CHtml::encode($data->jns_sediaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('b_i_r')); ?>:</b>
	<?php echo CHtml::encode($data->b_i_r); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gen_non')); ?>:</b>
	<?php echo CHtml::encode($data->gen_non); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nar_p_non')); ?>:</b>
	<?php echo CHtml::encode($data->nar_p_non); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oakrl')); ?>:</b>
	<?php echo CHtml::encode($data->oakrl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kronis')); ?>:</b>
	<?php echo CHtml::encode($data->kronis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stok')); ?>:</b>
	<?php echo CHtml::encode($data->stok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stok_min')); ?>:</b>
	<?php echo CHtml::encode($data->stok_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hb')); ?>:</b>
	<?php echo CHtml::encode($data->hb); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hna')); ?>:</b>
	<?php echo CHtml::encode($data->hna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('diskon')); ?>:</b>
	<?php echo CHtml::encode($data->diskon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hj')); ?>:</b>
	<?php echo CHtml::encode($data->hj); ?>
	<br />

	*/ ?>

</div>