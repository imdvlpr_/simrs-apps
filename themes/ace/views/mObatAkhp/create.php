 <?php
$this->breadcrumbs=array(
	array('name'=>'Mobat Akhp','url'=>array('admin')),
	array('name'=>'Create'),
);

?>

<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>
<div class="row">
	<div class="col-xs-12">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>
