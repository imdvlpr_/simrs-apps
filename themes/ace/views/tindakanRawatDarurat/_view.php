<?php
/* @var $this TindakanRawatDaruratController */
/* @var $data TindakanRawatDarurat */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tindakan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tindakan), array('view', 'id'=>$data->id_tindakan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_tindakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jrs')); ?>:</b>
	<?php echo CHtml::encode($data->jrs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('japel_dokter')); ?>:</b>
	<?php echo CHtml::encode($data->japel_dokter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('japel_askep')); ?>:</b>
	<?php echo CHtml::encode($data->japel_askep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tarip')); ?>:</b>
	<?php echo CHtml::encode($data->tarip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('catatan')); ?>:</b>
	<?php echo CHtml::encode($data->catatan); ?>
	<br />


</div>