<?php
/* @var $this TindakanRawatDaruratController */
/* @var $model TindakanRawatDarurat */

$this->breadcrumbs=array(
	array('name'=>'Tindakan Rawat Darurat','url'=>array('admin')),
	array('name'=>'Tindakan Rawat Darurat'),
);

$this->menu=array(
	array('label'=>'List TindakanRawatDarurat', 'url'=>array('index')),
	array('label'=>'Create TindakanRawatDarurat', 'url'=>array('create')),
	array('label'=>'Update TindakanRawatDarurat', 'url'=>array('update', 'id'=>$model->id_tindakan)),
	array('label'=>'Delete TindakanRawatDarurat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tindakan),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>View TindakanRawatDarurat #<?php echo $model->id_tindakan; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tindakan',
		'nama_tindakan',
		'jrs',
		'japel_dokter',
		'japel_askep',
		'tarip',
		'catatan',
	),
)); ?>
	</div>
</div>
