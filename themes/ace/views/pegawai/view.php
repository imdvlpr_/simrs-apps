<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';

$this->breadcrumbs=array(
    array('name' => 'Pegawai','url'=>Yii::app()->createUrl('pegawai')),
                            array('name' => 'View')
);


?>
    <div class="row">
          <div class="col-xs-12">

              <?php $this->widget('zii.widgets.CDetailView', array(
  'data'=>$model,
  'attributes'=>array(
    'KDPEGAWAI',
    'NAMA',
    'ALAMAT',
    'KOTA',
    'KODEPOS',
    'TELEPON',
    'PONSEL',
    'EMAIL',
    'STATUS',
    'USERLOG',
    'PASSWD',
  ),
)); ?>

            </div>
          </div>
