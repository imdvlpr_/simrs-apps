<?php
/* @var $this PegawaiController */
/* @var $model Pegawai */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pegawai-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class' => 'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KDPEGAWAI',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KDPEGAWAI',array('size'=>20,'maxlength'=>20,'readonly'=>'readonly','class'=>'input-sm')); ?>
		<?php echo $form->error($model,'KDPEGAWAI'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'NAMA',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'NAMA',array('size'=>50,'maxlength'=>50,'class'=>'input-sm')); ?>
		<?php echo $form->error($model,'NAMA'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'ALAMAT',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'ALAMAT',array('size'=>60,'maxlength'=>60,'class'=>'input-sm')); ?>
		<?php echo $form->error($model,'ALAMAT'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KOTA',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KOTA',array('size'=>50,'maxlength'=>50,'class'=>'input-sm')); ?>
		<?php echo $form->error($model,'KOTA'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KODEPOS',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KODEPOS',array('size'=>20,'maxlength'=>20,'class'=>'input-sm')); ?>
		<?php echo $form->error($model,'KODEPOS'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'TELEPON',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'TELEPON',array('size'=>50,'maxlength'=>50,'class'=>'input-sm')); ?>
		<?php echo $form->error($model,'TELEPON'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'PONSEL',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'PONSEL',array('size'=>20,'maxlength'=>20,'class'=>'input-sm')); ?>
		<?php echo $form->error($model,'PONSEL'); ?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'EMAIL',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'EMAIL',array('size'=>50,'maxlength'=>50,'class'=>'input-sm')); ?>
		<?php echo $form->error($model,'EMAIL'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'STATUS',array('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'STATUS',array('size'=>20,'maxlength'=>20,'class'=>'input-sm')); ?>
		<?php echo $form->error($model,'STATUS'); ?>
		</div>
	</div>
	

	
   <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>

        
        </div>
      </div>
             

<?php $this->endWidget(); ?>
