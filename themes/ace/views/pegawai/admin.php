<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Pegawai','url'=>Yii::app()->createUrl('pegawai/index')),
                            array('name' => 'List')
);

?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<script type="text/javascript">
	
	$(document).ready(function(){
		$('#search,#size').change(function(){
    
	        $('#tabelpegawai').yiiGridView.update('tabelpegawai', {
	            url:'?r=pegawai&filter='+$('#search').val()+'&size='+$('#size').val()   
	        });
	      
	     });

	   
	});
</script>
<div class="row">
    <div class="col-xs-12">
                        <div class="pull-left"><?php 
  echo CHtml::link('Tambah Pegawai Baru',array('pegawai/create'),array('class'=>'btn btn-success'));
?></div>
                         <div class="pull-right">Data per halaman
                            <?php echo CHtml::dropDownList('Pegawai[PAGESIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,20=>20,30=>30,),array('id'=>'size')); ?> <?php
        echo CHtml::textField('Pegawai[SEARCH]','',array('placeholder'=>'Cari','id'=>'search')); 
        ?> 	 </div> 
                   
                      
<?php $this->widget('application.components.ComplexGridView', array(
  'id'=>'tabelpegawai',
  'dataProvider'=>$model->search(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
    'KDPEGAWAI',
    'NAMA',
    'ALAMAT',
    'PONSEL',
    'EMAIL',
    
    // array(
    // 	'header' 	=> 'Nama',
    // 	'value'		=> '$data->users->USERNAME'
    // ),
    array(
      'class'=>'CButtonColumn',
      'template'=>'{update} {delete}',
      
    ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
    'pager'=>array(
                  'class'=>'SimplePager',
                  'header'=>'',
                  'firstPageLabel'=>'Pertama',
                  'prevPageLabel'=>'Sebelumnya',
                  'nextPageLabel'=>'Selanjutnya',
                  'lastPageLabel'=>'Terakhir',
                  'lastPageLabel'=>'Terakhir',
                  'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
                  'hiddenPageCssClass'=>'disabled',
                    'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
                  'maxButtonCount'=>5
                ),
                'itemsCssClass'=>'table  table-bordered table-hover',
                'summaryCssClass'=>'table-message-info',
                'filterCssClass'=>'filter',
                'summaryText'=>'menampilkan {start} - {end} dari {count} data',
                'template'=>'{items}{summary}{pager}',
                'emptyText'=>'Data tidak ditemukan',
                'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>

                    </div>
                </div>
                