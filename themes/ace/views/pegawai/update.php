<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';

$this->breadcrumbs=array(
    array('name' => 'Pegawai','url'=>Yii::app()->createUrl('pegawai')),
                            array('name' => 'Update')
);


?>
    <div class="row">
          <div class="col-xs-12">
            
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
          </div>
        </div>
