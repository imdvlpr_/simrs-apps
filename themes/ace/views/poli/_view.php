<?php
/* @var $this PoliController */
/* @var $data Poli */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('KODE_POLI')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->KODE_POLI), array('view', 'id'=>$data->KODE_POLI)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NAMA')); ?>:</b>
	<?php echo CHtml::encode($data->NAMA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KODE_KARCIS')); ?>:</b>
	<?php echo CHtml::encode($data->KODE_KARCIS); ?>
	<br />


</div>