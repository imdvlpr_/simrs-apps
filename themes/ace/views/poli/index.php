<?php
/* @var $this PoliController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array('name'=>'Polis'),
);

$this->menu=array(
	array('label'=>'Create Poli', 'url'=>array('create')),
	array('label'=>'Manage Poli', 'url'=>array('admin')),
);
?>

<h1>Polis</h1>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</div>
</div>
