<?php
/* @var $this PoliController */
/* @var $model Poli */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'poli-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KODE_POLI', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KODE_POLI'); ?>
		<?php echo $form->error($model,'KODE_POLI'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'NAMA', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'NAMA',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'NAMA'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KODE_KARCIS', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KODE_KARCIS',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'KODE_KARCIS'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
