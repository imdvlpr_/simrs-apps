<?php
/* @var $this PoliController */
/* @var $model Poli */

$this->breadcrumbs=array(
	array('name'=>'Poli','url'=>array('admin')),
	array('name'=>'Poli'),
);

$this->menu=array(
	array('label'=>'List Poli', 'url'=>array('index')),
	array('label'=>'Create Poli', 'url'=>array('create')),
	array('label'=>'Update Poli', 'url'=>array('update', 'id'=>$model->KODE_POLI)),
	array('label'=>'Delete Poli', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->KODE_POLI),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Poli', 'url'=>array('admin')),
);
?>

<h1>View Poli #<?php echo $model->KODE_POLI; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'KODE_POLI',
		'NAMA',
		'KODE_KARCIS',
	),
)); ?>
	</div>
</div>
