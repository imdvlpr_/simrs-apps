<?php
/* @var $this IcdController */
/* @var $model Icd */

$this->breadcrumbs=array(
	array('name'=>'Icd','url'=>array('admin')),
	array('name'=>'Icd'),
);

$this->menu=array(
	array('label'=>'List Icd', 'url'=>array('index')),
	array('label'=>'Create Icd', 'url'=>array('create')),
	array('label'=>'Update Icd', 'url'=>array('update', 'id'=>$model->kode)),
	array('label'=>'Delete Icd', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kode),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Icd', 'url'=>array('admin')),
);
?>

<h1>View Icd #<?php echo $model->kode; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'kode',
		'deskripsi',
		'dtd_kode',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
