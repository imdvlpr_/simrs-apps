<?php
/* @var $this UnitController */
/* @var $model Unit */

$this->breadcrumbs=array(
	array('name'=>'Unit','url'=>array('admin')),
	array('name'=>'Unit'),
);

$this->menu=array(
	array('label'=>'List Unit', 'url'=>array('index')),
	array('label'=>'Create Unit', 'url'=>array('create')),
	array('label'=>'Update Unit', 'url'=>array('update', 'id'=>$model->KodeUnit)),
	array('label'=>'Delete Unit', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->KodeUnit),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Unit', 'url'=>array('admin')),
);
?>

<h1>View Unit #<?php echo $model->KodeUnit; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<style type="text/css">
  
.signature-pad {
  position: relative;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  font-size: 10px;
  width: 500px;
  height: 400px;
  max-width: 700px;
  max-height: 460px;
  border: 1px solid #e8e8e8;
  background-color: #fff;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.27), 0 0 40px rgba(0, 0, 0, 0.08) inset;
  border-radius: 4px;
  padding: 16px;
}

.signature-pad::before,
.signature-pad::after {
  position: absolute;
  z-index: -1;
  content: "";
  width: 40%;
  height: 10px;
  bottom: 10px;
  background: transparent;
  box-shadow: 0 8px 12px rgba(0, 0, 0, 0.4);
}

.signature-pad::before {
  left: 20px;
  -webkit-transform: skew(-3deg) rotate(-3deg);
          transform: skew(-3deg) rotate(-3deg);
}

.signature-pad::after {
  right: 20px;
  -webkit-transform: skew(3deg) rotate(3deg);
          transform: skew(3deg) rotate(3deg);
}

.signature-pad--body {
  position: relative;
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  border: 1px solid #f4f4f4;
}

.signature-pad--body
canvas {
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  border-radius: 4px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.02) inset;
}

.signature-pad--footer {
  color: #C3C3C3;
  text-align: center;
  font-size: 1.2em;
  margin-top: 8px;
}

.signature-pad--actions {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  margin-top: 8px;
}
.button{
  color:black;
}
</style>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'KodeUnit',
		'NamaUnit',
		'unit_tipe',
	),
)); ?>
</div></div>
<div class="row">
	<div class="col-xs-12">

      <div class="pull-left"><br>
       <?= CHtml::link('<i class="fa fa-user"></i> Tambah Dokter','javascript:void(0)',array('class'=>'btn btn-success','id'=>'btn-add-dokter')); ?></div>

<div id="alert"></div>
                         
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'table-dokter-grid',
	'dataProvider'=>$dokter->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		
		'dokter.nama_dokter',
		'dokter.jenis_dokter',
		
		'dokter.alamat_praktik',
		'dokter.telp',
		/*
		'created',
		'nama_panggilan',
		*/
		array(
			'class'=>'CButtonColumn',
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => [
                    'options' => ['class' => 'save-ajax-button'],
                    'url' => 'Yii::app()->createAbsoluteUrl("unitDokter/update", array("id"=>$data->id))',
                    'click' => "function( e ){
                      e.preventDefault();
                      var url_string = $(this).attr('href');
                      var url = new URL(url_string);
                      var id = url.searchParams.get('id');
                      $('#unit_dokter_id').val(id);
                      $( '#update-dialog' )
                    .dialog( { title: 'Update Data' } )
                    .dialog( 'open' );

                      
                    }",
                ],
				'delete' => [
					'url'=>'Yii::app()->createUrl("unitDokter/delete/", array("id"=>$data->id))',  
				]
			]
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info btn-sm',
									'previousPageCssClass'=>'btn btn-info btn-sm',
									'nextPageCssClass'=>'btn btn-info btn-sm',
									'lastPageCssClass'=>'btn btn-info btn-sm',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info btn-sm',
									'selectedPageCssClass'=>'btn btn-sky btn-sm',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">

 <div class="pull-left"><br>
       <?= CHtml::link('<i class="fa fa-user"></i> Tambah Kepala Unit','javascript:void(0)',array('class'=>'btn btn-success','id'=>'btn-add-kepala')); ?></div>

<div id="alert-kepala"></div>
                         
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'table-kepala-grid',
	'dataProvider'=>$kepala->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		
		'pegawai.NAMA',
		/*
		'created',
		'nama_panggilan',
		*/
		array(
			'class'=>'CButtonColumn',
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => [
                    'options' => ['class' => 'update-kepala-link'],
                    'url' => 'Yii::app()->createAbsoluteUrl("unitKepala/update", array("id"=>$data->id))',
                    'click' => "function( e ){
                      e.preventDefault();
                      var url_string = $(this).attr('href');
                      var url = new URL(url_string);
                      var id = url.searchParams.get('id');
                      $('#unit_kepala_id').val(id);
                      $( '#update-dialog-kepala' )
                    .dialog( { title: 'Update Data' } )
                    .dialog( 'open' );

                      
                    }",
                ],
				'delete' => [
					'url'=>'Yii::app()->createUrl("unitKepala/delete/", array("id"=>$data->id))',  
				]
			]
		),
	),
	'htmlOptions'=>array(
		'class'=>'table'
	),
	'pager'=>array(
		'class'=>'SimplePager',
		'header'=>'',
		'firstPageLabel'=>'Pertama',
		'prevPageLabel'=>'Sebelumnya',
		'nextPageLabel'=>'Selanjutnya',
		'lastPageLabel'=>'Terakhir',
		'firstPageCssClass'=>'btn btn-info btn-sm',
		'previousPageCssClass'=>'btn btn-info btn-sm',
		'nextPageCssClass'=>'btn btn-info btn-sm',
		'lastPageCssClass'=>'btn btn-info btn-sm',
		'hiddenPageCssClass'=>'disabled',
		'internalPageCssClass'=>'btn btn-info btn-sm',
		'selectedPageCssClass'=>'btn btn-sky btn-sm',
		'maxButtonCount'=>5
	),
	'itemsCssClass'=>'table  table-bordered table-hover',
	'summaryCssClass'=>'table-message-info',
	'filterCssClass'=>'filter',
	'summaryText'=>'menampilkan {start} - {end} dari {count} data',
	'template'=>'{items}{summary}{pager}',
	'emptyText'=>'Data tidak ditemukan',
	'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">

  <div class="pull-left">
       <?= CHtml::link('<i class="fa fa-money"></i> Tambah Biaya','javascript:void(0)',array('class'=>'btn btn-success','id'=>'btn-add-biaya')); ?></div>

<div id="alert-biaya"></div>
                         
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'table-biaya-grid',
	'dataProvider'=>$biaya->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		
		'biaya',
		'jrs',
		'jaspel',
		'bhp',
		/*
		'created',
		'nama_panggilan',
		*/
		array(
			'class'=>'CButtonColumn',
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => [
                    'options' => ['class' => 'save-ajax-button'],
                    'url' => 'Yii::app()->createAbsoluteUrl("unitBiaya/update", array("id"=>$data->id))',
                    'click' => "function( e ){
                      e.preventDefault();
                      var url_string = $(this).attr('href');
                      var url = new URL(url_string);
                      var id = url.searchParams.get('id');
                      $('#unit_biaya_id').val(id);
                      $( '#update-dialog-biaya' )
                    .dialog( { title: 'Update Data Biaya' } )
                    .dialog( 'open' );

                      
                    }",
                ],
				'delete' => [
					'url'=>'Yii::app()->createUrl("unitBiaya/delete/", array("id"=>$data->id))',  
				]
			]
		),
	),
	'htmlOptions'=>array(
			'class'=>'table'
		),
		'pager'=>array(
			'class'=>'SimplePager',
			'header'=>'',
			'firstPageLabel'=>'Pertama',
			'prevPageLabel'=>'Sebelumnya',
			'nextPageLabel'=>'Selanjutnya',
			'lastPageLabel'=>'Terakhir',
			'firstPageCssClass'=>'btn btn-info btn-sm',
			'previousPageCssClass'=>'btn btn-info btn-sm',
			'nextPageCssClass'=>'btn btn-info btn-sm',
			'lastPageCssClass'=>'btn btn-info btn-sm',
			'hiddenPageCssClass'=>'disabled',
			'internalPageCssClass'=>'btn btn-info btn-sm',
			'selectedPageCssClass'=>'btn btn-sky btn-sm',
			'maxButtonCount'=>5
		),
		'itemsCssClass'=>'table  table-bordered table-hover',
		'summaryCssClass'=>'table-message-info',
		'filterCssClass'=>'filter',
		'summaryText'=>'menampilkan {start} - {end} dari {count} data',
		'template'=>'{items}{summary}{pager}',
		'emptyText'=>'Data tidak ditemukan',
		'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>
	</div>
</div>

<?php 

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'update-dialog',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 500,
    'resizable' => false,
  ),
)); ?>
<div class="update-dialog-content">
	 
	 <div class="row">
    <form class="form-horizontal">
    
    <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right"> Dokter</label>
      <div class="col-sm-9">

        <?php 
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'name'=>'nama_dokter',
                      'attribute' => 'nama_dokter',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getDokter').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',
                      'htmlOptions' =>[
                      	'placeholder' => 'Ketik nama dokter'
                      ],
                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 

                            $("#dokter_id").val(ui.item.id);
                          }',
                  ),
                  ));
	
		 ?>
        <input type="hidden" id="dokter_id"/>
        <input type="hidden" id="unit_dokter_id"/>
        <input type="hidden" id="unit_id" value="<?=$model->KodeUnit;?>"/>
      </div>
  	</div>
    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button id="btn-simpan" class="btn btn-info" type="button">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
          <img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
	  </div>
      </div>
	</form>  
</div>

</div>
<?php $this->endWidget(); ?>


<?php 

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'update-dialog-kepala',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 900,
    'height' => 600,
    'resizable' => false,
  ),
)); ?>
<div class="update-dialog-content">
	 
	 <div class="row">
    <form class="form-horizontal">
    
    <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right"> Pegawai</label>
      <div class="col-sm-9">

        <?php 
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
          'name'=>'nama_pegawai',
          'attribute' => 'nama_pegawai',
          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('AjaxRequest/getPegawai').'",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
          'htmlOptions' =>[
          	'placeholder' => 'Ketik nama pegawai'
          ],
          'options' => array(
              'minLength' => 2,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 

                $("#kepala_id").val(ui.item.id);
              }',
      ),
      ));
	
		 ?>
        <input type="hidden" id="kepala_id"/>
        <input type="hidden" id="unit_kepala_id"/>
      </div>
  	</div>
  	<div class="form-group">
  		<label class="col-sm-3 control-label no-padding-right">TTD</label>
  		<div class="col-sm-9">
  			<div id="signature-pad" class="signature-pad">
    <div class="signature-pad--body">
      <canvas></canvas>
    </div>
    <div class="signature-pad--footer">
      <div class="description">Sign above</div>

      <div class="signature-pad--actions">
        <div>
          <button type="button" class="button clear" data-action="clear">Clear</button>
          <button type="button" class="button" data-action="change-color">Change color</button>
          <button type="button" class="button" data-action="undo">Undo</button>

        </div>
        
      </div>
    </div>
  </div>
  		</div>
  	</div>
    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button id="btn-simpan-kepala" class="btn btn-info" type="button" data-action="save-png">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
          <img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
	  </div>
      </div>
	</form>  
</div>

</div>
<?php $this->endWidget(); ?>
<?php 

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'update-dialog-biaya',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 500,
    'resizable' => false,
  ),
)); ?>
<div class="update-dialog-content">
	 
	 <div class="row">
    <form class="form-horizontal">
    
    <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right"> Biaya</label>
      <div class="col-sm-9">

       
        <input type="text" id="biaya" name="biaya"/>
        <input type="hidden" id="unit_biaya_id"/>
        <input type="hidden" id="unit_id" value="<?=$model->KodeUnit;?>"/>
      </div>
  	</div>
  	<div class="form-group">
      <label class="col-sm-3 control-label no-padding-right"> JRS</label>
      <div class="col-sm-9">
        <input type="text" id="jrs" name="jrs"/>
       
      </div>
  	</div>
  		<div class="form-group">
      <label class="col-sm-3 control-label no-padding-right"> JASPEL</label>
      <div class="col-sm-9">
        <input type="text" id="jaspel" name="jaspel"/>
       
      </div>
  	</div>
  		<div class="form-group">
      <label class="col-sm-3 control-label no-padding-right"> BHP</label>
      <div class="col-sm-9">
        <input type="text" id="bhp" name="bhp"/>
       
      </div>
  	</div>
    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button id="btn-simpan-biaya" class="btn btn-info" type="button">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
          <img id="loading-biaya" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
	  </div>
      </div>
	</form>  
</div>

</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">

function updateTable(){
  $('#table-dokter-grid').yiiGridView.update('table-dokter-grid', {

      url:'<?=Yii::app()->createUrl('unit/view',array('id'=>$model->KodeUnit));?>',
     
  });
}

function updateTableBiaya(){
  $('#table-biaya-grid').yiiGridView.update('table-biaya-grid', {

      url:'<?=Yii::app()->createUrl('unit/view',array('id'=>$model->KodeUnit));?>',
     
  });
}


	$(document).ready(function(){
		$('#btn-add-dokter').click(function(){
			$('#unit_dokter_id').val('');
			$( '#update-dialog' )
		          .dialog( { title: 'Data Dokter' } )
		          .dialog( 'open' );
		});

		$('#btn-add-kepala').click(function(){
			$('#unit_pegawai_id').val('');
			$( '#update-dialog-kepala' )
		          .dialog( { title: 'Data Pegawai' } )
		          .dialog( 'open' );
		});

		$('#btn-add-biaya').click(function(){
			$('#unit_biaya_id').val('');
			$( '#update-dialog-biaya' )
		          .dialog( { title: 'Data Biaya' } )
		          .dialog( 'open' );
		});

		$('#btn-simpan-kepala').click(function(){
			
			if (signaturePad.isEmpty()) {
			    alert("Please provide a signature first.");
			  } else {
			    var dataURL = signaturePad.toDataURL();
			    
			    var obj = new Object;
			    obj.signature = dataURL;
			    obj.unit_id = $('#unit_id').val();
			    obj.pegawai_id = $('#kepala_id').val();
			    $.ajax({
			      type : 'POST',
			      url : '<?=Yii::app()->createUrl('pegawai/ajaxSimpan');?>',
			      data : {
			        dataItem : obj
			      },
			      beforeSend : function(){},
			      error : function(e){

			      },
			      success : function(data){
			        var hsl = $.parseJSON(data);
			        if(hsl.code == '200'){	
			        	$( '#update-dialog-kepala' ).dialog('close');
			        }

			        alert(hsl.message);	
			        
			      },
			    });
			    // download(dataURL, "signature.png");
			  }
			
		});

		$('#btn-simpan').click(function(){
			
			item = new Object;
			item.is_old = $('#unit_dokter_id').val() == '' ? 0 : 1;			
			item.dokter_id = $('#dokter_id').val();
			item.unit_id = $('#unit_id').val();
			item.unit_dokter_id = item.is_old == 1 ? $('#unit_dokter_id').val() : 0;

			$.ajax({
				type : 'post',
				url : '<?=Yii::app()->createUrl('unit/ajaxAddDokter');?>',

				data : {dataItem:item},
				beforeSend : function(){
				  $('#loading').show();
				},
				success : function(res){
				  var res = $.parseJSON(res);
				 
				  $('#alert').fadeIn(100);
				  $('#alert').html('<div class="alert alert-'+res.shortmsg+'">'+res.message+'</div>');
				  $('#loading').hide();
				  
				  if(res.shortmsg=='success'){
				    $('#alert').fadeOut(2000);
				    updateTable();
				  }

				  
				},
			});
			$( '#update-dialog' ).dialog('close');
		});

		$('#btn-simpan-biaya').click(function(){
			
			item = new Object;
			item.is_old = $('#unit_biaya_id').val() == '' ? 0 : 1;			
			item.unit_id = $('#unit_id').val();
			item.biaya = $('#biaya').val();
			item.jrs = $('#jrs').val();
			item.jaspel = $('#jaspel').val();
			item.bhp = $('#bhp').val();
			item.unit_biaya_id = item.is_old == 1 ? $('#unit_biaya_id').val() : 0;

			$.ajax({
				type : 'post',
				url : '<?=Yii::app()->createUrl('unit/ajaxAddBiaya');?>',

				data : {dataItem:item},
				beforeSend : function(){
				  $('#loading-biaya').show();
				},
				success : function(res){
				  var res = $.parseJSON(res);
				 
				  $('#alert-biaya').fadeIn(100);
				  $('#alert-biaya').html('<div class="alert alert-'+res.shortmsg+'">'+res.message+'</div>');
				  $('#loading-biaya').hide();
				  
				  if(res.shortmsg=='success'){
				    $('#alert-biaya').fadeOut(2000);
				    updateTableBiaya();
				  }

				  
				},
			});
			$( '#update-dialog-biaya' ).dialog('close');
		});
	});
</script>
<script src="<?=Yii::app()->baseUrl;?>/node_modules/signature_pad/dist/signature_pad.umd.js"></script>
  <script type="text/javascript">
    var wrapper = document.getElementById("signature-pad");
	var clearButton = wrapper.querySelector("[data-action=clear]");
	var changeColorButton = wrapper.querySelector("[data-action=change-color]");
	var undoButton = wrapper.querySelector("[data-action=undo]");
	var savePNGButton = wrapper.querySelector("[data-action=save-png]");
	var canvas = wrapper.querySelector("canvas");
	var signaturePad = new SignaturePad(canvas, {
  // It's Necessary to use an opaque color when saving image as JPEG;
  // this option can be omitted if only saving as PNG or SVG
		  backgroundColor: 'rgb(255, 255, 255)'
		});



// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
  // When zoomed out to less than 100%, for some very strange reason,
  // some browsers report devicePixelRatio as less than 1
  // and only part of the canvas is cleared then.
  var ratio =  1;//Math.max(window.devicePixelRatio || 1, 1);
  // console.log(ratio);
  // This part causes the canvas to be cleared
  canvas.width = canvas.offsetWidth * ratio;
  canvas.height = canvas.offsetHeight * ratio;
  canvas.getContext("2d").scale(ratio, ratio);

  // This library does not listen for canvas changes, so after the canvas is automatically
  // cleared by the browser, SignaturePad#isEmpty might still return false, even though the
  // canvas looks empty, because the internal data of this library wasn't cleared. To make sure
  // that the state of this library is consistent with visual state of the canvas, you
  // have to clear it manually.
  signaturePad.clear();
}

// On mobile devices it might make more sense to listen to orientation change,
// rather than window resize events.
window.onresize = resizeCanvas;
resizeCanvas();


// One could simply use Canvas#toBlob method instead, but it's just to show
// that it can be done using result of SignaturePad#toDataURL.
function dataURLToBlob(dataURL) {
  // Code taken from https://github.com/ebidel/filer.js
  var parts = dataURL.split(';base64,');
  var contentType = parts[0].split(":")[1];
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;
  var uInt8Array = new Uint8Array(rawLength);

  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], { type: contentType });
}

clearButton.addEventListener("click", function (event) {
  signaturePad.clear();
});

undoButton.addEventListener("click", function (event) {
  var data = signaturePad.toData();

  if (data) {
    data.pop(); // remove the last dot or line
    signaturePad.fromData(data);
  }
});

changeColorButton.addEventListener("click", function (event) {
  var r = Math.round(Math.random() * 255);
  var g = Math.round(Math.random() * 255);
  var b = Math.round(Math.random() * 255);
  var color = "rgb(" + r + "," + g + "," + b +")";

  signaturePad.penColor = color;
});


  </script>