<?php
/* @var $this BPendaftaranTindakanController */
/* @var $model BPendaftaranTindakan */

$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Tindakan','url'=>array('admin')),
	array('name'=>'Bpendaftaran Tindakan'),
);

$this->menu=array(
	array('label'=>'List BPendaftaranTindakan', 'url'=>array('index')),
	array('label'=>'Create BPendaftaranTindakan', 'url'=>array('create')),
	array('label'=>'Update BPendaftaranTindakan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BPendaftaranTindakan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BPendaftaranTindakan', 'url'=>array('admin')),
);
?>

<h1>View BPendaftaranTindakan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'reg_id',
		'tindakan',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
