<?php
/* @var $this TindakanMedisPenunjangController */
/* @var $model TindakanMedisPenunjang */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tindakan-medis-penunjang-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_tindakan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nama_tindakan',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama_tindakan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_id', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'kelas_id'); ?>
		<?php echo $form->error($model,'kelas_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bhp', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'bhp'); ?>
		<?php echo $form->error($model,'bhp'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jrs', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jrs'); ?>
		<?php echo $form->error($model,'jrs'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'japel', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'japel'); ?>
		<?php echo $form->error($model,'japel'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tarip', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tarip'); ?>
		<?php echo $form->error($model,'tarip'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'param', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'param',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'param'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'catatan', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'catatan',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'catatan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'created', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenis_penunjang', array ('class'=>'col-sm-3 control-label no-padding-right')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jenis_penunjang'); ?>
		<?php echo $form->error($model,'jenis_penunjang'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
