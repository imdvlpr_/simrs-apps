<?php
/* @var $this TrResepKamarController */
/* @var $model TrResepKamar */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tr-resep-kamar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

  
	<div class="form-group">
		<?php echo $form->labelEx($model,'dokter_id', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		
		<?php 
		  echo CHtml::textField('nama_dokter',!empty($model->dokter) ? $model->dokter->FULLNAME : '',array('class'=>'input-large nama_dokter','placeholder'=>'Ketik Nama Dokter'));
                    echo $form->hiddenField($model,'dokter_id');
		?>
		<?php echo $form->error($model,'dokter_id'); ?>
		</div>
	</div>

	


	<div class="form-group">
		<?php echo $form->labelEx($model,'tgl_resep', array ('class'=>'col-sm-3 control-label no-padding-right ', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'tgl_resep',['class'=>' date-picker']); ?>
		<?php echo $form->error($model,'tgl_resep'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kamar_id', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php 
		$listKamar = CHtml::listData(DmKamar::model()->findAll(['order'=>'nama_kamar']),'id_kamar',function($data){
			return $data->nama_kamar.' - '.$data->kelas->nama_kelas;
		});
		echo $form->dropDownList($model,'kamar_id',$listKamar); 
		?>
		<?php echo $form->error($model,'kamar_id'); ?>
		</div>
	</div>

	

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
<?php 
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
   
$cs->registerScriptFile($baseUrl.'/assets/focus-next.js');

?>
<script type="text/javascript">

$(document).bind("keyup.autocomplete",function(){

   $('.nama_dokter').autocomplete({
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
        
      },
      focus: function (event, ui) {
       $(this).next().val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

<?php 

if(!empty($_POST['TrResepKamar']['PASIEN_NAMA'])){
?>
$("#TrResepKamar_PASIEN_NAMA").val('<?=$_POST['TrResepKamar']['PASIEN_NAMA'];?>'); 
<?php 
}
?>

function isiDataPasien(ui)
{
	 $("#TrResepKamar_pasien_id").val(ui.item.id);
	 $("#TrResepKamar_PASIEN_NAMA").val(ui.item.value); 
	 
  $.ajax({
    url : '<?php echo Yii::app()->createUrl('AjaxRequest/GetPasienPendaftaran');?>',
    type : 'POST',
    data : 'norm='+$("#TrResepKamar_pasien_id").val(),
    async : true,
    success : function(data){
       var hsl = jQuery.parseJSON(data);
       
      $("#TrResepKamar_kode_daftar").val(hsl.kode_daftar); 
     $('#TrResepKamar_jenis_pasien').val(hsl.kode_gol);
     // $('#jenis_pasien').val(hsl.nama_gol);

    }
  });

}
</script>