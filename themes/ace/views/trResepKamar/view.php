<?php
/* @var $this TrResepKamarController */
/* @var $model TrResepKamar */

$this->breadcrumbs=array(
	array('name'=>'Tr Resep Kamar','url'=>array('admin')),
	array('name'=>'Tr Resep Kamar'),
);

$this->menu=array(
	array('label'=>'List TrResepKamar', 'url'=>array('index')),
	array('label'=>'Create TrResepKamar', 'url'=>array('create')),
	// array('label'=>'Update TrResepKamar', 'url'=>array('update', 'id'=>$model->id)),
	// array('label'=>'Delete TrResepKamar', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrResepKamar', 'url'=>array('admin')),
);
?>

 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>

<div class="row">
	<div class="col-xs-12">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="row">
				<div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
					<b>Info Pasien</b>
				</div>
			</div>

			<div>
				<ul class="list-unstyled spaced">
					<li>
						<i class="ace-icon fa fa-caret-right blue"></i>
						Nama : <?=$dataDaftar->noMedrec->NAMA;?> / <?=$dataDaftar->noMedrec->NoMedrec;?>
					</li>

					<li>
						<i class="ace-icon fa fa-caret-right blue"></i>
						JK : <?=$dataDaftar->noMedrec->JENSKEL == 'L' ? 'Laki-laki' : 'Perempuan';?>
					</li>

					<li>
						<i class="ace-icon fa fa-caret-right blue"></i>
						Alamat : <?=$dataDaftar->noMedrec->ALAMAT;?>
					</li>
					<li>
						<i class="ace-icon fa fa-caret-right blue"></i>
						Kode Daftar : <?=$dataDaftar->NODAFTAR;?>
					</li>

					
					
				</ul>
			</div>
		</div><!-- /.col -->
		<?php 
		$idx = 0;
		$total = 0;
		foreach($listResep as $resep)
		{
			$idx++;
			$subtotal = $resep->sumObatItem;
			$subtotal_racikan = $resep->sumObatRacikan;
			$total += $subtotal + $subtotal_racikan;
		?>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="widget-box transparent">
					<div class="widget-header widget-header-large">
						<h3 class="widget-title grey lighter">
							<i class="ace-icon fa fa-leaf green"></i>
							Resep <?=$idx;?>
						</h3>

						<div class="widget-toolbar no-border invoice-info">
							<span class="invoice-info-label">No.Resep:</span>
							<span class="red"><?=$resep->no_resep;?></span>

							<br />
							<span class="invoice-info-label">Date:</span>
							<span class="blue"><?=$resep->tgl_resep;?></span>

						</div>
						<div class="widget-toolbar no-border invoice-info">
							<span class="invoice-info-label">Dokter:</span>
							<span class="red"><?=$resep->dokter->FULLNAME;?></span>

							<br />
							<span class="invoice-info-label">Kamar:</span>
							<span class="blue"><?=$resep->kamar->nama_kamar.' / '.$resep->kamar->kelas->nama_kelas;?></span>

						</div>
						<div class="widget-toolbar hidden-480">
							<a class="" href="<?=Yii::app()->createUrl('trResepKamar/print',['id'=>$resep->id]);?>" title="Print Nota" target="_blank">
								<i class="ace-icon fa fa-print"></i>&nbsp;Print Nota
							</a>
						</div>

						<div class="widget-toolbar hidden-480">
							<a class="" href="<?=Yii::app()->createUrl('trResepKamar/input',['id'=>$resep->id]);?>" title="Update Data">
								<i class="ace-icon fa fa-pencil"></i>&nbsp;Update Data
							</a>
						</div>

					</div>

					<div class="widget-body">
						<div class="widget-main padding-24">
							<div class="row">
								

								<div class="col-sm-6">
									<div class="row">
										<div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
											<b>Non Racikan</b>
										</div>
									</div>

								
								</div><!-- /.col -->
								<div class="col-sm-6">
									<div class="row">
										<div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
											<b>Racikan</b>
										</div>
									</div>

								</div><!-- /.col -->
							</div><!-- /.row -->
							<div class="space-6"></div>
						

							<div class="col-xs-6">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th class="center">#</th>
											<th>Obat</th>
											<th class="hidden-xs">Qty</th>
											<th class="hidden-480">Harga Satuan</th>
											<th>Harga Total</th>
										</tr>
									</thead>

									<tbody>
										<?php 
										$i = 0;

										foreach($resep->trResepKamarItems as $item)
										{

										$i++;
										?>
										<tr>
											<td class="center"><?=($i);?></td>

											<td>
												<?=$item->obat->nama_barang;?>
											</td>
											<td class="hidden-xs">
												<?=$item->jumlah;?>
											</td>
											<td class="hidden-480"> <?=$item->obat->hj;?> </td>
											<td style="text-align: right"><?=Yii::app()->helper->formatRupiah($item->harga);?></td>
										</tr>
										<?php 
										}
										?>
										<tr>
											<td colspan="4" style="text-align: right">Subtotal</td>
											<td style="text-align: right"><b><?=Yii::app()->helper->formatRupiah($subtotal);?></b></td>
										</tr>
									</tbody>
									
								</table>
							</div>
							<div class="col-xs-6">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th class="center">#</th>
											<th>Obat</th>
											<th class="hidden-xs">Qty</th>
											<th class="hidden-480">Harga Satuan</th>
											<th>Harga Total</th>
										</tr>
									</thead>

								
									<tbody>
										<?php 
										
										$i=0;
										foreach($resep->trResepKamarRacikans as $item)
										{
											$i++;
										?>
										<tr>
											<td class="center"><?=($i);?></td>

											<td>
												<?=$item->trObatRacikan->nama;?>
											</td>
											<td class="hidden-xs">
												<?=$item->jumlah;?>
											</td>
											<td class="hidden-480"> --- </td>
											<td style="text-align: right"><?=Yii::app()->helper->formatRupiah($item->harga);?></td>
										</tr>
										<?php 
										}
										?>
										<tr>
											<td colspan="4" style="text-align: right">Subtotal</td>
											<td style="text-align: right"><b><?=Yii::app()->helper->formatRupiah($subtotal_racikan);?></b></td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="hr hr8 hr-double hr-dotted"></div>

							
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php 
		}
		?>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
		<div class="row">
			<div class="col-sm-5 pull-right">

				<h4 class="pull-right">
					<?=CHtml::link('<i class="fa fa-print"></i> Ekspor ke XLS',['trResepKamar/viewXls','kode_daftar'=>$dataDaftar->NODAFTAR],['class'=>'btn btn-success']);?>
					<b>Total amount :
					<span class="red"><?=Yii::app()->helper->formatRupiah($total);?></span></b>
				</h4>
			</div>
			
		</div>
	</div></div>
	</div>
</div>
