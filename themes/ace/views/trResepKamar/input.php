<?php
/* @var $this TrResepKamarController */
/* @var $model TrResepKamar */

$this->breadcrumbs=array(
	array('name'=>'Resep Kamar','url'=>array('trResepPasien/view','id'=>$model->tr_resep_pasien_id)),
	array('name'=>'Resep Kamar'),
);

$this->menu=array(
	array('label'=>'List TrResepKamar', 'url'=>array('index')),
	array('label'=>'Create TrResepKamar', 'url'=>array('create')),
	array('label'=>'Update TrResepKamar', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrResepKamar', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrResepKamar', 'url'=>array('admin')),
);
?>

<h1>View Resep #<?php echo $model->no_resep; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-6">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		[
      'label' => 'No RM',
      'value' => Yii::app()->helper->appendZeros($model->trResepPasien->pasien_id,6)
    ],
		[
			'label' => 'Pasien',
			'value' => $model->trResepPasien->pasien->NAMA
		],
		[
			'label' => 'Jenis Px',
			'value' => function($data){
				return $data->trResepPasien->nodaftar->kodeGol->NamaGol;
			}
		],
		[
			'label' => 'Kamar',
			'value' => function($data){
				return $data->kamar->nama_kamar.' / '.$data->kamar->kelas->nama_kelas;
			}
		],
		[
			'label' => 'Tgl Lahir',
			'value' => date('d/m/Y',strtotime($model->trResepPasien->pasien->TGLLAHIR))
		],
		
	),
)); ?>
	</div>
	<div class="col-xs-6">
		<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		'no_resep',
		[
			'label' => 'Dokter',
			'value' => $model->dokter->FULLNAME
		],
		'tgl_resep',
		[
			'label' => 'Petugas',
			'value' => $model->petugas->NAMA
		],
		'created',
	),
)); ?>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="tabbable">
			<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
				<li class="active">
					<a data-toggle="tab" href="#non-racikan">Non-Racikan</a>
				</li>

				<li>
					<a data-toggle="tab" href="#racikan">Racikan</a>
				</li>

			</ul>

			<div class="tab-content">
				<div id="non-racikan" class="tab-pane in active">
          <?php echo $this->renderPartial('_non-racikan',
  array(
    'model'=>$model,
      'resepItem' => $resepItem,
  )
);?>
				</div>

				<div id="racikan" class="tab-pane">
          
<?php echo $this->renderPartial('_racikan',
  array(
    'model'=>$model,
      'resepRacikan' => $resepRacikan
  )
);?>
				</div>

			</div>
		</div>
	</div>
</div>
<?php 
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
   
$cs->registerScriptFile($baseUrl.'/assets/focus-next.js');

?>