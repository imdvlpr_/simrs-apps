 <?php
$this->breadcrumbs=array(
	array('name'=>'Tr Resep Kamar','url'=>array('trResepPasien/view','id'=>$_GET['trid'])),
	array('name'=>'Create'),
);

?>

<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>
<div class="row">
	<div class="col-xs-12">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>
