
<div class="row">
          <div class="col-xs-6">
					<form class="form-horizontal">
						<div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Nama Obat</strong></label>
					    <div class="col-sm-9">
					      <input type="text"  placeholder="Ketik Kode/Nama Obat" name="nama_obat" class="input-sm" id="nama_obat"/>
					      <input type="hidden" name="kode_obat" class="input-sm" id="kode_obat"/>
					      <input type="hidden" name="harga_obat" class="input-sm" id="harga_obat"/>
                <strong>Stok Awal</strong>
                <input type="text" name="stok_awal" class="input-sm" id="stok_awal"/>
					    </div>
					  </div>
           
					 <!--  <div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Kekuatan</strong></label>
					    <div class="col-sm-9">
					      <input type="text" name="kekuatan" class="input-sm" id="kekuatan"/>
					      <label id="satuan_kekuatan"></label>
					    </div>
					  </div> -->
					  <!-- <div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Dosis permintaan</strong></label>
					    <div class="col-sm-9">
					      <input type="text" name="dosis_minta" class="input-sm" id="dosis_minta"/>
					    </div>
					  </div> -->
					   <div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Signa</strong></label>
					    <div class="col-sm-9">
					      <input type="text" name="signa1" class="input-sm" size="5" id="signa1"/>
					      <b>X</b>
					      <input type="text" name="signa2" class="input-sm" size="5" id="signa2"/>
					      <b>Hari</b>
					      <input type="text" name="hari" class="input-sm" size="5" id="hari"/>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Jml ke Apotik</strong></label>
					    <div class="col-sm-9">
					      <input type="text" name="jml_ke_apotik" class="input-sm" id="jml_ke_apotik"/>
					      <b>Jml ke BPJS</b>
					      <input type="text" name="jml_ke_bpjs" class="input-sm" id="jml_ke_bpjs"/>
					    </div>
					  </div>
					   <div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Stok Akhir</strong></label>
					    <div class="col-sm-9">
					     
					      <input type="text" name="stok_akhir" class="input-sm" id="stok_akhir"/>
                <strong>Aturan</strong>
                <input type="text" name="aturan" class="input-sm" id="aturan"/>
					    </div>
					  </div>
        
					  <div class="clearfix form-actions">
				        <div class="col-md-offset-3 col-md-9">
				          <button class="btn btn-info" type="button" id="btn-simpan">
				            <i class="ace-icon fa fa-check bigger-110"></i>
				            Simpan
				          </button>
                  <div id="alert"></div>
				        
				        </div>
				      </div>
					</form>
        </div>
					
          <div class="col-xs-6">
<?php 


 
$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabel-resep-kamar-obat',
  'dataProvider'=>$resepItem->search($model->id),

  // 'filter'=>$model,
  'columns'=>array(
    array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            
        ),

    
    [
    	'header' => 'Obat',
    	'value' => function($data){
    		return $data->obat->nama_barang;
    	}
    ],
     'dosis_permintaan',
    [
    	'header' => 'Signa',
    	'value' => function($data){
    		return $data->signa1.' x '.$data->signa2;
    	}
    ],
    'aturan',
    [
      'header' => 'Qty',
      'value' => '$data->jumlah'
    ],

      array(
          'name'=>'Nilai',
          'type'=>'raw',
          'value' => 'Yii::app()->helper->formatRupiah($data->harga)',
          'footer'=>'<div style="text-align:right;width:100%;font-weight:bold">'.Yii::app()->helper->formatRupiah($model->sumObatItem).'</div>',
          'htmlOptions'=>array('style' => 'text-align: right;'),
          
    ),
    array(
      'class'=>'CButtonColumn',
      'template'=>'{print} {delete}',
      'buttons' => array(
          'print' => array(
            
            'label' => '<b class="fa fa-print"></b>',
            'options' => ['title'=>'Cetak Aturan','target'=>'_blank'],
            'url'=>'Yii::app()->createUrl("trResepKamarItem/print/", array("id"=>$data->id))',
          ),
          'delete' => array(
            'url'=>'Yii::app()->createUrl("trResepKamarItem/delete/", array("id"=>$data->id))',   
          ),
      ),
      
    ),

  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),

  'itemsCssClass'=>'table  table-bordered table-hover',
  'summaryCssClass'=>'table-message-info',
  
  'summaryText'=>'',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
  
));
?>    
</div>
  </div>


<script type="text/javascript">
  

function updateTable(){
  $('#tabel-resep-kamar-obat').yiiGridView.update('tabel-resep-kamar-obat', {

      url:'<?=Yii::app()->createUrl('trResepKamar/view',array('id'=>$model->id));?>&filter='+$('#search').val(),
     
  });
}

function saveObat(){
  var obats = [];

  var i = 0;

  
  var signa1 = eval($('#signa1').val());
  var signa2 = eval($('#signa2').val());
  var hari = eval($('#hari').val());
  obat = new Object;
  obat.obat_id = $('#kode_obat').val();
  obat.harga = $('#harga_obat').val();
  obat.jml_stok = signa1 * signa2 * hari;//$('#jml_stok').val();
  obat.signa1 = $('#signa1').val();
  obat.signa2 = $('#signa2').val();
  obat.hari = $('#hari').val();
  obat.jml_ke_apotik = $('#jml_ke_apotik').val();
  obat.jml_ke_bpjs = $('#jml_ke_bpjs').val();
  obat.dosis_permintaan = 0;//$('#dosis_minta').val();
  obat.aturan = $('#aturan').val();

  $.ajax({
    type : 'post',
    url : '<?=Yii::app()->createUrl('trResepKamar/AjaxSimpanResepItem');?>',
    
    data : {dataObat:obat,rid:<?=$model->id;?>},
    beforeSend : function(){
      $('#loading').show();
    },
    success : function(res){
      var res = $.parseJSON(res);
     
      $('#alert').fadeIn(100);
      $('#alert').html('<div class="alert alert-'+res.shortmsg+'">'+res.message+'</div>');
      $('#loading').hide();
      
      if(res.shortmsg=='success'){
        $('#alert').fadeOut(500);
        updateTable();

        $('#nama_obat').focus();
      }

      
    },
  });
}
$(document).ready(function(){
  $('#nama_obat').focus();

  $('#btn-simpan').on('click',function(){

      saveObat();
      
      return false;
  });


  $('#jml_ke_bpjs, #stok_awal, #signa1, #signa2, #hari').on('keydown',function(e){
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {
      e.preventDefault();
      var signa1 = eval($('#signa1').val());
      var signa2 = eval($('#signa2').val());
      var hari = eval($('#hari').val());
      var stokAwal = eval($('#stok_awal').val());
      var stokAkhir = stokAwal - (signa1 * signa2 * hari);
      var stokAkhir = isNaN(stokAkhir) ? 0 : stokAkhir;
      
      var jml_ke_apotik = eval($('#jml_ke_apotik').val());
      var jml_ke_bpjs = signa1 * signa2 * hari - jml_ke_apotik;
      jml_ke_bpjs = isNaN(jml_ke_bpjs) ? 0 : jml_ke_bpjs;
      $('#stok_akhir').val(stokAkhir);

      $('#jml_ke_bpjs').val(jml_ke_bpjs);  
    }
  });

});



$(document).on('keydown','input#jml_ke_apotik', function(e) {
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {
      e.preventDefault();
      var signa1 = eval($('#signa1').val());
      var signa2 = eval($('#signa2').val());
      var hari = eval($('#hari').val());
      var jml_ke_apotik = eval($(this).val());
      var jml_ke_bpjs = signa1 * signa2 * hari - jml_ke_apotik;
      jml_ke_bpjs = isNaN(jml_ke_bpjs) ? 0 : jml_ke_bpjs;
      $('#jml_ke_bpjs').val(jml_ke_bpjs);  
    }
});



$(document).on("keydown.autocomplete","#nama_obat",function(e){
  $('#nama_obat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#kode_obat').val(ui.item.id);

        // $('#kekuatan').val(ui.item.kekuatan);
        $('#satuan_kekuatan').html(ui.item.satuan_kekuatan);
        $('#harga_obat').val(ui.item.hj);
        $('#stok_awal').val(ui.item.stok);
        // $('#satuan_obat').val(ui.item.satuan);
        // $('#jumlah_obat').focus();
                
      },
      focus: function (event, ui) {
        $('#kode_obat').val(ui.item.id);
         // $('#kekuatan').val(ui.item.kekuatan);
        $('#satuan_kekuatan').html(ui.item.satuan_kekuatan);
        $('#harga_obat').val(ui.item.hj);
        $('#stok_awal').val(ui.item.stok);
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetObatLikeParam');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});
</script>
