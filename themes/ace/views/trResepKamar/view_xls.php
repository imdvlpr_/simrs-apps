<?php
/* @var $this TrResepKamarController */
/* @var $model TrResepKamar */

$filename = 'laporan_resep_kamar.xls';

header('Content-type: application/excel');
header('Content-Disposition: attachment; filename='.$filename);

?>


<div class="row">
	<div class="col-xs-12">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="row">
				<div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
					<b>Info Pasien</b>
				</div>
			</div>

			<div>
				<ul class="list-unstyled spaced">
					<li>
						<i class="ace-icon fa fa-caret-right blue"></i>
						Nama : <?=$dataDaftar->noMedrec->NAMA;?> / <?=$dataDaftar->noMedrec->NoMedrec;?>
					</li>

					<li>
						<i class="ace-icon fa fa-caret-right blue"></i>
						JK : <?=$dataDaftar->noMedrec->JENSKEL == 'L' ? 'Laki-laki' : 'Perempuan';?>
					</li>

					<li>
						<i class="ace-icon fa fa-caret-right blue"></i>
						Alamat : <?=$dataDaftar->noMedrec->ALAMAT;?>
					</li>

					
					
				</ul>
			</div>
		</div><!-- /.col -->
		<?php 
		$idx = 0;
		$total = 0;
		foreach($listResep as $resep)
		{
			$idx++;
			$subtotal = $resep->sumObatItem;
			$subtotal_racikan = $resep->sumObatRacikan;
			$total += $subtotal + $subtotal_racikan;
		?>

						<h3 class="widget-title grey lighter">
							<i class="ace-icon fa fa-leaf green"></i>
							Resep <?=$idx;?>
						</h3>
						<table width="100%">
							<tr><td>
							<span class="invoice-info-label">No.Resep:</span>
						</td>
						<td>
							<span class="red"><?=$resep->no_resep;?></span>
						</td>
							<td>
							<span class="invoice-info-label">Date:</span>
						</td>
							<td>
							<span class="blue"><?=$resep->tgl_resep;?></span>
						</td>
						</tr>
						<tr>
							<td>
							<span class="invoice-info-label">Dokter:</span></td>
							<td>
							<span class="red"><?=$resep->dokter->FULLNAME;?></span>
						</td>

							<td>
							<span class="invoice-info-label">Kamar:</span>
						</td>
							<td>
							<span class="blue"><?=$resep->kamar->nama_kamar.' / '.$resep->kamar->kelas->nama_kelas;?></span>
						</td>
					</tr>
						</table>



							<table width="100%" border="1">
								<tr><td valign="top" width="50%" style="text-align: center;">
									<b>Non Racikan</b>
								<table width="100%" border="1">
									<thead>
										<tr>
											<th class="center">#</th>
											<th>Obat</th>
											<th class="hidden-xs">Qty</th>
											<th class="hidden-480">Harga Satuan</th>
											<th>Harga Total</th>
										</tr>
									</thead>

									<tbody>
										<?php 
										$i = 0;

										foreach($resep->trResepKamarItems as $item)
										{

										$i++;
										?>
										<tr>
											<td class="center"><?=($i);?></td>

											<td>
												<?=$item->obat->nama_barang;?>
											</td>
											<td class="hidden-xs">
												<?=$item->jumlah;?>
											</td>
											<td class="hidden-480"> <?=$item->obat->hj;?> </td>
											<td style="text-align: right"><?=Yii::app()->helper->formatRupiah($item->harga);?></td>
										</tr>
										<?php 
										}
										?>
										<tr>
											<td colspan="4" style="text-align: right">Subtotal</td>
											<td style="text-align: right"><b><?=Yii::app()->helper->formatRupiah($subtotal);?></b></td>
										</tr>
									</tbody>
									
								</table>
								</td>
								<td valign="top" width="50%" style="text-align: center;"> 
									<b>Racikan</b>
								<table width="100%" border="1">
									<thead>
										<tr>
											<th class="center">#</th>
											<th>Obat</th>
											<th class="hidden-xs">Qty</th>
											<th class="hidden-480">Harga Satuan</th>
											<th>Harga Total</th>
										</tr>
									</thead>

								
									<tbody>
										<?php 
										
										$i=0;
										foreach($resep->trResepKamarRacikans as $item)
										{
											$i++;
										?>
										<tr>
											<td class="center"><?=($i);?></td>

											<td>
												<?=$item->trObatRacikan->nama;?>
											</td>
											<td class="hidden-xs">
												<?=$item->jumlah;?>
											</td>
											<td class="hidden-480"> --- </td>
											<td style="text-align: right"><?=Yii::app()->helper->formatRupiah($item->harga);?></td>
										</tr>
										<?php 
										}
										?>
										<tr>
											<td colspan="4" style="text-align: right">Subtotal</td>
											<td style="text-align: right"><b><?=Yii::app()->helper->formatRupiah($subtotal_racikan);?></b></td>
										</tr>
									</tbody>
								</table>
							</td></tr></table>

						

		<?php 
		}
		?>
<table width="100%">
	<tr>
		<td colspan="10" style="text-align: right;" ><b>Total amount :
					<span class="red"><?=Yii::app()->helper->formatRupiah($total);?></span></b></td>
	</tr>
</table>