<div class="row">
            <div class="col-xs-6">
					<form class="form-horizontal">
						<div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Komposisi</strong></label>
					    <div class="col-sm-9">
                <?php 
                echo CHtml::dropDownList('komposisi','',[1=>'Paketan',2=>'Non-Paket'],['id'=>'komposisi']);
                ?>
                <span id="paketan">
                <strong>Paket Racikan</strong>
					      <input type="text" placeholder="Ketik Kode/Nama Racikan" name="nama_racikan" class="input-sm" id="nama_racikan"/>
                </span>
                <span id="non-paketan" style="display: none">
                <strong>Nama Obat</strong>
                <input type="text" placeholder="Ketik Kode/Nama Obat" name="nama_obat" class="input-sm" id="nama_obat_racik"/>
              </span>
					      <input type="hidden" name="kode_racikan" class="input-sm" id="kode_racikan"/>
					      <input type="hidden" name="harga_racikan" class="input-sm" id="harga_racikan"/>

					    </div>
					  </div>
					 <div class="form-group">
              <label class="col-sm-3 control-label no-padding-right"><strong>Kekuatan</strong></label>
              <div class="col-sm-9">
                <input type="text" name="kekuatan" class="input-sm" id="kekuatan"/>
               <strong>Dosis permintaan</strong>
                <input type="text" name="dosis_minta_2" class="input-sm" id="dosis_minta_2"/>
              </div>
            </div> 

				
					   <div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Signa</strong></label>
					    <div class="col-sm-9">
					      <input type="text" name="signa1_2" class="input-sm" size="5" id="signa1_2"/>
					      <b>X</b>
					      <input type="text" name="signa2_2" class="input-sm" size="5" id="signa2_2"/>
					      <b>Hari</b>
					      <input type="text" name="hari_2" class="input-sm" size="5" id="hari_2"/>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Jml ke Apotik</strong></label>
					    <div class="col-sm-9">
					      <input type="text" name="jml_ke_apotik_2" class="input-sm" id="jml_ke_apotik_2"/>
					      <b>Jml ke BPJS</b>
					      <input type="text" name="jml_ke_bpjs_2" class="input-sm" id="jml_ke_bpjs_2"/>
					    </div>
					  </div>
					   <div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right"><strong>Jml to Racikan</strong></label>
					    <div class="col-sm-9">
					      <input type="text" name="jml_racikan_2" class="input-sm" id="jml_racikan_2"/>
                
                 <b>Jml to Stok</b>
                <input type="text" name="jml_stok_2" class="input-sm" id="jml_stok_2"/>
					    </div>
					  </div>
             <div class="form-group">
              <label class="col-sm-3 control-label no-padding-right"><strong>Aturan</strong></label>
              <div class="col-sm-9">
               
                <input type="text" name="aturan_2" class="input-sm" id="aturan_2"/>
              </div>
            </div>
					  <div class="clearfix form-actions">
				        <div class="col-md-offset-3 col-md-9">
				          <button class="btn btn-info" type="button" id="btn-simpan_2">
				            <i class="ace-icon fa fa-check bigger-110"></i>
				            Simpan
				          </button>
                   <div id="alert2"></div>
				        
				        </div>
				      </div>
					</form>
					</div>
          <div class="col-xs-6">
<?php 


 
$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabel-resep-kamar-obat-racikan',
  'dataProvider'=>$resepRacikan->search($model->id),

  // 'filter'=>$model,
  'columns'=>array(
    array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            
        ),

    
    [
    	'header' => 'Obat',
    	'value' => function($data){
    		return $data->trObatRacikan->nama;
    	}
    ],
     'dosis_permintaan',
    [
    	'header' => 'Signa',
    	'value' => function($data){
    		return $data->signa1.' x '.$data->signa2;
    	}
    ],
    'aturan',
    [
      'header' => 'Qty',
      'value' => '$data->jumlah'
    ],

      array(
          'name'=>'Nilai',
          'type'=>'raw',
          'value' => 'Yii::app()->helper->formatRupiah($data->harga)',
          'footer'=>'<div style="text-align:right;width:100%;font-weight:bold">'.Yii::app()->helper->formatRupiah($model->sumObatRacikan).'</div>',
          'htmlOptions'=>array('style' => 'text-align: right;'),
          
    ),
    array(
      'class'=>'CButtonColumn',
      'template'=>'{print} {delete}',
      'buttons' => array(
         'print' => array(
            
            'label' => '<b class="fa fa-print"></b>',
            'options' => ['title'=>'Cetak Aturan','target'=>'_blank'],
            'url'=>'Yii::app()->createUrl("trResepKamarRacikan/print/", array("id"=>$data->id))',
          ),
          'delete' => array(
            'url'=>'Yii::app()->createUrl("trResepKamarRacikan/delete/", array("id"=>$data->id))',   
          ),
      ),
      
    ),

  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),

  'itemsCssClass'=>'table  table-bordered table-hover',
  'summaryCssClass'=>'table-message-info',
  
  'summaryText'=>'',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
  
));
?>    
</div></div>
<script type="text/javascript">
  
function updateTableRacikan(){
  $('#tabel-resep-kamar-obat-racikan').yiiGridView.update('tabel-resep-kamar-obat-racikan', {

      url:'<?=Yii::app()->createUrl('trResepKamar/view',array('id'=>$model->id));?>&filter='+$('#search').val(),
     
  });
}


function saveObatRacikan(){
  var obats = [];

  var i = 0;

  
  
  obat = new Object;
  obat.komposisi = $('#komposisi').val();
  obat.obat_id = $('#kode_racikan').val();
  obat.harga = $('#harga_racikan').val();
  obat.jml_stok = $('#jml_stok_2').val();
  obat.signa1 = $('#signa1_2').val();
  obat.signa2 = $('#signa2_2').val();
  obat.hari = $('#hari_2').val();
  obat.jml_ke_apotik = $('#jml_ke_apotik_2').val();
  obat.jml_ke_bpjs = $('#jml_ke_bpjs_2').val();
  obat.dosis_permintaan = $('#dosis_minta_2').val();
  obat.aturan = $('#aturan_2').val();
  
  $.ajax({
    type : 'post',
    url : '<?=Yii::app()->createUrl('trResepKamar/AjaxSimpanResepRacikan');?>',
    
    data : {dataObat:obat,rid:<?=$model->id;?>},
    beforeSend : function(){
      $('#loading').show();
    },
    success : function(res){
      // console.log(res);
      var res = $.parseJSON(res);
     
      $('#alert2').fadeIn(100);
      $('#alert2').html('<div class="alert alert-'+res.shortmsg+'">'+res.message+'</div>');
      $('#loading').hide();
      
      if(res.shortmsg=='success'){
        $('#alert2').fadeOut(50);
        updateTableRacikan();
      }

      
    },
  });
}


$(document).ready(function(){

  $('#komposisi').change(function(){
    var val = $(this).val();

    if(val == 1){
      $('#paketan').show();
      $('#non-paketan').hide();
    }

    else{
      $('#paketan').hide();
      $('#non-paketan').show();
    }
  });

  $('#btn-simpan_2').on('click',function(){

      saveObatRacikan();
      $('#nama_racikan').focus();
      return false;
  });


  $('#jml_ke_bpjs_2, #signa1_2, #signa2_2, #hari_2, #jml_ke_apotik_2').on('keydown',function(e){
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {
      e.preventDefault();
      var signa1 = eval($('#signa1_2').val());
      var signa2 = eval($('#signa2_2').val());
      var hari = eval($('#hari_2').val());
      var jumlah = signa1 * signa2 * hari;
      var dosis = eval($('#dosis_minta_2').val());
      var kekuatan = eval($('#kekuatan').val());
      kekuatan = kekuatan == 0 ? 1 : kekuatan;
      var jr = eval(jumlah * dosis / kekuatan);
      jr = isNaN(jr) ? 0 : jr;
      $('#jml_racikan_2').val(jr);
      $('#jml_stok_2').val(Math.ceil(jr));
      var jml_ke_apotik = eval($('#jml_ke_apotik_2').val());
      var jml_ke_bpjs = signa1 * signa2 * hari - jml_ke_apotik;
      jml_ke_bpjs = isNaN(jml_ke_bpjs) ? 0 : jml_ke_bpjs;
       $('#jml_ke_bpjs_2').val(jml_ke_bpjs);  
      
    }
  });
});




$(document).on("keydown.autocomplete","#nama_racikan",function(e){
  $('#nama_racikan').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#kode_racikan').val(ui.item.id);

        $('#kekuatan').val(ui.item.kekuatan);
        $('#harga_racikan').val(ui.item.hj);
        // $('#satuan_obat').val(ui.item.satuan);
        // $('#jumlah_obat').focus();
                
      },
      focus: function (event, ui) {
         $('#kode_racikan').val(ui.item.id);
         $('#kekuatan').val(ui.item.kekuatan);
        $('#harga_racikan').val(ui.item.hj);
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetRacikanLikeParam');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});


$(document).on("keydown.autocomplete","#nama_obat_racik",function(e){
  $('#nama_obat_racik').autocomplete({
      minLength:1,
      select:function(event, ui){

        $('#kode_racikan').val(ui.item.id);
        $('#kekuatan').val(ui.item.kekuatan);
        $('#harga_racikan').val(ui.item.hj);
        // $('#satuan_obat').val(ui.item.satuan);
        // $('#jumlah_obat').focus();
                
      },
      focus: function (event, ui) {
        $('#kode_racikan').val(ui.item.id);
        $('#kekuatan').val(ui.item.kekuatan);
        $('#harga_racikan').val(ui.item.hj);
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetObatLikeParam');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

</script>