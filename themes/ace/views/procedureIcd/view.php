<?php
/* @var $this ProcedureIcdController */
/* @var $model ProcedureIcd */

$this->breadcrumbs=array(
	array('name'=>'Procedure Icd','url'=>array('admin')),
	array('name'=>'Procedure Icd'),
);

$this->menu=array(
	array('label'=>'List ProcedureIcd', 'url'=>array('index')),
	array('label'=>'Create ProcedureIcd', 'url'=>array('create')),
	array('label'=>'Update ProcedureIcd', 'url'=>array('update', 'id'=>$model->code)),
	array('label'=>'Delete ProcedureIcd', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->code),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProcedureIcd', 'url'=>array('admin')),
);
?>

<h1>View ProcedureIcd #<?php echo $model->code; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code',
		'description',
		'short_description',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
