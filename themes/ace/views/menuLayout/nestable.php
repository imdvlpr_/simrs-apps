<?php
/* @var $this MenuLayoutController */
/* @var $model MenuLayout */

$this->breadcrumbs=array(
	array('name'=>'Menu Layout','url'=>array('admin')),
	array('name'=>'Menu Layout'),
);
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();



?>

<h1>View MenuLayout </h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<button class="btn btn-info" data-loading-text="Loading..."  id="btn-update">Update Menu</button>
<img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
<div class="row">
	<div class="col-xs-12">
		<div class="dd" id="nestable">
			<ol class="dd-list">
				<?php 
				foreach($models as $m1)
				{
				?>
				<li class="dd-item" data-id="<?=$m1->id;?>" data-parent="<?=$m1->parent;?>">
					<div class="dd-handle"><?=$m1->nama;?>
							<a class="blue" href="#">
								<i class="pull-right ace-icon fa fa-pencil bigger-130"></i>
							</a>

							<!-- <a class="red" href="#">
								<i class="ace-icon fa fa-trash-o bigger-130"></i>
							</a> -->
						
					</div>
					<?php 
					foreach($m1->menuLayouts as $m2)
					{
					?>
					<ol class="dd-list">
						<li class="dd-item" data-id="<?=$m2->id;?>" data-parent="<?=$m2->parent;?>">
							<div class="dd-handle">
								<?=$m2->nama;?>
								<a class="blue" href="#">
								<i class="pull-right ace-icon fa fa-pencil bigger-130"></i>
							</a>

							<?php 
							foreach($m2->menuLayouts as $m3)
							{
							?>
							<ol class="dd-list">
								<li class="dd-item" data-id="<?=$m3->id;?>" data-parent="<?=$m3->parent;?>">
									<div class="dd-handle">
										<?=$m3->nama;?>
										<a class="blue" href="#">
								<i class="pull-right ace-icon fa fa-pencil bigger-130"></i>
							</a>

									</div>
									<?php 
									foreach($m3->menuLayouts as $m4)
									{
									?>
									<ol class="dd-list">
										<li class="dd-item" data-id="<?=$m4->id;?>" data-parent="<?=$m4->parent;?>">
											<div class="dd-handle">
												<?=$m4->nama;?>
												<a class="blue" href="#">
								<i class="pull-right ace-icon fa fa-pencil bigger-130"></i>
							</a>

											</div>
										</li>
									</ol>
									<?php 
									}
									?>
								</li>
							</ol>
							<?php 
							}
							?>
						</li>
					</ol>
					<?php 
					}
					?>
				</li>
				<?php 
				}
				?>
			</ol>
		</div>
	</div>
</div>

<?php 
 $cs->registerScriptFile($baseUrl.'/assets/js/jquery.nestable.min.js',CClientScript::POS_END);
?>

<script type="text/javascript">
	jQuery(function($){
	
		$('.dd').nestable();

		$('.dd-handle a').on('mousedown', function(e){
			e.stopPropagation();
		});
		
		$('[data-rel="tooltip"]').tooltip();
		
		$('#btn-update').click(function(){
			let selector = $(this);
			var postData = $('.dd').nestable('serialize');
			$.ajax({
				url : '<?=Yii::app()->createUrl('menuLayout/ajaxBulkSave');?>',
				type : 'POST',
				data : {
					item : postData
				},
				beforeSend : function(){
					selector.button('loading');
				},
				error : function (e){
					selector.button('loading');
					console.log(e.responseText);
				},
				success : function(result){
					selector.button('reset');
					var res = $.parseJSON(result);

				}
			});
		});
	});
</script>