<?php
/* @var $this TrObatRacikanController */
/* @var $model TrObatRacikan */

$this->breadcrumbs=array(
	array('name'=>'Tr Obat Racikan','url'=>array('admin')),
	array('name'=>'Tr Obat Racikan'),
);

$this->menu=array(
	array('label'=>'List TrObatRacikan', 'url'=>array('index')),
	array('label'=>'Create TrObatRacikan', 'url'=>array('create')),
	array('label'=>'Update TrObatRacikan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrObatRacikan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrObatRacikan', 'url'=>array('admin')),
);
?>

<h1>View TrObatRacikan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		'kode',
		'nama',
	),
)); ?>

	</div>
</div>

<div class="row">
	<div class="col-xs-12">
<form class="form-horizontal">
	<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"><strong>Nama Obat</strong></label>
    <div class="col-sm-9">
      <input type="text"  placeholder="Ketik Kode/Nama Obat" name="nama_obat" class="input-sm" id="nama_obat"/>
      <b>Jml to Stok</b>
      <input type="text" name="qty" class="input-sm" id="qty"/>
      <button class="btn btn-info btn-sm" type="button" id="btn-simpan">
        <i class="ace-icon fa fa-check bigger-110"></i>
        Simpan
      </button>
      <input type="hidden" name="kode_obat" class="input-sm" id="kode_obat"/>
      <input type="hidden" name="harga_obat" class="input-sm" id="harga_obat"/>
    </div>
  </div>

 
</form>
<?php 


 
$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabel-obat-racikan',
  'dataProvider'=>$itemRacikan->search($model->id),

  // 'filter'=>$model,
  'columns'=>array(
    array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            
        ),

    
    [
    	'header' => 'Obat',
    	'value' => function($data){
    		return $data->obat->nama_barang;
    	}
    ],
    array(
          'name'=>'Qty',
          'type'=>'raw',
          'value' => '$data->qty',
          'footer'=>'<div style="text-align:right;width:100%;font-weight:bold">'.Yii::app()->helper->formatRupiah($model->sumObatQty).'</div>',
          'htmlOptions'=>array('style' => 'text-align: right;'),
          
    ),
   	 array(
          'name'=>'Nilai',
          'type'=>'raw',
          'value' => 'Yii::app()->helper->formatRupiah($data->harga)',
          'footer'=>'<div style="text-align:right;width:100%;font-weight:bold">'.Yii::app()->helper->formatRupiah($model->sumObatItem).'</div>',
          'htmlOptions'=>array('style' => 'text-align: right;'),
          
    ),
    array(
      'class'=>'CButtonColumn',
      'template'=>' {delete}',
      'buttons' => array(
          // 'update' => array(
            
          //   'type' => 'raw',
          //   'click' => new CJavaScriptExpression('function(e) {
          //         e.preventDefault();
          //         jQuery.yii.submitForm(document.body, $(this).attr("href"), {});
          //         return false;
          //     }'),
          // ),
          'delete' => array(
            'url'=>'Yii::app()->createUrl("trObatRacikanItem/delete/", array("id"=>$data->id))',   
          ),
      ),
      
    ),

  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),

  'itemsCssClass'=>'table  table-bordered table-hover',
  'summaryCssClass'=>'table-message-info',
  
  'summaryText'=>'',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
  
));
?>    
	</div>
</div>

<?php 
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
   
$cs->registerScriptFile($baseUrl.'/assets/focus-next.js');

?>
<script type="text/javascript">
	

function updateTable(){
  $('#tabel-obat-racikan').yiiGridView.update('tabel-obat-racikan', {

      url:'<?=Yii::app()->createUrl('trObatRacikan/view',array('id'=>$model->id));?>&filter='+$('#search').val(),
     
  });
}

function saveObat(){
  var obats = [];

  var i = 0;

  
  
  obat = new Object;
  obat.obat_id = $('#kode_obat').val();
  obat.harga = $('#harga_obat').val();
  obat.qty = $('#qty').val();

  $.ajax({
    type : 'post',
    url : '<?=Yii::app()->createUrl('trObatRacikan/AjaxSimpanResepRacikan');?>',
    
    data : {dataObat:obat,rid:<?=$model->id;?>},
    beforeSend : function(){
      $('#loading').show();
    },
    success : function(res){
      var res = $.parseJSON(res);
     
      $('#alert').fadeIn(100);
      $('#alert').html('<div class="alert alert-'+res.shortmsg+'">'+res.message+'</div>');
      $('#loading').hide();
      
      if(res.shortmsg=='success'){
        $('#alert').fadeOut(2000);
        updateTable();
      }

      
    },
  });
}
$(document).ready(function(){
	$('#nama_obat').focus();

	 $('#btn-simpan').on('click',function(){

	    saveObat();
	    $('#nama_obat').focus();
	    return false;
	  });

});

$(document).on("keydown.autocomplete","#nama_obat",function(e){
  $('#nama_obat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $('#kode_obat').val(ui.item.id);
        $('#harga_obat').val(ui.item.hj);
                
      },
      focus: function (event, ui) {
         $('#kode_obat').val(ui.item.id);
        $('#harga_obat').val(ui.item.hj);
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetObatLikeParam');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});
</script>