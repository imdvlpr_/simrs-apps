 <?php
$this->breadcrumbs=array(
	array('name'=>'Tr Resep Pasien','url'=>array('admin')),
	array('name'=>'Manage'),
);

?>
<h1>Manage Resep Pasien</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#tr-resep-pasien-grid').yiiGridView.update('tr-resep-pasien-grid', {
			    url:'<?php echo Yii::app()->createUrl("TrResepPasien/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                        <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('TrResepPasien/create'),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('TrResepPasien[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('TrResepPasien[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'tr-resep-pasien-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		// 'nodaftar_id',
		'nodaftar.TGLDAFTAR',
		'pasien_id',
		'pasien.NAMA',
		'pasien.ALAMAT',
		'pasien.JENSKEL',
		// 'created_at',
		// 'updated_at',
			[
      'type'=>'raw',
      'header' => 'Actions',
      'value' => function($data){

        $html = '<div class="btn-group">
  <button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle">
    Tindakan
    <span class="ace-icon fa fa-caret-down icon-on-right"></span>
  </button>

  <ul class="dropdown-menu dropdown-info dropdown-menu-right">
     <li>
      <a href="'.Yii::app()->createUrl("trResepPasien/view/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-eye bigger-120"></i> Lihat Rincian</a>
    </li>
     <li>
      <a href="'.Yii::app()->createUrl("trResepKamar/create/", array("trid"=>$data->id)).'"><i class="ace-icon fa fa-plus bigger-120"></i> Input Resep Baru</a>
    </li>
    <li class="divider"></li>
     <li>
      <a href="'.Yii::app()->createUrl("trResepPasien/rekap/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-book bigger-120"></i> Rekap Resep</a>
    </li>
    
    

    
  </ul>
</div>';

return $html;
      }
    ],
		array(
			'class'=>'CButtonColumn',
			'template' => '{delete}'
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

