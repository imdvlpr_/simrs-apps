<?php
/* @var $this TrResepPasienController */
/* @var $model TrResepPasien */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'registrasi-pasien-lama-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); 

?>                           <!-- block -->
    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>         

<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>             
       
      <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">No RM</label>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'pasien_id'); ?>
		<?php echo $form->error($model,'pasien_id'); ?>
        </div>
      </div>
       <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-search bigger-110"></i>
            Cari Pasien
          </button>

        
        </div>
      </div>

  <?php $this->endWidget();?>

</div>
   </div>
        </div>
<script type="text/javascript">

  $('#cari_no_rm').focus();

  $(document).on('keydown','input#cari_no_rm', function(e) {
      var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      

      if (e.keyCode == 13) {
          $('form').submit();
      }
    
  });
</script>
  