<?php
/* @var $this TrResepPasienController */
/* @var $model TrResepPasien */

$this->breadcrumbs=array(
	array('name'=>'Tr Resep Pasien','url'=>array('admin')),
	array('name'=>'Tr Resep Pasien'),
);

$this->menu=array(
	array('label'=>'List TrResepPasien', 'url'=>array('index')),
	array('label'=>'Create TrResepPasien', 'url'=>array('create')),
	array('label'=>'Update TrResepPasien', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrResepPasien', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrResepPasien', 'url'=>array('admin')),
);
?>

<h1>View Resep Pasien #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		'nodaftar.TGLDAFTAR',
		'pasien.NoMedrec',
		'pasien.NAMA',
		'pasien.ALAMAT',
		'pasien.JENSKEL',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>

<div class="row">
    <div class="col-xs-12">
        
            <?php    
    //         foreach(Yii::app()->user->getFlashes() as $key => $message) {
    //     echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    // }
?>
<div class="pull-left"> <?php	
// echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Tambah Baru',array('TrResepKamar/create','trid'=>$model->id),array('class'=>'btn btn-success'));
?>
&nbsp;
<?php
 // echo CHtml::link('<i class="fa fa-print"></i>&nbsp;Ekspor Laporan',array('laporan/resep'),array('class'=>'btn btn-info'));
?>
</div>

<div class="row">

<div class="col-xs-12">
<table class="table">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Qty</th>
			<th>Harga</th>	
		</tr>	
	</thead>
	<tbody>
		<?php
		$total = 0; 
		$i = 0;
		foreach($resepKamar as $item)
		{
			foreach($item->trResepKamarItems as $nr)
			{
				$i++;
				$total += $nr->harga;
		?>
		<tr>
			<td><?=$i;?></td>
			<td><?=$nr->obat->nama_barang;?></td>
			<td><?=$nr->jumlah;?></td>
			<td><?=Yii::app()->helper->formatRupiah($nr->harga);?></td>	
		</tr>	
		<?php
			} 
		}
		?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3" style="text-align: right">Total</td>
			<td><?=Yii::app()->helper->formatRupiah($total);?></td>	
		</tr>	
	</tfoot>
</table>
</div>
</div>
	</div>
</div>