<?php
/* @var $this TrResepPasienController */
/* @var $model TrResepPasien */

$this->breadcrumbs=array(
	array('name'=>'Tr Resep Pasien','url'=>array('admin')),
	array('name'=>'Tr Resep Pasien'),
);

$this->menu=array(
	array('label'=>'List TrResepPasien', 'url'=>array('index')),
	array('label'=>'Create TrResepPasien', 'url'=>array('create')),
	array('label'=>'Update TrResepPasien', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrResepPasien', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrResepPasien', 'url'=>array('admin')),
);
?>

<h1>View Resep Pasien #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		'nodaftar.TGLDAFTAR',
		'pasien.NoMedrec',
		'pasien.NAMA',
		'pasien.ALAMAT',
		'pasien.JENSKEL',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#tr-resep-kamar-grid').yiiGridView.update('tr-resep-kamar-grid', {
			    url:'<?php echo Yii::app()->createUrl("TrResepKamar/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="pull-left"> <?php	echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Tambah Baru',array('TrResepKamar/create','trid'=>$model->id),array('class'=>'btn btn-success'));
?>
&nbsp;
<?php echo CHtml::link('<i class="fa fa-print"></i>&nbsp;Ekspor Laporan',array('laporan/resep'),array('class'=>'btn btn-info'));
?>
&nbsp;
<?php echo CHtml::link('<i class="fa fa-print"></i>&nbsp;Cetak Rincian',array('TrResepKamar/print'),array('class'=>'btn btn-info'));
?>
</div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('TrResepKamar[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('TrResepKamar[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'tr-resep-kamar-grid',
	'dataProvider'=>$resepKamar->search($id),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		
		
		'dokter.FULLNAME',
		
		'tgl_resep',
		[
			'header' => 'Kamar',
			'value' => function($data){
				return $data->kamar->nama_kamar.' / '.$data->kamar->kelas->nama_kelas;
			}
		],
		/*
		'tgl_resep',
		'kamar_id',
		'pasien_id',
		*/
		array(
      'type'=>'raw',
      'header' => 'Actions',
      'value' => function($data){

        $html = '<div class="btn-group">
  <button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle">
    Tindakan
    <span class="ace-icon fa fa-caret-down icon-on-right"></span>
  </button>

  <ul class="dropdown-menu dropdown-info dropdown-menu-right">
     <li>
      <a href="'.Yii::app()->createUrl("trResepKamar/input/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-plus bigger-120"></i> Input Obat</a>
    </li>
    
    

    
  </ul>
</div>';

return $html;
      }
    ),
		array(

			'class'=>'CButtonColumn',
			 'template' => '{delete}'
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>