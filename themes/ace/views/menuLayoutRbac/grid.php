<?php
/* @var $this MenuLayoutRbacController */
/* @var $model MenuLayoutRbac */

$this->breadcrumbs=array(
	array('name'=>'Menu Layout Rbac','url'=>array('admin')),
	array('name'=>'Menu Layout Mapping','url'=>array('grid')),
);

?>
<style type="text/css">
	/* RADIOS & CHECKBOXES STYLES */
/* SOURCE: https://gist.github.com/dciccale/1367918 */
/* No more blue blur border */
input {
  outline: none;
}

/* base styles */
input[type="checkbox"] {
    height: 20px;
    width: 20px;
    vertical-align: middle;
    margin: 0 0.4em 0.4em 0;
    background: rgba(255, 255, 255, 1);
    border: 1px solid #AAAAAA;
    -webkit-appearance: none;
}

/* border radius for radio*/


/* border radius for checkbox */
input[type="checkbox"] {
    border-radius: 2px;
}

/* hover state */
input[type="checkbox"]:not(:disabled):hover {
    border: 1px solid rgba(58, 197, 201, 1);
}

/* active state */
input[type="checkbox"]:active:not(:disabled) {
    border: 1px solid rgba(58, 197, 201, 1);
}

/* input checked border color */

input[type="checkbox"]:checked {
    border: 1px solid rgba(58, 197, 201, 1);
}

input[type="checkbox"]:checked:not(:disabled) {
    background: rgba(58, 197, 201, 1);
}


/* checkbox checked */
input[type="checkbox"]:checked:before {
    font-weight: bold;
    color: white;
    content: '\2713';
    margin-left: 2px;
    font-size: 14px;
}


</style>
<h1>Role Mapping</h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpendaftaran-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>
<div class="row">
	<div class="col-xs-12">

		<table class="table table-striped">
			<thead>
			<tr>
				<th>Menu</th>
				<?php 
				foreach($listRole as $role)
				{
					echo '<th style="text-align:center">'.$role->NAMA_LEVEL.'<br><input type="checkbox" class="role" data-item="'.$role->KODE_LEVEL.'"></th>';
				}
				?>
			</tr>
		</thead>
		<tbody>
			<?php 
			$i =0;
			$j = 1;
			$k = 0;
			foreach($listMenu as $menu)
			{


			?>
			<tr>
				<td><?php
				$m = '';
				if(!empty($menu->parent0)){
					
					echo ' - - - ';

					if(!empty($menu->parent0->parent0)){
						$k++;
						echo ' - - - ';
						
						echo $menu->parent0->parent0->nama.' > ';
					}	
					else
					{
						$j++;
						$k=0;
					}

					echo $menu->parent0->nama.' > ';
				}

				else{
					$j=1;
					$i++;
					;
				}

				echo ' '.$menu->nama;



				?>
					
				</td>
				<?php 
				foreach($listRole as $role)
				{

					$checked = $values[$menu->id][$role->KODE_LEVEL] == 1 ? 'checked' : '';

					echo '<td width="100px" style="text-align:center"><label for="ch_'.$menu->id.'_'.$role->KODE_LEVEL.'"><input type="checkbox" '.$checked.' class="role_'.$role->KODE_LEVEL.'" name="ch_'.$menu->id.'_'.$role->KODE_LEVEL.'" id="ch_'.$menu->id.'_'.$role->KODE_LEVEL.'"> </label></td>';
				}
				?>
			</tr>
			<?php 
			}
			?>
		</tbody>
		</table>		
	</div>
</div>
	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        	<input type="hidden" value="1" name="flag"/>
		<button class="btn btn-info" type="submit" name="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('.role').change(function(){

			var value = this.checked;
			var val = $(this).attr('data-item');
			$('.role_'+val).each(function(){
				$(this).prop('checked',value);
			});
		});
	});
</script>