 <?php
$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Rjalan','url'=>array('index')),
	array('name'=>'Manage'),
);

?>
<h1>Data Kunjungan Rawat Jalan</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size,#kode_poli').change(function(){
			$('#bpendaftaran-rjalan-grid').yiiGridView.update('bpendaftaran-rjalan-grid', {
			    url:'<?php echo Yii::app()->createUrl("BPendaftaranRjalan/index"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()+'&search_by='+$('#search_by').val()+'&kode_poli='+$('#kode_poli').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                       <!--  <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('BPendaftaranRjalan/create'),array('class'=>'btn btn-success'));
?></div> -->


<div class="pull-right">Data per halaman
<?php echo CHtml::dropDownList('BPendaftaranRjalan[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
?>
&nbsp;Poli 
<?php echo CHtml::dropDownList('BPendaftaranRjalan[KodePoli]',isset($_GET['kode_poli'])?$_GET['kode_poli']:'',CHtml::listData(Unit::model()->findAllbyAttributes(['unit_tipe'=>2],['order'=>'NamaUnit ASC']),'KodeUnit','NamaUnit'),array('id'=>'kode_poli','empty'=>'- Pilih -')); 
?>
Cari berdasar
                            <?php 
$search_by = array(
  1=>'No RM',2=>'Nama Pasien'
);
echo CHtml::dropDownList('BPendaftaranRjalan[SEARCH_BY]',isset($_GET['search_by'])?$_GET['search_by']:'',$search_by,array('id'=>'search_by','class'=>'input-small')); 

?> 
<?php        echo CHtml::textField('BPendaftaranRjalan[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'bpendaftaran-rjalan-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		
		// 'NoDaftar',
		[
			'header' => 'No RM',
			'value' => function($data){
				return $data->noDaftar->NoMedrec;
			}
		],
		[
			'header' => 'Pasien',
			'value' => function($data){
				return $data->noDaftar->noMedrec->NAMA;
			}
		],
		[
			'header' => 'Jenis Px',
			'value' => function($data){
				return $data->noDaftar->kodeGol->NamaGol;
			}
		],
		[
			'header' => 'Tgl Daftar',
			'value' => function($data){
				return $data->noDaftar->TGLDAFTAR;
			}
		],
		[
			'header' => 'Jam Daftar',
			'value' => function($data){
				return $data->noDaftar->JamDaftar;
			}
		],
		[
			'header' => 'Poli',
			'value' => function($data){
				return $data->unit->NamaUnit;
			}
		],
		
		[
			'header' => 'Ket Pulang',
			'value' => function($data){
				return $data->KetPlg;
			}
		],
		
		[
			'header' => 'Kunjungan Ke',
			'value' => function($data){
				return $data->noDaftar->KunjunganKe;
			}
		],
		
		// 'KodeTdkLanjut',
		[
			'header' => 'Cara Masuk',
			'value' => function($data){
				return $data->noDaftar->kodeMasuk->NamaMasuk;
			}
		],

		// 'KetTdkL1',
		// 'KetTdkL2',
		// 'KetTdkL3',
		// 'KetTdkL4',
		// 'KetTdkL5',
		
		'PostMRS',
		array(
      'type'=>'raw',
      'header' => 'Actions',
      'value' => function($data){

        $html = '<div class="btn-group">
		  <button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle">
		    Tindakan
		    <span class="ace-icon fa fa-caret-down icon-on-right"></span>
		  </button>

		  <ul class="dropdown-menu dropdown-info dropdown-menu-right">
		    <li>
		      <a href="'.Yii::app()->createUrl("trPendaftaranRjalan/update/", ["id"=>$data->NoDaftar]).'"><i class="ace-icon fa fa-pencil-square-o bigger-120"></i> Update Data</a>
		    </li>

		    
		     <li class="divider"></li>

		     <li>
		      <a href="'.Yii::app()->createUrl("bPendaftaranRjalanTindakan/create/", ["id"=>$data->Id]).'"><i class="ace-icon fa fa-pencil bigger-120"></i> Input Tindakan</a>
		    </li>
		      <li>
		      <a href="'.Yii::app()->createUrl("bPendaftaranRjalanTindakan/index/", ["id"=>$data->Id]).'"><i class="ace-icon glyphicon glyphicon-list-alt  bigger-120"></i> Rincian Tindakan</a>
		    </li>
		  	<li class="divider"></li>
		    <li>
		      <a target="_blank" href="'.Yii::app()->createUrl("pasien/cetakEtiket/", array("id"=>$data->noDaftar->NoMedrec)).'"><i class="ace-icon fa fa-print bigger-120"></i> Cetak Etiket</a>
		    </li> 
		     <li>
		      <a target="_blank" href="'.Yii::app()->createUrl("pasien/cetakGelang/", array("id"=>$data->noDaftar->NoMedrec)).'"><i class="ace-icon fa fa-print bigger-120"></i> Cetak Gelang</a>
		    </li> 
		   
		  </ul>
		</div>';

		return $html;
		      }
		    ),
		// array(
		// 	'class'=>'CButtonColumn',
		// 	'template' => '{delete}',
			
			
		// ),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

