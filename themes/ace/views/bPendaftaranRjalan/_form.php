<?php
/* @var $this BPendaftaranRjalanController */
/* @var $model BPendaftaranRjalan */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpendaftaran-rjalan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'NoDaftar', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'NoDaftar'); ?>
		<?php echo $form->error($model,'NoDaftar'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodePoli', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'3')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodePoli'); ?>
		<?php echo $form->error($model,'KodePoli'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeSubPlg', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeSubPlg'); ?>
		<?php echo $form->error($model,'KodeSubPlg'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KetPlg', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KetPlg',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'KetPlg'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'UrutanPoli', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'6')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'UrutanPoli'); ?>
		<?php echo $form->error($model,'UrutanPoli'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KodeTdkLanjut', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'7')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KodeTdkLanjut'); ?>
		<?php echo $form->error($model,'KodeTdkLanjut'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'StatusKunj', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'8')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'StatusKunj'); ?>
		<?php echo $form->error($model,'StatusKunj'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KetTdkL1', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'9')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KetTdkL1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL1'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KetTdkL2', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'10')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KetTdkL2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL2'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KetTdkL3', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'11')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KetTdkL3',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL3'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KetTdkL4', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'12')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KetTdkL4',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL4'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'KetTdkL5', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'13')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'KetTdkL5',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'KetTdkL5'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'NoAntriPoli', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'14')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'NoAntriPoli'); ?>
		<?php echo $form->error($model,'NoAntriPoli'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'PostMRS', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'15')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'PostMRS',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'PostMRS'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>
