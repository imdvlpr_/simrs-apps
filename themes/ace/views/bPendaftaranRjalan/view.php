<?php
/* @var $this BPendaftaranRjalanController */
/* @var $model BPendaftaranRjalan */

$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Rjalan','url'=>array('admin')),
	array('name'=>'Bpendaftaran Rjalan'),
);

$this->menu=array(
	array('label'=>'List BPendaftaranRjalan', 'url'=>array('index')),
	array('label'=>'Create BPendaftaranRjalan', 'url'=>array('create')),
	array('label'=>'Update BPendaftaranRjalan', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Delete BPendaftaranRjalan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BPendaftaranRjalan', 'url'=>array('admin')),
);
?>

<h1>View BPendaftaranRjalan #<?php echo $model->Id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'NoDaftar',
		'KodePoli',
		'KodeSubPlg',
		'KetPlg',
		'UrutanPoli',
		'KodeTdkLanjut',
		'StatusKunj',
		'KetTdkL1',
		'KetTdkL2',
		'KetTdkL3',
		'KetTdkL4',
		'KetTdkL5',
		'NoAntriPoli',
		'PostMRS',
	),
)); ?>
	</div>
</div>
