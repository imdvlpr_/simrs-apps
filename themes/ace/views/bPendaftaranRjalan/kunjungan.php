 <?php
$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Rjalan','url'=>array('index')),
	array('name'=>'Manage'),
);

?>
<h1>Data Kunjungan Rawat Jalan</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size,#kode_poli').change(function(){
			$('#bpendaftaran-rjalan-grid').yiiGridView.update('bpendaftaran-rjalan-grid', {
			    url:'<?php echo Yii::app()->createUrl("BPendaftaranRjalan/kunjungan"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()+'&search_by='+$('#search_by').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
<?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                       <!--  <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('BPendaftaranRjalan/create'),array('class'=>'btn btn-success'));
?></div> -->


<div class="pull-right">Data per halaman
<?php echo CHtml::dropDownList('BPendaftaranRjalan[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
?>
Cari berdasar
                            <?php 
$search_by = array(
  1=>'No RM',2=>'Nama Pasien'
);
echo CHtml::dropDownList('BPendaftaranRjalan[SEARCH_BY]',isset($_GET['search_by'])?$_GET['search_by']:'',$search_by,array('id'=>'search_by','class'=>'input-small')); 

?> 
<?php        echo CHtml::textField('BPendaftaranRjalan[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'bpendaftaran-rjalan-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		
		// 'NoDaftar',
		[
			'header' => 'No RM',
			'value' => function($data){
				return $data->noDaftar->NoMedrec;
			}
		],
		[
			'header' => 'Pasien',
			'value' => function($data){
				return $data->noDaftar->noMedrec->NAMA;
			}
		],
		[
			'header' => 'Jenis Px',
			'value' => function($data){
				return $data->noDaftar->kodeGol->NamaGol;
			}
		],
		[
			'header' => 'Tgl Daftar',
			'value' => function($data){
				return $data->noDaftar->TGLDAFTAR;
			}
		],
		[
			'header' => 'Jam Daftar',
			'value' => function($data){
				return $data->noDaftar->JamDaftar;
			}
		],
		[
			'header' => 'Poli',
			'value' => function($data){
				return $data->unit->NamaUnit;
			}
		],
		
		[
			'header' => 'Ket Pulang',
			'value' => function($data){
				return $data->KetPlg;
			}
		],
		
		[
			'header' => 'Kunjungan Ke',
			'value' => function($data){
				return $data->noDaftar->KunjunganKe;
			}
		],
		
		// 'KodeTdkLanjut',
		[
			'header' => 'Cara Masuk',
			'value' => function($data){
				return $data->noDaftar->kodeMasuk->NamaMasuk;
			}
		],

		// 'KetTdkL1',
		// 'KetTdkL2',
		// 'KetTdkL3',
		// 'KetTdkL4',
		// 'KetTdkL5',
		
		'PostMRS',
		// array(
		// 	'class'=>'CButtonColumn',
		// 	'template' => '{delete}',
			
			
		// ),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

