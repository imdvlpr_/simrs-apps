<?php
/* @var $this DmKelasController */
/* @var $model DmKelas */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_kelas'); ?>
		<?php echo $form->textField($model,'id_kelas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_kelas'); ?>
		<?php echo $form->textField($model,'kode_kelas',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_kelas'); ?>
		<?php echo $form->textField($model,'nama_kelas',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_kelas_bpjs'); ?>
		<?php echo $form->textField($model,'kode_kelas_bpjs',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->