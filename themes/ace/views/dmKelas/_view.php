<?php
/* @var $this DmKelasController */
/* @var $data DmKelas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kelas')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_kelas), array('view', 'id'=>$data->id_kelas)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_kelas')); ?>:</b>
	<?php echo CHtml::encode($data->kode_kelas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kelas')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kelas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_kelas_bpjs')); ?>:</b>
	<?php echo CHtml::encode($data->kode_kelas_bpjs); ?>
	<br />


</div>