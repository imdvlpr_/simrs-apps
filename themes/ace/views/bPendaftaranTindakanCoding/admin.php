 <?php
$this->breadcrumbs=array(
	array('name'=>'Bpendaftaran Tindakan Coding','url'=>array('admin')),
	array('name'=>'Manage'),
);

?>
<h1>Manage Bpendaftaran Tindakan Coding</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#bpendaftaran-tindakan-coding-grid').yiiGridView.update('bpendaftaran-tindakan-coding-grid', {
			    url:'<?php echo Yii::app()->createUrl("BPendaftaranTindakanCoding/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                        <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('BPendaftaranTindakanCoding/create'),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('BPendaftaranTindakanCoding[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('BPendaftaranTindakanCoding[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'bpendaftaran-tindakan-coding-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		'id',
		'reg_id',
		'kode_dtd',
		'deskripsi',
		'created_at',
		'updated_at',
		array(
			'class'=>'CButtonColumn',
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

