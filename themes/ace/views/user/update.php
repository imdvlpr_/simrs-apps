<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	 array('name' => 'User','url'=>Yii::app()->createUrl('user/index')),
                            array('name' => 'List')
);

?>

<h1>Update User <?php echo $model->USERNAME; ?></h1>
<div class="row">
	<div class="col-xs-12">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>

