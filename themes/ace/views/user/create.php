<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	 array('name' => 'User','url'=>Yii::app()->createUrl('user/index')),
                            array('name' => 'List')
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>Create User</h1>
<div class="row">
	<div class="col-xs-12">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>

