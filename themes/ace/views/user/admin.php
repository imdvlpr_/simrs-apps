<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'User','url'=>Yii::app()->createUrl('user/index')),
                            array('name' => 'List')
);

?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<script type="text/javascript">
	
	$(document).ready(function(){
		$('#search,#size,#level').change(function(){
    
	        $('#tabeluser').yiiGridView.update('tabeluser', {
	            url:'?r=user/index&filter='+$('#search').val()+'&size='+$('#size').val()+'&level='+$('#level').val()   
	        });
	      
	     });


       $('#btn-tambah').click(function(){
           location.href = "<?php echo Yii::app()->createUrl('user/create');?>";
       }); 
	});
</script>

<?php 


$updateJS = CHtml::ajax( array(
  'url' => "js:url",
  'data' => "js:form.serialize() + action",
  'type' => 'post',
  'dataType' => 'json',
  'success' => "function( data )
  {

    if( data.status == 'failure' )
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      $( '#update-dialog div.update-dialog-content ' )
        .off() // Stop from re-binding event handlers
        
        .on( 'click','form input[type=submit]', function( e ){ // Send clicked button value
          e.preventDefault();
          updateDialog( false, $( this ).attr( 'name' ) );
      });
    }
    else
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      if( data.status == 'success' ) // Update all grid views on success
      {
        $( '#tabeluser' ).each( function(){ // Change the selector if you use different class or element
          $.fn.yiiGridView.update( $( this ).attr( 'id' ) );
        });

    
      }
      setTimeout( \"$( '#update-dialog' ).dialog( 'close' ).children( ':eq(0)' ).empty();\", 1000 );
    }
  }"
)); 

Yii::app()->clientScript->registerScript( 'updateDialog', "
function updateDialog( url, act )
{
  var action = '';
  var form = $( '#update-dialog div.update-dialog-content form' );
  if( url == false )
  {
    action = '&action=' + act;
    url = form.attr( 'action' );
  }
  {$updateJS}
}" );
?>
<div class="row">
    <div class="col-xs-12">
  
                         <div class="pull-right">Data per halaman
                            <?php echo CHtml::dropDownList('User[PAGESIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,20=>20,30=>30,),array('id'=>'size')); ?>
 <?php echo CHtml::dropDownList('User[LEVEL]',isset($_GET['level'])?$_GET['level']:'',CHtml::ListData(UserLevel::model()->findAll(),'KODE_LEVEL','NAMA_LEVEL'),array('id'=>'level','empty'=>'.:Semua:.')); ?>
 <?php
        echo CHtml::textField('User[SEARCH]','',array('placeholder'=>'Cari','id'=>'search')); 
        ?>
         	 </div> 
                   
         <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>  
                      <button id="btn-tambah" class="btn btn-success" ><i class="fa fa-plus"></i> Tambah Pengguna</button>
                   
<?php $this->widget('application.components.ComplexGridView', array(
  'id'=>'tabeluser',
  'dataProvider'=>$model->search(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
    'USERNAME',
    
    array(
    	'header' 	=> 'Nama',
    	'value'		=> '$data->pEGAWAI->NAMA'
    ),
     array(
      'header'  => 'Hak Akses',
      'value'   => '$data->lEVEL->NAMA_LEVEL'
    ),
     array(
      'header'  => 'Status Aktivasi',
      'value'   => '$data->STATUS_LABEL',
      'type'    => 'raw'
    ),
    array(
      'class'=>'CButtonColumn',
      'template'=>'{activate} {deactivate} {update} {delete}',
      'buttons' => array(
        'activate' => array(
            'visible' => '$data->STATUS == 0',
            'url'=>'Yii::app()->createUrl("user/approveUser/", array("id"=>$data->USERNAME))', 
             'click' => "function( e ){
                  e.preventDefault();
                  $( '#update-dialog' ).children( ':eq(0)' ).empty(); // Stop auto POST
                  updateDialog( $( this ).attr( 'href' ) );
                  $( '#update-dialog' )
                    .dialog( { title: 'Aktivasi Pengguna' } )
                    .dialog( 'open' ); }",
            'label'=>'<span class="label label-info"><i class="fa fa-check"></i>&nbsp;Aktifkan&nbsp;</span>',
            'options'=>array(
                'title'=>'Aktivasi Pengguna',
              ),
          ),
         'deactivate' => array(
            'visible' => '$data->STATUS == 1',
            'url'=>'Yii::app()->createUrl("user/disapproveUser/", array("id"=>$data->USERNAME))', 
             'click' => "function( e ){
                  e.preventDefault();
                  $( '#update-dialog' ).children( ':eq(0)' ).empty(); // Stop auto POST
                  updateDialog( $( this ).attr( 'href' ) );
                  $( '#update-dialog' )
                    .dialog( { title: 'Deaktivasi Pengguna' } )
                    .dialog( 'open' ); }",
            'label'=>'<span class="label label-info"><i class="fa fa-check"></i>&nbsp;Non-Aktifkan&nbsp;</span>',
            'options'=>array(
                'title'=>'Non-Aktivasi Pengguna',
              ),
          ),
      ),
    ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
   'pager'=>array(
                  'class'=>'SimplePager',
                  'header'=>'',
                  'firstPageLabel'=>'Pertama',
                  'prevPageLabel'=>'Sebelumnya',
                  'nextPageLabel'=>'Selanjutnya',
                  'lastPageLabel'=>'Terakhir',
                  'firstPageCssClass'=>'btn btn-info',
                  'previousPageCssClass'=>'btn btn-info',
                  'nextPageCssClass'=>'btn btn-info',
                  'lastPageCssClass'=>'btn btn-info',
                  'hiddenPageCssClass'=>'disabled',
                  'internalPageCssClass'=>'btn btn-info',
                  'selectedPageCssClass'=>'btn btn-sky',
                  'maxButtonCount'=>5
                ),
                'itemsCssClass'=>'table  table-bordered table-hover',
                'summaryCssClass'=>'table-message-info',
                'filterCssClass'=>'filter',
                'summaryText'=>'menampilkan {start} - {end} dari {count} data',
                'template'=>'{items}{summary}{pager}',
                'emptyText'=>'Data tidak ditemukan',
                'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>

                    </div>
                </div>
             
<?php 

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'update-dialog',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 300,
    'resizable' => false,
  ),
)); ?>
<div class="update-dialog-content"></div>
<?php $this->endWidget(); ?>
