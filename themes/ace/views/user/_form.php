
<style>
  .errorMessage, .errorSummary{
    color:red;
  }


}

</style>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'htmlOptions'=>array(
    'class'=>'form-horizontal'
  )
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki kesalahan berikut:','</div>'); ?>

             <div class="form-group">
        <?php echo $form->labelEx($model,'PEGAWAI_ID',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php 
       $level_list = CHtml::ListData(Pegawai::model()->findAll(['order'=>'NAMA']),'KDPEGAWAI','NAMA');
         echo $form->dropDownList($model, 'PEGAWAI_ID',$level_list);
         echo $form->error($model,'PEGAWAI_ID'); 

               ?>
          </div>
        </div>
            <div class="form-group">
        <?php echo $form->labelEx($model,'LEVEL',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php 
       $level_list = CHtml::ListData(UserLevel::model()->findAll(),'KODE_LEVEL','NAMA_LEVEL');
         echo $form->dropDownList($model, 'LEVEL',$level_list);
         echo $form->error($model,'LEVEL'); 

               ?>
          </div>
        </div>
        <div class="form-group">
        <?php echo $form->labelEx($model,'euid',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php 
            $list_erp_user = CHtml::ListData(ErpUser::model()->findAll(),'id','username');
         echo $form->dropDownList($model, 'euid',$list_erp_user);
         echo $form->error($model,'euid'); 

               ?>
          </div>
        </div>
        <div class="form-group">
        <?php echo $form->labelEx($model,'USERNAME',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php 
              echo $form->textField($model,'USERNAME',array('class'=>'input-large'));

              
            ?>

            <?php echo $form->error($model,'USERNAME'); ?>
          </div>
        </div>
         <div class="form-group">
        <?php echo $form->labelEx($model,'PASSWORD',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php 
             echo $form->passwordField($model,'PASSWORD'); 
              
            ?>

            <?php echo $form->error($model,'PASSWORD'); ?>
          </div>
        </div>
        <div class="form-group">
        <?php echo $form->labelEx($model,'repeat_password',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php 
             echo $form->passwordField($model,'repeat_password'); 
              
            ?>

            <?php echo $form->error($model,'repeat_password'); ?>
          </div>
        </div>
                
   <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>

        
        </div>
      </div>
     

<?php $this->endWidget(); ?>
