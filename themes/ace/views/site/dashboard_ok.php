  <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();



  ?>
<div class="row">
  <div class="col-xs-12">
    
      <div class="row">
        <div class="col-sm-12">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Pasien CITO & Elektif
              </h4>
              <img id="loadingCito" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahunCito">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>
                 <div id="containerCito" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
         
          
    </div>

        <div class="row">
        <div class="col-sm-12">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Jumlah Operasi Berdasarkan Jenis Anastesi
              </h4>
              <img id="loadingAn" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahunAn">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>
                 <div id="containerAn" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
    </div>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
          <div class="widget-box transparent" id="recent-box">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Jumlah Operasi Dari Masing-Masing Operator  
              </h4>

              <img id="loadingOp" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahunOp">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>

                 <div id="containerOp" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
                 
                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
        
      </div>
       <div class="row">
        <div class="col-sm-12">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Jumlah Operasi 5 Tahun Terakhir
              </h4>
              <img id="loadingLastfive" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
 
                 <div id="containerLastfive" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
    </div>
  </div>
</div>
<?php 
  $cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');

?>
<script type="text/javascript">

function getRekapOperasiLastfive(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetRekapOperasiLastfive');?>',
      async: true,
      beforeSend : function(){
        $('#loadingLastfive').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingLastfive').hide();
      },
      success : function(data){
         $('#loadingLastfive').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];
        var chartData2 = [];

        $.each(hasil.tahun,function(i,obj){
          kategori.push(obj);
        });

        $.each(hasil.items,function(i,obj){
          chartData.push(obj.total);
        });


        $('#containerLastfive').highcharts({
          title: {
              text: 'Jumlah Tindakan Operasi 5 Tahun Terakhir '
          },

          xAxis: {
            categories: kategori,
            title : {
              text : 'Tahun'
            }
          },
          yAxis: {
              title: {
                  text: 'Jumlah'
              }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },

          series: [{
              name: 'Data 5 Tahun Terakhir ',
              data: chartData
          }],

          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      }
                  }
              }]
          }
        });
      }
  });
}

function getRekapOperator(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetRekapOperator');?>',
      async: true,
      beforeSend : function(){
        $('#loadingOp').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingOp').hide();
      },
      success : function(data){
         $('#loadingOp').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];

        var cd = [];
        var obj = new Object;
        
        $.each(hasil.values,function(i,obj){
          kategori.push(obj.nama);
          var tmp = new Object;  
          // tmp.name = obj.des;
          cd.push(obj.total);
        });
        obj.name = 'Data Tahun '+tahun;
        obj.data = cd;
        chartData.push(obj);


        $('#containerOp').highcharts({
          chart: {
              type: 'bar'
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
          },
          xAxis: {
              categories: kategori,
              title: {
                  text: null
              }
          },
          yAxis: {
            min: 0,
            title: {
                text: 'Jumlah',
                // align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
          },
          title: {
            text: 'Jumlah Operasi Tiap Dokter Tahun '+tahun
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'top',
              x: -40,
              y: 0,
              floating: true,
              borderWidth: 1,
              backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
              shadow: true
          },
          plotOptions: {
               bar: {
                  dataLabels: {
                      enabled: true
                  }
              }
          },
          series: chartData
        });
      }
  });
}


function getRekapAnastesi(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetRekapAnastesi');?>',
      async: true,
      beforeSend : function(){
        $('#loadingAn').show();
      },
      error : function(e){
          console.log(e);
         $('#loadingAn').hide();
      },
      success : function(data){
         $('#loadingAn').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var dataLa = [];
        var dataSab = [];
        var dataGa = [];
        var dataEpi = [];
        var dataPer = [];

        $.each(hasil.values,function(i,obj){
          kategori.push(obj.nama);          
          dataLa.push(obj.la);
          dataSab.push(obj.sab);
          dataGa.push(obj.ga);
          dataEpi.push(obj.epi);
          dataPer.push(obj.per);
        });

        $('#containerAn').highcharts({
          chart: {
            type: 'column'
          },
          title: {
            text: 'Rekap Pasien Berdasarkan Jenis Anastesi Tahun '+tahun
          },
          xAxis: {
            categories: kategori,
            title : {
              text : 'UPF'
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Jumlah Px'
            }
          },
          series: [{
            name : 'LA',
            data : dataLa
          },{
            name : 'SAB',
            data : dataSab
          },{
            name : 'GA',
            data : dataGa
          },{
            name : 'EPI',
            data : dataEpi
          },{
            name : 'PER',
            data : dataPer
          }]
        });

      
      }
  });
}

function getRekap(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetOkCitoElektif');?>',
      async: true,
      beforeSend : function(){
        $('#loadingCito').show();
      },
      error : function(e){
          console.log(e);
         $('#loadingCito').hide();
      },
      success : function(data){
         $('#loadingCito').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var dataCito = [];
        var dataElektif = [];

        $.each(hasil.values,function(i,obj){
          kategori.push(obj.nama);
          dataCito.push(obj.cito);
          dataElektif.push(obj.elektif);
        });

        $('#containerCito').highcharts({
          chart: {
            type: 'column'
          },
          title: {
            text: 'Rekap Pasien CITO & ELEKTIF Tahun '+tahun
          },
          xAxis: {
            categories: kategori,
            title : {
              text : 'UPF'
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Jumlah Px'
            }
          },
          series: [{
            name : 'Cito',
            data : dataCito
          },{
            name : 'Elektif',
            data : dataElektif
          }]
        });

      
      }
  });
}
jQuery(function($) {
 
  var tahun = $('#tahunCito').val();

  getRekap(tahun);
  getRekapAnastesi(tahun);
  getRekapOperator(tahun);
  getRekapOperasiLastfive();

  $('#tahunCito').change(function(e){
    var tahun = $(this).val();
    
    if(tahun){
      getRekap(tahun);
    }
  });

  $('#tahunAn').change(function(e){
    var tahun = $(this).val();
    
    if(tahun){
      getRekapAnastesi(tahun);
    }
  });

  $('#tahunOp').change(function(e){
    var tahun = $(this).val();
    
    if(tahun){
      getRekapOperator(tahun);
    }
  });

});
</script>