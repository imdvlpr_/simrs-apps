  <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();



  ?>
<div class="row">
  <div class="col-xs-12">
      
      <div class="row">
        <div class="col-lg-4 col-sm-12 col-xs-12">
          <div class="widget-box transparent" id="recent-box">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Monthly Visitors 
              </h4>

              <img id="loadingKunjunganGol" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun_kg">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>
<?php
  $MonthArray = array(
    "01" => "January", "02" => "February", "03" => "March", "04" => "April",
    "05" => "May", "06" => "June", "07" => "July", "08" => "August",
    "09" => "September", "10" => "October", "11" => "November", "12" => "December",
  );
?>
<select name="month" id="bulan_kg">
  <option value="">Select Month</option>
  <?php
    foreach ($MonthArray as $monthNum=>$month) {
      $selected = (date('m') == $monthNum) ? 'selected' : '';
      //Uncomment line below if you want to prefix the month number with leading 0 'Zero'
      //$monthNum = str_pad($monthNum, 2, "0", STR_PAD_LEFT);
      echo '<option ' . $selected . ' value="' . $monthNum . '">' . $month . '</option>';
    }
  ?>
</select>
                 <div id="container_kg" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
                 
                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
         <div class="col-lg-4 col-sm-12 col-xs-12">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Kunjungan Tahunan
              </h4>
              <img id="loadingKGTahunan" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
    

                 <div id="container_kg_tahunan" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
         <div class="col-lg-4 col-sm-12 col-xs-12">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Kunjungan 5 Tahun Terakhir
              </h4>
              <img id="loading5tahun" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
    

                 <div id="container_5_tahun" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
      </div>
       <div class="row">
        <div class="col-sm-6">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Kunjungan Rawat jalan
              </h4>
              <img id="loadingRekap" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>
<?php
  $MonthArray = array(
    "01" => "January", "02" => "February", "03" => "March", "04" => "April",
    "05" => "May", "06" => "June", "07" => "July", "08" => "August",
    "09" => "September", "10" => "October", "11" => "November", "12" => "December",
  );
?>
<select name="month" id="bulan">
  <option value="">Select Month</option>
  <?php
    foreach ($MonthArray as $monthNum=>$month) {
      $selected = (date('m') == $monthNum) ? 'selected' : '';
      //Uncomment line below if you want to prefix the month number with leading 0 'Zero'
      //$monthNum = str_pad($monthNum, 2, "0", STR_PAD_LEFT);
      echo '<option ' . $selected . ' value="' . $monthNum . '">' . $month . '</option>';
    }
  ?>
</select>
                 <div id="container" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
         <div class="col-sm-6">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Kunjungan Rawat Inap
              </h4>
              <img id="loadingRekapInap" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun_inap">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>
<?php
  $MonthArray = array(
    "01" => "January", "02" => "February", "03" => "March", "04" => "April",
    "05" => "May", "06" => "June", "07" => "July", "08" => "August",
    "09" => "September", "10" => "October", "11" => "November", "12" => "December",
  );
?>
<select name="month" id="bulan_inap">
  <option value="">Select Month</option>
  <?php
    foreach ($MonthArray as $monthNum=>$month) {
      $selected = (date('m') == $monthNum) ? 'selected' : '';
      //Uncomment line below if you want to prefix the month number with leading 0 'Zero'
      //$monthNum = str_pad($monthNum, 2, "0", STR_PAD_LEFT);
      echo '<option ' . $selected . ' value="' . $monthNum . '">' . $month . '</option>';
    }
  ?>
</select>
                 <div id="container_inap" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
          
    </div>
  <div class="row">
      
        </div>   
      <div class="row">
        <div class="col-sm-6">
          <div class="widget-box transparent" id="recent-box">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Registrasi Terkini Pasien Rawat Jalan 
              </h4>

              <img id="loading1" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <div id="task-tab1" class="tab-pane active">
                     
                    
                  </div>

                 
                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
      <div class="col-sm-6">
          <div class="widget-box transparent" id="recent-box">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Registrasi Terkini Pasien Rawat Inap 
              </h4>

              <img id="loading2" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <div id="task-tab2" class="tab-pane active">
                     
                    
                  </div>

                 
                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
      </div><!-- /.row -->

  </div>
</div>
<?php 
  $cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');

?>
<script type="text/javascript">

function getKunjunganGolonganLastFive(){

    
  $.ajax({
      type : 'POST',
      url : '<?=Yii::app()->createUrl('AjaxRequest/countKunjunganGolonganLastfive');?>',
      async: true,
      beforeSend : function(){
        $('#loading5tahung').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loading5tahun').hide();
      },
      success : function(data){
         $('#loading5tahun').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];
        // var chartData2 = [];

        $.each(hasil.listTahun,function(i,obj){
          kategori.push(eval(obj));
        });

          // var sums = [];
        $.each(hasil.listGol.values,function(j,sub){
          var tmp = new Object;
          tmp.name = sub.NamaGol;
          var sums = [];
          var rows = hasil.rows[sub.KodeGol];
          $.each(rows,function(k,subs){
            sums.push(subs.jumlah);
          });
          tmp.kodeGol = sub.KodeGol
          tmp.data = sums;

          chartData.push(tmp);
        });

        $('#container_5_tahun').highcharts({
          title: {
              text: 'Kunjungan 5 Tahun Terakhir'
          },

          xAxis: {
            categories: kategori,
            title : {
              text : 'Tahun'
            }
          },
          yAxis: {
              title: {
                  text: 'Jumlah'
              }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },
          series: chartData,

          // series: [{
          //     name: 'Laba Tahun ',
          //     data: chartData
          // }],
          plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function(e) {
                            var tahun = this.category;
                            var kodeGol=this.series.options.kodeGol;
                            getKunjunganGolonganCount(tahun,kodeGol);
                        }
                    }
                }
            }
          },
          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      }
                  }
              }]
          }
        });
      }
  });
}

function getKunjunganGolonganCount(tahun,kode){

  if(!tahun)
    tahun = new Date().getFullYear();

  if(!kode)
    kode = '';

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun+'&kode='+kode,
      url : '<?=Yii::app()->createUrl('AjaxRequest/CountKunjunganGolongan');?>',
      async: true,
      beforeSend : function(){
        $('#loadingKGTahunan').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingKGTahunan').hide();
      },
      success : function(data){
         $('#loadingKGTahunan').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];
        // var chartData2 = [];

        $.each(hasil.listBulan,function(i,obj){
          kategori.push(obj);
        });
          // var sums = [];
        $.each(hasil.listGol.values,function(j,sub){
          var tmp = new Object;
          tmp.name = sub.NamaGol;
          tmp.kodeGol = sub.KodeGol;
          var sums = [];
          var rows = hasil.rows[sub.KodeGol];
          $.each(rows,function(k,subs){
            sums.push(subs.jumlah);
          });
          tmp.hasCustomFlag = true;
          tmp.data = sums;

          chartData.push(tmp);
        });

        $('#container_kg_tahunan').highcharts({
          title: {
              text: 'Kunjungan Tahun '+tahun
          },

          xAxis: {
            categories: kategori,
            title : {
              text : 'Bulan'
            }
          },
          yAxis: {
              title: {
                  text: 'Jumlah'
              }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },
          plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function(e) {
                            var kodeGol=this.series.options.kodeGol;
                            var bulan = this.category;
                            getUsiaSexGolTanggal(kodeGol, bulan,tahun);
                        }
                    }
                }
            }
          },
          series: chartData,
          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      }
                  }
              }]
          }
        });
      }
  });
}

function getUsiaSexGolTanggal(kode, bulan, tahun){

  if(!tahun)
    tahun = new Date().getFullYear();

  $.ajax({
      type : 'POST',
      data : 'kode='+kode+'&bulan='+bulan+'&tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetUsiaSexGolTanggal');?>',
      async: true,
      beforeSend : function(){
        $('#loadingKGTahunan').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingKGTahunan').hide();
      },
      success : function(data){
         $('#loadingKGTahunan').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = hasil.kategori;
        var chartData = [];

        var obj = new Object;
        obj.name = 'L';
        obj.data = hasil.males;

        chartData.push(obj); 

        var obj = new Object;
        obj.name = 'P';
        obj.data = hasil.females;

        chartData.push(obj);



        $('#container_kg_tahunan').highcharts({
          chart: {
            type: 'column'
          },

          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y}</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          title: {
            text: 'Visitors: '+bulan+' '+tahun
          },
          xAxis: {
            categories: kategori,
            title : {
              text : 'Usia'
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Jumlah'
            }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },
          series: chartData
        }, function (chart) { // on complete
        var i = 0;

        chart.renderer.button('Kembali',chart.chartWidth - 100, 10)
            .attr({
                zIndex: 3
            })
            .on('click', function () {
                getKunjunganGolonganCount();
            })
            .add();

    });
      }
  });
}

function getKunjunganGolongan(bulan, tahun){

  $.ajax({
      type : 'POST',
      data : 'bulan='+bulan+'&tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetKunjunganGolongan');?>',
      async: true,
      beforeSend : function(){
        $('#loadingKunjunganGol').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingKunjunganGol').hide();
      },
      success : function(data){
         $('#loadingKunjunganGol').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];

        $.each(hasil.values,function(i,obj){
          kategori.push(obj.NamaGol);
          var tmp = new Object;
          tmp.name = obj.NamaGol;
          tmp.y = obj.Total;
          chartData.push(tmp);
        });


        $('#container_kg').highcharts({
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },

          title: {
            text: 'Kelompok Jenis Pasien'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: true,
                      format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                      style: {
                          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                      }
                  }
              }
          },
          series: [{
            colorByPoint: true,
            name : 'Golongan',
            data : chartData
          }]
        });
      }
  });
}

function getRekapInap(bulan, tahun){

  $.ajax({
      type : 'POST',
      data : 'bulan='+bulan+'&tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetRekapKunjunganRawatInap');?>',
      async: true,
      beforeSend : function(){
        $('#loadingRekapInap').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingRekapInap').hide();
      },
      success : function(data){
         $('#loadingRekapInap').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];

        $.each(hasil.values,function(i,obj){
          kategori.push(obj.NamaUnit);
          chartData.push(obj.Total);
        });

        $('#container_inap').highcharts({
          chart: {
            type: 'column'
          },
          title: {
            text: 'Rekap Kunjungan Rawat Inap'
          },
          xAxis: {
            categories: kategori,
            title : {
              text : 'Kamar'
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Value'
            }
          },
          series: [{
            name : 'Jumlah Kunjungan',
            data : chartData
          }]
        });
      }
  });
}

function getRekap(bulan, tahun){

  $.ajax({
      type : 'POST',
      data : 'bulan='+bulan+'&tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetRekapKunjungan');?>',
      async: true,
      beforeSend : function(){
        $('#loadingRekap').show();
      },
      error : function(e){
          console.log(e);
         $('#loadingRekap').hide();
      },
      success : function(data){
         $('#loadingRekap').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];

        $.each(hasil.values,function(i,obj){
          kategori.push(obj.NamaUnit);
          chartData.push(obj.Total);
        });

        $('#container').highcharts({
          chart: {
            type: 'column'
          },
          title: {
            text: 'Rekap Kunjungan Rawat Jalan'
          },
          xAxis: {
            categories: kategori,
            title : {
              text : 'Poli'
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Value'
            }
          },
          series: [{
            name : 'Jumlah Kunjungan',
            data : chartData
          }]
        });
      }
  });
}

function updateRecent(){
  $.ajax({
      type : 'POST',
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetLastRegisteredPxRJ');?>',
      async: true,
      timeout:30000,
      beforeSend : function(){
        $('#loading1').show();
      },
      success : function(result){
        var result = jQuery.parseJSON(result);
        $('#loading1').hide();
        $('#task-tab1').empty();
        var row = '';
        $.each(result, function(q,v){
          var img = (v.jk == 'P') ? 'avatar3.png' : 'avatar4.png';
          // console.log(v.jk);
           row += '<div class="comments"><div class="itemdiv commentdiv">';
           row +=  '<div class="user">';
           row += '<img src="<?=$baseUrl;?>/assets/images/avatars/'+img+'" /></div>';
           row +=      '<div class="body">';
           row +=        '<div class="name">'
          row +=  '<a href="#">'+v.nama+'</a></div>';
          row +=         '<div class="time">';
          row += '<i class="ace-icon fa fa-clock-o"></i>';
           row +=          '<span class="green">'+v.tanggal+'</span></div>';
          row += '<div class="text">';
          row +=           '<i class="ace-icon fa fa-quote-left"></i>';
          row +=  v.msg;
         row +=          '</div>';
          row +=       '</div>';
          row +=     '</div>';
          row +=   '</div>';
          row +=   '<div class="hr hr8"></div>';
        });

        $('#task-tab1').html(row);
      }
    });

  $.ajax({
      type : 'POST',
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetLastRegisteredPx');?>',
      async: true,
      timeout:30000,

      beforeSend : function(){
        $('#loading2').show();
      },
      success : function(result){
        var result = jQuery.parseJSON(result);
        $('#loading2').hide();
        $('#task-tab2').empty();
        var row = '';
        $.each(result, function(q,v){
          var img = (v.jk == 'P') ? 'avatar3.png' : 'avatar4.png';
          // console.log(v.jk);
           row += '<div class="comments"><div class="itemdiv commentdiv">';
           row +=  '<div class="user">';
           row += '<img src="<?=$baseUrl;?>/assets/images/avatars/'+img+'" /></div>';
           row +=      '<div class="body">';
           row +=        '<div class="name">'
          row +=  '<a href="#">'+v.nama+'</a></div>';
          row +=         '<div class="time">';
          row += '<i class="ace-icon fa fa-clock-o"></i>';
           row +=          '<span class="green">'+v.tanggal+'</span></div>';
          row += '<div class="text">';
          row +=           '<i class="ace-icon fa fa-quote-left"></i>';
          row +=  v.msg;
         row +=          '</div>';
          row +=       '</div>';
          row +=     '</div>';
          row +=   '</div>';
          row +=   '<div class="hr hr8"></div>';
        });

        $('#task-tab2').html(row);
      }
    });
}
  
  jQuery(function($) {
    getKunjunganGolonganLastFive();
    var bulan = $('#bulan').val();
    var tahun = $('#tahun').val();

    getRekap(bulan, tahun);

    var bulan = $('#bulan_inap').val();
    var tahun = $('#tahun_inap').val();
    getRekapInap(bulan, tahun);

    var bulan = $('#bulan_kg').val();
    var tahun = $('#tahun_kg').val();

    getKunjunganGolongan(bulan, tahun);

    getKunjunganGolonganCount();

    $('#bulan, #tahun').change(function(e){
      var tahun = $('#tahun').val();
      var bulan = $('#bulan').val();

      if(tahun && bulan){
        getRekap(bulan,tahun);
      }
    });

    $('#bulan_inap, #tahun_inap').change(function(e){
      var tahun = $('#tahun_inap').val();
      var bulan = $('#bulan_inap').val();

      if(tahun && bulan){
        getRekapInap(bulan,tahun);
      }
    });

    $('#bulan_kg, #tahun_kg').change(function(e){
      var tahun = $('#tahun_kg').val();
      var bulan = $('#bulan_kg').val();

      if(tahun && bulan){
        getKunjunganGolongan(bulan,tahun);
      }
    });

    updateRecent();
    setInterval(function() {
      updateRecent();
  }, 30000);
    


 
  });
</script>