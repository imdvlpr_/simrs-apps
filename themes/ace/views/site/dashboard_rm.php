  <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();



  ?>
<div class="row">
  <div class="col-xs-12">
     
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
          <div class="widget-box transparent" id="recent-box">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Top-Ten Penyakit 
              </h4>

              <img id="loading1" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>

                 <div id="container" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
                 
                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
        
      </div>

  
  </div>
</div>
<?php 
  $cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');

?>
<script type="text/javascript">



function getKunjunganGolongan(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetTopTenPenyakit');?>',
      async: true,
      beforeSend : function(){
        $('#loading1').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loading1').hide();
      },
      success : function(data){
         $('#loading1').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];

        var cd = [];
        var obj = new Object;
        
        $.each(hasil,function(i,obj){
          kategori.push(obj.des);
          var tmp = new Object;  
          // tmp.name = obj.des;
          cd.push(obj.jml);
        });
        obj.name = 'Data Tahun '+tahun;
        obj.data = cd;
        chartData.push(obj);


        $('#container').highcharts({
          chart: {
              type: 'bar'
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
          },
          xAxis: {
              categories: kategori,
              title: {
                  text: null
              }
          },
          yAxis: {
            min: 0,
            title: {
                text: 'Jumlah',
                // align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
          },
          title: {
            text: 'Top-Ten Penyakit'
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'top',
              x: -40,
              y: 0,
              floating: true,
              borderWidth: 1,
              backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
              shadow: true
          },
          plotOptions: {
               bar: {
                  dataLabels: {
                      enabled: true
                  }
              }
          },
          series: chartData
        });
      }
  });
}

jQuery(function($) {
 
  var tahun = $('#tahun').val();

  getKunjunganGolongan(tahun);


  $('#tahun').change(function(e){
    var tahun = $('#tahun').val();
    
    if(tahun){
      getKunjunganGolongan(tahun);
    }
  });



});
</script>