  <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();



  ?>
<div class="row">
  <div class="col-xs-12">
      
      <div class="row">
         <div class="col-sm-6">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Pembelian Obat
              </h4>
              <img id="loadingBuy" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun_buy">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>

                 <div id="containerBuy" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
        <div class="col-sm-6">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Penjualan Obat
              </h4>
              <img id="loadingSales2" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun_sales">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>

                 <div id="containerSales" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
      </div>
      <div class="row">
         <div class="col-sm-6">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Perbandingan Pembelian & Penjualan Obat
              </h4>
              <img id="loadingComp" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun_comp">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>

                 <div id="containerComp" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
        <div class="col-sm-6">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Laba Penjualan Obat
              </h4>
              <img id="loadingSales" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun_obat">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>

                 <div id="containerObat" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="widget-box transparent">
            <div class="widget-header">
              <h4 class="widget-title lighter smaller">
                <i class="ace-icon fa fa-rss orange"></i>Grafik Pendapatan dari OK
              </h4>
              <img id="loadingOk" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
            </div>

            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <select name="year"  id="tahun_ok">
  <option value=''>Select Year</option>
  <?php
    for ($year = 2014; $year <= 2030; $year++) {
      $selected = (date('Y')==$year) ? 'selected' : '';
        echo "<option value=$year $selected>$year</option>";
      
    }
  ?>
</select>

                 <div id="containerOk" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div><!-- /.widget-box -->
        </div><!-- /.col -->
      </div>
  </div>
</div>
<?php 
  $cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');

?>
<script type="text/javascript">
Highcharts.setOptions({
    lang: {
        thousandsSep: '.',
        decimalPoint: ','
    }
});

function getPendapatanOk(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/getPendapatanOk');?>',
      async: true,
      beforeSend : function(){
        $('#loadingOk').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingOk').hide();
      },
      success : function(data){
         $('#loadingOk').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];
        var chartData2 = [];

        $.each(hasil.now,function(i,obj){
          kategori.push(obj.bulan);
          chartData.push(obj.jumlah);
        });

        $.each(hasil.then,function(i,obj){
          chartData2.push(obj.jumlah);
        });

        $('#containerOk').highcharts({
          title: {
              text: 'Pendapatan RS OK Tahun '+eval(tahun-1)+' dan Tahun '+eval(tahun)
          },

          xAxis: {
            categories: kategori,
            title : {
              text : 'Bulan'
            }
          },
          yAxis: {
              title: {
                  text: 'Nominal'
              }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },

          series: [{
              name: 'Pendapatan Tahun '+tahun,
              data: chartData
          },{
              name: 'Pendapatan Tahun '+eval(tahun-1),
              data: chartData2
          }],

          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      }
                  }
              }]
          }
        });
      }
  });
}

function getPerbandingan(tahun){
  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetPerbandingan');?>',
      async: true,
      beforeSend : function(){
        $('#loadingComp').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingComp').hide();
      },
      success : function(data){
         $('#loadingComp').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];
        var chartData2 = [];

        var tmp = [];
        $.each(hasil.buy,function(i,obj){
          kategori.push(obj.bulan);
          tmp.push(obj.jumlah);
          // chartData.push(obj.jumlah);
        });

        var obj = new Object;
        obj.name = 'Pembelian';
        obj.data = tmp;

        chartData.push(obj); 

        var tmp = [];
        $.each(hasil.sales,function(i,obj){
          // chartData2.push(obj.jumlah);
          tmp.push(obj.jumlah);
        });

        var obj = new Object;
        obj.name = 'Penjualan';
        obj.data = tmp;

        chartData.push(obj);

        $('#containerComp').highcharts({
          title: {
              text: 'Perbandingan Penjualan & Pembelian Obat Tahun '+eval(tahun-1)+' dan Tahun '+eval(tahun)
          },

          chart: {
            type: 'column'
          },

          xAxis: {
            categories: kategori,
            title : {
              text : 'Bulan'
            }
          },
          yAxis: {
              title: {
                  text: 'Nominal'
              }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },

          series: chartData

          // responsive: {
          //     rules: [{
          //         condition: {
          //             maxWidth: 500
          //         },
          //         chartOptions: {
          //             legend: {
          //                 layout: 'horizontal',
          //                 align: 'center',
          //                 verticalAlign: 'bottom'
          //             }
          //         }
          //     }]
          // }
        });
      }
  });
}

function getPembelian(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetPembelian');?>',
      async: true,
      beforeSend : function(){
        $('#loadingBuy').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingBuy').hide();
      },
      success : function(data){
         $('#loadingBuy').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];
        var chartData2 = [];

        $.each(hasil.now,function(i,obj){
          kategori.push(obj.bulan);
          chartData.push(obj.jumlah);
        });

        $.each(hasil.then,function(i,obj){
          chartData2.push(obj.jumlah);
        });

        $('#containerBuy').highcharts({
          title: {
              text: 'Pembelian Obat Tahun '+eval(tahun-1)+' dan Tahun '+eval(tahun)
          },

          xAxis: {
            categories: kategori,
            title : {
              text : 'Bulan'
            }
          },
          yAxis: {
              title: {
                  text: 'Nominal'
              }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },

          series: [{
              name: 'Pembelian Tahun '+tahun,
              data: chartData
          },{
              name: 'Pembelian Tahun '+eval(tahun-1),
              data: chartData2
          }],

          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      }
                  }
              }]
          }
        });
      }
  });
}

function getPenjualan(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetPenjualan');?>',
      async: true,
      beforeSend : function(){
        $('#loadingSales2').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingSales2').hide();
      },
      success : function(data){
         $('#loadingSales2').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];
        var chartData2 = [];

        $.each(hasil.now,function(i,obj){
          kategori.push(obj.bulan);
          chartData.push(obj.jumlah);
        });

        $.each(hasil.then,function(i,obj){
          chartData2.push(obj.jumlah);
        });

        $('#containerSales').highcharts({
          title: {
              text: 'Penjualan Obat Tahun '+eval(tahun-1)+' dan Tahun '+eval(tahun)
          },

          xAxis: {
            categories: kategori,
            title : {
              text : 'Bulan'
            }
          },
          yAxis: {
              title: {
                  text: 'Nominal'
              }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },

          series: [{
              name: 'Penjualan Tahun '+tahun,
              data: chartData
          },{
              name: 'Penjualan Tahun '+eval(tahun-1),
              data: chartData2
          }],

          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      }
                  }
              }]
          }
        });
      }
  });
}

function getObat(tahun){

  $.ajax({
      type : 'POST',
      data : 'tahun='+tahun,
      url : '<?=Yii::app()->createUrl('AjaxRequest/GetLaba');?>',
      async: true,
      beforeSend : function(){
        $('#loadingSales').show();
      },
      error : function(e){
          console.log(e.responseText);
         $('#loadingSales').hide();
      },
      success : function(data){
         $('#loadingSales').hide();
        var hasil = $.parseJSON(data);
        
        var kategori = [];
        var chartData = [];
        var chartData2 = [];

        $.each(hasil.now,function(i,obj){
          kategori.push(obj.bulan);
          chartData.push(obj.jumlah);
        });

        $.each(hasil.then,function(i,obj){
          chartData2.push(obj.jumlah);
        });

        $('#containerObat').highcharts({
          title: {
              text: 'Laba Obat Tahun '+eval(tahun-1)+' dan Tahun '+eval(tahun)
          },

          xAxis: {
            categories: kategori,
            title : {
              text : 'Kamar'
            }
          },
          yAxis: {
              title: {
                  text: 'Bulan'
              }
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },

          series: [{
              name: 'Laba Tahun '+tahun,
              data: chartData
          },{
              name: 'Laba Tahun '+eval(tahun-1),
              data: chartData2
          }],

          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      }
                  }
              }]
          }
        });
      }
  });
}
  jQuery(function($) {
    
    var tahun = $('#tahun_obat').val();
    getObat(tahun);
    getPenjualan(tahun);
    getPembelian(tahun);
    getPerbandingan(tahun);
    getPendapatanOk(tahun);

    $('#tahun_obat').change(function(e){
      var tahun = $('#tahun_obat').val();
      
      if(tahun){
        getObat(tahun);
      }
    });

    $('#tahun_sales').change(function(e){
      var tahun = $(this).val();
      
      if(tahun){
        getPenjualan(tahun);
      }
    });

    $('#tahun_buy').change(function(e){
      var tahun = $(this).val();
      
      if(tahun){
        getPembelian(tahun);
      }
    });

    $('#tahun_comp').change(function(e){
      var tahun = $(this).val();
      
      if(tahun){
        getPerbandingan(tahun);
      }
    });

    $('#tahun_ok').change(function(e){
      var tahun = $('#tahun_ok').val();
      
      if(tahun){
        getPendapatanOk(tahun);
      }
    });

  });
</script>