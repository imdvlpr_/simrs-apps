  <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();



  ?>
<div class="row">
  <div class="col-xs-12">
      <div class="alert alert-block alert-success">
          <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
          </button>

          <i class="ace-icon fa fa-check green"></i>

          Welcome to
          <strong class="green">
            SIMRS
            <small>(v2.0)</small>
          </strong>,
          Sistem Informasi Manajemen Rumah Sakit
        </div>
       
  </div>
</div>
