
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>false,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class'=>'form-horizontal'
    )
)); ?>
<label class="block clearfix">
  <span class="block input-icon input-icon-right">
    <?php echo $form->textField($model,'USERNAME',array('class'=>'form-control','placeholder'=>'Username')); ?>
        <?php echo $form->error($model,'USERNAME'); ?>
    <i class="ace-icon fa fa-user"></i>
  </span>
</label>
<label class="block clearfix">
  <span class="block input-icon input-icon-right">
    <?php echo $form->passwordField($model,'PASSWORD',array('class'=>'form-control','placeholder'=>'Password')); ?>
        <?php echo $form->error($model,'PASSWORD'); ?>
    
    <i class="ace-icon fa fa-user"></i>
  </span>
</label>

<div class="space"></div>

<div class="clearfix">


  <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
    <i class="ace-icon fa fa-key"></i>
    <span class="bigger-110">Login</span>
  </button>
</div>


<?php $this->endWidget(); ?>