<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_ok'); ?>
		<?php echo $form->textField($model,'id_ok'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_urut'); ?>
		<?php echo $form->textField($model,'no_urut'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_rm'); ?>
		<?php echo $form->textField($model,'no_rm'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_operasi'); ?>
		<?php echo $form->textField($model,'tgl_operasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jam_ok'); ?>
		<?php echo $form->textField($model,'jam_ok'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'diagnosa'); ?>
		<?php echo $form->textField($model,'diagnosa',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tindakan'); ?>
		<?php echo $form->textField($model,'tindakan',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jenis_operasi'); ?>
		<?php echo $form->textField($model,'jenis_operasi',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jenis_anastesi'); ?>
		<?php echo $form->textField($model,'jenis_anastesi',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dr_operator'); ?>
		<?php echo $form->textField($model,'dr_operator'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dr_anastesi'); ?>
		<?php echo $form->textField($model,'dr_anastesi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'asisten_dokter1'); ?>
		<?php echo $form->textField($model,'asisten_dokter1',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'asisten_dokter2'); ?>
		<?php echo $form->textField($model,'asisten_dokter2',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cito_elektif'); ?>
		<?php echo $form->textField($model,'cito_elektif',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'spesimen'); ?>
		<?php echo $form->textField($model,'spesimen',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_rawat_inap'); ?>
		<?php echo $form->textField($model,'id_rawat_inap'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status_ok'); ?>
		<?php echo $form->textField($model,'status_ok'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->