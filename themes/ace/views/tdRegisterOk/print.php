<table cellpadding="3" style="font-size: 10px;text-align: center">
  
  <thead>
    <tr>
      <th>
      PERINCIAN BIAYA PERAWATAN<br>
      TINDAKAN MEDIK OPERATIF
      </th>
      
      
    </tr>
  </thead>
  </table>
<table cellpadding="1" style="font-size: 10px">
  
    <tr>
      <td width="20%">Nama Pasien</td>
      <td width="2%">:</td>
      <td width="28%"><?=$model->pASIEN->NAMA;?></td>
      <td width="20%">UMUR</td>
      <td width="3%">:</td>
      <td width="27%"><?=Yii::app()->helper->hitungUmur($model->pASIEN->TGLLAHIR);?></td>
      
    </tr>
  <tr>
      <td width="20%">REGISTER</td>
      <td width="2%">:</td>
      <td width="28%"><?=$model->pASIEN->NoMedrec;?></td>
      <td width="20%">RUANG</td>
      <td width="3%">:</td>
      <td width="27%"><?=$model->unit->NamaUnit;?></td>
      
    </tr>
    <tr>
      <td width="20%">JENIS TINDKAN</td>
      <td width="2%">:</td>
      <td width="28%"><?=$model->diagnosa;?></td>
      <td width="20%">GOL OP</td>
      <td width="3%">:</td>
      <td width="27%"><?=$model->jenisOperasi->nama.' / '.$model->tindakan0->dmOkJenisTindakan->nama ;?></td>
      
    </tr>
    <tr>
      <td width="20%">KELAS PERAWATAN</td>
      <td width="2%">:</td>
      <td width="28%"><?= $model->tindakan0->dmKelas->nama_kelas ;?></td>
      <td width="20%">TANGGAL</td>
      <td width="3%">:</td>
      <td width="27%"><?=date('d/m/Y',strtotime($model->tgl_operasi));?></td>
      
    </tr>
</table><br><br>
<table border="1" cellpadding="4" style="font-size: 10px">
  
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="65%" style="text-align: center">ITEM</th>
      <th width="30%" style="text-align: center">BIAYA (Rp)</th>
      
    </tr>
  </thead>
  <tbody>
     <tr>
      <td width="5%">1</td>
      <td width="65%">JASA RS</td>
      <td width="30%" style="text-align: right;font-weight: bold;"><?=Yii::app()->helper->formatRupiah($modelBiaya->biaya_jrs);?></td>
      
    </tr>
    <tr>
      <td width="5%">2</td>
      <td width="65%">BAHAN</td>
      <td width="30%" style="text-align: right;font-weight: bold;"><?=Yii::app()->helper->formatRupiah($modelBiaya->biaya_bhn);?></td>
      
    </tr>
    <tr>
      <td width="5%">3</td>
      <td width="65%">JASA DOKTER OPERATOR <?=!empty($model->tdOkBiayas) ? $model->tdOkBiayas->drOperator->FULLNAME : '';?></td>
      <td width="30%" style="text-align: right;font-weight: bold;"><?=Yii::app()->helper->formatRupiah($modelBiaya->biaya_dr_op);?></td>
      
    </tr>
    <tr>
      <td width="5%">4</td>
      <td width="65%">JASA DOKTER ANESTESI <?=$model->drAnastesi->FULLNAME;?></td>
      <td width="30%" style="text-align: right;font-weight: bold;"><?=Yii::app()->helper->formatRupiah($modelBiaya->biaya_dr_anas);?></td>
      
    </tr>
    <tr>
      <td width="5%">5</td>
      <td width="65%">JASA PERAWATAN OK</td>
      <td width="30%" style="text-align: right;font-weight: bold;"><?=Yii::app()->helper->formatRupiah($modelBiaya->biaya_prwt_ok);?></td>
      
    </tr>
    <tr>
      <td width="5%">6</td>
      <td width="65%">JASA PERAWATAN ANESTESI</td>
      <td width="30%" style="text-align: right;font-weight: bold;"><?=Yii::app()->helper->formatRupiah($modelBiaya->biaya_prwt_anas);?></td>
      
    </tr>
    <tr>
      <td width="5%"></td>
      <td width="65%" style="text-align: center"><b>TOTAL</b></td>
      <td width="30%" style="text-align: right;font-weight: bold;"><?=Yii::app()->helper->formatRupiah($modelBiaya->biaya_jrs + $modelBiaya->biaya_bhn + $modelBiaya->biaya_dr_op + $modelBiaya->biaya_dr_anas + $modelBiaya->biaya_prwt_ok + $modelBiaya->biaya_prwt_anas);?></td>
      
    </tr>
  </tbody>
</table>