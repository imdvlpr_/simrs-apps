<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'td-register-ok-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="row">
  <div class="col-sm-4">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Pasien</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
          
                <!-- Medical Record Number Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NoMedrec',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'NoMedrec',array('class'=>'input-large'));
                        echo $form->error($pasien,'NoMedrec');
                    ?>
                     <!--  <a id='carirm' href="javascript:void(0);"><i class="icon-search"></i> Cari RM</a><span id='loading3' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span> -->
                  </div>
                </div>

                <!-- Identification Number Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NOIDENTITAS',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <!-- TODO: Input Number Only! -->
                    <?php echo $form->textField($pasien,'NOIDENTITAS',array(
                      'class'=>'input-large',
                      'tabindex'=>'1',
                      'placeholder' => '16 digit Nomor KTP',
                      'minLength' => '16',
                      'maxLength' => '16',
                      'onkeypress' => 'return isNumber(event)'));?>
                    <?php echo $form->error($pasien,'NOIDENTITAS'); ?>
                  </div>
                </div>

                <!-- Patient's Name Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NAMA',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'NAMA',array('class'=>'input-large','tabindex'=>'2')); ?>
                    <?php echo $form->error($pasien,'NAMA'); ?>
                  </div>
                </div>

                <!-- Patient's Sex Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'JENSKEL',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->dropDownList($pasien, 'JENSKEL',array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('tabindex'=>'4'));
                    // echo $form->radioButtonList($model, 'JENSKEL', array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('labelOptions'=>array('style'=>'display:inline','tabindex'=>'3')));
                    echo $form->error($pasien,'JENSKEL');
                    ?>
                  </div>
                </div>

                <!-- Patient's Date of Birth -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'TGLLAHIR',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <div id="datePickerComponent" class="input-append date margin-00" data-date="<?php echo date('d-m-Y')?>" data-date-format="dd-mm-yyyy">
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                      'model'=>$pasien,
                      'value'=>'TGLLAHIR',
                      'attribute'=>'TGLLAHIR',
                      // additional JavaScript options for the date picker plugin
                      'options'=>array(
                      'showAnim'=>'fold',
                      'showButtonPanel'=>true,
                      'autoSize'=>true,
                      'dateFormat'=>'dd/mm/yy',
                        'changeMonth'=>true,
                        'changeYear'=>true,
                        'yearRange'=>'1900:2099',
                       ),

                      'htmlOptions'=>array(
                        'class'=>'input-medium',
                      ),
                    ));
                    echo $form->error($pasien,'TGLLAHIR',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right')); ?>
                    </div>
                  </div>
                </div>

                <!-- Address Details -->
                <div class="form-group">
                  <?php echo CHtml::label('<strong>Alamat Lengkap:</strong>','',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <!-- TODO: Why Kosongan ada class control nya ? -->
                  <div class="col-sm-9"></div>
                </div>

                <!-- Address Street's Name Field -->
              
                <!-- Address District (Desa) Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'Desa',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php echo $form->textField($pasien,'Desa',array('class'=>'input-large'));
                  echo $form->error($pasien,'Desa'); ?>
                  </div>
                </div>

                  <div class="form-group">
              <?php echo $form->labelEx($pasien,'ALAMAT',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
              <div class="col-sm-9">
                <?php echo $form->textField($pasien,'ALAMAT',array('class'=>'input-large'));
                echo $form->error($pasien,'ALAMAT'); ?>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
 <div class="col-sm-4">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Form OK</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
               <div class="form-group">
                  <?php echo $form->labelEx($model,'unit_id',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                     echo $form->dropDownList($model, 'unit_id',CHtml::listData($listUnit,'KodeUnit',function($data){
                      switch($data->unit_tipe){
                        case 1 : 
                          return 'Kamar '.$data->NamaUnit;
                          break;
                        case 2 :
                          return 'Poli '.$data->NamaUnit;
                          break;

                      }

                     }));
                    
                    echo $form->error($model,'unit_id');?>
                  </div>
                </div>


                  
           <div class="form-group">
                  <?php echo $form->labelEx($model,'tgl_operasi',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'tgl_operasi',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'onClose' => 'js:function(dtText,dtI) {
                            //getBatasSEP(dtText);
                        }',
                    ),
                    ));


                    echo $form->error($model,'tgl_operasi');?>
                  </div>
                </div>

                <!-- Blood Type Field -->

            
                <!-- Religion Field -->
                     <div class="form-group">
                  <?php echo $form->labelEx($model,'diagnosa',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($model,'diagnosa',array(
                      'class'=>'input-large'));
                    echo $form->error($model,'diagnosa');?>
                  </div>
                </div>


                <!-- Job Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($model,'jenis_operasi',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                    echo $form->dropDownList($model,'jenis_operasi',CHtml::listData(DmOkGolOperasi::model()->findAll(),'id','nama'),array('empty'=>'.. Pilih Gol Operasi ..'));
                    echo $form->error($model,'jenis_operasi');

                     echo $form->hiddenField($model,'tindakan',array(
                      'class'=>'input-large'));
                  $imghtml=CHtml::image(Yii::app()->baseUrl.'/images/popup.png', 'Data Tindakan',array('style'=>'top:-5px;position:relative'));
                  echo CHtml::link($imghtml, '#', array('onclick'=>'$("#mydialog").dialog("open"); return false;',));

                    ?>
                  </div>
                </div>

  <div class="form-group">
                  <?php echo $form->labelEx($model,'cito_elektif',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                      echo $form->dropDownList($model, 'cito_elektif',CHtml::listData(DmOkJenisTindakan::model()->findAll(),'id','nama'),array('empty'=>'.. Pilih CITO/ELEKTIF ..'));
                    
                        echo $form->error($model,'cito_elektif');
                    ?>
              
                  </div>
                </div>
                  <div class="form-group">
                  <?php echo $form->labelEx($model,'upf',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                     echo $form->dropDownList($model, 'upf',CHtml::listData(TdUpf::model()->findAllByAttributes(['is_removed'=>0]),'id','nama'));
                    echo $form->error($model,'upf');?>
                  </div>
                </div>


          
                <!-- TEL. Number Field -->


          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Form Tindakan OK</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
             <div class="form-group">
                  <?php echo $form->labelEx($model,'jenis_anastesi',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                     echo $form->dropDownList($model, 'jenis_anastesi',CHtml::listData(DmOkAnastesi::model()->findAll(),'id','kode'),array('empty'=>'.. Pilih Jenis AN ..'));
                    echo $form->error($model,'jenis_anastesi');?>
                  </div>
                </div>
                   <div class="form-group">
                  <?php echo $form->labelEx($model,'dr_anastesi',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php
                     echo CHtml::textField('nama_dr_anastesi',!empty($model->drAnastesi) ? $model->drAnastesi->FULLNAME : '',array('class'=>'input-large nama_dokter_merawat','placeholder'=>'Ketik Nama Dokter'));
                    echo $form->hiddenField($model,'dr_anastesi');
                    echo $form->error($model,'dr_anastesi');?>
                  </div>
                </div>
         

                <!-- Principal's Name (Husband / Wife) Field -->
              
                

                <!-- Medical Record Number Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($model,'asisten_dokter1',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($model,'asisten_dokter1',array('class'=>'input-large'));
                        echo $form->error($model,'asisten_dokter1');
                    ?>
              
                  </div>
                </div>

                <!-- Identification Number Field -->
              <div class="form-group">
                  <?php echo $form->labelEx($model,'asisten_dokter2',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($model,'asisten_dokter2',array('class'=>'input-large'));
                        echo $form->error($model,'asisten_dokter2');
                    ?>
              
                  </div>
                </div>

                <!-- Patient's Name Field -->
                

                <!-- Patient's Sex Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($model,'spesimen',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php 
                      echo $form->radioButtonList($model,'spesimen',array('1'=>'Ya','0'=>'Tidak'),array('separator'=>' ',
'labelOptions'=>array('style'=>'display:inline'), // add this code
));
                        echo $form->error($model,'spesimen');
                    ?>
              
                  </div>
                </div>

               <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Submit
          </button>

        
        </div>
      </div>
          </div>
        </div>
      </div>
    </div>

</div>

<?php $this->endWidget(); ?>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'mydialog',
    // additional javascript options for the dialog plugin
    'options'=>array(
        'title'=>'Tindakan OK',
        'autoOpen'=>false,
        'width' => '700',
        'height' => '500'
    ),
));

?>
<table class="table table-striped" id="tabel-tindakan">
  <tr>
    <th>No</th>
    <th>Gol Operasi</th>
    <th>Jenis</th>
    <th>Kelas</th>
    <th>Action</th>
  </tr>
  <?php
  $i = 0; 
  foreach($tindakan as $t)
  {
    $i++;
  ?>
  <tr>
    <td><?=$i;?></td>
    <td><?=$t->dmGolOperasi->nama?></td>
    <td><?=$t->dmOkJenisTindakan->nama?></td>
    <td><?=$t->dmKelas->nama_kelas?></td>
    <td><button class="btn btn-success pilih">Pilih</button>
      <input type="hidden" class="id_tindakan" value="<?=$t->id;?>"/>
      <input type="hidden" class="jenis_tindakan" value="<?=$t->dm_ok_jenis_tindakan_id;?>"/>
      <input type="hidden" class="kelas_tindakan" value="<?=$t->dm_kelas_id;?>"/>
      <input type="hidden" class="gol_tindakan" value="<?=$t->dm_gol_operasi_id;?>"/>
    </td>
  </tr>
  <?php 
  }
  ?>
</table>
<?php

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<script type="text/javascript">

$(document).ready(function(){
  $('.pilih').on('click',function(){
    $('#TdRegisterOk_tindakan').val($(this).parent().find('.id_tindakan').val());
    $('#nama_tindakan').val($(this).parent().find('.nama_tindakan').val());
    $('#TdRegisterOk_jenis_operasi').val($(this).parent().find('.gol_tindakan').val());
    $('#TdRegisterOk_cito_elektif').val($(this).parent().find('.jenis_tindakan').val());
    $("#mydialog").dialog("close");
  });
});	

$(document).bind("keyup.autocomplete",function(){

   $('.nama_dokter_merawat').autocomplete({
      minLength:1,
      select:function(event, ui){
        $(this).next().val(ui.item.id);
        
      },
      focus: function (event, ui) {
       $(this).next().val(ui.item.id);
      
      },
      source:function(request, response) {
        $.ajax({
                url: "<?php echo Yii::app()->createUrl('AjaxRequest/GetDokter');?>",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 
});

</script>