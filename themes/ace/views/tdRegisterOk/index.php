 <?php
$this->breadcrumbs=array(
array('name' => 'Data OK','url'=>Yii::app()->createUrl('trRegisterOk/index')),
  array('name' => 'List'),
);

?>

<style>
  .errorMessage, .errorSummary{
    color:red;
  }
</style>
<div class="row">
  <div class="col-xs-12">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>

      <div class="form-group">
          <?php echo CHtml::label('Nama Pasien','',array('class'=>'col-sm-3 control-label no-padding-right'));?>
          <div class="col-sm-9">
            <?php
              $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'value' =>!empty($_POST['PASIEN_NAMA'])? $_POST['PASIEN_NAMA'] : '',
          'name' => 'PASIEN_NAMA',
          'source'=>'js: function(request, response) {
            $.ajax({
                    url: "'.$this->createUrl('AjaxRequest/getPasienInRawatInap').'",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
          'htmlOptions'=>array('placeholder'=>'Ketik Nama | No Reg','class'=>'input-sm'),    
          'options' => array(
              'minLength' => 1,
              'showAnim' => 'fold',
              'select' => 'js:function(event, ui){ 
                isiDataPasien(ui);
              }',
      ),
      ));
                 
            ?>
            
            
          </div>
        </div>

           <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">No RM</label>
        <div class="col-sm-9">
          <input type="text" class="input-sm" name="cari_no_rm" id="cari_no_rm" value="<?php echo !empty($_POST['cari_no_rm'])? $_POST['cari_no_rm'] : '';?>"/>
        </div>
      </div>

   
      <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Cari Pasien
          </button>

        
        </div>
      </div>
      <?php

    $this->endWidget();?>
  </div>
</div>

<script type="text/javascript">
	
function isiDataPasien(ui)
{
	 
   $("#cari_no_rm").val(ui.item.id);
   $("#PASIEN_NAMA").val(ui.item.value); 
   

}

</script>
 