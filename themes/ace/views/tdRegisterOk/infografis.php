  <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();
   
  ?>
<div class="row">
  <div class="col-xs-12">
      
    <div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">DATA TINDAKAN : CITO DAN ELEKTIF</h3>
        <p class="panel-subtitle">Tahun : <?=date('Y');?></p>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <td rowspan="2" style="text-align: center; vertical-align: middle;">LAYANAN SPESIALIS</td>
                            <?php 
                            foreach($listGol as $q => $v){
                            ?>
                            <td colspan="2" style="text-align: center; vertical-align: middle;"><?=$v;?></td>
                          <?php }?>
                             <td colspan="4" style="text-align: center; vertical-align: middle;">&Sigma; Total & %</td>
                             <td  style="text-align: center; vertical-align: middle;">&Sigma;Total</td>
                        </tr>
                        <tr>
                          <?php 
                            foreach($listGol as $q => $v){
                              echo '<td>E</td>';
                              echo '<td>C</td>';
                            }
                            ?>
                            <td>E</td>
                            <td>%</td>
                            <td>C</td>
                            <td>%</td>
                            <td></td>
                        </tr>
                        
                    </thead>
                    <tbody>
                      <?php 

                      $total_v['KHUSUS']['ELEKTIF'] = [];
                      $total_v['SEDANG']['ELEKTIF'] = [];
                      $total_v['BESAR']['ELEKTIF'] = [];
                      $total_v['KECIL']['ELEKTIF'] = [];
                      $total_v['KHUSUS']['CITO'] = [];
                      $total_v['SEDANG']['CITO'] = [];
                      $total_v['BESAR']['CITO'] = [];
                      $total_v['KECIL']['CITO'] = [];
                      foreach($listUpf as $upf)
                      {


                      ?>
                        <tr>
                           
                            <td><?=$upf->nama;?></td>
                            <?php 
                            $subtotal_e = 0;
                            $subtotal_c = 0;
                            foreach($listGol as $q => $v){

                              $subtotal_c += $dataTable[$upf->id][$v]['CITO'];
                              $subtotal_e += $dataTable[$upf->id][$v]['ELEKTIF'];
                              echo '<td>'.$dataTable[$upf->id][$v]['ELEKTIF'].'</td>';
                              echo '<td>'.$dataTable[$upf->id][$v]['CITO'].'</td>';
                              $total_v[$v]['ELEKTIF'][] = $dataTable[$upf->id][$v]['ELEKTIF'];
                              $total_v[$v]['CITO'][] = $dataTable[$upf->id][$v]['CITO'];
                            }

                            $subtotal = $subtotal_c + $subtotal_e;
                            $persen_c = 0;
                            $persen_e = 0;
                            if($subtotal != 0){
                              $persen_e = $subtotal_e / $subtotal * 100;
                              $persen_c = $subtotal_c / $subtotal * 100;
                            }
                            ?>
                            <td><?=$subtotal_e;?></td>
                            <td>0</td>
                            <td><?=$subtotal_c;?></td>
                            <td>0</td>
                            <td><?=$subtotal;?></td>
                        </tr>
                        <?php 
                      }
                      $total_e = array_sum($total_v['KHUSUS']['ELEKTIF']) + array_sum($total_v['BESAR']['ELEKTIF']) + array_sum($total_v['SEDANG']['ELEKTIF']) + array_sum($total_v['KECIL']['ELEKTIF']);

                      $total_c = array_sum($total_v['KHUSUS']['CITO']) + array_sum($total_v['BESAR']['CITO']) + array_sum($total_v['SEDANG']['CITO']) + array_sum($total_v['KECIL']['CITO']);
                      // print_r($total_v['KHUSUS']['ELEKTIF']);exit;
                        ?>
                       
                    </tbody>
                    <tfoot>
                        <tr>
                            <td >TOTAL</td>
                            <td><?=array_sum($total_v['KHUSUS']['ELEKTIF']);?></td>
                            <td><?=array_sum($total_v['KHUSUS']['CITO']);?></td>
                            <td><?=array_sum($total_v['BESAR']['ELEKTIF']);?></td>
                            <td><?=array_sum($total_v['BESAR']['CITO']);?></td>
                            <td><?=array_sum($total_v['SEDANG']['ELEKTIF']);?></td>
                            <td><?=array_sum($total_v['SEDANG']['CITO']);?></td>
                            <td><?=array_sum($total_v['KECIL']['ELEKTIF']);?></td>
                            <td><?=array_sum($total_v['KECIL']['CITO']);?></td>
                            <td><?=$total_e;?></td>
                            <td>-</td>
                            <td><?=$total_c;?></td>
                            <td>-</td>
                            <td><?=$total_e + $total_c;?></td> 
                        </tr>
                    </tfoot>
                </table>
            </div>   
        </div>
    </div>
</div>
</div>
<script type="text/javascript">

</script>