<?php
/* @var $this SiteController */
$this->breadcrumbs=array(
    array('name' => 'Data Rawat','url'=>Yii::app()->createUrl('trRawatInap/obatPasien')),
                           
                            array('name' => 'Input Tindakan')
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>


<div class="row">
    <div class="col-xs-12">

                      <div class="pull-right"> 
                       Data per halaman
                            <?php echo CHtml::dropDownList('trRawatInap[PAGESIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,20=>20,30=>30,),array('id'=>'size')); ?>
                      Jenis Pasien 
                            <?php 
$list_gol = CHtml::listData(GolPasien::model()->findAllByAttributes(array('IsKaryawan'=>1)),'KodeGol','NamaGol');
echo CHtml::dropDownList('trRawatInap[TIPE_PASIEN]',isset($_GET['jenis_pasien'])?$_GET['jenis_pasien']:'',$list_gol,array('id'=>'jenis_pasien','empty' => 'Semua')); ?> 
                      <?php
        echo CHtml::textField('trRawatInap[SEARCH]','',array('placeholder'=>'Cari','id'=>'search')); 
        ?></div> 
                    
<?php 
    
 // $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts.ri_navigation'); 
 //  $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts.nav_dataPasien'); 



?>  
<?php $this->renderPartial('_listPasien',array(
        'model' => $model
    )); ?>
                    </div>
                </div>
          