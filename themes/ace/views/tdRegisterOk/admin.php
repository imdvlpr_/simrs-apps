
<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */
$this->breadcrumbs=array(
     array('name' => 'Data OK','url'=>Yii::app()->createUrl('TdRegisterOk/admin')),
                            array('name' => 'List')
);
?>

<h2>Manage Data OK</h2>

<script type="text/javascript">
    $(document).ready(function(){
        $('#search,#size,#status_ok').change(function(){
            $('#td-register-ok-grid').yiiGridView.update('td-register-ok-grid', {
                url:'<?php echo Yii::app()->createUrl("TdRegisterOk/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()+'&status_ok='+$('#status_ok').val()
            });
        });
        
    });
</script>
<div class="row">
    <div class="col-xs-12">
        <!-- <div class="span3" id="sidebar">


        </div> -->

        <!--/span-->
       
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>


              
                        <div class="pull-left"> <?php    echo CHtml::link('Tambah Baru',array('TdRegisterOk/listPasien'),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('TdRegisterOk[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                            Status OK
                             <?php echo CHtml::dropDownList('TdRegisterOk[status_ok]',isset($_GET['status_ok'])?$_GET['status_ok']:'',array(1=>'Belum Operasi',2=>'Sedang Operasi',3=>'Sudah Operasi'),array('id'=>'status_ok','empty'=>'.:Semua:.')); 
                            ?> 
                             <?php        echo CHtml::textField('TdRegisterOk[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?>      </div>
                    

<?php $this->widget('application.components.ComplexGridView', array(
    'id'=>'td-register-ok-grid',
    'dataProvider'=>$model->search(),
    
    'columns'=>array(
    array(
        'header'=>'No',
        'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
        ),
        // 'id_ok',
       
        array(
            'header'=> 'No RM',
            'value' => '$data->pASIEN->NoMedrec'
        ),
        array(
            'header'=> 'Pasien',
            'value' => '$data->pASIEN->NAMA." / ".$data->kodeDaftar->kodeGol->NamaGol'
        ),
        'tgl_operasi',
        'kode_daftar',
        'diagnosa',
        [
          'header' => 'Status OK',
          'type' => 'raw',
          'value' => function($data){
            switch($data->status_ok){
                case 1 :
                    return "<button class='btn btn-danger btn-sm btn-block'>Belum Operasi</button>";
                break;
                case 2 :
                    return "<button class='btn btn-warning btn-sm btn-block'>Sedang Operasi</button>";
                break;
                case 3 :
                    return "<button class='btn btn-success btn-sm btn-block'>Sudah Operasi</button>";
                break;
            }
          }
        ],
        /*
        'tindakan',
        'upf',
        'jenis_operasi',
        'jenis_anastesi',
        'ruang',
        'kelas',
        'dr_operator',
        'dr_anastesi',
        'asisten_dokter1',
        'asisten_dokter2',
        'id_rawat_inap',
        'created',
        'status_ok',
        */
        array(
            'class'=>'CButtonColumn',
        ),
    ),
    'htmlOptions'=>array(
                                    'class'=>'table'
                                ),
                                'pager'=>array(
                                    'class'=>'SimplePager',
                                    'header'=>'',
                                    'firstPageLabel'=>'Pertama',
                                    'prevPageLabel'=>'Sebelumnya',
                                    'nextPageLabel'=>'Selanjutnya',
                                    'lastPageLabel'=>'Terakhir',
                                    'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
                                    'hiddenPageCssClass'=>'disabled',
                                     'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
                                    'maxButtonCount'=>5
                                ),
                                'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
                                'summaryCssClass'=>'table-message-info',
                                'filterCssClass'=>'filter',
                                'summaryText'=>'menampilkan {start} - {end} dari {count} data',
                                'template'=>'{items}{summary}{pager}',
                                'emptyText'=>'Data tidak ditemukan',
  'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


                    </div>
                </div>
         