<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */

$this->breadcrumbs=array(
	 array('name' => 'Data OK','url'=>Yii::app()->createUrl('tdRegisterOk/admin')),
  array('name' => 'View'),
);


?>
<p>
<div class="row"><div class="col-sm-6">
	
	<a href="<?=Yii::app()->createUrl('TdRegisterOk/update',array('id'=>$model->id_ok));?>" class="btn btn-info"><i class="fa fa-pencil"></i> Update Data</a>
	<a href="<?=Yii::app()->createUrl('TdRegisterOk/print',array('id'=>$model->id_ok));?>" class="btn btn-success"><i class="fa fa-print"></i> Print Data</a>
	
</div>
<div class="col-sm-6">
	<?php 
	$btn = '';
	$btnTxt = '';
	switch ($model->status_ok) {
		case 1:
			$btn = 'danger';
			$btnTxt = 'Belum Operasi';
			break;
		case 2:
			$btn = 'warning';
			$btnTxt = 'Sedang Operasi';
			break;
		case 3:
			$btn = 'success';
			$btnTxt = 'Sudah Operasi';
			break;
	}
	?>
	Status OK : <button id="status-ok-label" class="btn btn-<?=$btn;?>"><?=$btnTxt;?></button>
	<?php 
	echo CHtml::link('<button class="btn" id="btn-update-status">Update Status</button>', '#', array(
   'onclick'=>'$("#mydialog").dialog("open"); return false;',
));
	?>
	
	
</div>
</div>
</p>
<div class="row">
<div class="col-sm-6">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		'no_rm',
		'kode_daftar',	
		array(
			'label' => 'Nama Px',
			'value'=>$model->pASIEN->NAMA
		),
		array(
			'label' => 'Unit',
			'value'=>$model->unit->NamaUnit
		),
		array(
			'label' => 'Kelas',
			'value'=>$model->tindakan0->dmKelas->nama_kelas
		),
		array(
			'label' => 'UPF',
			'value'=>$model->dataUpf->nama
		),
		'tgl_operasi',
		'diagnosa',
		
		

	),
)); 
?>
</div>
<div class="col-sm-6">
	<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		
		
		[
			'label' => 'Gol Operasi',
			'value' => $model->jenisOperasi->nama 
		],
		[
			'label' => 'Jenis AN',
			'value' => $model->jenisAnastesi->kode.' ('.$model->jenisAnastesi->nama.')' 
		],
		array(
			'label' => 'Dr Anas',
			'value'=>$model->drAnastesi->FULLNAME
		),
		'asisten_dokter1',
		'asisten_dokter2',
		[
			'label' => 'Keterangan',
			'value' => $model->tindakan0->dmOkJenisTindakan->nama 
		],
		[
			'label' => 'Kelas Tindakan',
			'value' => $model->tindakan0->dmKelas->nama_kelas 
		],
		[
			'label' => 'Spesimen',
			'value' => $model->spesimen == 1? 'Ya' : 'Tidak' 
		],
		
	),
)); 
?>
</div>
</div>
<h1>Data Rincian Tindakan</h1>
<p><a class="btn btn-info" href="<?=Yii::app()->createUrl('tdOkBiaya/create',array('id_ok'=>$model->id_ok));?>"><i class="icon-edit"></i> Input Tindakan</a>
</p>

<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'td-ok-biaya-grid',
	'dataProvider'=>$modelTop->search($model->id_ok),
	
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		
		 array(
            'header'=> 'Dr OP',
            'value' => '$data->drOperator->FULLNAME'
        ),
		 'biaya_jrs',
		 'biaya_dr_op',
		
		  'biaya_dr_anas',
		  'biaya_prwt_ok',
		  'biaya_prwt_anas',

		
		array(
			'class'=>'CButtonColumn',
			'template' => '{view} {update} {delete}',
			'buttons' => array(
				'view' => array(
					'url'=>'Yii::app()->createUrl("tdOkBiaya/view/", array("id"=>$data->id))',   
				),

				'update' => array(
					'url'=>'Yii::app()->createUrl("tdOkBiaya/update/", array("id"=>$data->id))',
				),

				'delete' => array(
					'url'=>'Yii::app()->createUrl("tdOkBiaya/delete/", array("id"=>$data->id))',   
				),
			),
		),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									   'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>

<script>
	$(document).ready(function(){
		$('.btn-status').on('click',function(){
			var r = confirm("Ubah Status Px OK ? ");
			if(r){
				$.ajax({
					type : 'POST',
					url : '<?=Yii::app()->createUrl("ajaxRequest/inputOk");?>',
					data : 'idok='+$("#id_register_ok").val()+'&sok='+$(this).next().val(),
					beforeSend : function(){
						$('#loading').show();
					},
					success : function(res){
						var hsl = jQuery.parseJSON(res);
						if(hsl.code == 'success')
						{
							$('#loading').hide();
							$("#mydialog").dialog("close");
							$("#status-ok-label").attr('class','btn btn-'+hsl.btn);
							$('#status-ok-label').html(hsl.btnTxt);
						}
					}
				});
			}
		});
	});
</script>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'mydialog',
    // additional javascript options for the dialog plugin
    'options'=>array(
        'title'=>'Pilih Status Px OK',
        'autoOpen'=>false,
        'width' => '500',
        'height' => 'auto'
    ),
));

?>
<button class="btn btn-danger btn-status" id="btn-blm">Belum Operasi</button>
<input type="hidden" value="1"/>
<button class="btn btn-warning btn-status" id="btn-sdg">Sedang Operasi</button>
<input type="hidden" value="2"/>
<button class="btn btn-success btn-status" id="btn-sdh">Sudah Operasi</button>
<input type="hidden" value="3"/>
<input type="hidden" id="id_register_ok" value="<?=$model->id_ok;?>"/>
<img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
<?php

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>