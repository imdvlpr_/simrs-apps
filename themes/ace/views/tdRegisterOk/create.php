<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */

$this->breadcrumbs=array(
  array('name' => 'Data OK','url'=>Yii::app()->createUrl('tdRegisterOk/admin')),
  array('name' => 'List'),
);

?>
<style type="text/css">
    .errorMessage{
        color:red;
    }
</style>
<div class="row">
              <div class="col-xs-12">


<h1>Create OK</h1>

        <?php $this->renderPartial('_form', array(
        	'model'=>$model,
            'pasien'=>$pasien,
            'listUnit' => $listUnit,
            'tindakan' => $tindakan
        )); ?>
    </div></div>