<?php
/* @var $this TdRegisterOkController */
/* @var $data TdRegisterOk */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_ok')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_ok), array('view', 'id'=>$data->id_ok)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_urut')); ?>:</b>
	<?php echo CHtml::encode($data->no_urut); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_rm')); ?>:</b>
	<?php echo CHtml::encode($data->no_rm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_operasi')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_operasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_ok')); ?>:</b>
	<?php echo CHtml::encode($data->jam_ok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('diagnosa')); ?>:</b>
	<?php echo CHtml::encode($data->diagnosa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->tindakan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_operasi')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_operasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_anastesi')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_anastesi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dr_operator')); ?>:</b>
	<?php echo CHtml::encode($data->dr_operator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dr_anastesi')); ?>:</b>
	<?php echo CHtml::encode($data->dr_anastesi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('asisten_dokter1')); ?>:</b>
	<?php echo CHtml::encode($data->asisten_dokter1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('asisten_dokter2')); ?>:</b>
	<?php echo CHtml::encode($data->asisten_dokter2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cito_elektif')); ?>:</b>
	<?php echo CHtml::encode($data->cito_elektif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spesimen')); ?>:</b>
	<?php echo CHtml::encode($data->spesimen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_rawat_inap')); ?>:</b>
	<?php echo CHtml::encode($data->id_rawat_inap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_ok')); ?>:</b>
	<?php echo CHtml::encode($data->status_ok); ?>
	<br />

	*/ ?>

</div>