<?php
/* @var $this TdRegisterOkController */
/* @var $model TdRegisterOk */

$this->breadcrumbs=array(
	  array('name' => 'Data OK','url'=>Yii::app()->createUrl('tdRegisterOk/admin')),
  array('name' => 'List'),
	
);

$this->menu=array(
	array('label'=>'List TdRegisterOk', 'url'=>array('index')),
	array('label'=>'Create TdRegisterOk', 'url'=>array('create')),
	array('label'=>'View TdRegisterOk', 'url'=>array('view', 'id'=>$model->id_ok)),
	array('label'=>'Manage TdRegisterOk', 'url'=>array('admin')),
);
?>
<div class="row">
              <div class="col-xs-12">


<h1>Update Data OK <?php echo $model->id_ok; ?></h1>

		<?php $this->renderPartial('_form', array(
			'model'=>$model,
			'pasien'=>$pasien,
			'tindakan' => $tindakan,
			'listUnit' => $listUnit,
		)); ?>
	</div></div>