 <?php
$this->breadcrumbs=array(
	array('name'=>'Dm Lab Item Tarif','url'=>array('index')),
	array('name'=>'Update'),
);

?>



<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>
<div class="row">
	<div class="col-xs-12">
	<?php $this->renderPartial('_form', [
		'model'=>$model,
		'listKelas' => $listKelas,
		'listItem' => $listItem
	]); ?>
	</div>
</div>
