<?php
/* @var $this DmLabItemTarifController */
/* @var $model DmLabItemTarif */

$this->breadcrumbs=array(
	array('name'=>'Dm Lab Item Tarif','url'=>array('admin')),
	array('name'=>'Dm Lab Item Tarif'),
);

$this->menu=array(
	array('label'=>'List DmLabItemTarif', 'url'=>array('index')),
	array('label'=>'Create DmLabItemTarif', 'url'=>array('create')),
	array('label'=>'Update DmLabItemTarif', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmLabItemTarif', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmLabItemTarif', 'url'=>array('admin')),
);
?>

<h1>View DmLabItemTarif #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_id',
		'kelas_id',
		'bhp',
		'jrs',
		'japel',
		'jumlah',
		'created_at',
		'updated_at',
		'is_removed',
	),
)); ?>
	</div>
</div>
