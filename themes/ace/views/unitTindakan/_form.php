<?php
/* @var $this UnitTindakanController */
/* @var $model UnitTindakan */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'unit-tindakan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'unit_id', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		<?= $form->dropDownList($model, 'unit_id',$listUnit,array('prompt'=>'.: Pilih Unit :.'));?>
		<?php echo $form->error($model,'unit_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dm_tindakan_id', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'3')); ?>
		<div class="col-sm-9">
		<?= $form->dropDownList($model, 'dm_tindakan_id',$listTindakan,array('prompt'=>'.: Pilih Tindakan :.'));?>
		<?php echo $form->error($model,'dm_tindakan_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'akhp', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'akhp',['class'=>'digit']); ?>
		<?php echo $form->error($model,'akhp'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jrs', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jrs',['class'=>'digit']); ?>
		<?php echo $form->error($model,'jrs'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jaspel', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'jaspel',['class'=>'digit']); ?>
		<?php echo $form->error($model,'jaspel'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'nilai', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'nilai',['readonly'=>'readonly']); ?>
		<?php echo $form->error($model,'nilai'); ?>
		</div>
	</div>



	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(document).on("keyup","input.digit",function(event){
		
	// skip for arrow keys
		if(event.which >= 37 && event.which <= 40) return;

		// format number
		$(this).val(function(index, value) {
		  return value
		  .replace(/\D/g, "")
		  .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		  ;
		});
	});

	$(document).on('keydown','input', function(e) {

	    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	    
	    if(key == 13) {
	        e.preventDefault();
	        var inputs = $(this).closest('#unit-tindakan-form').find(':input:visible');
	              
	        inputs.eq( inputs.index(this)+ 1 ).focus().select();

	        var akhp = removePoints($('#UnitTindakan_akhp').val());
	        akhp = isNaN(akhp) ? 0 : akhp;

	        var jrs = removePoints($('#UnitTindakan_jrs').val());
	        jrs = isNaN(jrs) ? 0 : jrs;

	        var jaspel = removePoints($('#UnitTindakan_jaspel').val());
	        jaspel = isNaN(jaspel) ? 0 : jaspel;
	        var nilai = eval(akhp) + eval(jrs) + eval(jaspel);
	        

	        $('#UnitTindakan_nilai').val(addCommas(nilai));
	    }
	});
	

</script>