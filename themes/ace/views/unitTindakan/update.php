 <?php
$this->breadcrumbs=array(
	array('name'=>'Unit Tindakan','url'=>array('admin')),
	array('name'=>'Update'),
);

?>



<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>
<div class="row">
	<div class="col-xs-12">
	<?php $this->renderPartial('_form', [
		'model'=>$model,
		'listUnit' => $listUnit,
		'listTindakan' => $listTindakan
	]); ?>
	</div>
</div>
