<?php

$fontfamily = 'Times';
$fontSize = '12px';
$fontSizeBawah = '8px';
?>
<table width="100%" style="height: 1px;margin: 0px">
    <tr>
        <td width="10%"></td>
        <td width="80%" style="text-align: center">
            <strong style="font-size: 8px;font-family: <?=$fontfamily;?>">RSUD KABUPATEN KEDIRI</strong><br>
            <span style="font-size:7px;font-family: <?=$fontfamily;?>">Jl. PAHLAWAN KUSUMA BANGSA NO 1 TLP (0354) 391718, 391169, 394956 FAX. 391833<BR>
            PARE KEDIRI (64213) email : rsud.pare@kedirikab.go.id</span>
        </td>
        <td width="10%"></td>
    </tr>
</table>
<hr style="height: 1px;margin: 0px">
<div style="text-align: center;margin: 0px;font-size:8px;font-family: <?=$fontfamily;?>">SURAT PENGANTAR BAYAR POLI</div>
<table cellspacing="0" cellpadding="0"  style="margin-bottom: 3px;font-size: <?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
    <tr>
        <td style="width: 25%">No</td>
        <td  style="width: 5%">:</td>
        <td style="width: 70%"><?=Yii::app()->helper->appendZeros($model->id,8);?></td>
    </tr>
    <tr>
        <td >Tgl </td>
        <td>:</td>
        <td><?=date('d-m-Y');?></td>
    </tr>

     
     <tr>
        <td >No RM</td>
        <td>:</td>
        <td><?=$model->bPendaftaranRjalan->noDaftar->noMedrec->NoMedrec;?></td>
    </tr>
      <tr>
        <td >Jenis Px</td>
        <td>:</td>
        <td><?=$model->bPendaftaranRjalan->noDaftar->kodeGol->NamaGol;?></td>
    </tr>
    <tr>
        <td >Nama Px</td>
        <td>:</td>
        <td><?=$model->bPendaftaranRjalan->noDaftar->noMedrec->NAMA;?></td>
    </tr>
<tr>
        <td >Unit</td>
        <td>:</td>
        <td><?=$model->bPendaftaranRjalan->unit->NamaUnit;?></td>
    </tr>
    <tr>
        <td >Dokter</td>
        <td>:</td>
        <td><?=$model->dokter->FULLNAME;?></td>
    </tr>
    <tr>
        <td style="font-size: 9px">Nominal</td>
        <td>:</td>
        <td style="font-weight: bold;font-size: 9px">Rp <?php
         $total = ceil($model->biaya/100) * 100;
        echo  Yii::app()->helper->formatRupiah($total);
        ?></td>
    </tr>
</table>
<table width="100%" cellspacing="0" cellpadding="1" >
    <tr>
        
        <td width="100%" style="text-align: center;font-size:<?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
            <br><br>
            Pare, <?=date('d-m-Y');?>
            <br>

            Petugas Apotek
           
            <br>
            <br>
            <br>
            <u><b>(<?=Yii::app()->user->getState('nama');?>)</b></u>
            <br>
            
            
        </td>
    </tr>
</table>
