<?php
/* @var $this BPendaftaranRjalanTindakanController */
/* @var $model BPendaftaranRjalanTindakan */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bpendaftaran-rjalan-tindakan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dm_poli_tindakan_id', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		<?php 

		echo  $form->dropDownList($model,'dm_poli_tindakan_id',CHtml::ListData($listTindakan,'id',function($data){
			return $data->kode.' - '.$data->nama;
		}));
		 ?>
		  <img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
		<?php echo $form->error($model,'dm_poli_tindakan_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dokter_id', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'2')); ?>
		<div class="col-sm-9">
		<?php 
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'model' => $model,
                      'attribute' => 'nama_dokter',
                      'source'=>'js: function(request, response) {
                        $.ajax({
                                url: "'.$this->createUrl('AjaxRequest/getDokter').'",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }',
                      'htmlOptions' =>[
                      	'placeholder' => 'Ketik nama dokter'
                      ],
                      'options' => array(
                          'minLength' => 2,
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui){ 

                            $("#BPendaftaranRjalanTindakan_dokter_id").val(ui.item.id);
                          }',
                  ),
                  ));
	
		 ?>
		 <?php echo $form->hiddenField($model,'dokter_id'); ?>
		<?php echo $form->error($model,'dokter_id'); ?>
		</div>
	</div>
	 
	<div class="form-group">
		<?php echo $form->labelEx($model,'tanggal', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'4')); ?>
		<div class="col-sm-9">
		<?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'tanggal',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>

		<?php echo $form->error($model,'tanggal'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'akhp', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->numberField($model,'akhp'); ?>
		<?php echo $form->error($model,'akhp'); ?>
		</div>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'jrs', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->numberField($model,'jrs'); ?>
		<?php echo $form->error($model,'jrs'); ?>
		</div>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'jaspel', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->numberField($model,'jaspel'); ?>
		<?php echo $form->error($model,'jaspel'); ?>
		</div>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'biaya', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->numberField($model,'biaya'); ?>
		<?php echo $form->error($model,'biaya'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'keterangan', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'6')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'keterangan',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'keterangan'); ?>
		</div>
	</div>


	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>

<script>
	$(document).ready(function(){
		$('#BPendaftaranRjalanTindakan_dm_poli_tindakan_id').change(function(){
			var item = new Object;
			item.poli_tindakan_id = $(this).val();
			$.ajax({
				type : 'post',
				url : '<?=Yii::app()->createUrl('ajaxRequest/getItemTindakanPoli');?>',

				data : {dataItem:item},
				beforeSend : function(){
				  $('#loading').show();
				},
				success : function(res){
				  var res = $.parseJSON(res);
				  alert(res.result.jrs);
				  $('#loading').hide();
				  
				  
				  
				},
			});
		});
	});
</script>