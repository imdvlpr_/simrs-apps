<?php

$fontfamily = 'Times';
$fontSize = '20px';
$fontSizeBawah = '10px';
?>
<table width="100%" style="height: 1px;margin: 0px">
    <tr>
        <td width="10%"></td>
        <td width="80%" style="text-align: center">
            <strong style="font-size: 11px;font-family: <?=$fontfamily;?>">RSUD KABUPATEN KEDIRI</strong><br>
            <span style="font-size:10px;font-family: <?=$fontfamily;?>">Jl. PAHLAWAN KUSUMA BANGSA NO 1 TLP (0354) 391718, 391169, 394956 FAX. 391833<BR>
            PARE KEDIRI (64213) email : rsud.pare@kedirikab.go.id</span>
        </td>
        <td width="10%"></td>
    </tr>
</table>
<hr style="height: 1px;margin: 0px">
<div style="text-align: center;margin: 0px;font-size:10px;font-family: <?=$fontfamily;?>">NO KWITANSI : <?=Yii::app()->helper->appendZeros($model->id,8);?></div>
<table width="100%" cellpadding="1" cellspacing="0" border="1">
    <tr>
        <td width="55%" valign="top" >
            

<table style="font-size: <?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
    
     <tr>
        <td style="width: 100px" >No RM</td>
        <td style="width: 20px">:</td>
        <td style="width: 250px"><?=$model->bPendaftaranRjalan->noDaftar->noMedrec->NoMedrec;?></td>
    </tr>
     <tr>
        <td >Jenis Px</td>
        <td>:</td>
        <td><?=$model->bPendaftaranRjalan->noDaftar->kodeGol->NamaGol;?></td>
    </tr>
    <tr>
        <td >Nama Px</td>
        <td>:</td>
        <td><?=$model->bPendaftaranRjalan->noDaftar->noMedrec->NAMA;?></td>
    </tr>

    <tr>
        <td >Alamat</td>
        <td>:</td>
        <td><?=$model->bPendaftaranRjalan->noDaftar->noMedrec->ALAMAT;?></td>
    </tr>
    <tr>
        <td >Unit</td>
        <td>:</td>
        <td><?=$model->bPendaftaranRjalan->unit->NamaUnit;?></td>
    </tr>
   
    <tr>
        <td >Tgl</td>
        <td>:</td>
        <td><?=date('d/m/Y');?></td>
    </tr>
    <tr>
        <td >Dokter</td>
        <td>:</td>
        <td><?=$model->dokter->FULLNAME;?></td>
    </tr>
    <tr>
        <td >Untuk Pembayaran</td>
        <td>:</td>
        <td><?=$model->keterangan;?></td>
    </tr>
</table>
        </td>
        <td width="45%"  valign="top">
    <table style="font-size: <?=$fontSizeBawah;?>;font-family: <?=$fontfamily;?>">
    
    
     <tr>
        <td  width="25%">Nominal</td>
        <td width="5%">:</td>
        <td width="70%" style="font-weight: bold;text-align: center;">Rp <?php
        $total = ceil($model->biaya/100) * 100;
        echo  Yii::app()->helper->formatRupiah($total);
        
        ?></td>
    </tr>
    <tr>
        <td width="25%">Terbilang</td>
        <td width="5%">:</td>
        <td width="70%" style="text-align: left"><?=ucwords(MyHelper::terbilang($total));?></td>
    </tr>
</table>
<br><br>
<table width="100%">
    <tr>
        <td width="50%" style="text-align: center;font-size:10px;font-family: <?=$fontfamily;?>">
            <br><br>Penyetor
           
            <br>
            <br>
            <br>
            <u><b>(........................)</b></u><br>
        </td>
        <td width="50%" style="text-align: center;font-size:10px;font-family: <?=$fontfamily;?>">
            
            Pare, <?=date('d M Y');?>
            <br>

            Petugas Kasir
           
            <br>
            <br>
            <br>
            <u><b>(<?=Yii::app()->user->getState('nama');?>)</b></u><br>
            

            
        </td>
    </tr>
</table>
</td>
        
    </tr>
</table>

