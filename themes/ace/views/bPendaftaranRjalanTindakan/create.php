 <?php
$this->breadcrumbs=array(
	array('name'=>'Rawat Jalan','url'=>array('bPendaftaranRjalan/index')),
	array('name'=>'Rincian Tindakan','url'=>array('bPendaftaranRjalanTindakan/index','id'=>$_GET['id'])),
	array('name'=>'Create'),
);

?>

<style>
	.errorMessage, .errorSummary{
		color:red;
	}
</style>
<div class="row">
	<div class="col-xs-12">
<?php $this->renderPartial('_form', array(
	'model'=>$model,
	'listTindakan'=>$listTindakan
)); ?>
	</div>
</div>
