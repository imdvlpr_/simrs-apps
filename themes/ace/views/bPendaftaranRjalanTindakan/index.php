 <?php
$this->breadcrumbs=array(
	array('name'=>'Tindakan Poli','url'=>array('admin','id'=>$_GET['id'])),
	array('name'=>'Manage'),
);

?>
<h1>Manage Tindakan Poli</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#bpendaftaran-rjalan-tindakan-grid').yiiGridView.update('bpendaftaran-rjalan-tindakan-grid', {
			    url:'<?php echo Yii::app()->createUrl("BPendaftaranRjalanTindakan/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>

<table class="table">
   <tr>
     <td>Nama Pasien</td>
     <td>:</td>
     <td colspan="2"><strong><?php echo $pasien->NAMA.' / '.$parent->noDaftar->kodeGol->NamaGol;?></strong></td>
     
     <td>Tgl & Waktu MRS</td>
     <td>:</td>
     <td><?=$parent->noDaftar->TGLDAFTAR;?> <?=$parent->noDaftar->JamDaftar;?></td>
   </tr>
   <tr>

     <td>No. Reg</td>
     <td>:</td>
     <td colspan="2"><?php echo $pasien->NoMedrec;?>
  </td>
     
     <td>Tgl & Waktu Pelayanan</td>
     <td>:</td>
     <td>
     	<?php 
     	$jam_pelayanan = !empty($firstPelayanan) ? $firstPelayanan->created_at : date('Y-m-d H:i:s');
     	?>
 		<?=date('d/m/Y H:i:s',strtotime($jam_pelayanan));?>
     </td>
   </tr>
   
   <tr>
     <td>Alamat</td>
     <td>:</td>
     <td colspan="2"><?php echo $pasien->FULLADDRESS;?></td>
     
     <td>Poli</td>
     <td>:</td>
     <td>
  
    	<?=$parent->unit->NamaUnit;?>
     </td>

     </td>
   </tr>
    <tr>
     <td>Ket. Masuk</td>
     <td>:</td>
     <td colspan="2">	<?=$parent->noDaftar->kodeMasuk->NamaMasuk;?></td>
     
     <td>Durasi Pelayanan</td>
     <td>:</td>
     <td>
  
    	<?php 

    
    	// $date = str_replace('/', '-', $parent->noDaftar->TGLDAFTAR);
    	// $jam_registrasi = date('Y-m-d',strtotime($date)).' '.$parent->noDaftar->JamDaftar;
    	// $elapsed = Yii::app()->helper->hitungDurasi($jam_registrasi,$jam_pelayanan);
		echo !empty($firstPelayanan) ? $firstPelayanan->durasi_label : '';
    	// echo $jam_registrasi;
    	?>

     </td>

     </td>
   </tr>
 </table>


                        <div class="pull-left"> <?php	echo CHtml::link('Tambah Baru',array('BPendaftaranRjalanTindakan/create','id'=>$_GET['id']),array('class'=>'btn btn-success'));
?></div>


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('BPendaftaranRjalanTindakan[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('BPendaftaranRjalanTindakan[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php 

$this->widget('application.components.ComplexGridView', array(
	'id'=>'bpendaftaran-rjalan-tindakan-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		'namaTindakan',

		'namaDokter',
		'tanggal',
		[
			'header' => 'Biaya',
			'value' => function($data){
				return Yii::app()->helper->formatRupiah($data->biaya);
			},
			'footer' => Yii::app()->helper->formatRupiah($model->getTotals($model->search()->getData()))

		],
		'keterangan',
		
		/*
		'created_at',
		'updated_at',
		*/
		array(
			'class'=>'CButtonColumn',
			'template' => '{view} {update} {printPengantar} {printKwitansi} {delete}',
			'buttons' => array(
				
				'printPengantar' => array(
					'url'=>'Yii::app()->createUrl("BPendaftaranRjalanTindakan/cetakPengantar/", array("id"=>$data->id))',   
					'label'=>'<i class="fa fa-print"></i>',
					'options' => array('title'=>'Cetak Pengantar','class'=>'print'),      
	            ),
				'printKwitansi' => array(
					'url'=>'Yii::app()->createUrl("BPendaftaranRjalanTindakan/cetakKwitansi/", array("id"=>$data->id))',   
					'label'=>'<i class="fa fa-print"></i>',
					'options' => array('title'=>'Cetak Bukti','class'=>'print'),      
	            ),

				

			),
		),
	),
	'htmlOptions'=>array(
		'class'=>'table'
	),
	'pager'=>array(
		'class'=>'SimplePager',
		'header'=>'',
		'firstPageLabel'=>'Pertama',
		'prevPageLabel'=>'Sebelumnya',
		'nextPageLabel'=>'Selanjutnya',
		'lastPageLabel'=>'Terakhir',
		'firstPageCssClass'=>'btn btn-info',
		'previousPageCssClass'=>'btn btn-info',
		'nextPageCssClass'=>'btn btn-info',
		'lastPageCssClass'=>'btn btn-info',
		'hiddenPageCssClass'=>'disabled',
		'internalPageCssClass'=>'btn btn-info',
		'selectedPageCssClass'=>'btn btn-sky',
		'maxButtonCount'=>5
	),
	'itemsCssClass'=>'table  table-bordered table-hover',
	'summaryCssClass'=>'table-message-info',
	'filterCssClass'=>'filter',
	'summaryText'=>'menampilkan {start} - {end} dari {count} data',
	'template'=>'{items}{summary}{pager}',
	'emptyText'=>'Data tidak ditemukan',
	'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

<script type="text/javascript">
	
function popitup(url,label,pos) {
    var w = screen.width * 0.8;
    var h = 800;
    var left = pos == 1 ? screen.width - w : 0;
    var top = pos == 1 ? screen.height - h : 0;
    
    window.open(url,label,'height='+h+',width='+w+',top='+top+',left='+left);
    
}

$(document).ready(function(){
	$('.print').click(function(e){
		e.preventDefault();
		var url = $(this).attr('href');
        popitup(url,'resep',0);
	});
});
</script>