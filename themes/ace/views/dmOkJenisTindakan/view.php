<?php
/* @var $this DmOkJenisTindakanController */
/* @var $model DmOkJenisTindakan */

$this->breadcrumbs=array(
	array('name'=>'Dm Ok Jenis Tindakan','url'=>array('admin')),
	array('name'=>'Dm Ok Jenis Tindakan'),
);

$this->menu=array(
	array('label'=>'List DmOkJenisTindakan', 'url'=>array('index')),
	array('label'=>'Create DmOkJenisTindakan', 'url'=>array('create')),
	array('label'=>'Update DmOkJenisTindakan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmOkJenisTindakan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmOkJenisTindakan', 'url'=>array('admin')),
);
?>

<h1>View DmOkJenisTindakan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
	),
)); ?>
	</div>
</div>
