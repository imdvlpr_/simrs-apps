<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl;
    $cs = Yii::app()->getClientScript();
?>

<div class="page-header">
  <h1>Formulir Data Pasien</h1>
</div>
<h4>Pengisian Formulir</h4>
<style>
  .errorMessage, .errorSummary{
    color:red;
  }
   #Pasien_TGLLAHIR{
    background : url(<?php echo Yii::app()->baseUrl;?>/images/calendar-icon.png) no-repeat scroll 7px 7px;
    padding-left:30px;
}
#ui-datepicker-div button.ui-datepicker-current {display: none;}

</style>
<div class="row-fluid">
  <?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),
  )); ?>


  <!-- ========================= BPJS FORM ================================= -->

  <div class="span6">

    <?php echo $form->errorSummary($bpjsPasien); ?>
    

<div style="display:none" class="alert alert-error" id="bpjs_error"></div>
<div style="display:none" class="alert alert-info" id="bpjs_info"></div>
    <!-- block -->
    <div class="block">
      <div class="navbar navbar-inner block-header">
        <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/bpjs-icon.png"/> Data dari BPJS</div>
      </div>
      <div class="block-content collapse in">
        <div class="span12">
          <fieldset>
            <!-- <div class="control-group">
            <label class="control-label">Jenis Pasien<span class="required">*</span></label>
            <div class="controls">
              <?php 
              //echo CHtml::dropDownList('jenis_pasien', '',array('baru' => 'Baru', 'lama' => 'Lama'));
              ?>
            </div>
          </div> -->
          <!-- <div class="control-group">
          <label class="control-label">Kepesertaan<span class="required">*</span></label>
          <div class="controls">
            <?php 
            //echo CHtml::dropDownList('kepesertaan', $jenis, array('bpjs' => 'BPJS', 'nonbpjs' => 'Non-BPJS'));
            ?>
          </div>
          </div> -->

            <!-- BPJS ID Number Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'control-label')); ?>
              <div class="controls">
                <?php echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-large', 'onkeypress' => 'return isNumber(event)')); ?>
                <a id='carinomor' class="btn btn-info"  href="javascript:void(0);"><i class="icon-search"></i> Cari Nomor</a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
                <?php echo $form->error($bpjsPasien,'NoKartu'); ?>
              </div>
            </div>

            <!-- NIK Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'NIK',array('class'=>'control-label'));?>
              <div class="controls">
              <?php echo $form->textField($bpjsPasien,'NIK',array('class'=>'input-large', 'onkeypress' => 'return isNumber(event)'));
              echo $form->error($bpjsPasien,'NIK'); ?>
              <a id='carinik' class="btn btn-info" href="javascript:void(0);"><i class="icon-search"></i> Cari NIK</a><span id='loading2' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
              </div>
            </div>

            <!-- Patient's Name Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'NAMAPESERTA',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($bpjsPasien,'NAMAPESERTA',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'NAMAPESERTA'); ?>
              </div>
            </div>

            <!-- Sex Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'KELAMIN',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                 echo $form->textField($bpjsPasien,'KELAMIN',array('class'=>'input-large','readonly'=>'readonly'));
                //echo $form->radioButtonList($bpjsPasien, 'KELAMIN', array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('labelOptions'=>array('style'=>'display:inline')));
                echo $form->error($bpjsPasien,'KELAMIN'); ?>
              </div>
            </div>

            <!-- PISAT Field -->
            <!-- This field is autofill by BPJS ID -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'PISAT',array('class'=>'control-label'));?>
              <div class="controls">
                <?php
                  echo CHtml::textField('pisat_nama','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'PISAT',array('class'=>'input-large'));
                  echo $form->error($bpjsPasien,'PISAT');
                ?>
              </div>
            </div>

            <!-- Date of Birth Field -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                echo $form->textField($bpjsPasien,'TGLLAHIR',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));
                ?>
              </div>
            </div>

            <!-- PPKTK1 Field -->
            <!-- Autofill by  BPJS ID -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'PPKTK1',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($bpjsPasien,'PPKTK1',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'PPKTK1'); ?>
              </div>
            </div>

            <!-- BPJS Member's Name Field -->
            <!-- Autofill by BPJS ID -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'PESERTA',array('class'=>'control-label'));?>
              <div class="controls">
                <?php
                  echo CHtml::textField('PESERTA_NAMA','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'PESERTA');
                  echo $form->error($bpjsPasien,'PESERTA');
                ?>
              </div>
            </div>

            <!-- BPJS Class Field -->
            <!-- Autofill by BPJS ID -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'KELASRAWAT',array('class'=>'control-label'));?>
              <div class="controls">
                <?php
                  echo CHtml::textField('KELASRAWAT_NAMA','',array('class'=>'input-large','readonly'=>'readonly'));
                  echo $form->hiddenField($bpjsPasien,'KELASRAWAT');
                  echo $form->error($bpjsPasien,'KELASRAWAT');
                ?>
              </div>
            </div>

            <!-- BPJS Card Printing Date Field -->
            <!-- Autofill by BPJS ID -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'TGLCETAKKARTU',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($bpjsPasien,'TGLCETAKKARTU',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TGLCETAKKARTU'); ?>
              </div>
            </div>

            <!-- TMT Field -->
            <!-- Autofill by BPJS ID -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'TMT',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($bpjsPasien,'TMT',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TMT'); ?>
              </div>
            </div>

            <!-- TAT Field -->
            <!-- Autofill by BPJS ID -->
            <div class="control-group">
              <?php echo $form->labelEx($bpjsPasien,'TAT',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($bpjsPasien,'TAT',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($bpjsPasien,'TAT');?>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
    </div>
    <!-- /block -->
  </div>
  <!-- ================== END OF BPJS FORM ================================= -->

  <!-- ================== PATIENT'S DETAIL FORM ============================ -->

  <div class="span6">
    <?php echo $form->errorSummary($model); ?>
    <!-- block -->
    <div class="block">
      <div class="navbar navbar-inner block-header">
        <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Pendaftaran Rumah Sakit</div>
      </div>
      <div class="block-content collapse in">
        <div class="span12">
          <fieldset>
             <div class="control-group">
                  <?php echo CHtml::label('Jenis Rawat','',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php $jenis_rawat_list = array(
                        1 => 'Rawat Inap',
                        2 => 'Rawat Jalan',

                      );
                    echo CHtml::dropDownList('jenis_rawat', '',$jenis_rawat_list); ?>
                  </div>
                </div>
            <!-- Medical Record Number Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'NoMedrec',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'NoMedrec',array('class'=>'input-large','readonly'=>'readonly'));
                echo $form->error($model,'NoMedrec'); ?>
                <!-- <a id='carirm' class="btn btn-info"  href="javascript:void(0);"><i class="icon-search"></i> Cari RM</a><span id='loading3' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span> -->
              </div>
            </div>

            <!-- ID Number (KTP) Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'NOIDENTITAS',array('class'=>'control-label'));?>
              <div class="controls">
              <?php
                echo $form->textField($model,'NOIDENTITAS',array(
                  'class'=>'input-large',
                  'placeholder' => '16 digit Nomor KTP',
                  'minLength' => '16',
                  'maxLength' => '16',
                  'onkeypress' => 'return isNumber(event)'
                ));
                echo $form->error($model,'NOIDENTITAS');
              ?>
              </div>
            </div>

            <!-- Name Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'NAMA',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'NAMA',array('class'=>'input-large'));?>
                <?php echo $form->error($model,'NAMA'); ?>
              </div>
            </div>

            <!-- Sex Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'JENSKEL',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->dropDownList($model, 'JENSKEL',array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('tabindex'=>'4'));
                echo $form->error($model,'JENSKEL'); ?>
              </div>
            </div>

            <!-- Date of Birth Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'TGLLAHIR',array('class'=>'control-label'));?>
              <div class="controls">
                <div id="datePickerComponent" class="input-append date margin-00" data-date="<?php echo date('d-m-Y')?>" data-date-format="dd-mm-yyyy">
                  <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                      'model'=>$model,
                      'value'=>'TGLLAHIR',
                      'attribute'=>'TGLLAHIR',
                      // additional JavaScript options for the date picker plugin
                      'options'=>array(
                      'showAnim'=>'fold',
                      'showButtonPanel'=>true,
                      'autoSize'=>true,
                      'dateFormat'=>'yy-mm-dd',
                      'changeMonth'=>true,
                      'changeYear'=>true,
                      'maxDate' => 'new Date()',
                      'yearRange'=>'1900:c',
                      ),
                      'htmlOptions'=>array(
                      'class'=>'input-medium',
                      ),
                      ));
                      echo $form->error($model,'TGLLAHIR',array('class'=>'control-label'));
                  ?>
                </div>
              </div>
            </div>

            <!-- Detail Address Label -->
            <div class="control-group">
              <?php echo CHtml::label('<strong>Alamat Lengkap:</strong>','',array('class'=>'control-label'));?>
              <!-- TODO: Control kosongan buat apa ? -->
              <div class="controls"></div>
            </div>

            <!-- Steet's Name Field -->
         

            <!-- District's Name Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'Desa',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'Desa',array('class'=>'input-large'));
                echo $form->error($model,'Desa'); ?>
              </div>
            </div>

            <div class="control-group">
              <?php echo $form->labelEx($model,'ALAMAT',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'ALAMAT',array('class'=>'input-large'));
                echo $form->error($model,'ALAMAT'); ?>
              </div>
            </div>          

            <!-- Patient's Detail Label -->
            <div class="control-group">
              <?php echo CHtml::label('<strong>Data Detil Pasien:</strong>','',array('class'=>'control-label'));?>
              <!-- TODO: controk kosong untuk apa ? -->
              <div class="controls"></div>
            </div>

            <!-- Place of Birth Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'TMPLAHIR',array('class'=>'control-label'));?>
              <div class="controls">
              <?php echo $form->textField($model,'TMPLAHIR',array('class'=>'input-large'));
              echo $form->error($model,'TMPLAHIR'); ?>
              </div>
            </div>

            <!-- Patient's Weigth -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'BeratLahir',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'BeratLahir',array(
                  'class'=>'input-large',
                  'onkeypress' => 'return weightNumber(event)',
                  'placeholder' => 'Berat dalam Kg. Contoh: 64.5'
                ));
                echo $form->error($model,'BeratLahir'); ?>
              </div>
            </div>

            <!-- Blood Types -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'GOLDARAH',array('class'=>'control-label'));?>
              <div class="controls">
                <?php
                  echo $form->dropDownList($model,'GOLDARAH',CHtml::ListData(DmGoldarah::model()->findAll(),'id_goldarah','nama_goldarah'),array('prompt'=>'','class'=>'span4','options'=>array($model->GOLDARAH=>array('selected'=>true))));
                  echo $form->error($model,'GOLDARAH');
                ?>
              </div>
            </div>

            <!-- Religion Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'AGAMA',array('class'=>'control-label'));?>
              <div class="controls">
              <?php
                echo $form->dropDownList($model,'AGAMA',CHtml::ListData(DmAgama::model()->findAll(array('order'=>'id_agama ASC')),'id_agama','nama_agama'),array('prompt'=>'','class'=>'span4','options'=>array($model->AGAMA=>array('selected'=>true))));
                echo $form->error($model,'AGAMA');
              ?>
              </div>
            </div>

            <!-- Marital Status Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'STATUSPERKAWINAN',array('class'=>'control-label'));?>
              <div class="controls">
                <?php
                  echo $form->dropDownList($model,'STATUSPERKAWINAN',CHtml::ListData(DmStatuskawin::model()->findAll(array('order'=>'id_statuskawin ASC')),'id_statuskawin','nama_statuskawin'),array('prompt'=>'','class'=>'span4','options'=>array($model->STATUSPERKAWINAN=>array('selected'=>true))));
                  echo $form->error($model,'STATUSPERKAWINAN');
                ?>
              </div>
            </div>

            <!-- Job Types Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'PEKERJAAN',array('class'=>'control-label'));?>
              <div class="controls">
                <?php
                 echo $form->dropDownList($model,'PEKERJAAN',CHtml::ListData(DmPekerjaan::model()->findAll(array('order'=>'id_pekerjaan ASC')),'nama_pekerjaan','nama_pekerjaan'),array('prompt'=>'','class'=>'span4','options'=>array($model->PEKERJAAN=>array('selected'=>true))));
                echo $form->error($model,'PEKERJAAN');
                ?>
              </div>
            </div>

            <!-- TELP. Number Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'TELP',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'TELP',array(
                  'class'=>'input-large',
                  'onkeypress' => 'return isNumber(event)'
                ));
                echo $form->error($model,'TELP'); ?>
              </div>
            </div>

            <!-- Parent's Name Field -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'NamaOrtu',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'NamaOrtu',array('class'=>'input-large'));
                echo $form->error($model,'NamaOrtu'); ?>
              </div>
            </div>

            <!-- Principal's Name (Husband of Wife) -->
            <div class="control-group">
              <?php echo $form->labelEx($model,'NamaSuamiIstri',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'NamaSuamiIstri',array('class'=>'input-large'));
                echo $form->error($model,'NamaSuamiIstri'); ?>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
  </div>
  <!-- /block -->
  </div>
</div>
  <!-- Submit Button -->
  <div class="form-actions" align="center">
    <button type="submit" class="btn btn-primary" name="cetak_sep">CETAK SEP</button>
    <button type="button" class="btn">Batal</button>
  </div>
  <?php $this->endWidget();?>
</div>

<?php
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
?>
<script>

$(document).ready(function(){
  $('#carinomor').click(function(){
    carinomor();
  });

  $('#carinik').click(function(){
    carinik();
  });

  $('#carirm').click(function(){
    carirm();
  });



});


jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keypress', 'input,select', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        // Get all focusable elements on the page
        var $canfocus = $(':focusable').not('a').not('input[readonly]');

        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
        $('html, body').animate({
              scrollTop: $("#"+this.id).offset().top - 100
          }, 10);
    }
});


  // Function to ensure user only able to input number on "No Identitas"
  // @params evt
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }

  // Special function for Patient Weight (Berat) Field. Only Allow number and dot char (as floating number)
  // @params evt
  function weightNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }


  function isiKabProv(ui)
  {
     $("#Pasien_Kecamatan").val(ui.item.id);
     $("#Pasien_Kota").val(ui.item.id_kota);
     $("#KecamatanNama").val(ui.item.value);
     $("#KotaNama").val(ui.item.nama_kota_prov);

  }

  function carirm()
  {
      $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("AjaxRequest/getPasienByRM");?>',
        data : 'norm='+$('#Pasien_NoMedrec').val(),
        beforeSend : function(){
          $('#loading3').show();

        },
        error: function(){
            alert('timeout');
            // will fire when timeout is reached
        },
        timeout : 5000,
        success : function(data){


          $('#loading3').hide();

          var output = JSON.parse(data);
          if(output != null){
            var pasien = output.pasien;
            var kota = output.kota;
            var kec = output.kec;

            if(pasien == null) return;

            $('#Pasien_NAMA').val(pasien.NAMA);

            // if(pasien.JENSKEL == 'L')
            //   $('#Pasien_JENSKEL_0').prop("checked",true);
            // else
            //   $('#Pasien_JENSKEL_1').prop("checked",true);

            $('#Pasien_JENSKEL').val(pasien.JENSKEL);

             $('#Pasien_ALAMAT').val(pasien.ALAMAT);
              $('#Pasien_TGLLAHIR').val(pasien.TGLLAHIR);
               $('#Pasien_Desa').val(pasien.Desa);

               $('#Pasien_Kecamatan').val('');
               $('#Pasien_Kota').val('');
              if(kec != null){
                $('#Pasien_Kecamatan').val(kec.id);
                $('#KecamatanNama').val(kec.nama);
              }

              if(kota != null){
                $('#Pasien_Kota').val(kota.id);
                $('#KotaNama').val(kota.nama);
              }



          }
        }
      });
  }

  function carinomor()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];


    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NoKartu').val()+'&jenis=kartu',
        beforeSend : function(){
          $('#loading1').show();
           $('#bpjs_error').hide();
        },
        error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {
                 alert('Failed from timeout');
                   $('#loading1').hide();
                //do something. Try again perhaps?
            }
        },
        timeout : 3000,
        async: false,
        success : function(data){


          $('#loading1').hide();

          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200){
             $('#bpjs_error').hide();
            var peserta = output.response.peserta;
            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#Pasien_NOIDENTITAS').val(peserta.nik);

            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);
            $('#Pasien_NAMA').val(peserta.nama);





            if(peserta.sex == 'L'){
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
              //$('#Pasien_JENSKEL_0').prop("checked",true);
            }
            else{
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);
              // $('#Pasien_JENSKEL_1').prop("checked",true);
            }

            $('#Pasien_JENSKEL').val(peserta.sex);
            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);

            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
             $('#Pasien_TGLLAHIR').val(peserta.tglLahir);

            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);
             $('#BpjsPasien_TGLCETAKKARTU').val(peserta.tglCetakKartu);
             $('#BpjsPasien_PPKTK1').val(peserta.provUmum.kdProvider+' - '+peserta.provUmum.nmProvider);
            $('#BpjsPasien_TMT').val(peserta.tglTMT);
            $('#BpjsPasien_TAT').val(peserta.tglTAT);
            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
          else{
            $('#bpjs_error').show();
                   $('#bpjs_error').html(output.metadata.message);
          }
        }
      });

  }

   function carinik()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];
    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NIK').val()+'&jenis=nik',
        beforeSend : function(){
          $('#loading2').show();
          $('#bpjs_error').hide();
        },
        success : function(data){


          $('#loading2').hide();

          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200){
             $('#bpjs_error').hide();
            var peserta = output.response.peserta;
            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#Pasien_NOIDENTITAS').val(peserta.nik);
            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);
             $('#Pasien_NAMA').val(peserta.nama);

            if(peserta.sex == 'L')
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
            else
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);

            $('#Pasien_JENSKEL').val(peserta.sex);

            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);

            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
             $('#Pasien_TGLLAHIR').val(peserta.tglLahir);

            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);
             $('#BpjsPasien_TGLCETAKKARTU').val(peserta.tglCetakKartu);
             $('#BpjsPasien_PPKTK1').val(peserta.provUmum.kdProvider+' - '+peserta.provUmum.nmProvider);
            $('#BpjsPasien_TMT').val(peserta.tglTMT);
            $('#BpjsPasien_TAT').val(peserta.tglTAT);
            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
          else{
             $('#bpjs_error').show();
                   $('#bpjs_error').html(output.metadata.message);
          }
        }
      });

  }


  jQuery(document).ready(function() {
     FormValidation.init();


  });


        $(function() {
          
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();


        });



 </script>
