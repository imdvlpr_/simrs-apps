<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';


  $baseUrl = Yii::app()->theme->baseUrl;
    $cs = Yii::app()->getClientScript();


?>

<div class="page-header">
  <h1>Formulir Data Pasien</h1>
</div>
<style>
  .errorMessage, .errorSummary{
    color:red;
  }

  #Pasien_TGLLAHIR{
    background : url(<?php echo Yii::app()->baseUrl;?>/images/calendar-icon.png) no-repeat scroll 7px 7px;
padding-left:30px;

}

</style>

<div class="row-fluid">
  <div class="span3" id="sidebar">
      <!-- <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.html"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="calendar.html"><i class="icon-chevron-right"></i> Calendar</a>
                        </li>
                        <li>
                            <a href="stats.html"><i class="icon-chevron-right"></i> Statistics (Charts)</a>
                        </li>
                        <li class="active">
                            <a href="form.html"><i class="icon-chevron-right"></i> Forms</a>
                        </li>
                        <li>
                            <a href="tables.html"><i class="icon-chevron-right"></i> Tables</a>
                        </li>
                        <li>
                            <a href="buttons.html"><i class="icon-chevron-right"></i> Buttons & Icons</a>
                        </li>
                        <li>
                            <a href="editors.html"><i class="icon-chevron-right"></i> WYSIWYG Editors</a>
                        </li>

                    </ul> -->
    </div>
    <div id="content" class="span9">
      <div class="row-fluid">
        <div class="block">
          <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon.png"/> Form Pendaftaran Rumah Sakit</div>
          </div>
         <div class="block-content collapse in">
            <div class="span12">
              <?php
                $form=$this->beginWidget('CActiveForm', array(
                      'id'=>'partner-form',
                      // Please note: When you enable ajax validation, make sure the corresponding
                      // controller action is handling ajax validation correctly.
                      // There is a call to performAjaxValidation() commented in generated controller code.
                      // See class documentation of CActiveForm for details on this.
                      'enableAjaxValidation'=>false,
                      'htmlOptions' => array(
                          'class'=>"form-horizontal",
                        ),
                    ));
                $namaKec = '';
                $namaKota = '';

                if(!$model->isNewRecord)
                {
                    $namaKec = $model->kEC->NamaKec;
                    $namaKota = $model->kOTA->NamaKabKota;
                }
              ?>
              <fieldset>
                <?php echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>'); ?>
                <!-- TODO: This Legend tag for what ? -->
                <legend></legend>

                 <div class="control-group">
                  <?php echo CHtml::label('Jenis Rawat','',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php $jenis_rawat_list = array(
                        1 => 'Rawat Inap',
                        2 => 'Rawat Jalan',

                      );
                    echo CHtml::dropDownList('jenis_rawat', !empty($_POST['jenis_rawat']) ? $_POST['jenis_rawat']: '',$jenis_rawat_list); ?>
                  </div>
                </div>
                <!-- Patient's Type (BPJS or UMUM) -->
                <div class="control-group">
                  <?php echo CHtml::label('Kepesertaan','',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php $kepesertaan_list = CHtml::ListData(GolPasien::model()->searchUmum(),'KodeGol','NamaGol');
                    echo CHtml::dropDownList('Kepesertaan', '',$kepesertaan_list); ?>
                  </div>
                </div>

                <!-- Medical Record Number Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'NoMedrec',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($model,'NoMedrec',array('class'=>'input-large','disabled'=>'disabled'));
                        echo $form->error($model,'NoMedrec');
                    ?>
                     <!--  <a id='carirm' href="javascript:void(0);"><i class="icon-search"></i> Cari RM</a><span id='loading3' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span> -->
                  </div>
                </div>

                <!-- Identification Number Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'NOIDENTITAS',array('class'=>'control-label'));?>
                  <div class="controls">
                    <!-- TODO: Input Number Only! -->
                    <?php echo $form->textField($model,'NOIDENTITAS',array(
                      'class'=>'input-large',
                      'tabindex'=>'1',
                      'placeholder' => '16 digit Nomor KTP',
                      'minLength' => '16',
                      'maxLength' => '16',
                      'onkeypress' => 'return isNumber(event)'));?>
                    <?php echo $form->error($model,'NOIDENTITAS'); ?>
                  </div>
                </div>

                <!-- Patient's Name Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'NAMA',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($model,'NAMA',array('class'=>'input-large','tabindex'=>'2')); ?>
                    <?php echo $form->error($model,'NAMA'); ?>
                  </div>
                </div>

                <!-- Patient's Sex Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'JENSKEL',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->dropDownList($model, 'JENSKEL',array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('tabindex'=>'4'));
                    // echo $form->radioButtonList($model, 'JENSKEL', array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('labelOptions'=>array('style'=>'display:inline','tabindex'=>'3')));
                    echo $form->error($model,'JENSKEL');
                    ?>
                  </div>
                </div>

                <!-- Patient's Date of Birth -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'TGLLAHIR',array('class'=>'control-label'));?>
                  <div class="controls">
                    <div id="datePickerComponent" class="input-append date margin-00" data-date="<?php echo date('d-m-Y')?>" data-date-format="dd-mm-yyyy">
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                      'model'=>$model,
                      'value'=>'TGLLAHIR',
                      'attribute'=>'TGLLAHIR',
                      // additional JavaScript options for the date picker plugin
                      'options'=>array(
                      'showAnim'=>'fold',
                      'showButtonPanel'=>true,
                      'autoSize'=>true,
                      'dateFormat'=>'dd/mm/yy',
                        'changeMonth'=>true,
                        'changeYear'=>true,
                        'yearRange'=>'1900:2099',
                       ),

                      'htmlOptions'=>array(
                        'class'=>'input-medium',
                      ),
                    ));
                    echo $form->error($model,'TGLLAHIR',array('class'=>'control-label')); ?>
                    </div>
                  </div>
                </div>

                <!-- Address Details -->
                <div class="control-group">
                  <?php echo CHtml::label('<strong>Alamat Lengkap:</strong>','',array('class'=>'control-label'));?>
                  <!-- TODO: Why Kosongan ada class control nya ? -->
                  <div class="controls"></div>
                </div>

                <!-- Address Street's Name Field -->
              
                <!-- Address District (Desa) Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'Desa',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php echo $form->textField($model,'Desa',array('class'=>'input-large'));
                  echo $form->error($model,'Desa'); ?>
                  </div>
                </div>

                  <div class="control-group">
              <?php echo $form->labelEx($model,'ALAMAT',array('class'=>'control-label'));?>
              <div class="controls">
                <?php echo $form->textField($model,'ALAMAT',array('class'=>'input-large'));
                echo $form->error($model,'ALAMAT'); ?>
              </div>
            </div>         

                <!-- Address Region (Kecamatan) Field -->
                <!-- This field is autofill field -->
                <!-- Patient's Details Label -->
                <div class="control-group">
                  <?php echo CHtml::label('<strong>Data Detil Pasien:</strong>','',array('class'=>'control-label'));?>
                  <!-- TODO: Control kosongan terpakai kah ? -->
                  <div class="controls"></div>
                </div>

                <!-- Patient's Place of Birth Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'TMPLAHIR',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($model,'TMPLAHIR',array('class'=>'input-large'));
                    echo $form->error($model,'TMPLAHIR'); ?>
                  </div>
                </div>

                <!-- Weight Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'BeratLahir',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($model,'BeratLahir',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return weightNumber(event)',
                      'placeholder' => 'Berat dalam Kg. Contoh: 64.5'
                    ));
                    echo $form->error($model,'BeratLahir');?>
                  </div>
                </div>

                <!-- Blood Type Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'GOLDARAH',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($model,'GOLDARAH',CHtml::ListData(DmGoldarah::model()->findAll(),'id_goldarah','nama_goldarah'),array('prompt'=>'','class'=>'span4','options'=>array($model->GOLDARAH=>array('selected'=>true))));
                      echo $form->error($model,'GOLDARAH');
                    ?>
                  </div>
                </div>

                <!-- Religion Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'AGAMA',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php
                     echo $form->dropDownList($model,'AGAMA',CHtml::ListData(DmAgama::model()->findAll(array('order'=>'id_agama ASC')),'id_agama','nama_agama'),array('prompt'=>'','class'=>'span4','options'=>array($model->AGAMA=>array('selected'=>true))));
                     echo $form->error($model,'AGAMA');
                  ?>
                  </div>
                </div>

                <!-- Marital Status -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'STATUSPERKAWINAN',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($model,'STATUSPERKAWINAN',CHtml::ListData(DmStatuskawin::model()->findAll(array('order'=>'id_statuskawin ASC')),'id_statuskawin','nama_statuskawin'),array('prompt'=>'','class'=>'span4','options'=>array($model->STATUSPERKAWINAN=>array('selected'=>true))));
                      echo $form->error($model,'STATUSPERKAWINAN');
                    ?>
                  </div>
                </div>

                <!-- Job Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'PEKERJAAN',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php
                      echo $form->dropDownList($model,'PEKERJAAN',CHtml::ListData(DmPekerjaan::model()->findAll(array('order'=>'id_pekerjaan ASC')),'nama_pekerjaan','nama_pekerjaan'),array('prompt'=>'','class'=>'span4','options'=>array($model->PEKERJAAN=>array('selected'=>true))));
                      echo $form->error($model,'PEKERJAAN');
                    ?>
                  </div>
                </div>

                <!-- TEL. Number Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'TELP',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($model,'TELP',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return isNumber(event)'
                      ));
                    echo $form->error($model,'TELP'); ?>
                  </div>
                </div>

                <!-- Parent's Name Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'NamaOrtu',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($model,'NamaOrtu',array('class'=>'input-large'));
                    echo $form->error($model,'NamaOrtu'); ?>
                  </div>
                </div>

                <!-- Principal's Name (Husband / Wife) Field -->
                <div class="control-group">
                  <?php echo $form->labelEx($model,'NamaSuamiIstri',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($model,'NamaSuamiIstri',array('class'=>'input-large'));
                    echo $form->error($model,'NamaSuamiIstri'); ?>
                  </div>
                </div>

                <!-- Submit Action Button -->
                <div class="form-actions" align="center">
                  <button type="submit" class="btn btn-primary" name="simpan_rawat_jalan">Simpan Data </button>
                  <!--   <button type="submit" class="btn btn-success" name="simpan_rawat_inap">Simpan Data dan Lanjut ke Rawat Inap</button> -->
                  <button type="button" class="btn">Batal</button>
                </div>

              </fieldset>
              <?php $this->endWidget();?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>


    <!--
}
=====================================================================================================================
==============================================form kedua========================================================
=====================================================================================================================
=====================================================================================================================

    -->



<?php
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


  $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
  $cs->registerScriptFile($baseUrl.'/assets/enter-keypress.js');

?>
<script>

<?php 
if(!empty($_POST['Pasien']['KecNama'])){


?>
$('#Pasien_KecNama').val('<?php echo $_POST['Pasien']['KecNama'];?>');

<?php 

}

if(!empty($_POST['Pasien']['Kota'])){
  $kotanama = Refkabkota::model()->findByPk($_POST['Pasien']['Kota']);

?>
$('#Pasien_KotaNama').val('<?php echo $kotanama->NamaKabKota;?>');

<?php 
}
?>




$(document).ready(function(){
  $('#carinomor').click(function(){
    carinomor();
  });

  $('#carinik').click(function(){
    carinik();
  });

  $('#carirm').click(function(){
    carirm();
  });


  $('#KecamatanNama').val('<?php echo $namaKec;?>');
  $('#KotaNama').val('<?php echo $namaKota;?>');

});


  // Function to ensure user only able to input number on "No Identitas"
  // @params evt
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }

  // Special function for Patient Weight (Berat) Field. Only Allow number and dot char (as floating number)
  // @params evt
  function weightNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }


  function isiKabProv(ui)
  {
     $("#Pasien_Kecamatan").val(ui.item.id);
     $("#Pasien_Kota").val(ui.item.id_kota);
     $("#KecamatanNama").val(ui.item.value);
     $("#Pasien_KotaNama").val(ui.item.nama_kota_prov);

  }

  function carirm()
  {
      $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("AjaxRequest/getPasienByRM");?>',
        data : 'norm='+$('#Pasien_NoMedrec').val(),
        beforeSend : function(){
          $('#loading3').show();

        },
        success : function(data){


          $('#loading3').hide();

          var output = JSON.parse(data);
          if(output != null){
            var pasien = output.pasien;
            var kota = output.kota;
            var kec = output.kec;

            if(pasien == null) return;
            $('#Pasien_NAMA').val(pasien.NAMA);
            if(pasien.JENSKEL == 'L')
              $('#Pasien_JENSKEL_0').prop("checked",true);
            else
              $('#Pasien_JENSKEL_1').prop("checked",true);

             $('#Pasien_ALAMAT').val(pasien.ALAMAT);
              $('#Pasien_TGLLAHIR').val(pasien.TGLLAHIR);
               $('#Pasien_Desa').val(pasien.Desa);

               $('#Pasien_Kecamatan').val('');
               $('#Pasien_Kota').val('');
              if(kec != null){
                $('#Pasien_Kecamatan').val(kec.id);
                $('#KecamatanNama').val(kec.nama);
              }

              if(kota != null){
                $('#Pasien_Kota').val(kota.id);
                $('#KotaNama').val(kota.nama);
              }



          }
        }
      });
  }

  function carinomor()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];


    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NoKartu').val()+'&jenis=kartu',
        beforeSend : function(){
          $('#loading1').show();

        },
        success : function(data){


          $('#loading1').hide();

          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metaData.code == 200){
            var peserta = output.response.peserta;
            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);

            if(peserta.sex == 'L')
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
            else
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);


            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);
            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);

            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
        }
      });

  }

   function carinik()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];
    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("WSBpjs/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NIK').val()+'&jenis=nik',
        beforeSend : function(){
          $('#loading2').show();

        },
        success : function(data){


          $('#loading2').hide();

          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metaData.code == 200){
            var peserta = output.response.list[0];
            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);

            if(peserta.sex == 'L')
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
            else
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);



            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);
            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);

            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
        }
      });

  }


  jQuery(document).ready(function() {

    $('#Pasien_NOIDENTITAS').focus();

     FormValidation.init();


  });

        $(function() {

            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();


        });



 </script>
