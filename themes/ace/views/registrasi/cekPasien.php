<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);

?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<script type="text/javascript">
	
	$(document).ready(function(){
		$('#search').change(function(){
    
	        $('#tabelpasien').yiiGridView.update('tabelpasien', {
	            url:'?r=registrasi/cekPasien&filter='+$('#search').val()+'&cekby='+$('#cekby').val()   
	        });
	      
	     });

	     $('#cekby').change(function(){
	    
	        $('#tabelpasien').yiiGridView.update('tabelpasien', {
	            url:'?r=registrasi/cekPasien&filter='+$('#search').val()+'&cekby='+$('#cekby').val()    
	        });
	    
	     });     
	});
</script>

<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span3" id="sidebar">

            
        </div> -->
        
        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Pasien','url'=>Yii::app()->createUrl('pasien/index')),
                            array('name' => 'Cek Pasien')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            <h1>.: Pengecekan Pasien :.</h1>
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Data Pasien</div>
                         <div class="pull-right">Cek Pasien Berdasarkan : 
                            <?php 
                            $params = array('Nama','No RM','NIK','No Kartu BPJS');
         echo CHtml::dropDownList('Pasien[CEKBY]', isset($_GET['cekby'])?$_GET['cekby']:'',$params,array('id'=>'cekby'));
                            
         echo CHtml::textField('Pasien[SEARCH]','',array('placeholder'=>'Cari','id'=>'search')); 
        ?> </div> 
                    </div>
                    <div class="block-content collapse in">
                        
  
<?php 
$datanotfound = '<div class="alert alert-info">
						<button class="close" data-dismiss="alert">&times;</button>
						<strong>Info !</strong> Apakah Anda ingin melanjutkan ke pendaftaran 
							<a href="'.Yii::app()->createUrl("registrasi/pasien",array("jenis"=>"bpjs")).  '">
		                  	<button class="btn btn-success btn-mini"> BPJS</button></a>&nbsp;atau&nbsp; 
		                  	<a href="'.Yii::app()->createUrl("registrasi/pasien",array("jenis"=>"nonbpjs")).'">
		                  	<button class="btn btn-primary btn-mini"> Non-BPJS</button></a>&nbsp;?
		                  	
					</div>';

$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabelpasien',
  'dataProvider'=>$model->searchPasienBy(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
    'NoMedrec',
    'NAMA',
    'FULLADDRESS',
    'NamaSuamiIstri',
    array(
    	'header'=> 'Tindakan',
      'class'=>'CButtonColumn',
      'template'=>'{rj} {ri} ',
      'buttons' => array(
      	'rj' 	=> array(
        	'url'=>'Yii::app()->createUrl("registrasi/rawatJalan/", array("id"=>$data->NoMedrec))',      
  				'label'=>'<span class="label label-success">Rawat Jalan</span>',
				
  			),
      	'ri'		=> array(
        	'url'=>'Yii::app()->createUrl("trRawatInap/create/", array("rm"=>$data->NoMedrec))',      
  				'label'=>'<span class="label label-info">&nbsp;Rawat Inap&nbsp;</span>',

        ),
       
      ),
      
    ),
  ),
 
  'pager'=>array(
                        'class'=>'SimplePager',                  
                        'header'=>'',                      
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',        
                        'lastPageLabel'=>'Terakhir',  
            'firstPageCssClass'=>'btn',
                        'previousPageCssClass'=>'btn',
                        'nextPageCssClass'=>'btn',        
                        'lastPageCssClass'=>'btn',
            'hiddenPageCssClass'=>'disabled',
            'internalPageCssClass'=>'btn',
            'selectedPageCssClass'=>'btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table table-striped table-bordered',
  'summaryCssClass'=>'dataTables_info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>$datanotfound,
  'pagerCssClass'=>'dataTables_paginate paging_bootstrap pagination',
)); ?>

                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>