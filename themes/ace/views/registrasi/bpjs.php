<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3" id="sidebar">

            <div class='block'>
                <div class="navbar navbar-inner block-header"><div class="muted pull-left">Data Pasien</div></div>
                <div class="block-content collapse in">
                    <div class="span12">
                            
                        <div class="control-group">
                        <?php echo CHtml::label('NAMA : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <h3><?php echo $pasien->NAMA;?></h3>
                          </div>
                        </div>
                          <div class="control-group">
                        <?php echo CHtml::label('No RM : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <h2><?php echo $pasien->NoMedrec;?></h2>
                          </div>
                        </div>
                        <div class="control-group">
                        <?php echo CHtml::label('No Pendaftaran : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <h2><?php echo $model->NoDaftar;?></h2>
                          </div>
                        </div>   
                        
                        <div class="control-group">
                         <?php echo CHtml::label('Jenis Kelamin : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <strong><?php echo $pasien->JENSKEL;?></strong>
                          </div>
                        </div>
                         <div class="control-group">
                         <?php echo CHtml::label('Umur : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <strong><?php echo $umur;?></strong>
                          </div>
                        </div>
                        <div class="control-group">
                         <?php echo CHtml::label('Kunjungan Ke : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <strong><?php echo $kunjunganKe;?></strong>
                          </div>
                        </div>
                           <div class="control-group">
                          <?php echo CHtml::label('ALAMAT : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <strong><?php 
                            echo (!empty($pasien->ALAMAT) ? $pasien->ALAMAT : '-');

                            if(!empty($pasien->kEC) && !empty($pasien->kOTA))
                                echo $pasien->kEC->NamaKec.' - '.$pasien->kOTA->NamaKabKota;
                            ?></strong>
                          </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
        <!--/span-->
        <div class="span9" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('registrasi/rawatJalan')),
                            array('name' => 'Pendaftaran')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Formulir Data Pasien</div>
                      <!--   <div class="pull-right"><span class="badge badge-info">1,462</span>

                        </div> -->
                    </div>
                    <div class="block-content collapse in">
                        
                     <?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<h4>Pengisian Formulir</h4>
<style>
  .errorMessage, .errorSummary{
    color:red;
  }

</style>
<script type="text/javascript">
  $(document).ready(function(){
    $('#TrPendaftaranRjalan_GolPasien').change(function(){
      var txt = $('#TrPendaftaranRjalan_GolPasien option:selected').text();

      if(txt.includes('BPJS')) {
        $('#div-nokartu').show();
        $('#div-batas-sep').show();
      }
      else {
        $('#div-nokartu').hide();
        $('#div-batas-sep').hide();
      }
    });
  });
</script>
<?php 
  $lama = 'baru';
  $opsi = '';
  if($kunjunganKe > 1 && !$pasien->isNewRecord)
  {
    $lama = 'lama';
    $val = true;
    $opsi = array('readonly' => $val,'disabled'=> $val);
  }

  else if($pasien->isNewRecord)
  {
?>
<div class="alert alert-error">
                    <button class="close" data-dismiss="alert">&times;</button>
                    <strong>Error!</strong> Tidak Data Rekam Medis.
                  </div><?php 

                }

                  ?>
<div class="row-fluid">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>

  <?php 
echo $form->errorSummary($model);

?>                            <!-- block -->
          <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Pendaftaran/Kunjungan</div>
        </div>
        <div class="block-content collapse in">
            <div class="span6">
                
                
            <fieldset>
          
        <div class="control-group" >
        <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'control-label'));?>
          <div class="controls">
            <?php 
              echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-large'));

              
            ?>
            <span id="badge"></span>
            <a id='carinomor' href="javascript:void(0);"><i class="icon-search"></i> Cari Nomor</a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
            <?php echo $form->error($bpjsPasien,'NoKartu'); ?>
          </div>
        </div>
          
     
            <div class="control-group">
              <?php echo $form->labelEx($model,'WAKTU_DAFTAR',array('class'=>'control-label'));?>
              <div class="controls">
               <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'WAKTU_DAFTAR',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'onClose' => 'js:function(dtText,dtI) {
                            getBatasSEP(dtText);
                        }',
                    ),
        ));

              echo $form->error($model,'WAKTU_DAFTAR');
              ?>
                
                </div>
              </div>
            <div class="control-group">
            <?php echo $form->labelEx($model,'BATAS_CETAK_SEP',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->textField($model,'BATAS_CETAK_SEP',array('class'=>'input-large'));
                  echo $form->error($model,'BATAS_CETAK_SEP'); 
                  ?>
              </div>
            </div>
          
        
                <div style="margin-top: 10px;">
                     <button type="submit" class="btn btn-primary" name="simpan_pendaftaran">Simpan Data Baru</button>
                      <div class="btn-group">
                        <button data-toggle="dropdown" class="btn dropdown-toggle">Cetak <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                        <li><a href="#"><i class="icon-print"></i> Treser</a></li>
                        <li><a href="#"><i class="icon-print"></i> E-Tiket</a></li>
                        <li><a href="#"><i class="icon-print"></i> Kartu</a></li>
                        
                        </ul>
                      </div><!-- /btn-group -->
                     
                      </div><br><br><br><br><br>
        </fieldset>
    
        </div>
         <div class="span6">
                
                
            <fieldset>
          
        <div class="control-group" >
        <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'control-label'));?>
          <div class="controls">
            <?php 
              echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-large'));

              
            ?>
            <span id="badge"></span>
            <a id='carinomor' href="javascript:void(0);"><i class="icon-search"></i> Cari Nomor</a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
            <?php echo $form->error($bpjsPasien,'NoKartu'); ?>
          </div>
        </div>
          
     
            <div class="control-group">
              <?php echo $form->labelEx($model,'WAKTU_DAFTAR',array('class'=>'control-label'));?>
              <div class="controls">
               <?php 

               $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'WAKTU_DAFTAR',
                    'options'=>array(
                       'showAnim'=>'slide',
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat'=>'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'onClose' => 'js:function(dtText,dtI) {
                            getBatasSEP(dtText);
                        }',
                    ),
        ));

              echo $form->error($model,'WAKTU_DAFTAR');
              ?>
                
                </div>
              </div>
            <div class="control-group">
            <?php echo $form->labelEx($model,'BATAS_CETAK_SEP',array('class'=>'control-label'));?>
              <div class="controls">
                <?php 
                  echo $form->textField($model,'BATAS_CETAK_SEP',array('class'=>'input-large'));
                  echo $form->error($model,'BATAS_CETAK_SEP'); 
                  ?>
              </div>
            </div>
          
        
                <div style="margin-top: 10px;">
                     <button type="submit" class="btn btn-primary" name="simpan_pendaftaran">Simpan Data Baru</button>
                      <div class="btn-group">
                        <button data-toggle="dropdown" class="btn dropdown-toggle">Cetak <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                        <li><a href="#"><i class="icon-print"></i> Treser</a></li>
                        <li><a href="#"><i class="icon-print"></i> E-Tiket</a></li>
                        <li><a href="#"><i class="icon-print"></i> Kartu</a></li>
                        
                        </ul>
                      </div><!-- /btn-group -->
                     
                      </div><br><br><br><br><br>
        </fieldset>
    
        </div>
    </div>
</div>
<div id="myModal" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">&times;</button>
        <h3>Input Event pada Kalender</h3>
    </div>
    <div class="modal-body">
       
    <div class="control-group">
      <?php echo CHtml::label('NoKartu','',array('class'=>'control-label'));?>
        <div class="controls">
          <?php 
            echo CHtml::textField('NoKartu','',array('class'=>'input-large'));

            
          ?>

        </div>
      </div>
      <div class="control-group">
        <?php echo CHtml::label('NIK','',array('class'=>'control-label'));?>
          <div class="controls">
            <?php echo CHtml::textField('NIK','',array('class'=>'input-large')); 
                
            ?>
            
          </div>
        </div>
         <div class="control-group">
          <?php echo CHtml::label('NAMAPESERTA','',array('class'=>'control-label'));?>
            <div class="controls">
              <?php echo CHtml::textField('NAMAPESERTA','',array('class'=>'input-large'));

               ?>
            </div>
          </div>
         <div class="control-group">
          <?php echo CHtml::label('KELAMIN','',array('class'=>'control-label'));?>
            <div class="controls">
            <?php echo CHtml::textField('KELAMIN','' ,array('class'=>'input-large')); 

            ?>
            </div>
          </div>
           <div class="control-group">
          <?php echo CHtml::label('PISAT','',array('class'=>'control-label'));?>
            <div class="controls">
              
              <?php 
                echo CHtml::textField('pisat_nama','',array('class'=>'input-large'));
                
               ?>
            </div>
          </div>
         
          <div class="control-group">
            <?php echo CHtml::label('TGLLAHIR','',array('class'=>'control-label'));?>
            <div class="controls">
              <?php 
               echo CHtml::textField('TGLLAHIR','',array('class'=>'input-large'));
              ?>            
              
              </div>
            </div>
         
          <!--  <div class="control-group">
          <?php echo CHtml::label('PPKTK1','',array('class'=>'control-label'));?>
            <div class="controls">
              <?php 
                echo CHtml::textField('PPKTK1','',array('class'=>'input-large'));
               ?>
            </div>
          </div> -->
          <div class="control-group">
          <?php echo CHtml::label('PESERTA','',array('class'=>'control-label'));?>
            <div class="controls">
              <?php 

                echo CHtml::textField('PESERTA_NAMA','',array('class'=>'input-large'));
               ?>
            </div>
          </div>
           <div class="control-group">
          <?php echo CHtml::label('KELASRAWAT','',array('class'=>'control-label'));?>
            <div class="controls">
              <?php 
                echo CHtml::textField('KELASRAWAT_NAMA','',array('class'=>'input-large'));
               ?>
            </div>
          </div> 
    
     
   
  </fieldset>
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" class="btn" href="#">Tutup</a>
    </div>
</div>
<!-- /block -->

    <!--
=====================================================================================================================
==============================================form kedua========================================================
=====================================================================================================================
=====================================================================================================================

    -->
      
    </div>
    <!-- /block -->
              </div>
              
   <?php $this->endWidget();?>
</div>

<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');


 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
?>
<script>
$(document).ready(function(){
   $('#carinomor').click(function(){
      carinomor();
    });

});
 function carinomor()
  {
    var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];


    $.ajax({
        type : 'POST',
        url  : '<?php echo Yii::app()->createUrl("AjaxRequest/getPesertaByNomor");?>',
        data : 'nokartu='+$('#BpjsPasien_NoKartu').val()+'&jenis=kartu',
        beforeSend : function(){
          $('#loading1').show();

        },
        error: function(jqXHR, textStatus){
            if(textStatus === 'timeout')
            {     
                 alert('Failed from timeout');     
                   $('#loading1').hide();    
                //do something. Try again perhaps?
            }
        },
        timeout : 3000,
        async: false,
        success : function(data){


          $('#loading1').hide();
          
          if(data == '') return;

          var output = JSON.parse(data);
          if(output.metadata.code == 200){
            $('#badge').html('<span class="badge badge-success">Ok</span>');
            var peserta = output.response.peserta;
             $('#myModal').modal();  
             $('#NoKartu').val(peserta.noKartu);
            $('#NIK').val(peserta.nik);
            $('#NAMAPESERTA').val(peserta.nama);
           $('#KELAMIN').val(peserta.sex);
            $('#PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);
            $('#TGLLAHIR').val(peserta.tglLahir);
            $('#PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);

            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);

            $('#BpjsPasien_NIK').val(peserta.nik);
            $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);
            $('#Pasien_NAMA').val(peserta.nama);

            if(peserta.sex == 'L'){
              $('#BpjsPasien_KELAMIN_0').prop("checked",true);
              $('#Pasien_JENSKEL_0').prop("checked",true);
            }
            else{
              $('#BpjsPasien_KELAMIN_1').prop("checked",true);
               $('#Pasien_JENSKEL_1').prop("checked",true);
            }
              
            
            $('#BpjsPasien_NoKartu').val(peserta.noKartu);
            $('#BpjsPasien_KELAMIN').val(peserta.sex);
            $('#BpjsPasien_PISAT').val(peserta.pisa);
            $('#pisat_nama').val(pisat[peserta.pisa]);
            
            $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
             $('#Pasien_TGLLAHIR').val(peserta.tglLahir);

            $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
            $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);
             $('#BpjsPasien_TGLCETAKKARTU').val(peserta.tglCetakKartu);
             $('#BpjsPasien_PPKTK1').val(peserta.provUmum.kdProvider+' - '+peserta.provUmum.nmProvider);
            $('#BpjsPasien_TMT').val(peserta.tglTMT);
            $('#BpjsPasien_TAT').val(peserta.tglTAT);
            $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
            $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
          }
          else
          {
              $('#BpjsPasien_NoKartu').val('');
              $('#badge').html('<span class="badge badge-important">'+output.metadata.message+'</span>');
          }
        }
      });
   
  }

function getBatasSEP(tgl)
{
    $.ajax({
        url: '<?php echo Yii::app()->createUrl("ajaxRequest/GetBatasSEP");?>',
        type: 'POST', // Send post data
        data: 'tgl='+tgl,
        async: false,

        success: function(data){
            var hasil = JSON.parse(data);
          
            $('#TrPendaftaranRjalan_BATAS_CETAK_SEP').val(hasil.tanggal);
        }
    });
}

  jQuery(document).ready(function() {   
     FormValidation.init();

    
  });
  

        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();

            
        });



 </script>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>