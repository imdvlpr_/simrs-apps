<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3" id="sidebar">

            <div class='block'>
                <div class="navbar navbar-inner block-header"><div class="muted pull-left">Data Pasien</div></div>
                <div class="block-content collapse in">
                    <div class="span12">
                            
                        <div class="control-group">
                        <?php echo CHtml::label('NAMA : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <h3><?php echo $pasien->NAMA;?></h3>
                          </div>
                        </div>
                          <div class="control-group">
                        <?php echo CHtml::label('No RM : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <h2><?php echo $pasien->NoMedrec;?></h2>
                          </div>
                        </div>
                        <div class="control-group">
                        <?php echo CHtml::label('No Pendaftaran : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <h2><?php echo $model->NoDaftar;?></h2>
                          </div>
                        </div>   
                        
                        <div class="control-group">
                         <?php echo CHtml::label('Jenis Kelamin : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <strong><?php echo $pasien->JENSKEL;?></strong>
                          </div>
                        </div>
                         <div class="control-group">
                         <?php echo CHtml::label('Umur : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <strong><?php echo $umur;?></strong>
                          </div>
                        </div>
                        <div class="control-group">
                         <?php echo CHtml::label('Kunjungan Ke : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <strong><?php echo $kunjunganKe;?></strong>
                          </div>
                        </div>
                           <div class="control-group">
                          <?php echo CHtml::label('ALAMAT : ','',array('class'=>'control-label'));?>
                          <div class="controls">
                            <strong><?php 
                           echo $pasien->ALAMAT;
                            // echo (!empty($pasien->JALAN) ? $pasien->JALAN : ' - ');
                            // echo ' RT '.$pasien->RT.' RW '.$pasien->RW;

                            // if(!empty($pasien->kEC) && !empty($pasien->kOTA))
                            //     echo ' '.$pasien->kEC->NamaKec.' - '.$pasien->kOTA->NamaKabKota;
                            ?></strong>
                          </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
        <!--/span-->
        <div class="span9" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Rawat Jalan','url'=>Yii::app()->createUrl('registrasi/rawatJalan')),
                            array('name' => 'Pendaftaran')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Formulir Data Pasien</div>
                      <!--   <div class="pull-right"><span class="badge badge-info">1,462</span>

                        </div> -->
                    </div>
                    <div class="block-content collapse in">
                        
                        <?php 
                            $this->renderPartial($view,array(
                    'model'=>$model,
                    'pasien' => $pasien,
                    'umur' => $umur,
                    'kunjunganKe' => $kunjunganKe,
                    'bpjsPasien' => $bpjsPasien,
                    'poli' => $poli
                ));
                        ?>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>