<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();
?>

<div class="page-header">
  <h1>Pendaftaran Pasien RAWAT JALAN</h1>
</div>


<div class="row-fluid">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); ?>

<div class="span6">
                            <!-- block -->
          <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Form Pendaftaran BPJS</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                
                
            <fieldset>
            <div class="control-group">
        <label class="control-label">Jenis Pasien<span class="required">*</span></label>
        <div class="controls">
          <?php 
          echo CHtml::dropDownList('jenis_pasien', '', 
    array('baru' => 'Baru', 'lama' => 'Lama'));?>
        </div>
      </div>
      
    <div class="control-group">
        <label class="control-label">Kepesertaan<span class="required">*</span></label>
        <div class="controls">
          <?php 
          echo CHtml::dropDownList('kepesertaan', $jenis, 
    array('bpjs' => 'BPJS', 'nonbpjs' => 'Non-BPJS'));?>
        </div>
      </div>
              <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'NoKartu',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      echo $form->textField($bpjsPasien,'NoKartu',array('class'=>'input-large'));

                      echo $form->error($bpjsPasien,'NoKartu'); 
                    ?>
                    <a id='carinomor' onclick="return carinomor()" href="javascript:void(0);"><i class="icon-search"></i> Cari Nomor</a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
                  </div>
                </div>
               <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'NIK',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($bpjsPasien,'NIK',array('class'=>'input-large')); 
                        echo $form->error($bpjsPasien,'NIK');
                    ?>
                      <a id='carinik' onclick="return carinomor()" href="javascript:void(0);"><i class="icon-search"></i> Cari NIK</a><span id='loading2' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
                  </div>
                </div>
               <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'NAMAPESERTA',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($bpjsPasien,'NAMAPESERTA',array('class'=>'input-large'));

                      echo $form->error($bpjsPasien,'NAMAPESERTA');
                     ?>
                  </div>
                </div>
               <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'KELAMIN',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php echo $form->radioButtonList($bpjsPasien, 'KELAMIN', array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('labelOptions'=>array('style'=>'display:inline'))); 
                    echo $form->error($bpjsPasien,'KELAMIN');
                  ?>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PISAT',array('class'=>'control-label'));?>
                  <div class="controls">
                    
                    <?php 
                      echo CHtml::textField('pisat_nama','',array('class'=>'input-large'));
                      
                      echo $form->hiddenField($bpjsPasien,'PISAT',array('class'=>'input-large'));
                      echo $form->error($bpjsPasien,'PISAT');
                     ?>
                  </div>
                </div>
               
                <div class="control-group">
                  <?php echo $form->labelEx($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));?>
                  <div class="controls">
                    <div id="datePickerComponent" class="input-append date margin-00" data-date="<?php echo date('d-m-Y')?>" data-date-format="dd-mm-yyyy">
                  <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                      'model'=>$bpjsPasien,
                      'value'=>'TGLLAHIR',
                      'attribute'=>'TGLLAHIR',
                      // additional JavaScript options for the date picker plugin
                      'options'=>array(
                      'showAnim'=>'fold',
                      'showButtonPanel'=>true,
                      'autoSize'=>true,
                      'dateFormat'=>'yy-mm-dd',
                        'changeMonth'=>true,
                        'changeYear'=>true,
                        'yearRange'=>'1900:2099', 
                       ),

                      'htmlOptions'=>array(
                        'class'=>'input-medium',                
                      ),
                    ));

                  echo $form->error($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));
                  ?>
                    
                    </div>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PPKTK1',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      echo $form->textField($bpjsPasien,'PPKTK1',array('class'=>'input-large'));
                      echo $form->error($bpjsPasien,'PPKTK1');
                     ?>
                  </div>
                </div>
                <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PESERTA',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 

                      echo CHtml::textField('PESERTA_NAMA','',array('class'=>'input-large'));
                      echo $form->hiddenField($bpjsPasien,'PESERTA');
                      echo $form->error($bpjsPasien,'PESERTA');
                     ?>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'KELASRAWAT',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      echo CHtml::textField('KELASRAWAT_NAMA','',array('class'=>'input-large'));
                      
                      echo $form->hiddenField($bpjsPasien,'KELASRAWAT');
                      echo $form->error($bpjsPasien,'KELASRAWAT');
                     ?>
                  </div>
                </div>
                
            </fieldset>
        
            </div>
        </div>
    </div>
    <!-- /block -->
    </div>

    <!--
=====================================================================================================================
==============================================form kedua========================================================
=====================================================================================================================
=====================================================================================================================

    -->
        <div class="span6">
            <!-- block -->
            <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Form Pendaftaran Rumah Sakit</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
 
                
            <fieldset>
             
                
                <div class="control-group">
                <?php echo $form->labelEx($model,'nokartu',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      echo $form->textField($model,'NoKartu',array('class'=>'input-large'));

                      echo $form->error($model,'NoKartu'); 
                    ?>
                    <a id='carinomor' onclick="return carinomor()" href="javascript:void(0);"><i class="icon-search"></i> Cari Nomor</a><span id='loading1' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
                  </div>
                </div>
               <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'NIK',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($bpjsPasien,'NIK',array('class'=>'input-large')); 
                        echo $form->error($bpjsPasien,'NIK');
                    ?>
                      <a id='carinik' onclick="return carinomor()" href="javascript:void(0);"><i class="icon-search"></i> Cari NIK</a><span id='loading2' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
                  </div>
                </div>
               <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'NAMAPESERTA',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php echo $form->textField($bpjsPasien,'NAMAPESERTA',array('class'=>'input-large'));

                      echo $form->error($bpjsPasien,'NAMAPESERTA');
                     ?>
                  </div>
                </div>
               <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'KELAMIN',array('class'=>'control-label'));?>
                  <div class="controls">
                  <?php echo $form->radioButtonList($bpjsPasien, 'KELAMIN', array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('labelOptions'=>array('style'=>'display:inline'))); 
                    echo $form->error($bpjsPasien,'KELAMIN');
                  ?>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PISAT',array('class'=>'control-label'));?>
                  <div class="controls">
                    
                    <?php 
                      echo CHtml::textField('pisat_nama','',array('class'=>'input-large'));
                      
                      echo $form->hiddenField($bpjsPasien,'PISAT',array('class'=>'input-large'));
                      echo $form->error($bpjsPasien,'PISAT');
                     ?>
                  </div>
                </div>
               
                <div class="control-group">
                  <?php echo $form->labelEx($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));?>
                  <div class="controls">
                    <div id="datePickerComponent" class="input-append date margin-00" data-date="<?php echo date('d-m-Y')?>" data-date-format="dd-mm-yyyy">
                  <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                      'model'=>$bpjsPasien,
                      'value'=>'TGLLAHIR',
                      'attribute'=>'TGLLAHIR',
                      // additional JavaScript options for the date picker plugin
                      'options'=>array(
                      'showAnim'=>'fold',
                      'showButtonPanel'=>true,
                      'autoSize'=>true,
                      'dateFormat'=>'yy-mm-dd',
                        'changeMonth'=>true,
                        'changeYear'=>true,
                        'yearRange'=>'1900:2099', 
                       ),

                      'htmlOptions'=>array(
                        'class'=>'input-medium',                
                      ),
                    ));

                  echo $form->error($bpjsPasien,'TGLLAHIR',array('class'=>'control-label'));
                  ?>
                    
                    </div>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PPKTK1',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      echo $form->textField($bpjsPasien,'PPKTK1',array('class'=>'input-large'));
                      echo $form->error($bpjsPasien,'PPKTK1');
                     ?>
                  </div>
                </div>
                <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'PESERTA',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 

                      echo CHtml::textField('PESERTA_NAMA','',array('class'=>'input-large'));
                      echo $form->hiddenField($bpjsPasien,'PESERTA');
                      echo $form->error($bpjsPasien,'PESERTA');
                     ?>
                  </div>
                </div>
                 <div class="control-group">
                <?php echo $form->labelEx($bpjsPasien,'KELASRAWAT',array('class'=>'control-label'));?>
                  <div class="controls">
                    <?php 
                      echo CHtml::textField('KELASRAWAT_NAMA','',array('class'=>'input-large'));
                      
                      echo $form->hiddenField($bpjsPasien,'KELASRAWAT');
                      echo $form->error($bpjsPasien,'KELASRAWAT');
                     ?>
                  </div>
                </div>
                
            </fieldset>
       
            </div>
        </div>
    </div>
    <!-- /block -->
              </div>
              <div class="form-actions" align="center">
                  <button type="submit" class="btn btn-primary">Simpan Data Baru</button>
                  <button type="button" class="btn">Cancel</button>
                </div>
   <?php $this->endWidget();?>
</div>

<?php 
  $cs->registerCssFile($baseUrl.'/vendors/datepicker.css');
  $cs->registerCssFile($baseUrl.'/vendors/uniform.default.css');
  $cs->registerCssFile($baseUrl.'/vendors/chosen.min.css');
  $cs->registerCssFile($baseUrl.'/vendors/wysiwyg/bootstrap-wysihtml5.css');
 $cs->registerScriptFile($baseUrl.'/vendors/jquery-1.9.1.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery.uniform.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/chosen.jquery.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/bootstrap-datepicker.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wysiwyg/wysihtml5-0.3.0.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wysiwyg/bootstrap-wysihtml5.js');
  $cs->registerScriptFile($baseUrl.'/vendors/wizard/jquery.bootstrap.wizard.min.js');
  $cs->registerScriptFile($baseUrl.'/vendors/jquery-validation/dist/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl.'/assets/form-validation.js');
  $cs->registerScriptFile($baseUrl.'/assets/scripts.js');
?>
<script>
function carinomor()
{
  var pisat = ["None","Peserta","Suami","Istri","Anak","Tambahan"];
  $.ajax({
      type : 'POST',
      url  : '<?php echo Yii::app()->createUrl("AjaxRequest/getPesertaByKartu");?>',
      data : 'nokartu='+$('#BpjsPasien_NoKartu').val(),
      beforeSend : function(){
        $('#loading1').show();

      },
      success : function(data){


        $('#loading1').hide();
        
        if(data == '') return;

        var output = JSON.parse(data);
        if(output.metaData.code == 200){
          var peserta = output.response.peserta;
          $('#BpjsPasien_NIK').val(peserta.nik);
          $('#BpjsPasien_NAMAPESERTA').val(peserta.nama);

          if(peserta.sex == 'L')
            $('#BpjsPasien_KELAMIN_0').prop("checked",true);
          else
            $('#BpjsPasien_KELAMIN_1').prop("checked",true);
            
          

          $('#BpjsPasien_KELAMIN').val(peserta.sex);
          $('#BpjsPasien_PISAT').val(peserta.pisa);
          $('#pisat_nama').val(pisat[peserta.pisa]);
          $('#BpjsPasien_TGLLAHIR').val(peserta.tglLahir);
          $('#BpjsPasien_PESERTA').val(peserta.jenisPeserta.kdJenisPeserta);
          $('#BpjsPasien_KELASRAWAT').val(peserta.kelasTanggungan.kdKelas);

          $('#PESERTA_NAMA').val(peserta.jenisPeserta.nmJenisPeserta);
          $('#KELASRAWAT_NAMA').val(peserta.kelasTanggungan.nmKelas);
        }
      }
    });
 
}
  jQuery(document).ready(function() {   
     FormValidation.init();

    
  });
  

        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            
        });



 </script>