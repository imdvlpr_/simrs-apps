<?php
/* @var $this DmPoliController */
/* @var $model DmPoli */

$this->breadcrumbs=array(
	'Dm Polis'=>array('index'),
	'Manage',
);

?>

<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<script type="text/javascript">

	$(document).ready(function(){
		$('#search').change(function(){

	        $('#tabelkunjungan').yiiGridView.update('tabelkunjungan', {
	            url:'?r=registrasi/kunjungan&filter='+$('#search').val()+'&size='+$('#size').val()+'&poli1='+$('#poli1').val()
	        });

	     });

	     $('#size').change(function(){

	        $('#tabelkunjungan').yiiGridView.update('tabelkunjungan', {
	            url:'?r=registrasi/kunjungan&filter='+$('#search').val()+'&size='+$('#size').val()+'&poli1='+$('#poli1').val()
	        });

	     });

        $('#poli1').change(function(){

          $('#tabelkunjungan').yiiGridView.update('tabelkunjungan', {
              url:'?r=registrasi/kunjungan&filter='+$('#search').val()+'&size='+$('#size').val()+'&poli1='+$('#poli1').val()
          });

       });
	});
</script>
<h1>Data Kunjungan</h1>

<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span3" id="sidebar">


        </div> -->

        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">

                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Poli','url'=>Yii::app()->createUrl('DmPoli/index')),
                            array('name' => 'List')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>
                    </div>
                </div>
            </div>



            <div class="row-fluid">
            <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="pull-left">
                          Data poli
          <?php
        $level_list = CHtml::ListData($poli,'id_poli','nama_poli');
           echo CHtml::dropDownList('TrPendaftaranRjalan[poli1]',isset($_GET['poli1'])?$_GET['poli1']:'',$level_list,array('id'=>'poli1','empty'=>'-- Pilih Poli --')); ?> 
                        </div>


                         <div class="pull-right">Data per halaman
                            <?php echo CHtml::dropDownList('DmPoli[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size','size'=>1)); ?> <?php
        echo CHtml::textField('DmPoli[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    </div>
                    <div class="block-content collapse in">


<?php $this->widget('application.components.ComplexGridView', array(
  'id'=>'tabelkunjungan',
  'dataProvider'=>$model->searchKunjungan(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
   
		array(
        'header' => 'Nama Pasien',
        'value' => '$data->pASIEN->NAMA'
    ),
    array(
        'header' => 'Poli',
        'value' => '!empty($data->poli1) ? $data->poli1->nama_poli : "-"'
    ),
		'created',
		
    array(
      'class'=>'CButtonColumn',
      'template'=>'{cetakETiket} {cetakGelang}',
      'buttons' => array(
          'cetakETiket'   => array(
            'visible' => 'Yii::app()->user->checkAccess(array(WebUser::R_SA, WebUser::R_OP_DAFTAR))',
            'url'=>'Yii::app()->createUrl("pasien/cetakETiket/", array("id"=>$data->NoMedrec))',
            'label'=>'<span class="label label-info"><i class="icon-print"></i>&nbsp;Cetak E-Tiket&nbsp;</span>',
            'options'=>array(
                'title'=>'Cetak E-Tiket',
                'target' => '_blank',
              ),

          ),
          'cetakGelang'   => array(
            'visible' => 'Yii::app()->user->checkAccess(array(WebUser::R_SA, WebUser::R_OP_DAFTAR))',
            'url'=>'Yii::app()->createUrl("pasien/cetakGelang/", array("id"=>$data->NoMedrec))',
            'label'=>'<span class="label label-info"><i class="icon-print"></i>&nbsp;Cetak Gelang&nbsp;</span>',
            'options'=>array(
                'title'=>'Cetak Gelang',
                'target' => '_blank',
              ),

          ),

      ),
    ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
  'pager'=>array(
                        'class'=>'SimplePager',
                        'header'=>'',
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',
                        'lastPageLabel'=>'Terakhir',
            'firstPageCssClass'=>'btn',
                        'previousPageCssClass'=>'btn',
                        'nextPageCssClass'=>'btn',
                        'lastPageCssClass'=>'btn',
            'hiddenPageCssClass'=>'disabled',
            'internalPageCssClass'=>'btn',
            'selectedPageCssClass'=>'btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
  'summaryCssClass'=>'table-message-info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
  'pagerCssClass'=>'btn-group paging_full_numbers',
)); ?>

                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>

</div>
