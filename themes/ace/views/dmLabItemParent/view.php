<?php
/* @var $this DmLabItemParentController */
/* @var $model DmLabItemParent */

$this->breadcrumbs=array(
	array('name'=>'Dm Lab Item Parent','url'=>array('admin')),
	array('name'=>'Dm Lab Item Parent'),
);

$this->menu=array(
	array('label'=>'List DmLabItemParent', 'url'=>array('index')),
	array('label'=>'Create DmLabItemParent', 'url'=>array('create')),
	array('label'=>'Update DmLabItemParent', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DmLabItemParent', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DmLabItemParent', 'url'=>array('admin')),
);
?>

<h1>View DmLabItemParent #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'parent_id',
		'item_id',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
