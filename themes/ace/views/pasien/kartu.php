<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);

?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<script type="text/javascript">

	$(document).ready(function(){
		$('#search').change(function(){

	        $('#tabelpasien').yiiGridView.update('tabelpasien', {
	            url:'?r=pasien/kartu&filter='+$('#search').val()+'&cekby='+$('#cekby').val()
	        });

	     });

	     $('#cekby').change(function(){

	        $('#tabelpasien').yiiGridView.update('tabelpasien', {
	            url:'?r=pasien/kartu&filter='+$('#search').val()+'&cekby='+$('#cekby').val()
	        });

	     });
	});
</script>
<?php


$updateJS = CHtml::ajax( array(
  'url' => "js:url",
  'data' => "js:form.serialize() + action",
  'type' => 'post',
  'dataType' => 'json',
  'success' => "function( data )
  {

    if( data.status == 'failure' )
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      $( '#update-dialog div.update-dialog-content ' )
        .off() // Stop from re-binding event handlers

        .on( 'click','form input[type=submit]', function( e ){ // Send clicked button value
          e.preventDefault();
          updateDialog( false, $( this ).attr( 'name' ) );
      });
    }
    else
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      if( data.status == 'success' ) // Update all grid views on success
      {
        $( '#tabelpasien' ).each( function(){ // Change the selector if you use different class or element
          $.fn.yiiGridView.update( $( this ).attr( 'id' ) );
        });


      }
      setTimeout( \"$( '#update-dialog' ).dialog( 'close' ).children( ':eq(0)' ).empty();\", 1000 );
    }
  }"
));

Yii::app()->clientScript->registerScript( 'updateDialog', "
function updateDialog( url, act )
{
  var action = '';
  var form = $( '#update-dialog div.update-dialog-content form' );
  if( url == false )
  {
    action = '&action=' + act;
    url = form.attr( 'action' );
  }
  {$updateJS}
}" );
?>

<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span3" id="sidebar">


        </div> -->

        <!--/span-->
        <div class="span12" id="content">
            <div class="row-fluid">

                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Pasien','url'=>Yii::app()->createUrl('pasien/index')),
                            array('name' => 'Cek Pasien')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>
                    </div>
                </div>
            </div>
            <h1>.: CETAK KARTU :.</h1>


            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Data Pasien</div>
                         <div class="pull-right">Cek Pasien Berdasarkan :
                            <?php
                            $params = array('Nama','No RM','NIK','No Kartu BPJS');
         echo CHtml::dropDownList('Pasien[CEKBY]', isset($_GET['cekby'])?$_GET['cekby']:'',$params,array('id'=>'cekby'));

         echo CHtml::textField('Pasien[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> </div>
                    </div>
                    <div class="block-content collapse in">


<?php
$datanotfound = '<div class="alert alert-info">
						<button class="close" data-dismiss="alert">&times;</button>
						<strong>Info !</strong> Apakah Anda ingin melanjutkan ke pendaftaran
							<a href="'.Yii::app()->createUrl("registrasi/pasien",array("jenis"=>"bpjs")).  '">
		                  	<button class="btn btn-success btn-mini"> BPJS</button></a>&nbsp;atau&nbsp;
		                  	<a href="'.Yii::app()->createUrl("registrasi/pasien",array("jenis"=>"nonbpjs")).'">
		                  	<button class="btn btn-primary btn-mini"> Non-BPJS</button></a>&nbsp;?

					</div>';

$this->widget('application.components.ComplexGridView', array(
  'id'=>'tabelpasien',
  'dataProvider'=>$model->searchPasienBy(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
    'NoMedrec',
    'NAMA',
    array(
      'header' => 'Alamat',
      'value' => '$data->ALAMAT',
    ),
    'NamaSuamiIstri',
    array(
    	'header'=> 'Tindakan',
      'class'=>'CButtonColumn',
      'template'=>'{cetakkartu}  {cetakETiket} {cetakGelang}',
      'buttons' => array(

        'cetakkartu'   => array(
          'visible' => 'Yii::app()->user->checkAccess(array(WebUser::R_SA, WebUser::R_OP_DAFTAR))',
          'url'=>'Yii::app()->createUrl("pasien/cetakKartu/", array("id"=>$data->NoMedrec))',
          'label'=>'<span class="label label-info"><i class="icon-print"></i>&nbsp;Cetak Kartu&nbsp;</span>',
          'options'=>array(
              'title'=>'Cetak Kartu',
              'target' => '_blank',
            ),

        ),
        'cetakETiket'   => array(
          'visible' => 'Yii::app()->user->checkAccess(array(WebUser::R_SA, WebUser::R_OP_DAFTAR))',
          'url'=>'Yii::app()->createUrl("pasien/cetakETiket/", array("id"=>$data->NoMedrec))',
          'label'=>'<span class="label label-info"><i class="icon-print"></i>&nbsp;Cetak E-Tiket&nbsp;</span>',
          'options'=>array(
              'title'=>'Cetak E-Tiket',
              'target' => '_blank',
            ),

        ),
        'cetakGelang'   => array(
          'visible' => 'Yii::app()->user->checkAccess(array(WebUser::R_SA, WebUser::R_OP_DAFTAR))',
          'url'=>'Yii::app()->createUrl("pasien/cetakGelang/", array("id"=>$data->NoMedrec))',
          'label'=>'<span class="label label-info"><i class="icon-print"></i>&nbsp;Cetak Gelang&nbsp;</span>',
          'options'=>array(
              'title'=>'Cetak Gelang',
              'target' => '_blank',
            ),

        ),
        // 'batalbayar'   => array(
        //   'visible' => '$data->CETAK_KARTU==1',
        //   'url'=>'Yii::app()->createUrl("pasien/batalKartu/", array("id"=>$data->NoMedrec))',
        //   'click' => "function( e ){
        //         e.preventDefault();
        //         $( '#update-dialog' ).children( ':eq(0)' ).empty(); // Stop auto POST
        //         updateDialog( $( this ).attr( 'href' ) );
        //         $( '#update-dialog' )
        //           .dialog( { title: 'Pembatalan Kartu' } )
        //           .dialog( 'open' ); }",
        //   'label'=>'<span class="label label-warning"><i class="icon-share-alt"></i>&nbsp;Batal&nbsp;</span>',
        //   'options'=>array(
        //       'title'=>'Batal Kartu',
        //     ),

        // ),
        // 'bayarkartu'   => array(
        //   'visible' => '$data->CETAK_KARTU==0 && Yii::app()->user->checkAccess(array(WebUser::R_SA, WebUser::R_OP_KASIR))',
        //   'url'=>'Yii::app()->createUrl("pasien/ApproveKartu/", array("id"=>$data->NoMedrec))',
        //   'click' => "function( e ){
        //         e.preventDefault();
        //         $( '#update-dialog' ).children( ':eq(0)' ).empty(); // Stop auto POST
        //         updateDialog( $( this ).attr( 'href' ) );
        //         $( '#update-dialog' )
        //           .dialog( { title: 'Pembayaran Kartu' } )
        //           .dialog( 'open' ); }",
        //   'label'=>'<span class="label"><i class="icon-share-alt"></i>&nbsp;Bayar Kartu&nbsp;</span>',
        //   'options'=>array(
        //       'title'=>'Bayar Kartu',
        //     ),
        // ),

      ),

    ),
  ),

  'pager'=>array(
                        'class'=>'SimplePager',
                        'header'=>'',
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',
                        'lastPageLabel'=>'Terakhir',
            'firstPageCssClass'=>'btn',
                        'previousPageCssClass'=>'btn',
                        'nextPageCssClass'=>'btn',
                        'lastPageCssClass'=>'btn',
            'hiddenPageCssClass'=>'disabled',
            'internalPageCssClass'=>'btn',
            'selectedPageCssClass'=>'btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table table-striped table-bordered',
  'summaryCssClass'=>'dataTables_info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>$datanotfound,
  'pagerCssClass'=>'dataTables_paginate paging_bootstrap pagination',
)); ?>

                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>

</div>

<?php

$this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
  'id' => 'update-dialog',
  'options' => array(
    'title' => 'Dialog',
    'autoOpen' => false,
    'modal' => true,
    'width' => 250,
    'resizable' => false,
  ),
)); ?>
<div class="update-dialog-content"></div>
<?php $this->endWidget(); ?>
