<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Pasien','url'=>Yii::app()->createUrl('pasien/index')),
                            array('name' => 'List')
);

?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<script type="text/javascript">
	
	$(document).ready(function(){
		$('#search, #size').change(function(){
    
	        $('#tabelpasien').yiiGridView.update('tabelpasien', {
	            url:'<?php echo Yii::app()->createUrl("pasien/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()   
	        });
	      
	     });

	   
	});
</script>
<div class="row">
    <div class="col-xs-12">
                        <div class="muted pull-left">Formulir Data Pasien</div>
                         <div class="pull-right">Data per halaman
                            <?php echo CHtml::dropDownList('Pasien[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); ?> <?php
        echo CHtml::textField('Pasien[SEARCH]','',array('placeholder'=>'Cari','id'=>'search')); 
        ?> 	 </div> 
                        
                      
<?php $this->widget('application.components.ComplexGridView', array(
  'id'=>'tabelpasien',
  'dataProvider'=>$model->search(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
    'NoMedrec',
    'NAMA',
    'ALAMAT',
    'JENSKEL',
    'NamaSuamiIstri',
    array(
      'type'=>'raw',
      'header' => 'Actions',
      'value' => function($data){

        $html = '<div class="btn-group">
      <button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle">
        Tindakan
        <span class="ace-icon fa fa-caret-down icon-on-right"></span>
      </button>

      <ul class="dropdown-menu dropdown-info dropdown-menu-right">
        <li>
          <a href="'.Yii::app()->createUrl("pasien/update/", ["id"=>$data->NoMedrec]).'"><i class="ace-icon fa fa-pencil-square-o bigger-120"></i> Update Data</a>
        </li>

        
         <li class="divider"></li>
      
        <li>
          <a target="_blank" href="'.Yii::app()->createUrl("pasien/cetakKartu/", array("id"=>$data->NoMedrec)).'"><i class="ace-icon fa fa-print bigger-120"></i> Cetak Kartu</a>
        </li> 
       
      </ul>
    </div>';

    return $html;
          }
        ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
  'pager'=>array(
                        'class'=>'SimplePager',                  
                        'header'=>'',                      
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',        
                        'lastPageLabel'=>'Terakhir',  
           'firstPageCssClass'=>'btn btn-info',
                        'previousPageCssClass'=>'btn btn-info',
                        'nextPageCssClass'=>'btn btn-info',        
                        'lastPageCssClass'=>'btn btn-info',
            'hiddenPageCssClass'=>'disabled',
            'internalPageCssClass'=>'btn btn-info',
            'selectedPageCssClass'=>'btn btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table  table-bordered table-hover',
  'summaryCssClass'=>'table-message-info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
 'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>

                    </div>
                </div>
             
  