<?php
/* @var $this PasienController */
/* @var $model Pasien */

$this->breadcrumbs=array(
	array('name' => 'Pasien','url'=>Yii::app()->createUrl('pasien/index')),
    array('name' => 'Update')
);


?>
	<div class="row">
					<div class="col-xs-12">
<?php $this->renderPartial('_form', array(
	'pasien'=>$pasien,
	'alamat' => $alamat
)); ?>
	</div>
				</div>
