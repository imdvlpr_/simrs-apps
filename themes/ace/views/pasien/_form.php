<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
)); 
 
?>



<div class="row">
  <?php 
foreach(Yii::app()->user->getFlashes() as $key => $message) {
  echo '<div class="alert alert-' . $key . '">'
      .'<button type="button" class="close" data-dismiss="alert">×</button>'
      . $message . "</div>\n";
}
  ?>
  <div class="col-sm-4">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Pasien</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
          
                <!-- Medical Record Number Field -->
                 <!-- Medical Record Number Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NoMedrec',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'NoMedrec',array('class'=>'input-large','readonly'=>'readonly'));
                        echo $form->error($pasien,'NoMedrec');
                    ?>
                     <!--  <a id='carirm' href="javascript:void(0);"><i class="icon-search"></i> Cari RM</a><span id='loading3' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span> -->
                  </div>
                </div>

                <!-- Identification Number Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NOIDENTITAS',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <!-- TODO: Input Number Only! -->
                    <?php echo $form->textField($pasien,'NOIDENTITAS',array(
                      'class'=>'input-large',
                      'tabindex'=>'1',
                      'placeholder' => '16 digit Nomor KTP',
                      'minLength' => '16',
                      'maxLength' => '16',
                      ));?>
                    <?php echo $form->error($pasien,'NOIDENTITAS'); ?>
                  </div>
                </div>

                <!-- Patient's Name Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NAMA',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'NAMA',array('class'=>'input-large','tabindex'=>'2')); ?>
                    <?php 
                    echo $form->dropDownList($alamat,'salutation',[
                      'TN'=>'Tuan',
                      'NY'=>'Nyonya',
                      'SDR' => 'Saudara',
                      'NN' => 'Nona',
                      'ANAK'=>'Anak'
                    ]);
                    ?>
                    <?php echo $form->error($pasien,'NAMA'); ?>
                  </div>
                </div>

                <!-- Patient's Sex Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'JENSKEL',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->dropDownList($pasien, 'JENSKEL',array('L'=>'Laki-Laki', 'P'=>'Perempuan'),array('tabindex'=>'4'));

                  
                    echo $form->error($pasien,'JENSKEL');
                    ?>
                  </div>
                </div>

                <!-- Patient's Date of Birth -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'TGLLAHIR',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'TGLLAHIR',array('class'=>'input-medium'));?>

                    <?php 
                     echo $form->error($pasien,'TGLLAHIR'); 
                    ?>
                  </div>
                </div>

               
                
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
     
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Alamat Lengkap</h4>
        </div>
         <div class="widget-body">
          <div class="widget-main">
            <div class="form-group">
                  <?php echo $form->labelEx($alamat,'propinsi_id',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php 
                  $list = CHtml::ListData(Refprov::model()->findAll(),'KodeProv',function($data){
                    return strtoupper($data->NamaProv);
                  });
                  echo $form->dropDownList($alamat,'propinsi_id',$list,array(
                  'empty' => '- Pilih Propinsi -',
                  'ajax' => array(
                  'type'=>'POST', //request type
                  'url'=>CController::createUrl('ajaxRequest/getKota'), //url to call.
                  //Style: CController::createUrl('currentController/methodToCall')
                  'update'=>'#PasienAlamat_kota_id', //selector to update
                  //'data'=>'js:javascript statement' 
                  //leave out the data key to pass all form values through
                  )));
                  echo $form->error($alamat,'propinsi_id'); ?>
                  </div>
                </div>
                 <div class="form-group">
                  <?php echo $form->labelEx($alamat,'kota_id',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php 
                   

                  //empty since it will be filled by the other dropdown
                  echo $form->dropDownList($alamat,'kota_id',['empty'=>'-Pilih Kota-'],
                    array(
                  'ajax' => array(
                  'type'=>'POST', //request type
                  'url'=>CController::createUrl('ajaxRequest/getKec'), //url to call.
                  //Style: CController::createUrl('currentController/methodToCall')
                  'update'=>'#PasienAlamat_kecamatan_id', //selector to update
                  //'data'=>'js:javascript statement' 
                  //leave out the data key to pass all form values through
                  ))
                ); 
                  
                  echo $form->error($alamat,'kota_id'); ?>
                  </div>
                </div>
                  <div class="form-group">
                  <?php echo $form->labelEx($alamat,'kecamatan_id',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php 
                   

                  //empty since it will be filled by the other dropdown
                  echo $form->dropDownList($alamat,'kecamatan_id',['empty'=>'-Pilih Kecamatan-']); 
                  
                  echo $form->error($alamat,'kecamatan_id'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <?php echo $form->labelEx($alamat,'desa',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php echo $form->textField($alamat,'desa',array('class'=>'input-large'));
                  echo $form->error($alamat,'desa'); ?>
                  </div>
                </div>

                
                
                  <div class="form-group">
                  <?php echo CHtml::label('RT/RW','',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php 
                  echo $form->textField($alamat,'rt',array('class'=>'input-small'));
                  echo '&nbsp;';
                  echo $form->textField($alamat,'rw',array('class'=>'input-small'));
                  echo $form->error($alamat,'rt');
                  echo $form->error($alamat,'rw');
                   ?>
                  </div>
                </div>
                 <div class="form-group">
                  <?php echo $form->labelEx($alamat,'jalan',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php echo $form->textField($alamat,'jalan',array('class'=>'input-large'));
                  echo $form->error($alamat,'jalan'); ?>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Info Detil</h4>
        </div>

        <div class="widget-body">
          <div class="widget-main">
    

         <div class="form-group">
                  <?php echo $form->labelEx($pasien,'TMPLAHIR',array('class'=>'col-sm-3  control-label  no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'TMPLAHIR',array('class'=>'input-large'));
                    echo $form->error($pasien,'TMPLAHIR'); ?>
                  </div>
                </div>

                <!-- Weight Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'BeratLahir',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'BeratLahir',array(
                      'class'=>'input-large',
                      'onkeypress' => 'return weightNumber(event)',
                      'placeholder' => 'Berat dalam Kg. Contoh: 64.5'
                    ));
                    echo $form->error($pasien,'BeratLahir');?>
                  </div>
                </div>

                <!-- Blood Type Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'GOLDARAH',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php
                      echo $form->dropDownList($pasien,'GOLDARAH',CHtml::ListData(DmGoldarah::model()->findAll(),'id_goldarah','nama_goldarah'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->GOLDARAH=>array('selected'=>true))));
                      echo $form->error($pasien,'GOLDARAH');
                    ?>
                  </div>
                </div>

                 <div class="form-group">
                  <?php echo $form->labelEx($pasien,'AGAMA',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                  <?php
                     echo $form->dropDownList($pasien,'AGAMA',CHtml::ListData(DmAgama::model()->findAll(array('order'=>'id_agama ASC')),'id_agama','nama_agama'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->AGAMA=>array('selected'=>true))));
                     echo $form->error($pasien,'AGAMA');
                  ?>
                  </div>
                </div>

                <!-- Marital Status -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'STATUSPERKAWINAN',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php
                      echo $form->dropDownList($pasien,'STATUSPERKAWINAN',CHtml::ListData(DmStatuskawin::model()->findAll(array('order'=>'id_statuskawin ASC')),'id_statuskawin','nama_statuskawin'),['empty'=>'Pilih Status Kawin']);
                      echo $form->error($pasien,'STATUSPERKAWINAN');
                    ?>
                  </div>
                </div>

                <!-- Job Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'PEKERJAAN',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php
                      echo $form->dropDownList($pasien,'PEKERJAAN',CHtml::ListData(DmPekerjaan::model()->findAll(array('order'=>'id_pekerjaan ASC')),'nama_pekerjaan','nama_pekerjaan'),array('prompt'=>'','class'=>'span4','options'=>array($pasien->PEKERJAAN=>array('selected'=>true))));
                      echo $form->error($pasien,'PEKERJAAN');
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'TELP',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'TELP',array(
                      'class'=>'input-large',
                      
                      ));
                    echo $form->error($pasien,'TELP'); ?>
                  </div>
                </div>

                <!-- Parent's Name Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NamaOrtu',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'NamaOrtu',array('class'=>'input-large'));
                    echo $form->error($pasien,'NamaOrtu'); ?>
                  </div>
                </div>

                <!-- Principal's Name (Husband / Wife) Field -->
                <div class="form-group">
                  <?php echo $form->labelEx($pasien,'NamaSuamiIstri',array('class'=>'col-sm-3  col-sm-3  control-label no-padding-right no-padding-right'));?>
                  <div class="col-sm-9">
                    <?php echo $form->textField($pasien,'NamaSuamiIstri',array('class'=>'input-large'));
                    echo $form->error($pasien,'NamaSuamiIstri'); ?>
                  </div>
                </div>
              
          </div>
        </div>
      </div>
    </div>

</div>
 
                <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>

        
        </div>
      </div>

  <?php $this->endWidget();?>

      <?php 
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
   
$cs->registerScriptFile($baseUrl.'/assets/focus-next.js');
?>    


<script>

function fetchKecamatan(kota_id){
  $.ajax({
    type : 'POST',
    url : '<?=Yii::app()->createUrl('ajaxRequest/getKec');?>',
    data : 'kid='+kota_id,
    success : function(data){
      $('#PasienAlamat_kecamatan_id').empty();
      $('#PasienAlamat_kecamatan_id').append(data);
      $('#PasienAlamat_kecamatan_id').val('<?=$alamat->kecamatan_id;?>');
      
    }
  });
}


function fetchKota(prop_id){
  $.ajax({
    type : 'POST',
    url : '<?=Yii::app()->createUrl('ajaxRequest/getKota');?>',
    data : 'pid='+prop_id,
    success : function(data){
      $('#PasienAlamat_kota_id').empty();
      $('#PasienAlamat_kota_id').append(data);
      $('#PasienAlamat_kota_id').val('<?=$alamat->kota_id;?>');
      fetchKecamatan(<?=$alamat->kota_id;?>);
    }
  });
}


$(document).ready(function(){

  fetchKota($('#PasienAlamat_propinsi_id').val());

  $('#Pasien_TGLLAHIR, #BPendaftaran_TGLDAFTAR').datetextentry(); 
  $('#Pasien_NOIDENTITAS').keydown(function(e){
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    var txt = $(this).val();
      
    if(key == 13 && txt != ''){
      var tgl = eval(txt.substring(6,8));
      var bln = txt.substring(8,10);
      var thn = eval(txt.substring(10,12));
     

      if(tgl > 40){
        $('#Pasien_JENSKEL').val('P');
        tgl = tgl - 40;
      }

      else{
        $('#Pasien_JENSKEL').val('L');
      }

      if(thn < 20 ){
        var ttl = '20'+thn+'-'+bln+'-'+tgl;
       
      }

      else{
        var ttl = '19'+thn+'-'+bln+'-'+tgl;
      }
       $('#Pasien_TGLLAHIR').datetextentry('set_date', ttl);
      // console.log('a'+$('#Pasien_TGLLAHIR').val());
    }
  });

  $('#Pasien_TGLLAHIR, #BPendaftaran_TGLDAFTAR').datetextentry(); 



});




 </script>
