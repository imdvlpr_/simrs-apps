<?php
/* @var $this PasienController */
/* @var $data Pasien */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoMedrec')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->NoMedrec), array('view', 'id'=>$data->NoMedrec)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NAMA')); ?>:</b>
	<?php echo CHtml::encode($data->NAMA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ALAMAT')); ?>:</b>
	<?php echo CHtml::encode($data->ALAMAT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeKec')); ?>:</b>
	<?php echo CHtml::encode($data->KodeKec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TMPLAHIR')); ?>:</b>
	<?php echo CHtml::encode($data->TMPLAHIR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TGLLAHIR')); ?>:</b>
	<?php echo CHtml::encode($data->TGLLAHIR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PEKERJAAN')); ?>:</b>
	<?php echo CHtml::encode($data->PEKERJAAN); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('AGAMA')); ?>:</b>
	<?php echo CHtml::encode($data->AGAMA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JENSKEL')); ?>:</b>
	<?php echo CHtml::encode($data->JENSKEL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GOLDARAH')); ?>:</b>
	<?php echo CHtml::encode($data->GOLDARAH); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TELP')); ?>:</b>
	<?php echo CHtml::encode($data->TELP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JENISIDENTITAS')); ?>:</b>
	<?php echo CHtml::encode($data->JENISIDENTITAS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NOIDENTITAS')); ?>:</b>
	<?php echo CHtml::encode($data->NOIDENTITAS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('STATUSPERKAWINAN')); ?>:</b>
	<?php echo CHtml::encode($data->STATUSPERKAWINAN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BeratLahir')); ?>:</b>
	<?php echo CHtml::encode($data->BeratLahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Desa')); ?>:</b>
	<?php echo CHtml::encode($data->Desa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeGol')); ?>:</b>
	<?php echo CHtml::encode($data->KodeGol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TglInput')); ?>:</b>
	<?php echo CHtml::encode($data->TglInput); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JamInput')); ?>:</b>
	<?php echo CHtml::encode($data->JamInput); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AlmIp')); ?>:</b>
	<?php echo CHtml::encode($data->AlmIp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoMedrecLama')); ?>:</b>
	<?php echo CHtml::encode($data->NoMedrecLama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoKpst')); ?>:</b>
	<?php echo CHtml::encode($data->NoKpst); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodePisa')); ?>:</b>
	<?php echo CHtml::encode($data->KodePisa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KdPPK')); ?>:</b>
	<?php echo CHtml::encode($data->KdPPK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NamaOrtu')); ?>:</b>
	<?php echo CHtml::encode($data->NamaOrtu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NamaSuamiIstri')); ?>:</b>
	<?php echo CHtml::encode($data->NamaSuamiIstri); ?>
	<br />

	*/ ?>

</div>