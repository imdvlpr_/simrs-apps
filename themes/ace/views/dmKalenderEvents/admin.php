<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);

?>
<style>
    div#sidebar{
        padding-right:1%;
    }

</style>
<script type="text/javascript">
	
	$(document).ready(function(){
		$('#search, #size').change(function(){
    
	        $('#tabelpasien').yiiGridView.update('tabelpasien', {
	            url:'<?php echo Yii::app()->createUrl("DmKalender/admin"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()   
	        });
	      
	     });

	});
</script>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3" id="sidebar">
        	 <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
				<li>
					<a href="<?php echo Yii::app()->createUrl('DmKalender');?>"><i class="icon-chevron-right"></i> Kalender</a>
				</li>
				<li>
					<a href="<?php echo Yii::app()->createUrl('DmKalenderEvents/create');?>"><i class="icon-chevron-right"></i> Tambah Event Baru</a>
				</li>
			</ul>
            
        </div>
        
        <!--/span-->
        <div class="span9" id="content">
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Kalender','url'=>Yii::app()->createUrl('DmKalenderEvents/index')),
                            array('name' => 'Event List')
                          ),
                          'delimiter' => '&raquo;', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                  
                    <div class="block-content collapse in">
                        
                      
<?php $this->widget('application.components.ComplexGridView', array(
  'id'=>'tabelpasien',
  'dataProvider'=>$model->search(),

  // 'filter'=>$model,
  'columns'=>array(
    array(
			'header' => 'No',
			'value'	=> '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
    'nama_event',
    array(
      'class'=>'CButtonColumn',
      'template'=>'{update} {delete}',
      
    ),
  ),
  'htmlOptions'=>array(
    'class'=>'table'
  ),
  'pager'=>array(
                        'class'=>'SimplePager',                  
                        'header'=>'',                      
                        'firstPageLabel'=>'Pertama',
                        'prevPageLabel'=>'Sebelumnya',
                        'nextPageLabel'=>'Selanjutnya',        
                        'lastPageLabel'=>'Terakhir',  
            'firstPageCssClass'=>'btn',
                        'previousPageCssClass'=>'btn',
                        'nextPageCssClass'=>'btn',        
                        'lastPageCssClass'=>'btn',
            'hiddenPageCssClass'=>'disabled',
            'internalPageCssClass'=>'btn',
            'selectedPageCssClass'=>'btn-sky',
            'maxButtonCount'=>5,
                ),
  'itemsCssClass'=>'table table-striped table-content table-hover dataTable',
  'summaryCssClass'=>'table-message-info',
  'filterCssClass'=>'filter',
  'summaryText'=>'menampilkan {start} - {end} dari {count} data',
  'template'=>'{items}{summary}{pager}',
  'emptyText'=>'Data tidak ditemukan',
  'pagerCssClass'=>'btn-group paging_full_numbers',
)); ?>

                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>