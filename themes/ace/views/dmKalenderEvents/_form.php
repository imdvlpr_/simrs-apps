  <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model);

?>          
    <legend>
      

    </legend>
    

  <div class="control-group">
    <?php echo $form->labelEx($model,'nama_event',array('class'=>'control-label'));?>
      <div class="controls">
        <?php 
          echo $form->textField($model,'nama_event',array('class'=>'input-large'));

          
        ?>
       
        <?php echo $form->error($model,'nama_event'); ?>
      </div>
    </div>
   

   
    <div class="form-actions" align="center">

      <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-success')); ?>
    </div>
  </fieldset>
  <?php $this->endWidget();?>