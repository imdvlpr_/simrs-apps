<?php
/* @var $this DmKalenderEventsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dm Kalender Events',
);

$this->menu=array(
	array('label'=>'Create DmKalenderEvents', 'url'=>array('create')),
	array('label'=>'Manage DmKalenderEvents', 'url'=>array('admin')),
);
?>

<h1>Dm Kalender Events</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
