 <?php
$this->breadcrumbs=array(
	array('name'=>'Rawat Jalan','url'=>array('index')),
	array('name'=>'Manage'),
);

?>
<h1>Manage Rawat Jalan</h1>

<script type="text/javascript">
	$(document).ready(function(){
		$('#search,#size').change(function(){
			$('#tr-rawat-jalan-grid').yiiGridView.update('tr-rawat-jalan-grid', {
			    url:'<?php echo Yii::app()->createUrl("TrRawatJalan/index"); ?>&filter='+$('#search').val()+'&size='+$('#size').val()
			});
		});
		
	});
</script>
<div class="row">
    <div class="col-xs-12">
        
            <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }


?>
                       


                         <div class="pull-right">Data per halaman
                              <?php                            echo CHtml::dropDownList('TrRawatJalan[PAGE_SIZE]',isset($_GET['size'])?$_GET['size']:'',array(10=>10,50=>50,100=>100,),array('id'=>'size')); 
                            ?> 
                             <?php        echo CHtml::textField('TrRawatJalan[SEARCH]','',array('placeholder'=>'Cari','id'=>'search'));
        ?> 	 </div>
                    
<?php $this->widget('application.components.ComplexGridView', array(
	'id'=>'tr-rawat-jalan-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
	array(
		'header'=>'No',
		'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)'
		),
		'reg_id',
		'no_medrec',
		'noMedrec.NAMA',
		'unit.NamaUnit',
		'tanggal',
		[
			'name' => 'Status Rujukan',
			'value' => function($model){
				return !empty($model->status_rujukan) ? $model->statusRujukan->NamaSub : '-';
			}
		],
		/*
		'created_at',
		'updated_at',
		*/
		array(
      'type'=>'raw',
      'header' => 'Actions',
      'value' => function($data){

        $html = '<div class="btn-group">
  <button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle">
    Tindakan
    <span class="ace-icon fa fa-caret-down icon-on-right"></span>
  </button>

  <ul class="dropdown-menu dropdown-info dropdown-menu-right">
    ';
    
    if($data->is_locked == 0){
    $html .= '<li>
      <a href="'.Yii::app()->createUrl("trRawatJalan/update/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-pencil-square-o bigger-120"></i> Update Data</a>
    </li><li class="divider"></li>';
    $html .= '<li>
      <a href="'.Yii::app()->createUrl("trRawatJalanTindakan/create/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-pencil-square-o bigger-120"></i> Input Tindakan</a>
    </li><li class="divider"></li>';
    
    }
     $html .= '<li>
      <a href="'.Yii::app()->createUrl("trRawatJalanTindakan/index/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-list bigger-120"></i> List Tindakan</a>
    </li><li class="divider"></li>';
    if($data->is_locked == 0){
    $html .= ' <li>
      <a href="'.Yii::app()->createUrl("trRawatJalan/rujukan/", array("id"=>$data->id)).'"><i class="ace-icon fa fa-sign-out bigger-120"></i> Rujukan</a>
    </li> ';
    }
    $html .= '
  </ul>
</div>';

return $html;
      }
    ),
	),
	'htmlOptions'=>array(
									'class'=>'table'
								),
								'pager'=>array(
									'class'=>'SimplePager',
									'header'=>'',
									'firstPageLabel'=>'Pertama',
									'prevPageLabel'=>'Sebelumnya',
									'nextPageLabel'=>'Selanjutnya',
									'lastPageLabel'=>'Terakhir',
									'firstPageCssClass'=>'btn btn-info',
									'previousPageCssClass'=>'btn btn-info',
									'nextPageCssClass'=>'btn btn-info',
									'lastPageCssClass'=>'btn btn-info',
									'hiddenPageCssClass'=>'disabled',
									'internalPageCssClass'=>'btn btn-info',
									'selectedPageCssClass'=>'btn btn-sky',
									'maxButtonCount'=>5
								),
								'itemsCssClass'=>'table  table-bordered table-hover',
								'summaryCssClass'=>'table-message-info',
								'filterCssClass'=>'filter',
								'summaryText'=>'menampilkan {start} - {end} dari {count} data',
								'template'=>'{items}{summary}{pager}',
								'emptyText'=>'Data tidak ditemukan',
								'pagerCssClass'=>'pagination pull-right no-margin',
)); ?>


	</div>
</div>

