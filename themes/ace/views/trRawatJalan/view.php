<?php
/* @var $this TrRawatJalanController */
/* @var $model TrRawatJalan */

$this->breadcrumbs=array(
	array('name'=>'Tr Rawat Jalan','url'=>array('admin')),
	array('name'=>'Tr Rawat Jalan'),
);

$this->menu=array(
	array('label'=>'List TrRawatJalan', 'url'=>array('index')),
	array('label'=>'Create TrRawatJalan', 'url'=>array('create')),
	array('label'=>'Update TrRawatJalan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrRawatJalan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrRawatJalan', 'url'=>array('admin')),
);
?>

<h1>View TrRawatJalan #<?php echo $model->id; ?></h1>
 <?php    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
	<div class="col-xs-12">
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'reg_id',
		'no_medrec',
		'unit_id',
		'tanggal',
		'dokter_id',
		'created_at',
		'updated_at',
	),
)); ?>
	</div>
</div>
