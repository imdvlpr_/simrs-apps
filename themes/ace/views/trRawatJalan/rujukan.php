<?php
/* @var $this TrRawatJalanController */
/* @var $model TrRawatJalan */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tr-rawat-jalan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>
<h1>Rujukan Pasien</h1>
<div class="row">
  <div class="col-sm-6">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Pasien</h4>
           <img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
        </div>
        <div class="widget-body">
          <div class="widget-main">

											
          	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>
			<div class="profile-user-info profile-user-info-striped">
				<div class="profile-info-row">
					<div class="profile-info-name"> No RM </div>

					<div class="profile-info-value">
						<?php echo $form->hiddenField($model,'reg_id'); ?>	
						<?php echo $form->hiddenField($model,'no_medrec'); ?>
						<span ><?=$model->no_medrec;?></span>
					</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name"> Nama </div>

					<div class="profile-info-value">
						<span id="nama_pasien"></span>
					</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name"> Alamat </div>

					<div class="profile-info-value">
						<span id="alamat"></span>
					</div>
				</div>

				<div class="profile-info-row">
					<div class="profile-info-name"> JK </div>

					<div class="profile-info-value">
						<span id="jk"></span>
					</div>
				</div>

				<div class="profile-info-row">
					<div class="profile-info-name"> Unit </div>

					<div class="profile-info-value">
						<span><?=$model->unit->NamaUnit;?></span>
						<?php echo $form->hiddenField($model,'unit_id'); ?>
					</div>
				</div>

				<div class="profile-info-row">
					<div class="profile-info-name"> Tgl MRS </div>

					<div class="profile-info-value">
						<span ><?=$model->tanggal;?></span>
					</div>
				</div>

			</div>

	
          	

          </div>
      </div>
    </div>
</div>
<div class="col-sm-6">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Diagnosis</h4>
        </div>
        <div class="widget-body">
          <div class="widget-main">

	<div class="form-group">
		<?php echo $form->labelEx($model,'status_rujukan', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'5')); ?>
		<div class="col-sm-9">
		<?php echo $form->dropDownList($model,'status_rujukan',$listRefcarapulang ,['class'=>'form-control','prompt'=>'- Pilih Status']); ?>
		<?php echo $form->error($model,'status_rujukan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'keterangan', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'6')); ?>
		<div class="col-sm-9">
		<?php echo $form->textField($model,'keterangan',['class'=>'form-control']); ?>
		<?php echo $form->error($model,'keterangan'); ?>
		</div>
	</div>

	

	<div class="form-group">
		<?php echo $form->labelEx($model,'tanggal_rujukan', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'7')); ?>
		<div class="col-sm-9">
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'tanggal_rujukan',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
		?>
		<?php echo $form->error($model,'tanggal_rujukan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo CHtml::label('Unit','', array ('class'=>'col-sm-3 control-label no-padding-right', 'tabindex'=>'6')); ?>
		<div class="col-sm-9">
		<?php echo $form->dropDownList($model,'unit_tujuan_id',$listPoli,['class'=>'form-control','prompt'=>'Pilih Unit']); ?>
		</div>
	</div>
	</div>
          </div>
      </div>
    </div>
</div>
</div>
	


	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>

<script type="text/javascript">
function getDataPasien(rm){
	$.ajax({
		type : 'POST',
		url : '<?=Yii::app()->createUrl('ajaxRequest/getPasienByRM');?>',
		data : 'norm='+rm,
		beforeSend : ()=>{
			$('#loading').show();
		},
		error : (e)=>{
			$('#loading').hide();
			console.log(e);
		},
		success : (data)=>{
			$('#loading').hide();
			let hasil = $.parseJSON(data);
			
			$('#nama_pasien').html(hasil.pasien.NAMA);
			$('#alamat').html(hasil.pasien.ALAMAT);
			$('#jk').html(hasil.pasien.JENSKEL);
		}
	});
}

$(document).ready(()=>{
	getDataPasien('<?=$model->no_medrec;?>');
});
</script>