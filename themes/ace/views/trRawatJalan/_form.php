<?php
/* @var $this TrRawatJalanController */
/* @var $model TrRawatJalan */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tr-rawat-jalan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

<?php 
   foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Pasien</h4>
           <img id="loading" style="display: none" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
        </div>
        <div class="widget-body">
          <div class="widget-main">

											
          	<?php echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa kesalahan berikut:','</div>'); ?>
			<div class="profile-user-info profile-user-info-striped">
				<div class="profile-info-row">
					<div class="profile-info-name"> No RM </div>

					<div class="profile-info-value">
						<?php echo $form->hiddenField($model,'reg_id'); ?>	
						<?php echo $form->hiddenField($model,'no_medrec'); ?>
						<span ><?=$model->no_medrec;?></span>
					</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name"> Nama </div>

					<div class="profile-info-value">
						<span id="nama_pasien"></span>
					</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name"> Alamat </div>

					<div class="profile-info-value">
						<span id="alamat"></span>
					</div>
				</div>

				<div class="profile-info-row">
					<div class="profile-info-name"> JK </div>

					<div class="profile-info-value">
						<span id="jk"></span>
					</div>
				</div>

				<div class="profile-info-row">
					<div class="profile-info-name"> Unit </div>

					<div class="profile-info-value">
						<span><?=$model->unit->NamaUnit;?></span>
						<?php echo $form->hiddenField($model,'unit_id'); ?>
					</div>
				</div>

				<div class="profile-info-row">
					<div class="profile-info-name"> Tgl MRS </div>

					<div class="profile-info-value">
						<span ><?=$model->tanggal?></span>
					</div>
				</div>

			</div>

	
          	

          </div>
      </div>
    </div>
</div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="widget-box widget-color-blue2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">5 Riwayat Kunjungan Terakhir</h4>
        </div>
        <div class="widget-body">
          <div class="widget-main">
			<div class="profile-user-info profile-user-info-striped">
				
				<table class="table table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Jenis</th>
							<th>Unit</th>
							<th>Tgl MRS</th>
							<th width="16%">Diagnosis/<br>ICD 10</th>
							<th width="16%">Tindakan/<br>ICD 9-CM</th>
						</tr>
					</thead>
				
					<tbody>
				<?php 
				$i = 0;
				foreach ($riwayatKunjungan as $key => $value) {
					
					foreach ($value->bPendaftaranRjalans as $q => $v) {
						# code...
						
						$i++;
				?>
						<tr>
							<td><?=($i);?></td>
							<td><?=$value->kodeGol->NamaGol;?></td>
							<td><?=$v->unit->NamaUnit;?></td>
							<td><?=$value->TGLDAFTAR;?></td>
							<td><button data-loading-text="Loading..." data-item="<?=$value->NODAFTAR;?>" class="btn btn-info btn-xs view-icd btn-block"><i class="fa fa-eye"></i> Lihat</button>
							</td>
							<td><button data-loading-text="Loading..." data-item="<?=$value->NODAFTAR;?>" class="btn btn-info btn-xs view-icd9 btn-block"><i class="fa fa-eye"></i> Lihat</button>
							</td>
						</tr>
						<?php 
					}
				}
				?>
					</tbody>
				</table>
				
				
			</div>

	
          	

          </div>
      </div>
    </div>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="widget-box widget-color-red2">
        <div class="widget-header">
          <h4 class="widget-title lighter smaller">Data Diagnosis</h4>
        </div>
        <div class="widget-body">
          <div class="widget-main">
   
	<?php
		   	$index = 0;
		   	foreach($breg->listDiagnosis as $key => $item)
		   	{

		   		

		   	?>
		   	<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Diagnosis <span class="numbering"><?=$index+1;?></span></label>
				<div class="col-sm-8">
				<input name="diagnosis[]" class="diagnosis form-control" value="<?=$item->keterangan;?>"/>
				<label class="error_diagnosis"></label>

				</div>
				<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>
			</div>
			<?php 
				$index = $key + 1;
		}
			?>
			<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Diagnosis <span class="numbering"><?=$index+1;?></span></label>
				<div class="col-sm-10">
				<input name="diagnosis[]" class="diagnosis form-control"/>
				<label class="error_diagnosis"></label>
				</div>
			</div>
          </div>
      </div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	      <div class="widget-box widget-color-orange">
	        <div class="widget-header">
	          <h4 class="widget-title lighter smaller">Data Tindakan</h4>
	        </div>
	        <div class="widget-body">
	          <div class="widget-main">
		   	<?php
		   	$index = 0;
		   	foreach($breg->listTindakan as $key => $item)
		   	{

		   		

		   	?>
		   	<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Tindakan <span class="tnumbering"><?=$index+1;?></span></label>
				<div class="col-sm-8">
				<input name="tindakan[]" class="tindakan form-control" value="<?=$item->keterangan;?>"/>
				<label class="error_tindakan"></label>

				</div>
				<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger tremove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>
			</div>
			<?php 
				$index = $key + 1;
		}
			?>
			<div class="form-group">
		   		<label class="col-sm-2 control-label no-padding-right">Tindakan <span class="tnumbering"><?=$index+1;?></span></label>
				<div class="col-sm-10">
				<input name="tindakan[]" class="tindakan form-control"/>
				<label class="error_diagnosis"></label>
				</div>
			</div>
	          </div>
	      </div>
	    </div>
	</div>
</div>
	
<div id="my-modal" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin">Riwayat ICD</h3>
			</div>

			<div class="modal-body">
				<table id="list-icd" class="table table-striped">
					<thead>
						<th>No</th>
						<th>Kode</th>
						<th>Deskripsi</th>
					</thead>

					<tbody>
						
					</tbody>
				</table>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

	<div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Simpan
          </button>
	  </div>
      </div>
             

<?php $this->endWidget(); ?>

<script type="text/javascript">

function getDataPasien(rm){
	$.ajax({
		type : 'POST',
		url : '<?=Yii::app()->createUrl('ajaxRequest/getPasienByRM');?>',
		data : 'norm='+rm,
		beforeSend : ()=>{
			$('#loading').show();
		},
		error : (e)=>{
			$('#loading').hide();
			console.log(e);
		},
		success : (data)=>{
			$('#loading').hide();
			let hasil = $.parseJSON(data);
			
			$('#nama_pasien').html(hasil.pasien.NAMA);
			$('#alamat').html(hasil.pasien.ALAMAT);
			$('#jk').html(hasil.pasien.JENSKEL);
		}
	});
}


function refreshNumbering(){
	

	$('span.numbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});

	

	$('span.tnumbering').each(function(i,obj){
		$(this).html(eval(i+1));
	});
}


$(document).on('keydown','input.diagnosis', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">Diagnosis <span class="numbering"></span></label>';
		row += '<div class="col-sm-8">';
		row += '<input name="diagnosis[]" class="diagnosis form-control"/>';
		row += '<label class="error_diagnosis"></label>';
		row += '</div>';
		row += '<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.diagnosis').last().focus();
	}
});

$(document).on('keydown','input.tindakan', function(e) {
 	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

	if (key == 13) {
		var row = '	<div class="form-group">';
   		row += '<label class="col-sm-2 control-label no-padding-right">Tindakan <span class="tnumbering"></span></label>';
		row += '<div class="col-sm-8">';
		row += '<input name="tindakan[]" class="tindakan form-control"/>';
		row += '<label class="error_tindakan"></label>';
		row += '</div>';
		row += '<div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-danger tremove"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>';
		row += '</div>';  		      

		$(this).parent().parent().parent().append(row);
		
		refreshNumbering();
		$('input.tindakan').last().focus();
	}
});


$(document).on('click','a.tremove, a.remove',function(e){
	e.preventDefault();
	
	$(this).parent().parent().remove();
	refreshNumbering();
});


$(document).ready(()=>{
	getDataPasien('<?=$model->no_medrec;?>');

	$('.view-icd').click(function(e){
		e.preventDefault();
		let selector = $(this);
		let reg = selector.attr('data-item');
		
		$.ajax({
			type : 'POST',
			url : '<?=Yii::app()->createUrl('ajaxRequest/getRincianDiagnosa');?>',
			data : 'reg='+reg,
			beforeSend : ()=>{
				selector.button('loading');
			},
			error : (e)=>{
				selector.button('loading');
				console.log(e);
			},
			success : (data)=>{
				selector.button('reset');
				let hasil = $.parseJSON(data);
				$('#list-icd tbody').empty();
				var row = '';

				$.each(hasil,function(i,obj){
					row += '<tr>';
					row += '<td>'+eval(i+1)+'</td>';
					row += '<td>'+obj.kd+'</td>';
					row += '<td>'+obj.nm+'</td>';
					row += '</tr>';
				});
				
				$('#list-icd tbody').append(row);
				$('#my-modal').modal('show');
			}
		});
	});

	$('.view-icd9').click(function(e){
		e.preventDefault();
		let selector = $(this);
		let reg = selector.attr('data-item');
		
		$.ajax({
			type : 'POST',
			url : '<?=Yii::app()->createUrl('ajaxRequest/getRincianTindakan');?>',
			data : 'reg='+reg,
			beforeSend : ()=>{
				selector.button('loading');
			},
			error : (e)=>{
				selector.button('loading');
				console.log(e);
			},
			success : (data)=>{
				selector.button('reset');
				let hasil = $.parseJSON(data);
				$('#list-icd tbody').empty();
				var row = '';

				$.each(hasil,function(i,obj){
					row += '<tr>';
					row += '<td>'+eval(i+1)+'</td>';
					row += '<td>'+obj.kd+'</td>';
					row += '<td>'+obj.nm+'</td>';
					row += '</tr>';
				});
				
				$('#list-icd tbody').append(row);
				$('#my-modal').modal('show');
			}
		});
	});
});
</script>