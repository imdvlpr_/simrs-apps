<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Laporan Radiologi','url'=>Yii::app()->createUrl('laporan/radiologi')),
                            
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<div class="row" >
    <div class="col-xs-12">
        
                          <?php 

 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
            

<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  {

  ?>
<?php 
 

echo CHtml::link('<span class="label label-info" style="padding:5px 10px">Cetak Laporan Radiologi </span>',array('laporan/radiologiPasien','rid'=>$radio->id_tindakan_penunjang,'t1'=>$tanggal_awal,'t2'=>$tanggal_akhir,'print'=>1),array('style'=>'color:#fff','target'=>'_blank'));
echo '&nbsp;';
echo CHtml::link('<span class="label label-success" style="padding:5px 10px">Ekspor ke XLS </span>',array('laporan/radiologiPasien','rid'=>$radio->id_tindakan_penunjang,'t1'=>$tanggal_awal,'t2'=>$tanggal_akhir,'print'=>2),array('style'=>'color:#fff','target'=>'_blank'));


?>


<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No Registrasi</th>
      <th>Nama Pasien</th>
      <th>Status Px</th>
      <th>Kamar</th>
      <th>Klas</th>
      <th>Instalasi</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
      <th>Lm<br>Dirawat</th>
      <th>Nama Tindakan</th>
      <th>IRD</th>
      <th>IRNA</th>
     <th>Total</th> 
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;



    $all_total_biaya = 0;
    $all_total_ird = 0;
    $all_total_irna = 0;
    foreach($rawatInaps as $ri)
    {
      if(empty($ri)) continue;
      $total_biaya_irna = 0;
      $total_biaya_ird = 0;
      foreach($ri->trRawatInapPenunjangs as $vd)
      {

          // jenis == 0 radiologi
          if(($vd->penunjang->jenis_penunjang == 0) && ($vd->id_penunjang == $radio->id_tindakan_penunjang))
          {
             $total_biaya_irna = $total_biaya_irna + $vd->biaya_irna;
             $total_biaya_ird = $total_biaya_ird + $vd->biaya_ird;
          }   
        
      }

      $all_total_irna = $all_total_irna + $total_biaya_irna;
      $all_total_ird = $all_total_ird + $total_biaya_ird;

      $all_total_biaya = $all_total_biaya + $total_biaya_irna + $total_biaya_ird;
      
      if($all_total_biaya == 0) continue;

      $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }



  ?>
    <tr>
       <td><?php echo $i++;?></td>
       <td><?php 
       echo $ri->pASIEN->NoMedrec;
       ?></td>
      <td><?php 
      echo $ri->pASIEN->NAMA;
      ?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php 
      echo $ri->kamar->nama_kamar;
      ?>
      </td>
      <td>
      <?php 
      echo $ri->kamar->kelas->nama_kelas;
      ?>
      </td>
      <td>Radiologi</td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      <td><?php 
      echo  $radio->nama_tindakan;
       ?></td>
      
      <td style="text-align: right"><?php 
      echo CHtml::link(Yii::app()->helper->formatRupiah($total_biaya_ird),Yii::app()->createUrl('trRawatInap/penunjang',array('id'=>$ri->id_rawat_inap)));

              ?></td>
      <td style="text-align: right"><?php 
        echo CHtml::link(Yii::app()->helper->formatRupiah($total_biaya_irna),Yii::app()->createUrl('trRawatInap/penunjang',array('id'=>$ri->id_rawat_inap)));
              ?></td>
      <td>
        <?php 
      echo  Yii::app()->helper->formatRupiah($total_biaya_ird + $total_biaya_irna);
        ?>
      </td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_ird);
      ?></strong>
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_irna);
      ?></strong>
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_biaya);
      ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>
<?php

  }
?>



                    </div>
                </div>
 