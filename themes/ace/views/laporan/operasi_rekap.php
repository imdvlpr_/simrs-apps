<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Data OK','url'=>Yii::app()->createUrl('tdOkBiaya/rekap')),
                            array('name' => 'Laporan Rekap OK','url'=>'#'),
                            
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<style type="text/css">
	#tabel th, #tabel td{
		text-align: center;
	}
</style>
<div class="row">
    <div class="col-xs-12">
        

                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'rekap-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    <legend>
      

    </legend>
  <div class="form-group">
  <?php echo CHtml::label('Antara Tanggal','',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
   <div class="col-sm-9">
        <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>
   </div>
 </div>
  <div class="form-group">
  <?php echo CHtml::label('Sampai dengan','',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
   <div class="col-sm-9">
        <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>
   </div>
 </div>

   <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="button" id="lihat">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>
           <span id='loading' style="display:none"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif"/></span>
          <button  style="display:none" class="btn btn-success" type="button" id="btn-export"><i class="fa fa-download"></i> Export Biaya</button>
            
          
        
        </div>
      </div>
   
  </fieldset>
  <?php $this->endWidget();?>



                    </div>
                </div>
<div class="row">
	<div class="col-xs-12">
		<table  id="tabel" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">UPF/Dokter</th>
					<th colspan="5">Jenis Operasi</th>
					<th rowspan="2">CITO</th>
					<th rowspan="2">ELEKTIF</th>
					<th colspan="4">Kelas</th>
					<th colspan="5">Anastesi</th>
					<th rowspan="2">BPJS</th>
					<th rowspan="2">UMUM</th>
				</tr>
				<tr>
					<th>Khusus</th>
					<th>Besar</th>
					<th>Sedang</th>
					<th>Kecil</th>
					<th>Total</th>
					<th>VIP</th>
					<th>I</th>
					<th>II</th>
					<th>III</th>
					<th>LA</th>
					<th>SAB</th>
					<th>GA</th>
					<th>EPI</th>
					<th>PER</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>    
<script type="text/javascript">
	$('#btn-export').click(function(e){
		$('#rekap-form').submit();
	});
	$('#lihat').click(function(e){
		e.preventDefault();

		$.ajax({
			url : '<?php echo Yii::app()->createUrl('tdOkBiaya/ajaxGetRekap');?>',
			type : 'POST',
			data : 'sd='+$('#TdOkBiaya_TANGGAL_AWAL').val()+'&ed='+$('#TdOkBiaya_TANGGAL_AKHIR').val(),
			beforeSend: function(){
				$('#loading').show();
				$('#btn-export').hide();
				$('#tabel > tbody').empty();
			},
			error : function(e){
				$('#loading').hide();
				console.log(e.responseText);
				$('#btn-export').hide();
			},
			success : function(data){
				var hasil = $.parseJSON(data);
				$('#loading').hide();
				$('#btn-export').show();

				var row = '';
				var listUpf = [];
				var index = 0;
				var total_all = hasil.total;
				var total_tindakan = 0;
				$.each(hasil.items,function(i,obj){
					if(!listUpf.includes(obj.nama)){
						listUpf.push(obj.nama);
						row += '<tr>';
						row += '	<td></td>';
						row += '	<td colspan="19" style="text-align:left">'+obj.nama+'</td>';
						row += '</tr>';
						index = 0;
					}

					var total = eval(obj.khusus + obj.besar+obj.sedang+obj.kecil);
					total_tindakan += total;
					index++;
					row += '<tr>';
					row += '	<td>'+index+'</td>';
					row += '	<td style="text-align:left">'+obj.nama_dokter+'</td>';
					row += '	<td>'+obj.khusus+'</td>';
					row += '	<td>'+obj.besar+'</td>';
					row += '	<td>'+obj.sedang+'</td>';
					row += '	<td>'+obj.kecil+'</td>';
					row += '	<td>'+total+'</td>';
					row += ' <td>'+obj.cito+'</td>';
					row += ' <td>'+obj.elektif+'</td>';
					row += '	<td>'+obj.vip+'</td>';
					row += '	<td>'+obj.kelas1+'</td>';
					row += '	<td>'+obj.kelas2+'</td>';
					row += '	<td>'+obj.kelas3+'</td>';
					row += '	<td>'+obj.la+'</td>';
					row += '	<td>'+obj.sab+'</td>';
					row += '	<td>'+obj.ga+'</td>';
					row += '	<td>'+obj.epi+'</td>';
					row += '	<td>'+obj.per+'</td>';
					row += ' <td>'+obj.bpjs+'</td>';
					row += ' <td>'+obj.umum+'</td>';
					row += '</tr>';
					
				});

				row += '<tr>';
				row += '	<td></td>';
				row += '	<td style="text-align:left">Total</td>';
				row += '	<td>'+total_all.khusus+'</td>';
				row += '	<td>'+total_all.besar+'</td>';
				row += '	<td>'+total_all.sedang+'</td>';
				row += '	<td>'+total_all.kecil+'</td>';
				row += '	<td>'+total_tindakan+'</td>';
				row += ' <td>'+total_all.cito+'</td>';
				row += ' <td>'+total_all.elektif+'</td>';
				row += '	<td>'+total_all.vip+'</td>';
				row += '	<td>'+total_all.kelas1+'</td>';
				row += '	<td>'+total_all.kelas2+'</td>';
				row += '	<td>'+total_all.kelas3+'</td>';
				row += '	<td>'+total_all.la+'</td>';
				row += '	<td>'+total_all.sab+'</td>';
				row += '	<td>'+total_all.ga+'</td>';
				row += '	<td>'+total_all.epi+'</td>';
				row += '	<td>'+total_all.per+'</td>';
				row += ' <td>'+total_all.bpjs+'</td>';
				row += ' <td>'+total_all.umum+'</td>';
				row += '</tr>';
				
				$('#tabel > tbody').append(row);


			}
		});
	});
</script>       