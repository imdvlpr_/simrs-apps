<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
array('name' => 'Laporan','url'=>Yii::app()->createUrl('trRawatInap/laporan')),
                            array('name' => 'Laporan Patologi Klinik','url'=>Yii::app()->createUrl('laporan/patologiKlinik')),
);
 
?>

<div class="row" >
    <div class="col-xs-12">
        
          
         
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
              
              ?>
            
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    
     
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>

        
       
  <?php $this->endWidget();?>

 </div>
      </div>
<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>
<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>Nama Patologi Klinik</th>
      <!-- <th>Jenis</th> -->
      <th>Tindakan</th>
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;
    foreach($obats as $obat)
    {


      
   
  ?>
    <tr>
       <td><?php echo $i++;?></td>
      <td><?php echo $obat->nama_obat_alkes;?></td>
      <!-- <td><?php echo $obat->kelas->nama_kelas;?></td> -->
      <td>
      
      <?php 
echo CHtml::link('<span class="label label-info" style="padding:5px 10px">Lihat Laporan Pasien </span>',array('laporan/patologiKlinikPasien','oid'=>$obat->id_obat_alkes,'jns'=>'1','t1'=>$tanggal_awal,'t2'=>$tanggal_akhir),array('style'=>'color:#fff','target'=>'_blank'));
      ?>
     
      </td>
     </tr>
    <?php 
      
    }

    foreach($penunjangs as $penunjang)
    {


      
   
  ?>
    <tr>
       <td><?php echo $i++;?></td>
      <td><?php echo $penunjang->nama_tindakan;?></td>
      <!-- <td><?php echo $penunjang->kelas->nama_kelas;?></td> -->
      <td>
      
      <?php 
echo CHtml::link('<span class="label label-info" style="padding:5px 10px">Lihat Laporan Pasien </span>',array('laporan/patologiKlinikPasien','oid'=>$penunjang->id_tindakan_penunjang,'jns'=>'2','t1'=>$tanggal_awal,'t2'=>$tanggal_akhir),array('style'=>'color:#fff','target'=>'_blank'));
      ?>
     
      </td>
     </tr>
    <?php 
      
    }
    ?>
  </tbody>
</table>
<?php
  }
?>



                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>