<table cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th>
      LAPORAN RR <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No Rekam Medis</th>
      <th  width="15%">Nama Pasien</th>
      <th width="7%">Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th width="5%">LM DI<br>RWT</th>
     <th>RR-Askep</th>
      <th>RR<br>Monitor</th>
      <th>JUMLAH</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $listrr = array();
    $i = 0;
    foreach($model->searchLaporan() as $ri)
    {

      $criteria = new CDbCriteria; 
      $criteria->addCondition("id_rawat_inap=".$ri->id_rawat_inap);
      $criteria->addCondition("top.kode_tindakan='RRA'");
      $criteria->with = array('top');
      $criteria->together = true;
      $rra = TrRawatInapTop::model()->findAll($criteria);

      $vrra = 0;
      foreach($rra as $t)
      {
          $vrra = $vrra + $t->biaya_irna;
      }

      $criteria = new CDbCriteria; 
      $criteria->addCondition("id_rawat_inap=".$ri->id_rawat_inap);
      $criteria->addCondition("top.kode_tindakan='RRM'");
      $criteria->with = array('top');
      $criteria->together = true;
      $rrm = TrRawatInapTop::model()->findAll($criteria);

      $vrrm = 0;
      foreach($rrm as $t)
      {
          $vrrm = $vrrm + $t->biaya_irna;
      }

      if($vrra != 0 && $vrrm != 0)
      {
        $listrr[$ri->id_rawat_inap] = array(
          'rra' => $vrra,
          'rrm' => $vrrm
        );
      $i++;
    }

      
    }

     $i=1;

    $total_rra = 0;
    $total_rrm = 0;
    
   



      foreach($listrr as $q => $t)
      {

        $ri = TrRawatInap::model()->findByPk($q);

       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td  width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
       <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td  width="5%"><?php echo $selisih_hari;?></td>
      <td style="text-align: right;"><?php 
        
        
          $total_rra = $total_rra + $t['rra'];
          echo Yii::app()->helper->formatRupiah($t['rra']);
              ?></td>
      <!-- <td>0</td> -->
      <td style="text-align: right;"><?php 
        $total_rrm = $total_rrm + $t['rrm'];
          echo Yii::app()->helper->formatRupiah($t['rrm']);
              ?></td>
     <td style="text-align: right;">
         <?php 
         echo Yii::app()->helper->formatRupiah($t['rrm'] + $t['rra']);
         ?>
       </td>
     </tr>

    <?php 
      
    }

    unset($listrr);
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
     
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
       <td>&nbsp;</td>
     <td><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_rra);
      ?></strong></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_rrm);
      ?></strong>
      </td>
      <td><strong>
        <?php 
 echo Yii::app()->helper->formatRupiah($total_rra + $total_rrm);
        ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>