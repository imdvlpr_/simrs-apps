<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Laporan','url'=>Yii::app()->createUrl('trRawatInap/laporan')),
                            array('name' => 'Laporan Kamar','url'=>Yii::app()->createUrl('laporan/kamar')),
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<div class="row" >
    <div class="col-xs-12">
        
                          <?php 

 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
             
<?php 
echo CHtml::link('<span class="label label-info" style="padding:5px 10px">Cetak Laporan Kamar </span>',array('laporan/kamarPasien','kid'=>$kamar->id_kamar,'t1'=>$tanggal_awal,'t2'=>$tanggal_akhir,'tipe'=>$tipe,'print'=>1),array('style'=>'color:#fff','target'=>'_blank'));
echo '&nbsp;';
echo CHtml::link('<span class="label label-success" style="padding:5px 10px">Ekspor ke XLS </span>',array('laporan/kamarPasien','kid'=>$kamar->id_kamar,'t1'=>$tanggal_awal,'t2'=>$tanggal_akhir,'tipe'=>$tipe,'print'=>2),array('style'=>'color:#fff','target'=>'_blank'));
?>
<?php

  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>
<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No Registrasi</th>
      <th>Nama Pasien</th>
      <th>Status Px</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
      <th>Lm<br>Dirawat</th>
      <th>Askep</th>
      <th>T. Jasa<br>Medis</th>
      <th>T. JP<br>Perawatan</th>
      <th>BHP</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $total_askep = 0;
    
    $total_biaya_jm = 0;
    $total_biaya_jp = 0;
    $total_bhp = 0;

    foreach($rawatInaps as $ri)
    {
      

       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      else
      {
        $dnow = date('Y-m-d');
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
      }

      $biaya_jm = 0;
      $biaya_jp = 0;

      foreach($ri->trRawatInapTnops as $vi)
      {

          if($vi->id_tnop == 3)
                $biaya_jm = $biaya_jm + $vi->biaya_irna * $vi->jumlah_tindakan;

          if($vi->id_tnop == 6)
                $biaya_jp = $biaya_jp + $vi->biaya_irna * $vi->jumlah_tindakan;
          
      }

      $bhp_top = $ri->sumBHPTop;
      
      $total_bhp += $bhp_top;
      $total_biaya_jm = $total_biaya_jm + $biaya_jm;
      $total_biaya_jp = $total_biaya_jp + $biaya_jp;
        
      $total_askep = $total_askep + $ri->kamar->biaya_askep * $selisih_hari;
  ?>
    <tr>
       <td><?php echo $i++;?></td>
       <td><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($ri->kamar->biaya_askep * $selisih_hari);
              ?></td>
      <td style="text-align: right;">
        <?php echo Yii::app()->helper->formatRupiah($biaya_jm);?>
          
        </td>
      <td style="text-align: right;"><?php echo Yii::app()->helper->formatRupiah($biaya_jp);?></td>
       <td style="text-align: right;"><?php echo Yii::app()->helper->formatRupiah($bhp_top);?></td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>SUBTOTAL</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      
      <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_askep);
      ?></strong>
      </td>
      <td style="text-align: right;"><strong><?php echo Yii::app()->helper->formatRupiah($total_biaya_jm);?></td>
      <td style="text-align: right;"><strong><?php echo Yii::app()->helper->formatRupiah($total_biaya_jp);?></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_bhp);
      ?></strong>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      
      <td>&nbsp;</td>
      <td>&nbsp;
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>

      <td><strong><?php echo Yii::app()->helper->formatRupiah($total_askep+$total_biaya_jm + $total_biaya_jp + $total_bhp);?>
      </strong>
      </td>
    </tr>
  </tbody>

</table>
<?php


  }
?>


                    </div>
              