<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
     array('name' => 'Rekapitulasi Jasa Dokter','url'=>Yii::app()->createUrl('laporan/rekapDokter')),
                           
);


?>
<div class="row" >
    <div class="col-xs-12">
        
          
         
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
              
              ?>
            
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    
     
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>

        
       
  <?php $this->endWidget();?>

 </div>
      </div>

<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>

      <div class="row" align="center">
<?php

$form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
      'target' => '_blank'
    ),  
));

echo $form->hiddenField($model,'TANGGAL_AWAL');
echo $form->hiddenField($model,'TANGGAL_AKHIR');
echo CHtml::hiddenField('jenisPasien',!empty($_POST['jenis']) ? $_POST['jenis'] : '-');
echo CHtml::dropDownList('print','',array(1=>'PDF',2=>'XLS'));
echo '&nbsp;';
echo CHtml::submitButton('Cetak Rekap Dokter',array('class'=>'btn btn-info btn-sm'));


$this->endWidget();

?>

</div>

<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>Nama Dokter</th>
      <th>VISITE</th>
      <th>KONSUL</th>
      <th>TINDAKAN</th>
      <th>JUMLAH</th>
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $total_tindakan = 0;
    $i = 1;

    $total_visite = 0;
    $total_konsul = 0;

    $jenis = !empty($_POST['jenis']) ? $_POST['jenis'] : '-';
    
    foreach(DmDokter::model()->findAll() as $item)
    {

      
      $tindakan = (object)$model->searchRekapDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      // print_r($tindakan);exit;
      $n_tindakan = !empty($tindakan) ? $tindakan->nilai : 0;
      
      $visite = $model->searchVisiteDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      $n_visite = !empty($visite) ? $visite->nilai : 0;

      $konsul = $model->searchKonsulDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      $n_konsul = !empty($konsul) ? $konsul->nilai : 0;

      $total_tindakan += $n_tindakan;
      $total_visite += $n_visite;
      $total_konsul += $n_konsul;
 
      if($n_tindakan == 0 && $n_visite == 0 && $n_konsul == 0) continue;
  ?>
    <tr>
      <td><?php echo $i++;?></td>
      <td><?php echo $item->nama_dokter;?></td>
      <td style="text-align: right;">  <?php 
        if($n_visite != 0){
          // laporan/dokterPasien&drid=8&t1=2017-10-01&t2=2017-10-26
          $url = array('laporan/dokterPasien','drid'=>$item->id_dokter,'t1'=>$model->TANGGAL_AWAL,'t2'=>$model->TANGGAL_AKHIR);
          echo CHtml::link(
                  
                  Yii::app()->helper->formatRupiah($n_visite),
                  $url,
                  array(
                    'target' => '_blank',
                  )
          );  

        }
              ?></td>
      <td style="text-align: right;"> <?php 
      if($n_konsul != 0)
      {
        $url = array('laporan/dokterPasien','drid'=>$item->id_dokter,'t1'=>$model->TANGGAL_AWAL,'t2'=>$model->TANGGAL_AKHIR);
          echo CHtml::link(
                  
                  Yii::app()->helper->formatRupiah($n_konsul),
                  $url,
                  array(
                    'target' => '_blank',
                  )
          );  
         // echo Yii::app()->helper->formatRupiah($n_konsul);
      }
              ?></td>
      <td style="text-align: right;">

        <?php 
        if($n_tindakan != 0){
          $url = array('laporan/dokterPasien','drid'=>$item->id_dokter,'t1'=>$model->TANGGAL_AWAL,'t2'=>$model->TANGGAL_AKHIR);
          echo CHtml::link(
                  
                  Yii::app()->helper->formatRupiah($n_tindakan),
                  $url,
                  array(
                    'target' => '_blank',
                  )
          );  

         
        }
              ?>
              </td>
     <!--  <td>0</td> -->
      <td style="text-align: right;">
                    <?php 
echo Yii::app()->helper->formatRupiah($n_tindakan + $n_visite + $n_konsul);
            ?>    


      </td>
      <!-- <td></td> -->
     </tr>
    <?php 
      
    
  }

  $total = $total_tindakan + $total_konsul + $total_visite;
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      
      <td style="text-align: right;"><?php 
       echo Yii::app()->helper->formatRupiah($total_visite);
      ?></td>
      <td style="text-align: right;"><?php 
       echo Yii::app()->helper->formatRupiah($total_konsul);
      ?></td>
       <td style="text-align: right;"><?php 
       echo Yii::app()->helper->formatRupiah($total_tindakan);
      ?></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total);
      ?>
        </strong>
      </td>
       <!-- <td>&nbsp;</td> -->
    </tr>
  </tbody>

</table>
<?php

  }
?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>