<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Laporan RR','url'=>Yii::app()->createUrl('laporan/rr')),
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>
<div class="row" >
    <div class="col-xs-12">
        
          
         
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
              
              ?>
            
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    
     
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>

        
       
  <?php $this->endWidget();?>

 </div>
      </div>

<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>

<div class="control-group" align="center">
  
<?php

$form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
      'target' => '_blank'
    ),  
));

echo $form->hiddenField($model,'TANGGAL_AWAL');
echo $form->hiddenField($model,'TANGGAL_AKHIR');
echo CHtml::dropDownList('print','',array(2=>'XLS'));
echo '&nbsp;';
echo CHtml::submitButton('Cetak Laporan RR',array('class'=>'btn btn-info'));

$this->endWidget();

?>

</div>
<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No<br>Reg</th>
      <th>Nama Pasien</th>
    <th>Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th>LM DI<br>RWT</th>
       <th>Dr. AN</th>
      <th>JS Dr. AN</th>
       <th>JS Prwt AN</th>
       <th>RR-Askep</th>
      <th>RR<br>Monitor</th>
      <th>RR<br>Non-OP</th>
      <th>JUMLAH</th>
    </tr>
  </thead>
  <tbody>
  <?php 

$listTnop = TindakanMedisNonOperatif::model()->findByAttributes(['kode_tindakan'=>'rr']);


    $i = 1;
    $total_ja = 0;
     $total_pan = 0;

    $total_rra = 0;
    $total_rrm = 0;
    
    $subtotal_rr = 0; 

    foreach($model->searchMinimized() as $ri)
    {

  
       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      $nilai_ja = $ri->sumTopJa;  
      $nilai_pan = $ri->sumPrwtAN;
      // echo $ri->sumTopJa.'<br>';
      $total_ja += $nilai_ja;
      $total_pan += $nilai_pan;

      $sumRRA = $ri->sumTindRRA;
      $sumRRM = $ri->sumTindRRM;
      $total_rrm += $sumRRM;
      $total_rra += $sumRRA;

      $drJA = '';

      if($sumRRA == 0 && $sumRRM == 0 && $nilai_pan == 0 && $nilai_ja == 0) 
        continue;

      if(!empty($ri->trRawatInapTopJa[0])){
         $ja = $ri->trRawatInapTopJa[0];
        $drJA = !empty($ja->dokter) ? $ja->dokter->FULLNAME : '';
      }


        $attr = array(
          'id_rawat_inap' => $ri->id_rawat_inap,
          'id_tnop' => $listTnop->id_tindakan
        );
        $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);

        $biaya_rr = !empty($itemTnop) ? $itemTnop->biaya_irna : 0;
        $subtotal_rr += $biaya_rr;  

          
        
        
  ?>
    <tr>
       <td><?php echo $i++;?></td>
       <td><?php echo CHtml::link($ri->pASIEN->NoMedrec,Yii::app()->createUrl('trRawatInap/dataPasien',array('id'=>$ri->id_rawat_inap)));?></td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      <td style="text-align: right;">
        <?php
        
          echo $drJA;
      ?>
                
              </td>
     <!--  <td>0</td> -->
      <td style="text-align: right;"><?php
      
         echo CHtml::link(Yii::app()->helper->formatRupiah($nilai_ja),Yii::app()->createUrl('trRawatInap/medikTOP',array('id'=>$ri->id_rawat_inap)));
              ?></td>
       <td style="text-align: right;"><?php 
      
         echo CHtml::link(Yii::app()->helper->formatRupiah($nilai_pan),Yii::app()->createUrl('trRawatInap/medikTOP',array('id'=>$ri->id_rawat_inap)));
              ?></td>
      <td style="text-align: right;"><?php 
        
          
          echo Yii::app()->helper->formatRupiah($sumRRA);
         
              ?>
                

              </td>
        
     <!--  <td>0</td> -->
      <td style="text-align: right;"><?php 
          echo Yii::app()->helper->formatRupiah($sumRRM);
      
              ?>
                
              </td>
      <td style="text-align: right;"><?= Yii::app()->helper->formatRupiah($biaya_rr);?></td>
          
       <td style="text-align: right;">
         <?php 
         echo CHtml::link(Yii::app()->helper->formatRupiah($nilai_ja + $nilai_pan + $sumRRA + $sumRRM+ $biaya_rr),Yii::app()->createUrl('trRawatInap/medikTOP',array('id'=>$ri->id_rawat_inap)));
         ?>
       </td>
     </tr>
    <?php 
      
    
  }


    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_ja);
      ?></strong></td>
     <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_pan);
      ?></strong></td>
       <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_rra);
      ?></strong></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_rrm);
      ?></strong>
      </td>
      <td style="text-align: right;"><strong><?= Yii::app()->helper->formatRupiah($subtotal_rr);?></strong></td>
      <td style="text-align: right;"><strong>
        <?php 
 echo Yii::app()->helper->formatRupiah($total_rra + $total_rrm + $total_pan + $total_ja + $subtotal_rr);
        ?></strong>
      </td>
    </tr>
  </tbody>

</table>
<?php

  }
?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>