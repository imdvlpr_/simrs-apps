<?php 

header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_jasa_dokter_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="14">
      LAPORAN JASA DOKTER <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" >
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No <br>RM</th>
      <th  width="15%">Nama Pasien</th>
      <th width="7%">Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th width="5%">LM DI<br>RWT</th>
     <th>Dr. OP</th>
      <th>JS Dr. OP</th>
      <th>Dr. Anas</th>
      <th>JS Dr. Anas</th>
      <th>JUMLAH</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $listjm = array();
    
    foreach($model->searchLaporan() as $ri)
    {

      
      $valjm = 0;
      $dokterjm = array();
    $dokterja = array();

      foreach($ri->trRawatInapTopJm as $t)
      {
          $valjm = $valjm + $t->nilai;
          if(!empty($t->dokter))
            $dokterjm[] = $t->dokter->NICKNAME;
      }

      $valja = 0;
      foreach($ri->trRawatInapTopJa as $t)
      {
          $valja = $valja + $t->nilai;
          if(!empty($t->dokter))
            $dokterja[] = $t->dokter->NICKNAME;
      }

      if($valja != 0 && $valjm != 0)
      {
        $listjm[$ri->id_rawat_inap] = array(
          'ja' => array(
            'value' => $valja,
            'dokter' => $dokterja
          ),
          'jm' => array(
            'value' => $valjm,
            'dokter' => $dokterjm
          ),
        );
   
     }

      
    }

    $i=1;

    $total_ja = 0;
    $total_jm = 0;
    
   
      foreach($listjm as $q => $t)
      {

        $ri = TrRawatInap::model()->findByPk($q);

       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td  width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
       <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td  width="5%"><?php echo $selisih_hari;?></td>
      <td style="text-align: right;"><?php 
        
        
          $total_jm = $total_jm + $t['jm']['value'];
          echo ($t['jm']['value']);
              ?></td>
      <td>
          <?php 

          $count = count($t['jm']['dokter']);
          foreach($t['jm']['dokter'] as $d){
            if($count == 1)
              echo $d;
            else
              echo $d.', ';
          }
          
          ?>
            
          </td> 
      <!-- <td>0</td> -->
      <td style="text-align: right;"><?php 
        $total_ja = $total_ja + $t['ja']['value'];
          echo ($t['ja']['value']);
              ?></td>
       <td>
        <?php 
         $count = count($t['ja']['dokter']);
          foreach($t['ja']['dokter'] as $d){
            if($count == 1)
              echo $d;
            else
              echo $d.', ';
          }
        ?>
          
        </td>
     <td style="text-align: right;">
         <?php 
        echo ($t['jm']['value'] + $t['ja']['value']);
         ?>
       </td>
     </tr>

    <?php 
      
    }

    unset($listrr);
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>

      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
      
     <td><strong>
        <?php 
       echo ($total_ja);
      ?></strong></td>
       <td>&nbsp;</td>

      <td style="text-align: right;"><strong>
        <?php 
       echo ($total_jm);
      ?></strong>
      </td>
       <td>&nbsp;</td>
      <td><strong>
        <?php 
 echo ($total_ja + $total_jm);
        ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>