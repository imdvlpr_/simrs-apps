<table cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th>
      LAPORAN GIZI <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No Rekam Medis</th>
      <th  width="15%">Nama Pasien</th>
      <th width="7%">Status Px</th>
      <th>Klas</th>
      <th>Instalasi</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th  width="5%">Lm<br>Dirawat</th>
      <th>Asuhan Nutrisi</th>
      <!-- <th>Konsul Gizi</th> -->
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $total_asnut = 0;
    
    foreach($model->searchLaporan() as $ri)
    {
      
       $asnut = !empty($ri->trRawatInapRincians) ? $ri->trRawatInapRincians->asuhan_nutrisi : 0;

      if($asnut == 0) continue;

       $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }
   
        $total_asnut = $total_asnut + $asnut;
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td  width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      Gizi
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td  width="5%"><?php echo $selisih_hari;?></td>
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($asnut);
              ?></td>
      <!-- <td>0</td> -->
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($asnut);
              ?></td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_asnut);
      ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>