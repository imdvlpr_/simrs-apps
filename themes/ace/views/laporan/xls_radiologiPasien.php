
<?php 
header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));
$filename = 'laporan_radiologi_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="11">
      LAPORAN RADIOLOGI <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table cellpadding="4" border="1" >
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No Registrasi</th>
      <th width="15%">Nama Pasien</th>
      <th width="7%">Status Px</th>
      <th>Kamar</th>
      <th>Klas</th>
      <th>Instalasi</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
      <th>Lama dirawat</th>
      <th>Nama Tindakan</th>
        <th>IRD</th>
      <th>IRNA</th>
     <th>Total</th> 
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;



   
   $all_total_biaya = 0;
    $all_total_ird = 0;
    $all_total_irna = 0;
    
    foreach($rawatInaps as $ri)
    {
      if(empty($ri)) continue;
     $total_biaya_irna = 0;
      $total_biaya_ird = 0;
      foreach($ri->trRawatInapPenunjangs as $vd)
      {

          
          if(($vd->penunjang->jenis_penunjang == 0) && ($vd->id_penunjang == $radio->id_tindakan_penunjang))
          {
             $total_biaya_irna = $total_biaya_irna + $vd->biaya_irna;
             $total_biaya_ird = $total_biaya_ird + $vd->biaya_ird;
          }   
        
      }

     $all_total_irna = $all_total_irna + $total_biaya_irna;
      $all_total_ird = $all_total_ird + $total_biaya_ird;

      $all_total_biaya = $all_total_biaya + $total_biaya_irna + $total_biaya_ird;
      if($all_total_biaya == 0) continue;
        $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }

  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php 
       echo $ri->pASIEN->NoMedrec;
       ?></td>
      <td width="15%"><?php 
      echo $ri->pASIEN->NAMA;
      ?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
       <td>
      <?php 
      echo $ri->kamar->nama_kamar;
      ?>
      </td>
      <td>
      <?php 
      echo $ri->kamar->kelas->nama_kelas;
      ?>
      </td>
      <td>Radiologi</td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      <td><?php 
      echo  $radio->nama_tindakan;
       ?></td>
      
       <td style="text-align: right"><?php 
        echo ($total_biaya_ird);
              ?></td>
       <td style="text-align: right"><?php 
        echo ($total_biaya_irna);
              ?></td>
       <td style="text-align: right"><?php 
        echo ($total_biaya_ird + $total_biaya_irna);
              ?></td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
     <td>&nbsp;</td>
        <td style="text-align: right"><strong>
        <?php 
       echo ($all_total_ird);
      ?></strong>
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo ($all_total_irna);
      ?></strong>
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo ($all_total_biaya);
      ?></strong>
      </td>
    </tr>
  </tbody>

</table>