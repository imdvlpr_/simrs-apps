<table cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th>
      LAPORAN DOKTER <?php echo $dokter->FULLNAME;?><br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No Rekam Medik</th>
      <th width="15%">Nama Pasien</th>
      <th>Status Px</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>TMR</th>
      <th>TKR</th>
      <th width="5%">Lm<br>Dirawat</th>
      <th>Nama Dokter</th>
      <th>Visite</th>
      <th>Konsul</th>
      <th>Tindakan</th>
      <th>Jumlah</th>
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $all_total_konsul = 0;
    $all_total_visite = 0;
    $all_total_tindakan = 0;
    foreach($rawatInaps as $ri)
    {

        $biaya_visite_ird = 0;
        $biaya_visite_irna = 0;

        $biaya_konsul_ird = 0;
        $biaya_konsul_irna = 0;

        $total_konsul = 0;
        $total_visite = 0;
        $total_tindakan = 0;
        foreach($ri->tRIVisiteDokters as $vi)
        {

            if(!empty($vi->id_dokter) && $vi->id_dokter == $dokter->id_dokter)
            {
                 if($vi->jenisVisite->kode_visite == 'VISITE')
                 {
                    // $biaya_visite_ird = $biaya_visite_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_visite_irna = $biaya_visite_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                 }

                 else if($vi->jenisVisite->kode_visite == 'KONSUL')
                 {
                    // $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_konsul_irna = $biaya_konsul_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                 }  
            }

            if(!empty($vi->id_dokter_irna) && $vi->id_dokter_irna == $dokter->id_dokter)
            {
                 if($vi->jenisVisite->kode_visite == 'VISITE')
                 {
                    // $biaya_visite_ird = $biaya_visite_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_visite_irna = $biaya_visite_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                 }

                 else if($vi->jenisVisite->kode_visite == 'KONSUL')
                 {
                    // $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_konsul_irna = $biaya_konsul_irna + $vi->biaya_visite_irna ;//* $vi->jumlah_visite;
                 }  
            }
        }

        $total_konsul = $biaya_konsul_ird + $biaya_konsul_irna;
        $total_visite = $biaya_visite_ird + $biaya_visite_irna;

        $all_total_visite = $all_total_visite + $total_visite;
        $all_total_konsul = $all_total_konsul + $total_konsul;


   
        $biaya_tindakan_anas = 0;
        $biaya_tindakan_jm = 0;
        foreach($ri->trRawatInapTops as $vi)
        {

            if(!empty($vi->id_dokter_jm) && $vi->id_dokter_jm == $dokter->id_dokter)
            {
                $biaya_tindakan_jm = $biaya_tindakan_jm + $vi->biaya_irna * $vi->jumlah_tindakan;
            }

            if(!empty($vi->id_dokter_anas) && $vi->id_dokter_anas == $dokter->id_dokter)
            {
                $biaya_tindakan_anas = $biaya_tindakan_anas + $vi->biaya_irna * $vi->jumlah_tindakan;
            }
        }



        $total_tindakan = $biaya_tindakan_jm + $biaya_tindakan_anas;
        

        $biaya_top_jm = 0;
        foreach($ri->trRawatInapTopJm as $vi)
        {
          if(!empty($vi->id_dokter) && $vi->id_dokter == $dokter->id_dokter)
          { 
              $biaya_top_jm = $biaya_top_jm + $vi->nilai;
          } 
        }

        $total_tindakan = $total_tindakan + $biaya_top_jm;

        $biaya_top_ja = 0;
        foreach($ri->trRawatInapTopJa as $vi)
        {
          if(!empty($vi->id_dokter) && $vi->id_dokter == $dokter->id_dokter)
          { 
              $biaya_top_ja = $biaya_top_ja + $vi->nilai;
          } 
        }

        $total_tindakan = $total_tindakan + $biaya_top_ja;


        if($total_visite == 0 && $total_konsul == 0 && $total_tindakan == 0) continue;
        
        $all_total_tindakan = $all_total_tindakan + $total_tindakan;

        $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }
   
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td width="5%"><?php echo $selisih_hari;?></td>
      <td><?php echo $dokter->FULLNAME;?></td>
      <td style="text-align: right;" >
      <?php 
        echo Yii::app()->helper->formatRupiah($total_visite);
              ?></td>
      <td style="text-align: right;" >
         <?php 
        echo Yii::app()->helper->formatRupiah($total_konsul);
              ?>
      </td>
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($total_tindakan);
              ?>
                
              </td>
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($total_tindakan + $total_konsul + $total_visite);
              ?></td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td style="text-align: right;">
      <strong>
      <?php 
       echo Yii::app()->helper->formatRupiah($all_total_visite);
      ?></strong>
      </td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_konsul);
      ?></strong>
      </td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_tindakan);
      ?></strong>
      </td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_visite + $all_total_konsul + $all_total_tindakan);
      ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>