<?php 
header($_SERVER['SERVER_PROTOCOL'].' 200 OK');

$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_ird_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
header('Content-type: application/excel');

function sum()
{
    return array_sum(func_get_args());
}

$list_penunjangs = TindakanMedisPenunjang::model()->findAll();
$list_obats = ObatAlkes::model()->findAll();
$list_lains = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>6));
$listTnop = TindakanMedisNonOperatif::model()->findAll();
?>
<table border="1">
  
  <thead>
    <tr>
      <th>No</th>
      <th>No. Reg</th>
      <th>Nama Pasien</th>
      <th>Status<br>Px</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
      <th>Lm<br>Drwt</th>
      <th>DPJP</th>
      <th>Obs</th>
      <th>Dr<br>Pengawas</th>
      <th>Biaya<br>Pengawas</th>
      <th>Visite<br>Dr Ahli</th>
      <th>Biaya Visite<br>Dr Ahli</th>
      <th>Visite<br>Dr Umum</th>
      <th>Biaya Visite<br>Dr Umum</th>
      <th>JRS</th>
      <th>JM</th>
      <th>Japel</th>
      <?php

      foreach($list_penunjangs as $mp)
      {
        echo '<th>'.$mp->nama_tindakan.'</th>';
      }
      

      foreach($list_obats as $mp)
      {
        echo '<th>'.$mp->kode_alkes.'</th>';
      }


      foreach($list_lains as $mp)
      {
        echo '<th>'.$mp->nama_tindakan.'</th>';
      }
      ?>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
<?php
    $i=1;

    $total = 0;
    $vtotal_pengawas = 0;
    $vtotal_penunjangs = [];
    $vtotal_obats = [];
    $vtotal_lains = [];
    $vtotal_visite_ahli =0;
    $vtotal_visite_umum = 0;
    $vtotal_tnop_jrm = 0;
    $vtotal_tnop_jm = 0;
    $vtotal_tnop_japel = 0;
    $vtotal_total = 0; 
    $vtotal_obs = 0;
    foreach($rawatInaps as $rawatInap)
    {
      $rawatRincian = $rawatInap->trRawatInapRincians;
       $selisih_hari = 1;

      if(!empty($rawatInap->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
      }

      else
      {
        $dnow = date('Y-m-d');
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
      }

      $nama_dokter = '';
      $nama_dokter_ird = '';


      if(!empty($rawatInap->dokter)){
        $nama_dokter = $rawatInap->dokter->NICKNAME;
      } 


      if(!empty($rawatRincian->dokterIrd))
      {
        $nama_dokter_ird = $rawatRincian->dokterIrd->NICKNAME;
      }

      $subtotal_biaya_visite_ahli = 0;
      $subtotal_biaya_visite_umum = 0;
      $nama_dokter_visite_ahli = '';
      $nama_dokter_visite_umum = '';


      $subtotal = 0;
      foreach($rawatInap->getDataVisite('VISITE') as $visite)
      {

        if(empty($visite->jenisVisite)) 
          continue;

        if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Ahli'))
        {
           $subtotal_biaya_visite_ahli += $visite->biaya_visite;
           $nama_dokter_visite_ahli .= !empty($visite->dokterIrd) ? $visite->dokterIrd->NICKNAME.' ' : '';
        }

        if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Umum'))
        {
           $subtotal_biaya_visite_umum += $visite->biaya_visite;
           $nama_dokter_visite_umum .= !empty($visite->dokterIrd) ? $visite->dokterIrd->NICKNAME.' ' : '';
        }
      }

      $subtotal = $subtotal_biaya_visite_umum + $subtotal_biaya_visite_ahli;  

      $vtotal_visite_ahli += $subtotal_biaya_visite_ahli;
      $vtotal_visite_umum += $subtotal_biaya_visite_umum;

       
      $subtotal_jrm = 0;
      $subtotal_jm = 0;
      $subtotal_japel = 0;
       foreach($listTnop as $tnop)
       {
          // $attr = array(
          //   'id_rawat_inap' => $rawatInap->id_rawat_inap,
          //   'id_tnop' => $tnop->id_tindakan
          // );

           $db = Yii::app()->db->createCommand();
               $db->select('biaya_ird');
               $db->from('tr_rawat_inap_tnop t');
               // $db->join("tindakan_medis_non_operatif tn","tn.id_tindakan = t.id_tnop");
               $db->where("t.id_rawat_inap = ".$rawatInap->id_rawat_inap);
               $db->andWhere("t.id_tnop = ".$tnop->id_tindakan);
               
             $itemTnop =  $db->queryRow();

             // print_r($itemTnop);exit;
          // $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);

          if(!empty($itemTnop)){
            $itemTnop = (object)$itemTnop;
            if($tnop->kode_tindakan == 'jrm'){
              $subtotal_jrm += $itemTnop->biaya_ird;  
            }

            else if($tnop->kode_tindakan == 'jm'){
              $subtotal_jm +=  $itemTnop->biaya_ird;  
            }

            else if($tnop->kode_tindakan == 'japel'){
              $subtotal_japel += $itemTnop->biaya_ird;  
            }
          }


          
       }   

      $subtotal = $subtotal + $subtotal_jrm + $subtotal_jm + $subtotal_japel;
      $vtotal_tnop_jrm += $subtotal_jrm;
      $vtotal_tnop_jm += $subtotal_jm;
      $vtotal_tnop_japel += $subtotal_japel;

      $vtotal_obs += !empty($rawatRincian) ? $rawatRincian->obs_ird : 0;
      $vtotal_pengawas += !empty($rawatRincian) ? $rawatRincian->biaya_pengawasan : 0;
  ?>
    <tr>
       <td><?php echo $i++;?></td>
       <td>&nbsp;<?php echo $rawatInap->pASIEN->NoMedrec;?></td>
      <td><?php echo $rawatInap->pASIEN->NAMA;?></td>
      <td><?php echo $rawatInap->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $rawatInap->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $rawatInap->kamar->nama_kamar;?>
      </td>
      <td><?php echo $rawatInap->tanggal_masuk;?></td>
      <td><?php echo $rawatInap->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      <td><?=$nama_dokter;?></td>
      <td style="text-align: right"><?=(!empty($rawatRincian) ? $rawatRincian->obs_ird : 0);?></td>
      <td><?=$nama_dokter_ird;?></td>
      <td style="text-align: right"><?=(!empty($rawatRincian) ? $rawatRincian->biaya_pengawasan : 0);?></td>
       <td><?=$nama_dokter_visite_ahli;?></td>
      <td style="text-align: right"><?=($subtotal_biaya_visite_ahli);?></td>
       <td><?=$nama_dokter_visite_umum;?></td>
      <td style="text-align: right"><?=($subtotal_biaya_visite_umum);?></td>
      <td style="text-align: right"><?=($subtotal_jrm);?></td>
      <td style="text-align: right"><?=($subtotal_jm);?></td>
      <td style="text-align: right"><?=($subtotal_japel);?></td>
      <?php

      $subtotal = $subtotal + (!empty($rawatRincian) ? $rawatRincian->obs_ird : 0) + (!empty($rawatRincian) ? $rawatRincian->biaya_pengawasan : 0);


      $value_tmp = [];
      $subvtotal_penunjangs = [] ;
      foreach($list_penunjangs as $mp)
      {

        // $attr = array(
        //   'id_rawat_inap' => $rawatInap->id_rawat_inap,
        //   'id_penunjang' => $mp->id_tindakan_penunjang
        // );

        $db = Yii::app()->db->createCommand();
             $db->select('biaya_ird');
             $db->from('tr_rawat_inap_penunjang t');
             // $db->join("tindakan_medis_non_operatif tn","tn.id_tindakan = t.id_tnop");
             $db->where("t.id_rawat_inap = ".$rawatInap->id_rawat_inap);
             $db->andWhere("t.id_penunjang = ".$mp->id_tindakan_penunjang);
             
        $supps =  $db->queryAll();
        // $supps = TrRawatInapPenunjang::model()->findAllByAttributes($attr);
        $value_tmp[$mp->id_tindakan_penunjang] = 0;
        $biaya_ird_penunjangs_total = 0;
        foreach($supps as $supp)
        {

          $supp = (object)$supp;

          if($supp->biaya_ird == 0) continue;
          $value_tmp[$mp->id_tindakan_penunjang] = $supp->biaya_ird;

          $biaya_ird_penunjangs_total+= $supp->biaya_ird;

        }

        echo '<td style="text-align: right">'.($value_tmp[$mp->id_tindakan_penunjang]).'</td>';
         $subtotal = $subtotal + $value_tmp[$mp->id_tindakan_penunjang];
         $subvtotal_penunjangs[$mp->id_tindakan_penunjang] = $biaya_ird_penunjangs_total;
      }

      $vtotal_penunjangs[] = $subvtotal_penunjangs;
      ?>
       <?php

      $value_tmp = [];
      $subvtotal_obats = [] ;
      foreach($list_obats as $mp)
      {

        // $attr = array(
        //   'id_rawat_inap' => $rawatInap->id_rawat_inap,
        //   'id_alkes' => $mp->id_obat_alkes
        // );
         $db = Yii::app()->db->createCommand();
             $db->select('biaya_ird');
             $db->from('tr_rawat_inap_alkes t');
             // $db->join("tindakan_medis_non_operatif tn","tn.id_tindakan = t.id_tnop");
             $db->where("t.id_rawat_inap = ".$rawatInap->id_rawat_inap);
             $db->andWhere("t.id_alkes = ".$mp->id_obat_alkes);
             
        $supps =  $db->queryAll();
        // $supps = TrRawatInapAlkes::model()->findAllByAttributes($attr);
        $value_tmp[$mp->id_obat_alkes] = 0;
        $biaya_ird_obats_total = 0;
        foreach($supps as $supp)
        {
           $supp = (object)$supp;

          if($supp->biaya_ird == 0) continue;
          $value_tmp[$mp->id_obat_alkes] = $supp->biaya_ird;

          $biaya_ird_obats_total += $supp->biaya_ird;
        }
        $subtotal = $subtotal + $value_tmp[$mp->id_obat_alkes];
        echo '<td style="text-align: right">'.($value_tmp[$mp->id_obat_alkes]).'</td>';
        $subvtotal_obats[$mp->id_obat_alkes] = $biaya_ird_obats_total;
      }

      $vtotal_obats[] =$subvtotal_obats;

       $db = Yii::app()->db->createCommand();
           $db->select('id_tindakan');
           $db->from('tindakan_medis_lain t');
           // $db->join("tindakan_medis_non_operatif tn","tn.id_tindakan = t.id_tnop");
           $db->where("t.kelas_id = ".$rawatInap->kamar->kelas_id);
           // $db->andWhere("t.id_alkes = ".$mp->id_obat_alkes);
           
      $tmp =  $db->queryAll();
      // $tmp = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>$rawatInap->kamar->kelas_id));
      $value_tmp = [];
       $subvtotal_lains = [] ;
      foreach($tmp as $mp)
      {
        $mp = (object) $mp;
        // $attr = array(
        //   'id_rawat_inap' => $rawatInap->id_rawat_inap,
        //   'id_lain' => $mp->id_tindakan
        // );
            $db = Yii::app()->db->createCommand();
             $db->select('biaya_ird');
             $db->from('tr_rawat_inap_lain t');
             // $db->join("tindakan_medis_non_operatif tn","tn.id_tindakan = t.id_tnop");
             $db->where("t.id_rawat_inap = ".$rawatInap->id_rawat_inap);
             $db->andWhere("t.id_lain = ".$mp->id_tindakan);
             
        $supps =  $db->queryAll();
        // $supps = TrRawatInapLain::model()->findAllByAttributes($attr);
        $value_tmp[$mp->id_tindakan] = 0;
         $biaya_ird_lains_total = 0;
        foreach($supps as $supp)
        {
          $supp = (object) $supp;
          if($supp->biaya_ird == 0) continue;
          $value_tmp[$mp->id_tindakan] = $supp->biaya_ird;

          $biaya_ird_lains_total += $supp->biaya_ird;
        }

        $subvtotal_lains[$mp->id_tindakan] = $biaya_ird_lains_total;
        $subtotal = $subtotal + $value_tmp[$mp->id_tindakan];
        echo '<td style="text-align: right">'.($value_tmp[$mp->id_tindakan]).'</td>';
        
      }
      $vtotal_lains[] = $subvtotal_lains;
      ?>
      <td><?=($subtotal);?></td>
     </tr>
    <?php 
      
    $vtotal_total += $subtotal;
      
}
?>
 <tr>
      
      <th colspan="10" style="text-align: right">TOTAL</th>
      <th style="text-align: right"><?=($vtotal_obs);?></th>
      <th></th>
     <th style="text-align: right"><?=($vtotal_pengawas);?></th>
      <th></th>
      <th style="text-align: right"><?=($vtotal_visite_ahli);?></th>
      <th></th>
      <th style="text-align: right"><?=($vtotal_visite_umum);?></th>
      <th style="text-align: right"><?=($vtotal_tnop_jrm);?></th>
      <th style="text-align: right"><?=($vtotal_tnop_jm);?></th>
      <th style="text-align: right"><?=($vtotal_tnop_japel);?></th>
      <?php
       $sumResult = call_user_func_array('array_map', array_merge(['sum'], $vtotal_penunjangs));
      
     $j=0;
      foreach($list_penunjangs as $mp)
      {
        echo '<th style="text-align: right">'.($sumResult[$j]).'</th>';
        $j++;
      }
      

      $sumResult = call_user_func_array('array_map', array_merge(['sum'], $vtotal_obats));
       $j=0;
      foreach($list_obats as $mp)
      {
        echo '<th style="text-align: right">'.($sumResult[$j]).'</th>';
        $j++;
      }


      $sumResult = call_user_func_array('array_map', array_merge(['sum'], $vtotal_lains));
       $j=0;
      foreach($list_lains as $mp)
      {
        echo '<th style="text-align: right">'.($sumResult[$j]).'</th>';
        $j++;
      }

      ?>
     <th style="text-align: right"><?=($vtotal_total);?></th>
    </tr>
  </tbody>

</table>
<?php 
exit();
?>