<?php 
header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_dokter_per_kamar_pasien_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="14">
      LAPORAN DOKTER <?php echo $dokter->FULLNAME;?><br>
      KAMAR <?=$model->kamar->nama_kamar;?>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No Rekam Medik</th>
      <th>Nama Pasien</th>
      <th>Status Px</th>
      <th>Klas</th>
     
      <th>TMR</th>
      <th>TKR</th>
      <th>Lama Dirawat</th>
     
      <th>Visite</th>
      <th>Konsul</th>
      
      <!-- <th>Jumlah</th> -->
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $all_total_konsul = 0;
    $all_total_visite = 0;
    $all_total_tindakan = 0;
    foreach($rawatInaps as $ri)
    {

        $biaya_visite_ird = 0;
        $biaya_visite_irna = 0;

        $biaya_konsul_ird = 0;
        $biaya_konsul_irna = 0;

        $total_konsul = 0;
        $total_visite = 0;
        $total_tindakan = 0;
        foreach($ri->tRIVisiteDokters as $vi)
        {

            if(!empty($vi->id_dokter) && $vi->id_dokter == $dokter->id_dokter)
            {
                 if($vi->jenisVisite->kode_visite == 'VISITE')
                 {
                    // $total_visite += $vi->jumlah_visite;
                    // $biaya_visite_ird = $biaya_visite_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_visite_irna = $biaya_visite_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                 }

                 else if($vi->jenisVisite->kode_visite == 'KONSUL')
                 {
                    // $total_konsul += $vi->jumlah_visite;
                    // $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_konsul_irna = $biaya_konsul_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                 }  
            }

            if(!empty($vi->id_dokter_irna) && $vi->id_dokter_irna == $dokter->id_dokter)
            {
                 if($vi->jenisVisite->kode_visite == 'VISITE')
                 {
                    $total_visite += $vi->jumlah_visite;
                    // $biaya_visite_ird = $biaya_visite_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_visite_irna = $biaya_visite_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                 }

                 else if($vi->jenisVisite->kode_visite == 'KONSUL')
                 {
                    $total_konsul += $vi->jumlah_visite;
                    // $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_konsul_irna = $biaya_konsul_irna + $vi->biaya_visite_irna ;//* $vi->jumlah_visite;
                 }  
            }
        }

        // $total_konsul = $biaya_konsul_ird + $biaya_konsul_irna;
        // $total_visite = $biaya_visite_ird + $biaya_visite_irna;

        $all_total_visite = $all_total_visite + $total_visite;
        $all_total_konsul = $all_total_konsul + $total_konsul;


        // if($total_visite == 0 && $total_konsul == 0) continue;

        $all_total_tindakan = $all_total_tindakan + $total_tindakan;

        $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }
   
  ?>
    <tr>
       <td><?php echo $i++;?></td>
       <td>
        <?php 
        echo CHtml::link($ri->pASIEN->NoMedrec,array('trRawatInap/dataPasien','id'=>$ri->id_rawat_inap));
        ?>
          
        </td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td>
      <?php echo $selisih_hari;?>
      </td>
      <td>
      <?php 
        echo $total_visite
              ?></td>
      <td>
         <?php 
         echo $total_konsul;
        // echo Yii::app()->helper->formatRupiah($total_konsul);
              ?>
      </td>
      
     
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>
      <strong>
      <?php 
       echo $all_total_visite;
      ?></strong>
      </td>
      <td><strong>
        <?php 
       echo $all_total_konsul;
      ?></strong>
      </td>
    
    </tr>
  </tbody>

</table>

