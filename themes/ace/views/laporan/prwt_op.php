<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  'Forms',
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="container-fluid" >
    <div class="row-fluid">
        
        
        <!--/span-->
        <div class="span12" >
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            
                            array('name' => 'Laporan Perawat Operasi','url'=>Yii::app()->createUrl('laporan/prwtOp')),
                            
                          ),
                          'delimiter' => ' || ', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Laporan Perawat Operasi</div>
                      <div class="pull-right"> </div> 
                    </div>
                    <div class="block-content collapse in">
                          <?php 

 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    <legend>
      

    </legend>
    
  <div class="control-group">
    <?php echo CHtml::label('Antara Tanggal','',array('class'=>'control-label'));?>
      <div class="controls">

      <div class="span12">
      <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>
       
       &nbsp;sampai tanggal&nbsp;
        <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>
        &nbsp; <?php echo CHtml::submitButton('Lihat Laporan',array('class'=>'btn btn-success')); ?>
      </div>
      </div>
    </div>
  
  </fieldset>
  <?php $this->endWidget();?>

<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>

<div class="control-group" align="center">
  <div class="controls">

      <div class="span12">
<?php

$form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
      'target' => '_blank'
    ),  
));

echo $form->hiddenField($model,'TANGGAL_AWAL');
echo $form->hiddenField($model,'TANGGAL_AKHIR');
echo CHtml::dropDownList('print','',array(1=>'PDF',2=>'XLS'));
echo '&nbsp;';
echo CHtml::submitButton('Cetak Laporan Prwt OP',array('class'=>'btn btn-info'));


$this->endWidget();

?>

</div>
</div>
</div>
<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No Rekam Medis</th>
      <th>Nama Pasien</th>
      <th>Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th>LM DI<br>RWT</th>
      <th>JS. OP</th>
      <!-- <th>Konsul Gizi</th> -->
      <!-- <th>Jumlah</th> -->
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $total = 0;


    $i = 1;
    foreach($model->searchLaporan() as $ri)
    {

      if($ri->sumPrwtOP == 0) continue;

       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

     
                  
        $total = $total + $ri->sumPrwtOP;

        
  ?>
    <tr>
       <td><?php echo $i++;?></td>
       <td>
        <?php 

        echo CHtml::link($ri->pASIEN->NoMedrec,Yii::app()->createUrl('trRawatInap/dataPasien',array('id'=>$ri->id_rawat_inap)));?>
          
        </td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
 
     <!--  <td>0</td> -->
      <td style="text-align: right;"><?php 
         echo CHtml::link(Yii::app()->helper->formatRupiah($ri->sumPrwtOP),Yii::app()->createUrl('trRawatInap/medikTOP',array('id'=>$ri->id_rawat_inap)));
              ?></td>
      <!-- <td></td> -->
     </tr>
    <?php 
      
    
  }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
      <td>&nbsp;</td>

      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total);
      ?></strong>
      </td>
       <!-- <td>&nbsp;</td> -->
    </tr>
  </tbody>

</table>
<?php

  }
?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>