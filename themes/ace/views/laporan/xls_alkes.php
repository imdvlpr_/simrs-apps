<?php 
header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_alkes_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="12">
      LAPORAN ALKES <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" >
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No <br>RM</th>
      <th  width="15%">Nama Pasien</th>
      <th width="7%">Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th width="5%">LM DI<br>RWT</th>
     <th>Alkes IRD</th>
     <th>Alkes Kamar</th>
      <th>JUMLAH</th>
    </tr>
  </thead>
  <tbody>
  <?php 
 $i=1;

    $total_obat = 0;

    $total_ird = 0;
    $total_kamar = 0;
    foreach($model->searchLaporan() as $ri)
    {
      $total_ird = $total_ird + $ri->sumAlkesIrd;
      $total_kamar = $total_kamar + $ri->sumAlkes;

      $total_obat_item = $ri->sumAlkes + $ri->sumAlkesIrd;
          
      $total_obat = $total_obat + $total_obat_item;
      $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      if($total_obat_item == 0) continue;
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td  width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
       <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td  width="5%"><?php echo $selisih_hari;?></td>
      <td style="text-align: right;"><?php 
        
         echo ($ri->sumAlkesIrd);
              ?></td>
      <!-- <td>0</td> -->
      <td style="text-align: right;"><?php 
        echo ($ri->sumAlkes);
              ?></td>
     <td style="text-align: right;">
         <?php 
        echo ($total_obat_item);
         ?>
       </td>
     </tr>

    <?php 
      
    }

    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
     
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
       <td>&nbsp;</td>
     <td><strong>
        <?php 
       echo ($total_ird);
      ?></strong></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo ($total_kamar);
      ?></strong>
      </td>
      <td><strong>
        <?php 
 echo ($total_obat);
        ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>