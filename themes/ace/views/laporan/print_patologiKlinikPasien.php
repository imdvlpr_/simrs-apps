
<table cellpadding="4" border="1" style="font-size: 8px">
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No Registrasi</th>
      <th width="15%">Nama Pasien</th>
      <th width="7%">Status Px</th>
      <th>Kamar</th>
      <th>Klas</th>
      <th>Instalasi</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
      <th width="7%">Lm<br>Dirawat</th>
      <th>Nama Tindakan</th>
      <th>IRD</th>
      <th>IRNA</th>
     <th>Total</th> 
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;



   $all_total_biaya = 0;
    $all_total_ird = 0;
    $all_total_irna = 0;
    
    
    foreach($rawatInaps as $ri)
    {

      if(!$obat->isNewRecord)
      {
      if(empty($ri)) continue;
      $total_biaya_irna = 0;
         $total_biaya_ird = 0;
      foreach($ri->trRawatInapAlkes as $vd)
      {

          
          if(($vd->alkes->param == 'patoklinik') && ($vd->id_alkes == $obat->id_obat_alkes))
          {
               $total_biaya_irna = $total_biaya_irna + $vd->biaya_irna;
             $total_biaya_ird = $total_biaya_ird + $vd->biaya_ird;
          }   
        
      }

     $all_total_biaya = $all_total_biaya + $total_biaya_irna + $total_biaya_ird;

        $all_total_irna = $all_total_irna + $total_biaya_irna;
        $all_total_ird = $all_total_ird + $total_biaya_ird;

        if($total_biaya_irna == 0 && $total_biaya_ird == 0) continue;
      $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }

       
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php 
       echo $ri->pASIEN->NoMedrec;
       ?></td>
      <td width="15%"><?php 
      echo $ri->pASIEN->NAMA;
      ?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
       <td>
      <?php 
      echo $ri->kamar->nama_kamar;
      ?>
      </td>
      <td>
      <?php 
      echo $ri->kamar->kelas->nama_kelas;
      ?>
      </td>
      <td>Patologi Klinik</td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
       <td><?php echo $selisih_hari;?></td>
      <td><?php 
      echo  $obat->nama_obat_alkes;
       ?></td>
      
     <td style="text-align: right"><?php 
      echo Yii::app()->helper->formatRupiah($total_biaya_ird);

              ?></td>
      <td style="text-align: right"><?php 
        echo Yii::app()->helper->formatRupiah($total_biaya_irna);
              ?></td>
      <td style="text-align: right">
        <?php 
      echo  Yii::app()->helper->formatRupiah($total_biaya_ird + $total_biaya_irna);
        ?>
      </td>
     </tr>
    <?php 
      }
    // }
    
    // foreach($rawatInaps as $ri)
    // {

       if(!$penunjang->isNewRecord)
      {
        if(empty($ri)) continue;
        $total_biaya_irna = 0;
         $total_biaya_ird = 0;
        foreach($ri->trRawatInapPenunjangs as $vd)
        {

            
            if(($vd->penunjang->jenis_penunjang == 2) && ($vd->id_penunjang == $penunjang->id_tindakan_penunjang))
            {
               $total_biaya_irna = $total_biaya_irna + $vd->biaya_irna;
             $total_biaya_ird = $total_biaya_ird + $vd->biaya_ird;
            }   
          
        }

        $all_total_irna = $all_total_irna + $total_biaya_irna;
      $all_total_ird = $all_total_ird + $total_biaya_ird;

      $all_total_biaya = $all_total_biaya + $total_biaya_irna + $total_biaya_ird;

       if($total_biaya_irna == 0 && $total_biaya_ird == 0) continue;
      $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }

  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php 
       echo $ri->pASIEN->NoMedrec;
       ?></td>
      <td width="15%"><?php 
      echo $ri->pASIEN->NAMA;
      ?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
       <td>
      <?php 
      echo $ri->kamar->nama_kamar;
      ?>
      </td>
      <td>
      <?php 
      echo $ri->kamar->kelas->nama_kelas;
      ?>
      </td>
      <td>Patologi Klinik</td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
       <td><?php echo $selisih_hari;?></td>
      <td><?php 
      echo  $penunjang->nama_tindakan;
       ?></td>
      
    
       <td style="text-align: right"><?php 
      echo Yii::app()->helper->formatRupiah($total_biaya_ird);

              ?></td>
      <td style="text-align: right"><?php 
        echo Yii::app()->helper->formatRupiah($total_biaya_irna);
              ?></td>
      <td style="text-align: right">
        <?php 
      echo  Yii::app()->helper->formatRupiah($total_biaya_ird + $total_biaya_irna);
        ?>
      </td>
     </tr>
    <?php 
      }
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td>&nbsp;</td>

      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_ird);
      ?></strong>
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_irna);
      ?></strong>
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_biaya);
      ?></strong>
      </td>
    </tr>
  </tbody>

</table>