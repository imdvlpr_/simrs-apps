<?php 
header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_rekap_jasa_dokter_'.$jenis.'_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="6">
      LAPORAN REKAPITULASI JASA DOKTER <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" >
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="25%">Nama Dokter</th>
      <th width="10%">VISITE</th>
      <th width="10%">KONSUL</th>
      <th width="10%">TINDAKAN</th>
      <th width="10%">JUMLAH</th>
      
    </tr>
  </thead>
  <tbody>
  <?php 
 $i=1;

    $total_tindakan = 0;
    $i = 1;

    $total_visite = 0;
    $total_konsul = 0;
    
    foreach(DmDokter::model()->findAll() as $item)
    {

      
      $tindakan = (object)$model->searchRekapDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      // print_r($tindakan);exit;
      $n_tindakan = !empty($tindakan) ? $tindakan->nilai : 0;
      
      $visite = $model->searchVisiteDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      $n_visite = !empty($visite) ? $visite->nilai : 0;

      $konsul = $model->searchKonsulDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      $n_konsul = !empty($konsul) ? $konsul->nilai : 0;

      $total_tindakan += $n_tindakan;
      $total_visite += $n_visite;
      $total_konsul += $n_konsul;
 
      if($n_tindakan == 0 && $n_visite == 0 && $n_konsul == 0) continue;
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
      <td width="25%"><?php echo $item->nama_dokter;?></td>
      <td style="text-align: right;" width="10%">  <?php 
        if($n_visite != 0)
         echo ($n_visite);
       else 
        echo 0;
              ?></td>
      <td style="text-align: right;" width="10%"> <?php 
      if($n_konsul != 0)
         echo ($n_konsul);
       else 
        echo 0;
              ?></td>
      <td style="text-align: right;" width="10%">

        <?php 
        if($n_tindakan != 0)
         echo ($tindakan->nilai);
       else 
        echo 0;
              ?>
              </td>
     <!--  <td>0</td> -->
      <td style="text-align: right;" width="10%">
                    <?php 
echo ($n_tindakan + $n_visite + $n_konsul);
            ?>    


      </td>
     </tr>
    <?php 
      }
   $total = $total_tindakan + $total_konsul + $total_visite;

    ?>
    <tr>
    <td>&nbsp;</td>
      <td>JUMLAH</td>
      
      <td style="text-align: right;"><?php 
       echo ($total_visite);
      ?></td>
      <td style="text-align: right;"><?php 
       echo ($total_konsul);
      ?></td>
       <td style="text-align: right;"><?php 
       echo ($total_tindakan);
      ?></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo ($total);
      ?>
        </strong>
      </td>
      
    </tr>
  </tbody>

</table>