<table cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th>
      LAPORAN TINDAKAN OPERASI <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No Rekam Medis</th>
      <th  width="15%">Nama Pasien</th>
      <th width="7%">Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th width="5%">LM DI<br>RWT</th>
     <th>Dr. OP</th>
      <th>JS Dr. OP</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;
$total_jm = 0;
    
    foreach($model->searchLaporanPerDokter() as $ri)
    {
      

       $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }
   
         foreach($ri->trRawatInapTopJm as $jm)
        {

          if($jm->nilai == 0) continue;
          $total_jm = $total_jm + $jm->nilai;
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td  width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td  width="5%"><?php echo $selisih_hari;?></td>
      <td style="text-align: right;"><?php 
        
          $id_dr_jm = $jm->id_dokter;

          $drJM = $jm->dokter->NICKNAME;
          
echo $drJM;
              ?></td>
      <!-- <td>0</td> -->
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($jm->nilai);
              ?></td>
     </tr>
    <?php 
      }
    }

    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_jm);
      ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>