<div class="row">
  <div class="col-sm-6">
    <div class="widget-box transparent">
      <div class="widget-header">
        <h4 class="widget-title lighter smaller">
          <i class="ace-icon fa fa-file orange"></i>Laporan Kunjungan Poli
        </h4>
        <img id="loadingRekap" src="<?=Yii::app()->baseUrl;?>/images/loading.gif" style="display: none">
      </div>

      <div class="widget-body">
        <div class="widget-main padding-4">
          <div class="tab-content padding-8">
<form id="form_kunjungan" method="POST" action="<?=Yii::app()->createUrl('laporan/kunjungan');?>">        
            <select name="tahun"  id="tahun">
  <option value=''>Select Year</option>
  <?php
  for ($year = 2014; $year <= 2030; $year++) {
  $selected = (date('Y')==$year) ? 'selected' : '';
  echo "<option value=$year $selected>$year</option>";

  }
  ?>
  </select>
  <?php
  $MonthArray = array(
  "01" => "January", "02" => "February", "03" => "March", "04" => "April",
  "05" => "May", "06" => "June", "07" => "July", "08" => "August",
  "09" => "September", "10" => "October", "11" => "November", "12" => "December",
  );
  ?>
  <select name="bulan" id="bulan">
  <option value="">Select Month</option>
  <?php
  foreach ($MonthArray as $monthNum=>$month) {
  $selected = (date('m') == $monthNum) ? 'selected' : '';
  //Uncomment line below if you want to prefix the month number with leading 0 'Zero'
  //$monthNum = str_pad($monthNum, 2, "0", STR_PAD_LEFT);
  echo '<option ' . $selected . ' value="' . $monthNum . '">' . $month . '</option>';
  }
  ?>
  </select>
  
  <button id="lihat" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> Lihat</button>
  <button id="export" class="btn btn-success btn-sm "><i class="fa fa-download"></i> Export XLS</button>
</form>
        </div><!-- /.widget-main -->
      </div><!-- /.widget-body -->
    </div><!-- /.widget-box -->
  </div><!-- /.col -->
</div>
<div class="row">
  <table class="table table-bordered" id="tabel_kunjungan">
    <thead>
      <tr>
        <th rowspan="2" style="text-align: center;">No</th>
        <th rowspan="2"  style="text-align: center;">Unit</th>
        <th colspan="2" style="text-align: center;">JK</th>
        <th colspan="9" style="text-align: center;">Usia</th>
      </tr>
      <tr>
        <th style="text-align: center;">L</th>
        <th style="text-align: center;">P</th>
        <th style="text-align: center;">0-5</th>
        <th style="text-align: center;">6-11</th>
        <th style="text-align: center;">12-16</th>
        <th style="text-align: center;">17-25</th>
        <th style="text-align: center;">26-35</th>
        <th style="text-align: center;">36-45</th>
        <th style="text-align: center;">46-55</th>
        <th style="text-align: center;">56-65</th>
        <th style="text-align: center;"> > 65</th>
        
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
<script>
$(document).ready(function(){
    $('#lihat').click(function(e){
      e.preventDefault();
      var tahun = $("#tahun").val();
      var bulan = $("#bulan").val();

      $.ajax({
          type : 'POST',
          data : 'bulan='+bulan+'&tahun='+tahun,
          url : '<?=Yii::app()->createUrl('AjaxRequest/GetRekapPoliSexUsia');?>',
          async: true,
          beforeSend : function(){
            $('#loadingRekap').show();

          },
          error : function(e){
              console.log(e);
             $('#loadingRekap').hide();

          },
          success : function(data){
             $('#loadingRekap').hide();
            var hasil = $.parseJSON(data);
            
            var row = '';
            $.each(hasil.listUnit,function(i,obj){

                row += '<tr>';
                row += '<td style="text-align: center;">'+(i+1)+'</td>';
                row += '<td style="text-align: center;">'+obj.unit+'</td>';
                for(var j = 0;j<11;j++){
                  tmp = obj.items[j];
                  if(tmp && tmp.count != null){
                    row += '<td style="text-align: center;">'+tmp.count+'</td>';
                  }

                  else{
                    row += '<td style="text-align: center;">0</td>';
                  }
                }
                
                row += '</tr>';
            });
            $("#tabel_kunjungan tbody").empty();
            $("#tabel_kunjungan tbody").append(row);
            
          }
      });
    });

    $('#export').click(function(e){
      e.preventDefault();
      $('form#form_kunjungan').submit();
    });
});
</script>