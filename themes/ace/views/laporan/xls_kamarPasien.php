<?php 
header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_kamar_pasien_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>

<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="12">
      LAPORAN RAWAT INAP <?php echo $model->kamar->nama_kamar.' / '.$model->kamar->kelas->nama_kelas;?><br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table cellpadding="4" border="1" >
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No<br>Registrasi</th>
      <th width="15%">Nama Pasien</th>
      <th >Status Px</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
      <th width="5%">Lm<br>Dirawat</th>
      <th>Askep</th>
     <th>T. Jasa<br>Medis</th>
      <th>T. JP<br>Perawatan</th>
      <th>BHP</th>
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $total_askep = 0;
    
    $total_biaya_jm = 0;
    $total_biaya_jp = 0;
    $total_bhp = 0;

    foreach($rawatInaps as $ri)
    {
      

       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      else
      {
        $dnow = date('Y-m-d');
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
      }

      $biaya_jm = 0;
      $biaya_jp = 0;

      foreach($ri->trRawatInapTnops as $vi)
      {

          if($vi->id_tnop == 3)
                $biaya_jm = $biaya_jm + $vi->biaya_irna * $vi->jumlah_tindakan;

          if($vi->id_tnop == 6)
                $biaya_jp = $biaya_jp + $vi->biaya_irna * $vi->jumlah_tindakan;
          
      }

      $bhp_top = $ri->sumBHPTop;
      
      $total_bhp += $bhp_top;
      $total_biaya_jm = $total_biaya_jm + $biaya_jm;
      $total_biaya_jp = $total_biaya_jp + $biaya_jp;
        
      $total_askep = $total_askep + $ri->kamar->biaya_askep * $selisih_hari;
        
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td width="5%"><?php echo $selisih_hari;?></td>

      
      <td><?php 
        echo $ri->kamar->biaya_askep * $selisih_hari;
              ?></td>
       <td><?php echo ($biaya_jm);?></td>
      <td><?php echo ($biaya_jp);?></td>
      <td><?php echo ($bhp_top);?></td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>SUBTOTAL</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>

      <td><strong>
        <?php 
       echo ($total_askep);
      ?></strong>
      </td>
       <td><strong><?php echo ($total_biaya_jm);?></strong></td>
      <td><strong><?php echo ($total_biaya_jp);?></strong></td>
      <td><strong><?php echo ($total_bhp);?></strong></td>
    </tr>
     <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;
      </td>
       <td>&nbsp;</td>
      <td><strong><?php echo ($total_askep + $total_biaya_jm + $total_biaya_jp + $total_bhp);?></strong></td>
      
    </tr>
  </tbody>

</table>
<br><br>
<br><br>
<table>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>

      <td>&nbsp;
      </td>
       <td colspan="2">Kepala Ruangan<br><br><br><br><br>..............................</td>
      
    </tr>

</table>