<?php 
header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_rr_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="12">
      LAPORAN RR <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" >
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No <br>RM</th>
      <th  width="15%">Nama Pasien</th>
      <th width="7%">Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th width="5%">LM DI<br>RWT</th>
        <th>Dr. AN</th>
      <th>JS Dr. AN</th>
       <th>JS Prwt AN</th>
     <th>RR-Askep</th>
      <th>RR<br>Monitor</th>
      <th>JUMLAH</th>
    </tr>
  </thead>
  <tbody>
  <?php 

$listTnop = TindakanMedisNonOperatif::model()->findByAttributes(['kode_tindakan'=>'rr']);


    $i = 1;
    $total_ja = 0;
     $total_pan = 0;

    $total_rra = 0;
    $total_rrm = 0;
    
    $subtotal_rr = 0; 

    foreach($model->searchMinimized() as $ri)
    {

  
       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      $nilai_ja = $ri->sumTopJa;  
      $nilai_pan = $ri->sumPrwtAN;
      // echo $ri->sumTopJa.'<br>';
      $total_ja += $nilai_ja;
      $total_pan += $nilai_pan;

      $sumRRA = $ri->sumTindRRA;
      $sumRRM = $ri->sumTindRRM;
      $total_rrm += $sumRRM;
      $total_rra += $sumRRA;

      $drJA = '';

      if($sumRRA == 0 && $sumRRM == 0 && $nilai_pan == 0 && $nilai_ja == 0) 
        continue;

      if(!empty($ri->trRawatInapTopJa[0])){
         $ja = $ri->trRawatInapTopJa[0];
        $drJA = !empty($ja->dokter) ? $ja->dokter->FULLNAME : '';
      }

       $attr = array(
          'id_rawat_inap' => $ri->id_rawat_inap,
          'id_tnop' => $listTnop->id_tindakan
        );
        $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);

        $biaya_rr = !empty($itemTnop) ? $itemTnop->biaya_irna : 0;
        $subtotal_rr += $biaya_rr;  

  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td  width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
       <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td  width="5%"><?php echo $selisih_hari;?></td>
       <td style="text-align: right;">
        <?php
        
          echo $drJA;
      ?>
                
              </td>
     <!--  <td>0</td> -->
      <td style="text-align: right;"><?php
      
         echo Yii::app()->helper->formatBulat($nilai_ja);
              ?></td>
       <td style="text-align: right;"><?php 
      
         echo Yii::app()->helper->formatBulat($nilai_pan);
              ?></td>
      <td style="text-align: right;"><?php 
        
          
          echo Yii::app()->helper->formatBulat($sumRRA);
         
              ?>
                

              </td>
        
     <!--  <td>0</td> -->
      <td style="text-align: right;"><?php 
          echo Yii::app()->helper->formatBulat($sumRRM);
      
              ?>
                
              </td>
      <td style="text-align: right;"><?= Yii::app()->helper->formatBulat($biaya_rr);?></td>
       <td style="text-align: right;">
         <?php 
         echo Yii::app()->helper->formatBulat($nilai_ja + $nilai_pan + $sumRRA + $sumRRM + $biaya_rr);
         ?>
       </td>
     </tr>

    <?php 
      
    }

    unset($listrr);
    ?>
    <tr>
     <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatBulat($total_ja);
      ?></strong></td>
     <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatBulat($total_pan);
      ?></strong></td>
       <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatBulat($total_rra);
      ?></strong></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatBulat($total_rrm);
      ?></strong>
      </td>
       <td style="text-align: right;"><strong>
        <?php 
 echo Yii::app()->helper->formatBulat($subtotal_rr);
        ?></strong>
      </td>
      <td style="text-align: right;"><strong>
        <?php 
 echo Yii::app()->helper->formatBulat($total_rra + $total_rrm + $total_pan + $total_ja + $subtotal_rr);
        ?></strong>
      </td>
    </tr>
  </tbody>

</table>