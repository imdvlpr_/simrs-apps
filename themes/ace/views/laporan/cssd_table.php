<?php 
if($print == 2){
  header('Content-type: application/excel');
  $tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

  $filename = 'laporan_cssd_'.$tgl.'.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<table class="table  table-bordered table-hover" border="1">
  
  <thead>
    <tr>
      <th>No</th>
      <th>No RM</th>
      <th>Nama Px</th>
      <th>Status</th>
      
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl Operasi</th>
      <th>Jenis Tindakan</th>
     
      <th>Golongan Operasi</th>
      <!-- <th>Konsul Gizi</th> -->
      <th>Tarif CSSD</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $i = 1;
    $total_cssd = 0;
    foreach($model->searchLaporan() as $ri)
    {
      $biaya = $ri->tdRegisterOk->tindakan0->dmGolOperasi->biaya_cssd;

      $total_cssd += $biaya;
       

  ?>
    <tr>
       <td><?=$i;?></td>
       <td>
        <?php
        if($print==2)
        {
          echo $ri->tdRegisterOk->pASIEN->NoMedrec;
        
        }
        else{
          echo CHtml::link($ri->tdRegisterOk->pASIEN->NoMedrec,Yii::app()->createUrl('tdRegisterOk/view',array('id'=>$ri->tdRegisterOk->id_ok)));
        }
      ?>
          
        </td>
      <td><?= $ri->tdRegisterOk->pASIEN->NAMA;?></td>
      <td><?= $ri->tdRegisterOk->kodeDaftar->kodeGol->NamaGol;?></td>
      <td>
      <?= $ri->tdRegisterOk->tindakan0->dmKelas->nama_kelas;?>
      </td>
      <td>
      <?= $ri->tdRegisterOk->unit->NamaUnit;?>
      </td>
      <td style="text-align: right;"><?=date('d/m/Y',strtotime($ri->tdRegisterOk->tgl_operasi));?></td>
      <td style="text-align: right;"><?=$ri->tdRegisterOk->diagnosa;?></td>

     <!--  <td>0</td> -->
    
      <td style="text-align: right;"><?=$ri->tdRegisterOk->jenisOperasi->nama;?></td>
   
      <td style="text-align: right;"><?=$print==1 || empty($print)? Yii::app()->helper->formatRupiah($biaya) : $biaya;?></td>
      <!-- <td></td> -->
     </tr>
    <?php 
       
      
      $i++;
  }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td style="text-align: right;"><?=$print==1 || empty($print)?Yii::app()->helper->formatRupiah($total_cssd) : $total_cssd;?></td>
      
       <!-- <td>&nbsp;</td> -->
    </tr>
  </tbody>

</table>
