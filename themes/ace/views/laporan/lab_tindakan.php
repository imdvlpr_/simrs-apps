<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Data OK','url'=>Yii::app()->createUrl('tdRegisterOk/admin')),
                            array('name' => 'Laporan OK','url'=>'#'),
                            
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="row">
    <div class="col-xs-12">
        

                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-error">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    <legend>
      

    </legend>
  <div class="form-group">
  <?php echo CHtml::label('Antara Tanggal','',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
   <div class="col-sm-9">
        <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>
   </div>
 </div>
  <div class="form-group">
  <?php echo CHtml::label('Sampai dengan','',array('class'=>'col-sm-3 col-sm-3 control-label no-padding-right'));?>
   <div class="col-sm-9">
        <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
   
        ?>
   </div>
 </div>

  
   <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>
          <?php 
          if(!empty($_POST['LabRequestItem']['TANGGAL_AWAL']) && !empty($_POST['LabRequestItem']['TANGGAL_AKHIR']))
          {
            echo CHtml::link('<i class="fa fa-download"></i> Export Biaya',[
              'laporan/LabTindakanExport',
              'sd'=>$_POST['LabRequestItem']['TANGGAL_AWAL'],
              'ed'=>$_POST['LabRequestItem']['TANGGAL_AKHIR'],
              
            ],['class'=>'btn btn-success']);
          }
          ?>
        
        </div>
      </div>
   
  </fieldset>
  <?php $this->endWidget();?>

<?php
if(!empty($_POST['LabRequestItem']['TANGGAL_AWAL']) && !empty($_POST['LabRequestItem']['TANGGAL_AKHIR']))
{
  ?>


<table  class="table table-striped table-bordered">
  <tr>
    <TH>No</TH>
    <th>Kategori</th>
    <TH>Item Tindakan</TH>
    <th>Jumlah</th>
    
  </tr>
  <?php
  $i=0;
  
  $total=0;
  foreach($parents->searchAll() as $data){
    $i++;
    $count = $results[$data->id]; 
    $total+=$count;
    
  ?>
  <tr>
    <td><?=$i;?></td>
    <td><?=$data->parent->nama;?></td>
    <td><?=$data->item->nama ?></td>
    <td><?=$count?></td>
   
  </tr>
<?php } ?>
  <tr>
    <td colspan="3">TOTAL</td>
    <td  style="text-align: right;"><?=$total?></td>
    
    
  </tr>
</table>
  <?php
}
 ?>

                    </div>
                </div>
           