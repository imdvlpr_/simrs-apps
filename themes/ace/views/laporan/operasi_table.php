
<table class="table  table-bordered table-hover">
  
  <thead>
    <tr>
      <th>No</th>
      <th>No RM</th>
      <th>Nama Px</th>
      <th>Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
      <th>LM DIRWT</th>
      <th>Dr. OP</th>
      <th>JS Dr. OP</th>
     
      <th>JS Prwt OK</th>
      <!-- <th>Konsul Gizi</th> -->
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $total_jm = 0;
    $total_pok = 0;
    $total_all = 0;

    $i = 1;
    foreach($model->searchLaporan() as $ri)
    {

       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      $nilai_jm = $ri->sumTopJm;

      $nilai_pok = $ri->sumPrwtOK;
      


      if($nilai_jm == 0 &&  $nilai_pok == 0 ) 
        continue;

      $total_kanan = $nilai_jm + $nilai_pok;
      $total_all += $total_kanan;

      $total_jm += $nilai_jm;

      $total_pok += $nilai_pok;
      

     
          
      $j = 0;
      foreach($ri->trRawatInapTopJm as $jm)
      {


         $drJM = '';
      // $drJA = '';

      // if(!empty($ri->trRawatInapTopJm[0])){

        $drJM = !empty($jm->dokter) ? $jm->dokter->FULLNAME : '';
      // }

  ?>
    <tr>
       <td><?php 
       if($j==0) 
        echo $i;?></td>
       <td>
        <?php 

        echo CHtml::link($ri->pASIEN->NoMedrec,Yii::app()->createUrl('trRawatInap/dataPasien',array('id'=>$ri->id_rawat_inap)));?>
          
        </td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php
      if($j==0) 
       echo $ri->tanggal_masuk;?></td>
      <td><?php 
      if($j==0) 
      echo $ri->tanggal_keluar;?></td>
      <td><?php 
if($j==0)   
      echo $selisih_hari;?></td>
      <td style="text-align: right;">
        <?= $drJM;?></td>
     <!--  <td>0</td> -->
      <td style="text-align: right;"><?php 
         echo CHtml::link(Yii::app()->helper->formatRupiah($jm->nilai),Yii::app()->createUrl('trRawatInap/medikTOP',array('id'=>$ri->id_rawat_inap)));
              ?></td>

     <!--  <td>0</td> -->
    
      <td style="text-align: right;"><?php 
      if($j==0)
         echo CHtml::link(Yii::app()->helper->formatRupiah($nilai_pok),Yii::app()->createUrl('trRawatInap/medikTOP',array('id'=>$ri->id_rawat_inap)));
              ?></td>
   
      <td style="text-align: right;"><?php 
      if($j==0)
         echo Yii::app()->helper->formatRupiah($nilai_jm + $nilai_pok);
              ?></td>
      <!-- <td></td> -->
     </tr>
    <?php 
        $j++;
      }
      $i++;
  }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_jm);
      ?></strong>
      </td>
     
     
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_pok);
      ?></strong>
      </td>
       <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_all);
      ?></strong>
      </td>
       <!-- <td>&nbsp;</td> -->
    </tr>
  </tbody>

</table>
