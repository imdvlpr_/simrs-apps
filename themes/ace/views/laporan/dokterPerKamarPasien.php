<?php 
header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($tgl_awal)).'_'.date('d-m-Y',strtotime($tgl_akhir));

$filename = 'laporan_jaspel_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);

function sum()
{
    return array_sum(func_get_args());
}

$list_penunjangs = TindakanMedisPenunjang::model()->findAll();
$list_obats = ObatAlkes::model()->findAll();
$list_lains = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>6));
$listTnop = TindakanMedisNonOperatif::model()->findAll();
$listTops = TindakanMedisOperatif::model()->findAll();
?>
<table>
  <tr>
  <th colspan="16" style="text-align: center;">KLAIM JASPEL PX <?=$tgl;?></th>
</tr>
<tr>
  <th colspan="16"  style="text-align: center;">Ruang : <?=$modelKamarMaster->nama_kamar;?></th>
</tr>

</table>
<table border="1">
  
  <thead>
    <tr>
      <th>No</th>
      <th>No. Reg</th>
      <th>Nama Pasien</th>
      <th>Status<br>Px</th>
      <th>Kls</th>
      <th>Visite<br>Ahli</th>
      <th>Jml</th>
      <th>Biaya</th>
      <th>Konsul<br>Ahli</th>
      <th>Jml</th>
      <th>Biaya</th>
      <th>Visite<br>Umum</th>
      <th>Jml</th>
      <th>Biaya</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
      <th>Lm<br>Drwt</th>
      <th>Askep</th>
      <th>Tindakan</th>
      <th>Japel</th>
      
   
    </tr>
  </thead>
  <tbody>
<?php
    $i=1;

    $total = 0;

    $vtotal_kamar = 0;
    $vtotal_askep = 0;
    $vtotal_asnut = 0;
    $vtotal_visite_ahli =0;
    $vtotal_visite_umum = 0;
    $vtotal_konsul_ahli = 0;
    $vtotal_bhp = 0;
    $vtotal_tops = [];
    $vtotal_tnop_jrm = 0;
    $vtotal_tnop_jm = 0;
    $vtotal_tnop_japel = 0;
    $vtotal_penunjangs = [];
    $vtotal_obats = [];
    $vtotal_lains = [];
    $vtotal_tindakan = 0;
    $vtotal_total = 0; 


    foreach($rawatInaps as $rawatInap)
    {
      $rawatRincian = $rawatInap->trRawatInapRincians;
       $selisih_hari = 1;

      if(!empty($rawatInap->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
      }

      else
      {
        $dnow = date('Y-m-d');
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
      }

      $nama_dokter_irna = '';

      if(!empty($rawatInap->dokter)){
        $nama_dokter_irna = $rawatInap->dokter->FULLNAME;
      } 


    
      $subtotal_biaya_visite_ahli = 0;
      $subtotal_biaya_visite_umum = 0;
      $nama_dokter_visite_ahli = '';
      $nama_dokter_visite_umum = '';
      $jml_visite_ahli = 0;
      $jml_visite_umum = 0;
      $biaya_visite_ahli = 0;
      $biaya_visite_umum = 0;
      $biaya_konsul_ahli = 0;
      $subtotal_visite = 0;

      $id_dokter_irna = 0;

      $visiteAhli = array();
      $visiteUmum = array();

      $idx1 = 0;
      $idx2 = 0;
      foreach($rawatInap->getDataVisite('VISITE') as $visite)
      {
          if(empty($visite->jenisVisite)) 
            continue;

          if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Ahli'))
          {

             if(!empty($visite->dokterIrna))
             {

                if(trim($visite->dokterIrna->jenis_dokter) == 'AHLI')
                {
                  $visiteAhli[] = array(
                    'nama' => $visite->dokterIrna->FULLNAME,
                    'jml' => $visite->jumlah_visite,
                    'biaya' => $visite->biaya_visite_irna
                   );
                   
                   if($idx1==0){
                      $nama_dokter_visite_ahli = $visite->dokterIrna->FULLNAME;
                      $jml_visite_ahli = $visite->jumlah_visite;
                      $biaya_visite_ahli = $visite->biaya_visite_irna;
                      $id_dokter_irna = $visite->dokterIrna->id_dokter;
                   }

                    $idx1++;
                }

                else  if((trim($visite->dokterIrna->jenis_dokter) )== 'UMUM')
                {
                  $visiteUmum[] = array(
                    'nama' => $visite->dokterIrna->FULLNAME,
                    'jml' => $visite->jumlah_visite,
                'biaya' => $visite->biaya_visite_irna
                   );
                   

                   
                   if($idx2==0){
                      $nama_dokter_visite_umum = $visite->dokterIrna->FULLNAME;
                      $jml_visite_umum = $visite->jumlah_visite;
                      $biaya_visite_umum = $visite->biaya_visite_irna;
                   }

                     $idx2++;
                 }               
             }

          }

          if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Umum'))
          {

             if(!empty($visite->dokterIrna) && (trim($visite->dokterIrna->jenis_dokter)) == 'UMUM')
             {
              $visiteUmum[] = array(
                  'nama' => !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME : '',
                  'jml' => $visite->jumlah_visite,
                'biaya' => $visite->biaya_visite_irna
               );
              if($idx2==0){
                $nama_dokter_visite_umum = !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME.', ' : '';
                $jml_visite_umum = $visite->jumlah_visite;
                $biaya_visite_umum = $visite->biaya_visite_irna;
              }
            }
            $subtotal_biaya_visite_umum += $visite->biaya_visite_irna;
            
             
            // $nama_dokter_visite_umum .= !empty($visite->dokterIrna) ? $visite->dokterIrna->NICKNAME.', ' : '';
            $idx2++;
          }
      }

      // print_r($visiteUmum);exit;

      $subtotal_visite = $subtotal_biaya_visite_umum + $subtotal_biaya_visite_ahli;  
      $subtotal_biaya_konsul_ahli = 0;
      $nama_dokter_konsul_ahli = '';
      $jml_konsul_ahli=0;
      $biaya_konsul_ahli = 0;
      $subtotal_konsul = 0;
      $konsulAhli = array();
      $idx3=0;
      foreach($rawatInap->getDataVisite('KONSUL') as $visite)
      {

        if(empty($visite->jenisVisite)) 
          continue;

        if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Ahli'))
        {
           $konsulAhli[] = array(
                'nama' => !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME : '',
                'jml' => $visite->jumlah_visite,
                'biaya' => $visite->biaya_visite_irna
             );
           $subtotal_biaya_konsul_ahli += $visite->biaya_visite_irna;
           if($idx3==0){
              $nama_dokter_konsul_ahli = !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME.', ' : '';
              $jml_konsul_ahli = $visite->jumlah_visite;
              $biaya_konsul_ahli = $visite->biaya_visite_irna;
           }
             
           // $nama_dokter_konsul_ahli = !empty($visite->dokterIrna) ? $visite->dokterIrna->NICKNAME.', ' : '';
           $idx3++;
        }
      }

      $subtotal_konsul = $subtotal_biaya_konsul_ahli;  

      $visiteSort = array(
        count($visiteAhli),
        count($visiteUmum),
        count($konsulAhli)
      );


      rsort($visiteSort);
      

      $subtotal_jrm = 0;
      $subtotal_jm = 0;
      $subtotal_japel = 0;
       foreach($listTnop as $tnop)
       {
          $attr = array(
            'id_rawat_inap' => $rawatInap->id_rawat_inap,
            'id_tnop' => $tnop->id_tindakan
          );
          $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);

          if(!empty($itemTnop)){
            
            if($tnop->kode_tindakan == 'jrm'){
              $subtotal_jrm += $itemTnop->biaya_irna;  
            }

            else if($tnop->kode_tindakan == 'jm'){
              $subtotal_jm +=  $itemTnop->biaya_irna + $itemTnop->biaya_ird;;  
            }

            else if($tnop->kode_tindakan == 'japel'){
              $subtotal_japel += $itemTnop->biaya_irna;  
            }
          }
       }   

      $biaya_tindakan_anas = 0;
      $biaya_tindakan_jm = 0;
      $biaya_tindakan_total = 0;
      foreach($rawatInap->trRawatInapTops as $vi)
      {

          if(!empty($vi->id_dokter_jm) && $vi->id_dokter_jm == $id_dokter_irna)
          {
              $biaya_tindakan_jm = $biaya_tindakan_jm + $vi->biaya_irna * $vi->jumlah_tindakan;
          }

          if(!empty($vi->id_dokter_anas) && $vi->id_dokter_anas == $id_dokter_irna)
          {
              $biaya_tindakan_anas = $biaya_tindakan_anas + $vi->biaya_irna * $vi->jumlah_tindakan;
          }
      }

      $biaya_tindakan_total = $biaya_tindakan_jm + $biaya_tindakan_anas;
      
      $biaya_top_jm = 0;
      foreach($rawatInap->trRawatInapTopJm as $vi)
      {
        if(!empty($vi->id_dokter) && $vi->id_dokter == $id_dokter_irna)
        { 
            $biaya_top_jm = $biaya_top_jm + $vi->nilai;
        } 
      }

      $biaya_tindakan_total = $biaya_tindakan_total + $biaya_top_jm;
      $vtotal_tindakan += $biaya_tindakan_total;
  ?>



    <tr>
       <td><?php echo $i++;?></td>
       <td>&nbsp;<?php echo $rawatInap->pASIEN->NoMedrec;?></td>
      <td><?php echo $rawatInap->pASIEN->NAMA;?></td>
      <td><?php echo $rawatInap->jenisPasien->NamaGol;?></td>
      <td><?php echo $rawatInap->kamar->kelas->nama_kelas;?></td>
      <td><?=$nama_dokter_visite_ahli;?></td>
      <td><?=$jml_visite_ahli;?></td>
      <td><?=$biaya_visite_ahli;?></td>
      <td><?=$nama_dokter_konsul_ahli;?></td>
      <td><?=$jml_konsul_ahli;?></td>
      <td><?=$biaya_konsul_ahli;?></td>
      <td><?=$nama_dokter_visite_umum;?></td>
      <td><?=$jml_visite_umum;?></td>
      <td><?=$biaya_visite_umum;?></td>
      <td><?php echo $rawatInap->tanggal_masuk;?></td>
      <td><?php echo $rawatInap->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
        <?php
        $isneonatus = $rawatInap->kamar->nama_kamar == 'NEONATUS';
      $biaya_kamar = 0;
      if($isneonatus)
      {
        $biaya_neonatus = $rawatInap->biaya_kamar;
        $biaya_kamar = $biaya_neonatus;
      }

      else
      {

        if($rawatInap->kamar->kelas->kode_kelas != 'IRD')
        {
        
          $biaya_kamar = $rawatInap->kamar->biaya_kamar * $selisih_hari;   
        }
      }

        
      $subtotal = $biaya_kamar;
      $vtotal_kamar += $biaya_kamar;

        ?>
      <td style="text-align: right"><?php 
      echo ($rawatInap->kamar->biaya_askep * $selisih_hari);
      // echo ($rawatRincian->askep_kamar);
      $subtotal = $subtotal + $rawatInap->kamar->biaya_askep * $selisih_hari;
      $vtotal_askep += $rawatInap->kamar->biaya_askep * $selisih_hari;
      ?></td>
    
      <?php 

      $value_tmp = [];
      $subvtotal_tops = [] ;
      ?>
      <td style="text-align: center;"><?=($subtotal_jm);?></td>
      <?php

      $subtotal = $subtotal +  $subtotal_japel ;
      $vtotal_tnop_japel +=$subtotal_japel;
      $vtotal_total += $subtotal;
      ?>
      <td><?=($subtotal_japel);?></td>
     </tr>
<?php 
$topVisite = $visiteSort[0];
// print_r($topVisite);exit;
for($idx =1;$idx < $topVisite ; $idx++)
{

    $nmVAhli = !empty($visiteAhli[$idx]) ? $visiteAhli[$idx]['nama']: '';
    $nmVUmum = !empty($visiteUmum[$idx]) ? $visiteUmum[$idx]['nama']: '';
    $nmKAhli = !empty($konsulAhli[$idx]) ? $konsulAhli[$idx]['nama']: '';

    $bVAhli = !empty($visiteAhli[$idx]) ? $visiteAhli[$idx]['jml']: '';
    $bVUmum = !empty($visiteUmum[$idx]) ? $visiteUmum[$idx]['jml']: '';
    $bKAhli = !empty($konsulAhli[$idx]) ? $konsulAhli[$idx]['jml']: '';

    $bbVAhli = !empty($visiteAhli[$idx]) ? $visiteAhli[$idx]['biaya']: '';
    $bbVUmum = !empty($visiteUmum[$idx]) ? $visiteUmum[$idx]['biaya']: '';
    $bbKAhli = !empty($konsulAhli[$idx]) ? $konsulAhli[$idx]['biaya']: '';
    // if()

?>


 <tr>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td></td>
      <td></td>
      <td></td>
      <td><?=$nmVAhli;?></td>
      <td><?=$bVAhli;?></td>
      <td><?=$bbVAhli;?></td>
       <td><?=$nmKAhli;?></td>
      <td><?=$bKAhli;?></td>
        <td><?=$bbKAhli;?></td>
       <td><?=$nmVUmum;?></td>
      <td><?=$bVUmum;?></td>
      <td><?=$bbVUmum;?></td>
      <td></td>
      <td></td>
      <td></td>
 <td style="text-align: right"></td>
      <td style="text-align: right"></td>
      <td style="text-align: right"></td>
      
     </tr>
<?php 
}
?>
    <?php 
      
} // end rawatInaps

?>
     
       </tbody>

</table>