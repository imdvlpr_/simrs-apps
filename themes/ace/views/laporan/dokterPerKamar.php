<?php 
$this->breadcrumbs=array(
  array('name' => 'Laporan Jaspel PX Kamar','url'=>Yii::app()->createUrl('laporan/dokterPerKamar')),
                            
);
?>

<div class="row" >
    <div class="col-xs-12">
        
          
         
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  'method' => 'get',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
              
              ?>
            
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    
     
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Kamar</label>
            <div class="col-sm-9">
          <?php 


  
               
   echo CHtml::dropDownList('kamar',!empty($_POST['kamar']) ? $_POST['kamar'] : '-',$list_kamar);
        ?>
              
            </div>
          </div>
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-success" type="submit">
            <i class="ace-icon fa fa-download bigger-110"></i>
            Export Laporan
          </button>
          <?php 
           if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR)){
           $link = array('laporan/operasi','tgl_awal'=>$model->TANGGAL_AWAL,'tgl_akhir'=>$model->TANGGAL_AKHIR,'print'=>2);
            echo CHtml::link('Ekspor XLS Laporan Operasi',$link,array('class'=>'btn btn-success'));
        }
          ?>
  </div>
      </div>
        
      
   <?php
       
  $this->endWidget();?>


<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 

    $jenis = (!empty($model->TIPE_PASIEN) ? $model->TIPE_PASIEN : '');

$this->renderPartial('_irdPasien',array(
  'model'=>$model,
  'rawatInaps' => $rawatInaps
));
  }
?>

<?php 

?>

                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>