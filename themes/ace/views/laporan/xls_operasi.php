<?php 
header('Content-type: application/excel');
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_operasi_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="15">
      LAPORAN TINDAKAN OPERASI <br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" >
  <thead>
    <tr>
      <th>No</th>
      <th>No RM</th>
      <th>Nama Px</th>
      <th>Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th>LM DI<br>RWT</th>
      <th>Dr. OP</th>
      <th>JS Dr. OP</th>
      <th>JS Prwt OK</th>
      
      <!-- <th>Konsul Gizi</th> -->
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $total_jm = 0;
    
    $total_pok = 0;
    
     $total_all = 0;

    $i = 1;
    foreach($model->searchLaporan() as $ri)
    {

       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      $nilai_jm = $ri->sumTopJm;
    
      $nilai_pok = $ri->sumPrwtOK;
    

      if($nilai_jm == 0 && $nilai_pok == 0) 
        continue;

      $total_kanan = $nilai_jm + $nilai_pok;
      $total_all += $total_kanan;

      $total_jm += $nilai_jm;

      $total_pok += $nilai_pok;
      

      // $idx = 0;  

      // $drJM = '';
      

       $j = 0;
      foreach($ri->trRawatInapTopJm as $jm)
      {


        $drJM = !empty($jm->dokter) ? $jm->dokter->FULLNAME : '';
  ?>
    <tr>
       <td><?php 
        if($j==0) 
       echo $i;?></td>
       <td>
        <?=$ri->pasien_id;?>
          
        </td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      <td><?= $drJM;?></td>
      <td style="text-align: right;"><?=($jm->nilai);?></td>
      

      <td style="text-align: right;"><?=$j==0 ? ($nilai_pok) : ''?></td>
      
      <td style="text-align: right;"><?=$j==0 ? ($total_kanan):'';?></td>
      <!-- <td></td> -->
     </tr>
    <?php

        $j++; 
      }
    $i++; 
  }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo ($total_jm);
      ?></strong>
      </td>
      <td>&nbsp;</td>
      
      <td style="text-align: right;"><strong>
        <?php 
       echo ($total_pok);
      ?></strong>
      </td>
      
       <td style="text-align: right;"><strong>
        <?php 
       echo ($total_all);
      ?></strong>
      </td>
       <!-- <td>&nbsp;</td> -->
    </tr>
  </tbody>

</table>