<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Laporan Jasa Dokter','url'=>Yii::app()->createUrl('laporan/jasaDokter')),
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="row" >
    <div class="col-xs-12">
        
          
         
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
              
              ?>
            
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    
     
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>

        
      
  <?php $this->endWidget();?>
  </div>
      </div>

<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>
<div class="control-group" align="center">
  <div class="controls">

      <div class="span12">
<?php

$form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
      'target' => '_blank'
    ),  
));

echo $form->hiddenField($model,'TANGGAL_AWAL');
echo $form->hiddenField($model,'TANGGAL_AKHIR');
echo CHtml::dropDownList('print','',array(1=>'PDF',2=>'XLS'));
echo '&nbsp;';
echo CHtml::submitButton('Cetak Laporan JASA DOKTER',array('class'=>'btn btn-info'));


$this->endWidget();

?>

</div>
</div>
</div>
<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No<br>Reg</th>
      <th>Nama Pasien</th>
    <th>Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th>LM DI<br>RWT</th>
     <th>Dr. OP</th>
      <th>JS Dr. OP</th>
      <th>Dr. Anas</th>
      <th>JS Dr. Anas</th>
      <th>JUMLAH</th>
    </tr>
  </thead>
  <tbody>
  <?php 


    $listjm = array();
    
    foreach($model->searchLaporan() as $ri)
    {

      
      $valjm = 0;
      $dokterjm = array();
    $dokterja = array();

      foreach($ri->trRawatInapTopJm as $t)
      {
          $valjm = $valjm + $t->nilai;
          if(!empty($t->dokter))
            $dokterjm[] = $t->dokter->FULLNAME;
      }

      $valja = 0;
      foreach($ri->trRawatInapTopJa as $t)
      {
          $valja = $valja + $t->nilai;
          if(!empty($t->dokter))
            $dokterja[] = $t->dokter->FULLNAME;
      }

      if($valja != 0 && $valjm != 0)
      {
        $listjm[$ri->id_rawat_inap] = array(
          'ja' => array(
            'value' => $valja,
            'dokter' => $dokterja
          ),
          'jm' => array(
            'value' => $valjm,
            'dokter' => $dokterjm
          ),
        );
   
     }

      
    }

    // print_r($listrr);

    $i=1;

    $total_ja = 0;
    $total_jm = 0;
    
   
      foreach($listjm as $q => $t)
      {

        $ri = TrRawatInap::model()->findByPk($q);

       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }
  ?>
    <tr>
       <td><?php echo $i++;?></td>
       <td><?php echo CHtml::link($ri->pASIEN->NoMedrec,Yii::app()->createUrl('trRawatInap/dataPasien',array('id'=>$ri->id_rawat_inap)));?></td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      <td style="text-align: right;"><?php 
        
          $total_jm = $total_jm + $t['jm']['value'];
          echo Yii::app()->helper->formatRupiah($t['jm']['value']);
         
              ?>
                

              </td>
        <td>
          <?php 

          $count = count($t['jm']['dokter']);
          foreach($t['jm']['dokter'] as $d){
            if($count == 1)
              echo $d;
            else
              echo $d.', ';
          }
          
          ?>
            
          </td> 
     <!--  <td>0</td> -->
      <td style="text-align: right;"><?php 
        $total_ja = $total_ja + $t['ja']['value'];
          echo Yii::app()->helper->formatRupiah($t['ja']['value']);
      
              ?>
                
              </td>
             
      <td>
        <?php 
         $count = count($t['ja']['dokter']);
          foreach($t['ja']['dokter'] as $d){
            if($count == 1)
              echo $d;
            else
              echo $d.', ';
          }
        ?>
          
        </td>
       <td style="text-align: right;">
         <?php 
         echo CHtml::link(Yii::app()->helper->formatRupiah($t['jm']['value'] + $t['ja']['value']),Yii::app()->createUrl('trRawatInap/medikTOP',array('id'=>$ri->id_rawat_inap)));
         ?>
       </td>

     </tr>
    <?php 
      
    
  }

  unset($listjm);
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_ja);
      ?></strong></td>
     
      </td>
      <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_jm);
      ?></strong>
      <td>&nbsp;</td>
      <td><strong>
        <?php 
 echo Yii::app()->helper->formatRupiah($total_ja + $total_jm);
        ?></strong>
      </td>
    </tr>
  </tbody>

</table>
<?php
 


  }
?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>