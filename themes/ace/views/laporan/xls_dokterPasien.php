<?php 

$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_dokter_pasien_'.$tgl.'.xls';

header('Content-type: application/excel');
header('Content-Disposition: attachment; filename='.$filename);
?>
<table cellpadding="4" >
  
  <thead>
    <tr>
      <th colspan="14">
      LAPORAN DOKTER <?php echo $dokter->FULLNAME;?><br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" >
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No <br>RM</th>
      <th width="15%">Nama Pasien</th>
      <th>Status Px</th>
      <th>Kelas</th>
     
      <th>TMR</th>
      <th>TKR</th>
      <th width="5%">Lm<br>Dirawat</th>
      <th>Visite</th>
      <th>Jml Visite<br>IRD</th>
      <th>Jml Visite<br>IRNA</th>
      <th>Konsul</th>
       <th>Jml<br>Konsul IRD</th>
       <th>Jml<br>Konsul IRNA</th>
      <th>Tindakan OP</th>
      <th>T. Penunjang</th>
      <th>Jumlah</th>
      
    </tr>
  </thead>
  <tbody>
   <?php 
 
    $i=1;

    $all_total_konsul = 0;
    $all_total_visite = 0;
    $all_total_tindakan = 0;
    $all_total_penunjang = 0;

    $kamarLabel = [];
    $index_kamar = 0;

    $kali_visite_ird = 0;
    $kali_visite_ird_u = 0;
    $kali_visite_ird_non_u = 0;

    $jml_visite_ird = 0;
    $jml_visite_ird_u = 0;
    $jml_visite_ird_non_u = 0;
    $kali_visite_kelas_1 = 0;
    $jml_visite_kelas_1 = 0;

    $kali_visite_kelas_1_u = 0;
    $jml_visite_kelas_1_u = 0;
    $kali_visite_kelas_1_non_u = 0;
    $jml_visite_kelas_1_non_u = 0;

    $kali_visite_kelas_2_u = 0;
    $jml_visite_kelas_2_u = 0;
    $kali_visite_kelas_2_non_u = 0;
    $jml_visite_kelas_2_non_u = 0;

    $kali_visite_kelas_3_u = 0;
    $jml_visite_kelas_3_u = 0;
    $kali_visite_kelas_3_non_u = 0;
    $jml_visite_kelas_3_non_u = 0;

    $kali_visite_kelas_2 = 0;
    $jml_visite_kelas_2 = 0;
    $kali_visite_kelas_3 = 0;
    $jml_visite_kelas_3 = 0;
    $kali_visite_vip = 0;
    $jml_visite_vip = 0;

    $kali_visite_vip_u = 0;
    $jml_visite_vip_u = 0;

    $kali_visite_vip_non_u = 0;
    $jml_visite_vip_non_u = 0;

    $countRaberKelas_1 = 0;
    $countRaberKelas_2 = 0;
    $countRaberKelas_3 = 0;
    $countRaberVIP = 0;

    $countRaberKelas_1_u = 0;
    $countRaberKelas_2_u = 0;
    $countRaberKelas_3_u = 0;
    $countRaberVIP_u = 0;

    $countRaberKelas_1_non_u = 0;
    $countRaberKelas_2_non_u = 0;
    $countRaberKelas_3_non_u = 0;
    $countRaberVIP_non_u = 0;

    $kali_konsul_ird = 0;
    $jml_konsul_ird = 0;
    $kali_konsul_kelas_1 = 0;
    $jml_konsul_kelas_1 = 0;
    $kali_konsul_kelas_2 = 0;
    $jml_konsul_kelas_2 = 0;
    $kali_konsul_kelas_3 = 0;
    $jml_konsul_kelas_3 = 0;

    $kali_konsul_ird_u = 0;
    $jml_konsul_ird_u = 0;
    $kali_konsul_kelas_1_u = 0;
    $jml_konsul_kelas_1_u = 0;
    $kali_konsul_kelas_2_u = 0;
    $jml_konsul_kelas_2_u = 0;
    $kali_konsul_kelas_3_u = 0;
    $jml_konsul_kelas_3_u = 0;

    $kali_konsul_ird_non_u = 0;
    $jml_konsul_ird_non_u = 0;
    $kali_konsul_kelas_1_non_u = 0;
    $jml_konsul_kelas_1_non_u = 0;
    $kali_konsul_kelas_2_non_u = 0;
    $jml_konsul_kelas_2_non_u = 0;
    $kali_konsul_kelas_3_non_u = 0;
    $jml_konsul_kelas_3_non_u = 0;

    $kali_konsul_vip = 0;
    $jml_konsul_vip = 0;

    $kali_konsul_vip_u = 0;
    $jml_konsul_vip_u = 0;
    $kali_konsul_vip_non_u = 0;
    $jml_konsul_vip_non_u = 0;




    $jml_tindakan_op = [];
    $jml_tindakan_op_vip = 0;
    $jml_tindakan_op_1 = 0;
    $jml_tindakan_op_2 = 0;
    $jml_tindakan_op_3 = 0;

    $jml_tindakan_op_u = [];
    $jml_tindakan_op_vip_u = 0;
    $jml_tindakan_op_1_u = 0;
    $jml_tindakan_op_2_u = 0;
    $jml_tindakan_op_3_u = 0;

    $jml_tindakan_op_non_u = [];
    $jml_tindakan_op_vip_non_u = 0;
    $jml_tindakan_op_1_non_u = 0;
    $jml_tindakan_op_2_non_u = 0;
    $jml_tindakan_op_3_non_u = 0;

    foreach($rawatInaps as $ri)
    {

        
        $biaya_visite_ird = 0;
        $biaya_visite_irna = 0;

        $biaya_konsul_ird = 0;
        $biaya_konsul_irna = 0;

        $total_konsul = 0;
        $total_visite = 0;
        $total_tindakan = 0;
        $total_penunjang = 0;
        $jumlah_visite_irna = 0;
        $jumlah_konsul_irna = 0;
        $jumlah_visite_ird = 0;
        $jumlah_konsul_ird = 0;

        $biaya_top_jm = $ri->sumOp($dokter->id_dokter);
        $biaya_top_jm_u = 0;
        $biaya_top_jm_non_u = 0;
        if($ri->jenisPasien->NamaGol == 'UMUM')
        {
          $biaya_top_jm_u = $ri->sumOp($dokter->id_dokter);
        }

        else
        {
          $biaya_top_jm_non_u = $ri->sumOp($dokter->id_dokter);
        }

        $total_tindakan = $biaya_top_jm;

        if($ri->kamar->kelas->kode_kelas == 'VIP'){
          $countRaberVIP += $ri->countRaber($dokter->id_dokter);
          $jml_tindakan_op_vip += $biaya_top_jm;
          $jml_tindakan_op[$ri->kamar->kelas_id] = [
            'nama' => 'VIP',
            'value' => $jml_tindakan_op_vip
          ];

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $countRaberVIP_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_vip_u += $biaya_top_jm_u;
            $jml_tindakan_op_u[$ri->kamar->kelas_id] = [
              'nama' => 'VIP',
              'value' => $jml_tindakan_op_vip_u
            ];
          }

          else{
            $countRaberVIP_non_u += $ri->countRaber($dokter->id_dokter);   
            $jml_tindakan_op_vip_non_u += $biaya_top_jm_non_u;
            $jml_tindakan_op_non_u[$ri->kamar->kelas_id] = [
              'nama' => 'VIP',
              'value' => $jml_tindakan_op_vip_non_u
            ];
          }
        }

        else if($ri->kamar->kelas->kode_kelas == 'KLS 1'){
          $countRaberKelas_1 += $ri->countRaber($dokter->id_dokter);
          $jml_tindakan_op_1 += $biaya_top_jm;
          $jml_tindakan_op[$ri->kamar->kelas_id] = [
            'nama' => 'Kelas 1',
            'value' => $jml_tindakan_op_1
          ];

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $countRaberKelas_1_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_1_u += $biaya_top_jm_u;
            $jml_tindakan_op_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 1',
              'value' => $jml_tindakan_op_1_u
            ];
          }

          else{
            $countRaberKelas_1_non_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_1_non_u += $biaya_top_jm_non_u;
            $jml_tindakan_op_non_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 1',
              'value' => $jml_tindakan_op_1_non_u
            ];
          }
        }

        else if($ri->kamar->kelas->kode_kelas == 'KLS 2'){
          $countRaberKelas_2 += $ri->countRaber($dokter->id_dokter);
          $jml_tindakan_op_2 += $biaya_top_jm;
          $jml_tindakan_op[$ri->kamar->kelas_id] = [
            'nama' => 'Kelas 2',
            'value' => $jml_tindakan_op_2
          ];

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $countRaberKelas_2_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_2_u += $biaya_top_jm_u;
            $jml_tindakan_op_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 2',
              'value' => $jml_tindakan_op_2_u
            ];
          }

          else{
            $countRaberKelas_2_non_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_2_non_u += $biaya_top_jm_non_u;
            $jml_tindakan_op_non_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 2',
              'value' => $jml_tindakan_op_2_non_u
            ];
          }
        }

        else if($ri->kamar->kelas->kode_kelas == 'KLS 3'){
          $countRaberKelas_3 += $ri->countRaber($dokter->id_dokter);
          $jml_tindakan_op_3 += $biaya_top_jm;
          $jml_tindakan_op[$ri->kamar->kelas_id] = [
            'nama' => 'Kelas 3',
            'value' => $jml_tindakan_op_3
          ];

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $countRaberKelas_3_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_3_u += $biaya_top_jm_u;
            $jml_tindakan_op_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 3',
              'value' => $jml_tindakan_op_3_u
            ];
          }

          else{
            $countRaberKelas_3_non_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_3_non_u += $biaya_top_jm_non_u;

            $jml_tindakan_op_non_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 3',
              'value' => $jml_tindakan_op_3_non_u
            ];
          }
        }
       
        
        foreach($ri->tRIVisiteDokters as $vi)
        {

            if(!empty($vi->id_dokter) && $vi->id_dokter == $dokter->id_dokter)
            {
                 if($vi->jenisVisite->kode_visite == 'VISITE')
                 {

                    if($vi->biaya_visite != 0)
                    {
                      $jumlah_visite_ird = $vi->jumlah_visite_ird;
                      $biaya_visite_ird = $biaya_visite_ird + $vi->biaya_visite;// * $vi->jumlah_visite_ird;
                      // $biaya_visite_irna = $biaya_visite_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                      $jml_visite_ird += $vi->biaya_visite;
                      $kali_visite_ird += $vi->jumlah_visite_ird;
                      if($ri->jenisPasien->NamaGol == 'UMUM')
                      {
                        $jml_visite_ird_u += $vi->biaya_visite;
                        $kali_visite_ird_u += $vi->jumlah_visite_ird;
                      }

                      else
                      {
                        $jml_visite_ird_non_u += $vi->biaya_visite;
                        $kali_visite_ird_non_u += $vi->jumlah_visite_ird;
                      }
                      
                    }
                 }

                 else if($vi->jenisVisite->kode_visite == 'KONSUL')
                 {
                    if($vi->biaya_visite != 0)
                    {
                      $jumlah_konsul_ird = $vi->jumlah_visite_ird;
                      $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite;// * $vi->jumlah_visite_ird;
                    
                   
                      $kali_konsul_ird += $vi->jumlah_visite_ird;
                      $jml_konsul_ird += $vi->biaya_visite;
                      if($ri->jenisPasien->NamaGol=='UMUM'){
                        $kali_konsul_ird_u += $vi->jumlah_visite_ird;
                        $jml_konsul_ird_u += $vi->biaya_visite;
                      }

                      else{
                        $kali_konsul_ird_non_u += $vi->jumlah_visite_ird;
                        $jml_konsul_ird_non_u += $vi->biaya_visite;
                      }
                    }
                    
                 }  
            }

            if(!empty($vi->id_dokter_irna) && $vi->id_dokter_irna == $dokter->id_dokter)
            {
                 if($vi->jenisVisite->kode_visite == 'VISITE')
                 {
                   if($vi->biaya_visite_irna != 0)
                   {
                    switch($ri->kamar->kelas_id){
                      case 1 :
                        $kali_visite_vip += $vi->jumlah_visite;
                        $jml_visite_vip += $vi->biaya_visite_irna;
                        if($ri->jenisPasien->NamaGol == 'UMUM')
                        {
                            $kali_visite_vip_u += $vi->jumlah_visite;
                            $jml_visite_vip_u += $vi->biaya_visite_irna;
                        }
                        else
                        {
                            $kali_visite_vip_non_u += $vi->jumlah_visite;
                            $jml_visite_vip_non_u += $vi->biaya_visite_irna;
                        }
                        break;
                      case 4 :
                       $kali_visite_kelas_1 += $vi->jumlah_visite;
                       $jml_visite_kelas_1 += $vi->biaya_visite_irna;
                        if($ri->jenisPasien->NamaGol == 'UMUM')
                        {
                            $kali_visite_kelas_1_u += $vi->jumlah_visite;
                            $jml_visite_kelas_1_u += $vi->biaya_visite_irna;
                        }
                        else
                        {
                            $kali_visite_kelas_1_non_u += $vi->jumlah_visite;
                            $jml_visite_kelas_1_non_u += $vi->biaya_visite_irna;
                        }
                       break;
                      case 5:
                        $kali_visite_kelas_2 += $vi->jumlah_visite;
                        $jml_visite_kelas_2 += $vi->biaya_visite_irna;
                        if($ri->jenisPasien->NamaGol == 'UMUM')
                        {
                            $kali_visite_kelas_2_u += $vi->jumlah_visite;
                            $jml_visite_kelas_2_u += $vi->biaya_visite_irna;
                        }
                        else
                        {
                            $kali_visite_kelas_2_non_u += $vi->jumlah_visite;
                            $jml_visite_kelas_2_non_u += $vi->biaya_visite_irna;
                        }
                        break;
                      case 6:
                        $kali_visite_kelas_3 += $vi->jumlah_visite;
                        $jml_visite_kelas_3 += $vi->biaya_visite_irna;
                        if($ri->jenisPasien->NamaGol == 'UMUM')
                        {
                            $kali_visite_kelas_3_u += $vi->jumlah_visite;
                            $jml_visite_kelas_3_u += $vi->biaya_visite_irna;
                        }
                        else
                        {
                            $kali_visite_kelas_3_non_u += $vi->jumlah_visite;
                            $jml_visite_kelas_3_non_u += $vi->biaya_visite_irna;
                        }
                        break;

                    }
                    $jumlah_visite_irna = $vi->jumlah_visite;
                    
                    // $biaya_visite_ird = $biaya_visite_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_visite_irna = $biaya_visite_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                  }
                 }

                 else if($vi->jenisVisite->kode_visite == 'KONSUL')
                 {
                    if($vi->biaya_visite_irna != 0)
                    {
                      $jumlah_konsul_irna = $vi->jumlah_visite;
                      // $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                      $biaya_konsul_irna = $biaya_konsul_irna + $vi->biaya_visite_irna ;//* $vi->jumlah_visite;
                      switch($ri->kamar->kelas_id){
                        case 1 :
                          $kali_konsul_vip += $vi->jumlah_visite;
                          $jml_konsul_vip += $vi->biaya_visite_irna;
                          if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_konsul_vip_u += $vi->jumlah_visite;
                          $jml_konsul_vip_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_konsul_vip_non_u += $vi->jumlah_visite;
                          $jml_konsul_vip_non_u += $vi->biaya_visite_irna;
                         }
                          break;
                        case 4 :
                         $kali_konsul_kelas_1 += $vi->jumlah_visite;
                         $jml_konsul_kelas_1 += $vi->biaya_visite_irna;
                         if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_konsul_kelas_1_u += $vi->jumlah_visite;
                          $jml_konsul_kelas_1_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_konsul_kelas_1_non_u += $vi->jumlah_visite;
                         $jml_konsul_kelas_1_non_u += $vi->biaya_visite_irna;
                         }
                         break;
                        case 5:
                          $kali_konsul_kelas_2 += $vi->jumlah_visite;
                          $jml_konsul_kelas_2 += $vi->biaya_visite_irna;
                           if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_konsul_kelas_2_u += $vi->jumlah_visite;
                          $jml_konsul_kelas_2_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_konsul_kelas_2_non_u += $vi->jumlah_visite;
                         $jml_konsul_kelas_2_non_u += $vi->biaya_visite_irna;
                         }
                          break;
                        case 6:
                          $kali_konsul_kelas_3 += $vi->jumlah_visite;
                          $jml_konsul_kelas_3 += $vi->biaya_visite_irna;
                           if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_konsul_kelas_3_u += $vi->jumlah_visite;
                          $jml_konsul_kelas_3_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_konsul_kelas_3_non_u += $vi->jumlah_visite;
                         $jml_konsul_kelas_3_non_u += $vi->biaya_visite_irna;
                         }
                          break;

                      }
                    }
                 }  
            }
        }

        $total_konsul = $biaya_konsul_ird + $biaya_konsul_irna;
        $total_visite = $biaya_visite_ird + $biaya_visite_irna;

        $all_total_visite = $all_total_visite + $total_visite;
        $all_total_konsul = $all_total_konsul + $total_konsul;


   
        $biaya_tindakan_anas = 0;
        
        foreach($ri->trRawatInapTops as $vi)
        {

            // if(!empty($vi->id_dokter_jm) && $vi->id_dokter_jm == $dokter->id_dokter)
            // {
            //     $biaya_tindakan_jm = $biaya_tindakan_jm + $vi->biaya_irna * $vi->jumlah_tindakan;
            // }

            if(!empty($vi->id_dokter_anas) && $vi->id_dokter_anas == $dokter->id_dokter)
            {
                $biaya_tindakan_anas = $biaya_tindakan_anas + $vi->biaya_irna * $vi->jumlah_tindakan;
            }
        }

        $total_tindakan += $biaya_tindakan_anas;

        
        

        $biaya_penunjang = 0;
        foreach($ri->trRawatInapPenunjangs as $vi)
        {
            if(!empty($vi->dokter_id) && $vi->dokter_id == $dokter->id_dokter)
          { 
               if($vi->penunjang->nama_tindakan == 'USG' ||
                  $vi->penunjang->nama_tindakan == 'PA' ||
                  $vi->penunjang->nama_tindakan == 'EKG')
               {
                  $biaya_penunjang += $vi->biaya_irna;
               }
            }
        }

        $total_penunjang += $biaya_penunjang;
        $all_total_penunjang += $total_penunjang;

        // $biaya_top_ja = 0;
        // foreach($ri->trRawatInapTopJa as $vi)
        // {
        //   if(!empty($vi->id_dokter) && $vi->id_dokter == $dokter->id_dokter)
        //   { 
        //       $biaya_top_ja = $biaya_top_ja + $vi->nilai;
        //   } 
        // }

        // $total_tindakan = $total_tindakan + $biaya_top_ja;

         if($total_visite == 0 && $total_konsul == 0 && $total_tindakan == 0 && $total_penunjang == 0) continue;

        $all_total_tindakan = $all_total_tindakan + $total_tindakan;

        $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }
   
        if(!in_array(strtoupper($ri->kamar->kamarMaster->nama_kamar), $kamarLabel)){
          $kamarLabel[] = strtoupper($ri->kamar->kamarMaster->nama_kamar);
          echo '<tr><td colspan="14">'.$kamarLabel[$index_kamar].'</td></tr>';
          $index_kamar++;

          
        }
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
     
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td width="5%"><?php echo $selisih_hari;?></td>
      
      <td>
      <?php 
        echo ($total_visite);
              ?></td>
       <td><?=$jumlah_visite_ird;?></td>
        <td><?=$jumlah_visite_irna;?></td>
      <td>
         <?php 
        echo ($total_konsul);
              ?>
      </td>
      <td><?=$jumlah_konsul_ird;?></td>
      <td><?=$jumlah_konsul_irna;?></td>
      <td><?php 
        echo ($total_tindakan);
              ?>
                
              </td>
     <td><?php 
        echo ($total_penunjang);
              ?>
                
              </td>
      <td><?php 
        echo ($total_tindakan + $total_konsul + $total_visite + $total_penunjang);
              ?></td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td> <strong>
      <?php 
       echo ($all_total_visite);
      ?></strong></td>
      <td>&nbsp;</td>
      <td>
     
      </td>
      <td><strong>
        <?php 
       echo ($all_total_konsul);
      ?></strong>
      </td>
      <td><strong>
        <?php 
       echo ($all_total_tindakan);
      ?></strong>
      </td>
      <td><strong>
        <?php 
       echo ($all_total_penunjang);
      ?></strong>
      </td>
      <td></td>
      <td></td>
      <td><strong>
        <?php 
       echo ($all_total_visite + $all_total_konsul + $all_total_tindakan + $all_total_penunjang);
      ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>
<table width="100%">
<tr>
  <td width="33.3%">
    <table  border="1" width="100%">
  <tr>
  <th>VISITE</th>
  <th>Kali</th>
  <th>Rupiah</th>
  <th>Total Input</th>
</tr>
<?php 
 $biaya_visite_kelas_1 = JenisVisite::getBiaya('Visite Dokter Ahli',4);
  $countVisiteKelas1 = $kali_visite_kelas_1 + ($countRaberKelas_1 / 2);
  $countVisiteKelas1_u = $kali_visite_kelas_1_u + ($countRaberKelas_1_u / 2);
  $countVisiteKelas1_non_u = $kali_visite_kelas_1_non_u + ($countRaberKelas_1_non_u / 2);

  $biaya_visite_kelas_2 = JenisVisite::getBiaya('Visite Dokter Ahli',5);
  $countVisiteKelas2 = $kali_visite_kelas_2 + ($countRaberKelas_2 / 2);
  $countVisiteKelas2_u = $kali_visite_kelas_2_u + ($countRaberKelas_2_u / 2);
  $countVisiteKelas2_non_u = $kali_visite_kelas_2_non_u + ($countRaberKelas_2_non_u / 2);

  $biaya_visite_kelas_3 = JenisVisite::getBiaya('Visite Dokter Ahli',6);
  $countVisiteKelas3 = $kali_visite_kelas_3 + ($countRaberKelas_3 / 2);
  $countVisiteKelas3_u = $kali_visite_kelas_3_u + ($countRaberKelas_3_u / 2);
  $countVisiteKelas3_non_u = $kali_visite_kelas_3_non_u + ($countRaberKelas_3_non_u / 2);

  $biaya_visite_vip = JenisVisite::getBiaya('Visite Dokter Ahli',1);
  $countVisiteVIP = $kali_visite_vip + ($countRaberVIP / 2);
  $countVisiteVIP_u = $kali_visite_vip_u + ($countRaberVIP_u / 2);
  $countVisiteVIP_non_u = $kali_visite_vip_non_u + ($countRaberVIP_non_u / 2);

  $biaya_visite_ird = JenisVisite::getBiaya('Visite IRD',1);

  $total_count_biayaIrd = $biaya_visite_ird * $kali_visite_ird;
  $total_count_biaya1 = $biaya_visite_kelas_1 * $countVisiteKelas1;
  $total_count_biaya2 = $biaya_visite_kelas_2 * $countVisiteKelas2;
  $total_count_biaya3 = $biaya_visite_kelas_3 * $countVisiteKelas3;
  $total_count_biayaVip = $biaya_visite_vip * $countVisiteVIP;

  $total_visite_riil = $total_count_biayaIrd + $total_count_biayaVip + $total_count_biaya1 + $total_count_biaya2 + $total_count_biaya3;
  

  // Hitungan perkalian/ BUKAN inputan operator
  $jml_visite_ird = $kali_visite_ird * $biaya_visite_ird;
  $jml_visite_ird_u = $kali_visite_ird_u * $biaya_visite_ird;
  $jml_visite_ird_non_u = $kali_visite_ird_non_u * $biaya_visite_ird;

  $jml_visite_kelas_1 = $countVisiteKelas1 * $biaya_visite_kelas_1;
  $jml_visite_kelas_1_u = $countVisiteKelas1_u * $biaya_visite_kelas_1;
  $jml_visite_kelas_1_non_u = $countVisiteKelas1_non_u * $biaya_visite_kelas_1;

  $jml_visite_kelas_2 = $countVisiteKelas2 * $biaya_visite_kelas_2;
  $jml_visite_kelas_2_u = $countVisiteKelas2_u * $biaya_visite_kelas_2;
  $jml_visite_kelas_2_non_u = $countVisiteKelas2_non_u * $biaya_visite_kelas_2;

  $jml_visite_kelas_3 = $countVisiteKelas3 * $biaya_visite_kelas_3;
  $jml_visite_kelas_3_u = $countVisiteKelas3_u * $biaya_visite_kelas_3;
  $jml_visite_kelas_3_non_u = $countVisiteKelas3_non_u * $biaya_visite_kelas_3;
  
  $jml_visite_vip = $countVisiteVIP * $biaya_visite_vip;
  $jml_visite_vip_u = $countVisiteVIP_u * $biaya_visite_vip;
  $jml_visite_vip_non_u = $countVisiteVIP_non_u * $biaya_visite_vip;




  $total_jasa_input_123 = $jml_visite_ird + $jml_visite_kelas_1 + $jml_visite_kelas_2 + $jml_visite_kelas_3;
  $total_jasa_input_123_u = $jml_visite_ird_u + $jml_visite_kelas_1_u + $jml_visite_kelas_2_u + $jml_visite_kelas_3_u;
  $total_jasa_input_123_non_u = $jml_visite_ird_non_u + $jml_visite_kelas_1_non_u + $jml_visite_kelas_2_non_u + $jml_visite_kelas_3_non_u;

  $total_jasa_input_vip = $jml_visite_vip;
  $total_jasa_input_vip_u = $jml_visite_vip_u;
  $total_jasa_input_vip_non_u = $jml_visite_vip_non_u;

  $total_visite_input = $jml_visite_ird + $jml_visite_kelas_1 + $jml_visite_kelas_2 + $jml_visite_kelas_3 + $jml_visite_vip;
?>
<tr>
  <td>IRD</td>
  <td><?=$kali_visite_ird;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_visite_ird) ;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_ird) ;?></td>
</tr>
<tr>
  <td>Kelas 1</td>
  <td><?=$countVisiteKelas1;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_visite_kelas_1);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_kelas_1) ;?></td>
  
</tr>
<tr>
  <td>Kelas 2</td>
 <td><?=$countVisiteKelas2;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_visite_kelas_2);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_kelas_2) ;?></td>
  
</tr>
<tr>
  <td>Kelas 3</td>
  <td><?=$countVisiteKelas3;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_visite_kelas_3);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_kelas_3) ;?></td>
  
</tr>
<tr>
  <td>VIP</td>
 <td><?=$countVisiteVIP;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_visite_vip);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_vip) ;?></td>
  
</tr>
<tr>
  <td>Total</td>
 <td><?=$countVisiteVIP + $countVisiteKelas3 + $countVisiteKelas2 + $countVisiteKelas1;?></td>
  <td style="text-align: right"></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($total_visite_input);?></td>
</tr>
</table>
  </td>
  <td width="33.3%">
    <table  border="1" width="100%">
  <tr>
  <th>KONSUL</th>
  <th>Kali</th>
  <th>Rupiah</th>
  <th>Total Input</th>
</tr>
<?php 
    $biaya_konsul_kelas_1 = JenisVisite::getBiaya('Konsul Dokter Ahli',4);
  $countkonsulKelas1 = $kali_konsul_kelas_1;
  $countkonsulKelas1_u = $kali_konsul_kelas_1_u;
  $countkonsulKelas1_non_u = $kali_konsul_kelas_1_non_u;

  $biaya_konsul_kelas_2 = JenisVisite::getBiaya('Konsul Dokter Ahli',5);
  $countkonsulKelas2 = $kali_konsul_kelas_2 ;
  $countkonsulKelas2_u = $kali_konsul_kelas_2_u;
  $countkonsulKelas2_non_u = $kali_konsul_kelas_2_non_u;

  $biaya_konsul_kelas_3 = JenisVisite::getBiaya('Konsul Dokter Ahli',6);
  $countkonsulKelas3 = $kali_konsul_kelas_3 ;
  $countkonsulKelas3_u = $kali_konsul_kelas_3_u;
  $countkonsulKelas3_non_u = $kali_konsul_kelas_3_non_u;

  $biaya_konsul_vip = JenisVisite::getBiaya('Konsul Dokter Ahli',1);
  $countkonsulVIP = $kali_konsul_vip ;
  $countkonsulVIP_u = $kali_konsul_vip_u ;
  $countkonsulVIP_non_u = $kali_konsul_vip_non_u ;

  $biaya_konsul_ird = JenisVisite::getBiaya('Konsul IRD',1);

  $jml_konsul_ird = $kali_konsul_ird * $biaya_konsul_ird;
  $jml_konsul_ird_u = $kali_konsul_ird_u * $biaya_konsul_ird;
  $jml_konsul_ird_non_u = $kali_konsul_ird_non_u * $biaya_konsul_ird;

  $jml_konsul_kelas_1 = $countkonsulKelas1 * $biaya_konsul_kelas_1;
  $jml_konsul_kelas_1_u = $kali_konsul_kelas_1_u * $biaya_konsul_kelas_1;
  $jml_konsul_kelas_1_non_u = $countkonsulKelas1_non_u * $biaya_konsul_kelas_1;

  $jml_konsul_kelas_2 = $countkonsulKelas2 * $biaya_konsul_kelas_2;
  $jml_konsul_kelas_2_u = $kali_konsul_kelas_2_u * $biaya_konsul_kelas_2;
  $jml_konsul_kelas_2_non_u = $countkonsulKelas2_non_u * $biaya_konsul_kelas_2;

  $jml_konsul_kelas_3 = $countkonsulKelas3 * $biaya_konsul_kelas_3;
  $jml_konsul_kelas_3_u = $kali_konsul_kelas_3_u * $biaya_konsul_kelas_3;
  $jml_konsul_kelas_3_non_u = $countkonsulKelas3_non_u * $biaya_konsul_kelas_3;

  $total_count_biayaIrd = $biaya_visite_ird * $kali_visite_ird;
  $total_count_biayaIrd_u = $biaya_visite_ird * $kali_visite_ird_u;
  $total_count_biayaIrd_non_u = $biaya_visite_ird * $kali_visite_ird_non_u;

  $total_count_biaya1 = $biaya_konsul_kelas_1 * $countkonsulKelas1;
  $total_count_biaya2 = $biaya_konsul_kelas_2 * $countkonsulKelas2;
  $total_count_biaya3 = $biaya_konsul_kelas_3 * $countkonsulKelas3;
  $total_count_biayaVip = $biaya_konsul_vip * $countkonsulVIP;

  $total_count_biaya1_u = $biaya_konsul_kelas_1 * $countkonsulKelas1_u;
  $total_count_biaya2_u = $biaya_konsul_kelas_2 * $countkonsulKelas2_u;
  $total_count_biaya3_u = $biaya_konsul_kelas_3 * $countkonsulKelas3_u;
  $total_count_biayaVip_u = $biaya_konsul_vip * $countkonsulVIP_u;

  $total_count_biaya1_non_u = $biaya_konsul_kelas_1 * $countkonsulKelas1_non_u;
  $total_count_biaya2_non_u = $biaya_konsul_kelas_2 * $countkonsulKelas2_non_u;
  $total_count_biaya3_non_u = $biaya_konsul_kelas_3 * $countkonsulKelas3_non_u;
  $total_count_biayaVip_non_u = $biaya_konsul_vip * $countkonsulVIP_non_u;

  $total_konsul_riil = $jml_konsul_ird + $total_count_biayaVip + $total_count_biaya1 + $total_count_biaya2 + $total_count_biaya3;
  $total_konsul_input = $jml_konsul_ird +$jml_konsul_kelas_1+$jml_konsul_kelas_2+$jml_konsul_kelas_3+$jml_konsul_vip;
  $total_konsul_input_u = $jml_konsul_ird_u +$jml_konsul_kelas_1_u+$jml_konsul_kelas_2_u+$jml_konsul_kelas_3_u+$jml_konsul_vip_u;
  $total_konsul_input_non_u = $jml_konsul_ird_non_u +$jml_konsul_kelas_1_non_u+$jml_konsul_kelas_2_non_u+$jml_konsul_kelas_3_non_u+$jml_konsul_vip_non_u;

  $total_jasa_input_123 += ($jml_konsul_ird +$jml_konsul_kelas_1+$jml_konsul_kelas_2+$jml_konsul_kelas_3);
  $total_jasa_input_123_u += ($jml_konsul_ird_u +$jml_konsul_kelas_1_u+$jml_konsul_kelas_2_u+$jml_konsul_kelas_3_u);
  $total_jasa_input_123_non_u += ($jml_konsul_ird_non_u +$jml_konsul_kelas_1_non_u+$jml_konsul_kelas_2_non_u+$jml_konsul_kelas_3_non_u);

  $total_jasa_input_vip += $jml_konsul_vip;
  $total_jasa_input_vip_u += $jml_konsul_vip_u;
  $total_jasa_input_vip_non_u += $jml_konsul_vip_non_u;
?>
<tr>
  <td>IRD</td>
  <td><?=$kali_konsul_ird;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_konsul_ird) ;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_ird) ;?></td>
</tr>
<tr>
  <td>Kelas 1</td>
  <td><?=$countkonsulKelas1;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_konsul_kelas_1);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_kelas_1) ;?></td>
 
</tr>
<tr>
  <td>Kelas 2</td>
 <td><?=$countkonsulKelas2;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_konsul_kelas_2);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_kelas_2) ;?></td>
 
</tr>
<tr>
  <td>Kelas 3</td>
  <td><?=$countkonsulKelas3;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_konsul_kelas_3);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_kelas_3) ;?></td>

</tr>
<tr>
  <td>VIP</td>
 <td><?=$countkonsulVIP;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($biaya_konsul_vip);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_vip) ;?></td>
</tr>
<tr>
  <td>Total</td>
 <td><?=$countkonsulVIP + $countkonsulKelas3 + $countkonsulKelas2 + $countkonsulKelas1;?></td>
  <td style="text-align: right"></td>
   <td style="text-align: right"><?=Yii::app()->helper->formatBulat($total_konsul_input);?></td>
</tr>
</table>
  </td>
  <td width="33.3%">
    <table border="1" width="100%">
  <tr>
  <th>TINDAKAN</th>
  <th>Total </th>
  
</tr>
<?php
   usort($jml_tindakan_op, function($a, $b) {
      return $a['nama'] <=> $b['nama'];
  });

  usort($jml_tindakan_op_u, function($a, $b) {
      return $a['nama'] <=> $b['nama'];
  });

  usort($jml_tindakan_op_non_u, function($a, $b) {
      return $a['nama'] <=> $b['nama'];
  });
  $sum_op123 = 0;
  $sum_opVIP = 0;
  $sum_op = 0;

  $sum_op123_u = 0;
  $sum_opVIP_u = 0;
  $sum_op_u = 0;

  $sum_op123_non_u = 0;
  $sum_opVIP_non_u = 0;
  $sum_op_non_u = 0;
  foreach($jml_tindakan_op as $item)
  {
    if($item['nama'] == 'Kelas 1' || $item['nama'] == 'Kelas 2' || $item['nama'] == 'Kelas 3')
      $sum_op123 += $item['value'];
    else if($item['nama'] == 'VIP')
      $sum_opVIP += $item['value'];
    
    $sum_op += $item['value'];
?>
<tr>
  <td><?=$item['nama'];?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($item['value']);?></td>
  
</tr>
<?php 
}

foreach($jml_tindakan_op_u as $item)
{
  if($item['nama'] == 'Kelas 1' || $item['nama'] == 'Kelas 2' || $item['nama'] == 'Kelas 3')
    $sum_op123_u += $item['value'];
  else if($item['nama'] == 'VIP')
    $sum_opVIP_u += $item['value'];
  
  $sum_op_u += $item['value'];
}


foreach($jml_tindakan_op_non_u as $item)
{
  if($item['nama'] == 'Kelas 1' || $item['nama'] == 'Kelas 2' || $item['nama'] == 'Kelas 3')
    $sum_op123_non_u += $item['value'];
  else if($item['nama'] == 'VIP')
    $sum_opVIP_non_u += $item['value'];
  
  $sum_op_non_u += $item['value'];
}


$total_jasa_input_123 += $sum_op123;
$total_jasa_input_vip += $sum_opVIP;

$total_jasa_input_123_u += $sum_op123_u;
$total_jasa_input_vip_u += $sum_opVIP_u;

$total_jasa_input_123_non_u += $sum_op123_non_u;
$total_jasa_input_vip_non_u += $sum_opVIP_non_u;
?>

  <td>Total</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatBulat($sum_op);?></td>
</tr>
</table>
  </td>
</tr>
</table>
 <?php

    $jml_visite_kelas_123_u = $jml_visite_kelas_1_u + $jml_visite_kelas_2_u + $jml_visite_kelas_3_u + $jml_visite_ird_u;

    $jml_konsul_kelas_123_u = $jml_konsul_kelas_1_u + $jml_konsul_kelas_2_u + $jml_konsul_kelas_3_u + $jml_konsul_ird_u;

    $jml_visite_kelas_123_non_u = $jml_visite_kelas_1_non_u + $jml_visite_kelas_2_non_u + $jml_visite_kelas_3_non_u + $jml_visite_ird_non_u;

    $jml_konsul_kelas_123_non_u = $jml_konsul_kelas_1_non_u + $jml_konsul_kelas_2_non_u + $jml_konsul_kelas_3_non_u + $jml_konsul_ird_non_u;

    $total_jaspel123 = $jml_visite_kelas_1 + $jml_visite_kelas_2 + $jml_visite_kelas_3 + 
    $jml_konsul_kelas_1 +$jml_konsul_kelas_2 +$jml_konsul_kelas_3+ $sum_op123;
    $total_jaspel123_u = $jml_visite_kelas_123_u + $jml_konsul_kelas_123_u + $sum_op123_u;
    $total_jaspel123_non_u = $jml_visite_kelas_123_non_u + $jml_konsul_kelas_123_non_u + $sum_op123_non_u;


    $jaspel123 = $total_jaspel123 * 0.6;
    $jaspel123_u = $total_jaspel123_u * 0.6;
    $jaspel123_non_u = $total_jaspel123_non_u * 0.6;
    
    $total_jaspelVIP = $jml_visite_vip + $jml_konsul_vip + $sum_opVIP;
    $total_jaspelVIP_u = $jml_visite_vip_u + $jml_konsul_vip_u + $sum_opVIP_u;
    $total_jaspelVIP_non_u = $jml_visite_vip_non_u + $jml_konsul_vip_non_u + $sum_opVIP_non_u;

    $jaspelVIP = $total_jaspelVIP * 0.8;
    $jaspelVIP_u = $total_jaspelVIP_u * 0.8;
    $jaspelVIP_non_u = $total_jaspelVIP_non_u * 0.8;



    $pajak123 = $jaspel123 * 0.05;
    $pajak123_u = $jaspel123_u * 0.05;
    $pajak123_non_u = $jaspel123_non_u * 0.05;
    $pajakVIP = $jaspelVIP * 0.05;
    $pajakVIP_u = $jaspelVIP_u * 0.05;
    $pajakVIP_non_u = $jaspelVIP_non_u * 0.05;

    $bersih123 = $jaspel123 - $pajak123;
    $bersih123_u = $jaspel123_u - $pajak123_u;
    $bersih123_non_u = $jaspel123_non_u - $pajak123_non_u;
    $bersihVIP = $jaspelVIP - $pajakVIP;
    $bersihVIP_u = $jaspelVIP_u - $pajakVIP_u;
    $bersihVIP_non_u = $jaspelVIP_non_u - $pajakVIP_non_u;
    ?>

<table width="100%" border="1">
    <tr>
      <th>No</th>
      <th>Uraian</th>
      <th>Visite</th>
      <th>Konsul</th>
      <th>Tindakan</th>
      <th>Total</th>
      <th>% Jaspel</th>
      <th>Jaspel</th>
      <th>Pajak 5%</th>
      <th>Terima Bersih</th>
    </tr>
   
    <tr>
      <td>1</td>
      <td colspan="5">Pembagian Jaspel Kelas 1,2, dan 3</td>
    </tr>
    <tr>
      <td></td>
      <td>a. BPJS/DA/Asuransi Lain</td>
      
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_kelas_123_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_kelas_123_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($sum_op123_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($total_jaspel123_non_u);?></td>
      <td style="text-align: right">60 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jaspel123_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($pajak123_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($bersih123_non_u);?></td>
    </tr>
    <tr>
       <td></td>
      <td>b. UMUM</td>
      
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_kelas_123_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_kelas_123_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($sum_op123_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($total_jaspel123_u);?></td>
      <td style="text-align: right">60 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jaspel123_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($pajak123_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($bersih123_u);?></td>
    </tr>
     <tr>
      <td></td>
      <td>SUBTOTAL</td>
      
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_kelas_123_non_u + $jml_visite_kelas_123_u);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_kelas_123_non_u + $jml_konsul_kelas_123_u);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatBulat($sum_op123_non_u + $sum_op123_u);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatBulat($total_jaspel123_non_u + $total_jaspel123_u);?></td>
      <td style="text-align: right">60 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jaspel123_u + $jaspel123_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($pajak123_u + $pajak123_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($bersih123_u + $bersih123_non_u);?></td>
    </tr>
    <tr>
      <td>2</td>
      <td colspan="5">Pembagian Jaspel Kelas VIP</td>
      
      
    </tr>
     <tr>
      <td></td>
      <td>a. BPJS/DA/Asuransi Lain</td>
      
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_vip_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_vip_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($sum_opVIP_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($total_jaspelVIP_non_u);?></td>
      <td style="text-align: right">80 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jaspelVIP_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($pajakVIP_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($bersihVIP_non_u);?></td>
    </tr>
    <tr>
      <td></td>
      <td>b. UMUM</td>
      
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_vip_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_vip_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($sum_opVIP_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($total_jaspelVIP_u);?></td>
      <td style="text-align: right">80 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_vip_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($pajakVIP_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($bersihVIP_u);?></td>
    </tr>
    <tr>
      <td></td>
      <td>SUBTOTAL</td>
      
       <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_visite_vip_non_u + $jml_visite_vip_u);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatBulat($jml_konsul_vip_non_u + $jml_konsul_vip_u);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatBulat($sum_opVIP_non_u + $sum_opVIP_u);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatBulat($total_jaspelVIP_u +$total_jaspelVIP_non_u);?></td>
     <td style="text-align: right">80 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jaspelVIP_u + $jaspelVIP_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($pajakVIP_u + $pajakVIP_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($bersihVIP_u + $bersihVIP_non_u);?></td>
    </tr>
    <?php 
    $sumAllVisite = $jml_visite_kelas_123_u + $jml_visite_kelas_123_non_u + $jml_visite_vip_u + $jml_visite_vip_non_u;
    $sumAllKonsul = $jml_konsul_kelas_123_u + $jml_konsul_kelas_123_non_u + $jml_konsul_vip_u + $jml_konsul_vip_non_u;
    $sumAllOP = $sum_op123_u + $sum_op123_non_u + $sum_opVIP_u + $sum_opVIP_non_u;
    ?>
     <tr>
      <td></td>
      <td><b>TOTAL</b></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatBulat($sumAllVisite);?></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatBulat($sumAllKonsul);?></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatBulat($sumAllOP);?></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatBulat($sumAllKonsul + $sumAllVisite + $sumAllOP);?></td>
      <td>a</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($jaspel123_u + $jaspelVIP_u + $jaspel123_non_u + $jaspelVIP_non_u);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($pajak123_u + $pajakVIP_u + $pajak123_non_u + $pajakVIP_non_u);?></td>
      <td style="text-align: right"><b><?=Yii::app()->helper->formatBulat($bersih123_u + $bersihVIP_u + $bersih123_non_u + $bersihVIP_non_u);?></b></td>
    </tr>
  </table>