<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Laporan','url'=>Yii::app()->createUrl('trRawatInap/laporan')),
                            array('name' => 'Laporan IRD','url'=>Yii::app()->createUrl('laporan/ird')),
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="container-fluid" >
    <div class="row-fluid">
        
        
        <!--/span-->
        <div class="span12" >
            <div class="row-fluid">
           
                <div class="navbar">
                    <div class="navbar-inner">
                        <?php $this->widget('application.components.BreadCrumb', array(
                          'links' => array(
                            array('name' => 'Laporan','url'=>Yii::app()->createUrl('trRawatInap/laporan')),
                            array('name' => 'Laporan IRD','url'=>Yii::app()->createUrl('laporan/ird')),
                            
                          ),
                          'delimiter' => ' || ', // if you want to change it
                        )); ?>                       
                    </div>
                </div>
            </div>
            
          
           
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Laporan IRD</div>
                      <div class="pull-right"> </div> 
                    </div>
                    <div class="block-content collapse in">
                          <?php 

 $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();

              
              ?>
              <fieldset>
        
 </fieldset>
<div class="control-group" align="center">
  <div class="controls">

      <div class="span12">
<?php 
echo CHtml::link('<span class="label label-success" style="padding:5px 10px">Ekspor ke XLS </span>',array('laporan/irdPasien','kid'=>$kamar->id_kamar,'t1'=>$tanggal_awal,'t2'=>$tanggal_akhir,'tipe'=>$tipe,'print'=>2),array('style'=>'color:#fff','target'=>'_blank'));
?>
</div>
</div>
</div>
<?php

  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
    $this->renderPartial('_irdPasien', 
    array(
        'model'=>$model,
          'rawatInaps' => $rawatInaps,
          'kamar' => $kamar,
          'tipe' => $tipe,
          'tanggal_awal' => $tanggal_awal,
          'tanggal_akhir' => $tanggal_akhir,
        )
    ); 

  }
?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>