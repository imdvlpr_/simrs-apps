<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
   array('name' => 'Laporan','url'=>Yii::app()->createUrl('trRawatInap/laporan')),
                            array('name' => 'Laporan Dokter','url'=>Yii::app()->createUrl('laporan/dokter')),
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="row" >
    <div class="col-xs-12">
        
          
         
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
              
              ?>
            
              <?php 
// echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    
     
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'name' => 'a',
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'id'=>'datestart',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'name' => 'ab',
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'id'=>'dateend',
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="button" id="lihat">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>
          <img style="display:none" id="loading" src="<?=Yii::app()->baseUrl;?>/images/loading.gif"/>
          <?php 
        //    if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR)){
        //    $link = array('laporan/operasi','tgl_awal'=>$model->TANGGAL_AWAL,'tgl_akhir'=>$model->TANGGAL_AKHIR,'print'=>2);
        //     echo CHtml::link('Ekspor XLS Laporan Operasi',$link,array('class'=>'btn btn-success'));
        // }
          ?>
  </div>
      </div>
        
      
   <?php
       
  $this->endWidget();
  ?>


<?php 
  // if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  // { 
?>
<table class="table table-bordered" id="table_dokter">
  
  <thead>
    <tr>
      <th>No</th>
      <th>Nama Dokter</th>
      <th>Jenis</th>
      <th>Tindakan</th>
      
    </tr>
  </thead>
  <tbody>
 
  </tbody>
</table>
<script type="text/javascript">
  $(document).ready(function(){

    $('#lihat').click(function(){

        var ds = $('#datestart').val();
        var de = $('#dateend').val();
        
        if(ds == '') {
          alert('Date Start cannot be empty');
          return;
        }

        if(de == '') {
          alert('Date End cannot be empty');
          return;
        }
        ds = ds.split("/");
        ds = ds[2]+'-'+ds[1]+'-'+ds[0];
        de = de.split("/");
        de = de[2]+'-'+de[1]+'-'+de[0];
        $.ajax({
        type : 'POST',
        url : '<?=Yii::app()->createUrl("laporan/ajaxDokter")?>',
        async : true,
        crossDomain: true,
  // xhrFields: {
  //         withCredentials: true
  //     },
      // headers : {"Access-Control-Allow-Origin":"*"},
        data : 'sd='+ds+'&ed='+de,
        error : function(rs){
          $('#loading').hide();

        },
        beforeSend: function(){
          $('#loading').show();
        },
        success : function(data){
          console.log(data);
          var data = $.parseJSON(data);
          var i = 0;
          var row = '';
          $('#table_dokter > tbody').empty();
          
          $('#loading').hide();
          $.each(data,function(i,obj){
             if (obj.id > 0){
               
             
               i++;
               row += '<tr>';
               row += '<td>'+i+'</td>';
               row += '<td>'+obj.nama+'</td>';
               row += '<td>'+obj.jenis+'</td>';
               row += '<td>';
               row += '<a href="<?=Yii::app()->createUrl('laporan/dokterPasien');?>&drid='+obj.id+'&t1='+ds+'&t2='+de+'"><button class="btn btn-info btn-sm" >Lihat Laporan Pasien </button></a>'
               row += '</td>';
               row += '</tr>';
             }
             else{
                row += '<tr>';
               row += '<td colspan="4" style="text-align:center"><strong>'+obj.nama+'</strong></td>';
               
               row += '</tr>';
             }
          });


          $('#table_dokter > tbody').append(row);
        }
      });
    });

    
  });
</script>
<?php
  // }
?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>

