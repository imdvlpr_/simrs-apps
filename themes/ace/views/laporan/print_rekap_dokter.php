<table cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th>
      LAPORAN REKAPITULASI JASA DOKTER <?php echo $jenis;?><br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="25%">Nama Dokter</th>
      <th width="10%">VISITE</th>
      <th width="10%">KONSUL</th>
      <th width="10%">TINDAKAN</th>
      <th width="10%">JUMLAH</th>
      
    </tr>
  </thead>
  <tbody>
  <?php 

     $i=1;

    $total_tindakan = 0;
    $i = 1;

    $total_visite = 0;
    $total_konsul = 0;
    
    foreach(DmDokter::model()->findAll() as $item)
    {

      
      $tindakan = (object)$model->searchRekapDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      // print_r($tindakan);exit;
      $n_tindakan = !empty($tindakan) ? $tindakan->nilai : 0;
      
      $visite = $model->searchVisiteDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      $n_visite = !empty($visite) ? $visite->nilai : 0;

      $konsul = $model->searchKonsulDokter($jenis,$item->id_dokter, $model->TANGGAL_AWAL, $model->TANGGAL_AKHIR);
      $n_konsul = !empty($konsul) ? $konsul->nilai : 0;

      $total_tindakan += $n_tindakan;
      $total_visite += $n_visite;
      $total_konsul += $n_konsul;
 
      if($n_tindakan == 0 && $n_visite == 0 && $n_konsul == 0) continue;
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
      <td width="25%"><?php echo $item->nama_dokter;?></td>
      <td style="text-align: right;" width="10%">  <?php 
        if($n_visite != 0)
         echo Yii::app()->helper->formatRupiah($n_visite);
   
              ?></td>
      <td style="text-align: right;" width="10%"> <?php 
      if($n_konsul != 0)
         echo Yii::app()->helper->formatRupiah($n_konsul);
      
              ?></td>
      <td style="text-align: right;" width="10%">

        <?php 
        if($n_tindakan != 0)
         echo Yii::app()->helper->formatRupiah($tindakan->nilai);
     
              ?>
              </td>
     <!--  <td>0</td> -->
      <td style="text-align: right;" width="10%">
                    <?php 
echo Yii::app()->helper->formatRupiah($n_tindakan + $n_visite + $n_konsul);
            ?>    


      </td>
     </tr>
    <?php 
      
    }

    $total = $total_tindakan + $total_konsul + $total_visite;

    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      
      <td style="text-align: right;"><?php 
       echo Yii::app()->helper->formatRupiah($total_visite);
      ?></td>
      <td style="text-align: right;"><?php 
       echo Yii::app()->helper->formatRupiah($total_konsul);
      ?></td>
       <td style="text-align: right;"><?php 
       echo Yii::app()->helper->formatRupiah($total_tindakan);
      ?></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total);
      ?>
        </strong>
      </td>
       <!-- <td>&nbsp;</td> -->
    </tr>
  </tbody>

</table>