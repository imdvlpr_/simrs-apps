<?php 
if($params == 'xls'){
  $tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

  $filename = 'laporan_resep_'.$tgl.'.xls';

  header('Content-type: application/excel');
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<table width="100%">
  <tr>
    <th <?=$params == 'xls' ? 'colspan="6"' : 'style="text-align: center;"';?> > LAPORAN RESEP<br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?></th>
  </tr>
</table>
<table class="table  table-bordered table-hover" <?=$params == 'xls' ? 'border="1"' : '';?>>
  
  <thead>
    <tr>
      <th>No</th>
      <th>No RM</th>
      <th>Nama Px</th>
      <th>Biaya Racikan</th>
      <th>Biaya Non-Racikan</th>
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $total_non_racik = 0;
    $total_racik = 0;
    $total_all = 0;

    $i = 1;
    foreach($model->searchResep() as $ri)
    {


      $nilai_racik = $ri->sumObatRacikan;

      $nilai_non_racik = $ri->sumObatItem;
      


      
      $total_kanan = $nilai_racik + $nilai_non_racik;
      $total_all += $total_kanan;

      $total_racik += $nilai_racik;

      $total_non_racik += $nilai_non_racik;
      

     
     
  ?>
    <tr>
       <td><?php 

        echo $i;?></td>
       <td>
        <?php 

        echo CHtml::link($ri->trResepPasien->pasien->NoMedrec,Yii::app()->createUrl('trResepKamar/view',array('kode_daftar'=>$ri->trResepPasien->nodaftar_id)));?>
          
        </td>
      <td><?php echo $ri->trResepPasien->pasien->NAMA;?></td>
     
      <td style="text-align: right;"><?php 
      
         echo Yii::app()->helper->formatRupiah($nilai_racik);
              ?></td>
       <td style="text-align: right;"><?php 
         echo Yii::app()->helper->formatRupiah($nilai_non_racik);
              ?></td>
   
      <td style="text-align: right;"><?php 
      
         echo Yii::app()->helper->formatRupiah($nilai_racik + $nilai_non_racik);
              ?></td>
      <!-- <td></td> -->
     </tr>
    <?php 

      $i++;
  }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
    
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_racik);
      ?></strong>
      </td>
     
     
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_non_racik);
      ?></strong>
      </td>
       <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_all);
      ?></strong>
      </td>
       <!-- <td>&nbsp;</td> -->
    </tr>
  </tbody>

</table>
