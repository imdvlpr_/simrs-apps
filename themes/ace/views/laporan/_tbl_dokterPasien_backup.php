<?php 

if($params == 'xls'){
  $tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

  $filename = 'laporan_dokter_pasien_'.$tgl.'.xls';

  header('Content-type: application/excel');
  header('Content-Disposition: attachment; filename='.$filename);
}

$biaya_visite_kelas_1 = JenisVisite::getBiaya('Visite Dokter Ahli',4);
$biaya_visite_kelas_2 = JenisVisite::getBiaya('Visite Dokter Ahli',5);
$biaya_visite_kelas_3 = JenisVisite::getBiaya('Visite Dokter Ahli',6);
$biaya_visite_vip = JenisVisite::getBiaya('Visite Dokter Ahli',1);
$biaya_tindakan_ird = JenisVisite::getBiaya('Visite IRD',1);

$biaya_visite_umum_kelas_1 = JenisVisite::getBiaya('Visite Dokter Umum',4);
$biaya_visite_umum_kelas_2 = JenisVisite::getBiaya('Visite Dokter Umum',5);
$biaya_visite_umum_kelas_3 = JenisVisite::getBiaya('Visite Dokter Umum',6);
$biaya_visite_umum_vip = JenisVisite::getBiaya('Visite Dokter Umum',1);

$biaya_konsul_kelas_1 = JenisVisite::getBiaya('Konsul Dokter Ahli',4);
$biaya_konsul_kelas_2 = JenisVisite::getBiaya('Konsul Dokter Ahli',5);
$biaya_konsul_kelas_3 = JenisVisite::getBiaya('Konsul Dokter Ahli',6);
$biaya_konsul_vip = JenisVisite::getBiaya('Konsul Dokter Ahli',1);

$biaya_tindakan_konsul_ird = JenisVisite::getBiaya('Konsul IRD',1);
?>
<table width="100%">
  <tr>
    <th <?=$params == 'xls' ? 'colspan="17"' : 'style="text-align: center;"';?> > LAPORAN DOKTER <?php echo $dokter->FULLNAME;?><br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?></th>
  </tr>
</table>
<table class="table table-bordered" <?=$params == 'xls' ? 'border="1"' : '';?>>
  
  <thead>
    <tr>
      <th>No</th>
      <th>No Rekam Medik</th>
      <th>Nama Pasien</th>
      <th>Status Px</th>
      <th>Kelas</th>
     
      <th>TMR</th>
      <th>TKR</th>
      <th>Lama Dirawat</th>
      <th>DPJP</th>
      <th>Visite</th>
     <th>Jml Visite<br>IRD</th>
      <th>Jml Visite<br>IRNA</th>
      <th>Konsul</th>
       <th>Jml<br>Konsul IRD</th>
       <th>Jml<br>Konsul IRNA</th>
      <th>Tindakan OP</th>
      <th>T. Penunjang</th>
      <th>Pendampingan</th>
      <th>Jumlah</th>
      
    </tr>
  </thead>
  <tbody>
  <?php 

    $nourut=1;

    $all_total_konsul = 0;
    $all_total_visite = 0;
    $all_total_tindakan = 0;
    $all_total_penunjang = 0;

    $kamarLabel = [];
    $index_kamar = 0;

    $kali_visite_ird = 0;
    $kali_visite_ird_u = 0;
    $kali_visite_ird_non_u = 0;

    $jml_visite_ird = 0;
    $jml_visite_ird_u = 0;
    $jml_visite_ird_non_u = 0;
    $kali_visite_kelas_1 = 0;
    $jml_visite_kelas_1 = 0;

    $kali_visite_kelas_1_u = 0;
    $jml_visite_kelas_1_u = 0;
    $kali_visite_kelas_1_non_u = 0;
    $jml_visite_kelas_1_non_u = 0;

    $kali_visite_kelas_2_u = 0;
    $jml_visite_kelas_2_u = 0;
    $kali_visite_kelas_2_non_u = 0;
    $jml_visite_kelas_2_non_u = 0;

    $kali_visite_kelas_3_u = 0;
    $jml_visite_kelas_3_u = 0;
    $kali_visite_kelas_3_non_u = 0;
    $jml_visite_kelas_3_non_u = 0;

    $kali_visite_kelas_2 = 0;
    $jml_visite_kelas_2 = 0;
    $kali_visite_kelas_3 = 0;
    $jml_visite_kelas_3 = 0;
    $kali_visite_vip = 0;
    $jml_visite_vip = 0;

    $kali_visite_vip_u = 0;
    $jml_visite_vip_u = 0;

    $kali_visite_vip_non_u = 0;
    $jml_visite_vip_non_u = 0;

    $countRaberKelas_1 = 0;
    $countRaberKelas_2 = 0;
    $countRaberKelas_3 = 0;
    $countRaberVIP = 0;

    $countRaberKelas_1_u = 0;
    $countRaberKelas_2_u = 0;
    $countRaberKelas_3_u = 0;
    $countRaberVIP_u = 0;

    $countRaberKelas_1_non_u = 0;
    $countRaberKelas_2_non_u = 0;
    $countRaberKelas_3_non_u = 0;
    $countRaberVIP_non_u = 0;

    $kali_konsul_ird = 0;
    $jml_konsul_ird = 0;
    $kali_konsul_kelas_1 = 0;
    $jml_konsul_kelas_1 = 0;
    $kali_konsul_kelas_2 = 0;
    $jml_konsul_kelas_2 = 0;
    $kali_konsul_kelas_3 = 0;
    $jml_konsul_kelas_3 = 0;

    $kali_konsul_ird_u = 0;
    $jml_konsul_ird_u = 0;
    $kali_konsul_kelas_1_u = 0;
    $jml_konsul_kelas_1_u = 0;
    $kali_konsul_kelas_2_u = 0;
    $jml_konsul_kelas_2_u = 0;
    $kali_konsul_kelas_3_u = 0;
    $jml_konsul_kelas_3_u = 0;

    $kali_konsul_ird_non_u = 0;
    $jml_konsul_ird_non_u = 0;
    $kali_konsul_kelas_1_non_u = 0;
    $jml_konsul_kelas_1_non_u = 0;
    $kali_konsul_kelas_2_non_u = 0;
    $jml_konsul_kelas_2_non_u = 0;
    $kali_konsul_kelas_3_non_u = 0;
    $jml_konsul_kelas_3_non_u = 0;

    $kali_konsul_vip = 0;
    $jml_konsul_vip = 0;

    $kali_konsul_vip_u = 0;
    $jml_konsul_vip_u = 0;
    $kali_konsul_vip_non_u = 0;
    $jml_konsul_vip_non_u = 0;




    $jml_tindakan_op = [];
    $jml_tindakan_op_vip = 0;
    $jml_tindakan_op_1 = 0;
    $jml_tindakan_op_2 = 0;
    $jml_tindakan_op_3 = 0;

    $jml_tindakan_op_u = [];
    $jml_tindakan_op_vip_u = 0;
    $jml_tindakan_op_1_u = 0;
    $jml_tindakan_op_2_u = 0;
    $jml_tindakan_op_3_u = 0;

    $jml_tindakan_op_non_u = [];
    $jml_tindakan_op_vip_non_u = 0;
    $jml_tindakan_op_1_non_u = 0;
    $jml_tindakan_op_2_non_u = 0;
    $jml_tindakan_op_3_non_u = 0;

    $jumlah_visite_irna_kls_1_u = 0;
    $jumlah_visite_irna_kls_1_non_u = 0;
    $jumlah_visite_irna_kls_2_u = 0;
    $jumlah_visite_irna_kls_2_non_u = 0;
    $jumlah_visite_irna_kls_3_u = 0;
    $jumlah_visite_irna_kls_3_non_u = 0;
    $jumlah_visite_irna_vip_u = 0;
    $jumlah_visite_irna_vip_non_u = 0;

    $biaya_penunjang1_u = 0;
    $biaya_penunjang1_non_u = 0;
    $biaya_penunjang2_u = 0;
    $biaya_penunjang2_non_u = 0;
    $biaya_penunjang3_u = 0;
    $biaya_penunjang3_non_u = 0;
    $biaya_penunjangvip_u = 0;
    $biaya_penunjangvip_non_u = 0;

    $kali_pdp_ird = 0;
    $jml_pdp_ird = 0;
    $kali_pdp_kelas_1 = 0;
    $jml_pdp_kelas_1 = 0;
    $kali_pdp_kelas_2 = 0;
    $jml_pdp_kelas_2 = 0;
    $kali_pdp_kelas_3 = 0;
    $jml_pdp_kelas_3 = 0;

    $kali_pdp_ird_u = 0;
    $jml_pdp_ird_u = 0;
    $kali_pdp_kelas_1_u = 0;
    $jml_pdp_kelas_1_u = 0;
    $kali_pdp_kelas_2_u = 0;
    $jml_pdp_kelas_2_u = 0;
    $kali_pdp_kelas_3_u = 0;
    $jml_pdp_kelas_3_u = 0;

    $kali_pdp_ird_non_u = 0;
    $jml_pdp_ird_non_u = 0;
    $kali_pdp_kelas_1_non_u = 0;
    $jml_pdp_kelas_1_non_u = 0;
    $kali_pdp_kelas_2_non_u = 0;
    $jml_pdp_kelas_2_non_u = 0;
    $kali_pdp_kelas_3_non_u = 0;
    $jml_pdp_kelas_3_non_u = 0;

    $jml_pdp_vip_u = 0;
    $jml_pdp_vip_non_u = 0;

    $total_pdp_down = 0;

    foreach($rawatInaps as $ri)
    {

        
        $biaya_visite_ird = 0;
        $biaya_visite_irna = 0;

        $biaya_konsul_ird = 0;
        $biaya_konsul_irna = 0;

        $total_konsul = 0;
        $total_visite = 0;
        $total_tindakan = 0;
        $total_penunjang = 0;
        $jumlah_visite_irna = 0;
        $jumlah_konsul_irna = 0;
        $jumlah_visite_ird = 0;
        $jumlah_konsul_ird = 0;

        $jumlah_pdp_irna = 0;
        $jumlah_pdp_irna_u = 0;
        $jumlah_pdp_irna_non_u = 0;

        $biaya_pdp_irna = 0;
        $biaya_pdp_irna_u = 0;
        $biaya_pdp_irna_non_u = 0;

        $biaya_top_jm = $ri->sumOp($dokter->id_dokter);
        $biaya_top_jm_u = 0;
        $biaya_top_jm_non_u = 0;
        if($ri->jenisPasien->NamaGol == 'UMUM')
        {
          $biaya_top_jm_u = $ri->sumOp($dokter->id_dokter);
        }

        else
        {
          $biaya_top_jm_non_u = $ri->sumOp($dokter->id_dokter);
        }

        $total_tindakan = $biaya_top_jm;

        if($ri->kamar->kelas->kode_kelas == 'VIP'){
          $countRaberVIP += $ri->countRaber($dokter->id_dokter);
          $jml_tindakan_op_vip += $biaya_top_jm;
          $jml_tindakan_op[$ri->kamar->kelas_id] = [
            'nama' => 'VIP',
            'value' => $jml_tindakan_op_vip
          ];

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $countRaberVIP_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_vip_u += $biaya_top_jm_u;
            $jml_tindakan_op_u[$ri->kamar->kelas_id] = [
              'nama' => 'VIP',
              'value' => $jml_tindakan_op_vip_u
            ];
          }

          else{
            $countRaberVIP_non_u += $ri->countRaber($dokter->id_dokter);   
            $jml_tindakan_op_vip_non_u += $biaya_top_jm_non_u;
            $jml_tindakan_op_non_u[$ri->kamar->kelas_id] = [
              'nama' => 'VIP',
              'value' => $jml_tindakan_op_vip_non_u
            ];
          }
        }

        else if($ri->kamar->kelas->kode_kelas == 'KLS 1'){
          $countRaberKelas_1 += $ri->countRaber($dokter->id_dokter);
          $jml_tindakan_op_1 += $biaya_top_jm;
          $jml_tindakan_op[$ri->kamar->kelas_id] = [
            'nama' => 'Kelas 1',
            'value' => $jml_tindakan_op_1
          ];

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $countRaberKelas_1_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_1_u += $biaya_top_jm_u;
            $jml_tindakan_op_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 1',
              'value' => $jml_tindakan_op_1_u
            ];
          }

          else{
            $countRaberKelas_1_non_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_1_non_u += $biaya_top_jm_non_u;
            $jml_tindakan_op_non_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 1',
              'value' => $jml_tindakan_op_1_non_u
            ];
          }
        }

        else if($ri->kamar->kelas->kode_kelas == 'KLS 2'){
          $countRaberKelas_2 += $ri->countRaber($dokter->id_dokter);
          $jml_tindakan_op_2 += $biaya_top_jm;
          $jml_tindakan_op[$ri->kamar->kelas_id] = [
            'nama' => 'Kelas 2',
            'value' => $jml_tindakan_op_2
          ];

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $countRaberKelas_2_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_2_u += $biaya_top_jm_u;
            $jml_tindakan_op_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 2',
              'value' => $jml_tindakan_op_2_u
            ];
          }

          else{
            $countRaberKelas_2_non_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_2_non_u += $biaya_top_jm_non_u;
            $jml_tindakan_op_non_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 2',
              'value' => $jml_tindakan_op_2_non_u
            ];
          }
        }

        else if($ri->kamar->kelas->kode_kelas == 'KLS 3'){
          $countRaberKelas_3 += $ri->countRaber($dokter->id_dokter);
          $jml_tindakan_op_3 += $biaya_top_jm;
          $jml_tindakan_op[$ri->kamar->kelas_id] = [
            'nama' => 'Kelas 3',
            'value' => $jml_tindakan_op_3
          ];

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $countRaberKelas_3_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_3_u += $biaya_top_jm_u;
            $jml_tindakan_op_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 3',
              'value' => $jml_tindakan_op_3_u
            ];
          }

          else{
            $countRaberKelas_3_non_u += $ri->countRaber($dokter->id_dokter);
            $jml_tindakan_op_3_non_u += $biaya_top_jm_non_u;

            $jml_tindakan_op_non_u[$ri->kamar->kelas_id] = [
              'nama' => 'Kelas 3',
              'value' => $jml_tindakan_op_3_non_u
            ];
          }
        }
       
        
        foreach($ri->tRIVisiteDokters as $vi)
        {

            if(!empty($vi->id_dokter) && $vi->id_dokter == $dokter->id_dokter)
            {
                 if($vi->jenisVisite->kode_visite == 'VISITE')
                 {

                    if($vi->biaya_visite != 0)
                    {
                      $jumlah_visite_ird = $vi->jumlah_visite_ird;
                      $biaya_visite_ird = $biaya_visite_ird + $vi->biaya_visite;// * $vi->jumlah_visite_ird;
                      // $biaya_visite_irna = $biaya_visite_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                      $jml_visite_ird += $vi->biaya_visite;
                      $kali_visite_ird += $vi->jumlah_visite_ird;
                      if($ri->jenisPasien->NamaGol == 'UMUM')
                      {
                        $jml_visite_ird_u += $vi->biaya_visite;
                        $kali_visite_ird_u += $vi->jumlah_visite_ird;
                      }

                      else
                      {
                        $jml_visite_ird_non_u += $vi->biaya_visite;
                        $kali_visite_ird_non_u += $vi->jumlah_visite_ird;
                      }
                      
                    }
                 }

                 else if($vi->jenisVisite->kode_visite == 'KONSUL')
                 {
                    if($vi->biaya_visite != 0)
                    {
                      $jumlah_konsul_ird = $vi->jumlah_visite_ird;
                      $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite;// * $vi->jumlah_visite_ird;
                    
                   
                      $kali_konsul_ird += $vi->jumlah_visite_ird;
                      $jml_konsul_ird += $vi->biaya_visite;
                      if($ri->jenisPasien->NamaGol=='UMUM'){
                        $kali_konsul_ird_u += $vi->jumlah_visite_ird;
                        $jml_konsul_ird_u += $vi->biaya_visite;
                      }

                      else{
                        $kali_konsul_ird_non_u += $vi->jumlah_visite_ird;
                        $jml_konsul_ird_non_u += $vi->biaya_visite;
                      }
                    }
                    
                 }  
            }

            // dokter non dpjp 
            if(!empty($vi->id_dokter_irna) && $vi->id_dokter_irna == $dokter->id_dokter)
            {
                 if($vi->jenisVisite->kode_visite == 'VISITE')
                 {
                   if($vi->biaya_visite_irna != 0)
                   {
                    switch($ri->kamar->kelas_id){
                      case 1 :
                        $kali_visite_vip += $vi->jumlah_visite;
                        $jml_visite_vip += $vi->biaya_visite_irna;
                        if($ri->jenisPasien->NamaGol == 'UMUM')
                        {
                            $kali_visite_vip_u += $vi->jumlah_visite;
                            $jml_visite_vip_u += $vi->biaya_visite_irna;
                        }
                        else
                        {
                            $kali_visite_vip_non_u += $vi->jumlah_visite;
                            $jml_visite_vip_non_u += $vi->biaya_visite_irna;
                        }
                        break;
                      case 4 :
                       $kali_visite_kelas_1 += $vi->jumlah_visite;
                       $jml_visite_kelas_1 += $vi->biaya_visite_irna;
                        if($ri->jenisPasien->NamaGol == 'UMUM')
                        {
                            $kali_visite_kelas_1_u += $vi->jumlah_visite;
                            $jml_visite_kelas_1_u += $vi->biaya_visite_irna;
                        }
                        else
                        {
                            $kali_visite_kelas_1_non_u += $vi->jumlah_visite;
                            $jml_visite_kelas_1_non_u += $vi->biaya_visite_irna;
                        }
                       break;
                      case 5:
                        $kali_visite_kelas_2 += $vi->jumlah_visite;
                        $jml_visite_kelas_2 += $vi->biaya_visite_irna;
                        if($ri->jenisPasien->NamaGol == 'UMUM')
                        {
                            $kali_visite_kelas_2_u += $vi->jumlah_visite;
                            $jml_visite_kelas_2_u += $vi->biaya_visite_irna;
                        }
                        else
                        {
                            $kali_visite_kelas_2_non_u += $vi->jumlah_visite;
                            $jml_visite_kelas_2_non_u += $vi->biaya_visite_irna;
                        }
                        break;
                      case 6:
                        $kali_visite_kelas_3 += $vi->jumlah_visite;
                        $jml_visite_kelas_3 += $vi->biaya_visite_irna;
                        if($ri->jenisPasien->NamaGol == 'UMUM')
                        {
                            $kali_visite_kelas_3_u += $vi->jumlah_visite;
                            $jml_visite_kelas_3_u += $vi->biaya_visite_irna;
                        }
                        else
                        {
                            $kali_visite_kelas_3_non_u += $vi->jumlah_visite;
                            $jml_visite_kelas_3_non_u += $vi->biaya_visite_irna;
                        }
                        break;

                    }
                    $jumlah_visite_irna = $vi->jumlah_visite;
                    
                    // $biaya_visite_ird = $biaya_visite_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                    $biaya_visite_irna = $biaya_visite_irna + $vi->biaya_visite_irna;// * $vi->jumlah_visite;
                  }
                 }

                 else if($vi->jenisVisite->kode_visite == 'KONSUL')
                 {
                    if($vi->biaya_visite_irna != 0)
                    {
                      $jumlah_konsul_irna = $vi->jumlah_visite;
                      // $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                      $biaya_konsul_irna = $biaya_konsul_irna + $vi->biaya_visite_irna ;//* $vi->jumlah_visite;
                      switch($ri->kamar->kelas_id){
                        case 1 :
                          $kali_konsul_vip += $vi->jumlah_visite;
                          $jml_konsul_vip += $vi->biaya_visite_irna;
                          if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_konsul_vip_u += $vi->jumlah_visite;
                          $jml_konsul_vip_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_konsul_vip_non_u += $vi->jumlah_visite;
                          $jml_konsul_vip_non_u += $vi->biaya_visite_irna;
                         }
                          break;
                        case 4 :
                         $kali_konsul_kelas_1 += $vi->jumlah_visite;
                         $jml_konsul_kelas_1 += $vi->biaya_visite_irna;
                         if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_konsul_kelas_1_u += $vi->jumlah_visite;
                          $jml_konsul_kelas_1_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_konsul_kelas_1_non_u += $vi->jumlah_visite;
                         $jml_konsul_kelas_1_non_u += $vi->biaya_visite_irna;
                         }
                         break;
                        case 5:
                          $kali_konsul_kelas_2 += $vi->jumlah_visite;
                          $jml_konsul_kelas_2 += $vi->biaya_visite_irna;
                           if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_konsul_kelas_2_u += $vi->jumlah_visite;
                          $jml_konsul_kelas_2_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_konsul_kelas_2_non_u += $vi->jumlah_visite;
                         $jml_konsul_kelas_2_non_u += $vi->biaya_visite_irna;
                         }
                          break;
                        case 6:
                          $kali_konsul_kelas_3 += $vi->jumlah_visite;
                          $jml_konsul_kelas_3 += $vi->biaya_visite_irna;
                           if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_konsul_kelas_3_u += $vi->jumlah_visite;
                          $jml_konsul_kelas_3_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_konsul_kelas_3_non_u += $vi->jumlah_visite;
                         $jml_konsul_kelas_3_non_u += $vi->biaya_visite_irna;
                         }
                          break;

                      }
                    }
                 }

                 else if($vi->jenisVisite->kode_visite == 'PDP')
                 {
                    if($vi->biaya_visite_irna != 0)
                    {
                      $jumlah_pdp_irna = $vi->jumlah_visite;
                      // $biaya_konsul_ird = $biaya_konsul_ird + $vi->biaya_visite * $vi->jumlah_visite_ird;
                      $biaya_pdp_irna += $vi->biaya_visite_irna ;//* $vi->jumlah_visite;
                      $total_pdp_down += $biaya_pdp_irna;
                      switch($ri->kamar->kelas_id){
                        case 1 :
                          $kali_pdp_vip += $vi->jumlah_visite;
                          $jml_pdp_vip += $vi->biaya_visite_irna;
                          if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_pdp_vip_u += $vi->jumlah_visite;
                          $jml_pdp_vip_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_pdp_vip_non_u += $vi->jumlah_visite;
                          $jml_pdp_vip_non_u += $vi->biaya_visite_irna;
                         }
                          break;
                        case 4 :
                         $kali_pdp_kelas_1 += $vi->jumlah_visite;
                         $jml_pdp_kelas_1 += $vi->biaya_visite_irna;
                         if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_pdp_kelas_1_u += $vi->jumlah_visite;
                          $jml_pdp_kelas_1_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_pdp_kelas_1_non_u += $vi->jumlah_visite;
                         $jml_pdp_kelas_1_non_u += $vi->biaya_visite_irna;
                         }
                         break;
                        case 5:
                          $kali_pdp_kelas_2 += $vi->jumlah_visite;
                          $jml_pdp_kelas_2 += $vi->biaya_visite_irna;
                           if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_pdp_kelas_2_u += $vi->jumlah_visite;
                          $jml_pdp_kelas_2_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_pdp_kelas_2_non_u += $vi->jumlah_visite;
                         $jml_pdp_kelas_2_non_u += $vi->biaya_visite_irna;
                         }
                          break;
                        case 6:
                          $kali_pdp_kelas_3 += $vi->jumlah_visite;
                          $jml_pdp_kelas_3 += $vi->biaya_visite_irna;
                           if($ri->jenisPasien->NamaGol == 'UMUM')
                         {
                          $kali_pdp_kelas_3_u += $vi->jumlah_visite;
                          $jml_pdp_kelas_3_u += $vi->biaya_visite_irna;
                         }

                         else{
                          $kali_pdp_kelas_3_non_u += $vi->jumlah_visite;
                         $jml_pdp_kelas_3_non_u += $vi->biaya_visite_irna;
                         }
                          break;

                      }
                    }
                 }    
            }
        }

        $total_konsul = $biaya_konsul_ird + $biaya_konsul_irna;
        $total_visite = $biaya_visite_ird + $biaya_visite_irna;

        $all_total_visite = $all_total_visite + $total_visite;
        $all_total_konsul = $all_total_konsul + $total_konsul;

        $tmp_jumlah_visite_irna_kls_1_u = 0;
        $tmp_jumlah_visite_irna_kls_1_non_u = 0;
        $tmp_jumlah_visite_irna_kls_2_u = 0;
        $tmp_jumlah_visite_irna_kls_2_non_u = 0;
        $tmp_jumlah_visite_irna_kls_3_u = 0;
        $tmp_jumlah_visite_irna_kls_3_non_u = 0;
        $tmp_jumlah_visite_irna_vip_u = 0;
        $tmp_jumlah_visite_irna_vip_non_u = 0;
        
        if($ri->kamar->kelas->kode_kelas == 'VIP')
        {
          // $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_vip;
          
          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $tmp_jumlah_visite_irna_vip_u = $biaya_visite_irna / $biaya_visite_vip;
          }
          else
          {
            if(Yii::app()->helper->contains($ri->jenisPasien->NamaGol, 'BPJS'))
            {
              $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_vip;
              // $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_3;
            }
            $tmp_jumlah_visite_irna_vip_non_u = $biaya_visite_irna / $biaya_visite_vip;
          }
        }

        else if($ri->kamar->kelas->kode_kelas == 'KLS 1'){
          // $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_1;
          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $tmp_jumlah_visite_irna_kls_1_u = $biaya_visite_irna / $biaya_visite_kelas_1;
          }
          else
          {
            if(Yii::app()->helper->contains($ri->jenisPasien->NamaGol, 'BPJS'))
            {
              $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_1;
              // $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_3;
            }
            $tmp_jumlah_visite_irna_kls_1_non_u = $biaya_visite_irna / $biaya_visite_kelas_1;
          }
        }
        else if($ri->kamar->kelas->kode_kelas == 'KLS 2'){
          // $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_2;
          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $tmp_jumlah_visite_irna_kls_2_u = $biaya_visite_irna / $biaya_visite_kelas_2;
          }
          else
          {
            if(Yii::app()->helper->contains($ri->jenisPasien->NamaGol, 'BPJS'))
            {
              $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_2;
              // $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_3;
            }
            $tmp_jumlah_visite_irna_kls_2_non_u = $biaya_visite_irna / $biaya_visite_kelas_2;
          }
        }
        else if($ri->kamar->kelas->kode_kelas == 'KLS 3'){
          // $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_3;

          if($ri->jenisPasien->NamaGol == 'UMUM')
          {
            $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_3;
            $tmp_jumlah_visite_irna_kls_3_u = $biaya_visite_irna / $biaya_visite_kelas_3;
          }
          else
          {

            $tmp_jumlah_visite_irna_kls_3_non_u = $biaya_visite_irna / $biaya_visite_kelas_3;
            if(Yii::app()->helper->contains($ri->jenisPasien->NamaGol, 'BPJS'))
            {
              $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_3;
              // $jumlah_visite_irna = $biaya_visite_irna / $biaya_visite_kelas_3;
            }
            
            
          }
        }

        
        $jumlah_visite_irna = floor($jumlah_visite_irna * 2) / 2;
        $tmp_jumlah_visite_irna_vip_u = floor($tmp_jumlah_visite_irna_vip_u * 2) / 2;
        $tmp_jumlah_visite_irna_vip_non_u = floor($tmp_jumlah_visite_irna_vip_non_u * 2) / 2;
        $tmp_jumlah_visite_irna_kls_3_u = floor($tmp_jumlah_visite_irna_kls_3_u * 2) / 2;
        $tmp_jumlah_visite_irna_kls_3_non_u = floor($tmp_jumlah_visite_irna_kls_3_non_u * 2) / 2;
        $tmp_jumlah_visite_irna_kls_2_u = floor($tmp_jumlah_visite_irna_kls_2_u * 2) / 2;
        $tmp_jumlah_visite_irna_kls_2_non_u = floor($tmp_jumlah_visite_irna_kls_2_non_u * 2) / 2;
        $tmp_jumlah_visite_irna_kls_1_u = floor($tmp_jumlah_visite_irna_kls_1_u * 2) / 2;
        $tmp_jumlah_visite_irna_kls_1_non_u = floor($tmp_jumlah_visite_irna_kls_1_non_u * 2) / 2;


        $jumlah_visite_irna_kls_1_u += $tmp_jumlah_visite_irna_kls_1_u;
        $jumlah_visite_irna_kls_1_non_u += $tmp_jumlah_visite_irna_kls_1_non_u;
        $jumlah_visite_irna_kls_2_u += $tmp_jumlah_visite_irna_kls_2_u;
        $jumlah_visite_irna_kls_2_non_u += $tmp_jumlah_visite_irna_kls_2_non_u;
        $jumlah_visite_irna_kls_3_u += $tmp_jumlah_visite_irna_kls_3_u;
        $jumlah_visite_irna_kls_3_non_u += $tmp_jumlah_visite_irna_kls_3_non_u;
        $jumlah_visite_irna_vip_u += $tmp_jumlah_visite_irna_vip_u;
        $jumlah_visite_irna_vip_non_u += $tmp_jumlah_visite_irna_vip_non_u;
   
        $biaya_tindakan_anas = 0;
        
        foreach($ri->trRawatInapTops as $vi)
        {

            // if(!empty($vi->id_dokter_jm) && $vi->id_dokter_jm == $dokter->id_dokter)
            // {
            //     $biaya_tindakan_jm = $biaya_tindakan_jm + $vi->biaya_irna * $vi->jumlah_tindakan;
            // }

            if(!empty($vi->id_dokter_anas) && $vi->id_dokter_anas == $dokter->id_dokter)
            {
                $biaya_tindakan_anas = $biaya_tindakan_anas + $vi->biaya_irna * $vi->jumlah_tindakan;
            }
        }

        $total_tindakan += $biaya_tindakan_anas;

        
        

        $biaya_penunjang = 0;
        foreach($ri->trRawatInapPenunjangs as $vi)
        {
            if(!empty($vi->dokter_id) && $vi->dokter_id == $dokter->id_dokter)
          { 
               if($vi->penunjang->nama_tindakan == 'USG' ||
                  $vi->penunjang->nama_tindakan == 'PA' ||
                  $vi->penunjang->nama_tindakan == 'EKG')
               {

                  $biaya_penunjang += $vi->biaya_irna;
                  if($ri->kamar->kelas->kode_kelas == 'VIP'){
                    if($ri->jenisPasien->NamaGol == 'UMUM')
                    {
                      $biaya_penunjangvip_u += $vi->biaya_irna;
                    }

                    else
                    {
                      $biaya_penunjangvip_non_u += $vi->biaya_irna; 
                    }

                  }

                  else if($ri->kamar->kelas->kode_kelas == 'KLS 1'){
                    if($ri->jenisPasien->NamaGol == 'UMUM')
                    {
                      $biaya_penunjang1_u += $vi->biaya_irna;
                    }

                    else
                    {
                      $biaya_penunjang1_non_u += $vi->biaya_irna; 
                    }

                  }
                  else if($ri->kamar->kelas->kode_kelas == 'KLS 2'){
                    if($ri->jenisPasien->NamaGol == 'UMUM')
                    {
                      $biaya_penunjang2_u += $vi->biaya_irna;
                    }

                    else
                    {
                      $biaya_penunjang2_non_u += $vi->biaya_irna; 
                    }
                  }
                  else if($ri->kamar->kelas->kode_kelas == 'KLS 3'){
                    if($ri->jenisPasien->NamaGol == 'UMUM')
                    {
                      $biaya_penunjang3_u += $vi->biaya_irna;
                    }

                    else
                    {
                      $biaya_penunjang3_non_u += $vi->biaya_irna; 
                    }
                  }
                  
               }
            }
        }

        $total_penunjang += $biaya_penunjang;
        $all_total_penunjang += $total_penunjang;

        // $biaya_top_ja = 0;
        // foreach($ri->trRawatInapTopJa as $vi)
        // {
        //   if(!empty($vi->id_dokter) && $vi->id_dokter == $dokter->id_dokter)
        //   { 
        //       $biaya_top_ja = $biaya_top_ja + $vi->nilai;
        //   } 
        // }

        // $total_tindakan = $total_tindakan + $biaya_top_ja;

         if($total_visite == 0 && $total_konsul == 0 && $total_tindakan == 0 && $total_penunjang == 0) continue;
         
        $all_total_tindakan = $all_total_tindakan + $total_tindakan ;

        $selisih_hari = 1;

        if(!empty($ri->tanggal_keluar))
        {
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
        }
   
        // if($params == 'non_xls'){
          if(!in_array(strtoupper($ri->kamar->kamarMaster->nama_kamar), $kamarLabel)){
            $kamarLabel[] = strtoupper($ri->kamar->kamarMaster->nama_kamar);
            echo '<tr><td colspan="3">'.$kamarLabel[$index_kamar].'</td>';
            for($i=0;$i<14;$i++)
              echo '<td></td>';
            echo '</tr>';
            $index_kamar++;

            
          }
        // }
  ?>
    <tr>
       <td><?php echo $nourut++;?></td>
       <td>
        <?php 
        echo CHtml::link($ri->pASIEN->NoMedrec,array('trRawatInap/dataPasien','id'=>$ri->id_rawat_inap));
        ?>
          
        </td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td>
      <?php echo $selisih_hari;?>
      </td>
      <td><?=$ri->dokter == $dokter ? 'DPJP' : '';?></td>
      <td style="text-align: right">
      <?php 

        echo Yii::app()->helper->formatRupiah($total_visite,0,$params);
              ?></td>
      <td><?=$jumlah_visite_ird;?></td>
        <td><?=$jumlah_visite_irna ;?></td>
      <td style="text-align: right">
         <?php 
        echo Yii::app()->helper->formatRupiah($total_konsul,0,$params);
              ?>
      </td>
       <td><?=$jumlah_konsul_ird;?></td>
      <td><?=$jumlah_konsul_irna;?></td>
      <td style="text-align: right"><?php 
        echo Yii::app()->helper->formatRupiah($total_tindakan,0,$params);
              ?>
                
              </td>
        <td style="text-align: right"><?php 
        echo CHtml::link(Yii::app()->helper->formatRupiah($total_penunjang,0,$params),array('trRawatInap/penunjang','id'=>$ri->id_rawat_inap));
        // echo Yii::app()->helper->formatRupiah($biaya_penunjang);
              ?>
                
              </td>
        <td style="text-align: right"><?php 
        echo Yii::app()->helper->formatRupiah($biaya_pdp_irna,0,$params);
        // echo Yii::app()->helper->formatRupiah($biaya_penunjang);
              ?>
                
              </td>
      <td style="text-align: right"><?php 
        echo Yii::app()->helper->formatRupiah($total_tindakan + $total_konsul + $total_visite + $total_penunjang + $biaya_pdp_irna,0,$params);
              ?></td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td style="text-align: right"> <strong>
      <?php 
       echo Yii::app()->helper->formatRupiah($all_total_visite,0,$params);
      ?></strong></td>
      <td>&nbsp;</td>

      <td>
     
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_konsul,0,$params);
      ?></strong></td>
      <td>&nbsp;</td>
      <td>
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_tindakan,0,$params);
      ?></strong>
      </td>
      <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_penunjang,0,$params);
      ?></strong>
      </td>
    <td style="text-align: right"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_pdp_down,0,$params);
      ?></strong>
      </td>
      <td style="text-align: right" ><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($all_total_visite + $all_total_konsul + $all_total_tindakan + $all_total_penunjang + $total_pdp_down,0,$params);
      ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>

<h2>
  Summary
</h2>
<div class="col-xs-3">
<table class="table table-bordered" <?=$params == 'xls' ? 'border="1"' : '';?>>
  <tr>
  <th>VISITE</th>
  <th>Kali</th>
  <th>Rupiah</th>
  <th>Total Input</th>
</tr>
<?php 



  $countVisiteKelas1 = $jumlah_visite_irna_kls_1_u + $jumlah_visite_irna_kls_1_non_u;//$kali_visite_kelas_1 + ($countRaberKelas_1 / 2);
  $countVisiteKelas1_u =  $jumlah_visite_irna_kls_1_u; //$kali_visite_kelas_1_u + ($countRaberKelas_1_u / 2);
  $countVisiteKelas1_non_u = $jumlah_visite_irna_kls_1_non_u;// $kali_visite_kelas_1_non_u + ($countRaberKelas_1_non_u / 2);

  
  $countVisiteKelas2 = $jumlah_visite_irna_kls_2_u + $jumlah_visite_irna_kls_2_non_u;//$kali_visite_kelas_2 + ($countRaberKelas_2 / 2);
  $countVisiteKelas2_u = $jumlah_visite_irna_kls_2_u;//$kali_visite_kelas_2_u + ($countRaberKelas_2_u / 2);
  $countVisiteKelas2_non_u = $jumlah_visite_irna_kls_2_non_u;//$kali_visite_kelas_2_non_u + ($countRaberKelas_2_non_u / 2);

 
  $countVisiteKelas3 = $jumlah_visite_irna_kls_3_u + $jumlah_visite_irna_kls_3_non_u;//$kali_visite_kelas_3 + ($countRaberKelas_3 / 2);
  $countVisiteKelas3_u =$jumlah_visite_irna_kls_3_u; //$kali_visite_kelas_3_u + ($countRaberKelas_3_u / 2);
  $countVisiteKelas3_non_u = $jumlah_visite_irna_kls_3_non_u;//$kali_visite_kelas_3_non_u + ($countRaberKelas_3_non_u / 2);

  
  $countVisiteVIP = $jumlah_visite_irna_vip_u + $jumlah_visite_irna_vip_non_u;// $kali_visite_vip + ($countRaberVIP / 2);
  $countVisiteVIP_u = $jumlah_visite_irna_vip_u;//$kali_visite_vip_u + ($countRaberVIP_u / 2);
  $countVisiteVIP_non_u = $jumlah_visite_irna_vip_non_u;//$kali_visite_vip_non_u + ($countRaberVIP_non_u / 2);

  

  $total_count_biayaIrd = $biaya_tindakan_ird * $kali_visite_ird;
  $total_count_biaya1 = $biaya_visite_kelas_1 * $countVisiteKelas1;
  $total_count_biaya2 = $biaya_visite_kelas_2 * $countVisiteKelas2;
  $total_count_biaya3 = $biaya_visite_kelas_3 * $countVisiteKelas3;
  $total_count_biayaVip = $biaya_visite_vip * $countVisiteVIP;

  $total_visite_riil = $total_count_biayaIrd + $total_count_biayaVip + $total_count_biaya1 + $total_count_biaya2 + $total_count_biaya3;
  

  // Hitungan perkalian/ BUKAN inputan operator
  $jml_visite_ird = $kali_visite_ird * $biaya_tindakan_ird;
  $jml_visite_ird_u = $kali_visite_ird_u * $biaya_tindakan_ird;
  $jml_visite_ird_non_u = $kali_visite_ird_non_u * $biaya_tindakan_ird;

  $jml_visite_kelas_1 = $countVisiteKelas1 * $biaya_visite_kelas_1;
  $jml_visite_kelas_1_u = $countVisiteKelas1_u * $biaya_visite_kelas_1;
  $jml_visite_kelas_1_non_u = $countVisiteKelas1_non_u * $biaya_visite_kelas_1;

  $jml_visite_kelas_2 = $countVisiteKelas2 * $biaya_visite_kelas_2;
  $jml_visite_kelas_2_u = $countVisiteKelas2_u * $biaya_visite_kelas_2;
  $jml_visite_kelas_2_non_u = $countVisiteKelas2_non_u * $biaya_visite_kelas_2;

  $jml_visite_kelas_3 = $countVisiteKelas3 * $biaya_visite_kelas_3;
  $jml_visite_kelas_3_u = $countVisiteKelas3_u * $biaya_visite_kelas_3;
  $jml_visite_kelas_3_non_u = $countVisiteKelas3_non_u * $biaya_visite_kelas_3;

  $jml_visite_vip = $countVisiteVIP * $biaya_visite_vip;
  $jml_visite_vip_u = $countVisiteVIP_u * $biaya_visite_vip;
  $jml_visite_vip_non_u = $countVisiteVIP_non_u * $biaya_visite_vip;


  $total_jasa_input_123 = $jml_visite_ird + $jml_visite_kelas_1 + $jml_visite_kelas_2 + $jml_visite_kelas_3;
  $total_jasa_input_123_u = $jml_visite_ird_u + $jml_visite_kelas_1_u + $jml_visite_kelas_2_u + $jml_visite_kelas_3_u;
  $total_jasa_input_123_non_u = $jml_visite_ird_non_u + $jml_visite_kelas_1_non_u + $jml_visite_kelas_2_non_u + $jml_visite_kelas_3_non_u;

  $total_jasa_input_vip = $jml_visite_vip;
  $total_jasa_input_vip_u = $jml_visite_vip_u;
  $total_jasa_input_vip_non_u = $jml_visite_vip_non_u;

  $total_visite_input = $jml_visite_ird + $jml_visite_kelas_1 + $jml_visite_kelas_2 + $jml_visite_kelas_3 + $jml_visite_vip;


?>
<tr>
  <td>IRD</td>
  <td><?=$kali_visite_ird;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_tindakan_ird,0,$params) ;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_ird,0,$params) ;?></td>
</tr>
<tr>
  <td>Kelas 1</td>
  <td><?=$countVisiteKelas1;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_visite_kelas_1,0,$params);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_kelas_1,0,$params) ;?></td>
  
</tr>
<tr>
  <td>Kelas 2</td>
 <td><?=$countVisiteKelas2;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_visite_kelas_2,0,$params);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_kelas_2,0,$params) ;?></td>
  
</tr>
<tr>
  <td>Kelas 3</td>
  <td><?=$countVisiteKelas3;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_visite_kelas_3,0,$params);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_kelas_3,0,$params) ;?></td>
  
</tr>
<tr>
  <td>VIP</td>
 <td><?=$countVisiteVIP;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_visite_vip,0,$params);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_vip,0,$params) ;?></td>
  
</tr>
<tr>
  <td>Total</td>
 <td><?=$countVisiteVIP + $countVisiteKelas3 + $countVisiteKelas2 + $countVisiteKelas1;?></td>
  <td style="text-align: right"></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_visite_input,0,$params);?></td>
</tr>
</table>
</div>
<div class="col-xs-3">
<table class="table table-bordered" <?=$params == 'xls' ? 'border="1"' : '';?>>
  <tr>
  <th>KONSUL</th>
  <th>Kali</th>
  <th>Rupiah</th>
  <th>Total Input</th>
</tr>
<?php 

  $countkonsulKelas1 = $kali_konsul_kelas_1;
  $countkonsulKelas1_u = $kali_konsul_kelas_1_u;
  $countkonsulKelas1_non_u = $kali_konsul_kelas_1_non_u;

  
  $countkonsulKelas2 = $kali_konsul_kelas_2 ;
  $countkonsulKelas2_u = $kali_konsul_kelas_2_u;
  $countkonsulKelas2_non_u = $kali_konsul_kelas_2_non_u;

  
  $countkonsulKelas3 = $kali_konsul_kelas_3 ;
  $countkonsulKelas3_u = $kali_konsul_kelas_3_u;
  $countkonsulKelas3_non_u = $kali_konsul_kelas_3_non_u;

 
  $countkonsulVIP = $kali_konsul_vip ;
  $countkonsulVIP_u = $kali_konsul_vip_u ;
  $countkonsulVIP_non_u = $kali_konsul_vip_non_u ;


  $jml_konsul_ird = $kali_konsul_ird * $biaya_tindakan_konsul_ird;
  $jml_konsul_ird_u = $kali_konsul_ird_u * $biaya_tindakan_konsul_ird;
  $jml_konsul_ird_non_u = $kali_konsul_ird_non_u * $biaya_tindakan_konsul_ird;

  $jml_konsul_kelas_1 = $countkonsulKelas1 * $biaya_konsul_kelas_1;
  $jml_konsul_kelas_1_u = $kali_konsul_kelas_1_u * $biaya_konsul_kelas_1;
  $jml_konsul_kelas_1_non_u = $countkonsulKelas1_non_u * $biaya_konsul_kelas_1;

  $jml_konsul_kelas_2 = $countkonsulKelas2 * $biaya_konsul_kelas_2;
  $jml_konsul_kelas_2_u = $kali_konsul_kelas_2_u * $biaya_konsul_kelas_2;
  $jml_konsul_kelas_2_non_u = $countkonsulKelas2_non_u * $biaya_konsul_kelas_2;

  $jml_konsul_kelas_3 = $countkonsulKelas3 * $biaya_konsul_kelas_3;
  $jml_konsul_kelas_3_u = $kali_konsul_kelas_3_u * $biaya_konsul_kelas_3;
  $jml_konsul_kelas_3_non_u = $countkonsulKelas3_non_u * $biaya_konsul_kelas_3;

  $total_count_biayaIrd = $biaya_visite_ird * $kali_visite_ird;
  $total_count_biayaIrd_u = $biaya_visite_ird * $kali_visite_ird_u;
  $total_count_biayaIrd_non_u = $biaya_visite_ird * $kali_visite_ird_non_u;

  $total_count_biaya1 = $biaya_konsul_kelas_1 * $countkonsulKelas1;
  $total_count_biaya2 = $biaya_konsul_kelas_2 * $countkonsulKelas2;
  $total_count_biaya3 = $biaya_konsul_kelas_3 * $countkonsulKelas3;
  $total_count_biayaVip = $biaya_konsul_vip * $countkonsulVIP;

  $total_count_biaya1_u = $biaya_konsul_kelas_1 * $countkonsulKelas1_u;
  $total_count_biaya2_u = $biaya_konsul_kelas_2 * $countkonsulKelas2_u;
  $total_count_biaya3_u = $biaya_konsul_kelas_3 * $countkonsulKelas3_u;
  $total_count_biayaVip_u = $biaya_konsul_vip * $countkonsulVIP_u;

  $total_count_biaya1_non_u = $biaya_konsul_kelas_1 * $countkonsulKelas1_non_u;
  $total_count_biaya2_non_u = $biaya_konsul_kelas_2 * $countkonsulKelas2_non_u;
  $total_count_biaya3_non_u = $biaya_konsul_kelas_3 * $countkonsulKelas3_non_u;
  $total_count_biayaVip_non_u = $biaya_konsul_vip * $countkonsulVIP_non_u;

  $total_konsul_riil = $jml_konsul_ird + $total_count_biayaVip + $total_count_biaya1 + $total_count_biaya2 + $total_count_biaya3;
  $total_konsul_input = $jml_konsul_ird +$jml_konsul_kelas_1+$jml_konsul_kelas_2+$jml_konsul_kelas_3+$jml_konsul_vip;
  $total_konsul_input_u = $jml_konsul_ird_u +$jml_konsul_kelas_1_u+$jml_konsul_kelas_2_u+$jml_konsul_kelas_3_u+$jml_konsul_vip_u;
  $total_konsul_input_non_u = $jml_konsul_ird_non_u +$jml_konsul_kelas_1_non_u+$jml_konsul_kelas_2_non_u+$jml_konsul_kelas_3_non_u+$jml_konsul_vip_non_u;

  $total_jasa_input_123 += ($jml_konsul_ird +$jml_konsul_kelas_1+$jml_konsul_kelas_2+$jml_konsul_kelas_3);
  $total_jasa_input_123_u += ($jml_konsul_ird_u +$jml_konsul_kelas_1_u+$jml_konsul_kelas_2_u+$jml_konsul_kelas_3_u);
  $total_jasa_input_123_non_u += ($jml_konsul_ird_non_u +$jml_konsul_kelas_1_non_u+$jml_konsul_kelas_2_non_u+$jml_konsul_kelas_3_non_u);

  $total_jasa_input_vip += $jml_konsul_vip;
  $total_jasa_input_vip_u += $jml_konsul_vip_u;
  $total_jasa_input_vip_non_u += $jml_konsul_vip_non_u;
?>
<tr>
  <td>IRD</td>
  <td><?=$kali_konsul_ird;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_tindakan_konsul_ird,0,$params) ;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_ird,0,$params) ;?></td>
</tr>
<tr>
  <td>Kelas 1</td>
  <td><?=$countkonsulKelas1;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_konsul_kelas_1,0,$params);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_kelas_1,0,$params) ;?></td>
 
</tr>
<tr>
  <td>Kelas 2</td>
 <td><?=$countkonsulKelas2;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_konsul_kelas_2,0,$params);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_kelas_2,0,$params) ;?></td>
 
</tr>
<tr>
  <td>Kelas 3</td>
  <td><?=$countkonsulKelas3;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_konsul_kelas_3,0,$params);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_kelas_3,0,$params) ;?></td>

</tr>
<tr>
  <td>VIP</td>
 <td><?=$countkonsulVIP;?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_konsul_vip,0,$params);?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_vip,0,$params) ;?></td>
</tr>
<tr>
  <td>Total</td>
 <td><?=$countkonsulVIP + $countkonsulKelas3 + $countkonsulKelas2 + $countkonsulKelas1;?></td>
  <td style="text-align: right"></td>
   <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_konsul_input,0,$params);?></td>
</tr>
</table>
</div>
<div class="col-xs-2">
<table class="table table-bordered" <?=$params == 'xls' ? 'border="1"' : '';?>>
  <tr>
  <th>TINDAKAN OP</th>
  <th>Total </th>
  
</tr>
<?php
  usort($jml_tindakan_op, function($a, $b) {
      return $a['nama'] <=> $b['nama'];
  });

  usort($jml_tindakan_op_u, function($a, $b) {
      return $a['nama'] <=> $b['nama'];
  });

  usort($jml_tindakan_op_non_u, function($a, $b) {
      return $a['nama'] <=> $b['nama'];
  });
  $sum_op123 = 0;
  $sum_opVIP = 0;
  $sum_op = 0;

  $sum_op123_u = 0;
  $sum_opVIP_u = 0;
  $sum_op_u = 0;

  $sum_op123_non_u = 0;
  $sum_opVIP_non_u = 0;
  $sum_op_non_u = 0;

  foreach($jml_tindakan_op as $item)
  {
    if($item['nama'] == 'Kelas 1' || $item['nama'] == 'Kelas 2' || $item['nama'] == 'Kelas 3')
      $sum_op123 += $item['value'];
    else if($item['nama'] == 'VIP')
      $sum_opVIP += $item['value'];
    
    $sum_op += $item['value'];
?>
<tr>
  <td><?=$item['nama'];?></td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($item['value'],0,$params);?></td>
  
</tr>
<?php 
}

foreach($jml_tindakan_op_u as $item)
{
  if($item['nama'] == 'Kelas 1' || $item['nama'] == 'Kelas 2' || $item['nama'] == 'Kelas 3')
    $sum_op123_u += $item['value'];
  else if($item['nama'] == 'VIP')
    $sum_opVIP_u += $item['value'];
  
  $sum_op_u += $item['value'];
}


foreach($jml_tindakan_op_non_u as $item)
{
  if($item['nama'] == 'Kelas 1' || $item['nama'] == 'Kelas 2' || $item['nama'] == 'Kelas 3')
    $sum_op123_non_u += $item['value'];
  else if($item['nama'] == 'VIP')
    $sum_opVIP_non_u += $item['value'];
  
  $sum_op_non_u += $item['value'];
}


$total_jasa_input_123 += $sum_op123;
$total_jasa_input_vip += $sum_opVIP;

$total_jasa_input_123_u += $sum_op123_u;
$total_jasa_input_vip_u += $sum_opVIP_u;

$total_jasa_input_123_non_u += $sum_op123_non_u;
$total_jasa_input_vip_non_u += $sum_opVIP_non_u;
?>

  <td>Total</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($sum_op,0,$params);?></td>
</tr>
</table>
</div>
<div class="col-xs-2">
<table class="table table-bordered" <?=$params == 'xls' ? 'border="1"' : '';?>>
  <tr>
  <th>TINDAKAN<br>Non-OP</th>
  <th>Total </th>
  
</tr>
<?php 
$biaya_penunjang1 = $biaya_penunjang1_u + $biaya_penunjang1_non_u;
$biaya_penunjang2 = $biaya_penunjang2_u + $biaya_penunjang2_non_u;
$biaya_penunjang3 = $biaya_penunjang3_u + $biaya_penunjang3_non_u;
$biaya_penunjang123 = $biaya_penunjang1 + $biaya_penunjang2 + $biaya_penunjang3;
$biaya_penunjangvip = $biaya_penunjangvip_u + $biaya_penunjangvip_non_u;
$biaya_penunjang_sum = $biaya_penunjang1 + $biaya_penunjang2 + $biaya_penunjang3 + $biaya_penunjangvip;
$biaya_penunjang123_u = $biaya_penunjang1_u + $biaya_penunjang2_u + $biaya_penunjang3_u;
$biaya_penunjang123_non_u = $biaya_penunjang1_non_u + $biaya_penunjang2_non_u + $biaya_penunjang3_non_u;
$biaya_penunjangvip_u = $biaya_penunjangvip_u;
$biaya_penunjangvip_non_u = $biaya_penunjangvip_non_u;
?>
<tr>
  <td>KELAS 1</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjang1,0,$params);?></td>
  
</tr>
<tr>
  <td>KELAS 2</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjang2,0,$params);?></td>
  
</tr>
<tr>
  <td>KELAS 3</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjang3,0,$params);?></td>
  
</tr>
<tr>
  <td>VIP</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjangvip,0,$params);?></td>
  
</tr>
  <td>Total</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjang_sum,0,$params);?></td>
</tr>
</table>
</div>
<div class="col-xs-2">
<table class="table table-bordered" <?=$params == 'xls' ? 'border="1"' : '';?>>
  <tr>
  <th>Pendampingan</th>
  <th>Total </th>
  
</tr>
<?php 
$biaya_pdp1 = $jml_pdp_kelas_1_u + $jml_pdp_kelas_1_non_u;
$biaya_pdp2 = $jml_pdp_kelas_2_u + $jml_pdp_kelas_2_non_u;
$biaya_pdp3 = $jml_pdp_kelas_3_u + $jml_pdp_kelas_3_non_u;
$biaya_pdp123 = $biaya_pdp1 + $biaya_pdp2 + $biaya_pdp3;
$biaya_pdpvip = $jml_pdp_vip_u + $jml_pdp_vip_non_u;
$biaya_pdp_sum = $biaya_pdp1 + $biaya_pdp2 + $biaya_pdp3 + $biaya_pdpvip;
$biaya_pdp123_u = $jml_pdp_kelas_1_u + $jml_pdp_kelas_2_u + $jml_pdp_kelas_3_u;
$biaya_pdp123_non_u = $jml_pdp_kelas_1_non_u + $jml_pdp_kelas_2_non_u + $jml_pdp_kelas_3_non_u;
$biaya_pdpvip_u = $jml_pdp_vip_u;
$biaya_pdpvip_non_u = $jml_pdp_vip_non_u;
?>
<tr>
  <td>KELAS 1</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdp1,0,$params);?></td>
  
</tr>
<tr>
  <td>KELAS 2</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdp2,0,$params);?></td>
  
</tr>
<tr>
  <td>KELAS 3</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdp3,0,$params);?></td>
  
</tr>
<tr>
  <td>VIP</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdpvip,0,$params);?></td>
  
</tr>
  <td>Total</td>
  <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdp_sum,0,$params);?></td>
</tr>
</table>
</div>
    <?php

    $jml_visite_kelas_123_u = $jml_visite_kelas_1_u + $jml_visite_kelas_2_u + $jml_visite_kelas_3_u + $jml_visite_ird_u;

    $jml_konsul_kelas_123_u = $jml_konsul_kelas_1_u + $jml_konsul_kelas_2_u + $jml_konsul_kelas_3_u + $jml_konsul_ird_u;

    $jml_visite_kelas_123_non_u = $jml_visite_kelas_1_non_u + $jml_visite_kelas_2_non_u + $jml_visite_kelas_3_non_u + $jml_visite_ird_non_u;

    $jml_konsul_kelas_123_non_u = $jml_konsul_kelas_1_non_u + $jml_konsul_kelas_2_non_u + $jml_konsul_kelas_3_non_u + $jml_konsul_ird_non_u;

    $total_jaspel123 = $jml_visite_kelas_1 + $jml_visite_kelas_2 + $jml_visite_kelas_3 + 
    $jml_konsul_kelas_1 +$jml_konsul_kelas_2 +$jml_konsul_kelas_3+ $sum_op123 + $biaya_penunjang123 + $biaya_pdp123;
    $total_jaspel123_u = $jml_visite_kelas_123_u + $jml_konsul_kelas_123_u + $sum_op123_u + $biaya_penunjang123_u + $biaya_pdp123_u;
    $total_jaspel123_non_u = $jml_visite_kelas_123_non_u + $jml_konsul_kelas_123_non_u + $sum_op123_non_u + $biaya_penunjang123_non_u + $biaya_pdp123_non_u;


    $jaspel123 = $total_jaspel123 * 0.6;
    $jaspel123_u = $total_jaspel123_u * 0.6;
    $jaspel123_non_u = $total_jaspel123_non_u * 0.6;
    
    $total_jaspelVIP = $jml_visite_vip + $jml_konsul_vip + $sum_opVIP + $biaya_penunjangvip + $biaya_pdpvip;
    $total_jaspelVIP_u = $jml_visite_vip_u + $jml_konsul_vip_u + $sum_opVIP_u + $biaya_penunjangvip_u + $biaya_pdpvip_u;
    $total_jaspelVIP_non_u = $jml_visite_vip_non_u + $jml_konsul_vip_non_u + $sum_opVIP_non_u + $biaya_penunjangvip_non_u + $biaya_pdpvip_non_u;

    $jaspelVIP = $total_jaspelVIP * 0.8;
    $jaspelVIP_u = $total_jaspelVIP_u * 0.8;
    $jaspelVIP_non_u = $total_jaspelVIP_non_u * 0.8;



    $pajak123 = $jaspel123 * 0.05;
    $pajak123_u = $jaspel123_u * 0.05;
    $pajak123_non_u = $jaspel123_non_u * 0.05;
    $pajakVIP = $jaspelVIP * 0.05;
    $pajakVIP_u = $jaspelVIP_u * 0.05;
    $pajakVIP_non_u = $jaspelVIP_non_u * 0.05;

    $bersih123 = $jaspel123 - $pajak123;
    $bersih123_u = $jaspel123_u - $pajak123_u;
    $bersih123_non_u = $jaspel123_non_u - $pajak123_non_u;
    $bersihVIP = $jaspelVIP - $pajakVIP;
    $bersihVIP_u = $jaspelVIP_u - $pajakVIP_u;
    $bersihVIP_non_u = $jaspelVIP_non_u - $pajakVIP_non_u;
    ?>

<div class="row">
<div class="col-xs-12">
  <h2>
  Jasa yang diterima
</h2>
  <table class="table table-bordered" <?=$params == 'xls' ? 'border="1"' : '';?>>
    <tr>
      <th>No</th>
      <th>Uraian</th>
      <th>Visite</th>
      <th>Konsul</th>
      <th>Tindakan<br>OP</th>
      <th>Tindakan<br>Non-OP</th>
      <th>Pendampingan</th>
      <th>Total</th>
      <th>% Jaspel</th>
      <th>Jaspel</th>
      <th>Pajak 5%</th>
      <th>Terima Bersih</th>
    </tr>
    <?php 
    $params = 'non_xls';
    ?>
    <tr>
      <td>1</td>
      <td colspan="5">Pembagian Jaspel Kelas 1,2, dan 3, IRD</td>
      
    </tr>
    <tr>
      <td></td>
      <td>a. BPJS/DA/Asuransi Lain</td>
      
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_kelas_123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_kelas_123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($sum_op123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjang123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdp123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_jaspel123_non_u,0,$params);?></td>
      <td style="text-align: right">60 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jaspel123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($pajak123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($bersih123_non_u,0,$params);?></td>
    </tr>
    <tr>
      <td></td>
      <td>b. UMUM</td>
      
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_kelas_123_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_kelas_123_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($sum_op123_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjang123_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdp123_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_jaspel123_u,0,$params);?></td>
      <td style="text-align: right">60 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jaspel123_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($pajak123_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($bersih123_u,0,$params);?></td>
    </tr>
    <tr>
      <td></td>
      <td>SUBTOTAL</td>
      
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_kelas_123_non_u + $jml_visite_kelas_123_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_kelas_123_non_u + $jml_konsul_kelas_123_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($sum_op123_non_u + $sum_op123_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjang123_u+$biaya_penunjang123_non_u,0,$params);?></td>
       <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdp123_u+$biaya_pdp123_non_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_jaspel123_non_u + $total_jaspel123_u,0,$params);?></td>
      <td style="text-align: right">60 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jaspel123_u + $jaspel123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($pajak123_u + $pajak123_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($bersih123_u + $bersih123_non_u,0,$params);?></td>
    </tr>
    <tr>
      <td>2</td>
      <td colspan="5">Pembagian Jaspel Kelas VIP</td>
      
    </tr>
     <tr>
      <td></td>
      <td>a. BPJS/DA/Asuransi Lain</td>
      
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_vip_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_vip_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($sum_opVIP_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjangvip_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdpvip_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_jaspelVIP_non_u,0,$params);?></td>
      <td style="text-align: right">80 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jaspelVIP_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($pajakVIP_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($bersihVIP_non_u,0,$params);?></td>
    </tr>
    <tr>
      <td></td>
      <td>b. UMUM</td>
      
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_vip_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_vip_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($sum_opVIP_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjangvip_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdpvip_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_jaspelVIP_u,0,$params);?></td>
      <td style="text-align: right">80 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jaspelVIP_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($pajakVIP_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($bersihVIP_u,0,$params);?></td>
    </tr>
    <tr>
      <td></td>
      <td>SUBTOTAL</td>
      
       <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_visite_vip_non_u + $jml_visite_vip_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($jml_konsul_vip_non_u + $jml_konsul_vip_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($sum_opVIP_non_u + $sum_opVIP_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_penunjangvip_u + $biaya_penunjangvip_non_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($biaya_pdpvip_u + $biaya_pdpvip_non_u,0,$params);?></td>
      <td style="text-align: right" style="text-align: right"><?=Yii::app()->helper->formatRupiah($total_jaspelVIP_u +$total_jaspelVIP_non_u,0,$params);?></td>
     <td style="text-align: right">80 %</td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jaspelVIP_u + $jaspelVIP_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($pajakVIP_u + $pajakVIP_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($bersihVIP_u + $bersihVIP_non_u,0,$params);?></td>
    </tr>
    <?php 
    $sumAllVisite = $jml_visite_kelas_123_u + $jml_visite_kelas_123_non_u + $jml_visite_vip_u + $jml_visite_vip_non_u;
    $sumAllKonsul = $jml_konsul_kelas_123_u + $jml_konsul_kelas_123_non_u + $jml_konsul_vip_u + $jml_konsul_vip_non_u;
     $sumAllOP = $sum_op123_u + $sum_op123_non_u + $sum_opVIP_u + $sum_opVIP_non_u;
     $sumAllPenunjang = $biaya_penunjang_sum;
     $sumAllPdp = $biaya_pdp_sum;
    ?>
     <tr>
      <td></td>
      <td><b>TOTAL</b></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatRupiah($sumAllVisite,0,$params);?></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatRupiah($sumAllKonsul,0,$params);?></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatRupiah($sumAllOP,0,$params);?></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatRupiah($sumAllPenunjang,0,$params);?></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatRupiah($sumAllPdp,0,$params);?></td>
      <td style="text-align: right" ><?=Yii::app()->helper->formatRupiah($sumAllKonsul + $sumAllVisite + $sumAllOP + $sumAllPenunjang + $sumAllPdp,0,$params);?></td>
      <td></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($jaspel123_u + $jaspelVIP_u + $jaspel123_non_u + $jaspelVIP_non_u,0,$params);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatRupiah($pajak123_u + $pajakVIP_u + $pajak123_non_u + $pajakVIP_non_u,0,$params);?></td>
      <td style="text-align: right"><b><?=Yii::app()->helper->formatRupiah($bersih123_u + $bersihVIP_u + $bersih123_non_u + $bersihVIP_non_u,0,$params);?></b></td>
    </tr>
  </table>