<?php
/* @var $this SiteController */

$this->breadcrumbs=array(
   array('name' => 'Laporan','url'=>Yii::app()->createUrl('trRawatInap/laporan')),
   array('name' => 'Laporan Dokter','url'=>Yii::app()->createUrl('laporan/dokter')),
);

?>


<div class="row" >
    <div class="col-xs-12">


<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 


?>
    <?php


echo CHtml::link('<span class="label label-info" >Cetak Laporan Pasien </span>',array('laporan/dokterPasien','drid'=>$dokter->id_dokter,'t1'=>$tanggal_awal,'t2'=>$tanggal_akhir,'print'=>1),array('style'=>'color:#fff','target'=>'_blank'));
     
echo '&nbsp;';

echo CHtml::link('<span class="label label-success" >Ekspor ke XLS </span>',array('laporan/dokterPasien','drid'=>$dokter->id_dokter,'t1'=>$tanggal_awal,'t2'=>$tanggal_akhir,'print'=>2),array('style'=>'color:#fff','target'=>'_blank'));
  
  $this->renderPartial('_tbl_dokterPasien', [
    'model' => $model,
    'dokter' => $dokter,
    'rawatInaps' => $rawatInaps,
    'tanggal_awal' => $model->TANGGAL_AWAL,
    'tanggal_akhir' => $model->TANGGAL_AKHIR,
    'params' => $params
  ]

  );   

?>
</div>
</div>
      <?php 
 
  }
?>


      </div>
  </div>
 