<?php 
$tgl = date('d-m-Y',strtotime($model->TANGGAL_AWAL)).'_'.date('d-m-Y',strtotime($model->TANGGAL_AKHIR));

$filename = 'laporan_rawat_inap_'.$tgl.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
header('Content-type: application/excel');

function sum()
{
    return array_sum(func_get_args());
}

$list_penunjangs = TindakanMedisPenunjang::model()->findAll();
$list_obats = ObatAlkes::model()->findAll();
$list_lains = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>6));
$listTnop = TindakanMedisNonOperatif::model()->findAll();
$listTops = TindakanMedisOperatif::model()->findAll();
?>
<table border="1">
  
  <thead>

    <tr>
      <th rowspan="2">No</th>
      <th rowspan="2">No.RM</th>
      <th rowspan="2">Nama Pasien</th>
      <th rowspan="2">Status</th>
      <th rowspan="2">Kls</th>
      <th rowspan="2">Ruang</th>
      <th rowspan="2">Tgl<br>Masuk</th>
      
      <th rowspan="2">Tgl<br>Keluar</th>
      <th rowspan="2">Lm<br>drwt</th>
     <th rowspan="2">DPJP</th>
      <th rowspan="2">Biaya Kamar</th>
      <th rowspan="2">Askep</th>
      <th rowspan="2">Asuhan<br>Nutrisi</th>
      <th rowspan="2">Visite<br>dr Ahli</th>
      <th rowspan="2">Biaya Visite<br>dr Ahli</th>
      <th rowspan="2">Konsul<br>dr Ahli</th>
      <th rowspan="2">Biaya Konsul<br>dr Ahli</th>
      <th rowspan="2">Visite<br>dr Umum</th>
      <th rowspan="2">Biaya Visite<br>dr Umum</th>
      <th colspan="<?=count($listTops);?>">Tindakan Operasi</th>  
      <th rowspan="2">JRS</th>
      <th rowspan="2">JM</th>
      <th rowspan="2">Japel</th>

      <th colspan="<?=count($list_penunjangs);?>">Tindakan Penunjang</th>  
      <th colspan="<?=count($list_obats);?>">Obat</th>  
      <th colspan="<?=count($list_lains);?>">Lain-lain</th>  
      <th rowspan="2">SubTotal</th>
      <th rowspan="2">Paket 1</th>
      <th rowspan="2">Paket 2</th>
      <th rowspan="2">Paket 3</th>
      <th rowspan="2">Biaya Dibayar</th>
    </tr>
    <tr>
     
      
   
      
      <?php 

       foreach($listTops as $mp)
      {
        echo '<th>'.$mp->nama_tindakan.'</th>';
      }
      
      ?>
    
      <?php

      foreach($list_penunjangs as $mp)
      {
        echo '<th>'.$mp->nama_tindakan.'</th>';
      }
      

      foreach($list_obats as $mp)
      {
        echo '<th>'.$mp->kode_alkes.'</th>';
      }

      foreach($list_lains as $mp)
      {
        echo '<th>'.$mp->nama_tindakan.'</th>';
      }
      ?>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
<?php
    $i=1;

    $total = 0;

    $vtotal_kamar = 0;
    $vtotal_askep = 0;
    $vtotal_asnut = 0;
    $vtotal_visite_ahli =0;
    $vtotal_visite_umum = 0;
    $vtotal_konsul_ahli = 0;
    $vtotal_bhp = 0;
    $vtotal_tops = [];
    $vtotal_tnop_jrm = 0;
    $vtotal_tnop_jm = 0;
    $vtotal_tnop_japel = 0;
    $vtotal_penunjangs = [];
    $vtotal_obats = [];
    $vtotal_lains = [];
    $vtotal_total = 0; 
    $vtotal_paket1 = 0;
    $vtotal_paket2 = 0;
    $vtotal_paket3 = 0;
    $vtotal_dibayar = 0;
    foreach($rawatInaps as $rawatInap)
    {

      $vtotal_paket1 += $rawatInap->biaya_paket_1;
      $vtotal_paket2 += $rawatInap->biaya_paket_2;
      $vtotal_paket3 += $rawatInap->biaya_paket_3;

      $rawatRincian = $rawatInap->trRawatInapRincians;
      if(empty($rawatRincian)) 
      {
          // print_r($rawatInap->pasien_id);
          continue;
      }

      $selisih_hari = 1;

      if(!empty($rawatInap->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($rawatInap->tanggal_keluar));
      }

      else
      {
        $dnow = date('Y-m-d');
          $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($rawatInap->tanggal_masuk), Yii::app()->helper->convertSQLDate($dnow));
      }

      $nama_dokter_irna = '';


      if(!empty($rawatInap->dokter)){
        $nama_dokter_irna = $rawatInap->dokter->FULLNAME;
      } 


    
      $subtotal_biaya_visite_ahli = 0;
      $subtotal_biaya_visite_umum = 0;
      $nama_dokter_visite_ahli = '';
      $nama_dokter_visite_umum = '';
      $biaya_visite_ahli = 0;
      $biaya_visite_umum = 0;



      $subtotal_visite = 0;

      $visiteAhli = array();
      $visiteUmum = array();

        $idx1 = 0;
      $idx2 = 0;

      foreach($rawatInap->getDataVisite('VISITE') as $visite)
      {
          if(empty($visite->jenisVisite)) 
            continue;

          if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Ahli'))
          {
            
             $subtotal_biaya_visite_ahli += $visite->biaya_visite_irna;
             
             if(!empty($visite->dokterIrna))
             {

                if(trim($visite->dokterIrna->jenis_dokter) == 'AHLI')
                {
                  $visiteAhli[] = array(
                    'nama' => $visite->dokterIrna->FULLNAME,
                    'biaya' => $visite->biaya_visite_irna
                   );
                   
                   if($idx1==0){
                      $nama_dokter_visite_ahli = $visite->dokterIrna->FULLNAME;
                      $biaya_visite_ahli = $visite->biaya_visite_irna;
                   }

                    $idx1++;
                }

                else  if((trim($visite->dokterIrna->jenis_dokter) )== 'UMUM')
                {
                  $visiteUmum[] = array(
                    'nama' => $visite->dokterIrna->FULLNAME,
                   'biaya' => $visite->biaya_visite_irna
                   );
                   

                   
                   if($idx2==0){
                      $nama_dokter_visite_umum = $visite->dokterIrna->FULLNAME;
                      $biaya_visite_umum = $visite->biaya_visite_irna;
                   }

                     $idx2++;
                 }

               
             }

          }

          if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Umum'))
          {

             if(!empty($visite->dokterIrna) && (trim($visite->dokterIrna->jenis_dokter)) == 'UMUM')
             {
              $visiteUmum[] = array(
                  'nama' => !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME : '',
                  'biaya' => $visite->biaya_visite_irna
               );
              if($idx2==0){
                $nama_dokter_visite_umum = !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME.', ' : '';
                $biaya_visite_umum = $visite->biaya_visite_irna;
              }
            }
            $subtotal_biaya_visite_umum += $visite->biaya_visite_irna;
            
             
            // $nama_dokter_visite_umum .= !empty($visite->dokterIrna) ? $visite->dokterIrna->NICKNAME.', ' : '';
            $idx2++;
          }

          // if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Umum'))
          // {
          //   $visiteUmum[] = array(
          //       'nama' => !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME : '',
          //       'biaya' => $visite->biaya_visite_irna
          //    );
          //   $subtotal_biaya_visite_umum += $visite->biaya_visite_irna;
          //   $nama_dokter_visite_umum .= !empty($visite->dokterIrna) ? $visite->dokterIrna->NICKNAME.', ' : '';
          // }
      }

      $subtotal_visite = $subtotal_biaya_visite_umum + $subtotal_biaya_visite_ahli;  
      $subtotal_biaya_konsul_ahli = 0;
      $nama_dokter_konsul_ahli = '';
      $biaya_konsul_ahli = 0;
      $subtotal_konsul = 0;
      $konsulAhli = array();
        $idx3=0;
      foreach($rawatInap->getDataVisite('KONSUL') as $visite)
      {
        if(empty($visite->jenisVisite)) 
          continue;

        if(Yii::app()->helper->contains($visite->jenisVisite->nama_visite, 'Ahli'))
        {
           $konsulAhli[] = array(
                'nama' => !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME : '',
                'biaya' => $visite->biaya_visite_irna
             );
           $subtotal_biaya_konsul_ahli += $visite->biaya_visite_irna;
           if($idx3==0){
              $nama_dokter_konsul_ahli = !empty($visite->dokterIrna) ? $visite->dokterIrna->FULLNAME.', ' : '';
              $biaya_konsul_ahli = $visite->biaya_visite_irna;
           }
             
           // $nama_dokter_konsul_ahli = !empty($visite->dokterIrna) ? $visite->dokterIrna->NICKNAME.', ' : '';
           $idx3++;
        }
      }

      $subtotal_konsul = $subtotal_biaya_konsul_ahli;  

      $visiteSort = array(
        count($visiteAhli),
        count($visiteUmum),
        count($konsulAhli)
      );


      rsort($visiteSort);
      

      $subtotal_jrm = 0;
      $subtotal_jm = 0;
      $subtotal_japel = 0;
       foreach($listTnop as $tnop)
       {
          $attr = array(
            'id_rawat_inap' => $rawatInap->id_rawat_inap,
            'id_tnop' => $tnop->id_tindakan
          );
          $itemTnop = TrRawatInapTnop::model()->findByAttributes($attr);

          if(!empty($itemTnop)){
            
            if($tnop->kode_tindakan == 'jrm'){
              $subtotal_jrm += $itemTnop->biaya_irna;  
            }

            else if($tnop->kode_tindakan == 'jm'){
              $subtotal_jm +=  $itemTnop->biaya_irna;  
            }

            else if($tnop->kode_tindakan == 'japel'){
              $subtotal_japel += $itemTnop->biaya_irna;  
            }
          }
       }   
  ?>



    <tr>
       <td><?php echo $i++;?></td>
       <td>&nbsp;<?php echo $rawatInap->pASIEN->NoMedrec;?></td>
      <td><?php echo $rawatInap->pASIEN->NAMA;?></td>
      <td><?php echo $rawatInap->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $rawatInap->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      <?php echo $rawatInap->kamar->nama_kamar;?>
      </td>
      <td><?php echo $rawatInap->tanggal_masuk;?></td>
      <td><?php echo $rawatInap->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      <td><?=$nama_dokter_irna;?></td>
      <td style="text-align: right">
        <?php
        $isneonatus = $rawatInap->kamar->nama_kamar == 'NEONATUS';
      $biaya_kamar = 0;
      if($isneonatus)
      {


        $biaya_neonatus = $rawatInap->biaya_kamar;
        $biaya_kamar = $biaya_neonatus;
        
      }

      else
      {

        if($rawatInap->kamar->kelas->kode_kelas != 'IRD')
        {
        
          $biaya_kamar = $rawatInap->kamar->biaya_kamar * $selisih_hari;   
        }

        else{

          echo '0';

        }
      }

      echo Yii::app()->helper->formatBulat($biaya_kamar);
        
      $subtotal = $biaya_kamar;
      $vtotal_kamar += $biaya_kamar;

        ?></td>
      <td style="text-align: right"><?php 
      echo Yii::app()->helper->formatBulat($rawatInap->kamar->biaya_askep * $selisih_hari);
      // echo Yii::app()->helper->formatBulat($rawatRincian->askep_kamar);
      $subtotal = $subtotal + $rawatInap->kamar->biaya_askep * $selisih_hari;
      $vtotal_askep += $rawatInap->kamar->biaya_askep * $selisih_hari;
      ?></td>
      <td style="text-align: right"><?php
      
      echo Yii::app()->helper->formatBulat($rawatRincian->asuhan_nutrisi);
      $subtotal = $subtotal + $rawatRincian->asuhan_nutrisi;
      $vtotal_asnut += $rawatRincian->asuhan_nutrisi;
      ?></td>
      <td><?=$nama_dokter_visite_ahli;?></td>
      <td style="text-align: right"><?php
      echo Yii::app()->helper->formatBulat($subtotal_biaya_visite_ahli);
      $subtotal += $subtotal_biaya_visite_ahli;
      $vtotal_visite_ahli += $subtotal_biaya_visite_ahli; 
      ?></td>
      <td><?=$nama_dokter_konsul_ahli;?></td>
      <td style="text-align: right"><?php 
      echo Yii::app()->helper->formatBulat($subtotal_biaya_konsul_ahli);
      $subtotal += $subtotal_biaya_konsul_ahli;
      $vtotal_konsul_ahli += $subtotal_biaya_konsul_ahli;
      ?></td>
       <td><?=$nama_dokter_visite_umum;?></td>
      <td style="text-align: right"><?php 
      echo Yii::app()->helper->formatBulat($subtotal_biaya_visite_umum);
      $subtotal += $subtotal_biaya_visite_umum;
      $vtotal_visite_umum += $subtotal_biaya_visite_umum;
      ?></td>
      <?php 

      $value_tmp = [];
      $subvtotal_tops = [] ;
      foreach($listTops as $mp)
      {
        $attr = array(
          'id_rawat_inap' => $rawatInap->id_rawat_inap,
          'id_top' => $mp->id_tindakan
        );
        $itemTop = TrRawatInapTop::model()->findAllByAttributes($attr);

        $jumlah_tindakan = 0;
        $biaya_irna = 0;

        $valueJm = '';
        $valueAnas = '';
        
        $value_tmp[$mp->id_tindakan] = 0;
        foreach($itemTop as $supp)
        {

          if($supp->biaya_irna == 0) continue;
          
          $value_tmp[$mp->id_tindakan] = $supp->biaya_irna;
          
        }
        

        echo '<td style="text-align: right">'.Yii::app()->helper->formatBulat($value_tmp[$mp->id_tindakan]).'</td>';   
        $subtotal = $subtotal + $value_tmp[$mp->id_tindakan];

        $subvtotal_tops[$mp->id_tindakan] = $value_tmp[$mp->id_tindakan];
        
      }

      $vtotal_tops[] = $subvtotal_tops;
      ?>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($subtotal_jrm);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($subtotal_jm);?></td>
      <td style="text-align: right"><?=Yii::app()->helper->formatBulat($subtotal_japel);?></td>
      <?php

      $subtotal = $subtotal + $subtotal_jrm + $subtotal_japel + $subtotal_jm;
      $vtotal_tnop_jrm +=$subtotal_jrm;
      $vtotal_tnop_jm +=$subtotal_jm;
      $vtotal_tnop_japel +=$subtotal_japel;


      $value_tmp = [];
      $subvtotal_penunjangs = [] ;
      foreach($list_penunjangs as $mp)
      {

        $attr = array(
          'id_rawat_inap' => $rawatInap->id_rawat_inap,
          'id_penunjang' => $mp->id_tindakan_penunjang
        );

        $supps = TrRawatInapPenunjang::model()->findAllByAttributes($attr);
        $value_tmp[$mp->id_tindakan_penunjang] = 0;
        $biaya_irna_penunjangs_total = 0;
        foreach($supps as $supp)
        {

          if($supp->biaya_irna == 0) continue;
          $value_tmp[$mp->id_tindakan_penunjang] = $supp->biaya_irna;

          $biaya_irna_penunjangs_total+= $supp->biaya_irna;
        }

        $subvtotal_penunjangs[$mp->id_tindakan_penunjang] = $biaya_irna_penunjangs_total;
          
        echo '<td style="text-align: right">'.Yii::app()->helper->formatBulat($value_tmp[$mp->id_tindakan_penunjang]).'</td>';
        $subtotal = $subtotal + $value_tmp[$mp->id_tindakan_penunjang];
        
      }

      $vtotal_penunjangs[] = $subvtotal_penunjangs;


      $value_tmp = [];
      $subvtotal_obats = [] ;
      foreach($list_obats as $mp)
      {

        $attr = array(
          'id_rawat_inap' => $rawatInap->id_rawat_inap,
          'id_alkes' => $mp->id_obat_alkes
        );

        $supps = TrRawatInapAlkes::model()->findAllByAttributes($attr);
        $value_tmp[$mp->id_obat_alkes] = 0;
        $biaya_irna_obats_total = 0;
        foreach($supps as $supp)
        {

          if($supp->biaya_irna == 0) continue;
          $value_tmp[$mp->id_obat_alkes] = $supp->biaya_irna;

          $biaya_irna_obats_total += $supp->biaya_irna;
          

        }

        $subvtotal_obats[$mp->id_obat_alkes] = $biaya_irna_obats_total;
        $subtotal = $subtotal + $value_tmp[$mp->id_obat_alkes];
        echo '<td style="text-align: right">'.Yii::app()->helper->formatBulat($value_tmp[$mp->id_obat_alkes]).'</td>';
        
      }

      $vtotal_obats[] =$subvtotal_obats;
      $tmp = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>$rawatInap->kamar->kelas_id));
      $value_tmp = [];
       $subvtotal_lains = [] ;
      foreach($tmp as $mp)
      {

        $attr = array(
          'id_rawat_inap' => $rawatInap->id_rawat_inap,
          'id_lain' => $mp->id_tindakan
        );

        $supps = TrRawatInapLain::model()->findAllByAttributes($attr);
        $value_tmp[$mp->id_tindakan] = 0;
        $biaya_irna_lains_total = 0;
        foreach($supps as $supp)
        {

          if($supp->biaya_irna == 0) continue;
          
          $value_tmp[$mp->id_tindakan] = $supp->biaya_irna;

          $biaya_irna_lains_total += $supp->biaya_irna;

        }
        $subtotal = $subtotal + $value_tmp[$mp->id_tindakan];
        echo '<td style="text-align: right">'.Yii::app()->helper->formatBulat($value_tmp[$mp->id_tindakan]).'</td>';
        
        $subvtotal_lains[$mp->id_tindakan] = $biaya_irna_lains_total;
        
      }

      $vtotal_lains[] = $subvtotal_lains;
      $vtotal_total += $subtotal;

      $nilai_total = 0;


      if(strpos($rawatInap->jenisPasien->NamaGol, 'JAMKESDA') !== false )
      {
        $nilai_total = 0;
      }

      else if(strpos($rawatInap->jenisPasien->NamaGol, 'JR') !== false )
      {
          
            $nilai_total = $subtotal > 20000000 ? $subtotal - 20000000 : 0;
         
      }
      
      else if(strpos($rawatInap->jenisPasien->NamaGol, 'BPJS') !== false )
      {
        $nama_kelas = $rawatInap->kamar->kelas->nama_kelas;


        // Khusus VIP
        if($nama_kelas == 'VIP' && $rawatInap->biaya_paket_1 != 0)
        {
          
          $selisih_maks = 0;
          // nilai real VIP > 175 % dari tarif INA CBG kelas 1
          if(($subtotal / $rawatInap->biaya_paket_1) > 1.75)
          {

            $selisih_maks = 0.75 * $rawatInap->biaya_paket_1;
            
          }
          
          // nilai real VIP < 175 % dari tarif INA CBG kelas 1 dan real > paket 1
          else if(($subtotal / $rawatInap->biaya_paket_1) <= 1.75 && $subtotal >= $rawatInap->biaya_paket_1)
          {
            $selisih_maks = $subtotal - $rawatInap->biaya_paket_1;
            
          }

          $selisih_inacbg = 0;
          if($rawatInap->biaya_paket_3 != 0)
          {
            $selisih_inacbg = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_3;     
          }

          else if($rawatInap->biaya_paket_2 != 0)
          {
            $selisih_inacbg = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_2;
          }



          $tambahan_pasien = $selisih_maks + $selisih_inacbg;
          $pendapatan_rs = $rawatInap->biaya_paket_1 + $tambahan_pasien;

          $nilai_total = $tambahan_pasien;
        }

        // NON VIP
        else
        {
          if($rawatInap->biaya_paket_1 != 0 && $rawatInap->biaya_paket_3 != 0)
          {
            $selisih_paket = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_3; 
          } 

          else if($rawatInap->biaya_paket_2 != 0 && $rawatInap->biaya_paket_3 != 0)
          {
            $selisih_paket = $rawatInap->biaya_paket_2 - $rawatInap->biaya_paket_3;
          }
          
          else if($rawatInap->biaya_paket_1 != 0 && $rawatInap->biaya_paket_2 != 0)
          {
            $selisih_paket = $rawatInap->biaya_paket_1 - $rawatInap->biaya_paket_2;
          }

          else{
            $selisih_paket = 0;//$subtotal;

          }

          // $nilai_total = $subtotal - $selisih_paket;
          $nilai_total = $selisih_paket;


        }
      }

      

      else
      {
        $nilai_total = $subtotal;
      }

      $vtotal_dibayar += $nilai_total;
      ?>
      <td><?=Yii::app()->helper->formatBulat($subtotal);?></td>
      <td><?=$rawatInap->biaya_paket_1;?></td>
      <td><?=$rawatInap->biaya_paket_2;?></td>
      <td><?=$rawatInap->biaya_paket_3;?></td>
      <td><?=$nilai_total;?></td>
     </tr>
<?php 
$topVisite = $visiteSort[0];
// print_r($topVisite);exit;
for($idx =1;$idx < $topVisite ; $idx++)
{

    $nmVAhli = !empty($visiteAhli[$idx]) ? $visiteAhli[$idx]['nama']: '';
    $nmVUmum = !empty($visiteUmum[$idx]) ? $visiteUmum[$idx]['nama']: '';
    $nmKAhli = !empty($konsulAhli[$idx]) ? $konsulAhli[$idx]['nama']: '';

    $bVAhli = !empty($visiteAhli[$idx]) ? $visiteAhli[$idx]['biaya']: 0;
    $bVUmum = !empty($visiteUmum[$idx]) ? $visiteUmum[$idx]['biaya']: 0;
    $bKAhli = !empty($konsulAhli[$idx]) ? $konsulAhli[$idx]['biaya']: 0;

    // if()
    $vtotal_visite_ahli += $bVAhli;
    $vtotal_visite_umum += $bVUmum;
    $vtotal_konsul_ahli += $bKAhli;
?>


 <tr>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td></td>
      <td></td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td></td>
      <td></td>
      <td style="text-align: right"></td>
      <td style="text-align: right"></td>
      <td style="text-align: right"></td>
      <td><?=$nmVAhli;?></td>
      <td style="text-align: right"><?php
      echo Yii::app()->helper->formatBulat($bVAhli);
      ?></td>
      <td><?=$nmKAhli;?></td>
      <td style="text-align: right"><?php 
      echo Yii::app()->helper->formatBulat($bKAhli);
      ?></td>
       <td><?=$nmVUmum;?></td>
      <td style="text-align: right"><?php 
      echo Yii::app()->helper->formatBulat($bVUmum);
      ?></td>
      <?php 
      foreach($listTops as $mp)
      {
    echo '<td style="text-align: right"></td>';   
      }
 ?>
      <td style="text-align: right"></td>
      <td style="text-align: right"></td>
      <td style="text-align: right"></td>
      <?php
    foreach($list_penunjangs as $mp)
      {

    echo '<td style="text-align: right"></td>';
      
    }

    foreach($list_obats as $mp)
      {
        echo '<td style="text-align: right"></td>';
      }

      $tmp = TindakanMedisLain::model()->findAllByAttributes(array('kelas_id'=>$rawatInap->kamar->kelas_id));
      
    foreach($tmp as $mp)
      {
        echo '<td style="text-align: right"></td>';
      }
      ?>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
     </tr>
<?php 
}
?>
    <?php 
      
}
?>
      <!-- <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th> -->
      <th colspan="10" style="text-align: right">TOTAL</th>
      <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_kamar);?></th>
      <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_askep);?></th>
      <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_asnut);?></th>
      <th></th>
      <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_visite_ahli);?></th>
      <th></th>
      <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_konsul_ahli);?></th>
      <th></th>
      <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_visite_umum);?></th>
      <?php 
      $sumResult = call_user_func_array('array_map', array_merge(['sum'], $vtotal_tops));

     
      

      
      $j=0;
      foreach($listTops as $mp)
      {
        echo '<th style="text-align: right">'.Yii::app()->helper->formatBulat($sumResult[$j]).'</th>';
        $j++;
      }
      
      ?>
     <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_tnop_jrm);?></th>
      <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_tnop_jm);?></th>
      <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_tnop_japel);?></th>

      <?php
      $sumResult = call_user_func_array('array_map', array_merge(['sum'], $vtotal_penunjangs));
      
      $j=0;
      foreach($list_penunjangs as $mp)
      {
        echo '<th style="text-align: right">'.Yii::app()->helper->formatBulat($sumResult[$j]).'</th>';
        $j++;
      }
      
      $sumResult = call_user_func_array('array_map', array_merge(['sum'], $vtotal_obats));
      
     $j=0;
      foreach($list_obats as $mp)
      {
        echo '<th style="text-align: right">'.Yii::app()->helper->formatBulat($sumResult[$j]).'</th>';
        $j++;
      }
      $sumResult = call_user_func_array('array_map', array_merge(['sum'], $vtotal_lains));
      
      $j=0;
      foreach($list_lains as $mp)
      {
        echo '<th style="text-align: right">'.Yii::app()->helper->formatBulat($sumResult[$j]).'</th>';
        $j++;
      }
      ?>
       <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_total);?></th>
       <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_paket1);?></th>
       <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_paket2);?></th>
       <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_paket3);?></th>
       <th style="text-align: right"><?=Yii::app()->helper->formatBulat($vtotal_dibayar);?></th>
  </tbody>

</table>