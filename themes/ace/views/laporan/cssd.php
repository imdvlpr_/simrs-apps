<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Laporan Tindakan CSSD','url'=>Yii::app()->createUrl('laporan/cssd')),
);
  $baseUrl = Yii::app()->theme->baseUrl; 
    $cs = Yii::app()->getClientScript();


?>

<div class="row" >
    <div class="col-xs-12">
        
          
         
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  'method' => 'get',
  'action' => Yii::app()->createUrl('laporan/cssd'),
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
              
              ?>
            
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    
     
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>
          <?php 
          
           if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR)){
           $link = array('laporan/cssd','tgl_awal'=>$model->TANGGAL_AWAL,'tgl_akhir'=>$model->TANGGAL_AKHIR,'print'=>2);
            echo CHtml::link('Ekspor XLS Laporan CSSD',$link,array('class'=>'btn btn-success'));
        }
          ?>
  </div>
      </div>
        
      
   <?php
       
  $this->endWidget();?>


<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 
?>

  <?php $this->renderPartial('cssd_table',array(
          'model'=>$model,
          'tanggal_awal' => $model->TANGGAL_AWAL,
          'tanggal_akhir' => $model->TANGGAL_AKHIR,
          'print' => $print
  ));?>

<?php

  }
?>



                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>