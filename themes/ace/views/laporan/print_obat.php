<table cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th>
      LAPORAN OBAT <?php 
       $dmkamar = DmKamar::model()->findByPk($kamar_pasien);
      echo !empty($dmkamar) ? $dmkamar->nama_kamar : '';
      ?><br>
      Dari tanggal <?php echo date('d/m/Y',strtotime($model->TANGGAL_AWAL)).' hingga '.date('d/m/Y',strtotime($model->TANGGAL_AKHIR));?>
      </th>
      
      
    </tr>
  </thead>
  </table>
<table border="1" cellpadding="4" style="font-size: 8px">
  
  <thead>
    <tr>
      <th width="3%">No</th>
      <th width="7%">No Rekam Medis</th>
      <th  width="15%">Nama Pasien</th>
      <th width="7%">Status</th>
      <th>Klas</th>
      <th>Ruang</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th width="5%">LM DI<br>RWT</th>
     <th>Obat IRD</th>
     <th>Obat Kamar</th>
      <th>JUMLAH</th>
    </tr>
  </thead>
  <tbody>
  <?php 
 $i=1;

    $total_obat = 0;

    $total_ird = 0;
    $total_kamar = 0;
    foreach($model->searchLaporan() as $ri)
    {
      $total_ird = $total_ird + $ri->sumObatIrd;
      $total_kamar = $total_kamar + $ri->sumObat;

      $total_obat_item = $ri->sumObat + $ri->sumObatIrd;
          
      $total_obat = $total_obat + $total_obat_item;
      $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      if($total_obat_item == 0) continue;
  ?>
    <tr>
       <td width="3%"><?php echo $i++;?></td>
       <td width="7%"><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td  width="15%"><?php echo $ri->pASIEN->NAMA;?></td>
      <td width="7%"><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
       <td>
      <?php echo $ri->kamar->nama_kamar;?>
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td  width="5%"><?php echo $selisih_hari;?></td>
      <td style="text-align: right;"><?php 
        
         echo Yii::app()->helper->formatRupiah($ri->sumObatIrd);
              ?></td>
      <!-- <td>0</td> -->
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($ri->sumObat);
              ?></td>
     <td style="text-align: right;">
         <?php 
        echo Yii::app()->helper->formatRupiah($total_obat_item);
         ?>
       </td>
     </tr>

    <?php 
      
    }

    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
     
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
       <td>&nbsp;</td>
     <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_ird);
      ?></strong></td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_kamar);
      ?></strong>
      </td>
      <td style="text-align: right;"><strong>
        <?php 
 echo Yii::app()->helper->formatRupiah($total_obat);
        ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>