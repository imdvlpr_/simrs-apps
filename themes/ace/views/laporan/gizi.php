<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Forms';
$this->breadcrumbs=array(
  array('name' => 'Laporan Gizi','url'=>Yii::app()->createUrl('laporan/gizi')),
);


?>

<div class="row" >
    <div class="col-xs-12">
        
          
         
                          <?php 
                $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
    ),  
));
              
              ?>
            
              <?php 
echo $form->errorSummary($model,'<div class="alert alert-danger">Silakan perbaiki beberapa eror berikut:','</div>');

?>          
    
     
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Antara tanggal</label>
            <div class="col-sm-9">
            <?php 

       $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AWAL',
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>'',
              'class' => 'input'
          ),
      ));
   
        ?>
           </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="focusedInput">Sampai tanggal</label>
            <div class="col-sm-9">
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
          'model' => $model,
          'attribute' => 'TANGGAL_AKHIR',
          
          'options'=>array(
              'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
              'showOtherMonths'=>true,// Show Other month in jquery
              'selectOtherMonths'=>true,// Select Other month in jquery
               'dateFormat'=>'dd/mm/yy',
                'changeMonth' => true,
                'changeYear' => true,
          ),
          'htmlOptions'=>array(
              'style'=>''
          ),
      ));
        ?>
              
            </div>
          </div>
                  <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
          <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Lihat Laporan
          </button>

        
      
  <?php $this->endWidget();?>
  </div>
      </div>
 <div class="form-group" align="center">
  <div class="col-sm-9">

      <div class="span12">
<?php 
  if(!empty($model->TANGGAL_AWAL) && !empty($model->TANGGAL_AKHIR))
  { 

     $form=$this->beginWidget('CActiveForm', array(
  'id'=>'partner-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'htmlOptions' => array(
      'class'=>"form-horizontal",
      'target' => '_blank'
    ),  
));

echo $form->hiddenField($model,'TANGGAL_AWAL');
echo $form->hiddenField($model,'TANGGAL_AKHIR');
echo CHtml::dropDownList('print','',array(1=>'PDF',2=>'XLS'));
echo '&nbsp;';
echo CHtml::submitButton('Cetak Laporan Gizi',array('class'=>'btn btn-info'));


$this->endWidget();

?>
</div>
</div>
</div>
<table class="table table-bordered" >
  
  <thead>
    <tr>
      <th>No</th>
      <th>No Rekam Medis</th>
      <th>Nama Pasien</th>
      <th>Status Px</th>
      <th>Klas</th>
      <th>Instalasi</th>
      <th>Tgl. Masuk</th>
      <th>Tgl. Keluar</th>
       <th>Lm Dirawat</th>
      <th>Asuhan Nutrisi</th>
      <!-- <th>Konsul Gizi</th> -->
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody>
  <?php 

    $i=1;

    $total_asnut = 0;
    
    foreach($model->searchLaporan() as $ri)
    {

      $asnut = !empty($ri->trRawatInapRincians) ? $ri->trRawatInapRincians->asuhan_nutrisi : 0;

      if($asnut == 0) continue;


       $selisih_hari = 1;

      if(!empty($ri->tanggal_keluar))
      {
        $selisih_hari = Yii::app()->helper->getSelisihHariInap(Yii::app()->helper->convertSQLDate($ri->tanggal_masuk), Yii::app()->helper->convertSQLDate($ri->tanggal_keluar));
      }

      if(!empty($tr))
      $total_asnut = $total_asnut + $asnut;
        
  ?>
    <tr>
       <td><?php echo $i++;?></td>
       <td><?php echo $ri->pASIEN->NoMedrec;?></td>
      <td><?php echo $ri->pASIEN->NAMA;?></td>
      <td><?php echo $ri->jenisPasien->NamaGol;?></td>
      <td>
      <?php echo $ri->kamar->kelas->nama_kelas;?>
      </td>
      <td>
      Gizi
      </td>
      <td><?php echo $ri->tanggal_masuk;?></td>
      <td><?php echo $ri->tanggal_keluar;?></td>
      <td><?php echo $selisih_hari;?></td>
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($asnut);
              ?></td>
     <!--  <td>0</td> -->
      <td style="text-align: right;"><?php 
        echo Yii::app()->helper->formatRupiah($asnut);
              ?></td>
     </tr>
    <?php 
      
    }
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>JUMLAH</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <!-- <td>&nbsp;</td> -->
      <td>&nbsp;</td>
      <td>&nbsp;</td>
       <td>&nbsp;</td>
      <td style="text-align: right;"><strong>
        <?php 
       echo Yii::app()->helper->formatRupiah($total_asnut);
      ?></strong>
      </td>
      
    </tr>
  </tbody>

</table>
<?php


  }
?>


                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
  
</div>