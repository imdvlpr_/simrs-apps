

$(document).on('keydown','input', function(e) {
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if (e.ctrlKey && e.keyCode == 13) {
        $('form').submit();
    }
    else if(key == 13) {
   		e.preventDefault();
		var inputs = $(this).closest('form').find(':input:visible');
	          
        inputs.eq( inputs.index(this)+ 1 ).focus().select();
        $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});