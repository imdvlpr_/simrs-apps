
function toggleLoading(selector, show) {
  if(show){
  	$(selector).parent().prepend('<img class="loadergif" src="/images/loading.gif">');	
  }
   
  else{
  	$('img.loadergif').remove();	
  }
   
}



function focusNextElement() {
  var focussableElements = 'a:not([disabled]), button:not([disabled]), input[type=text]:not([disabled]), [tabindex]:not([disabled]):not([tabindex="-1"])';
  if (document.activeElement && document.activeElement.form) {
    var focussable = Array.prototype.filter.call(document.activeElement.form.querySelectorAll(focussableElements),
      function(element) {
        return element.offsetWidth > 0 || element.offsetHeight > 0 || element === document.activeElement
      });
    var index = focussable.indexOf(document.activeElement);
    focussable[index + 1].focus();
  }
}

function removePoints(str){
	var value = str;
	return value.split('.').join("");
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function doGetCaretPosition (oField) {

  // Initialize
  var iCaretPos = 0;

  // IE Support
  if (document.selection) {

    // Set focus on the element
    // oField.focus();

    // To get cursor position, get empty selection range
    var oSel = document.selection.createRange();

    // Move selection start to 0 position
    oSel.moveStart('character', -oField.value.length);

    // The caret position is selection length
    iCaretPos = oSel.text.length;
  }

  // Firefox support
  else if (oField.selectionStart || oField.selectionStart == '0')
    iCaretPos = oField.selectionStart;

  // Return results
  return iCaretPos;
}

function addCalculator(){

  var html = '<a href="javascript:void(0)" class="formula-img"><img src="images/calculator.png"></a>&nbsp;';
	 html += '<input type="hidden" class="formula_bar"/>';

	$('.uang').prev().prev('.formula-img').remove(); 
	$('.uang').before(html);
	
}


$(function() {
	
	$.mask.definitions['~']='[+-]';
	$('.input-mask-date').mask('99/99/9999');
	$('.input-mask-phone').mask('(999) 999-9999');
	$('.input-mask-eyescript').mask('~9.99 ~9.99 999');
	$(".input-mask-product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});
	
	addCalculator();

	  $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true,
        dateFormat : 'dd/mm/yy'
    })
    //show datepicker when clicking on the icon
    .next().on(ace.click_event, function(){
        $(this).prev().focus();
    });

	$(document).on('click','a.formula-img',function(evt){
		var formulaBar = $(this).next();
		var txt = $(this).next().next();
		if(formulaBar.val() == ''){
			txt.val('='+removePoints(txt.val()));
		}

		else{
			txt.val(formulaBar.val());
		}
		txt.attr('class', 'input-medium formula');
		txt.focus();
		// if(txt.val()==''){
		// 	txt.val('=');
		// }
		// txt.val('=');

		
		$(this).val(formulaBar.val());

	});

	// $(document).on('dblclick','input.formula',function(evt){
	// 	var formulaBar = $(this).next();
	// 	$(this).val(formulaBar.val());
	// });

	$(document).on("keyup","input.formula",function(event){
		
		
		if(event.which == 13){
			var txt = $(this).val();
			var formulaBar = $(this).prev();

			if(txt.charAt(0)=='='){
				formulaBar.val(txt);
				
				txt = txt.replace('=','');
				
				$(this).val(eval(txt));
				$(this).val(addCommas($(this).val()));
				
			}

			$(this).attr('class', 'input-medium uang');
		}

		
	});

	$(document).on("keyup","input.uang",function(event){
		
	// skip for arrow keys
		if(event.which >= 37 && event.which <= 40) return;

		// format number
		$(this).val(function(index, value) {
		  return value
		  .replace(/\D/g, "")
		  .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		  ;
		});
	});
	
    // Side Bar Toggle
    $('.hide-sidebar').click(function() {
	  $('#sidebar').hide('fast', function() {
	  	$('#content').removeClass('span9');
	  	$('#content').addClass('span12');
	  	$('.hide-sidebar').hide();
	  	$('.show-sidebar').show();
	  });
	});

	$('.show-sidebar').click(function() {
		$('#content').removeClass('span12');
	   	$('#content').addClass('span9');
	   	$('.show-sidebar').hide();
	   	$('.hide-sidebar').show();
	  	$('#sidebar').show('fast');
	});


	$(document).on('keydown','input', function(e) {
	    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	    

	    if (e.ctrlKey && e.keyCode == 13) {
	        $('form').submit();
	    }
	    else if(key == 13) {
	        

	        var nama_class = $(this).attr('class');

	        if(nama_class != null) {
	        	e.preventDefault();
		        if(nama_class.includes('per_item')) {
		            
		            var input = $(this).val();

		            if(jQuery.isNumeric(input))
		            {
		                var item = $(this).prev('input');
		              
		                var nAskep = eval(item.val()) + eval(input);
		                $(item).val(nAskep);
		                $(this).val(0);
		                $(this).select();
		            }
		        }
			}

			// focusNextElement();
	    }

	    var actElement = document.activeElement;
	    curPos = doGetCaretPosition(actElement);


	    switch(e.which){
	        case 37: // left
	        
	          if(curPos == 0){
	            var inputs = $(this).closest('form').find(':input:visible');
	    
	            inputs.eq( inputs.index(this)- 1 ).focus();
	            
	            $('html, body').animate({
	                scrollTop: $(this).offset().top - 100
	            }, 10);

	          }
	        break;

	        case 39: // right


	            if(curPos == actElement.value.length){
	          
	              var inputs = $(this).closest('form').find(':input:visible');
	          
	              inputs.eq( inputs.index(this)+ 1 ).focus();
	              $('html, body').animate({
	                    scrollTop: $(this).offset().top - 100
	              }, 10);

	              
	            }
	        break;

	        default: 
	        return; 
	    }
	});

	$(document).on('focus','input',function(){
		$(this).select();
	});
	// $('input').on('focus', function (e) {
	    
	// });

	$(document).on("click",".hapus-dokter",function(){
  
	  var ukuran = $(this).closest("#tbody-dokter").children('.row-dokter').size();
	  


	  if(ukuran>1){
	      
	      $(this).parent().parent().remove();

	      var i = 1;
	      $('span.number').each(function(){
	        $(this).html(i+". ");
	        i++;
	       
	      });
	      return false;
	  }




	});



});